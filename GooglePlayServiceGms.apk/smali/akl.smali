.class public final Lakl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;

.field public static h:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "appstate.base_service_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->a:Lbfy;

    const-string v0, "appstate.server_api_path"

    const-string v1, "/appstate/v1/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->b:Lbfy;

    const-string v0, "appstate.internal_server_api_path"

    const-string v1, "/appstate/v1whitelisted/states/"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->c:Lbfy;

    const-string v0, "appstate.cache_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->d:Lbfy;

    const-string v0, "appstate.verbose_volley_logging"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->e:Lbfy;

    const-string v0, "appstate.max_state_bytes"

    const/high16 v1, 0x40000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->f:Lbfy;

    const-string v0, "appstate.max_state_keys"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->g:Lbfy;

    const-string v0, "appstate.sync_buffer_millis"

    const-wide/16 v1, 0x7530

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lakl;->h:Lbfy;

    return-void
.end method
