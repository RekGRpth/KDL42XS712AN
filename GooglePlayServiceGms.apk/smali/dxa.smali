.class public final Ldxa;
.super Ldxh;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/view/LayoutInflater;

.field private c:Ljava/lang/String;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ldxh;-><init>()V

    iput v0, p0, Ldxa;->e:I

    iput v0, p0, Ldxa;->f:I

    iput-object p1, p0, Ldxa;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldxa;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-object v0, p0, Ldxa;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Ldxa;->b(I)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldxa;->c:Ljava/lang/String;

    invoke-virtual {p0}, Ldxa;->notifyDataSetChanged()V

    return-void
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Ldxa;->e:I

    invoke-virtual {p0}, Ldxa;->notifyDataSetChanged()V

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Ldxa;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldxa;->f:I

    invoke-virtual {p0}, Ldxa;->notifyDataSetChanged()V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    if-nez p2, :cond_0

    iget-object v0, p0, Ldxa;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Ldxb;

    invoke-direct {v0, p0, p2}, Ldxb;-><init>(Ldxa;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxb;

    iget-object v1, v0, Ldxb;->c:Ldxa;

    iget v1, v1, Ldxa;->e:I

    if-lez v1, :cond_1

    iget-object v1, v0, Ldxb;->c:Ldxa;

    iget v1, v1, Ldxa;->e:I

    :goto_0
    iget-object v2, v0, Ldxb;->c:Ldxa;

    iget v2, v2, Ldxa;->f:I

    if-lez v2, :cond_2

    iget-object v2, v0, Ldxb;->c:Ldxa;

    iget v2, v2, Ldxa;->f:I

    :goto_1
    iget-object v3, v0, Ldxb;->a:Landroid/view/View;

    iget-object v4, v0, Ldxb;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    iget-object v5, v0, Ldxb;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    invoke-virtual {v3, v4, v1, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    iget-object v1, v0, Ldxb;->b:Landroid/widget/TextView;

    iget-object v0, v0, Ldxb;->c:Ldxa;

    iget-object v0, v0, Ldxa;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    iget-object v1, v0, Ldxb;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    goto :goto_0

    :cond_2
    iget-object v2, v0, Ldxb;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    goto :goto_1
.end method
