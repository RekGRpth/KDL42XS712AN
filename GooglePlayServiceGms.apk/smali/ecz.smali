.class public abstract Lecz;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method protected static d(I)I
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/16 v0, 0x2712

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x4e20

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x2716

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x2714

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x9 -> :sswitch_2
        0x3eb -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final E_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;

    iput-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {p0}, Lecz;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {p0}, Lecz;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d(I)V

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e()Lduk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lecz;->b(Lduk;)V

    :cond_0
    return-void
.end method

.method protected final J()Z
    .locals 2

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {p0}, Lecz;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    return v0
.end method

.method public abstract a()I
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lecz;->c:Z

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not supported for sign-in fragments; use startActivityForResult."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    iget-boolean v0, p0, Lecz;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lecz;->c:Z

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempting to launch more than one activity."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract a(Lduk;)V
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "activity_launched"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecz;->c:Z

    :cond_0
    return-void
.end method

.method public abstract b()I
.end method

.method public final b(Lduk;)V
    .locals 2

    iget-boolean v0, p0, Lecz;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lecz;->b:Z

    invoke-virtual {p0, p1}, Lecz;->a(Lduk;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempting to transition multiple times."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final c(I)V
    .locals 1

    invoke-virtual {p0}, Lecz;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lecz;->b:Z

    :cond_0
    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    return-void
.end method

.method protected final e(I)V
    .locals 1

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c(I)V

    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "activity_launched"

    iget-boolean v1, p0, Lecz;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f(I)V
    .locals 1

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d(I)V

    return-void
.end method
