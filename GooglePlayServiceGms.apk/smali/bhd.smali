.class public final Lbhd;
.super Lbgo;
.source "SourceFile"


# instance fields
.field private final b:Lbgo;

.field private final c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lbgo;Ljava/util/Comparator;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgo;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p1, p0, Lbhd;->b:Lbgo;

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lbhd;->c:[Ljava/lang/Integer;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbhd;->c:[Ljava/lang/Integer;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lbhd;->c:[Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbhd;->c:[Ljava/lang/Integer;

    new-instance v1, Lbhe;

    invoke-direct {v1, p0, p2}, Lbhe;-><init>(Lbhd;Ljava/util/Comparator;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-void
.end method

.method static synthetic a(Lbhd;)Lbgo;
    .locals 1

    iget-object v0, p0, Lbhd;->b:Lbgo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lbhd;->c:[Ljava/lang/Integer;

    array-length v0, v0

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lbhd;->b:Lbgo;

    iget-object v1, p0, Lbhd;->c:[Ljava/lang/Integer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lbhd;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->b()V

    return-void
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lbhd;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    new-instance v0, Lbgp;

    invoke-direct {v0, p0}, Lbgp;-><init>(Lbgo;)V

    return-object v0
.end method
