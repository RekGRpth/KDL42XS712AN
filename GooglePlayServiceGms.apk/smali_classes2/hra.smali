.class Lhra;
.super Lhrv;
.source "SourceFile"


# instance fields
.field private volatile e:Z

.field private final f:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final g:Landroid/os/PowerManager;

.field private volatile h:Ljava/lang/String;

.field private final i:[B

.field private volatile j:Z

.field private volatile k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private volatile m:Lhpn;

.field private final n:Lilx;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;Ljava/lang/String;[BLhqq;Limb;Lhsd;Lilx;)V
    .locals 6

    const/16 v5, 0x2f

    const/4 v4, 0x0

    invoke-direct {p0, p4, p5, p6}, Lhrv;-><init>(Lhqq;Limb;Lhsd;)V

    iput-boolean v4, p0, Lhra;->e:Z

    new-instance v0, Lhrb;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v2, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v3, 0x32

    invoke-direct {v2, v3}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    new-instance v3, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;

    invoke-direct {v3}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Lhrb;-><init>(Lhra;Ljava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lhra;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    iput-boolean v4, p0, Lhra;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhra;->k:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhra;->l:Ljava/lang/Object;

    iput-object p1, p0, Lhra;->g:Landroid/os/PowerManager;

    iput-object p2, p0, Lhra;->h:Ljava/lang/String;

    iget-object v0, p0, Lhra;->h:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhra;->h:Ljava/lang/String;

    :cond_0
    iput-object p3, p0, Lhra;->i:[B

    iput-object p7, p0, Lhra;->n:Lilx;

    return-void
.end method

.method static synthetic a(Lhra;Livi;)Lhrw;
    .locals 1

    invoke-direct {p0, p1}, Lhra;->c(Livi;)Lhrw;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;[B)Lhrw;
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Lhra;->b()Lhrw;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/FileOutputStream;->write([B)V

    new-instance v0, Lhrw;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    const-string v2, "Failed to save data: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lhrw;

    const/4 v4, 0x0

    invoke-direct {v0, v4, v3, v2}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lhra;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhra;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b()Lhrw;
    .locals 9

    const/4 v0, 0x0

    iget-object v2, p0, Lhra;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lhra;->j:Z

    if-eqz v1, :cond_0

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lhra;->b(Ljava/lang/String;)Lhrw;

    move-result-object v1

    if-eqz v1, :cond_1

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_1
    const-string v1, "%d-%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    const-wide v7, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhra;->h:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lhra;->b(Ljava/lang/String;)Lhrw;

    move-result-object v1

    if-eqz v1, :cond_2

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhra;->j:Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private b(Ljava/lang/String;)Lhrw;
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhra;->b:Limb;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mkdir: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v0

    :goto_0
    if-nez v0, :cond_2

    const-string v0, "Failed to create dir: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhra;->b:Limb;

    invoke-virtual {v0, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lhrw;

    invoke-direct {v0, v6, v1, v2}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method static synthetic b(Lhra;)Z
    .locals 1

    iget-boolean v0, p0, Lhra;->j:Z

    return v0
.end method

.method private b(Livi;)Z
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x6

    invoke-virtual {p1, v4}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v4}, Livi;->f(I)Livi;

    move-result-object v0

    invoke-virtual {v0, v5}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No sequence number specified!"

    invoke-static {v0, v3}, Lhsn;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1, v4}, Livi;->f(I)Livi;

    move-result-object v0

    invoke-virtual {v0, v5}, Livi;->c(I)I

    move-result v0

    :try_start_0
    iget-object v3, p0, Lhra;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v4, Lhrc;

    invoke-direct {v4, p0, v0, p1}, Lhrc;-><init>(Lhra;ILivi;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Failed to write to file: work queue full."

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhra;->b:Limb;

    invoke-virtual {v3, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lhra;->a:Lhqq;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, v1}, Lhqq;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_1
.end method

.method static synthetic c(Lhra;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lhra;->g:Landroid/os/PowerManager;

    return-object v0
.end method

.method private c(Livi;)Lhrw;
    .locals 5

    invoke-direct {p0}, Lhra;->b()Lhrw;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhra;->m:Lhpn;

    if-nez v0, :cond_1

    iget-object v0, p0, Lhra;->i:[B

    if-nez v0, :cond_0

    new-instance v0, Lhrw;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "Encryption Key invalid."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhpn;

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    iget-object v2, p0, Lhra;->i:[B

    iget-object v3, p0, Lhra;->b:Limb;

    invoke-static {v2, v3}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v2

    iget-object v3, p0, Lhra;->b:Limb;

    invoke-direct {v0, v1, v2, v3}, Lhpn;-><init>(Ljava/lang/String;Lhpk;Limb;)V

    iput-object v0, p0, Lhra;->m:Lhpn;

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lhra;->m:Lhpn;

    invoke-virtual {v0, p1}, Lhpn;->a(Livi;)Lhrw;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lhra;)Lilx;
    .locals 1

    iget-object v0, p0, Lhra;->n:Lilx;

    return-object v0
.end method

.method static synthetic e(Lhra;)Z
    .locals 4

    iget-boolean v0, p0, Lhra;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lhra;->b()Lhrw;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v0

    iget-object v1, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhru;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhra;->e:Z

    if-nez v0, :cond_0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhra;->b:Limb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to lock dir "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lhrw;
    .locals 4

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    const-string v1, "sessionId in two writes should be consistent."

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Lhrw;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v0, "sessionId"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lhra;->a(Ljava/lang/String;[B)Lhrw;

    move-result-object v0

    iget-boolean v1, v0, Lhrw;->a:Z

    if-eqz v1, :cond_1

    iput-object p1, p0, Lhra;->k:Ljava/lang/String;

    new-instance v0, Lhrw;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhrw;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a()V
    .locals 2

    iget-object v0, p0, Lhra;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhra;->b:Limb;

    const-string v1, "LocalScanResultWriter.workerThread is shutting down."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method final a(Lhsd;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lhra;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lhsd;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhra;->b:Limb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create sessionSummary "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final declared-synchronized a(Livi;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhra;->c:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhra;->b:Limb;

    const-string v1, "Writer closed, no data can be appended."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lhra;->b(Livi;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Livi;Livi;)Z
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Livi;->b(ILivi;)Livi;

    invoke-direct {p0, p1}, Lhra;->b(Livi;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lhra;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhra;->b:Limb;

    const-string v1, "Might leak LocalScanResultWriter.workerThread."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
