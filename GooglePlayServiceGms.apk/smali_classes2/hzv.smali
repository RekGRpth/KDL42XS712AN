.class public final Lhzv;
.super Lhyp;
.source "SourceFile"


# instance fields
.field final a:I

.field public final b:Leqr;

.field final c:Ljava/lang/String;

.field final e:Landroid/app/PendingIntent;

.field final f:[Ljava/lang/String;


# direct methods
.method public constructor <init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Leqr;)V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhyp;-><init>(I)V

    iput p1, p0, Lhzv;->a:I

    iput-object p4, p0, Lhzv;->c:Ljava/lang/String;

    iput-object p2, p0, Lhzv;->e:Landroid/app/PendingIntent;

    iput-object p3, p0, Lhzv;->f:[Ljava/lang/String;

    iput-object p5, p0, Lhzv;->b:Leqr;

    return-void
.end method

.method public static a(Ljava/lang/String;Leqr;)Lhzv;
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lhzv;

    const/4 v1, 0x1

    move-object v3, v2

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhzv;-><init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Leqr;)V

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 3

    invoke-virtual {p0}, Lhzv;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lhzv;->b:Leqr;

    if-eqz v1, :cond_0

    :try_start_0
    iget v1, p0, Lhzv;->a:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Remove action not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lhzv;->b:Leqr;

    iget-object v2, p0, Lhzv;->e:Landroid/app/PendingIntent;

    invoke-interface {v1, v0, v2}, Leqr;->a(ILandroid/app/PendingIntent;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lhzv;->b:Leqr;

    iget-object v2, p0, Lhzv;->f:[Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Leqr;->b(I[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RemoveGeofenceRequest [mRemoveAction="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhzv;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhzv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPendingIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhzv;->e:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRequestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhzv;->f:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
