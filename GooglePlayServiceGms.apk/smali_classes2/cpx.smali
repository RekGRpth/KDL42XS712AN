.class public final Lcpx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[I

.field final b:[B

.field final c:[B


# direct methods
.method public constructor <init>([B[B)V
    .locals 2

    const/16 v1, 0x10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lcpx;->a:[I

    array-length v0, p1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    array-length v0, p2

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iput-object p1, p0, Lcpx;->b:[B

    iput-object p2, p0, Lcpx;->c:[B

    return-void
.end method

.method private static a(II)I
    .locals 2

    shl-int v0, p0, p1

    rsub-int/lit8 v1, p1, 0x20

    ushr-int v1, p0, v1

    or-int/2addr v0, v1

    return v0
.end method

.method static a([BI)I
    .locals 2

    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x0

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a([BI[BII)V
    .locals 8

    const/16 v0, 0x40

    new-array v1, v0, [B

    if-nez p5, :cond_2

    :cond_0
    return-void

    :cond_1
    add-int/lit8 p5, p5, -0x40

    add-int/lit8 p4, p4, 0x40

    add-int/lit8 p2, p2, 0x40

    :cond_2
    iget-object v2, p0, Lcpx;->a:[I

    const/16 v0, 0x10

    new-array v3, v0, [I

    const/4 v0, 0x0

    :goto_0
    const/16 v4, 0x10

    if-ge v0, v4, :cond_3

    aget v4, v2, v0

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/16 v0, 0x14

    :goto_1
    if-lez v0, :cond_4

    const/4 v4, 0x4

    const/4 v5, 0x4

    aget v5, v3, v5

    const/4 v6, 0x0

    aget v6, v3, v6

    const/16 v7, 0xc

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0x8

    const/16 v5, 0x8

    aget v5, v3, v5

    const/4 v6, 0x4

    aget v6, v3, v6

    const/4 v7, 0x0

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xc

    const/16 v5, 0xc

    aget v5, v3, v5

    const/16 v6, 0x8

    aget v6, v3, v6

    const/4 v7, 0x4

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget v5, v3, v5

    const/16 v6, 0xc

    aget v6, v3, v6

    const/16 v7, 0x8

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0x9

    const/16 v5, 0x9

    aget v5, v3, v5

    const/4 v6, 0x5

    aget v6, v3, v6

    const/4 v7, 0x1

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xd

    const/16 v5, 0xd

    aget v5, v3, v5

    const/16 v6, 0x9

    aget v6, v3, v6

    const/4 v7, 0x5

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget v5, v3, v5

    const/16 v6, 0xd

    aget v6, v3, v6

    const/16 v7, 0x9

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x5

    const/4 v5, 0x5

    aget v5, v3, v5

    const/4 v6, 0x1

    aget v6, v3, v6

    const/16 v7, 0xd

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xe

    const/16 v5, 0xe

    aget v5, v3, v5

    const/16 v6, 0xa

    aget v6, v3, v6

    const/4 v7, 0x6

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    aget v5, v3, v5

    const/16 v6, 0xe

    aget v6, v3, v6

    const/16 v7, 0xa

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x6

    const/4 v5, 0x6

    aget v5, v3, v5

    const/4 v6, 0x2

    aget v6, v3, v6

    const/16 v7, 0xe

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xa

    const/16 v5, 0xa

    aget v5, v3, v5

    const/4 v6, 0x6

    aget v6, v3, v6

    const/4 v7, 0x2

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x3

    aget v5, v3, v5

    const/16 v6, 0xf

    aget v6, v3, v6

    const/16 v7, 0xb

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x7

    const/4 v5, 0x7

    aget v5, v3, v5

    const/4 v6, 0x3

    aget v6, v3, v6

    const/16 v7, 0xf

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xb

    const/16 v5, 0xb

    aget v5, v3, v5

    const/4 v6, 0x7

    aget v6, v3, v6

    const/4 v7, 0x3

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xf

    const/16 v5, 0xf

    aget v5, v3, v5

    const/16 v6, 0xb

    aget v6, v3, v6

    const/4 v7, 0x7

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget v5, v3, v5

    const/4 v6, 0x0

    aget v6, v3, v6

    const/4 v7, 0x3

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    aget v5, v3, v5

    const/4 v6, 0x1

    aget v6, v3, v6

    const/4 v7, 0x0

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x3

    aget v5, v3, v5

    const/4 v6, 0x2

    aget v6, v3, v6

    const/4 v7, 0x1

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget v5, v3, v5

    const/4 v6, 0x3

    aget v6, v3, v6

    const/4 v7, 0x2

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x6

    const/4 v5, 0x6

    aget v5, v3, v5

    const/4 v6, 0x5

    aget v6, v3, v6

    const/4 v7, 0x4

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x7

    const/4 v5, 0x7

    aget v5, v3, v5

    const/4 v6, 0x6

    aget v6, v3, v6

    const/4 v7, 0x5

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x4

    const/4 v5, 0x4

    aget v5, v3, v5

    const/4 v6, 0x7

    aget v6, v3, v6

    const/4 v7, 0x6

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x5

    const/4 v5, 0x5

    aget v5, v3, v5

    const/4 v6, 0x4

    aget v6, v3, v6

    const/4 v7, 0x7

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xb

    const/16 v5, 0xb

    aget v5, v3, v5

    const/16 v6, 0xa

    aget v6, v3, v6

    const/16 v7, 0x9

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0x8

    const/16 v5, 0x8

    aget v5, v3, v5

    const/16 v6, 0xb

    aget v6, v3, v6

    const/16 v7, 0xa

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0x9

    const/16 v5, 0x9

    aget v5, v3, v5

    const/16 v6, 0x8

    aget v6, v3, v6

    const/16 v7, 0xb

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xa

    const/16 v5, 0xa

    aget v5, v3, v5

    const/16 v6, 0x9

    aget v6, v3, v6

    const/16 v7, 0x8

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xc

    const/16 v5, 0xc

    aget v5, v3, v5

    const/16 v6, 0xf

    aget v6, v3, v6

    const/16 v7, 0xe

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xd

    const/16 v5, 0xd

    aget v5, v3, v5

    const/16 v6, 0xc

    aget v6, v3, v6

    const/16 v7, 0xf

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x9

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xe

    const/16 v5, 0xe

    aget v5, v3, v5

    const/16 v6, 0xd

    aget v6, v3, v6

    const/16 v7, 0xc

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0xd

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    const/16 v4, 0xf

    const/16 v5, 0xf

    aget v5, v3, v5

    const/16 v6, 0xe

    aget v6, v3, v6

    const/16 v7, 0xd

    aget v7, v3, v7

    add-int/2addr v6, v7

    const/16 v7, 0x12

    invoke-static {v6, v7}, Lcpx;->a(II)I

    move-result v6

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v0, v0, -0x2

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_2
    const/16 v4, 0x10

    if-ge v0, v4, :cond_5

    aget v4, v3, v0

    aget v5, v2, v0

    add-int/2addr v4, v5

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_3
    const/16 v2, 0x10

    if-ge v0, v2, :cond_6

    mul-int/lit8 v2, v0, 0x4

    aget v4, v3, v0

    add-int/lit8 v5, v2, 0x0

    ushr-int/lit8 v6, v4, 0x0

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    add-int/lit8 v5, v2, 0x1

    ushr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    add-int/lit8 v5, v2, 0x2

    ushr-int/lit8 v6, v4, 0x10

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    add-int/lit8 v2, v2, 0x3

    ushr-int/lit8 v4, v4, 0x18

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcpx;->a:[I

    const/16 v2, 0x8

    iget-object v3, p0, Lcpx;->a:[I

    const/16 v4, 0x8

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0x1

    aput v3, v0, v2

    iget-object v0, p0, Lcpx;->a:[I

    const/16 v2, 0x8

    aget v0, v0, v2

    if-nez v0, :cond_7

    iget-object v0, p0, Lcpx;->a:[I

    const/16 v2, 0x9

    iget-object v3, p0, Lcpx;->a:[I

    const/16 v4, 0x9

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0x1

    aput v3, v0, v2

    :cond_7
    const/16 v0, 0x40

    if-gt p5, v0, :cond_8

    const/4 v0, 0x0

    :goto_4
    if-ge v0, p5, :cond_0

    add-int v2, v0, p4

    add-int v3, v0, p2

    aget-byte v3, p1, v3

    aget-byte v4, v1, v0

    xor-int/2addr v3, v4

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, p3, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    :goto_5
    const/16 v2, 0x40

    if-ge v0, v2, :cond_1

    add-int v2, v0, p4

    add-int v3, v0, p2

    aget-byte v3, p1, v3

    aget-byte v4, v1, v0

    xor-int/2addr v3, v4

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, p3, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method
