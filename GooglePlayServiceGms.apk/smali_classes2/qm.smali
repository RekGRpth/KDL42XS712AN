.class public final Lqm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lqm;->a:Ljava/util/Map;

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    invoke-virtual {v0}, Lqw;->f()Lqw;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:contributor"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:language"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:publisher"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:relation"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:subject"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:type"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    invoke-virtual {v0}, Lqw;->f()Lqw;

    invoke-virtual {v0}, Lqw;->h()Lqw;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:creator"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:date"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    invoke-virtual {v0}, Lqw;->f()Lqw;

    invoke-virtual {v0}, Lqw;->h()Lqw;

    invoke-virtual {v0}, Lqw;->j()Lqw;

    invoke-virtual {v0}, Lqw;->l()Lqw;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:description"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:rights"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lqm;->a:Ljava/util/Map;

    const-string v2, "dc:title"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static a(Lqh;Lqv;)Lps;
    .locals 13

    invoke-virtual {p0}, Lqh;->a()Lqj;

    move-result-object v7

    invoke-virtual {p0}, Lqh;->a()Lqj;

    move-result-object v1

    const-string v2, "http://purl.org/dc/elements/1.1/"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lql;->a(Lqj;Ljava/lang/String;Z)Lqj;

    invoke-virtual {p0}, Lqh;->a()Lqj;

    move-result-object v1

    invoke-virtual {v1}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqj;

    const-string v2, "http://purl.org/dc/elements/1.1/"

    invoke-virtual {v1}, Lqj;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move v3, v2

    :goto_1
    invoke-virtual {v1}, Lqj;->c()I

    move-result v2

    if-gt v3, v2, :cond_0

    invoke-virtual {v1, v3}, Lqj;->a(I)Lqj;

    move-result-object v5

    sget-object v2, Lqm;->a:Ljava/util/Map;

    invoke-virtual {v5}, Lqj;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqw;

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v4

    iget v4, v4, Lqu;->a:I

    and-int/lit16 v4, v4, 0x300

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_3

    new-instance v4, Lqj;

    invoke-virtual {v5}, Lqj;->j()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6, v2}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    const-string v6, "[]"

    invoke-virtual {v5, v6}, Lqj;->c(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lqj;->a(Lqj;)V

    invoke-virtual {v1, v3, v4}, Lqj;->a(ILqj;)V

    invoke-virtual {v2}, Lqw;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v2

    invoke-virtual {v2}, Lqw;->b()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lqj;

    const-string v4, "xml:lang"

    const-string v6, "x-default"

    const/4 v9, 0x0

    invoke-direct {v2, v4, v6, v9}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    invoke-virtual {v5, v2}, Lqj;->d(Lqj;)V

    :cond_1
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v4

    const/16 v6, 0x1e00

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v9}, Lqw;->a(IZ)V

    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v4

    invoke-virtual {v4, v2}, Lqw;->a(Lqw;)V

    invoke-virtual {v2}, Lqw;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v5}, Lqm;->a(Lqj;)V

    goto :goto_3

    :cond_4
    const-string v2, "http://ns.adobe.com/exif/1.0/"

    invoke-virtual {v1}, Lqj;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "exif:GPSTimeStamp"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v3

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v3}, Lqj;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lpv;->e(Ljava/lang/String;)Lpq;

    move-result-object v4

    invoke-interface {v4}, Lpq;->a()I

    move-result v2

    if-nez v2, :cond_5

    invoke-interface {v4}, Lpq;->b()I

    move-result v2

    if-nez v2, :cond_5

    invoke-interface {v4}, Lpq;->c()I
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_4
    const-string v2, "exif:UserComment"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lqm;->a(Lqj;)V

    goto/16 :goto_0

    :cond_6
    :try_start_1
    const-string v2, "exif:DateTimeOriginal"

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v2

    if-nez v2, :cond_7

    const-string v2, "exif:DateTimeDigitized"

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v2

    :cond_7
    invoke-virtual {v2}, Lqj;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lpv;->e(Ljava/lang/String;)Lpq;

    move-result-object v2

    invoke-interface {v4}, Lpq;->i()Ljava/util/Calendar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v2}, Lpq;->a()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/4 v5, 0x2

    invoke-interface {v2}, Lpq;->b()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/4 v5, 0x5

    invoke-interface {v2}, Lpq;->c()I

    move-result v2

    invoke-virtual {v4, v5, v2}, Ljava/util/Calendar;->set(II)V

    new-instance v2, Lqg;

    invoke-direct {v2, v4}, Lqg;-><init>(Ljava/util/Calendar;)V

    invoke-static {v2}, Lqa;->a(Lpq;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lqj;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Lpr; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v2

    goto :goto_4

    :cond_8
    const-string v2, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    invoke-virtual {v1}, Lqj;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "xmpDM:copyright"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v9

    if-eqz v9, :cond_0

    :try_start_2
    move-object v0, p0

    check-cast v0, Lqh;

    move-object v1, v0

    invoke-virtual {v1}, Lqh;->a()Lqj;

    move-result-object v1

    const-string v2, "http://purl.org/dc/elements/1.1/"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lql;->a(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v1

    invoke-virtual {v9}, Lqj;->k()Ljava/lang/String;

    move-result-object v10

    const-string v11, "\n\n"

    const-string v2, "dc:rights"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v12

    if-eqz v12, :cond_9

    invoke-virtual {v12}, Lqj;->f()Z

    move-result v1

    if-nez v1, :cond_b

    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v2, "http://purl.org/dc/elements/1.1/"

    const-string v3, "rights"

    const-string v4, ""

    const-string v5, "x-default"

    move-object v1, p0

    invoke-interface/range {v1 .. v6}, Lps;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_5
    invoke-virtual {v9}, Lqj;->a()Lqj;

    move-result-object v1

    invoke-virtual {v1, v9}, Lqj;->c(Lqj;)V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    goto/16 :goto_0

    :cond_b
    const-string v1, "x-default"

    invoke-static {v12, v1}, Lql;->a(Lqj;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_c

    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Lqj;->a(I)Lqj;

    move-result-object v1

    invoke-virtual {v1}, Lqj;->k()Ljava/lang/String;

    move-result-object v6

    const-string v2, "http://purl.org/dc/elements/1.1/"

    const-string v3, "rights"

    const-string v4, ""

    const-string v5, "x-default"

    move-object v1, p0

    invoke-interface/range {v1 .. v6}, Lps;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x-default"

    invoke-static {v12, v1}, Lql;->a(Lqj;Ljava/lang/String;)I

    move-result v1

    :cond_c
    invoke-virtual {v12, v1}, Lqj;->a(I)Lqj;

    move-result-object v1

    invoke-virtual {v1}, Lqj;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_d

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lqj;->d(Ljava/lang/String;)V

    goto :goto_5

    :cond_d
    add-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lqj;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Lpr; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_5

    :cond_e
    const-string v2, "http://ns.adobe.com/xap/1.0/rights/"

    invoke-virtual {v1}, Lqj;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "xmpRights:UsageTerms"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lqm;->a(Lqj;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v7}, Lqj;->n()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lqj;->b(Z)V

    invoke-virtual {p1}, Lqv;->b()Z

    move-result v4

    invoke-virtual {v7}, Lqj;->q()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_10
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqj;

    invoke-virtual {v1}, Lqj;->n()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v1}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v6

    :cond_11
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqj;

    invoke-virtual {v2}, Lqj;->o()Z

    move-result v3

    if-eqz v3, :cond_11

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lqj;->c(Z)V

    invoke-static {}, Lpt;->a()Lpu;

    move-result-object v3

    invoke-virtual {v2}, Lqj;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8}, Lpu;->c(Ljava/lang/String;)Lqx;

    move-result-object v8

    if-eqz v8, :cond_11

    invoke-interface {v8}, Lqx;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-static {v7, v3, v9, v10}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;Z)Lqj;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lqj;->a(Z)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8}, Lqx;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Lqx;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v3, v9, v10}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v9

    if-nez v9, :cond_14

    invoke-interface {v8}, Lqx;->d()Lqt;

    move-result-object v9

    invoke-virtual {v9}, Lqt;->a()Z

    move-result v9

    if-eqz v9, :cond_13

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8}, Lqx;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Lqx;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lqj;->c(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lqj;->a(Lqj;)V

    :cond_12
    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_7

    :cond_13
    new-instance v9, Lqj;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8}, Lqx;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v8}, Lqx;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8}, Lqx;->d()Lqt;

    move-result-object v8

    invoke-virtual {v8}, Lqt;->d()Lqw;

    move-result-object v8

    invoke-direct {v9, v10, v8}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    invoke-virtual {v3, v9}, Lqj;->a(Lqj;)V

    invoke-static {v6, v2, v9}, Lqm;->a(Ljava/util/Iterator;Lqj;Lqj;)V

    goto/16 :goto_7

    :cond_14
    invoke-interface {v8}, Lqx;->d()Lqt;

    move-result-object v3

    invoke-virtual {v3}, Lqt;->a()Z

    move-result v3

    if-eqz v3, :cond_16

    if-eqz v4, :cond_15

    const/4 v3, 0x1

    invoke-static {v2, v9, v3}, Lqm;->a(Lqj;Lqj;Z)V

    :cond_15
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_7

    :cond_16
    const/4 v3, 0x0

    invoke-interface {v8}, Lqx;->d()Lqt;

    move-result-object v8

    invoke-virtual {v8}, Lqt;->c()Z

    move-result v8

    if-eqz v8, :cond_18

    const-string v8, "x-default"

    invoke-static {v9, v8}, Lql;->a(Lqj;Ljava/lang/String;)I

    move-result v8

    const/4 v10, -0x1

    if-eq v8, v10, :cond_17

    invoke-virtual {v9, v8}, Lqj;->a(I)Lqj;

    move-result-object v3

    :cond_17
    :goto_9
    if-nez v3, :cond_19

    invoke-static {v6, v2, v9}, Lqm;->a(Ljava/util/Iterator;Lqj;Lqj;)V

    goto/16 :goto_7

    :cond_18
    invoke-virtual {v9}, Lqj;->f()Z

    move-result v8

    if-eqz v8, :cond_17

    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Lqj;->a(I)Lqj;

    move-result-object v3

    goto :goto_9

    :cond_19
    if-eqz v4, :cond_12

    const/4 v8, 0x1

    invoke-static {v2, v3, v8}, Lqm;->a(Lqj;Lqj;Z)V

    goto :goto_8

    :cond_1a
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lqj;->b(Z)V

    goto/16 :goto_6

    :cond_1b
    invoke-virtual {v7}, Lqj;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1d

    invoke-virtual {v7}, Lqj;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x24

    if-lt v1, v2, :cond_1d

    invoke-virtual {v7}, Lqj;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "uuid:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_1c
    invoke-static {v1}, Lqf;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v2, "http://ns.adobe.com/xap/1.0/mm/"

    const-string v3, "InstanceID"

    invoke-static {v2, v3}, Lqr;->a(Ljava/lang/String;Ljava/lang/String;)Lqq;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v7, v2, v3, v4}, Lql;->a(Lqj;Lqq;ZLqw;)Lqj;

    move-result-object v2

    if-eqz v2, :cond_1e

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lqj;->a(Lqw;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uuid:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lqj;->d(Ljava/lang/String;)V

    invoke-virtual {v2}, Lqj;->b()V

    invoke-virtual {v2}, Lqj;->e()V

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lqj;->c(Ljava/lang/String;)V

    :cond_1d
    invoke-static {v7}, Lqm;->b(Lqj;)V

    return-object p0

    :cond_1e
    new-instance v1, Lpr;

    const-string v2, "Failure creating xmpMM:InstanceID"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v1
.end method

.method private static a(Ljava/util/Iterator;Lqj;Lqj;)V
    .locals 4

    invoke-virtual {p2}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Alias to x-default already has a language qualifier"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    new-instance v0, Lqj;

    const-string v1, "xml:lang"

    const-string v2, "x-default"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    invoke-virtual {p1, v0}, Lqj;->d(Lqj;)V

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    const-string v0, "[]"

    invoke-virtual {p1, v0}, Lqj;->c(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lqj;->a(Lqj;)V

    return-void
.end method

.method private static a(Lqj;)V
    .locals 6

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->h()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->j()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->l()Lqw;

    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->l()Lqw;

    move-result-object v2

    invoke-virtual {v2}, Lqw;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lqj;->l()Lqw;

    move-result-object v2

    invoke-virtual {v2}, Lqw;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lqj;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_5
    new-instance v2, Lqj;

    const-string v3, "xml:lang"

    const-string v4, "x-repair"

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    invoke-virtual {v0, v2}, Lqj;->d(Lqj;)V

    goto :goto_0
.end method

.method private static a(Lqj;Lqj;Z)V
    .locals 5

    const/16 v2, 0xcb

    const/4 v4, 0x0

    invoke-virtual {p0}, Lqj;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lqj;->c()I

    move-result v0

    invoke-virtual {p1}, Lqj;->c()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Lpr;

    const-string v1, "Mismatch between alias and base nodes"

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    if-nez p2, :cond_3

    invoke-virtual {p0}, Lqj;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lqj;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {p1}, Lqj;->l()Lqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqj;->d()I

    move-result v0

    invoke-virtual {p1}, Lqj;->d()I

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_2
    new-instance v0, Lpr;

    const-string v1, "Mismatch between alias and base nodes"

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_3
    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {p1}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqj;

    invoke-static {v0, v1, v4}, Lqm;->a(Lqj;Lqj;Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lqj;->i()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {p1}, Lqj;->i()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqj;

    invoke-static {v0, v1, v4}, Lqm;->a(Lqj;Lqj;Z)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method private static b(Lqj;)V
    .locals 2

    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method
