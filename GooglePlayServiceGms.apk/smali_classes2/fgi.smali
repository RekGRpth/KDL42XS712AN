.class final Lfgi;
.super Lfgq;
.source "SourceFile"


# instance fields
.field final synthetic c:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Landroid/os/Bundle;)V
    .locals 6

    iput-object p5, p0, Lfgi;->c:Landroid/os/Bundle;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfgq;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final c()Landroid/util/Pair;
    .locals 8

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    iget-object v2, p0, Lfgi;->a:Landroid/content/Context;

    iget-object v3, p0, Lfgi;->c:Landroid/os/Bundle;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "internal_call_method"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "GET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v3, "internal_call_result"

    invoke-static {v2}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->a()Lfbe;

    move-result-object v2

    invoke-virtual {v2}, Lfbe;->c()I

    move-result v2

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    invoke-direct {v0, v1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    const-string v6, "SET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v2}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->a()Lfbe;

    move-result-object v2

    const-string v5, "internal_call_arg_1"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v5, 0x0

    invoke-static {v5}, Lbkm;->c(Ljava/lang/String;)V

    const-string v5, "PeoplePreferences"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "setShowSyncErrorNotification: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v2, Lfbe;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "show_sync_error_notification"

    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
