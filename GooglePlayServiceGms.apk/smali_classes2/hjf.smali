.class public final Lhjf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lidz;


# instance fields
.field final a:Ljava/lang/Object;

.field public final b:Lidx;

.field c:Livi;


# direct methods
.method private constructor <init>(Lidq;Ljava/io/File;Ljavax/crypto/SecretKey;[B)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhjf;->a:Ljava/lang/Object;

    new-instance v0, Livi;

    sget-object v1, Lihj;->aU:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iput-object v0, p0, Lhjf;->c:Livi;

    new-instance v0, Lidx;

    const/4 v1, 0x1

    const/4 v3, 0x2

    sget-object v5, Lihj;->aU:Livk;

    move-object v2, p3

    move-object v4, p4

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    iput-object v0, p0, Lhjf;->b:Lidx;

    return-void
.end method

.method public constructor <init>(Lidq;Ljavax/crypto/SecretKey;[B)V
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lidq;->e()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_clts"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, p2, p3}, Lhjf;-><init>(Lidq;Ljava/io/File;Ljavax/crypto/SecretKey;[B)V

    return-void
.end method

.method private static a(Livi;I)Ljava/util/Date;
    .locals 3

    invoke-virtual {p0, p1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-virtual {p0, p1}, Livi;->d(I)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()Ljava/util/List;
    .locals 5

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Livi;->k(I)I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Livi;->c(II)Livi;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(J)Livi;
    .locals 10

    iget-object v2, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v0, Livi;

    sget-object v1, Lihj;->X:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Livi;->k(I)I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Livi;->c(II)Livi;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Livi;->d(I)J

    move-result-wide v4

    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Livi;->c(I)I

    move-result v6

    cmp-long v7, v4, p1

    if-ltz v7, :cond_0

    new-instance v7, Livi;

    sget-object v8, Lihj;->W:Livk;

    invoke-direct {v7, v8}, Livi;-><init>(Livk;)V

    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-static {v3, v8, v9}, Livm;->a(Livi;II)I

    move-result v3

    const/4 v8, 0x2

    invoke-virtual {v7, v8, v3}, Livi;->e(II)Livi;

    const/4 v3, 0x1

    invoke-virtual {v7, v3, v6}, Livi;->e(II)Livi;

    invoke-static {v4, v5}, Lilv;->a(J)J

    move-result-wide v3

    const/4 v5, 0x3

    long-to-int v3, v3

    invoke-virtual {v7, v5, v3}, Livi;->e(II)Livi;

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v7}, Livi;->a(ILivi;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lhjf;->b:Lidx;

    iget-object v2, p0, Lhjf;->c:Livi;

    invoke-virtual {v0, v2}, Lidx;->a(Livi;)[B

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lhjf;->b:Lidx;

    invoke-virtual {v1, v0}, Lidx;->a([B)V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CollectorState"

    const-string v1, "Unable to save data to disk."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(JLjava/util/Map;)V
    .locals 9

    iget-object v2, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1, p2}, Livi;->a(IJ)Livi;

    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lilv;->a(Livi;I)V

    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v4, Livi;

    sget-object v1, Lihj;->aT:Livk;

    invoke-direct {v4, v1}, Livi;-><init>(Livk;)V

    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    new-instance v5, Livi;

    sget-object v6, Lihj;->aS:Livk;

    invoke-direct {v5, v6}, Livi;-><init>(Livk;)V

    const/4 v6, 0x1

    iget-wide v7, v0, Lilt;->a:J

    long-to-int v7, v7

    invoke-virtual {v5, v6, v7}, Livi;->e(II)Livi;

    const/4 v6, 0x2

    iget-wide v7, v0, Lilt;->b:J

    long-to-int v0, v7

    invoke-virtual {v5, v6, v0}, Livi;->e(II)Livi;

    const/4 v0, 0x2

    invoke-virtual {v4, v0, v5}, Livi;->a(ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, Livi;->a(ILivi;)V

    goto :goto_0

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/util/Calendar;)V
    .locals 5

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x6

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Livi;->a(IJ)Livi;

    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Livi;->e(II)Livi;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Livi;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b()Livi;
    .locals 4

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Livi;->k(I)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v2, p0, Lhjf;->c:Livi;

    const/4 v3, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v3, v0}, Livi;->c(II)Livi;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()J
    .locals 3

    iget-object v2, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v0

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final d()I
    .locals 3

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Livi;->c(I)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()I
    .locals 3

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Livi;->c(I)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Z
    .locals 3

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Livi;->k(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Z
    .locals 3

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Livi;->k(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final h()V
    .locals 5

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lhjf;->n()Ljava/util/List;

    move-result-object v0

    new-instance v2, Lhjg;

    invoke-direct {v2, p0}, Lhjg;-><init>(Lhjf;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x14

    if-le v2, v3, :cond_0

    add-int/lit8 v3, v2, -0x14

    invoke-interface {v0, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lhjf;->c:Livi;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lilv;->a(Livi;I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Livi;->a(ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final i()J
    .locals 3

    iget-object v2, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v0

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final j()V
    .locals 3

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v2, 0x8

    invoke-static {v0, v2}, Lilv;->a(Livi;I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()Ljava/util/Date;
    .locals 6

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    invoke-static {v0}, Livm;->b(Livi;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final l()I
    .locals 2

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    invoke-static {v0}, Livm;->a(Livi;)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final m()Ljava/util/Map;
    .locals 12

    const/4 v2, 0x0

    iget-object v4, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lhjf;->c:Livi;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v4

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move v3, v2

    :goto_1
    iget-object v1, p0, Lhjf;->c:Livi;

    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Livi;->k(I)I

    move-result v1

    if-ge v3, v1, :cond_3

    iget-object v1, p0, Lhjf;->c:Livi;

    const/16 v5, 0xa

    invoke-virtual {v1, v5, v3}, Livi;->c(II)Livi;

    move-result-object v5

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Livi;->g(I)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    :goto_2
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Livi;->k(I)I

    move-result v7

    if-ge v1, v7, :cond_2

    const/4 v7, 0x2

    invoke-virtual {v5, v7, v1}, Livi;->c(II)Livi;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Livi;->c(I)I

    move-result v8

    int-to-long v8, v8

    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Livi;->c(I)I

    move-result v7

    int-to-long v10, v7

    cmp-long v7, v8, v10

    if-gez v7, :cond_1

    new-instance v7, Lilt;

    invoke-direct {v7, v8, v9, v10, v11}, Lilt;-><init>(JJ)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NextSensorCollectionTimeSinceEpoch: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lhjf;->c:Livi;

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lhjf;->a(Livi;I)Ljava/util/Date;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Livi;->k(I)I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lhjf;->c:Livi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Livi;->c(II)Livi;

    move-result-object v3

    const-string v4, ", ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "start: %s, end: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v3, v7}, Lhjf;->a(Livi;I)Ljava/util/Date;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x2

    invoke-static {v3, v7}, Lhjf;->a(Livi;I)Ljava/util/Date;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
