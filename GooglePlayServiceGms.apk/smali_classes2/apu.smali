.class public final enum Lapu;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Lapu;

.field public static final enum B:Lapu;

.field public static final enum C:Lapu;

.field private static final synthetic E:[Lapu;

.field public static final enum a:Lapu;

.field public static final enum b:Lapu;

.field public static final enum c:Lapu;

.field public static final enum d:Lapu;

.field public static final enum e:Lapu;

.field public static final enum f:Lapu;

.field public static final enum g:Lapu;

.field public static final enum h:Lapu;

.field public static final enum i:Lapu;

.field public static final enum j:Lapu;

.field public static final enum k:Lapu;

.field public static final enum l:Lapu;

.field public static final enum m:Lapu;

.field public static final enum n:Lapu;

.field public static final enum o:Lapu;

.field public static final enum p:Lapu;

.field public static final enum q:Lapu;

.field public static final enum r:Lapu;

.field public static final enum s:Lapu;

.field public static final enum t:Lapu;

.field public static final enum u:Lapu;

.field public static final enum v:Lapu;

.field public static final enum w:Lapu;

.field public static final enum x:Lapu;

.field public static final enum y:Lapu;

.field public static final enum z:Lapu;


# instance fields
.field private final D:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lapu;

    const-string v1, "AUTH"

    const-string v2, "Auth"

    invoke-direct {v0, v1, v4, v2}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->a:Lapu;

    new-instance v0, Lapu;

    const-string v1, "EMAIL"

    const-string v2, "Email"

    invoke-direct {v0, v1, v5, v2}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->b:Lapu;

    new-instance v0, Lapu;

    const-string v1, "STATUS"

    sget-object v2, Laso;->M:Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->c:Lapu;

    new-instance v0, Lapu;

    const-string v1, "JSON_STATUS"

    sget-object v2, Laso;->N:Ljava/lang/String;

    invoke-direct {v0, v1, v7, v2}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->d:Lapu;

    new-instance v0, Lapu;

    const-string v1, "CAPTCHA_TOKEN_RES"

    const-string v2, "CaptchaToken"

    invoke-direct {v0, v1, v8, v2}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->e:Lapu;

    new-instance v0, Lapu;

    const-string v1, "DETAIL"

    const/4 v2, 0x5

    const-string v3, "ErrorDetail"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->f:Lapu;

    new-instance v0, Lapu;

    const-string v1, "CAPTCHA_URL"

    const/4 v2, 0x6

    const-string v3, "CaptchaUrl"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->g:Lapu;

    new-instance v0, Lapu;

    const-string v1, "CAPTCHA_DATA"

    const/4 v2, 0x7

    const-string v3, "CaptchaData"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->h:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SERVICES"

    const/16 v2, 0x8

    const-string v3, "services"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->i:Lapu;

    new-instance v0, Lapu;

    const-string v1, "YOUTUBE_USER"

    const/16 v2, 0x9

    const-string v3, "YouTubeUser"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->j:Lapu;

    new-instance v0, Lapu;

    const-string v1, "PICASA_USER"

    const/16 v2, 0xa

    const-string v3, "PicasaUser"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->k:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SCOPE_CONSENT_DESCRIPTION"

    const/16 v2, 0xb

    const-string v3, "Permission"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->l:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SCOPE_CONSENT_DETAILS"

    const/16 v2, 0xc

    const-string v3, "ScopeConsentDetails"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->m:Lapu;

    new-instance v0, Lapu;

    const-string v1, "PERMISSION_ADVICE"

    const/16 v2, 0xd

    const-string v3, "issueAdvice"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->n:Lapu;

    new-instance v0, Lapu;

    const-string v1, "STORE_CONSENT_REMOTELY"

    const/16 v2, 0xe

    const-string v3, "storeConsentRemotely"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->o:Lapu;

    new-instance v0, Lapu;

    const-string v1, "EXPIRY_IN_S"

    const/16 v2, 0xf

    const-string v3, "Expiry"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->p:Lapu;

    new-instance v0, Lapu;

    const-string v1, "INFO"

    const/16 v2, 0x10

    const-string v3, "Info"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->q:Lapu;

    new-instance v0, Lapu;

    const-string v1, "TOKEN"

    const/16 v2, 0x11

    const-string v3, "Token"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->r:Lapu;

    new-instance v0, Lapu;

    const-string v1, "CAN_UPGRADE_PLUS"

    const/16 v2, 0x12

    const-string v3, "GooglePlusUpgrade"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->s:Lapu;

    new-instance v0, Lapu;

    const-string v1, "NEEDS_CREDIT_CARD"

    const/16 v2, 0x13

    const-string v3, "CC"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->t:Lapu;

    new-instance v0, Lapu;

    const-string v1, "FIRST_NAME"

    const/16 v2, 0x14

    const-string v3, "firstName"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->u:Lapu;

    new-instance v0, Lapu;

    const-string v1, "LAST_NAME"

    const/16 v2, 0x15

    const-string v3, "lastName"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->v:Lapu;

    new-instance v0, Lapu;

    const-string v1, "ROP_TEXT"

    const/16 v2, 0x16

    const-string v3, "RopText"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->w:Lapu;

    new-instance v0, Lapu;

    const-string v1, "ROP_REVISION"

    const/16 v2, 0x17

    const-string v3, "RopRevision"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->x:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SERVICE_HOSTED"

    const/16 v2, 0x18

    const-string v3, "HOSTED"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->y:Lapu;

    new-instance v0, Lapu;

    const-string v1, "CONSENT_DATA_BASE64"

    const/16 v2, 0x19

    const-string v3, "ConsentDataBase64"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->z:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SERVICE_GPLUS"

    const/16 v2, 0x1a

    const-string v3, "googleme"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->A:Lapu;

    new-instance v0, Lapu;

    const-string v1, "URL"

    const/16 v2, 0x1b

    const-string v3, "Url"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->B:Lapu;

    new-instance v0, Lapu;

    const-string v1, "SERVICE_ES_MOBILE"

    const/16 v2, 0x1c

    const-string v3, "esmobile"

    invoke-direct {v0, v1, v2, v3}, Lapu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lapu;->C:Lapu;

    const/16 v0, 0x1d

    new-array v0, v0, [Lapu;

    sget-object v1, Lapu;->a:Lapu;

    aput-object v1, v0, v4

    sget-object v1, Lapu;->b:Lapu;

    aput-object v1, v0, v5

    sget-object v1, Lapu;->c:Lapu;

    aput-object v1, v0, v6

    sget-object v1, Lapu;->d:Lapu;

    aput-object v1, v0, v7

    sget-object v1, Lapu;->e:Lapu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lapu;->f:Lapu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lapu;->g:Lapu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lapu;->h:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lapu;->i:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lapu;->j:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lapu;->k:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lapu;->l:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lapu;->m:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lapu;->n:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lapu;->o:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lapu;->p:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lapu;->q:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lapu;->r:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lapu;->s:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lapu;->t:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lapu;->u:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lapu;->v:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lapu;->w:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lapu;->x:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lapu;->y:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lapu;->z:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lapu;->A:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lapu;->B:Lapu;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lapu;->C:Lapu;

    aput-object v2, v0, v1

    sput-object v0, Lapu;->E:[Lapu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lapu;->D:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lapu;
    .locals 1

    const-class v0, Lapu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lapu;

    return-object v0
.end method

.method public static values()[Lapu;
    .locals 1

    sget-object v0, Lapu;->E:[Lapu;

    invoke-virtual {v0}, [Lapu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapu;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lapu;->D:Ljava/lang/String;

    return-object v0
.end method
