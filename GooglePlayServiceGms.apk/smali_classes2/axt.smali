.class final Laxt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field private final d:I

.field private final e:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/net/DatagramPacket;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v0

    iput-object v0, p0, Laxt;->a:[B

    invoke-virtual {p1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v0

    iput v0, p0, Laxt;->d:I

    const/4 v0, 0x0

    iput v0, p0, Laxt;->b:I

    const/4 v0, -0x1

    iput v0, p0, Laxt;->c:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laxt;->e:Ljava/util/Map;

    return-void
.end method

.method private e()I
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laxt;->a(I)V

    iget-object v0, p0, Laxt;->a:[B

    iget v1, p0, Laxt;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Laxt;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget v0, p0, Laxt;->c:I

    if-ltz v0, :cond_0

    iget v0, p0, Laxt;->c:I

    :goto_0
    iget v1, p0, Laxt;->b:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Laxt;->d:I

    goto :goto_0
.end method

.method final a(I)V
    .locals 1

    invoke-virtual {p0}, Laxt;->a()I

    move-result v0

    if-ge v0, p1, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 4

    array-length v0, p1

    invoke-virtual {p0, v0}, Laxt;->a(I)V

    iget-object v0, p0, Laxt;->a:[B

    iget v1, p0, Laxt;->b:I

    const/4 v2, 0x0

    array-length v3, p1

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Laxt;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Laxt;->b:I

    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Laxt;->a(I)V

    iget-object v0, p0, Laxt;->a:[B

    iget v1, p0, Laxt;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Laxt;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Laxt;->a:[B

    iget v2, p0, Laxt;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Laxt;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final c()[Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {p0}, Laxt;->a()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0, v2}, Laxt;->a(I)V

    iget-object v0, p0, Laxt;->a:[B

    iget v1, p0, Laxt;->b:I

    aget-byte v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Laxt;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laxt;->b:I

    :cond_1
    :goto_0
    iget-object v0, p0, Laxt;->e:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0

    :cond_2
    and-int/lit16 v0, v0, 0xc0

    const/16 v1, 0xc0

    if-ne v0, v1, :cond_3

    move v1, v2

    :goto_1
    iget v6, p0, Laxt;->b:I

    if-eqz v1, :cond_5

    invoke-direct {p0}, Laxt;->e()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0}, Laxt;->e()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    iget-object v3, p0, Laxt;->e:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid label pointer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_4
    move-object v3, v0

    :goto_2
    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Laxt;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v0

    goto :goto_2

    :cond_6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    invoke-direct {p0}, Laxt;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Laxt;->a(I)V

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Laxt;->a:[B

    iget v3, p0, Laxt;->b:I

    sget-object v4, Laxn;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iget v2, p0, Laxt;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Laxt;->b:I

    return-object v1
.end method
