.class public final Lehm;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:[Leho;

.field public e:[Lehn;

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lehm;->a:I

    const-string v0, ""

    iput-object v0, p0, Lehm;->b:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lehm;->c:I

    invoke-static {}, Leho;->c()[Leho;

    move-result-object v0

    iput-object v0, p0, Lehm;->d:[Leho;

    invoke-static {}, Lehn;->c()[Lehn;

    move-result-object v0

    iput-object v0, p0, Lehm;->e:[Lehn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lehm;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lehm;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v2, p0, Lehm;->a:I

    if-eqz v2, :cond_0

    iget v2, p0, Lehm;->a:I

    invoke-static {v4, v2}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lehm;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lehm;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget v2, p0, Lehm;->c:I

    if-eq v2, v4, :cond_2

    const/4 v2, 0x3

    iget v3, p0, Lehm;->c:I

    invoke-static {v2, v3}, Lizn;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lehm;->d:[Leho;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lehm;->d:[Leho;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lehm;->d:[Leho;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Lehm;->d:[Leho;

    aget-object v3, v3, v0

    if-eqz v3, :cond_3

    const/4 v4, 0x4

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    :cond_5
    iget-object v2, p0, Lehm;->e:[Lehn;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lehm;->e:[Lehn;

    array-length v2, v2

    if-lez v2, :cond_7

    :goto_1
    iget-object v2, p0, Lehm;->e:[Lehn;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    iget-object v2, p0, Lehm;->e:[Lehn;

    aget-object v2, v2, v1

    if-eqz v2, :cond_6

    const/4 v3, 0x5

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    iget-wide v1, p0, Lehm;->f:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_8

    const/4 v1, 0x6

    iget-wide v2, p0, Lehm;->f:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lehm;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehm;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehm;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehm;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lehm;->d:[Leho;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leho;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lehm;->d:[Leho;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Leho;

    invoke-direct {v3}, Leho;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lehm;->d:[Leho;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Leho;

    invoke-direct {v3}, Leho;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lehm;->d:[Leho;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lehm;->e:[Lehn;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lehn;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lehm;->e:[Lehn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lehn;

    invoke-direct {v3}, Lehn;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lehm;->e:[Lehn;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lehn;

    invoke-direct {v3}, Lehn;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lehm;->e:[Lehn;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lehm;->f:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lehm;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lehm;->a:I

    invoke-virtual {p1, v3, v0}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Lehm;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Lehm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Lehm;->c:I

    if-eq v0, v3, :cond_2

    const/4 v0, 0x3

    iget v2, p0, Lehm;->c:I

    invoke-virtual {p1, v0, v2}, Lizn;->c(II)V

    :cond_2
    iget-object v0, p0, Lehm;->d:[Leho;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lehm;->d:[Leho;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_0
    iget-object v2, p0, Lehm;->d:[Leho;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lehm;->d:[Leho;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lehm;->e:[Lehn;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lehm;->e:[Lehn;

    array-length v0, v0

    if-lez v0, :cond_6

    :goto_1
    iget-object v0, p0, Lehm;->e:[Lehn;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lehm;->e:[Lehn;

    aget-object v0, v0, v1

    if-eqz v0, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    iget-wide v0, p0, Lehm;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    iget-wide v1, p0, Lehm;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_7
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehm;

    iget v2, p0, Lehm;->a:I

    iget v3, p1, Lehm;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lehm;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lehm;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lehm;->b:Ljava/lang/String;

    iget-object v3, p1, Lehm;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lehm;->c:I

    iget v3, p1, Lehm;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lehm;->d:[Leho;

    iget-object v3, p1, Lehm;->d:[Leho;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lehm;->e:[Lehn;

    iget-object v3, p1, Lehm;->e:[Lehn;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-wide v2, p0, Lehm;->f:J

    iget-wide v4, p1, Lehm;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    iget v0, p0, Lehm;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lehm;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehm;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lehm;->d:[Leho;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lehm;->e:[Lehn;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehm;->f:J

    iget-wide v3, p0, Lehm;->f:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lehm;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
