.class final Lgtf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Z

.field final c:I

.field final d:Ljava/util/Locale;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZILjava/util/Locale;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lgtf;-><init>(ZZILjava/util/Locale;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(ZZILjava/util/Locale;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lgtf;->a:Z

    iput-boolean p2, p0, Lgtf;->b:Z

    iput p3, p0, Lgtf;->c:I

    iput-object p4, p0, Lgtf;->d:Ljava/util/Locale;

    iput-object p5, p0, Lgtf;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lgtf;

    iget-boolean v2, p0, Lgtf;->a:Z

    iget-boolean v3, p1, Lgtf;->a:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lgtf;->b:Z

    iget-boolean v3, p1, Lgtf;->b:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lgtf;->c:I

    iget v3, p1, Lgtf;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lgtf;->e:Ljava/lang/String;

    iget-object v3, p1, Lgtf;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgtf;->d:Ljava/util/Locale;

    iget-object v3, p1, Lgtf;->d:Ljava/util/Locale;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Lgtf;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lgtf;->a:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lgtf;->d:Ljava/util/Locale;

    if-nez v4, :cond_2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lgtf;->b:Z

    if-eqz v1, :cond_3

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgtf;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lgtf;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lgtf;->d:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v2, v3

    goto :goto_3
.end method
