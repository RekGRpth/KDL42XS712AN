.class public final Lele;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/ContentProviderClient;

.field final b:Landroid/database/Cursor;


# direct methods
.method private constructor <init>(Landroid/content/ContentProviderClient;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lele;->a:Landroid/content/ContentProviderClient;

    iput-object p2, p0, Lele;->b:Landroid/database/Cursor;

    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Lele;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "Could not connect to content provider %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v6

    :goto_1
    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v4, p2

    :try_start_2
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    move-object v0, v6

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    new-instance v0, Lelf;

    invoke-direct {v0, v1}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_2
    new-instance v1, Lele;

    invoke-direct {v1, v0, v2}, Lele;-><init>(Landroid/content/ContentProviderClient;Landroid/database/Cursor;)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(I)Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(I)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(I)J
    .locals 2

    :try_start_0
    iget-object v0, p0, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    new-instance v1, Lelf;

    invoke-direct {v1, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
