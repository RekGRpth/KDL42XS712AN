.class final Lhlk;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Livi;III)Ljava/util/List;
    .locals 14

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Livi;->k(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0, v2, v1}, Livi;->c(II)Livi;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Livi;->i(I)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Livi;->f(I)Livi;

    move-result-object v8

    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Livi;->d(I)J

    move-result-wide v3

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v8, p1}, Livi;->k(I)I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-virtual {v8, p1, v2}, Livi;->c(II)Livi;

    move-result-object v6

    const/4 v5, 0x3

    new-array v9, v5, [F

    const/4 v5, 0x0

    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Livi;->e(I)F

    move-result v10

    aput v10, v9, v5

    const/4 v5, 0x1

    const/4 v10, 0x2

    invoke-virtual {v6, v10}, Livi;->e(I)F

    move-result v10

    aput v10, v9, v5

    const/4 v5, 0x2

    const/4 v10, 0x3

    invoke-virtual {v6, v10}, Livi;->e(I)F

    move-result v10

    aput v10, v9, v5

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Livi;->i(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Livi;->c(I)I

    move-result v5

    :goto_2
    move/from16 v0, p3

    invoke-virtual {v6, v0}, Livi;->i(I)Z

    move-result v10

    if-eqz v10, :cond_1

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Livi;->c(I)I

    move-result v6

    :goto_3
    int-to-long v10, v5

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v3, v10

    int-to-long v5, v6

    add-long/2addr v3, v5

    new-instance v5, Lhng;

    invoke-direct {v5, v3, v4, v9}, Lhng;-><init>(J[F)V

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    goto :goto_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-object v7
.end method

.method static a(Livi;Lhrz;)Ljava/util/List;
    .locals 3

    const/4 v1, 0x7

    const/16 v2, 0x8

    sget-object v0, Lhrz;->d:Lhrz;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    invoke-static {p0, v0, v1, v2}, Lhlk;->a(Livi;III)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhrz;->e:Lhrz;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x4

    invoke-static {p0, v0, v1, v2}, Lhlk;->a(Livi;III)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lhrz;->f:Lhrz;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x5

    const/16 v1, 0x9

    invoke-static {p0, v0, v2, v1}, Lhlk;->a(Livi;III)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported scanner type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
