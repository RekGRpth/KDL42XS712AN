.class final Lfgo;
.super Lfgu;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p8, p0, Lfgo;->c:Ljava/lang/String;

    iput-object p9, p0, Lfgo;->d:Ljava/util/List;

    iput-object p10, p0, Lfgo;->e:Ljava/lang/String;

    iput-object p11, p0, Lfgo;->f:Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 9

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lfgo;->c:Ljava/lang/String;

    iget-object v3, p0, Lfgo;->d:Ljava/util/List;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v0}, Lfdl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p2, p3, v2, v5, v4}, Lffe;->c(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v2

    const-string v0, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v4, "PeopleService"

    const-string v5, "%s people added to %s"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v0

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_1
    aput-object v0, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-nez v2, :cond_4

    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lffc;->d:Lffc;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-object v0

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iget-object v3, p0, Lfgo;->e:Ljava/lang/String;

    iget-object v4, p0, Lfgo;->f:Ljava/lang/String;

    iget-object v5, p0, Lfgo;->c:Ljava/lang/String;

    iget-object v6, p0, Lfgo;->d:Ljava/util/List;

    invoke-virtual {v0, v3, v4, v5, v6}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    if-ne v0, v8, :cond_6

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lfgo;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, Lfgo;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v3, p0, Lfgo;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lfhz;->c(Ljava/lang/String;)V

    :cond_5
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "circle_id"

    iget-object v4, p0, Lfgo;->c:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_name"

    if-nez v2, :cond_7

    :goto_4
    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "added_people"

    iget-object v0, p0, Lfgo;->d:Ljava/util/List;

    sget-object v2, Lfdl;->c:[Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lfgo;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v3, p0, Lfgo;->e:Ljava/lang/String;

    iget-object v4, p0, Lfgo;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v1}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_3

    :cond_7
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method
