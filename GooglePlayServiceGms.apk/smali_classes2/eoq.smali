.class final Leoq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Context must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Alias must not be empty."

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "[a-zA-Z0-9_]*"

    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Alias must match: [a-zA-Z0-9_]*"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Leoq;->a:Landroid/content/Context;

    iput-object p2, p0, Leoq;->b:Ljava/lang/String;

    return-void
.end method
