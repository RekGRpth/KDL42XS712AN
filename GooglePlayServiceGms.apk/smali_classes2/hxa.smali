.class public abstract Lhxa;
.super Lhxe;
.source "SourceFile"


# instance fields
.field a:J

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lhxe;-><init>()V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lhxa;->a:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhxa;->b:Z

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 2

    iget-wide v0, p0, Lhxa;->a:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lhxa;->a:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->j:Z

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 3

    invoke-super {p0, p1}, Lhxe;->a(Ljava/lang/StringBuilder;)V

    const-string v0, ", interval[ms]="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhxa;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", trigger="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lhxa;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lhxa;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->j:Z

    :cond_0
    invoke-super {p0}, Lhxe;->b()V

    return-void
.end method
