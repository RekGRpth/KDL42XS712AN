.class final Ldkv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldkn;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Ldkn;ILjava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Ldkv;->a:Ldkn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Ldkv;->b:I

    iput-object p3, p0, Ldkv;->c:Ljava/lang/String;

    iput-boolean p4, p0, Ldkv;->d:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Ldkv;->a:Ldkn;

    invoke-static {v0}, Ldkn;->a(Ldkn;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkv;->a:Ldkn;

    invoke-static {v0}, Ldkn;->b(Ldkn;)Ldkj;

    move-result-object v0

    iget v1, p0, Ldkv;->b:I

    iget-object v2, p0, Ldkv;->c:Ljava/lang/String;

    iget-boolean v3, p0, Ldkv;->d:Z

    invoke-interface {v0, v1, v2, v3}, Ldkj;->a(ILjava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ldkn;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ignoring onReliableSendResult() callback."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
