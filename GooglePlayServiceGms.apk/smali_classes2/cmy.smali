.class final Lcmy;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final f:J

.field private final g:J


# direct methods
.method constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JJJ)V
    .locals 8

    sget-object v3, Lcmr;->f:Lcmr;

    sget-object v7, Lcms;->b:Lcms;

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-object p4, p0, Lcmy;->c:Ljava/lang/String;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v2, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-wide p6, p0, Lcmy;->d:J

    move-wide/from16 v0, p8

    iput-wide v0, p0, Lcmy;->f:J

    move-wide/from16 v0, p10

    iput-wide v0, p0, Lcmy;->g:J

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->e:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmy;->f:J

    const-string v0, "contentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "contentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcmy;->c:Ljava/lang/String;

    :goto_0
    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmy;->d:J

    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcjc;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const-string v0, "fileSize"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmy;->g:J

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcmy;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmy;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 4

    iget-wide v0, p0, Lcmy;->f:J

    invoke-interface {p1, v0, v1}, Lcfz;->b(J)Lcge;

    move-result-object v0

    invoke-virtual {v0}, Lcge;->l()V

    iget-object v0, p0, Lcmy;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmy;->c:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmy;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcfp;->s(Ljava/lang/String;)V

    :goto_0
    iget-wide v0, p0, Lcmy;->g:J

    invoke-virtual {p2, v0, v1}, Lcfp;->a(J)V

    iget-object v0, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p2, v0}, Lcjc;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {p2}, Lcfp;->Y()V

    invoke-virtual {p2}, Lcfp;->k()V

    new-instance v0, Lcmj;

    iget-object v1, p0, Lclu;->b:Lcfc;

    iget-object v2, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcms;->b:Lcms;

    invoke-direct {v0, v1, v2, v3}, Lcmj;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcfp;->s(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "pendingUploadSqlId"

    iget-wide v2, p0, Lcmy;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v1, p0, Lcmy;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "contentHash"

    iget-object v2, p0, Lcmy;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v1, "writeOpenTime"

    iget-wide v2, p0, Lcmy;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "metadataDelta"

    iget-object v2, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcjc;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "fileSize"

    iget-wide v2, p0, Lcmy;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcmy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcmy;

    iget-object v2, p0, Lcmy;->c:Ljava/lang/String;

    iget-object v3, p1, Lcmy;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lcmy;->g:J

    iget-wide v4, p1, Lcmy;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lcmy;->f:J

    iget-wide v4, p1, Lcmy;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lcmy;->d:J

    iget-wide v4, p1, Lcmy;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    const/16 v6, 0x20

    iget-object v0, p0, Lcmy;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcmy;->g:J

    iget-wide v4, p0, Lcmy;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcmy;->f:J

    iget-wide v3, p0, Lcmy;->f:J

    ushr-long/2addr v3, v6

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcmy;->d:J

    iget-wide v3, p0, Lcmy;->d:J

    ushr-long/2addr v3, v6

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcmy;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UndoContentAndMetadataOp [mContentHash="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcmy;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mWriteOpenTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcmy;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMetadataChangeSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcmy;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPendingUploadSqlId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcmy;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcmy;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
