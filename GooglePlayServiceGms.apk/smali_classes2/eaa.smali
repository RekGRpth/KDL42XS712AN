.class public final Leaa;
.super Ldwz;
.source "SourceFile"


# instance fields
.field private final g:Ldvn;

.field private final h:Landroid/view/LayoutInflater;

.field private final i:Leac;

.field private final j:Z

.field private final k:I

.field private l:Ldzz;

.field private m:Lbgo;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldvn;Leac;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Leaa;-><init>(Ldvn;Leac;I)V

    return-void
.end method

.method public constructor <init>(Ldvn;Leac;I)V
    .locals 2

    sget v0, Lxb;->j:I

    invoke-direct {p0, p1, v0, p3}, Ldwz;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Leaa;->g:Ldvn;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leaa;->h:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leac;

    iput-object v0, p0, Leaa;->i:Leac;

    invoke-virtual {p1}, Ldvn;->l()Z

    move-result v0

    iput-boolean v0, p0, Leaa;->j:Z

    invoke-virtual {p1}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lwy;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Leaa;->k:I

    return-void
.end method

.method static synthetic a(Leaa;)Z
    .locals 1

    iget-boolean v0, p0, Leaa;->j:Z

    return v0
.end method

.method static synthetic b(Leaa;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leaa;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Leaa;)Ldvn;
    .locals 1

    iget-object v0, p0, Leaa;->g:Ldvn;

    return-object v0
.end method

.method static synthetic d(Leaa;)I
    .locals 1

    iget v0, p0, Leaa;->k:I

    return v0
.end method

.method static synthetic e(Leaa;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leaa;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Leaa;)Leac;
    .locals 1

    iget-object v0, p0, Leaa;->i:Leac;

    return-object v0
.end method

.method static synthetic g(Leaa;)Ldzz;
    .locals 1

    iget-object v0, p0, Leaa;->l:Ldzz;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 7

    check-cast p4, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lead;

    instance-of v1, p4, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v1, :cond_3

    check-cast p4, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {p4}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v3

    iget-object v1, v0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v2

    sget v4, Lwz;->f:I

    invoke-virtual {v0, v1, v2, v4}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Lead;->r:Leaa;

    iget-boolean v1, v1, Leaa;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lead;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v1, v0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lead;->i:Landroid/database/CharArrayBuffer;

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Lead;->h:Landroid/widget/TextView;

    iget-object v2, v0, Lead;->i:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v3, 0x0

    iget-object v4, v0, Lead;->i:Landroid/database/CharArrayBuffer;

    iget v4, v4, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/TextView;->setText([CII)V

    iget-object v1, v0, Lead;->j:Landroid/widget/TextView;

    sget v2, Lxf;->bh:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lead;->k:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lead;->l:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iget-object v2, v0, Lead;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v1, v0, Lead;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_0
    iget-object v1, v0, Lead;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p4}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget-object v4, v0, Lead;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :pswitch_0
    iget-object v4, v0, Lead;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    sget v4, Lwz;->u:I

    invoke-virtual {v0, v1, v2, v4}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    sget v4, Lwz;->u:I

    invoke-virtual {v0, v1, v2, v4}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    sget v4, Lwz;->u:I

    invoke-virtual {v0, v1, v2, v4}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v4, v0, Lead;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v4, 0x0

    sget v5, Lwz;->u:I

    invoke-virtual {v0, v1, v4, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v4, 0x0

    sget v5, Lwz;->u:I

    invoke-virtual {v0, v1, v4, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v4, v0, Lead;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lead;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v4, 0x0

    sget v5, Lwz;->u:I

    invoke-virtual {v0, v1, v4, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lead;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Game;

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, v0, Lead;->n:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lead;->o:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lead;->o:Landroid/widget/TextView;

    sget v2, Lxf;->bm:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lead;->p:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lead;->q:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_3
    return-void

    :cond_3
    invoke-virtual {v0, p4}, Lead;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V
    .locals 7

    if-eqz p2, :cond_1

    iget-object v0, p0, Leaa;->l:Ldzz;

    invoke-virtual {v0, p1}, Ldzz;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    :cond_0
    invoke-virtual {p0}, Leaa;->notifyDataSetChanged()V

    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Leaa;->m:Lbgo;

    invoke-virtual {v1}, Lbgo;->a()I

    move-result v4

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Leaa;->m:Lbgo;

    invoke-virtual {v0, v3}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_1
    if-lez v2, :cond_3

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->a(Ljava/util/ArrayList;)V

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0
.end method

.method public final a(Ldzz;)V
    .locals 3

    iput-object p1, p0, Leaa;->l:Ldzz;

    iget-object v0, p1, Ldzz;->b:Lbhb;

    iput-object v0, p0, Leaa;->m:Lbgo;

    const/4 v0, 0x2

    new-array v0, v0, [Lbgo;

    const/4 v1, 0x0

    iget-object v2, p1, Ldzz;->a:Lbhb;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Leaa;->m:Lbgo;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Leaa;->a([Lbgo;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Leaa;->n:Ljava/lang/String;

    iput-object p2, p0, Leaa;->o:Ljava/lang/String;

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Leaa;->h:Landroid/view/LayoutInflater;

    sget v1, Lxc;->x:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lead;

    invoke-direct {v1, p0, v0}, Lead;-><init>(Leaa;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
