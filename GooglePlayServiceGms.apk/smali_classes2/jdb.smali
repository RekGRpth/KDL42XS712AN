.class public final Ljdb;
.super Lizk;
.source "SourceFile"


# instance fields
.field a:I

.field b:Ljda;

.field c:Ljdf;

.field d:Ljcz;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Ljdb;->a:I

    iput-object v1, p0, Ljdb;->b:Ljda;

    iput-object v1, p0, Ljdb;->c:Ljdf;

    iput-object v1, p0, Ljdb;->d:Ljcz;

    const/4 v0, -0x1

    iput v0, p0, Ljdb;->i:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljdb;->i:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljdb;->b()I

    :cond_0
    iget v0, p0, Ljdb;->i:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljdb;->a(I)Ljdb;

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljda;

    invoke-direct {v0}, Ljda;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Ljdb;->a(Ljda;)Ljdb;

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Ljdb;->a(Ljdf;)Ljdb;

    goto :goto_0

    :sswitch_4
    new-instance v0, Ljcz;

    invoke-direct {v0}, Ljcz;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Ljdb;->a(Ljcz;)Ljdb;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(I)Ljdb;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdb;->e:Z

    iput p1, p0, Ljdb;->a:I

    return-object p0
.end method

.method public final a(Ljcz;)Ljdb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdb;->h:Z

    iput-object p1, p0, Ljdb;->d:Ljcz;

    return-object p0
.end method

.method public final a(Ljda;)Ljdb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdb;->f:Z

    iput-object p1, p0, Ljdb;->b:Ljda;

    return-object p0
.end method

.method public final a(Ljdf;)Ljdb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdb;->g:Z

    iput-object p1, p0, Ljdb;->c:Ljdf;

    return-object p0
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljdb;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdb;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Ljdb;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljdb;->b:Ljda;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Ljdb;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdb;->c:Ljdf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljdb;->h:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljdb;->d:Ljcz;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljdb;->e:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdb;->a:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljdb;->f:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljdb;->b:Ljda;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljdb;->g:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdb;->c:Ljdf;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljdb;->h:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljdb;->d:Ljcz;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Ljdb;->i:I

    return v0
.end method
