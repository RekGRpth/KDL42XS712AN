.class public final Ligp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ligk;


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Z

.field public final c:Lhon;

.field public final d:Ljava/util/HashMap;

.field private final e:Landroid/content/Context;

.field private final f:Lhvb;

.field private final g:Ligf;

.field private final h:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhvb;Ligf;Landroid/os/Handler;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ligp;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ligp;->b:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ligp;->d:Ljava/util/HashMap;

    iput-object p1, p0, Ligp;->e:Landroid/content/Context;

    iput-object p2, p0, Ligp;->f:Lhvb;

    iput-object p3, p0, Ligp;->g:Ligf;

    iput-object p4, p0, Ligp;->h:Landroid/os/Handler;

    new-instance v0, Lhon;

    const-class v1, Lcom/google/android/location/internal/GoogleLocationManagerService;

    new-instance v2, Ligq;

    invoke-direct {v2, p0}, Ligq;-><init>(Ligp;)V

    const/4 v3, 0x3

    invoke-direct {v0, p1, v1, v2, v3}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;Lhor;I)V

    iput-object v0, p0, Ligp;->c:Lhon;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/places/PlaceSubscription;)V
    .locals 8

    const/4 v7, 0x1

    new-instance v0, Ligi;

    iget-object v1, p0, Ligp;->e:Landroid/content/Context;

    iget-object v2, p0, Ligp;->h:Landroid/os/Handler;

    iget-object v3, p0, Ligp;->f:Lhvb;

    iget-object v4, p0, Ligp;->g:Ligf;

    move-object v5, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ligi;-><init>(Landroid/content/Context;Landroid/os/Handler;Lhvb;Ligf;Lcom/google/android/location/places/PlaceSubscription;Ligk;)V

    iget-object v1, p0, Ligp;->d:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ligi;

    if-eqz v1, :cond_0

    const-string v2, "PlaceSubscriptionManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Replacing place reporter for subscription "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/location/places/PlaceSubscription;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ligi;->a()V

    :cond_0
    const-string v1, "PlaceReporter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "starting location updates for subscription: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {v3}, Lcom/google/android/location/places/PlaceSubscription;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v1, v1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    iget-object v2, v0, Ligi;->c:Landroid/content/pm/PackageManager;

    invoke-static {v1, v2}, Lilm;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v3

    new-instance v5, Lild;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, v0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, v2, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v1, v2}, Lild;-><init>(ILjava/lang/String;)V

    iget-object v6, v0, Ligi;->d:Lhvb;

    iget-object v1, v0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v1, v1, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    iget-object v2, v0, Ligi;->h:Lign;

    const/4 v0, 0x2

    if-ne v3, v0, :cond_1

    move v4, v7

    :goto_0
    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v0, v6

    move v3, v7

    invoke-virtual/range {v0 .. v5}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZZLjava/util/Collection;)V

    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Ligi;)V
    .locals 1

    iget-object v0, p1, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {p0, v0}, Ligp;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    return-void
.end method

.method public final b(Lcom/google/android/location/places/PlaceSubscription;)V
    .locals 3

    iget-object v1, p0, Ligp;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ligp;->c:Lhon;

    invoke-virtual {v0, p1}, Lhon;->b(Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Ligp;->b:Z

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ligp;->d:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    const-string v0, "PlaceSubscriptionManager"

    const-string v1, "removePlaceUpdates for unknown callbackIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    invoke-virtual {v0}, Ligi;->a()V

    goto :goto_0
.end method
