.class public final Liwm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liwy;


# static fields
.field private static final c:Lixh;


# instance fields
.field final a:Liwi;

.field private b:J

.field private final d:Lixh;

.field private final e:Lixh;

.field private final f:Lixh;

.field private final g:Liww;

.field private h:Liwh;

.field private i:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lixh;->a()Lixh;

    move-result-object v0

    sput-object v0, Liwm;->c:Lixh;

    return-void
.end method

.method constructor <init>(Liwi;)V
    .locals 7

    const/4 v6, 0x0

    const-wide/high16 v4, 0x4019000000000000L    # 6.25

    const/4 v3, 0x1

    const/4 v2, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Liww;

    invoke-direct {v0}, Liww;-><init>()V

    iput-object v0, p0, Liwm;->g:Liww;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liwm;->i:D

    iput-object p1, p0, Liwm;->a:Liwi;

    new-instance v0, Lixh;

    invoke-direct {v0, v2, v3}, Lixh;-><init>(II)V

    iput-object v0, p0, Liwm;->d:Lixh;

    new-instance v0, Lixh;

    invoke-direct {v0, v2, v2}, Lixh;-><init>(II)V

    iput-object v0, p0, Liwm;->e:Lixh;

    new-instance v0, Lixh;

    invoke-direct {v0, v2, v2}, Lixh;-><init>(II)V

    iput-object v0, p0, Liwm;->f:Lixh;

    iget-object v0, p0, Liwm;->f:Lixh;

    invoke-virtual {v0, v6, v6, v4, v5}, Lixh;->a(IID)V

    iget-object v0, p0, Liwm;->f:Lixh;

    invoke-virtual {v0, v3, v3, v4, v5}, Lixh;->a(IID)V

    return-void
.end method

.method public static b()Liwm;
    .locals 9

    const/4 v5, 0x1

    const/4 v2, 0x2

    new-instance v3, Lixi;

    invoke-direct {v3, v2, v2}, Lixi;-><init>(II)V

    new-instance v4, Lixi;

    invoke-direct {v4, v2, v2}, Lixi;-><init>(II)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    new-instance v6, Liwj;

    new-instance v7, Lixh;

    invoke-direct {v7, v2, v5}, Lixh;-><init>(II)V

    new-instance v8, Lixh;

    invoke-direct {v8, v2, v2}, Lixh;-><init>(II)V

    invoke-direct {v6, v7, v8, v3, v4}, Liwj;-><init>(Lixh;Lixh;Lixi;Lixi;)V

    new-instance v7, Liwn;

    invoke-direct {v7, v6}, Liwn;-><init>(Liwi;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Liwk;

    const-wide/high16 v3, 0x4036000000000000L    # 22.0

    invoke-direct/range {v0 .. v5}, Liwk;-><init>(Ljava/util/Collection;IDI)V

    new-instance v1, Liwm;

    invoke-direct {v1, v0}, Liwm;-><init>(Liwi;)V

    return-object v1
.end method

.method public static c()V
    .locals 0

    return-void
.end method

.method static synthetic g()Lixh;
    .locals 1

    sget-object v0, Liwm;->c:Lixh;

    return-object v0
.end method


# virtual methods
.method public final a()Liwe;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Liwm;->a(I)Liwe;

    move-result-object v0

    return-object v0
.end method

.method final a(I)Liwe;
    .locals 19

    move-object/from16 v0, p0

    iget-object v2, v0, Liwm;->h:Liwh;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Liwm;->a:Liwi;

    invoke-interface {v2}, Liwi;->a()Lixh;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Liwm;->e()D

    move-result-wide v9

    if-eqz v2, :cond_1

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v3, v9, v3

    if-nez v3, :cond_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lixh;->a(II)D

    move-result-wide v13

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lixh;->a(II)D

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v5, v0, Liwm;->g:Liww;

    iget-object v2, v5, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v2}, Ljava/util/EnumMap;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    iget-object v2, v5, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v2}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liwf;

    move-object v12, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Liwm;->h:Liwh;

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Liwh;->a(D)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v2, v0, Liwm;->h:Liwh;

    invoke-virtual {v2, v13, v14}, Liwh;->b(D)I

    move-result v4

    const-wide v5, 0x408f400000000000L    # 1000.0

    mul-double/2addr v5, v9

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    mul-double/2addr v5, v7

    double-to-int v5, v5

    new-instance v2, Liwe;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-wide v10, v0, Liwm;->i:D

    move/from16 v8, p1

    invoke-direct/range {v2 .. v12}, Liwe;-><init>(IIILjava/lang/String;Ljava/lang/String;IFDLiwf;)V

    goto :goto_0

    :cond_3
    sget-object v4, Liwf;->e:Liwf;

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iget-object v5, v5, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v5}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-object v6, v4

    move-wide v4, v2

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liwx;

    iget-wide v7, v3, Liwx;->a:D

    sub-double/2addr v7, v13

    iget-wide v0, v3, Liwx;->b:D

    move-wide/from16 v17, v0

    sub-double v17, v17, v15

    mul-double/2addr v7, v7

    mul-double v17, v17, v17

    add-double v7, v7, v17

    cmpg-double v3, v7, v4

    if-gez v3, :cond_5

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liwf;

    move-object v4, v2

    move-wide v2, v7

    :goto_3
    move-object v6, v4

    move-wide v4, v2

    goto :goto_2

    :cond_4
    move-object v12, v6

    goto :goto_1

    :cond_5
    move-wide v2, v4

    move-object v4, v6

    goto :goto_3
.end method

.method public final a(JLiwe;)V
    .locals 11

    iput-wide p1, p0, Liwm;->b:J

    iget-wide v0, p3, Liwe;->g:D

    iput-wide v0, p0, Liwm;->i:D

    const/4 v1, 0x0

    iget-object v0, p0, Liwm;->h:Liwh;

    if-nez v0, :cond_3

    new-instance v0, Liwh;

    iget v1, p3, Liwe;->c:I

    iget v2, p3, Liwe;->d:I

    invoke-direct {v0, v1, v2}, Liwh;-><init>(II)V

    iput-object v0, p0, Liwm;->h:Liwh;

    const/4 v0, 0x1

    move v1, v0

    :cond_0
    iget-object v0, p0, Liwm;->h:Liwh;

    iget v2, p3, Liwe;->c:I

    invoke-virtual {v0, v2}, Liwh;->a(I)D

    move-result-wide v2

    iget-object v0, p0, Liwm;->h:Liwh;

    iget v4, p3, Liwe;->d:I

    invoke-virtual {v0, v4}, Liwh;->b(I)D

    move-result-wide v4

    iget-object v6, p0, Liwm;->g:Liww;

    iget-object v7, p3, Liwe;->b:Liwf;

    iget-object v0, v6, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, v7}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwx;

    if-nez v0, :cond_1

    new-instance v0, Liwx;

    const/4 v8, 0x0

    invoke-direct {v0, v8}, Liwx;-><init>(B)V

    iget-object v6, v6, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v6, v7, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iput-wide v4, v0, Liwx;->a:D

    iput-wide v2, v0, Liwx;->b:D

    iget-object v0, p0, Liwm;->d:Lixh;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, v4, v5}, Lixh;->a(IID)V

    iget-object v0, p0, Liwm;->d:Lixh;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v2, v3}, Lixh;->a(IID)V

    iget v0, p3, Liwe;->e:I

    iget-object v2, p3, Liwe;->b:Liwf;

    sget-object v3, Liwf;->b:Liwf;

    if-ne v2, v3, :cond_2

    const/16 v2, 0x4e20

    if-ge v0, v2, :cond_2

    const/16 v0, 0x4e20

    :cond_2
    int-to-double v2, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    mul-double/2addr v2, v2

    iget-object v0, p0, Liwm;->e:Lixh;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v2, v3}, Lixh;->a(IID)V

    iget-object v0, p0, Liwm;->e:Lixh;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5, v2, v3}, Lixh;->a(IID)V

    if-eqz v1, :cond_4

    iget-object v0, p0, Liwm;->a:Liwi;

    iget-object v1, p0, Liwm;->d:Lixh;

    iget-object v2, p0, Liwm;->e:Lixh;

    invoke-interface {v0, v1, v2}, Liwi;->a(Lixh;Lixh;)V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Liwm;->a:Liwi;

    invoke-interface {v0}, Liwi;->a()Lixh;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lixh;->a(II)D

    move-result-wide v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lixh;->a(II)D

    move-result-wide v4

    iget-object v6, p0, Liwm;->h:Liwh;

    invoke-virtual {v6, v4, v5}, Liwh;->a(D)I

    move-result v7

    int-to-double v7, v7

    invoke-static {v7, v8}, Liwd;->b(D)D

    move-result-wide v7

    iget-wide v9, v6, Liwh;->a:D

    div-double v6, v7, v9

    mul-double/2addr v6, v2

    sub-double v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpl-double v6, v6, v8

    if-lez v6, :cond_0

    iget-object v6, p0, Liwm;->h:Liwh;

    new-instance v7, Liwh;

    iget v8, p3, Liwe;->c:I

    iget v9, p3, Liwe;->d:I

    invoke-direct {v7, v8, v9}, Liwh;-><init>(II)V

    iput-object v7, p0, Liwm;->h:Liwh;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Liwm;->h:Liwh;

    invoke-virtual {v9, v2, v3, v6}, Liwh;->a(DLiwh;)D

    move-result-wide v2

    invoke-virtual {v0, v7, v8, v2, v3}, Lixh;->a(IID)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v7, p0, Liwm;->h:Liwh;

    invoke-virtual {v7, v4, v5, v6}, Liwh;->b(DLiwh;)D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lixh;->a(IID)V

    iget-object v2, p0, Liwm;->a:Liwi;

    iget-object v3, p0, Liwm;->a:Liwi;

    invoke-interface {v3}, Liwi;->b()Lixh;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Liwi;->a(Lixh;Lixh;)V

    iget-object v0, p0, Liwm;->g:Liww;

    iget-object v2, p0, Liwm;->h:Liwh;

    iget-object v0, v0, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwx;

    iget-wide v4, v0, Liwx;->a:D

    invoke-virtual {v2, v4, v5, v6}, Liwh;->a(DLiwh;)D

    move-result-wide v4

    iput-wide v4, v0, Liwx;->a:D

    iget-wide v4, v0, Liwx;->b:D

    invoke-virtual {v2, v4, v5, v6}, Liwh;->b(DLiwh;)D

    move-result-wide v4

    iput-wide v4, v0, Liwx;->b:D

    goto :goto_1

    :cond_4
    iget-object v0, p0, Liwm;->a:Liwi;

    iget-object v1, p0, Liwm;->d:Lixh;

    sget-object v2, Liwm;->c:Lixh;

    iget-object v3, p0, Liwm;->e:Lixh;

    invoke-interface {v0, v1, v2, v3}, Liwi;->b(Lixh;Lixh;Lixh;)V

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 4

    iget-object v0, p0, Liwm;->h:Liwh;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Liwm;->a:Liwi;

    sget-object v1, Liwm;->c:Lixh;

    iget-object v2, p0, Liwm;->f:Lixh;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Liwi;->a(Lixh;Lixh;Lixh;)V

    goto :goto_0
.end method

.method public final e()D
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    iget-object v4, p0, Liwm;->a:Liwi;

    if-eqz v4, :cond_0

    iget-object v4, p0, Liwm;->h:Liwh;

    if-nez v4, :cond_2

    :cond_0
    move-wide v0, v2

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    iget-object v4, p0, Liwm;->a:Liwi;

    invoke-interface {v4}, Liwi;->b()Lixh;

    move-result-object v4

    if-nez v4, :cond_3

    move-wide v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v4, v5, v5}, Lixh;->a(II)D

    move-result-wide v2

    invoke-virtual {v4, v6, v6}, Lixh;->a(II)D

    move-result-wide v4

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    cmpg-double v4, v2, v0

    if-ltz v4, :cond_1

    move-wide v0, v2

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Liwm;->h:Liwh;

    iget-object v0, p0, Liwm;->g:Liww;

    iget-object v0, v0, Liww;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->clear()V

    return-void
.end method
