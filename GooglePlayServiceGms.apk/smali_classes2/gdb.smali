.class public final Lgdb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Lfsy;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgdb;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgdb;->b:Ljava/lang/String;

    iput-object p3, p0, Lgdb;->c:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 7

    const/16 v6, 0x1f4

    const/4 v5, 0x4

    const/4 v4, 0x0

    :try_start_0
    iget-object v1, p0, Lgdb;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lgdb;->b:Ljava/lang/String;

    sget-object v0, Lfsr;->af:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p2, Lfrx;->d:Lfry;

    invoke-static {}, Lfry;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Lfry;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    iget-object v0, p0, Lgdb;->c:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdb;->c:Lfsy;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    instance-of v3, v0, Lry;

    if-nez v3, :cond_1

    iget-object v3, v0, Lsp;->a:Lrz;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lsp;->a:Lrz;

    iget v3, v3, Lrz;->a:I

    if-lt v3, v6, :cond_2

    :cond_1
    iget-object v3, p2, Lfrx;->d:Lfry;

    invoke-static {p1, v1, v2}, Lfry;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_2
    throw v0
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgdb;->c:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdb;->c:Lfsy;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v5, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v0, p2, Lfrx;->d:Lfry;

    invoke-static {p1, v1, v2}, Lfry;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_3
    .catch Lamq; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lsp; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lgdb;->c:Lfsy;

    if-eqz v1, :cond_0

    instance-of v1, v0, Lrn;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgdb;->c:Lfsy;

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, v5, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v1, v2}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    :cond_4
    instance-of v1, v0, Lry;

    if-nez v1, :cond_5

    iget-object v1, v0, Lsp;->a:Lrz;

    if-eqz v1, :cond_6

    iget-object v0, v0, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    if-lt v0, v6, :cond_6

    :cond_5
    iget-object v0, p0, Lgdb;->c:Lfsy;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lgdb;->c:Lfsy;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lgdb;->c:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdb;->c:Lfsy;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v3, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    return-void
.end method
