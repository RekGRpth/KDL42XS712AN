.class public Lccy;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public a:Lcal;

.field public b:Lcom/google/android/gms/drive/data/view/CustomListView;

.field public c:Lcam;

.field private final d:Lcaj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcaj;->b:Lcaj;

    iput-object v0, p0, Lccy;->d:Lcaj;

    invoke-direct {p0}, Lccy;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcaj;->b:Lcaj;

    iput-object v0, p0, Lccy;->d:Lcaj;

    invoke-direct {p0}, Lccy;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcaj;->b:Lcaj;

    iput-object v0, p0, Lccy;->d:Lcaj;

    invoke-direct {p0}, Lccy;->a()V

    return-void
.end method

.method private a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lccy;->setWillNotDraw(Z)V

    invoke-virtual {p0, p0}, Lccy;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0, p1}, Lcal;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0}, Lcal;->b()V

    return-void
.end method

.method public final i()V
    .locals 4

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0}, Lcal;->e()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Lccy;->getDrawingRect(Landroid/graphics/Rect;)V

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    iget-object v2, p0, Lccy;->a:Lcal;

    invoke-virtual {p0}, Lccy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, Lcal;->a(IILandroid/content/res/Resources;)V

    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    instance-of v0, p2, Lcom/google/android/gms/drive/data/view/CustomListView;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/gms/drive/data/view/CustomListView;

    iput-object p2, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    iget-object v0, p0, Lccy;->d:Lcaj;

    new-instance v1, Lcal;

    iget-object v2, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/data/view/CustomListView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    invoke-direct {v1, v2, v3, p0, v0}, Lcal;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcaj;)V

    iput-object v1, p0, Lccy;->a:Lcal;

    iget-object v0, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    iget-object v1, p0, Lccy;->a:Lcal;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/CustomListView;->a(Lcal;)V

    iget-object v0, p0, Lccy;->a:Lcal;

    iget-object v1, p0, Lccy;->c:Lcam;

    invoke-virtual {v0, v1}, Lcal;->a(Lcam;)V

    iget-object v0, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/data/view/CustomListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :cond_0
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    if-ne p2, v0, :cond_0

    iput-object v1, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    invoke-virtual {p0}, Lccy;->e()V

    iput-object v1, p0, Lccy;->a:Lcal;

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0, p1}, Lcal;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcal;->a(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {p0}, Lccy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcal;->a(IILandroid/content/res/Resources;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lccy;->a:Lcal;

    invoke-virtual {v0, p1}, Lcal;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
