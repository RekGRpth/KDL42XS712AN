.class final Ldkn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:Ldkj;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ldkn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldkn;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/os/Handler;Ldkj;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldkn;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Ldkn;->b:Landroid/os/Handler;

    iput-object p2, p0, Ldkn;->c:Ldkj;

    return-void
.end method

.method static synthetic a(Ldkn;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Ldkn;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic b(Ldkn;)Ldkj;
    .locals 1

    iget-object v0, p0, Ldkn;->c:Ldkj;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Ldkn;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldkp;

    invoke-direct {v1, p0}, Ldkp;-><init>(Ldkn;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(ILjava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldkv;

    invoke-direct {v1, p0, p1, p2, p3}, Ldkv;-><init>(Ldkn;ILjava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Ldjx;)V
    .locals 3

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldko;

    const/4 v2, 0x1

    invoke-direct {v1, p0, p1, v2}, Ldko;-><init>(Ldkn;Ldjx;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Ldkj;)V
    .locals 2

    iget-object v0, p0, Ldkn;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iput-object p1, p0, Ldkn;->c:Ldkj;

    iget-object v0, p0, Ldkn;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldkr;

    invoke-direct {v1, p0, p1, p2}, Ldkr;-><init>(Ldkn;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldks;

    invoke-direct {v1, p0, p1, p2}, Ldks;-><init>(Ldkn;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Ljava/lang/String;[B)V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldkt;

    invoke-direct {v1, p0, p1, p2}, Ldkt;-><init>(Ldkn;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final b()V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldkq;

    invoke-direct {v1, p0}, Ldkq;-><init>(Ldkn;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final b(Ldjx;)V
    .locals 3

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldko;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Ldko;-><init>(Ldkn;Ldjx;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final b(Ljava/lang/String;[B)V
    .locals 2

    iget-object v0, p0, Ldkn;->b:Landroid/os/Handler;

    new-instance v1, Ldku;

    invoke-direct {v1, p0, p1, p2}, Ldku;-><init>(Ldkn;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
