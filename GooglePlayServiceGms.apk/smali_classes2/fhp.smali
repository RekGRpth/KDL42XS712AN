.class public final Lfhp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lfjl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfhp;->a:Landroid/content/Context;

    iget-object v0, p0, Lfhp;->a:Landroid/content/Context;

    invoke-static {v0}, Lfjl;->a(Landroid/content/Context;)Lfjl;

    move-result-object v0

    iput-object v0, p0, Lfhp;->b:Lfjl;

    return-void
.end method

.method public static a(JZ)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DEFAULT-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, "-PAGE"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lfhp;->b:Lfjl;

    invoke-static {p3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfhp;->a:Landroid/content/Context;

    invoke-static {v0}, Lfft;->a(Landroid/content/Context;)Lfft;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lfft;->c(Ljava/lang/String;Z)[B

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lffl;->a:[B

    if-eq v0, v1, :cond_0

    sget-object v1, Lffl;->b:[B

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lfhp;->b:Lfjl;

    invoke-static {p3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2, v0}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Z

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lfhp;->b:Lfjl;

    invoke-static {p3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lfhp;->a:Landroid/content/Context;

    invoke-static {v1}, Lfft;->a(Landroid/content/Context;)Lfft;

    move-result-object v1

    invoke-virtual {v1, p3}, Lfft;->b(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v2, Lffl;->a:[B

    if-eq v1, v2, :cond_2

    sget-object v2, Lffl;->b:[B

    if-eq v1, v2, :cond_2

    iget-object v2, p0, Lfhp;->b:Lfjl;

    invoke-static {p3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, p2, v3, v1}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Z

    goto :goto_0

    :cond_2
    sget-object v2, Lffl;->a:[B

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IZ)[B
    .locals 12

    const/4 v11, 0x0

    iget-object v0, p0, Lfhp;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v3

    invoke-virtual {v3, p2}, Lfbe;->c(Z)J

    move-result-wide v4

    iget-object v0, p0, Lfhp;->a:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    if-nez p2, :cond_1

    sget-object v0, Lfbd;->Z:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_0
    invoke-static {v1}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, p2}, Lfhp;->a(JZ)Ljava/lang/String;

    move-result-object v2

    sub-long v4, v6, v4

    sget-object v0, Lfbd;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v0, v4, v9

    if-gez v0, :cond_0

    iget-object v0, p0, Lfhp;->b:Lfjl;

    invoke-virtual {v0, v2, v11, v8}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-static {v6, v7, p2}, Lfhp;->a(JZ)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v11, v1, v4}, Lfhp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v4, p0, Lfhp;->b:Lfjl;

    invoke-virtual {v4, v0, v11, v8}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lfhp;->b:Lfjl;

    invoke-virtual {v4, v2, v11}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v11}, Lbkm;->c(Ljava/lang/String;)V

    iget-object v2, v3, Lfbe;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {p2}, Lfbe;->b(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    iget-object v2, p0, Lfhp;->b:Lfjl;

    invoke-static {v1}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v11, v1, p1}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lfbd;->aa:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method
