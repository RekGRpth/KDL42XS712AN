.class public final Lcfp;
.super Lcfl;
.source "SourceFile"


# static fields
.field static a:Lcon;

.field public static final b:Landroid/net/Uri;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:J

.field private D:Z

.field private E:Z

.field private F:Ljava/lang/String;

.field private G:J

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Z

.field private L:Ljava/lang/Long;

.field private M:Ljava/lang/Long;

.field private N:Ljava/lang/Long;

.field private O:J

.field private P:Z

.field private Q:Ljava/lang/Long;

.field private R:J

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Z

.field private c:Ljava/lang/String;

.field private d:J

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;

.field private i:Ljava/util/Date;

.field private j:Ljava/util/Date;

.field private k:Ljava/util/Date;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/util/Collection;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcfs;

.field private v:Z

.field private final w:J

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcoo;->a:Lcoo;

    sput-object v0, Lcfp;->a:Lcon;

    const/4 v0, 0x0

    sput-object v0, Lcfp;->b:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>(Lcdu;JLjava/lang/String;Lcgf;)V
    .locals 8

    invoke-interface {p5}, Lcgf;->a()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lcfp;-><init>(Lcdu;JLjava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Lcdu;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcfp;-><init>(Lcdu;JLjava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method private constructor <init>(Lcdu;JLjava/lang/String;Ljava/lang/String;J)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v0

    sget-object v2, Lcfp;->b:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v2}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    const-string v0, ""

    iput-object v0, p0, Lcfp;->g:Ljava/lang/String;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->h:Ljava/util/Date;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->i:Ljava/util/Date;

    iput-object v3, p0, Lcfp;->j:Ljava/util/Date;

    iput-object v3, p0, Lcfp;->l:Ljava/lang/String;

    iput-object v3, p0, Lcfp;->m:Ljava/lang/String;

    iput-object v3, p0, Lcfp;->n:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfp;->o:Ljava/util/Collection;

    iput-boolean v1, p0, Lcfp;->p:Z

    iput-boolean v1, p0, Lcfp;->q:Z

    iput-boolean v1, p0, Lcfp;->r:Z

    iput-boolean v1, p0, Lcfp;->s:Z

    sget-object v0, Lcfs;->a:Lcfs;

    iput-object v0, p0, Lcfp;->u:Lcfs;

    iput-object v3, p0, Lcfp;->M:Ljava/lang/Long;

    iput-object v3, p0, Lcfp;->N:Ljava/lang/Long;

    iput-wide v4, p0, Lcfp;->O:J

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcfp;->R:J

    iput-object p5, p0, Lcfp;->c:Ljava/lang/String;

    iput-wide p2, p0, Lcfp;->d:J

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcfp;->l:Ljava/lang/String;

    iput-wide p6, p0, Lcfp;->O:J

    const-wide/16 v2, 0x1

    cmp-long v0, p6, v2

    if-eqz v0, :cond_0

    const-wide/16 v2, 0x2

    cmp-long v0, p6, v2

    if-eqz v0, :cond_0

    cmp-long v0, p6, v4

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid locality: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcfp;->a:Lcon;

    invoke-interface {v1}, Lcon;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->k:Ljava/util/Date;

    invoke-virtual {p1}, Lcdu;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcfp;->w:J

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>(Lcdu;Landroid/database/Cursor;)V
    .locals 8

    sget-object v0, Lceg;->s:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lceg;->l:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2}, Lcef;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lceg;->k:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcfp;-><init>(Lcdu;JLjava/lang/String;Ljava/lang/String;J)V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v0

    invoke-virtual {v0}, Lcef;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcfp;->d(J)V

    sget-object v0, Lceg;->l:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcfp;->c(Ljava/lang/String;)V

    sget-object v0, Lceg;->a:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->g:Ljava/lang/String;

    new-instance v0, Ljava/util/Date;

    sget-object v1, Lceg;->b:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->h:Ljava/util/Date;

    sget-object v0, Lceg;->B:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->n:Ljava/lang/String;

    :try_start_0
    sget-object v0, Lceg;->C:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lclc;->a(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcfp;->o:Ljava/util/Collection;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljava/util/Date;

    sget-object v1, Lceg;->c:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->i:Ljava/util/Date;

    sget-object v0, Lceg;->d:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcfp;->j:Ljava/util/Date;

    :cond_0
    sget-object v0, Lceg;->e:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcfp;->M:Ljava/lang/Long;

    sget-object v0, Lceg;->g:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcfp;->N:Ljava/lang/Long;

    new-instance v0, Ljava/util/Date;

    sget-object v1, Lceg;->h:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcfp;->k:Ljava/util/Date;

    sget-object v0, Lceg;->i:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->m:Ljava/lang/String;

    sget-object v0, Lceg;->n:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->p:Z

    sget-object v0, Lceg;->f:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->q:Z

    sget-object v0, Lceg;->o:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcfs;->a(J)Lcfs;

    move-result-object v0

    invoke-direct {p0, v0}, Lcfp;->a(Lcfs;)V

    sget-object v0, Lceg;->p:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->r:Z

    sget-object v0, Lceg;->u:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->v:Z

    sget-object v0, Lceg;->q:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->t:Z

    sget-object v0, Lceg;->m:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->P:Z

    sget-object v0, Lceg;->r:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcfp;->k(Z)V

    sget-object v0, Lceg;->t:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcfp;->Q:Ljava/lang/Long;

    sget-object v0, Lceg;->v:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcfp;->R:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcfp;->v:Z

    :cond_1
    sget-object v0, Lceg;->w:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->S:Ljava/lang/String;

    sget-object v0, Lceg;->z:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->x:Ljava/lang/String;

    sget-object v0, Lceg;->D:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->y:Ljava/lang/String;

    sget-object v0, Lceg;->E:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->z:Z

    sget-object v0, Lceg;->F:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->A:Ljava/lang/String;

    sget-object v0, Lceg;->G:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->B:Ljava/lang/String;

    sget-object v0, Lceg;->H:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcfp;->C:J

    sget-object v0, Lceg;->I:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->D:Z

    sget-object v0, Lceg;->J:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->E:Z

    sget-object v0, Lceg;->K:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->F:Ljava/lang/String;

    sget-object v0, Lceg;->L:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcfp;->G:J

    sget-object v0, Lceg;->M:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->H:Ljava/lang/String;

    sget-object v0, Lceg;->N:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->I:Ljava/lang/String;

    sget-object v0, Lceg;->O:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->J:Ljava/lang/String;

    sget-object v0, Lceg;->P:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcfp;->K:Z

    sget-object v0, Lceg;->x:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->T:Ljava/lang/String;

    sget-object v0, Lceg;->y:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfp;->U:Ljava/lang/String;

    sget-object v0, Lceg;->A:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcfp;->L:Ljava/lang/Long;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid JSON ownerNames string array"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;Z)I
    .locals 2

    invoke-static {p0}, Lcfr;->a(Ljava/lang/String;)Lcfr;

    move-result-object v0

    sget-object v1, Lcfr;->d:Lcfr;

    if-ne v0, v1, :cond_0

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcfq;->a(Ljava/lang/String;)Lcfq;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcfq;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcfr;->b()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcfr;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcfp;
    .locals 1

    new-instance v0, Lcfp;

    invoke-direct {v0, p0, p1}, Lcfp;-><init>(Lcdu;Landroid/database/Cursor;)V

    return-object v0
.end method

.method private a(Lcfs;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfs;

    iput-object v0, p0, Lcfp;->u:Lcfs;

    return-void
.end method

.method public static o(Ljava/lang/String;)I
    .locals 2

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcfr;->a(Ljava/lang/String;)Lcfr;

    move-result-object v0

    sget-object v1, Lcfr;->d:Lcfr;

    invoke-virtual {v0, v1}, Lcfr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcfr;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcfq;->a(Ljava/lang/String;)Lcfq;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcfq;->b()I

    move-result v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0b003c    # com.google.android.gms.R.string.drive_document_type_unknown

    goto :goto_0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    sget-object v0, Lcfs;->c:Lcfs;

    iget-object v1, p0, Lcfp;->u:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final B()V
    .locals 1

    sget-object v0, Lcfs;->b:Lcfs;

    invoke-direct {p0, v0}, Lcfp;->a(Lcfs;)V

    return-void
.end method

.method public final C()Z
    .locals 2

    sget-object v0, Lcfs;->b:Lcfs;

    iget-object v1, p0, Lcfp;->u:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final D()V
    .locals 1

    sget-object v0, Lcfs;->a:Lcfs;

    invoke-direct {p0, v0}, Lcfp;->a(Lcfs;)V

    return-void
.end method

.method public final E()Z
    .locals 2

    sget-object v0, Lcfs;->a:Lcfs;

    iget-object v1, p0, Lcfp;->u:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final F()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->t:Z

    return v0
.end method

.method public final G()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->P:Z

    return v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->z:Z

    return v0
.end method

.method public final J()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final L()J
    .locals 2

    iget-wide v0, p0, Lcfp;->C:J

    return-wide v0
.end method

.method public final M()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->D:Z

    return v0
.end method

.method public final N()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->E:Z

    return v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final P()J
    .locals 2

    iget-wide v0, p0, Lcfp;->G:J

    return-wide v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->K:Z

    return v0
.end method

.method public final U()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->s:Z

    return v0
.end method

.method public final V()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcfp;->Q:Ljava/lang/Long;

    return-object v0
.end method

.method public final W()Z
    .locals 2

    iget-object v0, p0, Lcfp;->l:Ljava/lang/String;

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final X()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcfp;->V:Z

    return-void
.end method

.method public final Z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 4

    iget-wide v0, p0, Lcfl;->f:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Lcfp;->C:J

    return-void
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcfp;->g:Ljava/lang/String;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lceg;->a:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->i:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->j:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->k:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcfp;->O:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->n:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->p:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->f:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->q:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->o:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->u:Lcfs;

    invoke-virtual {v4}, Lcfs;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->p:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->r:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->u:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->v:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->q:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->t:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-boolean v0, p0, Lcfp;->P:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sget-object v4, Lceg;->m:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v0, :cond_5

    move-object v0, v3

    :goto_5
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->r:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcfp;->s:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->b:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->c:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->i:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcfp;->j:Ljava/util/Date;

    if-nez v0, :cond_8

    move-object v0, v3

    :goto_7
    sget-object v4, Lceg;->d:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->e:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->M:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->g:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->N:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->h:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->k:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->s:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcfp;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->l:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->B:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfp;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->C:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcfp;->g()Ljava/util/Collection;

    move-result-object v4

    if-nez v4, :cond_9

    :goto_8
    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->t:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->Q:Ljava/lang/Long;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->w:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->D:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->E:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcfp;->z:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->F:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->G:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->H:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v3, p0, Lcfp;->C:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->I:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcfp;->D:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->J:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcfp;->E:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->K:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->L:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v3, p0, Lcfp;->G:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceg;->M:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->N:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->O:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcfp;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->P:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcfp;->K:Z

    if-eqz v3, :cond_d

    :goto_c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lceg;->x:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfp;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceg;->y:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfp;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v0, p0, Lcfp;->R:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_e

    sget-object v2, Lceg;->v:Lceg;

    invoke-virtual {v2}, Lceg;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_d
    iget-object v0, p0, Lcfp;->L:Ljava/lang/Long;

    if-eqz v0, :cond_f

    sget-object v0, Lceg;->A:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfp;->L:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_e
    iget-object v0, p0, Lcfp;->x:Ljava/lang/String;

    if-eqz v0, :cond_10

    sget-object v0, Lceg;->z:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfp;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_f
    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_10
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto :goto_10

    :cond_7
    move v0, v2

    goto/16 :goto_6

    :cond_8
    iget-object v0, p0, Lcfp;->j:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_7

    :cond_9
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    :cond_a
    move v0, v2

    goto/16 :goto_9

    :cond_b
    move v0, v2

    goto/16 :goto_a

    :cond_c
    move v0, v2

    goto/16 :goto_b

    :cond_d
    move v1, v2

    goto/16 :goto_c

    :cond_e
    sget-object v0, Lceg;->v:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_d

    :cond_f
    sget-object v0, Lceg;->A:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_e

    :cond_10
    sget-object v0, Lceg;->z:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_f
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcfp;->M:Ljava/lang/Long;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcfp;->g:Ljava/lang/String;

    return-void

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->getType(I)I

    move-result v2

    const/16 v5, 0xf

    if-eq v2, v5, :cond_1

    const/16 v5, 0x10

    if-eq v2, v5, :cond_1

    const/16 v5, 0xd

    if-eq v2, v5, :cond_1

    const/16 v5, 0xe

    if-eq v2, v5, :cond_1

    const/16 v5, 0xc

    if-eq v2, v5, :cond_1

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_2

    move v2, v3

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    const/16 v2, 0x20

    goto :goto_3

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcfp;->o:Ljava/util/Collection;

    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcfp;->h:Ljava/util/Date;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->v:Z

    return-void
.end method

.method public final aa()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()Lcom/google/android/gms/drive/DriveId;
    .locals 6

    invoke-virtual {p0}, Lcfp;->q()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/DriveId;

    iget-object v1, p0, Lcfp;->c:Ljava/lang/String;

    iget-wide v2, p0, Lcfl;->f:J

    iget-wide v4, p0, Lcfp;->w:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/DriveId;-><init>(Ljava/lang/String;JJ)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/DriveId;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcfl;->f:J

    iget-wide v4, p0, Lcfp;->w:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/DriveId;-><init>(Ljava/lang/String;JJ)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)V
    .locals 0

    iput-wide p1, p0, Lcfp;->G:J

    return-void
.end method

.method public final b(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcfp;->N:Ljava/lang/Long;

    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->g:Ljava/lang/String;

    return-void
.end method

.method public final b(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcfp;->i:Ljava/util/Date;

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->p:Z

    return-void
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcfp;->d:J

    return-wide v0
.end method

.method public final c(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcfp;->L:Ljava/lang/Long;

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcfp;->l:Ljava/lang/String;

    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcfp;->j:Ljava/util/Date;

    return-void
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->q:Z

    return-void
.end method

.method public final d()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcfp;->h:Ljava/util/Date;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->n:Ljava/lang/String;

    return-void
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->r:Z

    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->m:Ljava/lang/String;

    return-void
.end method

.method public final e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->t:Z

    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcfp;->y()Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-virtual {p0}, Lcfp;->W()Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p1, p0, Lcfp;->c:Ljava/lang/String;

    return-void
.end method

.method public final f(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->P:Z

    return-void
.end method

.method public final g()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcfp;->o:Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcfp;->o:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->y:Ljava/lang/String;

    return-void
.end method

.method public final g(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->z:Z

    return-void
.end method

.method public final h()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcfp;->i:Ljava/util/Date;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->A:Ljava/lang/String;

    return-void
.end method

.method public final h(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->D:Z

    return-void
.end method

.method public final i()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcfp;->j:Ljava/util/Date;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->B:Ljava/lang/String;

    return-void
.end method

.method public final i(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->E:Z

    return-void
.end method

.method public final j()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcfp;->M:Ljava/lang/Long;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->F:Ljava/lang/String;

    return-void
.end method

.method public final j(Z)V
    .locals 0

    iput-boolean p1, p0, Lcfp;->K:Z

    return-void
.end method

.method public final k()V
    .locals 4

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    iget-object v1, p0, Lcfp;->Q:Ljava/lang/Long;

    :try_start_0
    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcfp;->Q:Ljava/lang/Long;

    invoke-super {p0}, Lcfl;->k()V

    iget-wide v2, p0, Lcfl;->f:J

    iget-object v0, p0, Lcfp;->e:Lcdu;

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcfp;->V:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcfp;->V:Z

    :cond_0
    iget-object v1, p0, Lcfp;->e:Lcdu;

    invoke-static {p0, v1}, Lcfu;->a(Lcfp;Lcdu;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/events/ChangeEvent;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/events/ChangeEvent;-><init>(Lcom/google/android/gms/drive/DriveId;I)V

    iget-object v0, p0, Lcfp;->e:Lcdu;

    iget-object v0, v0, Lcdu;->b:Lchb;

    invoke-interface {v0, v2}, Lchb;->a(Lcom/google/android/gms/drive/events/DriveEvent;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iput-object v1, p0, Lcfp;->Q:Ljava/lang/Long;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    iput-object v1, p0, Lcfp;->Q:Ljava/lang/Long;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->H:Ljava/lang/String;

    return-void
.end method

.method public final k(Z)V
    .locals 1

    iput-boolean p1, p0, Lcfp;->s:Z

    if-eqz p1, :cond_0

    const-string v0, "unknown_as_place_holder"

    iput-object v0, p0, Lcfp;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 4

    iget-wide v0, p0, Lcfl;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    invoke-super {p0}, Lcfl;->l()V

    iget-object v0, p0, Lcfp;->e:Lcdu;

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfp;->e:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->I:Ljava/lang/String;

    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->J:Ljava/lang/String;

    return-void
.end method

.method public final n()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcfp;->N:Ljava/lang/Long;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v0, p0, Lcfp;->O:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    const-wide/16 v0, 0x2

    iput-wide v0, p0, Lcfp;->O:J

    iput-object p1, p0, Lcfp;->c:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfp;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcfp;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcfp;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->S:Ljava/lang/String;

    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->T:Ljava/lang/String;

    return-void
.end method

.method public final q()Z
    .locals 4

    iget-wide v0, p0, Lcfp;->O:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->U:Ljava/lang/String;

    return-void
.end method

.method public final r()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->p:Z

    return v0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcfp;->x:Ljava/lang/String;

    return-void
.end method

.method public final s()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->q:Z

    return v0
.end method

.method public final t()Z
    .locals 2

    sget-object v0, Lcfs;->a:Lcfs;

    iget-object v1, p0, Lcfp;->u:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "Entry %s (account %d) with resource id: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcfp;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcfp;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    iget-boolean v0, p0, Lcfp;->r:Z

    return v0
.end method

.method public final v()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcfp;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcfp;->l:Ljava/lang/String;

    const-string v2, "application/vnd.google-apps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcfp;->t()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcfp;->v:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final w()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcfp;->L:Ljava/lang/Long;

    return-object v0
.end method

.method public final x()I
    .locals 1

    iget-object v0, p0, Lcfp;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 1

    iget-object v0, p0, Lcfp;->L:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()V
    .locals 1

    sget-object v0, Lcfs;->c:Lcfs;

    invoke-direct {p0, v0}, Lcfp;->a(Lcfs;)V

    return-void
.end method
