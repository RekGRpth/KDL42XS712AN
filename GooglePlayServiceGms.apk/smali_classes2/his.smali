.class public abstract Lhis;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Lidu;

.field protected final c:Ljava/util/Random;

.field d:J

.field e:J

.field private final f:Lhkd;

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lidu;Lhkd;Ljava/util/Random;)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lhis;->d:J

    iput-wide v0, p0, Lhis;->e:J

    iput-object p1, p0, Lhis;->a:Ljava/lang/String;

    iput-object p2, p0, Lhis;->b:Lidu;

    const/16 v0, 0x9

    iput v0, p0, Lhis;->g:I

    iput-object p3, p0, Lhis;->f:Lhkd;

    iput-object p4, p0, Lhis;->c:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method public final a()Lhue;
    .locals 13

    const-wide/16 v11, 0x0

    iget-object v0, p0, Lhis;->f:Lhkd;

    invoke-virtual {v0}, Lhkd;->b()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhis;->a:Ljava/lang/String;

    const-string v1, "Collection not allowed as of now."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lhit;->a:Lhit;

    invoke-static {v0, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v1

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget-object v0, p0, Lhis;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {p0}, Lhis;->b()Lhue;

    move-result-object v1

    iget-object v0, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, v1, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Lhit;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x493e0

    sub-long/2addr v5, v7

    cmp-long v1, v3, v11

    if-eqz v1, :cond_4

    const-wide/32 v7, 0x5265c00

    sub-long v7, v3, v7

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    cmp-long v1, v7, v9

    if-gez v1, :cond_4

    cmp-long v1, v5, v3

    if-gez v1, :cond_4

    iget-wide v5, p0, Lhis;->d:J

    cmp-long v1, v5, v3

    if-eqz v1, :cond_3

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhis;->a:Ljava/lang/String;

    const-string v5, "Reusing alarm time set in previous call."

    invoke-static {v1, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0, v3, v4, v0}, Lhis;->a(JLhit;)Lhue;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v11

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lhis;->a(Ljava/util/Calendar;)Lhue;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lhit;->a:Lhit;

    invoke-static {v0, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method protected final a(JLhit;)Lhue;
    .locals 7

    iget-wide v0, p0, Lhis;->d:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhis;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-object v2, p0, Lhis;->b:Lidu;

    iget v3, p0, Lhis;->g:I

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Lidu;->a(IJLilx;)V

    invoke-virtual {p0, p1, p2}, Lhis;->a(J)V

    iput-wide p1, p0, Lhis;->d:J

    iput-wide v0, p0, Lhis;->e:J

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhis;->a:Ljava/lang/String;

    const-string v3, "Collection scheduled at %s, type: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, p1, p2}, Ljava/util/Date;-><init>(J)V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-wide v0, p0, Lhis;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/util/Calendar;)Lhue;
.end method

.method protected abstract a(J)V
.end method

.method protected abstract b()Lhue;
.end method
