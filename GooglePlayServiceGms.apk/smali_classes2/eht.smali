.class public final Leht;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lizs;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leht;->a:J

    const/4 v0, 0x0

    iput v0, p0, Leht;->b:I

    const/4 v0, -0x1

    iput v0, p0, Leht;->C:I

    return-void
.end method

.method public static a([B)Leht;
    .locals 1

    new-instance v0, Leht;

    invoke-direct {v0}, Leht;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Leht;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v1, p0, Leht;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Leht;->a:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Leht;->b:I

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Leht;->b:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Leht;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Leht;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leht;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-wide v0, p0, Leht;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Leht;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_0
    iget v0, p0, Leht;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Leht;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_1
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leht;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leht;

    iget-wide v2, p0, Leht;->a:J

    iget-wide v4, p1, Leht;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Leht;->b:I

    iget v3, p1, Leht;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget-wide v0, p0, Leht;->a:J

    iget-wide v2, p0, Leht;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leht;->b:I

    add-int/2addr v0, v1

    return v0
.end method
