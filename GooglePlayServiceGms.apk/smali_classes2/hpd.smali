.class public final Lhpd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lhpd;


# instance fields
.field private b:Lfko;

.field private final c:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhpd;->c:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lhpd;
    .locals 2

    const-class v1, Lhpd;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhpd;->a:Lhpd;

    if-nez v0, :cond_0

    new-instance v0, Lhpd;

    invoke-direct {v0, p0}, Lhpd;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhpd;->a:Lhpd;

    :cond_0
    sget-object v0, Lhpd;->a:Lhpd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhpd;->b:Lfko;

    if-nez v0, :cond_0

    new-instance v0, Lfko;

    iget-object v1, p0, Lhpd;->c:Landroid/content/Context;

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lfko;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lhpd;->b:Lfko;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Litx;

    invoke-direct {v0}, Litx;-><init>()V

    iput-object p1, v0, Litx;->a:Ljava/lang/String;

    new-instance v1, Liua;

    invoke-direct {v1}, Liua;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Liua;->a:Ljava/lang/Integer;

    iput-object v0, v1, Liua;->b:Litx;

    new-instance v0, Litz;

    invoke-direct {v0}, Litz;-><init>()V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Litz;->a:Ljava/lang/Integer;

    iput-object v1, v0, Litz;->b:Liua;

    new-instance v1, Lith;

    invoke-direct {v1}, Lith;-><init>()V

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lith;->a:Ljava/lang/Integer;

    iput-object v0, v1, Lith;->c:Litz;

    iget-object v0, p0, Lhpd;->b:Lfko;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpd;->b:Lfko;

    const-string v2, "NlpLog"

    invoke-static {v1}, Lith;->a(Lizs;)[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhpd;->b:Lfko;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpd;->b:Lfko;

    invoke-virtual {v0}, Lfko;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhpd;->b:Lfko;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
