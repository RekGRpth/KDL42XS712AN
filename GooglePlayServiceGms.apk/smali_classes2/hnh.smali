.class public final Lhnh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhms;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    const-wide/32 v0, 0x17d78400

    return-wide v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 11

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lhmt;->a(Ljava/util/List;)[D

    move-result-object v8

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide/16 v2, 0x1

    array-length v9, v8

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_1

    aget-wide v0, v8, v6

    cmpg-double v10, v0, v4

    if-gez v10, :cond_0

    move-wide v4, v0

    :cond_0
    cmpl-double v10, v0, v2

    if-lez v10, :cond_3

    :goto_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-wide v2, v0

    goto :goto_0

    :cond_1
    sub-double v0, v2, v4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    sget-object v0, Lhmz;->e:Lhmz;

    :goto_2
    new-instance v1, Lhmy;

    const/16 v2, 0x64

    invoke-direct {v1, v0, v2}, Lhmy;-><init>(Lhmz;I)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v7

    :cond_2
    sget-object v0, Lhmz;->d:Lhmz;

    goto :goto_2

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method
