.class final Lfgk;
.super Lfgr;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:I

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p6, p0, Lfgk;->c:Ljava/lang/String;

    iput-object p7, p0, Lfgk;->d:Ljava/lang/String;

    iput-object p8, p0, Lfgk;->e:Ljava/lang/String;

    iput p9, p0, Lfgk;->f:I

    iput-object p10, p0, Lfgk;->g:Ljava/lang/String;

    iput-object p11, p0, Lfgk;->h:Ljava/lang/String;

    invoke-direct/range {p0 .. p5}, Lfgr;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 18

    invoke-static/range {p1 .. p1}, Lffe;->a(Landroid/content/Context;)Lffe;

    move-result-object v2

    new-instance v1, Lgex;

    invoke-virtual {v2}, Lffe;->a()Lffg;

    move-result-object v2

    invoke-direct {v1, v2}, Lgex;-><init>(Lbmi;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgk;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfgk;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lfgk;->e:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lfgk;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lfgk;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfgk;->h:Ljava/lang/String;

    invoke-virtual/range {v1 .. v7}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v15

    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    new-instance v17, Lfef;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgk;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfgk;->g:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lfgk;->e:Ljava/lang/String;

    const/4 v11, 0x7

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v14}, Lffu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;ILjava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Lfef;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Lfef;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lfef;->b(I)Lfee;

    move-result-object v2

    invoke-interface {v2}, Lfee;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {v17 .. v17}, Lfef;->b()V

    sget-object v1, Lfdu;->c:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v8

    invoke-virtual {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    const/4 v1, 0x0

    move v7, v1

    :goto_1
    invoke-virtual {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v7, v1, :cond_3

    invoke-virtual {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lgkp;

    invoke-interface {v6}, Lgkp;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v6}, Lgkp;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6}, Lgkp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6}, Lgkp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6}, Lgkp;->h()Lgks;

    move-result-object v5

    invoke-interface {v5}, Lgks;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "person"

    invoke-interface {v6}, Lgkp;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_2
    invoke-static/range {v1 .. v6}, Lfdu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v8, v1}, Lbgt;->a(Ljava/util/HashMap;)Lbgt;

    :cond_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual/range {v17 .. v17}, Lfef;->b()V

    throw v1

    :cond_2
    const/4 v6, 0x2

    goto :goto_2

    :cond_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "pageToken"

    invoke-virtual {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Lbgt;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    return-object v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method
