.class public final Lhgp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:Ljava/util/HashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x11

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lhgp;->a:[C

    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lhgp;->a:[C

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lhgp;->b:Ljava/util/HashSet;

    sget-object v1, Lhgp;->a:[C

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-char v3, v1, v0

    sget-object v4, Lhgp;->b:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 2
        0x53s
        0x43s
        0x4es
        0x4fs
        0x31s
        0x32s
        0x33s
        0x44s
        0x5as
        0x58s
        0x41s
        0x55s
        0x46s
        0x50s
        0x54s
        0x42s
        0x52s
    .end array-data
.end method

.method public static a(C)Z
    .locals 2

    sget-object v0, Lhgp;->b:Ljava/util/HashSet;

    invoke-static {p0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a()[C
    .locals 2

    sget-object v0, Lhgp;->a:[C

    sget-object v1, Lhgp;->a:[C

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([CI)[C

    move-result-object v0

    return-object v0
.end method

.method public static b()I
    .locals 1

    sget-object v0, Lhgp;->a:[C

    array-length v0, v0

    return v0
.end method
