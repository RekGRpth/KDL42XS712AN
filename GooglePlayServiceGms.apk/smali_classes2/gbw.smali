.class public final Lgbw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lfsy;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbw;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgbw;->c:Ljava/lang/String;

    iput-object p3, p0, Lgbw;->b:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lgbw;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgbw;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lfrx;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lgks;

    move-result-object v0

    invoke-interface {v0}, Lgks;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->u()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;ILjava/lang/String;)V

    iget-object v1, p0, Lgbw;->b:Lfsy;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lfsy;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbw;->b:Lfsy;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v6}, Lfsy;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgbw;->b:Lfsy;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6}, Lfsy;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lgbw;->b:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbw;->b:Lfsy;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfsy;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    :cond_0
    return-void
.end method
