.class public abstract Lqu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lqu;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lqu;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lqu;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lqu;->b:Ljava/util/Map;

    invoke-direct {p0, p1}, Lqu;->d(I)V

    invoke-virtual {p0, p1}, Lqu;->b(I)V

    return-void
.end method

.method private d(I)V
    .locals 4

    invoke-virtual {p0}, Lqu;->e()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lqu;->c(I)V

    return-void

    :cond_0
    new-instance v1, Lpr;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The option bit(s) 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " are invalid!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x67

    invoke-direct {v1, v0, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v1
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    iget v0, p0, Lqu;->a:I

    or-int/2addr v0, p1

    :goto_0
    iput v0, p0, Lqu;->a:I

    return-void

    :cond_0
    iget v0, p0, Lqu;->a:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method protected final a(I)Z
    .locals 1

    iget v0, p0, Lqu;->a:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    invoke-direct {p0, p1}, Lqu;->d(I)V

    iput p1, p0, Lqu;->a:I

    return-void
.end method

.method protected c(I)V
    .locals 0

    return-void
.end method

.method protected abstract e()I
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    iget v0, p0, Lqu;->a:I

    check-cast p1, Lqu;

    iget v1, p1, Lqu;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lqu;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lqu;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
