.class Lqj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lqj;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Lqw;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lqj;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lqj;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lqj;->e:Ljava/util/List;

    iput-object v0, p0, Lqj;->f:Ljava/util/List;

    iput-object v0, p0, Lqj;->g:Lqw;

    iput-object p1, p0, Lqj;->b:Ljava/lang/String;

    iput-object p2, p0, Lqj;->c:Ljava/lang/String;

    iput-object p3, p0, Lqj;->g:Lqw;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lqw;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Lqj;
    .locals 3

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    iget-object v2, v0, Lqj;->b:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    const-string v0, "[]"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lqj;->a(Ljava/lang/String;)Lqj;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lpr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate property or field node \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->e:Ljava/util/List;

    :cond_0
    return-void
.end method

.method private s()Z
    .locals 2

    const-string v0, "xml:lang"

    iget-object v1, p0, Lqj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private t()Z
    .locals 2

    const-string v0, "rdf:type"

    iget-object v1, p0, Lqj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private u()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lqj;->e:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    return-object v0
.end method

.method private v()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lqj;->f:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Lqj;
    .locals 1

    iget-object v0, p0, Lqj;->d:Lqj;

    return-object v0
.end method

.method public final a(I)Lqj;
    .locals 2

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lqj;
    .locals 1

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lqj;->a(Ljava/util/List;Ljava/lang/String;)Lqj;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILqj;)V
    .locals 2

    iput-object p0, p2, Lqj;->d:Lqj;

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lqj;)V
    .locals 1

    iget-object v0, p1, Lqj;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lqj;->e(Ljava/lang/String;)V

    iput-object p0, p1, Lqj;->d:Lqj;

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lqw;)V
    .locals 0

    iput-object p1, p0, Lqj;->g:Lqw;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lqj;->h:Z

    return-void
.end method

.method public final b(Ljava/lang/String;)Lqj;
    .locals 1

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    invoke-static {v0, p1}, Lqj;->a(Ljava/util/List;Ljava/lang/String;)Lqj;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->e:Ljava/util/List;

    return-void
.end method

.method public final b(I)V
    .locals 2

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-direct {p0}, Lqj;->r()V

    return-void
.end method

.method public final b(Lqj;)V
    .locals 2

    iget-object v0, p1, Lqj;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lqj;->e(Ljava/lang/String;)V

    iput-object p0, p1, Lqj;->d:Lqj;

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lqj;->i:Z

    return-void
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Lqj;
    .locals 2

    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lqj;->b:Ljava/lang/String;

    return-void
.end method

.method public final c(Lqj;)V
    .locals 1

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lqj;->r()V

    return-void
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Lqj;->j:Z

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    :try_start_0
    new-instance v0, Lqw;

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v1

    iget v1, v1, Lqu;->a:I

    invoke-direct {v0, v1}, Lqw;-><init>(I)V
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    new-instance v1, Lqj;

    iget-object v2, p0, Lqj;->b:Ljava/lang/String;

    iget-object v3, p0, Lqj;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    :try_start_1
    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v1, v0}, Lqj;->a(Lqj;)V
    :try_end_1
    .catch Lpr; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-boolean v0, Lqj;->a:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    goto :goto_0

    :cond_0
    :try_start_2
    invoke-virtual {p0}, Lqj;->i()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v1, v0}, Lqj;->d(Lqj;)V
    :try_end_2
    .catch Lpr; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :cond_1
    return-object v1
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->c:Ljava/lang/String;

    check-cast p1, Lqj;

    iget-object v1, p1, Lqj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lqj;->b:Ljava/lang/String;

    check-cast p1, Lqj;

    iget-object v1, p1, Lqj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lqj;->c:Ljava/lang/String;

    return-void
.end method

.method public final d(Lqj;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p1, Lqj;->b:Ljava/lang/String;

    const-string v3, "[]"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v2}, Lqj;->b(Ljava/lang/String;)Lqj;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Lpr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' qualifier"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    iput-object p0, p1, Lqj;->d:Lqj;

    invoke-virtual {p1}, Lqj;->l()Lqw;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3, v1}, Lqw;->a(IZ)V

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v2

    invoke-virtual {v2, v1}, Lqw;->a(Z)Lqw;

    invoke-direct {p1}, Lqj;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lqj;->g:Lqw;

    invoke-virtual {v2, v1}, Lqw;->b(Z)Lqw;

    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p1}, Lqj;->t()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lqj;->g:Lqw;

    invoke-virtual {v2, v1}, Lqw;->c(Z)Lqw;

    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lqj;->g:Lqw;

    invoke-virtual {v3}, Lqw;->b()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-interface {v2, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lqj;->k:Z

    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0, v1}, Lqw;->a(Z)Lqw;

    invoke-virtual {v0, v1}, Lqw;->b(Z)Lqw;

    invoke-virtual {v0, v1}, Lqw;->c(Z)Lqw;

    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->f:Ljava/util/List;

    return-void
.end method

.method public final e(Lqj;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-direct {p1}, Lqj;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2}, Lqw;->b(Z)Lqw;

    :cond_0
    :goto_0
    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lqj;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Lqw;->a(Z)Lqw;

    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->f:Ljava/util/List;

    :cond_1
    return-void

    :cond_2
    invoke-direct {p1}, Lqj;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lqw;->c(Z)Lqw;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lqj;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/util/Iterator;
    .locals 2

    iget-object v0, p0, Lqj;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lqj;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v0, Lqk;

    invoke-direct {v0, p0, v1}, Lqk;-><init>(Lqj;Ljava/util/Iterator;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lqj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lqj;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lqw;
    .locals 1

    iget-object v0, p0, Lqj;->g:Lqw;

    if-nez v0, :cond_0

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    iput-object v0, p0, Lqj;->g:Lqw;

    :cond_0
    iget-object v0, p0, Lqj;->g:Lqw;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    iget-boolean v0, p0, Lqj;->h:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lqj;->i:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lqj;->j:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lqj;->k:Z

    return v0
.end method

.method public final q()Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {p0}, Lqj;->u()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
