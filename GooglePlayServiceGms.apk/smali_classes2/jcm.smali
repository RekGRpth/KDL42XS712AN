.class public final Ljcm;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([BLjavax/crypto/SecretKey;)Ljcn;
    .locals 4

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    :try_start_0
    invoke-static {p0}, Ljde;->a([B)Ljde;

    move-result-object v0

    sget-object v1, Ljcr;->a:Ljcr;

    sget-object v2, Ljcq;->b:Ljcq;

    invoke-static {v0, p1, v1, p1, v2}, Ljcx;->a(Ljde;Ljava/security/Key;Ljcr;Ljava/security/Key;Ljcq;)Ljdd;

    move-result-object v1

    iget-object v0, v1, Ljdd;->a:Ljdc;

    iget-object v0, v0, Ljdc;->g:Lizf;

    invoke-virtual {v0}, Lizf;->b()[B

    move-result-object v0

    new-instance v2, Ljcl;

    invoke-direct {v2}, Ljcl;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Ljcl;

    iget v2, v0, Ljcl;->b:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Unsupported protocol version"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    :try_start_1
    new-instance v2, Ljcn;

    iget v0, v0, Ljcl;->a:I

    invoke-static {v0}, Ljco;->a(I)Ljco;

    move-result-object v0

    iget-object v1, v1, Ljdd;->b:Lizf;

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    invoke-direct {v2, v0, v1}, Ljcn;-><init>(Ljco;[B)V
    :try_end_1
    .catch Lizj; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v2

    :catch_1
    move-exception v0

    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
