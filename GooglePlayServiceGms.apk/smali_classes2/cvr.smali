.class final Lcvr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Landroid/content/ContentValues;

.field final c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcvr;->a:Ljava/lang/String;

    iput-object p1, p0, Lcvr;->b:Landroid/content/ContentValues;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcvr;->c:Ljava/util/Set;

    iget-object v0, p0, Lcvr;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcvr;->b:Landroid/content/ContentValues;

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()J
    .locals 2

    iget-object v0, p0, Lcvr;->b:Landroid/content/ContentValues;

    const-string v1, "expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(J)Z
    .locals 3

    invoke-direct {p0}, Lcvr;->b()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcvr;->b:Landroid/content/ContentValues;

    const-string v1, "expiration_timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    check-cast p1, Lcvr;

    invoke-direct {p0}, Lcvr;->b()J

    move-result-wide v0

    invoke-direct {p1}, Lcvr;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcvr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcvr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
