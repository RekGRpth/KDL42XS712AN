.class abstract Lbun;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbum;


# instance fields
.field private final a:Ljava/lang/ThreadLocal;

.field private b:Lbuq;

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    const/4 v0, 0x0

    iput-object v0, p0, Lbun;->b:Lbuq;

    const/4 v0, 0x0

    iput v0, p0, Lbun;->c:I

    return-void
.end method

.method private static a(Lbup;)V
    .locals 1

    iget-object v0, p0, Lbup;->b:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0}, Lbun;->d()Lbuq;

    move-result-object v0

    invoke-interface {v0, p1}, Lbuq;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    new-instance v2, Lbup;

    invoke-direct {v2, p1, v0}, Lbup;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    new-instance v3, Lbup;

    invoke-direct {v3, p1, v1}, Lbup;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method private declared-synchronized d()Lbuq;
    .locals 6

    const/16 v1, 0xa

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbun;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbun;->c:I

    if-le v0, v1, :cond_0

    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    const-string v2, "HttpIssuer connection leak, number of active connections exceeded %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lbun;->b:Lbuq;

    const/4 v0, 0x1

    iput v0, p0, Lbun;->c:I

    :cond_0
    iget-object v0, p0, Lbun;->b:Lbuq;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbun;->a()Lbuq;

    move-result-object v0

    iput-object v0, p0, Lbun;->b:Lbuq;

    iget-object v0, p0, Lbun;->b:Lbuq;

    invoke-interface {v0}, Lbuq;->a()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    new-instance v1, Lbuo;

    invoke-direct {v1, p0}, Lbuo;-><init>(Lbun;)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    :cond_1
    iget-object v0, p0, Lbun;->b:Lbuq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbun;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbun;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected abstract a()Lbuq;
.end method

.method public a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 2

    iget-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lbun;->b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v0, "More than 1 active request per thread is not allowed."

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    iget-object v0, v0, Lbup;->c:Ljava/io/IOException;

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method

.method public b()V
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    if-nez v0, :cond_0

    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    const-string v2, "Attempt to close HttpIssuer when no request is executing."

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v4, 0x0

    :try_start_0
    invoke-direct {p0}, Lbun;->e()V

    iget-object v1, v0, Lbup;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v0}, Lbun;->a(Lbup;)V

    iput-boolean v2, v0, Lbup;->d:Z

    :goto_1
    iget-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v6, v0, Lbup;->b:Lorg/apache/http/HttpResponse;

    if-nez v6, :cond_3

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    move v1, v2

    :goto_2
    invoke-static {v0}, Lbun;->a(Lbup;)V

    if-eqz v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/16 v6, 0x3e8

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    const-string v1, "HttpIssuerBase"

    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    const-string v7, "Excessive delay between abort and stream closure: %sms"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-static {v1, v6, v7, v8}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    iput-boolean v2, v0, Lbup;->d:Z

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lbun;->a(Lbup;)V

    iput-boolean v2, v0, Lbup;->d:Z

    throw v1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lbun;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    if-nez v0, :cond_0

    const-string v0, "HttpIssuerBase"

    const-string v1, "Attempt to consume entity of HttpIssuer when no request is executing."

    invoke-static {v0, v1}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lbun;->a(Lbup;)V

    goto :goto_0
.end method
