.class public final Lean;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Ldvn;

.field private final h:Landroid/view/LayoutInflater;

.field private final i:Leab;

.field private final j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldvn;Leab;)V
    .locals 2

    sget v0, Lxb;->j:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ldwx;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Lean;->g:Ldvn;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lean;->h:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leab;

    iput-object v0, p0, Lean;->i:Leab;

    invoke-virtual {p1}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lwy;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lean;->j:I

    return-void
.end method

.method static synthetic a(Lean;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lean;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lean;)Leab;
    .locals 1

    iget-object v0, p0, Lean;->i:Leab;

    return-object v0
.end method

.method static synthetic c(Lean;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lean;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 11

    check-cast p4, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leao;

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v4

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Leee;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v1, v0, Leao;->k:Lean;

    iget-object v1, v1, Lean;->k:Ljava/lang/String;

    invoke-static {v3, v1}, Ldgd;->a(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v8

    add-int v9, v7, v8

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v8, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, v0, Leao;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v4}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->e:I

    invoke-virtual {v0, v1, v2, v3}, Leao;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Leao;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {v4, v1}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Leao;->b:Landroid/widget/TextView;

    iget-object v2, v0, Leao;->c:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v3, 0x0

    iget-object v5, v0, Leao;->c:Landroid/database/CharArrayBuffer;

    iget v5, v5, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v3, v5}, Landroid/widget/TextView;->setText([CII)V

    iget-object v1, v0, Leao;->k:Lean;

    iget-object v1, v1, Lean;->g:Ldvn;

    invoke-interface {p4}, Lcom/google/android/gms/games/multiplayer/Invitation;->h()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Leee;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Leao;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Leao;->k:Lean;

    iget-object v1, v1, Lean;->g:Ldvn;

    sget v2, Lxf;->bl:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-virtual {v1, v2, v3}, Ldvn;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Leao;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v3, v0, Leao;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    iget-object v1, v0, Leao;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    add-int/lit8 v7, v3, 0x1

    if-le v9, v7, :cond_3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    sget v7, Lwz;->f:I

    invoke-virtual {v0, v1, v2, v7}, Leao;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTag(Ljava/lang/Object;)V

    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_2

    :cond_3
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    move-object v1, v2

    goto :goto_3

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v6, v0, Leao;->k:Lean;

    iget v6, v6, Lean;->j:I

    iget v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v1, v3, v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    const/4 v1, 0x5

    if-le v9, v1, :cond_6

    iget-object v1, v0, Leao;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Leao;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leao;->f:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_4
    iget-object v1, v0, Leao;->h:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-interface {v4}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v1

    if-eqz v1, :cond_7

    sget v1, Lxf;->bg:I

    :goto_5
    iget-object v2, v0, Leao;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Leao;->i:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Leao;->j:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_6
    iget-object v1, v0, Leao;->f:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    sget v1, Lxf;->bk:I

    goto :goto_5
.end method

.method public final a(Lbgo;)V
    .locals 1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lbhb;

    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_0
    invoke-super {p0, p1}, Ldwx;->a(Lbgo;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lean;->k:Ljava/lang/String;

    iput-object p2, p0, Lean;->l:Ljava/lang/String;

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lean;->h:Landroid/view/LayoutInflater;

    sget v1, Lxc;->A:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Leao;

    invoke-direct {v1, p0, v0}, Leao;-><init>(Lean;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
