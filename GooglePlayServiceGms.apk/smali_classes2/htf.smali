.class public abstract Lhtf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Collection;


# instance fields
.field protected final b:I

.field protected final c:I

.field protected final d:I

.field protected final e:I

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field protected final i:J

.field protected final j:Ljava/util/Collection;

.field protected final k:I

.field protected final l:I

.field protected m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhtf;->a:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(JIIIILjava/util/Collection;IIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lhtf;->i:J

    iput p3, p0, Lhtf;->k:I

    iput p4, p0, Lhtf;->b:I

    iput p5, p0, Lhtf;->c:I

    iput p6, p0, Lhtf;->d:I

    iput-object p7, p0, Lhtf;->j:Ljava/util/Collection;

    iput p12, p0, Lhtf;->l:I

    iput p8, p0, Lhtf;->e:I

    iput p9, p0, Lhtf;->f:I

    iput p10, p0, Lhtf;->g:I

    iput p11, p0, Lhtf;->h:I

    return-void
.end method

.method public static a(Ljava/lang/String;)Livi;
    .locals 9

    const/4 v0, 0x0

    const/4 v3, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v1, v2

    const/4 v4, 0x4

    if-lt v1, v4, :cond_0

    const/4 v1, 0x0

    :try_start_0
    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x1

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v1, 0x2

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    new-instance v1, Livi;

    sget-object v8, Lihj;->j:Livk;

    invoke-direct {v1, v8}, Livi;-><init>(Livk;)V

    if-eq v4, v3, :cond_6

    const/16 v8, 0xa

    invoke-virtual {v1, v8, v4}, Livi;->e(II)Livi;

    const/4 v8, 0x6

    if-ne v4, v8, :cond_5

    move v2, v3

    :goto_1
    if-eq v2, v3, :cond_2

    const/4 v4, 0x1

    invoke-virtual {v1, v4, v2}, Livi;->e(II)Livi;

    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v7}, Livi;->e(II)Livi;

    if-eq v6, v3, :cond_3

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v6}, Livi;->e(II)Livi;

    :cond_3
    if-eq v5, v3, :cond_4

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v5}, Livi;->e(II)Livi;

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    array-length v4, v2

    const/4 v8, 0x5

    if-ne v4, v8, :cond_0

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_6
    move v2, v3

    goto :goto_1
.end method

.method static a(Ljava/lang/StringBuilder;Lhtf;)Ljava/lang/StringBuilder;
    .locals 4

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-object p0

    :cond_0
    const-string v0, "[cid: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhtf;->b:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " mcc: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhtf;->c:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " mnc: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhtf;->d:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lhtf;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " radioType: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhtf;->k:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " signalStrength: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhtf;->l:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " timeStamp: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lhtf;->i:J

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " neighbors["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iget-object v1, p1, Lhtf;->j:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtf;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    const-string v0, "]]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(Ljava/io/PrintWriter;Lhtf;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lhtf;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Livi;)Ljava/lang/String;
    .locals 7

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v2}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v2}, Livi;->c(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v3}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v3}, Livi;->c(I)I

    move-result v2

    :goto_1
    invoke-virtual {p0, v4}, Livi;->i(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v4}, Livi;->c(I)I

    move-result v3

    :goto_2
    const/16 v4, 0xa

    invoke-virtual {p0, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v4, 0xa

    invoke-virtual {p0, v4}, Livi;->c(I)I

    move-result v4

    :goto_3
    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    return-object v0

    :cond_0
    invoke-virtual {p0, v6}, Livi;->i(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0, v6}, Livi;->c(I)I

    move-result v1

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_2
    move v4, v1

    goto :goto_3

    :cond_3
    move v3, v1

    goto :goto_2

    :cond_4
    move v2, v1

    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method private k()Livi;
    .locals 10

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    const-wide v8, 0x4085b38e38e38e39L    # 694.4444444444445

    const v7, 0x7fffffff

    new-instance v2, Livi;

    sget-object v3, Lihj;->j:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    iget v3, p0, Lhtf;->b:I

    if-eq v3, v7, :cond_0

    const/4 v3, 0x2

    iget v4, p0, Lhtf;->b:I

    invoke-virtual {v2, v3, v4}, Livi;->e(II)Livi;

    :cond_0
    iget v3, p0, Lhtf;->d:I

    if-eq v3, v5, :cond_1

    iget v3, p0, Lhtf;->d:I

    if-eq v3, v7, :cond_1

    const/4 v3, 0x3

    iget v4, p0, Lhtf;->d:I

    invoke-virtual {v2, v3, v4}, Livi;->e(II)Livi;

    :cond_1
    iget v3, p0, Lhtf;->c:I

    if-eq v3, v5, :cond_2

    iget v3, p0, Lhtf;->c:I

    if-eq v3, v7, :cond_2

    const/4 v3, 0x4

    iget v4, p0, Lhtf;->c:I

    invoke-virtual {v2, v3, v4}, Livi;->e(II)Livi;

    :cond_2
    iget v3, p0, Lhtf;->l:I

    const/16 v4, -0x270f

    if-eq v3, v4, :cond_3

    const/4 v3, 0x5

    iget v4, p0, Lhtf;->l:I

    invoke-virtual {v2, v3, v4}, Livi;->e(II)Livi;

    :cond_3
    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0}, Livi;->e(II)Livi;

    :cond_4
    iget v3, p0, Lhtf;->g:I

    iget v4, p0, Lhtf;->h:I

    if-eq v3, v7, :cond_5

    if-eq v4, v7, :cond_5

    const v5, 0x13c680

    if-gt v3, v5, :cond_5

    const v5, -0x13c680

    if-lt v3, v5, :cond_5

    const v3, 0x278d00

    if-gt v4, v3, :cond_5

    const v3, -0x278d00

    if-lt v4, v3, :cond_5

    move v0, v1

    :cond_5
    if-eqz v0, :cond_6

    new-instance v0, Livi;

    sget-object v3, Lihj;->i:Livk;

    invoke-direct {v0, v3}, Livi;-><init>(Livk;)V

    iget v3, p0, Lhtf;->g:I

    int-to-double v3, v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    invoke-virtual {v0, v1, v3}, Livi;->e(II)Livi;

    const/4 v1, 0x2

    iget v3, p0, Lhtf;->h:I

    int-to-double v3, v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    invoke-virtual {v0, v1, v3}, Livi;->e(II)Livi;

    const/16 v1, 0x9

    invoke-virtual {v2, v1, v0}, Livi;->a(ILivi;)V

    :cond_6
    const/16 v0, 0xa

    invoke-virtual {p0}, Lhtf;->j()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Livi;->e(II)Livi;

    invoke-virtual {p0, v2}, Lhtf;->a(Livi;)V

    return-object v2
.end method


# virtual methods
.method public abstract a(JI)Lhtf;
.end method

.method public final a(J)Livi;
    .locals 4

    invoke-virtual {p0}, Lhtf;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Livi;

    sget-object v0, Lihj;->k:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    const/4 v0, 0x2

    iget-wide v2, p0, Lhtf;->i:J

    add-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    const/4 v0, 0x1

    invoke-direct {p0}, Lhtf;->k()Livi;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Livi;->b(ILivi;)Livi;

    iget-object v0, p0, Lhtf;->j:Ljava/util/Collection;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtf;

    const/4 v3, 0x3

    invoke-direct {v0}, Lhtf;->k()Livi;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Livi;->a(ILivi;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method abstract a(Livi;)V
.end method

.method public final a(Livi;J)V
    .locals 3

    invoke-virtual {p0, p2, p3}, Lhtf;->a(J)Livi;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Livi;->b(ILivi;)Livi;

    :cond_0
    return-void
.end method

.method public abstract a(Lhtf;)Z
.end method

.method abstract b()Z
.end method

.method public final b(Lhtf;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lhtf;->b:I

    iget v2, p1, Lhtf;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->c:I

    iget v2, p1, Lhtf;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->d:I

    iget v2, p1, Lhtf;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->k:I

    iget v2, p1, Lhtf;->k:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, p1}, Lhtf;->a(Lhtf;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method abstract c()Ljava/lang/String;
.end method

.method public final d()I
    .locals 2

    iget v0, p0, Lhtf;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lhtf;->f:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lhtf;->e:I

    goto :goto_0
.end method

.method public final e()I
    .locals 2

    iget v0, p0, Lhtf;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lhtf;->d:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lhtf;->c:I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lhtf;

    if-eqz v1, :cond_0

    check-cast p1, Lhtf;

    iget v1, p0, Lhtf;->b:I

    iget v2, p1, Lhtf;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->c:I

    iget v2, p1, Lhtf;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->d:I

    iget v2, p1, Lhtf;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtf;->k:I

    iget v2, p1, Lhtf;->k:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lhtf;->i:J

    return-wide v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lhtf;->k:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lhtf;->l:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lhtf;->b:I

    mul-int/lit16 v0, v0, 0x92b

    iget v1, p0, Lhtf;->c:I

    mul-int/lit16 v1, v1, 0x3a9

    xor-int/2addr v0, v1

    iget v1, p0, Lhtf;->d:I

    mul-int/lit8 v1, v1, 0x65

    xor-int/2addr v0, v1

    iget v1, p0, Lhtf;->k:I

    mul-int/lit16 v1, v1, 0xe3b

    xor-int/2addr v0, v1

    return v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lhtf;->b:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhtf;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhtf;->c:I

    if-ltz v0, :cond_0

    iget v0, p0, Lhtf;->d:I

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lhtf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 5

    const/4 v1, 0x4

    const/4 v0, 0x3

    const/4 v2, -0x1

    iget v3, p0, Lhtf;->k:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v3, p0, Lhtf;->k:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget v3, p0, Lhtf;->k:I

    if-ne v3, v0, :cond_2

    const/4 v0, 0x5

    goto :goto_0

    :cond_2
    iget v0, p0, Lhtf;->k:I

    if-ne v0, v1, :cond_3

    const/4 v0, 0x6

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p0}, Lhtf;->a(Ljava/lang/StringBuilder;Lhtf;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
