.class final Ldkd;
.super Lbqh;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldkb;


# direct methods
.method private constructor <init>(Ldkb;)V
    .locals 0

    iput-object p1, p0, Ldkd;->a:Ldkb;

    invoke-direct {p0}, Lbqh;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldkb;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldkd;-><init>(Ldkb;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_0
    return v0

    :pswitch_1
    iget-object v2, p0, Ldkd;->a:Ldkb;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Ldkb;->e:Ljava/lang/String;

    iget-object v0, p0, Ldkd;->a:Ldkb;

    iget-object v2, p0, Ldkd;->a:Ldkb;

    invoke-static {v2}, Ldkb;->b(Ldkb;)Ldkc;

    move-result-object v2

    invoke-static {v0, v2}, Ldkb;->a(Ldkb;Lbqg;)V

    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldka;

    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to send message %s. Message can be sent only inside a room"

    new-array v5, v1, [Ljava/lang/Object;

    iget v6, v0, Ldka;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Ldkd;->a:Ldkb;

    invoke-static {v2}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v2

    const/16 v3, 0x1b5c

    iget v4, v0, Ldka;->a:I

    iget-object v0, v0, Ldka;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v0}, Ldrt;->a(IILjava/lang/String;)V

    move v0, v1

    goto :goto_0

    :pswitch_4
    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to send message.  Message can be sent only inside a room"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Ldkd;->a:Ldkb;

    invoke-static {v0}, Ldkb;->a(Ldkb;)Ldkk;

    move-result-object v0

    invoke-virtual {v0}, Ldkk;->d()V

    iget-object v0, p0, Ldkd;->a:Ldkb;

    iput-object v1, v0, Ldkb;->e:Ljava/lang/String;

    iget-object v0, p0, Ldkd;->a:Ldkb;

    iput-object v1, v0, Ldkb;->f:Ljava/lang/String;

    return-void
.end method
