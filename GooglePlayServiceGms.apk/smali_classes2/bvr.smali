.class public final Lbvr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcfz;

.field private final b:Lcov;


# direct methods
.method public constructor <init>(Lcfz;Lcon;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbvr;->a:Lcfz;

    new-instance v0, Lcbj;

    invoke-direct {v0, p2}, Lcbj;-><init>(Lcon;)V

    iput-object v0, p0, Lbvr;->b:Lcov;

    return-void
.end method

.method public constructor <init>(Lcoy;)V
    .locals 2

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p1}, Lcoy;->l()Lcon;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvr;-><init>(Lcfz;Lcon;)V

    return-void
.end method


# virtual methods
.method final a(Lcfc;Lclk;Ljava/lang/Boolean;JZZ)V
    .locals 9

    invoke-static {p1}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v3

    iget-object v0, p0, Lbvr;->a:Lcfz;

    iget-object v1, p0, Lbvr;->b:Lcov;

    invoke-interface {v0, v1}, Lcfz;->a(Lcov;)V

    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    :try_start_0
    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {p2}, Lclk;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lclk;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Lcfz;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;

    move-result-object v4

    if-nez p7, :cond_0

    invoke-interface {p2}, Lclk;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcfp;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    move v2, v0

    :goto_0
    invoke-interface {p2}, Lclk;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v4}, Lcfp;->i()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-virtual {v4}, Lcfp;->t()Z

    move-result v0

    invoke-interface {p2}, Lclk;->n()Z

    move-result v5

    if-ne v0, v5, :cond_1

    invoke-virtual {v4}, Lcfp;->A()Z

    move-result v0

    invoke-interface {p2}, Lclk;->s()Z

    move-result v5

    if-eq v0, v5, :cond_7

    :cond_1
    const/4 v0, 0x1

    :goto_2
    or-int/2addr v0, v1

    if-nez v2, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {p2, v4}, Lcfm;->a(Lclk;Lcfp;)V

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcfp;->k(Z)V

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {v4}, Lcfp;->F()Z

    move-result v0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_4

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcfp;->e(Z)V

    :cond_4
    invoke-virtual {v4}, Lcfp;->k()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Lclk;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v5

    :try_start_2
    const-string v5, "DocEntrySynchronizer"

    const-string v6, "App ID has invalid syntax: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbvr;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_5
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    if-eqz p6, :cond_a

    :try_start_3
    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {v0, v4, v1}, Lcfz;->a(Lcfp;Ljava/util/Set;)V

    :cond_9
    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {v0, v3, v4}, Lcfz;->a(Lbsp;Lcfp;)Lcgs;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v1}, Lcgs;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-interface {v2, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Lcgs;->close()V

    throw v0

    :cond_a
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, p0, Lbvr;->a:Lcfz;

    sget-object v2, Lbsj;->a:Lbsj;

    invoke-interface {v0, v4, v5, v6, v2}, Lcfz;->a(Lcfp;JLbsj;)Lbsj;

    goto :goto_5

    :cond_b
    invoke-interface {v1}, Lcgs;->close()V

    invoke-interface {p2}, Lclk;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lbvr;->a:Lcfz;

    const-string v6, "application/vnd.google-apps.folder"

    invoke-interface {v5, v3, v6, v0}, Lcfz;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;

    move-result-object v5

    invoke-virtual {v5}, Lcfp;->W()Z

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Parent is not a collection: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {v5}, Lcfp;->U()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, ""

    invoke-virtual {v5, v0}, Lcfp;->a(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcfp;->k()V

    :cond_d
    :goto_7
    invoke-virtual {v5}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    if-nez v0, :cond_c

    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-virtual {v5}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lcfz;->a(Lcfp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfv;

    move-result-object v0

    invoke-virtual {v0}, Lcfv;->k()V

    goto :goto_6

    :cond_e
    invoke-virtual {v5}, Lcfp;->V()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v6, p4

    if-gez v0, :cond_d

    invoke-virtual {v5}, Lcfp;->k()V

    goto :goto_7

    :cond_f
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    iget-object v2, p0, Lbvr;->a:Lcfz;

    invoke-virtual {v4}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcfz;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_8

    :cond_10
    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v0, p0, Lbvr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    return-void
.end method

.method public final a(Lcfc;Lclk;Ljava/lang/Boolean;Z)V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p4

    invoke-virtual/range {v0 .. v7}, Lbvr;->a(Lcfc;Lclk;Ljava/lang/Boolean;JZZ)V

    return-void
.end method
