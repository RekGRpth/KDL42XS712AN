.class final Lhzf;
.super Lhyx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;

.field private e:Ljava/util/Calendar;

.field private f:Z


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 1

    iput-object p1, p0, Lhzf;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyx;-><init>(Lhyt;Lhys;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lhzf;->e:Ljava/util/Calendar;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhzf;->f:Z

    return-void
.end method

.method private k()Z
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lhzf;->e:Ljava/util/Calendar;

    iget-object v3, p0, Lhzf;->a:Lhyt;

    invoke-static {v3}, Lhyt;->s(Lhyt;)Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lhzf;->e:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v3, 0x7

    if-lt v0, v3, :cond_0

    const/16 v3, 0x17

    if-lt v0, v3, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lhzf;->c:Lhys;

    iget-object v3, v3, Lhys;->f:Lhyi;

    iget-object v3, v3, Lhyi;->g:Lhzo;

    invoke-virtual {v3}, Lhzo;->d()Z

    move-result v4

    iget-object v3, p0, Lhzf;->c:Lhys;

    sget-boolean v5, Lhyb;->a:Z

    if-eqz v5, :cond_1

    const-string v5, "GeofencerStateInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "hasRecentUserInteraction="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v3, Lhys;->k:Lhyn;

    iget-wide v7, v7, Lhyn;->b:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v5, v3, Lhys;->c:Lbpe;

    invoke-interface {v5}, Lbpe;->b()J

    move-result-wide v5

    iget-object v3, v3, Lhys;->k:Lhyn;

    iget-wide v7, v3, Lhyn;->b:J

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x36ee80

    cmp-long v3, v5, v7

    if-gtz v3, :cond_5

    move v3, v2

    :goto_1
    if-nez v3, :cond_6

    move v3, v2

    :goto_2
    sget-boolean v5, Lhyb;->a:Z

    if-eqz v5, :cond_2

    const-string v5, "GeofencerStateMachine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sleepTime="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " stillForALongTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " noActivityForALongTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    move v0, v2

    :goto_3
    iget-boolean v3, p0, Lhzf;->f:Z

    if-eq v3, v0, :cond_8

    iput-boolean v0, p0, Lhzf;->f:Z

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "mightUpdateInDeepStill: mInDeepStill="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lhzf;->f:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_4
    return v2

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    move v3, v1

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_4
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "StillActivityState"

    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lhyx;->a(Landroid/content/Intent;)Z

    invoke-direct {p0}, Lhzf;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lhzf;->b(Z)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected final a(Landroid/util/Pair;)Z
    .locals 1

    invoke-direct {p0}, Lhzf;->k()Z

    invoke-super {p0, p1}, Lhyx;->a(Landroid/util/Pair;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    invoke-super {p0}, Lhyx;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhzf;->f:Z

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-super {p0}, Lhyx;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhzf;->f:Z

    return-void
.end method

.method protected final d()I
    .locals 4

    const/16 v0, 0x12c

    iget-object v1, p0, Lhzf;->c:Lhys;

    invoke-virtual {v1}, Lhys;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhzf;->c:Lhys;

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhzf;->c:Lhys;

    const-wide v2, 0x4072c00000000000L    # 300.0

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lhzf;->f:Z

    if-nez v1, :cond_0

    const/16 v0, 0xb4

    goto :goto_0
.end method

.method protected final e()I
    .locals 4

    const/16 v0, 0x384

    iget-object v1, p0, Lhzf;->c:Lhys;

    invoke-virtual {v1}, Lhys;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3c

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhzf;->c:Lhys;

    const-wide v2, 0x40aa0aaaaaaaaaabL    # 3333.3333333333335

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x708

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhzf;->c:Lhys;

    const-wide v2, 0x4072c00000000000L    # 300.0

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lhzf;->f:Z

    if-nez v1, :cond_0

    const/16 v0, 0x168

    goto :goto_0
.end method

.method protected final f()I
    .locals 2

    iget-object v0, p0, Lhzf;->c:Lhys;

    iget v0, v0, Lhys;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method protected final i()D
    .locals 2

    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    return-wide v0
.end method
