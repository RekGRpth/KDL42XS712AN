.class public final Lebc;
.super Ldwu;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Lebh;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldvn;

.field private final c:Lebh;

.field private final d:Ldws;

.field private final e:Ldwt;

.field private final f:Ldxi;

.field private final g:Ldwp;

.field private final h:Lebf;

.field private final i:I

.field private final j:Ldwp;

.field private final k:Lebf;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lebc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lebc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldvn;Lebh;Ldwt;Ldws;)V
    .locals 9

    const/4 v8, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Ldwu;-><init>()V

    iput-object p1, p0, Lebc;->b:Ldvn;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebh;

    iput-object v0, p0, Lebc;->c:Lebh;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwt;

    iput-object v0, p0, Lebc;->e:Ldwt;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldws;

    iput-object v0, p0, Lebc;->d:Ldws;

    new-instance v0, Ldxi;

    sget v2, Lwz;->B:I

    sget v3, Lxf;->aW:I

    move-object v1, p1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lebc;->f:Ldxi;

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lebc;->g:Ldwp;

    iget-object v0, p0, Lebc;->g:Ldwp;

    sget v1, Lxf;->aT:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    new-instance v0, Lebf;

    sget v1, Lxb;->h:I

    invoke-direct {v0, p1, p0, v1}, Lebf;-><init>(Ldvn;Lebh;I)V

    iput-object v0, p0, Lebc;->h:Lebf;

    iget-object v0, p0, Lebc;->h:Lebf;

    invoke-virtual {v0}, Lebf;->b()V

    iget-object v0, p0, Lebc;->h:Lebf;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Lebc;->h:Lebf;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Lebc;->i:I

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lebc;->j:Ldwp;

    iget-object v0, p0, Lebc;->j:Ldwp;

    sget v1, Lxf;->aV:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    new-instance v0, Lebf;

    sget v1, Lxb;->i:I

    invoke-direct {v0, p1, p0, v1}, Lebf;-><init>(Ldvn;Lebh;I)V

    iput-object v0, p0, Lebc;->k:Lebf;

    iget-object v0, p0, Lebc;->k:Lebf;

    invoke-virtual {v0}, Lebf;->b()V

    iget-object v0, p0, Lebc;->k:Lebf;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Lebc;->k:Lebf;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Lebc;->l:I

    new-array v0, v8, [Landroid/widget/BaseAdapter;

    iget-object v1, p0, Lebc;->f:Ldxi;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    iget-object v2, p0, Lebc;->g:Ldwp;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lebc;->h:Lebf;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lebc;->j:Ldwp;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lebc;->k:Lebf;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lebc;->a([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0}, Lebc;->a()V

    return-void
.end method

.method private a(Ldwp;II)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-lez p2, :cond_4

    invoke-virtual {p1, v1}, Ldwp;->b(Z)V

    iget-object v2, p0, Lebc;->g:Ldwp;

    if-ne p1, v2, :cond_2

    if-le p2, p3, :cond_1

    const-string v0, "giftsButton"

    invoke-virtual {p1, p0, v0}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    sub-int v0, p2, p3

    invoke-virtual {p1, v0}, Ldwp;->b(I)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget v0, Lxf;->aU:I

    const-string v2, "openAllButton"

    invoke-virtual {p1, p0, v0, v2}, Ldwp;->a(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lebc;->j:Ldwp;

    if-ne p1, v2, :cond_0

    const-string v2, "wishesButton"

    invoke-virtual {p1, p0, v2}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    if-le p2, p3, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p1, v0}, Ldwp;->a(Z)V

    sub-int v0, p2, p3

    invoke-virtual {p1, v0}, Ldwp;->b(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v0}, Ldwp;->b(Z)V

    move v1, v0

    goto :goto_0
.end method

.method private b()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lebc;->h:Lebf;

    invoke-virtual {v0}, Lebf;->d()I

    move-result v3

    iget-object v0, p0, Lebc;->g:Ldwp;

    iget v4, p0, Lebc;->i:I

    invoke-direct {p0, v0, v3, v4}, Lebc;->a(Ldwp;II)Z

    move-result v0

    iget-object v4, p0, Lebc;->k:Lebf;

    invoke-virtual {v4}, Lebf;->d()I

    move-result v4

    iget-object v5, p0, Lebc;->j:Ldwp;

    iget v6, p0, Lebc;->l:I

    invoke-direct {p0, v5, v4, v6}, Lebc;->a(Ldwp;II)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v4, p0, Lebc;->g:Ldwp;

    if-le v3, v2, :cond_2

    move v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Ldwp;->a(Z)V

    iget-object v3, p0, Lebc;->f:Ldxi;

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Ldxi;->b(Z)V

    invoke-virtual {p0}, Lebc;->notifyDataSetChanged()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lebc;->h:Lebf;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Lebc;->k:Lebf;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Lebc;->f:Ldxi;

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    iget-object v0, p0, Lebc;->j:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    iget-object v0, p0, Lebc;->k:Lebf;

    invoke-virtual {v0}, Lebf;->f()V

    iget-object v0, p0, Lebc;->g:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    return-void
.end method

.method public final a(Lbdu;)V
    .locals 2

    iget-object v0, p0, Lebc;->b:Ldvn;

    iget-object v1, p0, Lebc;->b:Ldvn;

    invoke-virtual {v1}, Ldvn;->l()Z

    move-result v1

    invoke-static {p1, v0, v1}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcte;->o:Ldlf;

    const v1, 0xffff

    invoke-interface {v0, p1, v1}, Ldlf;->a(Lbdu;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    invoke-static {p1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lebc;->h:Lebf;

    iput-object v0, v1, Lebf;->g:Ljava/lang/String;

    iget-object v1, p0, Lebc;->k:Lebf;

    iput-object v0, v1, Lebf;->g:Ljava/lang/String;

    sget-object v0, Lcte;->n:Lctp;

    const/4 v1, 0x4

    invoke-interface {v0, p1, v1}, Lctp;->a(Lbdu;I)V

    goto :goto_0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lebc;->b:Ldvn;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcte;->o:Ldlf;

    const v1, 0xffff

    invoke-interface {v0, p1, p2, p3, v1}, Ldlf;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Lebc;->h:Lebf;

    iput-object p4, v0, Lebf;->g:Ljava/lang/String;

    iget-object v0, p0, Lebc;->k:Lebf;

    iput-object p4, v0, Lebf;->g:Ljava/lang/String;

    sget-object v0, Lcte;->n:Lctp;

    const/4 v1, 0x4

    invoke-interface {v0, p1, p2, v1}, Lctp;->a(Lbdu;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final synthetic a(Lbek;)V
    .locals 5

    check-cast p1, Ldlh;

    invoke-interface {p1}, Ldlh;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ldlh;->a(I)Ldkz;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Ldlh;->a(I)Ldkz;

    move-result-object v2

    :try_start_0
    iget-object v3, p0, Lebc;->b:Ldvn;

    invoke-virtual {v3, v0}, Ldvn;->d(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ldkz;->b()V

    invoke-virtual {v2}, Ldkz;->b()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ldkz;->a()I

    move-result v3

    invoke-virtual {v2}, Ldkz;->a()I

    move-result v4

    add-int/2addr v3, v4

    if-nez v3, :cond_2

    invoke-static {v0}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lebc;->d:Ldws;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lebc;->d:Ldws;

    invoke-interface {v0}, Ldws;->z_()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-virtual {v1}, Ldkz;->b()V

    invoke-virtual {v2}, Ldkz;->b()V

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance v0, Leax;

    invoke-direct {v0, v1}, Leax;-><init>(Lbgo;)V

    iget-object v3, p0, Lebc;->h:Lebf;

    invoke-virtual {v3, v0}, Lebf;->a(Leax;)V

    new-instance v0, Leax;

    invoke-direct {v0, v2}, Leax;-><init>(Lbgo;)V

    iget-object v3, p0, Lebc;->k:Lebf;

    invoke-virtual {v3, v0}, Lebf;->a(Leax;)V

    iget-object v0, p0, Lebc;->d:Ldws;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lebc;->d:Ldws;

    invoke-interface {v0}, Ldws;->y_()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    invoke-virtual {v1}, Ldkz;->b()V

    invoke-virtual {v2}, Ldkz;->b()V

    invoke-direct {p0}, Lebc;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldkz;->b()V

    invoke-virtual {v2}, Ldkz;->b()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 1

    iget-object v0, p0, Lebc;->c:Lebh;

    invoke-interface {v0, p1}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    invoke-direct {p0}, Lebc;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lebc;->c:Lebh;

    invoke-interface {v0, p1, p2}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lebc;->h:Lebf;

    invoke-virtual {v0, p1, p2, p3}, Lebf;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V

    iget-object v0, p0, Lebc;->k:Lebf;

    invoke-virtual {v0, p1, p2, p3}, Lebf;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V

    invoke-direct {p0}, Lebc;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    iget-object v0, p0, Lebc;->c:Lebh;

    invoke-interface {v0, p1}, Lebh;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    invoke-direct {p0}, Lebc;->b()V

    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    iget-object v0, p0, Lebc;->c:Lebh;

    invoke-interface {v0, p1}, Lebh;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    return-void
.end method

.method public final b_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lebc;->c:Lebh;

    invoke-interface {v0, p1}, Lebh;->b_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    const-string v1, "openAllButton"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lebc;->h:Lebf;

    invoke-virtual {v0}, Lebf;->e()Lbgo;

    move-result-object v0

    invoke-static {v0}, Leay;->a(Lbgo;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    iget-object v1, p0, Lebc;->c:Lebh;

    invoke-interface {v1, v0}, Lebh;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lebc;->e:Ldwt;

    invoke-interface {v1, v0}, Ldwt;->d_(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lebc;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
