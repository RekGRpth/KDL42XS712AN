.class public final Lbwi;
.super Lbvv;
.source "SourceFile"


# instance fields
.field private b:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lbvs;J)V
    .locals 1

    invoke-direct {p0, p1}, Lbvv;-><init>(Lbvs;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lbwi;->b:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public final a(Lclm;Lclk;)V
    .locals 6

    invoke-interface {p2}, Lclk;->p()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v1}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget-object v2, p0, Lbwi;->b:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, Lbwi;->b:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lbvv;->a(Lclm;Lclk;)V

    return-void

    :catch_0
    move-exception v0

    const-string v2, "UpdatedDateMonitorProcessor"

    const-string v3, "Error parsing date %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lbwi;->b:Ljava/util/Date;

    :cond_0
    invoke-super {p0, p1}, Lbvv;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b()Ljava/util/Date;
    .locals 3

    iget-object v0, p0, Lbwi;->b:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lbwi;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method
