.class public final Lceh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcek;

.field public final c:Lcdt;

.field public final d:Lcdp;

.field public final e:Z

.field public final f:Ljava/util/Set;

.field public final g:Ljava/lang/Object;

.field public final h:Z

.field public final i:Lcej;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcek;ZLjava/util/Set;ZLjava/lang/Object;Lcdt;Lcdp;Lcej;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p9}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lceh;->a:Ljava/lang/String;

    iput-object p2, p0, Lceh;->b:Lcek;

    invoke-static {p4}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lceh;->f:Ljava/util/Set;

    iput-boolean p5, p0, Lceh;->h:Z

    iput-object p6, p0, Lceh;->g:Ljava/lang/Object;

    iput-object p7, p0, Lceh;->c:Lcdt;

    iput-object p8, p0, Lceh;->d:Lcdp;

    iput-object p9, p0, Lceh;->i:Lcej;

    iput-boolean p3, p0, Lceh;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcek;ZLjava/util/Set;ZLjava/lang/Object;Lcdt;Lcdp;Lcej;B)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lceh;-><init>(Ljava/lang/String;Lcek;ZLjava/util/Set;ZLjava/lang/Object;Lcdt;Lcdp;Lcej;)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "FieldDefinition[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lceh;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lceh;->b:Lcek;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
