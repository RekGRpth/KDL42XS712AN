.class public final Ljbt;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Ljbt;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbt;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbt;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbt;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbt;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbt;->j:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Ljbt;->m:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbt;->m:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbt;->b()I

    :cond_0
    iget v0, p0, Ljbt;->m:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->a:Z

    iput-object v0, p0, Ljbt;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->c:Z

    iput-object v0, p0, Ljbt;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->k:Z

    iput-object v0, p0, Ljbt;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->e:Z

    iput-object v0, p0, Ljbt;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->g:Z

    iput-object v0, p0, Ljbt;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Ljbt;->i:Z

    iput-object v0, p0, Ljbt;->j:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljbt;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Ljbt;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iget-object v1, p0, Ljbt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Ljbt;->k:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iget-object v1, p0, Ljbt;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Ljbt;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    iget-object v1, p0, Ljbt;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Ljbt;->g:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    iget-object v1, p0, Ljbt;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Ljbt;->i:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    iget-object v1, p0, Ljbt;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_5
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbt;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbt;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljbt;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v2, p0, Ljbt;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljbt;->k:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    iget-object v2, p0, Ljbt;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljbt;->e:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    iget-object v2, p0, Ljbt;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljbt;->g:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    iget-object v2, p0, Ljbt;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljbt;->i:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    iget-object v2, p0, Ljbt;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Ljbt;->m:I

    return v0
.end method
