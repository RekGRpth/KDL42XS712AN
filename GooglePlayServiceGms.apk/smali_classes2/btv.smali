.class public final Lbtv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Ljava/lang/String;


# instance fields
.field private final c:Lbtd;

.field private final d:Landroid/content/Context;

.field private final e:Lcow;

.field private f:J

.field private g:Ljava/io/BufferedInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lbqs;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2/files?uploadType=resumable&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbtv;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lbqs;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2/files/%s?uploadType=resumable&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbtv;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcoy;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbtv;->f:J

    const/4 v0, 0x0

    iput-object v0, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {p1}, Lcoy;->m()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lbtv;->c:Lbtd;

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbtv;->d:Landroid/content/Context;

    new-instance v0, Lbtx;

    invoke-direct {v0}, Lbtx;-><init>()V

    iput-object v0, p0, Lbtv;->e:Lcow;

    return-void
.end method

.method private static a(Ljava/lang/String;)Lbtz;
    .locals 3

    :try_start_0
    invoke-static {p0}, Lbtz;->a(Ljava/lang/String;)Lbtz;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lbub;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to upload file: invalid byte range returned by server. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbub;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 3

    new-instance v0, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    :try_start_0
    new-instance v1, Lbnl;

    invoke-direct {v1}, Lbnl;-><init>()V

    invoke-virtual {v1, p0, v0}, Lbnl;->a(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lbub;

    const-string v2, "Failed to process contents"

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lbub;

    const-string v2, "Failed to process contents"

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lbtv;->a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lbty;)Ljava/net/HttpURLConnection;
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    :try_start_0
    iget-boolean v0, p1, Lbty;->d:Z

    if-eqz v0, :cond_1

    sget-object v0, Lbtv;->a:Ljava/lang/String;

    invoke-static {v0}, Lbtv;->b(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "Content-Type"

    const-string v2, "application/json; charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Host"

    const-string v2, "www.googleapis.com"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "X-Upload-Content-Type"

    iget-object v2, p1, Lbty;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "X-Upload-Content-Length"

    iget-wide v2, p1, Lbty;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbtv;->d:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lbug;->a(Ljava/net/HttpURLConnection;Lbty;Landroid/content/Context;)V

    iget-object v1, p1, Lbty;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    iget-object v1, p1, Lbty;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v1}, Lcjc;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    const/4 v2, 0x0

    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-direct {v4, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    :try_start_4
    sget-object v0, Lbtv;->b:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbtv;->b(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lbub;

    const-string v2, "Failed to start session"

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_5
    new-instance v2, Lbub;

    const-string v3, "Failed to upload metadata"

    invoke-direct {v2, v3, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_2

    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_2
    :goto_4
    throw v0

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;J)V
    .locals 8

    const/4 v7, 0x0

    const-wide/16 v0, 0x0

    const/16 v2, 0x4000

    new-array v2, v2, [B

    :goto_0
    cmp-long v3, v0, p2

    if-gez v3, :cond_0

    const-wide/16 v3, 0x4000

    sub-long v5, p2, v0

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-virtual {p0, v2, v7, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    invoke-virtual {p1, v2, v7, v3}, Ljava/io/OutputStream;->write([BII)V

    int-to-long v3, v3

    add-long/2addr v0, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;Lbty;)V
    .locals 2

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x190

    if-eq v0, v1, :cond_0

    const/16 v1, 0x194

    if-eq v0, v1, :cond_0

    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbty;->a(Ljava/lang/String;)V

    new-instance v0, Lbub;

    const-string v1, "Url expired."

    invoke-direct {v0, v1}, Lbub;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private static b(Ljava/net/HttpURLConnection;)J
    .locals 5

    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0x134

    if-eq v2, v3, :cond_0

    new-instance v0, Lbub;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbub;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v2, "Range"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    return-wide v0

    :cond_1
    invoke-static {v2}, Lbtv;->a(Ljava/lang/String;)Lbtz;

    move-result-object v2

    iget-wide v3, v2, Lbtz;->c:J

    cmp-long v0, v3, v0

    if-eqz v0, :cond_2

    new-instance v0, Lbub;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, Lbub;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-wide v0, v2, Lbtz;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private b(Lbty;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 16

    move-object/from16 v0, p1

    iget-wide v1, v0, Lbty;->f:J

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbtv;->f:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x40000

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v4, v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lbtv;->e:Lcow;

    invoke-interface {v1}, Lcow;->a()Lcov;

    move-result-object v5

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    :try_start_0
    sget-object v1, Lbqs;->J:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v3, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lbtv;->g:Ljava/io/BufferedInputStream;

    const/high16 v6, 0x40000

    invoke-virtual {v1, v6}, Ljava/io/BufferedInputStream;->mark(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/net/URL;

    move-object/from16 v0, p1

    iget-object v6, v0, Lbty;->h:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    const-string v6, "Content-Type"

    move-object/from16 v0, p1

    iget-object v7, v0, Lbty;->g:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbtv;->d:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0, v6}, Lbug;->a(Ljava/net/HttpURLConnection;Lbty;Landroid/content/Context;)V

    const-string v6, "PUT"

    invoke-virtual {v1, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    if-lez v4, :cond_3

    const-string v6, "Content-Range"

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v8, "bytes %d-%d/%d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-wide v11, v0, Lbtv;->f:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-wide v11, v0, Lbtv;->f:J

    int-to-long v13, v4

    add-long/2addr v11, v13

    const-wide/16 v13, 0x1

    sub-long/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p1

    iget-wide v11, v0, Lbty;->f:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v1, v4}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    int-to-long v8, v4

    invoke-static {v6, v7, v8, v9}, Lbtv;->a(Ljava/io/InputStream;Ljava/io/OutputStream;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lbtv;->a(Ljava/net/HttpURLConnection;Lbty;)V

    invoke-static {v1}, Lbtv;->c(Ljava/net/HttpURLConnection;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_5

    move-object v2, v1

    :cond_0
    if-eqz v2, :cond_1

    :try_start_3
    invoke-static {v2}, Lbtv;->c(Ljava/net/HttpURLConnection;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_1
    new-instance v1, Lbub;

    const-string v3, "Failed to upload. Ran out of tries"

    invoke-direct {v1, v3}, Lbub;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v1

    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v1

    :cond_3
    const/4 v6, 0x1

    :try_start_4
    invoke-virtual {v1, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v1, v2

    :goto_3
    move-object v2, v1

    :goto_4
    :try_start_5
    const-string v1, "ResumableUploader"

    const-string v6, "Upload attempt failed, retrying."

    invoke-static {v1, v6}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->reset()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-interface {v5}, Lcov;->d()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    :cond_5
    move-object v2, v1

    goto :goto_4

    :cond_6
    :try_start_7
    invoke-static {v2}, Lbtv;->a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/internal/model/File;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_8

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    :goto_6
    return-object v1

    :cond_8
    :try_start_8
    invoke-static {v2}, Lbtv;->b(Ljava/net/HttpURLConnection;)J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-wide v7, v0, Lbtv;->f:J

    int-to-long v3, v4

    add-long/2addr v3, v7

    cmp-long v1, v3, v5

    if-eqz v1, :cond_9

    new-instance v1, Lbub;

    const-string v3, "Server did not receive the correct number of bytes."

    invoke-direct {v1, v3}, Lbub;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object/from16 v0, p0

    iput-wide v5, v0, Lbtv;->f:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a
    const/4 v1, 0x0

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catchall_1
    move-exception v2

    move-object v15, v2

    move-object v2, v1

    move-object v1, v15

    goto :goto_2

    :catch_2
    move-exception v2

    goto :goto_3
.end method

.method private static b(Ljava/lang/String;)Ljava/net/URL;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lbub;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c(Lbty;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 5

    new-instance v0, Ljava/net/URL;

    iget-object v1, p1, Lbty;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    :try_start_0
    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v1, "Content-Range"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes */"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p1, Lbty;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    invoke-static {v0}, Lbtv;->a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/internal/model/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-static {v0, p1}, Lbtv;->a(Ljava/net/HttpURLConnection;Lbty;)V

    invoke-static {v0}, Lbtv;->b(Ljava/net/HttpURLConnection;)J

    move-result-wide v1

    iput-wide v1, p0, Lbtv;->f:J

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    iget-wide v2, p0, Lbtv;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/io/BufferedInputStream;->skip(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v1
.end method

.method private static c(Ljava/net/HttpURLConnection;)Z
    .locals 2

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_0

    const/16 v1, 0x257

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lbty;Lbtw;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lbtv;->c:Lbtd;

    const-string v1, "upload"

    const-string v3, "uploadStarted"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v3, v4, v5}, Lbtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p1, Lbty;->a:Ljava/io/InputStream;

    const/high16 v3, 0x40000

    invoke-direct {v0, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    iget-object v0, p1, Lbty;->h:Ljava/lang/String;

    if-nez v0, :cond_6

    const-string v0, "ResumableUploader"

    const-string v1, "Starting from the beginning"

    invoke-static {v0, v1}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {p2, v0}, Lbtw;->a(Lcom/google/android/gms/drive/DriveId;)V
    :try_end_0
    .catch Lbub; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbuf; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0, p1}, Lbtv;->a(Lbty;)Ljava/net/HttpURLConnection;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    :try_start_2
    invoke-static {v1, p1}, Lbtv;->a(Ljava/net/HttpURLConnection;Lbty;)V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_2

    new-instance v2, Lbub;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to upload item: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to upload "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbub;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :goto_0
    :try_start_3
    new-instance v2, Lbub;

    const-string v3, "Failed to start request"

    invoke-direct {v2, v3, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    throw v0
    :try_end_4
    .catch Lbub; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lbuf; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    :cond_1
    throw v0

    :cond_2
    :try_start_6
    const-string v0, "Location"

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lbub;

    const-string v2, "Unable to upload item: Server upload URI invalid."

    invoke-direct {v0, v2}, Lbub;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1, v0}, Lbty;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v1, :cond_5

    :try_start_7
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v2

    :cond_4
    :goto_2
    if-nez v0, :cond_9

    invoke-static {}, Lbta;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lbuf;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbuf;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lbub; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lbuf; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catch_2
    move-exception v0

    :try_start_8
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_5
    move-object v0, v2

    goto :goto_2

    :cond_6
    :try_start_9
    const-string v0, "ResumableUploader"

    const-string v1, "Starting from status request"

    invoke-static {v0, v1}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lbtv;->c(Lbty;)Lcom/google/android/gms/drive/internal/model/File;
    :try_end_9
    .catch Lbub; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lbuf; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    :cond_7
    :goto_3
    return-object v0

    :cond_8
    :try_start_a
    invoke-direct {p0, p1}, Lbtv;->b(Lbty;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v6

    iget-object v1, p1, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    iget-wide v2, p0, Lbtv;->f:J

    iget-wide v4, p1, Lbty;->f:J

    move-object v0, p2

    invoke-interface/range {v0 .. v5}, Lbtw;->a(Lcom/google/android/gms/drive/DriveId;JJ)V

    move-object v0, v6

    goto :goto_2

    :cond_9
    iget-object v1, p1, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {p2, v1}, Lbtw;->b(Lcom/google/android/gms/drive/DriveId;)V
    :try_end_a
    .catch Lbub; {:try_start_a .. :try_end_a} :catch_1
    .catch Lbuf; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lbtv;->g:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :catch_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0
.end method
