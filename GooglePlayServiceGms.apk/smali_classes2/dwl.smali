.class final Ldwl;
.super Ldwj;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldvv;

.field private final g:I


# direct methods
.method public constructor <init>(Ldvv;I)V
    .locals 2

    const/4 v1, 0x1

    iput-object p1, p0, Ldwl;->a:Ldvv;

    invoke-static {p2}, Ldvv;->d(I)I

    move-result v0

    invoke-direct {p0, p1, v0, v1, v1}, Ldwj;-><init>(Ldvv;IIZ)V

    iput p2, p0, Ldwl;->g:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0082    # com.google.android.gms.R.id.summary

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, p0, Ldwl;->g:I

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldwl;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v2, 0x7f0b020b    # com.google.android.gms.R.string.games_gcore_multiplayer_notifications

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0b020c    # com.google.android.gms.R.string.games_gcore_multiplayer_notifications_summary

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const v0, 0x7f0a01ae    # com.google.android.gms.R.id.widget_frame

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget v1, p0, Ldwl;->g:I

    invoke-static {v1}, Ldec;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldwl;->a:Ldvv;

    invoke-static {v2}, Ldvv;->a(Ldvv;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void

    :pswitch_1
    const v2, 0x7f0b020d    # com.google.android.gms.R.string.games_gcore_request_notifications

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0b020e    # com.google.android.gms.R.string.games_gcore_request_notifications_summary

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
