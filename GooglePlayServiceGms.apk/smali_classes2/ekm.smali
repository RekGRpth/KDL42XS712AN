.class final Lekm;
.super Leku;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lels;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Lejm;


# direct methods
.method constructor <init>(Lejm;Ljava/lang/String;Lels;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lekm;->e:Lejm;

    iput-object p2, p0, Lekm;->a:Ljava/lang/String;

    iput-object p3, p0, Lekm;->b:Lels;

    iput-object p4, p0, Lekm;->c:Ljava/util/List;

    iput-object p5, p0, Lekm;->d:Ljava/util/List;

    invoke-direct {p0, p1}, Leku;-><init>(Lejm;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lekm;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lekm;->e:Lejm;

    iget-object v0, v0, Lejm;->g:Leiy;

    iget-object v1, p0, Lekm;->b:Lels;

    iget-object v2, p0, Lekm;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lekm;->e:Lejm;

    iget-object v0, v0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1}, Leiy;->e(Ljava/lang/String;)Lema;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v5}, Lema;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregister: cannot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lema;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when previously "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lema;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lekm;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lekm;->d:Ljava/util/List;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v0, v0, Lehh;->f:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lekm;->c:Ljava/util/List;

    iget-object v2, p0, Lekm;->e:Lejm;

    iget-object v3, p0, Lekm;->b:Lels;

    invoke-virtual {v2, v1, v3}, Lejm;->a(Ljava/lang/String;Lels;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lekm;->e:Lejm;

    iget-object v0, v0, Lejm;->g:Leiy;

    iget-object v1, p0, Lekm;->b:Lels;

    invoke-virtual {v0, v1}, Leiy;->a(Lels;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lekm;->e:Lejm;

    iget-object v1, v1, Lejm;->g:Leiy;

    invoke-virtual {v1, v0}, Leiy;->e(Ljava/lang/String;)Lema;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, v5}, Lema;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lekm;->d:Ljava/util/List;

    invoke-virtual {v1}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v1, v1, Lehh;->f:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lekm;->c:Ljava/util/List;

    iget-object v3, p0, Lekm;->e:Lejm;

    iget-object v4, p0, Lekm;->b:Lels;

    invoke-virtual {v3, v0, v4}, Lejm;->a(Ljava/lang/String;Lels;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
