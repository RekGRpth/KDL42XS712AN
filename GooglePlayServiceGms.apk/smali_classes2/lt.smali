.class Llt;
.super Landroid/view/ActionProvider;
.source "SourceFile"


# instance fields
.field final a:Lei;

.field final synthetic b:Lls;


# direct methods
.method public constructor <init>(Lls;Lei;)V
    .locals 2

    iput-object p1, p0, Llt;->b:Lls;

    iget-object v0, p2, Lei;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Llt;->a:Lei;

    invoke-static {p1}, Lls;->a(Lls;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llt;->a:Lei;

    new-instance v1, Llu;

    invoke-direct {v1, p0, p1}, Llu;-><init>(Llt;Lls;)V

    invoke-virtual {v0, v1}, Lei;->a(Lek;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Llt;->a:Lei;

    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Llt;->b:Lls;

    invoke-static {v0}, Lls;->a(Lls;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llt;->b:Lls;

    invoke-virtual {v0}, Lls;->b()Z

    :cond_0
    iget-object v0, p0, Llt;->a:Lei;

    invoke-virtual {v0}, Lei;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPerformDefaultAction()Z
    .locals 1

    iget-object v0, p0, Llt;->a:Lei;

    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareSubMenu(Landroid/view/SubMenu;)V
    .locals 1

    iget-object v0, p0, Llt;->a:Lei;

    iget-object v0, p0, Llt;->b:Lls;

    invoke-virtual {v0, p1}, Lls;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    return-void
.end method
