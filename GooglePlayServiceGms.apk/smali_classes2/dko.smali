.class final Ldko;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldkn;

.field private final b:Ldjx;

.field private final c:Z


# direct methods
.method public constructor <init>(Ldkn;Ldjx;Z)V
    .locals 0

    iput-object p1, p0, Ldko;->a:Ldkn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ldko;->b:Ldjx;

    iput-boolean p3, p0, Ldko;->c:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Ldko;->a:Ldkn;

    invoke-static {v0}, Ldkn;->a(Ldkn;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ldkn;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Ignoring "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Ldko;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "onDataConnectionSucceeded() "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "callback."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "onDataConnectionFailed() "

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Ldko;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldko;->a:Ldkn;

    invoke-static {v0}, Ldkn;->b(Ldkn;)Ldkj;

    move-result-object v0

    iget-object v1, p0, Ldko;->b:Ldjx;

    invoke-interface {v0, v1}, Ldkj;->b(Ldjx;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ldko;->a:Ldkn;

    invoke-static {v0}, Ldkn;->b(Ldkn;)Ldkj;

    move-result-object v0

    iget-object v1, p0, Ldko;->b:Ldjx;

    invoke-interface {v0, v1}, Ldkj;->a(Ldjx;)V

    goto :goto_1
.end method
