.class public final Ldkk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldez;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicReference;

.field private static b:Z

.field private static final c:Ljava/lang/Object;


# instance fields
.field private final d:Lcom/google/android/gms/games/jingle/Libjingle;

.field private final e:Ldkm;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Ljava/util/HashSet;

.field private final j:Ljava/util/HashMap;

.field private final k:Ljava/util/HashMap;

.field private final l:Ljava/util/HashMap;

.field private final m:Ljava/util/HashSet;

.field private final n:Ldkw;

.field private final o:Ldkn;

.field private final p:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v0, 0x0

    sput-boolean v0, Ldkk;->b:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldkk;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldkj;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldkk;->i:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkk;->k:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkk;->l:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldkk;->m:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkk;->p:Ljava/util/HashMap;

    new-instance v0, Ldkm;

    invoke-direct {v0}, Ldkm;-><init>()V

    iput-object v0, p0, Ldkk;->e:Ldkm;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Jingle callback thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Ldkw;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0, p0, p2}, Ldkw;-><init>(Landroid/os/Looper;Ldkk;Ldkj;)V

    iput-object v1, p0, Ldkk;->n:Ldkw;

    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->a()Ldkn;

    move-result-object v0

    iput-object v0, p0, Ldkk;->o:Ldkn;

    new-instance v0, Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Ldkk;->n:Ldkw;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/games/jingle/Libjingle;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-static {}, Ldav;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ldkj;I)Ldkk;
    .locals 3

    const-class v1, Ldkk;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/games/service/RoomAndroidService;->a()V

    sget-object v2, Ldkk;->c:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :try_start_1
    sput-boolean v0, Ldkk;->b:Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, Ldkk;

    invoke-direct {v2, p0, p1, p2}, Ldkk;-><init>(Landroid/content/Context;Ldkj;I)V

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :goto_0
    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkk;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_4
    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkk;

    invoke-direct {v0, p1}, Ldkk;->a(Ldkj;)V

    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkk;

    invoke-direct {v0}, Ldkk;->h()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private declared-synchronized a(Ldkj;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->d()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldkk;->n:Ldkw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldkw;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0, p1}, Ldkn;->a(Ldkj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p3}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p3}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ldkl;)V
    .locals 3

    const/16 v2, 0x20

    invoke-virtual {p2}, Ldkl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Ldkl;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Ldkl;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2, v2}, Ldkl;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ldkk;->j(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Presence established with peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Attempting to connect."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    invoke-virtual {p2, v2}, Ldkl;->a(I)V

    iget-object v0, p0, Ldkk;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "We are the initiator. Initiating connection to: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    const-string v1, ""

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->e(Ljava/lang/String;)V

    iget-object v2, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Ldkk;->h(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized g()V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-object v1, Ldkk;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v2, "Not scheduling cleanup on unusable DCM instance."

    invoke-static {v0, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->c()V

    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->d()V

    const/4 v0, 0x1

    sput-boolean v0, Ldkk;->b:Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-object v1, Ldkk;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v2, "Attempt to remove cleanup operations on unusable DCM instance."

    invoke-static {v0, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Ldkk;->b:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Ldkk;->l:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldkk;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v0}, Ldkx;->a()V

    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v2, "DataConnectionManager"

    const-string v3, "Error closing socket: "

    invoke-static {v2, v3, v0}, Ldav;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private i(Ljava/lang/String;)Ldkl;
    .locals 2

    iget-object v0, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkl;

    if-nez v0, :cond_0

    new-instance v0, Ldkl;

    invoke-direct {v0}, Ldkl;-><init>()V

    iget-object v1, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private j(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private k(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Ldkk;->i(Ljava/lang/String;)Ldkl;

    move-result-object v0

    iget-object v1, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v1, p1}, Ldkw;->a(Ljava/lang/String;)Landroid/os/Message;

    move-result-object v1

    iput-object v1, v0, Ldkl;->b:Landroid/os/Message;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a([BLjava/lang/String;)I
    .locals 3

    const/4 v0, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    if-eqz p2, :cond_1

    :try_start_1
    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p2, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;[B)I

    move-result v0

    goto :goto_0

    :cond_1
    const-string v1, "DataConnectionManager"

    const-string v2, "Bad recipient Jid"

    invoke-static {v1, v2}, Ldav;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([B[Ljava/lang/String;)I
    .locals 6

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    array-length v3, p2

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, p2, v2

    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v4, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v4, v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;[B)V

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "No session exists for jid "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "; unreliable message not sent."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Ldkk;->l:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_0

    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.android.games"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/net/LocalServerSocket;

    invoke-direct {v2, v1}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    new-instance v3, Ldkx;

    invoke-direct {v3, p1, p0, v1, v2}, Ldkx;-><init>(Ljava/lang/String;Ldkk;Ljava/lang/String;Landroid/net/LocalServerSocket;)V

    monitor-enter v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v1, v3, Ldkx;->g:Ldky;

    invoke-virtual {v1}, Ldky;->start()V

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v1, p0, Ldkk;->l:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v3, Ldkx;->d:Ljava/lang/String;

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v1

    :try_start_5
    const-string v2, "DataConnectionManager"

    const-string v3, "IOException creating socket."

    invoke-static {v2, v3, v1}, Ldav;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v1

    :try_start_6
    const-string v2, "DataConnectionManager"

    const-string v3, "InterruptedException creating socket."

    invoke-static {v2, v3, v1}, Ldav;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v1, "Ibb Send result received when XMPP is not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0, p1, p2, p3}, Ldkn;->a(ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mLibjingleReadyForCleanup "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Ldkk;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Ldkm;->b:I

    invoke-static {v3}, Ldkm;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mXmppRegistrationLatencyMs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Ldkm;->a:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mActiveSessions"

    iget-object v2, p0, Ldkk;->i:Ljava/util/HashSet;

    invoke-static {p1, v0, v1, v2}, Ldkk;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInitiatorPeerJids"

    iget-object v2, p0, Ldkk;->m:Ljava/util/HashSet;

    invoke-static {p1, v0, v1, v2}, Ldkk;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mJidToSessionIdMap"

    iget-object v2, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-static {p1, v0, v1, v2}, Ldkk;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSessionIdToJidMap"

    iget-object v2, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-static {p1, v0, v1, v2}, Ldkk;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  mPresenceStateForPeerMap:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/util/TreeMap;

    iget-object v1, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "    "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mLibjingle -> { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v1}, Lcom/google/android/gms/games/jingle/Libjingle;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1, p3}, Ldkk;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, p3}, Ldkk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0, p1, p2}, Ldkn;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "DataConnectionManager"

    const-string v1, "Ignoring Buzz notification when XMPP not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ldkk;->h()V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Network is already connected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iput-object p2, p0, Ldkk;->g:Ljava/lang/String;

    iput-object p3, p0, Ldkk;->h:Ljava/lang/String;

    iget-object v0, p0, Ldkk;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkk;->h:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "DataConnectionManager"

    const-string v1, "Bad player ID or bare JID. Aborting XMPP connection attempt."

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    iget-object v0, p0, Ldkk;->e:Ldkm;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ldkm;->a(II)V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Ldkk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)V
    .locals 3

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received directed presence from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with availability: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    if-nez p2, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v1, "Received directed presence of type \'unavailable\' from peer."

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "DataConnectionManager"

    const-string v1, "Ignoring directed presence since XMPP is not ready."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Ldkk;->i(Ljava/lang/String;)Ldkl;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldkl;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ldkl;->a(I)V

    :goto_1
    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ldkl;->b(I)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ldkl;->b(I)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Sending directed presence to: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->c(Ljava/lang/String;)I

    invoke-direct {p0, p1}, Ldkk;->k(Ljava/lang/String;)V

    invoke-virtual {v1}, Ldkl;->b()V

    :cond_2
    invoke-direct {p0, p1, v1}, Ldkk;->a(Ljava/lang/String;Ldkl;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldkl;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final declared-synchronized a(Ljava/lang/String;ZI)V
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->c()Z

    move-result v1

    iget-object v2, p0, Ldkk;->e:Ldkm;

    iget v2, v2, Ldkm;->b:I

    if-ne v2, v3, :cond_0

    :goto_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Ldkk;->e:Ldkm;

    iput p3, v0, Ldkm;->a:I

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ldkm;->a(II)V

    iput-object p1, p0, Ldkk;->f:Ljava/lang/String;

    iget-object v1, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    sget-object v0, Lcwh;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Z)V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v2, p0, Ldkk;->e:Ldkm;

    const/4 v3, 0x1

    iput v3, v2, Ldkm;->b:I

    invoke-direct {p0}, Ldkk;->g()V

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0}, Ldkn;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0}, Ldkn;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 2

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v1, "Ibb data received when XMPP is not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0, p1, p2}, Ldkn;->a(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v1, "Buzz subscription result received when XMPP is not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    iget-object v0, p0, Ldkk;->e:Ldkm;

    const/4 v1, 0x3

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ldkm;->a(II)V

    iget-object v0, p0, Ldkk;->o:Ldkn;

    iget-object v1, p0, Ldkk;->f:Ljava/lang/String;

    iget-object v2, p0, Ldkk;->e:Ldkm;

    iget v2, v2, Ldkm;->a:I

    invoke-virtual {v0, v1, v2}, Ldkn;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Ldkk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->d(Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v0}, Ldkn;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldkk;->f:Ljava/lang/String;

    return-object v0
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Ldkk;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;ZI)V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v0, "DataConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnectionResult called for sessionId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldav;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "DataConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find JID for session ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Ldkk;->i(Ljava/lang/String;)Ldkl;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ldkl;->a(I)V

    if-eqz p2, :cond_2

    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldkk;->o:Ldkn;

    new-instance v2, Ldjx;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Ldjx;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldkn;->a(Ldjx;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "DataConnectionManager"

    const-string v1, "Ignoring data connection succeeded callback when XMPP not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldkk;->o:Ldkn;

    new-instance v2, Ldjx;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "P2P_FAILED"

    invoke-direct {v2, v0, v3, v4}, Ldjx;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldkn;->b(Ldjx;)V

    :goto_1
    invoke-direct {p0, v0}, Ldkk;->h(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Ldkk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "DataConnectionManager"

    const-string v2, "Ignoring data connection failed callback when DCM is not usable."

    invoke-static {v1, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b(Ljava/lang/String;[B)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "DataConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Data received for non-existent peer for session: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v1}, Ldkm;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "DataConnectionManager"

    const-string v1, "Data received for peer when XMPP is not connected."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v1, p0, Ldkk;->l:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldkx;

    if-eqz v1, :cond_2

    iget-object v2, v1, Ldkx;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :try_start_3
    iget-object v0, v1, Ldkx;->f:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ldkx;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    :try_start_5
    const-string v2, "SocketProxy"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException writing data to socket."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Ldkx;->a:Ldkk;

    iget-object v1, v1, Ldkx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldkk;->b(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v3, "SocketProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error closing socket:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldkk;->o:Ldkn;

    invoke-virtual {v1, v0, p2}, Ldkn;->b(Ljava/lang/String;[B)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;ZI)I
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v2, p1, p3}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;I)V

    iget-object v2, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v2}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, -0x2

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Ldkk;->j(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection already exists for peer: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Ldkk;->i(Ljava/lang/String;)Ldkl;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ldkl;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate connection attempt for peer: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Sending directed presence to: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->c(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Ldkk;->k(Ljava/lang/String;)V

    const/16 v0, 0x10

    invoke-virtual {v2, v0}, Ldkl;->a(I)V

    invoke-virtual {v2}, Ldkl;->b()V

    if-eqz p2, :cond_4

    iget-object v0, p0, Ldkk;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ldkk;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, v2}, Ldkk;->a(Ljava/lang/String;Ldkl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcwh;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "DataConnectionManager"

    const-string v1, "Not un-registering with Buzzbot."

    invoke-static {v0, v1}, Ldav;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "DataConnectionManager"

    const-string v1, "Received null peerJid for network diagnostics."

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->f(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldkk;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldkk;->f()V

    :cond_0
    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Ldkk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->d(Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    const/4 v1, 0x5

    iput v1, v0, Ldkm;->b:I

    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->b()V

    iget-object v0, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v0, "DataConnectionManager"

    const-string v1, "tearDown() called when network is NOT ready for match !"

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e()V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-object v1, Ldkk;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-boolean v0, Ldkk;->b:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Ldkk;->h()V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a()V

    iget-object v0, p0, Ldkk;->n:Ldkw;

    invoke-virtual {v0}, Ldkw;->e()V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    const/4 v2, 0x0

    iput v2, v0, Ldkm;->b:I

    sget-object v0, Ldkk;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "DataConnectionManager"

    const-string v2, "Duplicate cleanup request !"

    invoke-static {v0, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_4
    const-string v0, "DataConnectionManager"

    const-string v2, "Xmpp NOT disconnected, ignoring scheduled cleanup."

    invoke-static {v0, v2}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "DataConnectionManager"

    const-string v2, "Libjingle in use, ignoring scheduled cleanup."

    invoke-static {v0, v2}, Ldav;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcwh;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Ldkk;->h:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "DataConnectionManager"

    const-string v1, "Not registering with Buzzbot."

    invoke-static {v0, v1}, Ldav;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized f(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, p1}, Ldkk;->h(Ljava/lang/String;)V

    iget-object v0, p0, Ldkk;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connection does not exist for peer:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldkl;

    iget-object v3, p0, Ldkk;->p:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_3

    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Ldkl;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v1, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkk;->o:Ldkn;

    new-instance v1, Ldjx;

    const/4 v2, 0x0

    const-string v3, "P2P_FAILED"

    invoke-direct {v1, p1, v2, v3}, Ldjx;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ldkn;->b(Ldjx;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Ldkk;->n:Ldkw;

    iget-object v1, v1, Ldkl;->b:Landroid/os/Message;

    invoke-virtual {v0, v1}, Ldkw;->a(Landroid/os/Message;)V

    move v0, v2

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "disconnect() received before connect() for peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v2

    invoke-static {}, Ldav;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized g(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Presence retry check for peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    invoke-direct {p0, p1}, Ldkk;->i(Ljava/lang/String;)Ldkl;

    move-result-object v0

    invoke-virtual {v0}, Ldkl;->a()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, v0, Ldkl;->a:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Ldkk;->d:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->c(Ljava/lang/String;)I

    invoke-direct {p0, p1}, Ldkk;->k(Ljava/lang/String;)V

    iget v1, v0, Ldkl;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Ldkl;->a:I

    const-string v1, "DataConnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Retrying presence, current retry count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Ldkl;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldav;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Presence failed, unable to establish connection to peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldav;->b()V

    iget-object v0, p0, Ldkk;->e:Ldkm;

    invoke-virtual {v0}, Ldkm;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkk;->o:Ldkn;

    new-instance v1, Ldjx;

    const/4 v2, 0x0

    const-string v3, "P2P_FAILED"

    invoke-direct {v1, p1, v2, v3}, Ldjx;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ldkn;->b(Ldjx;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "DataConnectionManager"

    const-string v1, "onDataConnectionFailed() callback when XMPP is not connected."

    invoke-static {v0, v1}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "DataConnectionManager"

    const-string v1, "Presence established, ignoring retry check."

    invoke-static {v0, v1}, Ldav;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
