.class public final Lhgu;
.super Lhha;
.source "SourceFile"


# instance fields
.field private final g:Lhgy;

.field private final h:Lhhg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    new-instance v1, Lhhg;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lhhg;-><init>(B)V

    invoke-direct {p0, p1, v0, v1}, Lhgu;-><init>(Landroid/content/Context;Lhgy;Lhhg;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lhgy;Lhhg;)V
    .locals 0

    invoke-direct {p0, p1}, Lhha;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lhgu;->g:Lhgy;

    iput-object p3, p0, Lhgu;->h:Lhhg;

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    :cond_1
    :try_start_0
    const-string v0, "bitmap"

    const/4 v3, 0x0

    invoke-static {v0, v3, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    new-instance v2, Lhhi;

    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lhhi;-><init>(Ljava/net/URL;)V

    iget-object v3, p0, Lhgu;->h:Lhhg;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lhhg;->a(Lhhi;Ljava/io/OutputStream;I)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-nez v2, :cond_2

    move-object v0, v1

    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_1
    const-string v2, "ImageNetworkWorker"

    const-string v3, "Exception downloading image to disk"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;I)[B
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0xc00

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    :try_start_0
    new-instance v2, Lhhi;

    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lhhi;-><init>(Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lhgu;->h:Lhhg;

    invoke-virtual {v3, v2, v1, p2}, Lhhg;->a(Lhhi;Ljava/io/OutputStream;I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "ImageNetworkWorker"

    const-string v2, "Bad URL provided."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 7

    check-cast p1, Lhgv;

    const/4 v0, 0x0

    iget-boolean v1, p1, Lhgv;->b:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lhgu;->b:Landroid/content/Context;

    iget-object v2, p1, Lhgv;->a:Ljava/lang/String;

    iget-object v3, p0, Lhgu;->g:Lhgy;

    iget-object v3, v3, Lhgy;->d:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lhgu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lhgu;->g:Lhgy;

    iget v2, v2, Lhgy;->a:I

    iget-object v3, p0, Lhgu;->g:Lhgy;

    iget v3, v3, Lhgy;->b:I

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v5, v6, v2, v3}, Lhgz;->a(IIII)I

    move-result v2

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v2, 0x0

    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p1, Lhgv;->a:Ljava/lang/String;

    iget-object v2, p0, Lhgu;->g:Lhgy;

    iget v2, v2, Lhgy;->c:I

    invoke-direct {p0, v1, v2}, Lhgu;->a(Ljava/lang/String;I)[B

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lhgw;)V
    .locals 3

    iget-object v0, p1, Lhgw;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p1, Lhgw;->d:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lhgu;->e:Landroid/graphics/Bitmap;

    iput-object v0, p1, Lhgw;->e:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p1, Lhgw;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lhgw;->a()Lhgv;

    move-result-object v0

    iget-object v1, p1, Lhgw;->c:Landroid/widget/ImageView;

    iget-object v2, p1, Lhgw;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1, v2}, Lhgu;->a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lhgw;->a()Lhgv;

    move-result-object v0

    iget-object v1, p1, Lhgw;->c:Landroid/widget/ImageView;

    iget v2, p1, Lhgw;->d:I

    invoke-virtual {p0, v0, v1, v2}, Lhgu;->a(Ljava/lang/Object;Landroid/widget/ImageView;I)V

    goto :goto_0
.end method
