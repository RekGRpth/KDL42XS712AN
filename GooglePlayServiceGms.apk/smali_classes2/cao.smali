.class final enum Lcao;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcao;

.field public static final enum b:Lcao;

.field private static final synthetic g:[Lcao;


# instance fields
.field private final c:Lcar;

.field private final d:Lcar;

.field private final e:Lcar;

.field private final f:Lcar;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcao;

    const-string v1, "FOLDERS"

    const v2, 0x7f0b004a    # com.google.android.gms.R.string.drive_fast_scroll_title_grouper_collections

    invoke-direct {v0, v1, v3, v2}, Lcao;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcao;->a:Lcao;

    new-instance v0, Lcao;

    const-string v1, "FILES"

    const v2, 0x7f0b0083    # com.google.android.gms.R.string.drive_title_grouper_files

    invoke-direct {v0, v1, v4, v2}, Lcao;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcao;->b:Lcao;

    const/4 v0, 0x2

    new-array v0, v0, [Lcao;

    sget-object v1, Lcao;->a:Lcao;

    aput-object v1, v0, v3

    sget-object v1, Lcao;->b:Lcao;

    aput-object v1, v0, v4

    sput-object v0, Lcao;->g:[Lcao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v2, v1}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcao;->c:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v1, v1}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcao;->d:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v1, v2}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcao;->e:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v2, v2}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcao;->f:Lcar;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcao;
    .locals 1

    const-class v0, Lcao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcao;

    return-object v0
.end method

.method public static values()[Lcao;
    .locals 1

    sget-object v0, Lcao;->g:[Lcao;

    invoke-virtual {v0}, [Lcao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcao;

    return-object v0
.end method


# virtual methods
.method public final a(ZZ)Lcar;
    .locals 1

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcao;->d:Lcar;

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    iget-object v0, p0, Lcao;->c:Lcar;

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcao;->e:Lcar;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcao;->f:Lcar;

    goto :goto_0
.end method
