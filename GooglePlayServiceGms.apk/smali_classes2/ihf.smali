.class final Lihf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lihf;->a:I

    iput v0, p0, Lihf;->b:I

    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lihf;->a:I

    const/4 v0, 0x0

    iput v0, p0, Lihf;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Livi;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lihf;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lihf;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Livi;

    sget-object v1, Lihj;->Y:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    iget v2, p0, Lihf;->a:I

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    const/4 v1, 0x2

    iget v2, p0, Lihf;->b:I

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lihf;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lihf;->a:I

    if-eqz p1, :cond_0

    iget v0, p0, Lihf;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lihf;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
