.class public final enum Lcdd;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcdd;

.field public static final enum b:Lcdd;

.field public static final enum c:Lcdd;

.field public static final enum d:Lcdd;

.field public static final enum e:Lcdd;

.field public static final enum f:Lcdd;

.field public static final enum g:Lcdd;

.field public static final enum h:Lcdd;

.field private static final synthetic j:[Lcdd;


# instance fields
.field private final i:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v0, Lcdd;

    const-string v1, "ACCOUNT_HOLDER_NAME"

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "accountHolderName"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->a:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "FORCE_FULL_SYNC"

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "forceFullSync"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcdq;->a(I)Lcdq;

    move-result-object v2

    const/16 v3, 0xb

    new-instance v4, Lcei;

    const-string v5, "forceFullSync"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->b:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "LAST_CONTENT_SYNC_TIME"

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "lastContentSyncTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->c:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "LAST_SYNC_TIME"

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "lastSyncTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->d:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "FOLDER_SYNC_CLIP_TIME"

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "folderSyncClipTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->e:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "DOCUMENT_SYNC_CLIP_TIME"

    const/4 v2, 0x5

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "documentSyncClipTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->f:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "LAST_SYNC_CHANGE_STAMP"

    const/4 v2, 0x6

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "lastSyncChangeStamp"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->g:Lcdd;

    new-instance v0, Lcdd;

    const-string v1, "LAST_SYNC_SEQUENCE_NUMBER"

    const/4 v2, 0x7

    invoke-static {}, Lcdc;->d()Lcdc;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "lastSyncSequenceNumber"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcdd;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdd;->h:Lcdd;

    const/16 v0, 0x8

    new-array v0, v0, [Lcdd;

    sget-object v1, Lcdd;->a:Lcdd;

    aput-object v1, v0, v8

    sget-object v1, Lcdd;->b:Lcdd;

    aput-object v1, v0, v7

    sget-object v1, Lcdd;->c:Lcdd;

    aput-object v1, v0, v9

    sget-object v1, Lcdd;->d:Lcdd;

    aput-object v1, v0, v10

    sget-object v1, Lcdd;->e:Lcdd;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcdd;->f:Lcdd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcdd;->g:Lcdd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcdd;->h:Lcdd;

    aput-object v2, v0, v1

    sput-object v0, Lcdd;->j:[Lcdd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcdd;->i:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdd;
    .locals 1

    const-class v0, Lcdd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdd;

    return-object v0
.end method

.method public static values()[Lcdd;
    .locals 1

    sget-object v0, Lcdd;->j:[Lcdd;

    invoke-virtual {v0}, [Lcdd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdd;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcdd;->i:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcdd;->i:Lcdp;

    return-object v0
.end method
