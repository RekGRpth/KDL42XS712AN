.class public final Leyl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:D

.field public b:D

.field public c:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Leyl;Leyl;)D
    .locals 6

    iget-wide v0, p0, Leyl;->a:D

    iget-wide v2, p1, Leyl;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Leyl;->b:D

    iget-wide v4, p1, Leyl;->b:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Leyl;->c:D

    iget-wide v4, p1, Leyl;->c:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Leyl;Leyl;Leyl;)V
    .locals 11

    iget-wide v0, p0, Leyl;->b:D

    iget-wide v2, p1, Leyl;->c:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Leyl;->c:D

    iget-wide v4, p1, Leyl;->b:D

    mul-double/2addr v2, v4

    sub-double v1, v0, v2

    iget-wide v3, p0, Leyl;->c:D

    iget-wide v5, p1, Leyl;->a:D

    mul-double/2addr v3, v5

    iget-wide v5, p0, Leyl;->a:D

    iget-wide v7, p1, Leyl;->c:D

    mul-double/2addr v5, v7

    sub-double/2addr v3, v5

    iget-wide v5, p0, Leyl;->a:D

    iget-wide v7, p1, Leyl;->b:D

    mul-double/2addr v5, v7

    iget-wide v7, p0, Leyl;->b:D

    iget-wide v9, p1, Leyl;->a:D

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Leyl;->a(DDD)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leyl;->c:D

    iput-wide v0, p0, Leyl;->b:D

    iput-wide v0, p0, Leyl;->a:D

    return-void
.end method

.method public final a(D)V
    .locals 2

    iget-wide v0, p0, Leyl;->a:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Leyl;->a:D

    iget-wide v0, p0, Leyl;->b:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Leyl;->b:D

    iget-wide v0, p0, Leyl;->c:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Leyl;->c:D

    return-void
.end method

.method public final a(DDD)V
    .locals 0

    iput-wide p1, p0, Leyl;->a:D

    iput-wide p3, p0, Leyl;->b:D

    iput-wide p5, p0, Leyl;->c:D

    return-void
.end method

.method public final a(Leyl;)V
    .locals 2

    iget-wide v0, p1, Leyl;->a:D

    iput-wide v0, p0, Leyl;->a:D

    iget-wide v0, p1, Leyl;->b:D

    iput-wide v0, p0, Leyl;->b:D

    iget-wide v0, p1, Leyl;->c:D

    iput-wide v0, p0, Leyl;->c:D

    return-void
.end method

.method public final b()V
    .locals 4

    invoke-virtual {p0}, Leyl;->c()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    div-double v0, v2, v0

    invoke-virtual {p0, v0, v1}, Leyl;->a(D)V

    :cond_0
    return-void
.end method

.method public final c()D
    .locals 6

    iget-wide v0, p0, Leyl;->a:D

    iget-wide v2, p0, Leyl;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Leyl;->b:D

    iget-wide v4, p0, Leyl;->b:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Leyl;->c:D

    iget-wide v4, p0, Leyl;->c:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method
