.class final Lhay;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhar;


# direct methods
.method constructor <init>(Lhar;)V
    .locals 0

    iput-object p1, p0, Lhay;->a:Lhar;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhay;->a:Lhar;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lhar;->a(Lhar;I)V

    return-void
.end method

.method public final a(Lipy;)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    invoke-static {p1}, Lhfx;->a(Lipy;)I

    move-result v0

    const/16 v1, 0x198

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhay;->a:Lhar;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lhar;->a(Lhar;ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->D(Lhar;)Z

    move-result v1

    invoke-static {p1, v1}, Lhfx;->a(Lipy;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lipy;->d:Ljak;

    iget-object v1, v1, Ljak;->c:Ljal;

    iget-object v2, p0, Lhay;->a:Lhar;

    iget-object v3, v1, Ljal;->a:Ljava/lang/String;

    iget-object v1, v1, Ljal;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lhar;->a(Lhar;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->D(Lhar;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->z(Lhar;)Lhbp;

    move-result-object v0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->x(Lhar;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lhbp;->b(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1, v0}, Lhar;->a(Lhar;I)V

    goto :goto_0
.end method

.method public final a(Lizz;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lhay;->a:Lhar;

    iput-object p1, v0, Lhar;->an:Lizz;

    iget v0, p1, Lizz;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->i(Lhar;)V

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, p1}, Lhar;->a(Lhar;Lizz;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhay;->a:Lhar;

    iput-boolean v3, v0, Lhar;->al:Z

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->j(Lhar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->k(Lhar;)Z

    iget-object v1, p1, Lizz;->a:Ljbb;

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v1, Ljbb;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v2, v1, Ljbb;->a:[I

    invoke-static {v0, v2}, Lhar;->a(Lhar;[I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->l(Lhar;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, p1}, Lhar;->b(Lhar;Lizz;)V

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, v1}, Lhar;->a(Lhar;Ljbb;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->l(Lhar;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iget-object v2, p0, Lhay;->a:Lhar;

    iget-object v2, v2, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v2, v3, :cond_4

    new-instance v0, Lgrz;

    invoke-direct {v0}, Lgrz;-><init>()V

    iget-object v2, p0, Lhay;->a:Lhar;

    iget-object v2, v2, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v2, :pswitch_data_0

    :cond_4
    :goto_1
    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, v1}, Lhar;->b(Lhar;Ljbb;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2}, Lhar;->m(Lhar;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, p1, v0}, Lhar;->a(Lhar;Lizz;Lgrz;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, p1, v0}, Lhar;->b(Lhar;Lizz;Lgrz;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, v1}, Lhar;->c(Lhar;Ljbb;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, v1}, Lhar;->d(Lhar;Ljbb;)V

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2, v1, v0}, Lhar;->a(Lhar;Ljbb;Lgrz;)V

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->n(Lhar;)V

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->o(Lhar;)V

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1, v5}, Lhar;->a(Lhar;Z)V

    iget-object v1, p0, Lhay;->a:Lhar;

    iget-object v1, v1, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lhay;->a:Lhar;

    iget-object v1, v1, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eq v1, v3, :cond_6

    :cond_5
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->p(Lhar;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_6
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->q(Lhar;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->r(Lhar;)Z

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->c(Lhar;)V

    :cond_7
    :goto_2
    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->l(Lhar;)V

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->s(Lhar;)Luu;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->t(Lhar;)Lut;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->s(Lhar;)Luu;

    move-result-object v1

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2}, Lhar;->t(Lhar;)Lut;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "create_to_ui_populated"

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-virtual {v1}, Lhar;->T_()Lo;

    move-result-object v1

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2}, Lhar;->s(Lhar;)Luu;

    move-result-object v2

    invoke-static {v1, v2}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->u(Lhar;)Luu;

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->v(Lhar;)Lut;

    :cond_8
    if-eqz v0, :cond_0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-virtual {v1}, Lhar;->T_()Lo;

    move-result-object v1

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v3

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lgsm;->a:Landroid/content/Context;

    const-string v6, "wallet_items_cache"

    const-string v7, "update_ui_loaded_from_cache"

    iget-object v8, v0, Lgrz;->b:[Ljava/lang/String;

    iget-boolean v1, v0, Lgrz;->a:Z

    if-eqz v1, :cond_a

    const-wide/16 v1, 0x1

    :goto_3
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v5, v6, v7, v8, v1}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V

    iget-boolean v0, v0, Lgrz;->a:Z

    if-nez v0, :cond_0

    const-string v0, "ui_disruption_due_to_cache"

    invoke-static {v3, v4, v0}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_0
    const-string v2, "continue"

    invoke-virtual {v0, v2}, Lgrz;->a(Ljava/lang/String;)Lgrz;

    goto/16 :goto_1

    :pswitch_1
    const-string v2, "updateInstrument"

    invoke-virtual {v0, v2}, Lgrz;->a(Ljava/lang/String;)Lgrz;

    goto/16 :goto_1

    :pswitch_2
    const-string v2, "updateAddress"

    invoke-virtual {v0, v2}, Lgrz;->a(Ljava/lang/String;)Lgrz;

    goto/16 :goto_1

    :cond_9
    iget-object v1, p0, Lhay;->a:Lhar;

    iget-object v1, v1, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->b(Lhar;)V

    goto/16 :goto_2

    :cond_a
    const-wide/16 v1, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljav;J)V
    .locals 5

    iget-object v0, p1, Ljav;->i:[I

    array-length v0, v0

    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p1, Ljav;->i:[I

    aget v1, v1, v0

    packed-switch v1, :pswitch_data_0

    iget-object v0, p0, Lhay;->a:Lhar;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lhar;->a(Lhar;I)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->z(Lhar;)Lhbp;

    move-result-object v0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->y(Lhar;)Ljau;

    move-result-object v1

    iget-object v2, p0, Lhay;->a:Lhar;

    iget-object v2, v2, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1, v2}, Lhbp;->a(Ljau;Lioj;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->x(Lhar;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1, p2, p3}, Lhfx;->a(Ljav;Ljau;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v2

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ah:Landroid/view/View;

    const v1, 0x7f0a0328    # com.google.android.gms.R.id.credit_card_generation_view

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/FullWallet;->c()Lcom/google/android/gms/wallet/ProxyCard;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lhay;->a:Lhar;

    iget-object v1, v1, Lhar;->ah:Landroid/view/View;

    const v4, 0x7f0a0329    # com.google.android.gms.R.id.credit_card_generation_message

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f0b00e6    # com.google.android.gms.R.string.wallet_finished_generating_secure_card

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;)V

    new-instance v0, Lhaz;

    invoke-direct {v0, p0, v2}, Lhaz;-><init>(Lhay;Lcom/google/android/gms/wallet/FullWallet;)V

    const-wide/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1, v2}, Lhay;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljax;)V
    .locals 9

    const/16 v8, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v2, p1, Ljax;->a:[I

    invoke-static {v0, v2}, Lhar;->a(Lhar;[I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Ljax;->a:[I

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v2, p1, Ljax;->a:[I

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget v4, v2, v0

    packed-switch v4, :pswitch_data_0

    const-string v0, "ChooseMethodsFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled required action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, v8}, Lhar;->a(Lhar;I)V

    goto :goto_0

    :pswitch_0
    const-string v5, "ChooseMethodsFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Required action: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p1, Ljax;->d:Ljbb;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lhay;->a:Lhar;

    const/4 v5, 0x0

    iput-object v5, v4, Lhar;->an:Lizz;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->an:Lizz;

    if-nez v0, :cond_3

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->i(Lhar;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, v1}, Lhar;->a(Lhar;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p1, Ljax;->b:Ljbh;

    if-eqz v0, :cond_5

    iget-object v0, p1, Ljax;->b:Ljbh;

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->w(Lhar;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhay;->a:Lhar;

    invoke-static {v2}, Lhar;->x(Lhar;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lhfx;->a(Ljbh;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lhay;->a:Lhar;

    const/4 v2, -0x1

    invoke-static {v0, v2, v1}, Lhar;->a(Lhar;ILandroid/content/Intent;)V

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-virtual {v0}, Lhar;->T_()Lo;

    move-result-object v0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhay;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "buyer_selection_masked_wallet"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "ChooseMethodsFragment"

    const-string v1, "GetMaskedWalletForBuyerSelectionResponse has neither required action nor MerchantResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0, v8}, Lhar;->a(Lhar;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhay;->a:Lhar;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lhar;->a(Lhar;I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->A(Lhar;)Z

    iget-object v0, p0, Lhay;->a:Lhar;

    iget-boolean v0, v0, Lhar;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->B(Lhar;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhay;->a:Lhar;

    invoke-static {v0}, Lhar;->C(Lhar;)V

    goto :goto_0
.end method
