.class public final Lbvi;
.super Lbvj;
.source "SourceFile"


# static fields
.field public static final a:Lbvi;

.field public static final b:Lbvi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbvi;

    sget-object v1, Lbvh;->a:Lbvh;

    invoke-direct {v0, v1}, Lbvi;-><init>(Lbvh;)V

    sput-object v0, Lbvi;->a:Lbvi;

    new-instance v0, Lbvi;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbvi;-><init>(Lbvh;)V

    sput-object v0, Lbvi;->b:Lbvi;

    return-void
.end method

.method private constructor <init>(Lbvh;)V
    .locals 0

    invoke-direct {p0, p1, p1}, Lbvi;-><init>(Lbvh;Lbvh;)V

    return-void
.end method

.method private constructor <init>(Lbvh;Lbvh;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbvj;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lbvi;
    .locals 2

    invoke-static {p0}, Lbvh;->a(Ljava/lang/String;)Lbvh;

    move-result-object v0

    new-instance v1, Lbvi;

    invoke-direct {v1, v0}, Lbvi;-><init>(Lbvh;)V

    return-object v1
.end method

.method public static a(Ljava/util/Date;Ljava/util/Date;)Lbvi;
    .locals 3

    new-instance v0, Lbvi;

    invoke-static {p0}, Lbvh;->a(Ljava/util/Date;)Lbvh;

    move-result-object v1

    invoke-static {p1}, Lbvh;->a(Ljava/util/Date;)Lbvh;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbvi;-><init>(Lbvh;Lbvh;)V

    return-object v0
.end method

.method public static c()Lbvi;
    .locals 2

    new-instance v0, Lbvi;

    invoke-static {}, Lbvh;->f()Lbvh;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvi;-><init>(Lbvh;)V

    return-object v0
.end method

.method public static d()Lbvi;
    .locals 2

    new-instance v0, Lbvi;

    invoke-static {}, Lbvh;->d()Lbvh;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvi;-><init>(Lbvh;)V

    return-object v0
.end method

.method public static e()Lbvi;
    .locals 2

    new-instance v0, Lbvi;

    invoke-static {}, Lbvh;->e()Lbvh;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvi;-><init>(Lbvh;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lbvh;
    .locals 1

    iget-object v0, p0, Lbvi;->c:Ljava/lang/Object;

    check-cast v0, Lbvh;

    return-object v0
.end method

.method public final a(Lbvi;)Lbvi;
    .locals 4

    new-instance v2, Lbvi;

    iget-object v0, p0, Lbvi;->c:Ljava/lang/Object;

    check-cast v0, Lbvh;

    iget-object v1, p1, Lbvi;->c:Ljava/lang/Object;

    check-cast v1, Lbvh;

    invoke-static {v0, v1}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v3

    iget-object v0, p0, Lbvi;->d:Ljava/lang/Object;

    check-cast v0, Lbvh;

    iget-object v1, p1, Lbvi;->d:Ljava/lang/Object;

    check-cast v1, Lbvh;

    invoke-static {v0, v1}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lbvi;-><init>(Lbvh;Lbvh;)V

    return-object v2
.end method

.method public final b()Lbvh;
    .locals 1

    iget-object v0, p0, Lbvi;->d:Ljava/lang/Object;

    check-cast v0, Lbvh;

    return-object v0
.end method
