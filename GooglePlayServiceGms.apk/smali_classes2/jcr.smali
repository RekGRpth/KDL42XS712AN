.class public final enum Ljcr;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ljcr;

.field public static final enum b:Ljcr;

.field public static final enum c:Ljcr;

.field private static final synthetic g:[Ljcr;


# instance fields
.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-instance v0, Ljcr;

    const-string v1, "HMAC_SHA256"

    const-string v4, "HmacSHA256"

    move v5, v2

    invoke-direct/range {v0 .. v5}, Ljcr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v0, Ljcr;->a:Ljcr;

    new-instance v4, Ljcr;

    const-string v5, "ECDSA_P256_SHA256"

    const-string v8, "SHA256withECDSA"

    move v6, v3

    move v9, v3

    invoke-direct/range {v4 .. v9}, Ljcr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v4, Ljcr;->b:Ljcr;

    new-instance v5, Ljcr;

    const-string v6, "RSA2048_SHA256"

    const-string v9, "SHA256withRSA"

    move v8, v11

    move v10, v3

    invoke-direct/range {v5 .. v10}, Ljcr;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v5, Ljcr;->c:Ljcr;

    new-array v0, v11, [Ljcr;

    sget-object v1, Ljcr;->a:Ljcr;

    aput-object v1, v0, v2

    sget-object v1, Ljcr;->b:Ljcr;

    aput-object v1, v0, v3

    sget-object v1, Ljcr;->c:Ljcr;

    aput-object v1, v0, v7

    sput-object v0, Ljcr;->g:[Ljcr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Ljcr;->d:I

    iput-object p4, p0, Ljcr;->e:Ljava/lang/String;

    iput-boolean p5, p0, Ljcr;->f:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ljcr;
    .locals 1

    const-class v0, Ljcr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljcr;

    return-object v0
.end method

.method public static values()[Ljcr;
    .locals 1

    sget-object v0, Ljcr;->g:[Ljcr;

    invoke-virtual {v0}, [Ljcr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljcr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljcr;->d:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcr;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Ljcr;->f:Z

    return v0
.end method
