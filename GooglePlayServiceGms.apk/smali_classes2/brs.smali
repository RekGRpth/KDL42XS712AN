.class public final Lbrs;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lbrl;

.field private final d:Lbsr;

.field private final e:Lcnf;

.field private final f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;


# direct methods
.method public constructor <init>(Lbrc;Lbrl;Lbsr;Lcnf;Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p6}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrs;->c:Lbrl;

    iput-object p3, p0, Lbrs;->d:Lbsr;

    iput-object p4, p0, Lbrs;->e:Lcnf;

    iput-object p5, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    const-string v3, "Invalid close request: no request"

    invoke-static {v0, v3}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v3, "Invalid close request: no DriveId"

    invoke-static {v0, v3}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v3, "Invalid close request: no metadata"

    invoke-static {v0, v3}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    const-string v3, "Invalid close request: no contents"

    invoke-static {v0, v3}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Invalid close request: invalid request"

    invoke-static {v0, v3}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbrs;->b:Lbrc;

    iget-object v3, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v0, v3}, Lbrc;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->G()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    iget-object v4, p0, Lbrs;->b:Lbrc;

    invoke-interface {v4}, Lbrc;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    if-ne v3, v4, :cond_2

    :cond_0
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v3, "The user cannot edit the resource."

    invoke-direct {v0, v1, v3, v2}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v2, Lcne;

    iget-object v3, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcne;-><init>(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    iget-object v0, p0, Lbrs;->c:Lbrl;

    iget-object v3, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v3

    iget-object v4, p0, Lbrs;->f:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v4

    invoke-interface {v0, p1, v3, v4, v1}, Lbrl;->a(Lbsp;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Z)V

    iget-object v0, p0, Lbrs;->e:Lcnf;

    iget-object v1, p0, Lbrs;->d:Lbsr;

    invoke-virtual {v2, v0, v1}, Lcne;->a(Lcnf;Lbsr;)V

    iget-object v0, p0, Lbrs;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->a()V

    return-void
.end method
