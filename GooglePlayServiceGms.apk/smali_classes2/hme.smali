.class final Lhme;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0x4b

    const/16 v8, 0xc

    const/4 v7, 0x0

    const-wide v5, 0x3fc3333333333333L    # 0.15

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fed4579f2346562L    # 0.914731

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f92d2bb23571d1dL    # 0.018382

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9272862f5989dfL    # 0.018015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c8583621fafdL    # 9.891298

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40225476295208e1L    # 9.164964

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4023551d4738a3b5L    # 9.666239

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f23660e51d25aabL    # 1.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde09a671ef30a5L    # 0.469339

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3eb851eb851ecL    # 0.155625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f18e757928e0c9eL    # 9.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe19ec918e325d5L    # 0.550633

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5715c63ae2541eL    # 0.001409

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f89ce075f6fd220L    # 0.0126

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f13a92a30553261L    # 7.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1cd5f99c38b04bL    # 1.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5e0828c36da87aL    # 0.001833

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fac322291fb3fa7L    # 0.05507

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f898e9707182236L    # 0.012479

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3edd5c31593e5fb7L    # 7.0E-6

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f315df6555c52e7L    # 2.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f99f01b866e43aaL    # 0.02533

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f70a569b17481b2L    # 0.004064

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402473ce2089e343L    # 10.226182

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4009ec016052502fL    # 3.240237

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f502ff8ec0f8833L    # 9.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddfd199bb2788eL    # 0.468573

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f014d2f5dbb9cfaL    # 3.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f10c6f7a0b5ed8dL    # 6.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eef75104d551d69L    # 1.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40243314ca925fe9L    # 10.099768

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fcc9ea9a3d2eL    # 0.156152

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f526e978d4fdf3bL    # 0.001125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40249fdb09a671efL    # 10.312218

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe10da3c21187e8L    # 0.532915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffe29802c0a4a06L    # 1.885132

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe050507a6bd6e9L    # 0.509804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f58655193708aadL    # 0.001489

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fb7fb04ee7c49fdL    # 0.093674

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f720296b3354c12L    # 0.004397

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff915970b49e01eL    # 1.567771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f29b0ab2e1693c0L    # 1.96E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd9852f7f498c3bL    # 0.398754

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f68611fd5885d31L    # 0.002976

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9e368f08461f9fL    # 0.029505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f741b75a74c09c4L    # 0.004909

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f536a400fba8827L    # 0.001185

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402336060fe47992L    # 9.605515

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faf8fabda0dfdf0L    # 0.061643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f52d77318fc5048L    # 0.00115

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4b8ed1bf7ad4b2L    # 8.41E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40249243137b0707L    # 10.285668

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5b8aa00192a737L    # 0.001681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd00cd855970b4aL    # 0.250784

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4027912428d434a0L    # 11.783479

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcb1d81f10667f9L    # 0.211838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e4712e40852b5L    # 4.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddee17a02fb5d0L    # 0.467657

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f60d9d7774aba38L    # 0.002057

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3b003686a4ca4fL    # 4.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd6df19d66adb40L    # 0.357367

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f38f81e8a2ec28bL    # 3.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa0476f2a5a469dL    # 0.031795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fce2a66dbd72bcbL    # 0.235669

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc407dd44135547L    # 0.15649

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e4712e40852b5L    # 2.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40379314445aaL    # 0.156356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f8ca8198f1d4L    # 0.15603

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f37763e4abe6a33L    # 3.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc7d9d3458cd20bL    # 0.186335

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f58def416bdb1a7L    # 0.001518

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3d3aa369fcf3dcL    # 4.46E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f20e8858ff75968L    # 1.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd497d49d7ba66aL    # 0.321767

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe41c6b05319829L    # 0.628469

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd1be9c886162f1L    # 0.277259

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f34e7ee9142b303L    # 3.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc556323bbc6eb1L    # 0.166693

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fca26e5471715c6L    # 0.204312

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc404f2ae07e5a8L    # 0.156401

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2f53825e13b18eL    # 2.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff67e8dda48b652L    # 1.405897

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fca5154866a11edL    # 0.205607

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5908e581cf7879L    # 0.001528

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d4738a3b57c4eL    # 0.007148

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f313c68661ae70cL    # 2.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a79fec99f1ae3L    # 1.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d19157abb8801L    # 4.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f201f31f46ed246L    # 1.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f420e1f7d73c925L    # 5.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f48def416bdb1a7L    # 7.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76bca5375c8da0L    # 0.005551

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8097c80841ede1L    # 0.008102

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5758e219652bd4L    # 0.001425

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402673c60029f16bL    # 11.22612

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff79f4b1ee24357L    # 1.47639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4f20a73f748a1L    # 0.327273

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2cd5f99c38b04bL    # 2.2E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f559c49774256bdL    # 0.001319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d3aa369fcf3dcL    # 4.46E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9e40c8472c0e7cL    # 0.029544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f52513b5bf6a0dcL    # 0.001118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9dd00f776c4828L    # 0.029114

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa9d27c39368273L    # 0.050434

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3754b05b7cfe58L    # 3.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe3e63c74fb54a0L    # 0.621855

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6f644955b4677bL    # 0.003832

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a9341bfbdf091L    # 13.287611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7e8c47a17f4129L    # 0.007458

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f77060bb2bba98fL    # 0.005621

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd6abbcb1cc9646L    # 0.354232

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e42e126202539L    # 0.003694

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f525dd095af294eL    # 0.001121

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40226c3bb40b34e7L    # 9.211393

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40295fe69270b06cL    # 12.687306

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402b83f961804d98L    # 13.757762

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020c8d002e23290L    # 8.392212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5d5c31593e5fb7L    # 0.001792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4005460913a4f872L    # 2.659197

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd71cbccf28c79fL    # 0.361129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7d78811b1d92b8L    # 0.007195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc826a22b3892efL    # 0.188679

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4dd7ecbb7f9d7L    # 0.326019

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a9ce93e1c9b41L    # 13.306467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff1763800218defL    # 1.091362

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd83b8a19c9d5a2L    # 0.378634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd751654d61b2a2L    # 0.364343

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023ae3b46fdeb53L    # 9.840296

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f25a07b352a8438L    # 1.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa3db1a6d698fe7L    # 0.038781

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcb1d81f10667f9L    # 0.211838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f80f062d40aaeb0L    # 0.008271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022902ff8ec0f88L    # 9.281616

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40019912556d19dfL    # 2.199742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8cc74b838c111bL    # 0.014052

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc406290eed02cdL    # 0.156438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402dee1693c03bc5L    # 14.965016

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40213924f227d029L    # 8.61161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde01b866e43aa8L    # 0.468855

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4029f6cf850df15aL    # 12.982052

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd96f9767903212L    # 0.397436

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6f03d145d85165L    # 0.003786

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1de26916440f24L    # 1.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd0e23af31b153L    # 0.226994

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffcea3161a1db87L    # 1.807176

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b64e054690de1L    # 8.36E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5c219eb6390c91L    # 0.001717

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ee92a737110e454L    # 1.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe1352220bc382aL    # 0.537736

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0a36e2eb1c432dL    # 5.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd2aee631f8a090L    # 0.291925

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff3ad538ac18f82L    # 1.229816

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0e68a0d349be90L    # 5.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0abd1aa821f299L    # 5.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4004209aaa3ad18dL    # 2.51592

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe098dcdb37c99bL    # 0.51866

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef6052502eec7c9L    # 2.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4240746455eaeeL    # 5.57E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b79d909f1f14aL    # 0.003354

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4070329802c0aL    # 0.156464

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa6d7e889c1b542L    # 0.044616

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa41aef6f8f0414L    # 0.039268

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3797cc39ffd60fL    # 3.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd174e65bea0ba2L    # 0.27276

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f32ad81adea8976L    # 2.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcf616f86a09891L    # 0.245161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcf2df505d0fa59L    # 0.24359

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b64e054690de1L    # 2.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd9ef459d99029bL    # 0.405229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402876ebb341e14cL    # 12.232267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc0c636b0963561L    # 0.131049

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f53a0c6b484d76bL    # 0.001198

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba779207d4e098L    # 0.103387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f84517614595358L    # 0.009921

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fecc74751ce28edL    # 0.899326

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe34ccccccccccdL    # 0.603125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fec9e68a0d349bfL    # 0.894337

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f57ce52deca2553L    # 0.001453

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f34f8b588e368f1L    # 3.2E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f516ebd4cfd08d5L    # 0.001064

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f54cec41dd1a21fL    # 0.00127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8acffa7eb6bf44L    # 0.013092

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd21cfbb949624aL    # 0.283019

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa0b0079a2834d2L    # 0.032593

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f51b60ae9680e06L    # 0.001081

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ab0856e696a27L    # 0.003258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5426fe718a86d7L    # 0.00123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbaac8605681ecdL    # 0.104195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd8e38eb0318b93L    # 0.388889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6d7342edbb59deL    # 0.003595

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f908dd1e53a81dcL    # 0.016166

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fdd548238540464L    # 0.458283

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd9c426351deefeL    # 0.402597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa98bb4d48882f1L    # 0.049894

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe3a95421c04428L    # 0.61442

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fce7ecfe9b7bf1fL    # 0.238245

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022f0954a7f8013L    # 9.469889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf8066c2acb85aL    # 0.492212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9eb3dd11be6e65L    # 0.029983

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd83a2595bbbe88L    # 0.378549

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb127c393682731L    # 0.067013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3908e581cf7879L    # 3.82E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcbed634549b62cL    # 0.218182

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x402088485a0be511L    # 8.266177

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7cac083126e979L    # 0.007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5cf355cd91eeaaL    # 0.001767

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2d3aa369fcf3dcL    # 2.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f255d5f56a7ac82L    # 1.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1711947cfa26a2L    # 8.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff678e7ddca4b12L    # 1.404518

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbc45132f87ad08L    # 0.110429

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3fea8112ba16e8L    # 4.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb9ebc83a96d4c3L    # 0.101254

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5f3e89a88ace25L    # 0.001907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe109fa54c55433L    # 0.532468

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9c7820a30db6a2L    # 0.027802

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4027fb5696e58a33L    # 11.990895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f157eed45e9185dL    # 8.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40034cc575c0767dL    # 2.412486

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9382e44b6e935cL    # 0.019054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd1cd24e160d888L    # 0.278146

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6af5ba2be0589bL    # 0.003291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8df97aaac1094aL    # 0.014636

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f90ac3a860dcb9bL    # 0.016282

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2cd5f99c38b04bL    # 2.2E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40094980b242070cL    # 3.16089

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff32f5ec80c73acL    # 1.199065

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4c4fc1df3300deL    # 8.64E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3bfbdf090f733bL    # 4.27E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f294c016052502fL    # 1.93E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc9ec918e325d4aL    # 0.202532

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f536e71cda2b5a2L    # 0.001186

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fcd84901d19157bL    # 0.230608

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f975ee99a62ed35L    # 0.022823

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fca12f901083dbcL    # 0.203704

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddda9c99285a92L    # 0.466468

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3c6088d6d3b6ccL    # 4.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41376d5497311L    # 0.156844

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f16ce789e774eecL    # 8.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fee0a5accd53049L    # 0.938764

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8f4d37c1376d55L    # 0.015284

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2d3aa369fcf3dcL    # 2.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd86fb4c3c18b50L    # 0.381818

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032c28a82a5614eL    # 18.759926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd9852f7f498c3bL    # 0.398754

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd054bcf0b6b6e1L    # 0.255172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb751159c497742L    # 0.091081

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4032bbfcfc3f811fL    # 18.734329

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e03f705857affL    # 2.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f41a975afaf8594L    # 5.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61dbca9691a75dL    # 0.00218

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc2638433d6c722L    # 0.143662

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401b2822ff08893bL    # 6.789196

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40041657fb69984aL    # 2.51091

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f77cd466f5019f4L    # 0.005811

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcab76f6d762520L    # 0.208723

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26052502eec7c9L    # 1.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403071c96030c240L    # 16.444479

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84b9cb6848beb6L    # 0.01012

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034cc8a159817b9L    # 20.798982

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3acde19fc2a887L    # 4.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d6b265add9c28L    # 14.709277

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa12e83a109d063L    # 0.033558

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f817f84449dbec2L    # 0.008544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400041fc08fa7a85L    # 2.032219

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5743e963dc486bL    # 0.00142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa53e4ef0281ba8L    # 0.041491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f952deca25529feL    # 0.020683

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6652bd3c361134L    # 0.002725

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fff185ad538ac19L    # 1.943446

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2c92ddbdb5d895L    # 2.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8317efe0ce0b91L    # 0.009323

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f663fdd65a14489L    # 0.002716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f924dd2f1a9fbe7L    # 0.017875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1fb82c2bd7f51fL    # 1.21E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa2f31b152f3c2eL    # 0.037011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f634ce3de6149c7L    # 0.002356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fe4540895d0b73dL    # 0.635258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a500d5e8d5411L    # 8.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023d3892ee84ad8L    # 9.913156

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7ca18bd66277c4L    # 0.00699

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0cd5f99c38b04bL    # 5.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5886df82b1f688L    # 0.001497

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40310bc148344c38L    # 17.045918

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4006a1ef73c0c1fdL    # 2.82907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe15885d31337ebL    # 0.542056

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b97353b4b2fa9L    # 8.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faf0cdc8754f377L    # 0.060645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402272d70e6f2e8cL    # 9.224297

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022581b003686a5L    # 9.172081

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f83b85e80bed741L    # 0.009629

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0f75104d551d69L    # 6.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4198aeb80ecfa7L    # 5.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f25a07b352a8438L    # 1.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5c25d074213a0cL    # 0.001718

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fde50d6b228dc98L    # 0.473684

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f329cbab649d389L    # 2.84E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f302ff8ec0f8833L    # 2.47E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f48def416bdb1a7L    # 7.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd0403148776816L    # 0.253918

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe1d77318fc5048L    # 0.55755

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f720d130df9bdc7L    # 0.004407

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9e0bd44998d046L    # 0.029342

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f25c209246bf013L    # 1.66E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a9b8cb8e086beL    # 2.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3edd052934acbL    # 0.155695

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5dde37585be1a8L    # 0.001823

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f42dfd694ccab3fL    # 5.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f502795703f2d3cL    # 9.86E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400916c15d2d01c1L    # 3.136111

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd20d1fa333764fL    # 0.282051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d1

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84dad31fcd24e1L    # 0.010183

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc79439de481f54L    # 0.184211

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f455d5f56a7ac82L    # 6.52E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f69c38b04ab606bL    # 0.003145

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f068b5cbff47736L    # 4.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff466f9335d249eL    # 1.27514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbf7de939eadd59L    # 0.123015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ebc83a96d4c34L    # 4.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb2ec39799e518fL    # 0.073917

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa52e2fbe33acd6L    # 0.041368

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6737542a23bff9L    # 0.002834

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021a8398a654930L    # 8.828564

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc5224aada33bdaL    # 0.165109

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f59a415f45e0b4eL    # 0.001565

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff93b517a46173cL    # 1.576982

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9513b5bf6a0dbbL    # 0.020583

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020e42784a9478dL    # 8.445614

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401c997fa5d28d00L    # 7.149901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5205bc01a36e2fL    # 0.0011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401bed694ccab3eeL    # 6.981847

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4012b05b39e279ddL    # 4.672223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c7de5082cf52cL    # 0.001739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde16c1e364bec6L    # 0.470139

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb3e7f2c3009b30L    # 0.077758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f67bfa4c61d8623L    # 0.002899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40134fb267c6b8b7L    # 4.827829

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ffb271de69ad42cL    # 1.69705

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84373f316e3715L    # 0.009871

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5897a67a52ac75L    # 0.001501

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40203f47b677f6b2L    # 8.123594

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f803968d759ee89L    # 0.007922

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb8da3c21187e7cL    # 0.09708

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa328b6d86ec17fL    # 0.03742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffb75e529bae46dL    # 1.716283

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcbc24c404a72ebL    # 0.216867

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe4063beec3979aL    # 0.625761

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4018a43bb40b3L    # 0.156297

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f58efbb0e5e6794L    # 0.001522

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff687ae147ae148L    # 1.408125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb1103c8e25c811L    # 0.066654

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f62dba4d6e47dc3L    # 0.002302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe615f2452c59fbL    # 0.690179

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4006b506573215fdL    # 2.838391

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402055c358afc47eL    # 8.167506

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033826f9335d24aL    # 19.509515

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ffb98fd5cb790fbL    # 1.724851

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401990e560418937L    # 6.3915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3facf5903a7546d4L    # 0.056561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a500d5e8d5411L    # 8.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb7e2d6238da3c2L    # 0.093305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd080cf9e3864cbL    # 0.257862

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcd3c0c1fc8f323L    # 0.228395

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff67178b3700475L    # 1.402703

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4031afc4199fe436L    # 17.686586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff686f694467382L    # 1.40795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40213e8cef672b88L    # 8.622169

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd65f78359bc340L    # 0.349577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020d02c9081c2e3L    # 8.40659

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40103a53fc0096ffL    # 4.056961

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4037ff0f7b9e0610L    # 23.99633

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd1be9c886162f1L    # 0.277259

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe41c6b05319829L    # 0.628469

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc2ff7596851223L    # 0.148421

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d883ba3443d47L    # 0.00721

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8ea897635e742aL    # 0.01497

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc5325918a009f6L    # 0.165599

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb8755ffe6d58c9L    # 0.095541

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffde9ed3953dea4L    # 1.869611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc22aed1394317bL    # 0.141935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034242ca148ba84L    # 20.141306

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff6a31a4bdba0a5L    # 1.41482

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4e6106ab14ec2L    # 0.163271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd9119ce075f6fdL    # 0.3917

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd3d91eeaa6d267L    # 0.310127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa70ae536501e26L    # 0.045005

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc416b54e2b063eL    # 0.156943

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a3b14a90470a8L    # 0.001601

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4001e09fe86833c6L    # 2.23468

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ecd4aa10e0221L    # 9.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc3c914b167ec78L    # 0.154574

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc21cf7878b7a1cL    # 0.141509

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb41db0142f61edL    # 0.078578

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401abede54b48d3bL    # 6.686395

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff89719f7f8ca82L    # 1.53689

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8ded6ba8c58687L    # 0.014613

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fcdf9830e3cd9a5L    # 0.234177

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f935ac8a3726070L    # 0.018901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f941a261bf37b8dL    # 0.019631

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff45c9a34ca0c28L    # 1.272608

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fccb7cbb3c81909L    # 0.224359

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc399999999999aL    # 0.153125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f50f94c87980f56L    # 0.001036

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffdf3647baa9b4aL    # 1.871922

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd303d9a95421c0L    # 0.29711

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc8d844d013a92aL    # 0.1941

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40510f825e13b18eL    # 68.242332

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7dd4c76d117b53L    # 0.007283

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f94289118c197e5L    # 0.019686

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdd67dbb16c1e36L    # 0.459464

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7e0f7fcfc3f812L    # 0.007339

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc0456f75d9a0f1L    # 0.127119

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff5eb8d82342246L    # 1.370008

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f616440f2389720L    # 0.002123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa84ee7c49fd7a1L    # 0.047477

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f6512a94ff002L    # 0.030659

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fce7064ece9a2c6L    # 0.237805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9053e2d6238da4L    # 0.015945

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa0b1572580c309L    # 0.032603

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41bc126a65cf6L    # 0.157097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa612839042d8c3L    # 0.04311

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f5ad96a6a012L    # 0.155935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc2c764adff822cL    # 0.14671

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400f58cf398e9707L    # 3.918364

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f841e9af5ba2be0L    # 0.009824

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff01dba908a265fL    # 1.007258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd129a1b9f98b72L    # 0.268166

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7db1209edbf8baL    # 0.007249

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4042b9715c63ae25L    # 37.448772

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27b95a294141eaL    # 1.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f192a737110e454L    # 9.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f86da87a072d1afL    # 0.011159

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff00b9fdbd2fa0dL    # 1.002838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8c64345cfede98L    # 0.013863

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fca41a261bf37b9L    # 0.205128

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4fd1569f490603L    # 9.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb6f9767903211dL    # 0.089744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb2c03ff69014b6L    # 0.073246

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd01a6d698fe692L    # 0.251613

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f95cc426351def0L    # 0.021287

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cd

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4000411f4f50a02cL    # 2.031798

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13a92a30553261L    # 7.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8dd5d3dc8b86b1L    # 0.014568

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f71a21ea35935fcL    # 0.004305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cc

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fece364bec679ccL    # 0.902758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cb

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40371701a79fec9aL    # 23.089869

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ca

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4055c9876188b114L    # 87.148888

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc48344c37e6f72L    # 0.160256

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4047c3a75cd0bb6fL    # 47.528545

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cd
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9d45a5fc7e6b40L    # 0.028586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d0

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffba98244e93e1dL    # 1.728884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ce
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f875a74c09c3ce2L    # 0.011403

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d1
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0bb0a2ca9ac36L    # 0.065354

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e5

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc13198288051caL    # 0.134326

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d6

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdda897635e742aL    # 0.463415

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d3

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5b36bd2b6f1993L    # 0.001661

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d3
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f477ea1c68ec52aL    # 7.17E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d4
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402fc37e4de3b8a2L    # 15.881823

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d6
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x4019617b5286b591L    # 6.345197

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e4

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa6e8af81626b2fL    # 0.044744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d8

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40330f29c347e8cdL    # 19.059231

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d8
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa6f9767903211dL    # 0.044872

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e3

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4240746455eaeeL    # 5.57E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1dc

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7cb039ef0f16f4L    # 0.007004

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1da

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f40624dd2f1a9fcL    # 5.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1da
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e03f705857affL    # 4.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1db

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1db
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1dc
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27763e4abe6a33L    # 1.79E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1de

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403032bb6672fba0L    # 16.198172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1dd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1de
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f422749f0e4da0aL    # 5.54E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e0

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020001adea89763L    # 8.000205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1df

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e0
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f29b0ab2e1693c0L    # 1.96E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e2

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f523810e8858ff7L    # 0.001112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
