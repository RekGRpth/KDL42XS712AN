.class public final Lbsm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lbsn;

.field public final b:Lbsp;

.field public final c:Lbsl;


# direct methods
.method private constructor <init>(Lbsn;Lbsp;Lbsl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsn;

    iput-object v0, p0, Lbsm;->a:Lbsn;

    iput-object p2, p0, Lbsm;->b:Lbsp;

    iput-object p3, p0, Lbsm;->c:Lbsl;

    return-void
.end method

.method public static a(Lbsn;Lbsl;)Lbsm;
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lbsn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "result.isSuccess() must be false"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lbsm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lbsm;-><init>(Lbsn;Lbsp;Lbsl;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lbsn;Lbsp;)Lbsm;
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lbsn;->a()Z

    move-result v0

    const-string v1, "result.isSuccess() must be true"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lbsm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lbsm;-><init>(Lbsn;Lbsp;Lbsl;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AuthStatus [result="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbsm;->a:Lbsn;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbsm;->b:Lbsp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failureCause="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbsm;->c:Lbsl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
