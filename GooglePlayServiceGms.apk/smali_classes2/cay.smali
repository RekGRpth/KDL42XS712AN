.class public final Lcay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(Ljava/util/Calendar;)V
    .locals 14

    const/4 v13, 0x1

    const-wide/32 v11, 0x5265c00

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v8

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcay;->a:J

    iget-wide v5, p0, Lcay;->a:J

    sub-long/2addr v5, v11

    iput-wide v5, p0, Lcay;->b:J

    iget-wide v5, p0, Lcay;->a:J

    add-int/lit8 v2, v7, -0x1

    int-to-long v9, v2

    mul-long/2addr v9, v11

    sub-long/2addr v5, v9

    iput-wide v5, p0, Lcay;->c:J

    iget-wide v5, p0, Lcay;->a:J

    add-int/lit8 v2, v3, -0x1

    int-to-long v2, v2

    mul-long/2addr v2, v11

    sub-long v2, v5, v2

    iput-wide v2, p0, Lcay;->d:J

    iget-wide v2, p0, Lcay;->a:J

    add-int/lit8 v5, v8, -0x1

    int-to-long v5, v5

    mul-long/2addr v5, v11

    sub-long/2addr v2, v5

    iput-wide v2, p0, Lcay;->e:J

    add-int/lit8 v1, v1, -0x1

    move v2, v4

    move v3, v13

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcay;->f:J

    return-void
.end method


# virtual methods
.method public final a(J)Lcax;
    .locals 2

    iget-wide v0, p0, Lcay;->a:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    sget-object v0, Lcax;->a:Lcax;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcay;->b:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    sget-object v0, Lcax;->b:Lcax;

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcay;->c:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    sget-object v0, Lcax;->c:Lcax;

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lcay;->d:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    sget-object v0, Lcax;->d:Lcax;

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lcay;->e:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_4

    sget-object v0, Lcax;->e:Lcax;

    goto :goto_0

    :cond_4
    iget-wide v0, p0, Lcay;->f:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_5

    sget-object v0, Lcax;->f:Lcax;

    goto :goto_0

    :cond_5
    sget-object v0, Lcax;->g:Lcax;

    goto :goto_0
.end method
