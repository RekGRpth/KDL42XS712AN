.class public final Lhwx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final a:Landroid/hardware/SensorManager;

.field private final b:Landroid/hardware/Sensor;

.field private final c:Landroid/os/Handler;

.field private final d:Lixb;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/hardware/SensorManager;Lixd;Landroid/os/Handler;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lixb;

    invoke-direct {v0}, Lixb;-><init>()V

    iput-object v0, p0, Lhwx;->d:Lixb;

    iput-object p1, p0, Lhwx;->a:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lhwx;->a:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lhwx;->b:Landroid/hardware/Sensor;

    iget-object v0, p0, Lhwx;->d:Lixb;

    iput-object p2, v0, Lixb;->j:Lixd;

    iput-object p3, p0, Lhwx;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lhwx;->b:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwx;->a:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lhwx;->b:Landroid/hardware/Sensor;

    const/16 v2, 0x4e20

    iget-object v3, p0, Lhwx;->c:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhwx;->b:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwx;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhwx;->e:J

    return-void
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 14

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v2, p0, Lhwx;->e:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf42400

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Lhwx;->e:J

    iget-object v0, p0, Lhwx;->d:Lixb;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lhwx;->d:Lixb;

    iget-wide v3, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v5, v0, v1

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x1

    aget v6, v0, v1

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v7, v0, v1

    iget-object v8, v2, Lixb;->c:Lixf;

    iget-object v0, v8, Lixf;->a:Liwo;

    invoke-virtual {v0, v5}, Liwo;->a(F)V

    iget-object v0, v8, Lixf;->b:Liwo;

    invoke-virtual {v0, v6}, Liwo;->a(F)V

    iget-object v0, v8, Lixf;->c:Liwo;

    invoke-virtual {v0, v7}, Liwo;->a(F)V

    iget-object v0, v8, Lixf;->d:[F

    iput-object v0, v8, Lixf;->e:[F

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x0

    iget-object v9, v8, Lixf;->a:Liwo;

    iget v9, v9, Liwo;->b:F

    iget-object v10, v8, Lixf;->a:Liwo;

    iget v10, v10, Liwo;->d:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    aput v9, v0, v1

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x1

    iget-object v9, v8, Lixf;->b:Liwo;

    iget v9, v9, Liwo;->b:F

    iget-object v10, v8, Lixf;->b:Liwo;

    iget v10, v10, Liwo;->d:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    aput v9, v0, v1

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x2

    iget-object v9, v8, Lixf;->c:Liwo;

    iget v9, v9, Liwo;->b:F

    iget-object v10, v8, Lixf;->c:Liwo;

    iget v10, v10, Liwo;->d:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    aput v9, v0, v1

    iget-object v9, v8, Lixf;->e:[F

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_1
    array-length v10, v9

    if-ge v0, v10, :cond_1

    aget v10, v9, v0

    aget v11, v9, v0

    mul-float/2addr v10, v11

    add-float/2addr v1, v10

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    float-to-double v0, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, v8, Lixf;->f:F

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x0

    iget-object v9, v8, Lixf;->e:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    iget v10, v8, Lixf;->f:F

    div-float/2addr v9, v10

    aput v9, v0, v1

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x1

    iget-object v9, v8, Lixf;->e:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    iget v10, v8, Lixf;->f:F

    div-float/2addr v9, v10

    aput v9, v0, v1

    iget-object v0, v8, Lixf;->e:[F

    const/4 v1, 0x2

    iget-object v9, v8, Lixf;->e:[F

    const/4 v10, 0x2

    aget v9, v9, v10

    iget v8, v8, Lixf;->f:F

    div-float v8, v9, v8

    aput v8, v0, v1

    iget-object v0, v2, Lixb;->c:Lixf;

    iget-object v0, v0, Lixf;->e:[F

    iget-object v1, v2, Lixb;->h:[F

    const/4 v8, 0x0

    aput v5, v1, v8

    iget-object v1, v2, Lixb;->h:[F

    const/4 v5, 0x1

    aput v6, v1, v5

    iget-object v1, v2, Lixb;->h:[F

    const/4 v5, 0x2

    aput v7, v1, v5

    iget-object v1, v2, Lixb;->h:[F

    const/4 v5, 0x0

    aget v5, v0, v5

    const/4 v6, 0x0

    aget v6, v1, v6

    mul-float/2addr v5, v6

    const/4 v6, 0x1

    aget v6, v0, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    const/4 v6, 0x2

    aget v0, v0, v6

    const/4 v6, 0x2

    aget v1, v1, v6

    mul-float/2addr v0, v1

    add-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v0, v2, Lixb;->d:Liwo;

    iget v0, v0, Liwo;->d:I

    iget v5, v2, Lixb;->a:I

    if-le v0, v5, :cond_4

    const/4 v0, 0x1

    :goto_2
    mul-float v5, v1, v1

    iget-object v6, v2, Lixb;->d:Liwo;

    invoke-virtual {v6, v5}, Liwo;->a(F)V

    iget-object v6, v2, Lixb;->e:Liwo;

    invoke-virtual {v6, v1}, Liwo;->a(F)V

    iget-object v6, v2, Lixb;->e:Liwo;

    invoke-static {v6}, Lixb;->a(Lixe;)F

    move-result v6

    iget-object v7, v2, Lixb;->d:Liwo;

    invoke-static {v7, v6}, Lixb;->a(Lixe;F)F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v7, v8

    if-gtz v8, :cond_2

    if-nez v0, :cond_3

    :cond_2
    iget-object v8, v2, Lixb;->f:Lixe;

    invoke-virtual {v8, v5}, Lixe;->a(F)V

    iget-object v5, v2, Lixb;->g:Lixe;

    invoke-virtual {v5, v1}, Lixe;->a(F)V

    :cond_3
    if-eqz v0, :cond_0

    iget-object v0, v2, Lixb;->g:Lixe;

    invoke-static {v0}, Lixb;->a(Lixe;)F

    move-result v1

    iget-object v0, v2, Lixb;->f:Lixe;

    invoke-static {v0, v1}, Lixb;->a(Lixe;F)F

    move-result v5

    iget-object v8, v2, Lixb;->e:Liwo;

    iget v0, v2, Lixb;->a:I

    iget v9, v8, Liwo;->d:I

    if-lt v0, v9, :cond_5

    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " >= size "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v8, Liwo;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    iget v9, v8, Liwo;->c:I

    add-int/lit8 v9, v9, -0x1

    sub-int v0, v9, v0

    if-gez v0, :cond_6

    iget-object v9, v8, Liwo;->a:[F

    array-length v9, v9

    add-int/2addr v0, v9

    :cond_6
    iget-object v8, v8, Liwo;->a:[F

    aget v0, v8, v0

    const/high16 v8, 0x41700000    # 15.0f

    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v8

    iget-object v9, v2, Lixb;->i:Lixc;

    if-nez v9, :cond_9

    new-instance v9, Lixc;

    invoke-direct {v9, v3, v4, v8}, Lixc;-><init>(JF)V

    iput-object v9, v2, Lixb;->i:Lixc;

    :goto_3
    cmpl-float v9, v0, v6

    if-ltz v9, :cond_0

    sub-float v6, v0, v6

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v9, 0x3fe999999999999aL    # 0.8

    float-to-double v5, v5

    mul-double/2addr v5, v9

    cmpl-double v0, v0, v5

    if-lez v0, :cond_0

    iget v0, v2, Lixb;->b:F

    cmpl-float v0, v7, v0

    if-lez v0, :cond_0

    iget-wide v0, v2, Lixb;->k:J

    const-wide/16 v5, 0x0

    cmp-long v0, v0, v5

    if-lez v0, :cond_a

    iget v0, v2, Lixb;->m:F

    invoke-static {v8, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-wide v5, v2, Lixb;->l:J

    sub-long v5, v3, v5

    long-to-float v1, v5

    mul-float/2addr v0, v1

    iget-object v1, v2, Lixb;->i:Lixc;

    iget v1, v1, Lixc;->a:F

    sub-float/2addr v0, v1

    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v5, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v0, v5

    if-gtz v0, :cond_7

    iget-wide v0, v2, Lixb;->k:J

    sub-long v0, v3, v0

    const-wide/32 v5, 0x17d78400

    cmp-long v0, v0, v5

    if-lez v0, :cond_8

    :cond_7
    const-wide/16 v0, 0x0

    iput-wide v0, v2, Lixb;->k:J

    :cond_8
    :goto_4
    iget-object v0, v2, Lixb;->i:Lixc;

    const/4 v1, 0x0

    iput v1, v0, Lixc;->a:F

    iput-wide v3, v2, Lixb;->l:J

    iput v8, v2, Lixb;->m:F

    goto/16 :goto_0

    :cond_9
    iget-object v9, v2, Lixb;->i:Lixc;

    iget v10, v9, Lixc;->a:F

    iget v11, v9, Lixc;->c:F

    add-float/2addr v11, v8

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iget-wide v12, v9, Lixc;->b:J

    sub-long v12, v3, v12

    long-to-float v12, v12

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    iput v10, v9, Lixc;->a:F

    iput v8, v9, Lixc;->c:F

    iput-wide v3, v9, Lixc;->b:J

    goto :goto_3

    :cond_a
    iput-wide v3, v2, Lixb;->k:J

    iget-object v0, v2, Lixb;->j:Lixd;

    if-eqz v0, :cond_8

    iget-object v0, v2, Lixb;->j:Lixd;

    invoke-interface {v0, v3, v4}, Lixd;->a(J)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
