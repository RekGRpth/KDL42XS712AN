.class public final Lion;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile e:[Lion;


# instance fields
.field public a:Lioj;

.field public b:Lipv;

.field public c:Liob;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lion;->a:Lioj;

    iput-object v0, p0, Lion;->b:Lipv;

    iput-object v0, p0, Lion;->c:Liob;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lion;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lion;->C:I

    return-void
.end method

.method public static c()[Lion;
    .locals 2

    sget-object v0, Lion;->e:[Lion;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lion;->e:[Lion;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lion;

    sput-object v0, Lion;->e:[Lion;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lion;->e:[Lion;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lion;->a:Lioj;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lion;->a:Lioj;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lion;->c:Liob;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lion;->c:Liob;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lion;->d:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lion;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lion;->b:Lipv;

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lion;->b:Lipv;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lion;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lion;->a:Lioj;

    if-nez v0, :cond_1

    new-instance v0, Lioj;

    invoke-direct {v0}, Lioj;-><init>()V

    iput-object v0, p0, Lion;->a:Lioj;

    :cond_1
    iget-object v0, p0, Lion;->a:Lioj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lion;->c:Liob;

    if-nez v0, :cond_2

    new-instance v0, Liob;

    invoke-direct {v0}, Liob;-><init>()V

    iput-object v0, p0, Lion;->c:Liob;

    :cond_2
    iget-object v0, p0, Lion;->c:Liob;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lion;->d:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lion;->b:Lipv;

    if-nez v0, :cond_3

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    iput-object v0, p0, Lion;->b:Lipv;

    :cond_3
    iget-object v0, p0, Lion;->b:Lipv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Lion;->a:Lioj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lion;->a:Lioj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Lion;->c:Liob;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lion;->c:Liob;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-boolean v0, p0, Lion;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lion;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_2
    iget-object v0, p0, Lion;->b:Lipv;

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lion;->b:Lipv;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lion;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lion;

    iget-object v2, p0, Lion;->a:Lioj;

    if-nez v2, :cond_3

    iget-object v2, p1, Lion;->a:Lioj;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lion;->a:Lioj;

    iget-object v3, p1, Lion;->a:Lioj;

    invoke-virtual {v2, v3}, Lioj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lion;->b:Lipv;

    if-nez v2, :cond_5

    iget-object v2, p1, Lion;->b:Lipv;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lion;->b:Lipv;

    iget-object v3, p1, Lion;->b:Lipv;

    invoke-virtual {v2, v3}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lion;->c:Liob;

    if-nez v2, :cond_7

    iget-object v2, p1, Lion;->c:Liob;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lion;->c:Liob;

    iget-object v3, p1, Lion;->c:Liob;

    invoke-virtual {v2, v3}, Liob;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Lion;->d:Z

    iget-boolean v3, p1, Lion;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lion;->a:Lioj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lion;->b:Lipv;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lion;->c:Liob;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lion;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lion;->a:Lioj;

    invoke-virtual {v0}, Lioj;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lion;->b:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lion;->c:Liob;

    invoke-virtual {v1}, Liob;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method
