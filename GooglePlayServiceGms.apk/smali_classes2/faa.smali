.class public final Lfaa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# static fields
.field private static final a:D


# instance fields
.field private final A:[F

.field private final B:[F

.field private final C:[F

.field private final D:[F

.field private final E:[F

.field private final F:[F

.field private final G:[F

.field private final H:Leyk;

.field private final I:Leyk;

.field private final J:Leyj;

.field private final K:[I

.field private L:Lezf;

.field private M:Z

.field private N:Lezl;

.field private O:Lezx;

.field private P:Lezl;

.field private Q:Z

.field private R:F

.field private final S:Landroid/graphics/RectF;

.field private final T:Landroid/content/Context;

.field private b:Lezd;

.field private c:Z

.field private d:Lezw;

.field private e:Z

.field private f:I

.field private g:I

.field private final h:I

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private final p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:Leyv;

.field private x:Lezb;

.field private y:Leyx;

.field private z:Lezk;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sput-wide v0, Lfaa;->a:D

    return-void
.end method

.method public constructor <init>(Lezx;Lezw;Landroid/content/Context;I)V
    .locals 7

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v2, 0x10

    const/4 v6, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v6, p0, Lfaa;->c:Z

    iput-boolean v6, p0, Lfaa;->e:Z

    const/high16 v0, 0x42960000    # 75.0f

    iput v0, p0, Lfaa;->i:F

    iget v0, p0, Lfaa;->i:F

    iput v0, p0, Lfaa;->j:F

    iput v1, p0, Lfaa;->k:F

    iput v1, p0, Lfaa;->l:F

    iget v0, p0, Lfaa;->i:F

    iput v0, p0, Lfaa;->m:F

    iput v1, p0, Lfaa;->n:F

    iput v1, p0, Lfaa;->o:F

    iput v1, p0, Lfaa;->p:F

    iput v1, p0, Lfaa;->q:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lfaa;->r:F

    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lfaa;->s:F

    iput v1, p0, Lfaa;->t:F

    iput v1, p0, Lfaa;->u:F

    iput v1, p0, Lfaa;->v:F

    new-array v0, v2, [F

    iput-object v0, p0, Lfaa;->A:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lfaa;->B:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lfaa;->C:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lfaa;->D:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lfaa;->E:[F

    new-array v0, v4, [F

    iput-object v0, p0, Lfaa;->F:[F

    new-array v0, v4, [F

    iput-object v0, p0, Lfaa;->G:[F

    new-instance v0, Leyk;

    invoke-direct {v0}, Leyk;-><init>()V

    iput-object v0, p0, Lfaa;->H:Leyk;

    new-instance v0, Leyk;

    invoke-direct {v0}, Leyk;-><init>()V

    iput-object v0, p0, Lfaa;->I:Leyk;

    new-instance v0, Leyj;

    invoke-direct {v0}, Leyj;-><init>()V

    iput-object v0, p0, Lfaa;->J:Leyj;

    new-array v0, v4, [I

    iput-object v0, p0, Lfaa;->K:[I

    iput-object v3, p0, Lfaa;->L:Lezf;

    iput-boolean v6, p0, Lfaa;->M:Z

    iput-object v3, p0, Lfaa;->N:Lezl;

    iput-object v3, p0, Lfaa;->O:Lezx;

    iput-object v3, p0, Lfaa;->P:Lezl;

    iput-boolean v6, p0, Lfaa;->Q:Z

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lfaa;->R:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lfaa;->S:Landroid/graphics/RectF;

    iput-object p3, p0, Lfaa;->T:Landroid/content/Context;

    const v0, 0x7f0201e5    # com.google.android.gms.R.drawable.panorama_cubemap

    iput v0, p0, Lfaa;->h:I

    iput-object p1, p0, Lfaa;->O:Lezx;

    if-eqz p2, :cond_1

    iput-object p2, p0, Lfaa;->d:Lezw;

    iget-object v0, p0, Lfaa;->y:Leyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaa;->y:Leyx;

    invoke-virtual {v0}, Leyx;->a()V

    :cond_0
    iget-object v0, p0, Lfaa;->d:Lezw;

    iget v0, v0, Lezw;->b:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget-object v2, p0, Lfaa;->d:Lezw;

    iget v2, v2, Lezw;->f:F

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget-object v2, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lfaa;->d:Lezw;

    iget v0, v0, Lezw;->c:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    iget-object v3, p0, Lfaa;->d:Lezw;

    iget v3, v3, Lezw;->e:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    double-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget-object v2, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v0, v2, v0

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0}, Lfaa;->k()V

    new-instance v0, Leyx;

    iget-object v1, p0, Lfaa;->d:Lezw;

    iget-object v1, v1, Lezw;->a:Lfaf;

    invoke-direct {v0, v1}, Leyx;-><init>(Lfaf;)V

    iput-object v0, p0, Lfaa;->y:Leyx;

    iget-object v0, p0, Lfaa;->y:Leyx;

    new-instance v1, Lfab;

    invoke-direct {v1, p0}, Lfab;-><init>(Lfaa;)V

    iput-object v1, v0, Leyx;->f:Lezl;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfaa;->Q:Z

    iget v0, p0, Lfaa;->f:I

    if-lez v0, :cond_1

    iget v0, p0, Lfaa;->g:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lfaa;->O:Lezx;

    invoke-virtual {v0}, Lezx;->requestRender()V

    :cond_1
    iget-object v0, p0, Lfaa;->E:[F

    invoke-static {v0, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    return-void
.end method

.method private static a(FFF)F
    .locals 6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    float-to-double v0, p1

    float-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    float-to-double v2, p2

    mul-double/2addr v0, v4

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method static synthetic a(Lfaa;)Lezx;
    .locals 1

    iget-object v0, p0, Lfaa;->O:Lezx;

    return-object v0
.end method

.method private a(FF[F)V
    .locals 11

    const/4 v4, 0x0

    iget v0, p0, Lfaa;->g:I

    int-to-float v0, v0

    sub-float v1, v0, p2

    iget-object v0, p0, Lfaa;->K:[I

    aput v4, v0, v4

    iget-object v0, p0, Lfaa;->K:[I

    const/4 v2, 0x1

    aput v4, v0, v2

    iget-object v0, p0, Lfaa;->K:[I

    const/4 v2, 0x2

    iget v3, p0, Lfaa;->f:I

    aput v3, v0, v2

    iget-object v0, p0, Lfaa;->K:[I

    const/4 v2, 0x3

    iget v3, p0, Lfaa;->g:I

    aput v3, v0, v2

    const/high16 v2, -0x40800000    # -1.0f

    iget-object v3, p0, Lfaa;->B:[F

    iget-object v5, p0, Lfaa;->A:[F

    iget-object v7, p0, Lfaa;->K:[I

    move v0, p1

    move v6, v4

    move v8, v4

    move-object v9, p3

    move v10, v4

    invoke-static/range {v0 .. v10}, Landroid/opengl/GLU;->gluUnProject(FFF[FI[FI[II[FI)I

    return-void
.end method

.method private c(F)V
    .locals 4

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lfaa;->m:F

    iget v1, p0, Lfaa;->f:I

    iget v2, p0, Lfaa;->g:I

    if-le v1, v2, :cond_0

    iput v0, p0, Lfaa;->k:F

    iget v0, p0, Lfaa;->k:F

    iget v1, p0, Lfaa;->f:I

    int-to-float v1, v1

    iget v2, p0, Lfaa;->g:I

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lfaa;->a(FFF)F

    move-result v0

    iput v0, p0, Lfaa;->l:F

    iget v0, p0, Lfaa;->k:F

    iput v0, p0, Lfaa;->j:F

    :goto_0
    const v0, 0x3eb33333    # 0.35f

    iget v1, p0, Lfaa;->k:F

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lfaa;->s:F

    iget v0, p0, Lfaa;->m:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    sget-wide v2, Lfaa;->a:D

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lfaa;->v:F

    invoke-direct {p0}, Lfaa;->k()V

    invoke-direct {p0}, Lfaa;->g()V

    return-void

    :cond_0
    iput v0, p0, Lfaa;->l:F

    iget v1, p0, Lfaa;->g:I

    int-to-float v1, v1

    iget v2, p0, Lfaa;->f:I

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lfaa;->a(FFF)F

    move-result v0

    iput v0, p0, Lfaa;->k:F

    iget v0, p0, Lfaa;->l:F

    iput v0, p0, Lfaa;->j:F

    goto :goto_0
.end method

.method private declared-synchronized g()V
    .locals 11

    const/high16 v5, 0x3f000000    # 0.5f

    const/16 v10, 0x10

    const/4 v9, 0x0

    const/4 v8, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lfaa;->f:I

    if-lez v0, :cond_0

    iget v0, p0, Lfaa;->g:I

    if-lez v0, :cond_0

    iget v0, p0, Lfaa;->l:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v0, v0, v9

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lfaa;->f:I

    int-to-float v0, v0

    iget v1, p0, Lfaa;->g:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lfaa;->l:F

    float-to-double v1, v1

    const-wide v3, 0x4076800000000000L    # 360.0

    div-double/2addr v1, v3

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v5, v1

    mul-float v3, v5, v0

    iget-object v0, p0, Lfaa;->A:[F

    const/4 v1, 0x0

    neg-float v2, v3

    neg-float v4, v5

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v7, 0x43480000    # 200.0f

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    iget-object v0, p0, Lfaa;->B:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iget v0, p0, Lfaa;->q:F

    iget v1, p0, Lfaa;->q:F

    sub-float v1, v9, v1

    const v2, 0x3da3d70a    # 0.08f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lfaa;->q:F

    iget-boolean v0, p0, Lfaa;->M:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfaa;->b:Lezd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfaa;->b:Lezd;

    iget v0, v0, Lezd;->a:I

    int-to-float v0, v0

    iget-object v1, p0, Lfaa;->L:Lezf;

    iget v1, v1, Lezf;->m:F

    sub-float v2, v0, v1

    iget-object v0, p0, Lfaa;->B:[F

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    :cond_2
    iget-object v9, p0, Lfaa;->L:Lezf;

    iget-object v0, v9, Lezf;->k:Leze;

    invoke-virtual {v0}, Leze;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v9, Lezf;->k:Leze;

    invoke-virtual {v0}, Leze;->c()[D

    move-result-object v0

    iput-object v0, v9, Lezf;->p:[D

    :cond_3
    const/16 v0, 0x10

    new-array v0, v0, [F

    move v1, v8

    :goto_1
    if-ge v1, v10, :cond_4

    iget-object v2, v9, Lezf;->p:[D

    aget-wide v2, v2, v1

    double-to-float v2, v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const/16 v1, 0x10

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    const/4 v2, 0x0

    iget v3, v9, Lezf;->m:F

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v2, v9, Lezf;->l:[F

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v4, v1

    move-object v6, v0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v0, v9, Lezf;->l:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lfaa;->E:[F

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v4, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v1, p0, Lfaa;->D:[F

    const/4 v2, 0x0

    iget-object v3, p0, Lfaa;->B:[F

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v5, v0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v0, p0, Lfaa;->D:[F

    const/4 v1, 0x0

    iget v2, p0, Lfaa;->n:F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Lfaa;->D:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lfaa;->B:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lfaa;->D:[F

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lfaa;->C:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lfaa;->A:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lfaa;->D:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lfaa;->B:[F

    const/4 v1, 0x0

    iget v2, p0, Lfaa;->q:F

    neg-float v2, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Lfaa;->B:[F

    const/4 v1, 0x0

    iget v2, p0, Lfaa;->o:F

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Lfaa;->B:[F

    const/4 v1, 0x0

    iget v2, p0, Lfaa;->n:F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Lfaa;->C:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lfaa;->A:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lfaa;->B:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private h()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-boolean v5, p0, Lfaa;->Q:Z

    new-instance v0, Lezk;

    invoke-direct {v0}, Lezk;-><init>()V

    iput-object v0, p0, Lfaa;->z:Lezk;

    iget-object v0, p0, Lfaa;->d:Lezw;

    iget-object v0, v0, Lezw;->a:Lfaf;

    invoke-static {}, Lfaa;->i()I

    move-result v1

    invoke-interface {v0, v1}, Lfaf;->a(I)V

    new-instance v0, Leyv;

    iget-object v1, p0, Lfaa;->y:Leyx;

    invoke-direct {v0, v1}, Leyv;-><init>(Leyx;)V

    iput-object v0, p0, Lfaa;->w:Leyv;

    iget-object v0, p0, Lfaa;->w:Leyv;

    iget-object v1, p0, Lfaa;->d:Lezw;

    invoke-virtual {v0, v1}, Leyv;->a(Lezw;)V

    iget-object v0, p0, Lfaa;->w:Leyv;

    iget-object v1, p0, Lfaa;->z:Lezk;

    invoke-virtual {v0, v1}, Leyv;->a(Leyw;)V

    sget-object v0, Lexx;->f:[F

    aget v0, v0, v5

    sget-object v1, Lexx;->f:[F

    aget v1, v1, v6

    sget-object v2, Lexx;->f:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    sget-object v3, Lexx;->f:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iget-object v1, p0, Lfaa;->T:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lfaa;->h:I

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Lezb;

    invoke-direct {v1, v0}, Lezb;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lfaa;->x:Lezb;

    iput-boolean v6, p0, Lfaa;->c:Z

    iget-object v0, p0, Lfaa;->P:Lezl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaa;->P:Lezl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lezl;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static i()I
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0xd33

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v0, v0, v2

    return v0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lfaa;->N:Lezl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaa;->N:Lezl;

    iget-boolean v1, p0, Lfaa;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lezl;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lfaa;->O:Lezx;

    invoke-virtual {v0}, Lezx;->requestRender()V

    return-void
.end method

.method private k()V
    .locals 7

    const/high16 v4, 0x42b40000    # 90.0f

    const/high16 v6, 0x40000000    # 2.0f

    iget-object v0, p0, Lfaa;->d:Lezw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaa;->w:Leyv;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfaa;->d:Lezw;

    iget-object v0, v0, Lezw;->i:Lezp;

    iget v0, v0, Lezp;->o:F

    iget-object v1, p0, Lfaa;->d:Lezw;

    iget-object v1, v1, Lezw;->i:Lezp;

    iget v1, v1, Lezp;->n:F

    const/high16 v2, 0x41a00000    # 20.0f

    iget v3, p0, Lfaa;->j:F

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iput v2, p0, Lfaa;->R:F

    iget v2, p0, Lfaa;->o:F

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lfaa;->o:F

    iget-object v2, p0, Lfaa;->d:Lezw;

    iget v2, v2, Lezw;->b:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-gez v2, :cond_0

    iget v2, p0, Lfaa;->k:F

    div-float/2addr v2, v6

    iget v3, p0, Lfaa;->l:F

    div-float/2addr v3, v6

    iget-object v4, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lfaa;->R:F

    sub-float/2addr v4, v5

    add-float/2addr v4, v2

    iget v5, p0, Lfaa;->n:F

    add-float/2addr v4, v1

    invoke-static {v5, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iput v4, p0, Lfaa;->n:F

    iget-object v4, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget v5, p0, Lfaa;->R:F

    add-float/2addr v4, v5

    sub-float v2, v4, v2

    iget v4, p0, Lfaa;->n:F

    add-float/2addr v1, v2

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lfaa;->n:F

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lfaa;->R:F

    sub-float/2addr v1, v2

    add-float/2addr v1, v3

    iget v2, p0, Lfaa;->o:F

    neg-float v2, v2

    add-float/2addr v1, v0

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    neg-float v1, v1

    iput v1, p0, Lfaa;->o:F

    iget-object v1, p0, Lfaa;->S:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lfaa;->R:F

    add-float/2addr v1, v2

    sub-float/2addr v1, v3

    iget v2, p0, Lfaa;->o:F

    neg-float v2, v2

    add-float/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    neg-float v0, v0

    iput v0, p0, Lfaa;->o:F

    invoke-direct {p0}, Lfaa;->g()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lfaa;->u:F

    const/high16 v0, -0x3e100000    # -30.0f

    iput v0, p0, Lfaa;->n:F

    invoke-direct {p0}, Lfaa;->k()V

    const/4 v0, 0x0

    iput v0, p0, Lfaa;->r:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfaa;->e:Z

    invoke-direct {p0}, Lfaa;->j()V

    return-void
.end method

.method public final a(F)V
    .locals 1

    iget v0, p0, Lfaa;->i:F

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lfaa;->c(F)V

    return-void
.end method

.method public final a(FF)V
    .locals 1

    iget-boolean v0, p0, Lfaa;->M:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lfaa;->o:F

    iput p2, p0, Lfaa;->n:F

    invoke-direct {p0}, Lfaa;->k()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfaa;->e:Z

    invoke-direct {p0}, Lfaa;->g()V

    invoke-direct {p0}, Lfaa;->j()V

    goto :goto_0
.end method

.method public final a(FFLeyj;)V
    .locals 6

    iget-object v0, p0, Lfaa;->G:[F

    invoke-direct {p0, p1, p2, v0}, Lfaa;->a(FF[F)V

    iget-object v0, p0, Lfaa;->G:[F

    iget-object v1, p0, Lfaa;->I:Leyk;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v0, v0, v4

    invoke-virtual {v1, v2, v3, v0}, Leyk;->a(FFF)V

    iget-object v0, p0, Lfaa;->I:Leyk;

    invoke-virtual {v0}, Leyk;->a()F

    iget-object v0, p0, Lfaa;->I:Leyk;

    iget v0, v0, Leyk;->b:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    double-to-float v1, v0

    iget-object v0, p0, Lfaa;->I:Leyk;

    iget v0, v0, Leyk;->a:F

    float-to-double v2, v0

    iget-object v0, p0, Lfaa;->I:Leyk;

    iget v0, v0, Leyk;->c:F

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v0, v2

    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    float-to-double v2, v0

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v2, v4

    double-to-float v0, v2

    :cond_0
    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, p3, Leyj;->a:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p3, Leyj;->b:F

    return-void
.end method

.method public final a(Lezd;Lezf;)V
    .locals 0

    iput-object p1, p0, Lfaa;->b:Lezd;

    iput-object p2, p0, Lfaa;->L:Lezf;

    return-void
.end method

.method public final a(Lezl;)V
    .locals 0

    iput-object p1, p0, Lfaa;->N:Lezl;

    return-void
.end method

.method public final a(Z)V
    .locals 3

    iput-boolean p1, p0, Lfaa;->M:Z

    iget-boolean v0, p0, Lfaa;->M:Z

    if-nez v0, :cond_0

    iget v0, p0, Lfaa;->g:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lfaa;->f:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    int-to-float v0, v0

    iget-object v2, p0, Lfaa;->J:Leyj;

    invoke-virtual {p0, v1, v0, v2}, Lfaa;->a(FFLeyj;)V

    iget-object v0, p0, Lfaa;->J:Leyj;

    iget v0, v0, Leyj;->a:F

    neg-float v0, v0

    iput v0, p0, Lfaa;->o:F

    const/high16 v0, 0x43340000    # 180.0f

    iget-object v1, p0, Lfaa;->J:Leyj;

    iget v1, v1, Leyj;->b:F

    sub-float/2addr v0, v1

    iput v0, p0, Lfaa;->n:F

    invoke-direct {p0}, Lfaa;->k()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lfaa;->o:F

    invoke-direct {p0}, Lfaa;->k()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfaa;->e:Z

    invoke-direct {p0}, Lfaa;->g()V

    invoke-direct {p0}, Lfaa;->j()V

    return-void
.end method

.method public final b(F)V
    .locals 1

    iget v0, p0, Lfaa;->i:F

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lfaa;->c(F)V

    iget v0, p0, Lfaa;->j:F

    iput v0, p0, Lfaa;->i:F

    return-void
.end method

.method public final b(Lezl;)V
    .locals 1

    iput-object p1, p0, Lfaa;->P:Lezl;

    iget-boolean v0, p0, Lfaa;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lezl;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lfaa;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfaa;->e:Z

    invoke-direct {p0}, Lfaa;->j()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()F
    .locals 1

    iget v0, p0, Lfaa;->n:F

    return v0
.end method

.method public final e()F
    .locals 1

    iget v0, p0, Lfaa;->o:F

    return v0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lfaa;->y:Leyx;

    invoke-virtual {v0}, Leyx;->a()V

    return-void
.end method

.method public final onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    iget-boolean v0, p0, Lfaa;->Q:Z

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lfaa;->h()V
    :try_end_0
    .catch Leyu; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lfaa;->d:Lezw;

    iget-object v0, v0, Lezw;->a:Lfaf;

    invoke-static {}, Lfaa;->i()I

    move-result v1

    invoke-interface {v0, v1}, Lfaf;->a(I)V

    iget-object v0, p0, Lfaa;->w:Leyv;

    iget-object v1, p0, Lfaa;->d:Lezw;

    invoke-virtual {v0, v1}, Leyv;->a(Lezw;)V

    :cond_0
    iget-boolean v0, p0, Lfaa;->c:Z

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Leyu;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfaa;->O:Lezx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfaa;->O:Lezx;

    invoke-virtual {v0}, Lezx;->a()V

    :cond_3
    iget-boolean v0, p0, Lfaa;->e:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lfaa;->n:F

    iget v1, p0, Lfaa;->s:F

    add-float/2addr v0, v1

    iput v0, p0, Lfaa;->n:F

    iput v3, p0, Lfaa;->t:F

    iget v0, p0, Lfaa;->u:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    iget v0, p0, Lfaa;->u:F

    iget v1, p0, Lfaa;->s:F

    sub-float/2addr v0, v1

    iput v0, p0, Lfaa;->u:F

    iget v0, p0, Lfaa;->u:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_4

    iput-boolean v4, p0, Lfaa;->e:Z

    iput v3, p0, Lfaa;->t:F

    invoke-direct {p0}, Lfaa;->j()V

    :cond_4
    invoke-direct {p0}, Lfaa;->g()V

    invoke-direct {p0}, Lfaa;->k()V

    iget-object v0, p0, Lfaa;->O:Lezx;

    invoke-virtual {v0}, Lezx;->requestRender()V

    :cond_5
    :goto_2
    iget v0, p0, Lfaa;->r:F

    iget v1, p0, Lfaa;->r:F

    sub-float v1, v3, v1

    const v2, 0x3d4ccccd    # 0.05f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lfaa;->r:F

    iget-boolean v0, p0, Lfaa;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfaa;->M:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lfaa;->g()V

    :cond_6
    iget v0, p0, Lfaa;->f:I

    iget v1, p0, Lfaa;->g:I

    invoke-static {v4, v4, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0x100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    :try_start_1
    iget-object v0, p0, Lfaa;->x:Lezb;

    iget-object v1, p0, Lfaa;->C:[F

    invoke-virtual {v0, v1}, Lezb;->b([F)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Landroid/opengl/GLES20;->glLineWidth(F)V

    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    iget-object v0, p0, Lfaa;->z:Lezk;

    invoke-virtual {v0}, Lezk;->a()V

    iget-object v0, p0, Lfaa;->z:Lezk;

    iget v1, p0, Lfaa;->r:F

    invoke-virtual {v0, v1}, Lezk;->a(F)V

    iget-object v0, p0, Lfaa;->H:Leyk;

    iget v1, p0, Lfaa;->f:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lfaa;->g:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lfaa;->F:[F

    invoke-direct {p0, v1, v2, v3}, Lfaa;->a(FF[F)V

    iget-object v1, p0, Lfaa;->F:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lfaa;->F:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lfaa;->F:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Leyk;->a(FFF)V

    invoke-virtual {v0}, Leyk;->a()F

    iget-object v0, p0, Lfaa;->w:Leyv;

    iget-object v1, p0, Lfaa;->C:[F

    iget-object v2, p0, Lfaa;->H:Leyk;

    iget v3, p0, Lfaa;->v:F

    invoke-virtual {v0, v1, v2, v3}, Leyv;->a([FLeyk;F)V
    :try_end_1
    .catch Leyu; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Leyu;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    iget v0, p0, Lfaa;->t:F

    const v1, 0x3951b717    # 2.0E-4f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    iget v0, p0, Lfaa;->n:F

    iget v1, p0, Lfaa;->s:F

    iget v2, p0, Lfaa;->t:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lfaa;->n:F

    iget v0, p0, Lfaa;->t:F

    const v1, 0x3f6b851f    # 0.92f

    mul-float/2addr v0, v1

    iput v0, p0, Lfaa;->t:F

    invoke-direct {p0}, Lfaa;->g()V

    invoke-direct {p0}, Lfaa;->k()V

    iget-object v0, p0, Lfaa;->O:Lezx;

    invoke-virtual {v0}, Lezx;->requestRender()V

    goto/16 :goto_2
.end method

.method public final onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 2

    iput p2, p0, Lfaa;->f:I

    iput p3, p0, Lfaa;->g:I

    iget-object v0, p0, Lfaa;->d:Lezw;

    if-nez v0, :cond_0

    const-string v0, "PanoramaViewRenderer"

    const-string v1, "Image file not set. Cannot initialize rendering."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lfaa;->c:Z

    if-nez v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lfaa;->h()V
    :try_end_0
    .catch Leyu; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget v0, p0, Lfaa;->m:F

    invoke-direct {p0, v0}, Lfaa;->c(F)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Leyu;->printStackTrace()V

    goto :goto_1
.end method

.method public final onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 0

    return-void
.end method
