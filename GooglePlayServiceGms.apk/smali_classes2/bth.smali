.class public final enum Lbth;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbth;

.field public static final enum b:Lbth;

.field public static final enum c:Lbth;

.field public static final enum d:Lbth;

.field public static final enum e:Lbth;

.field public static final enum f:Lbth;

.field public static final enum g:Lbth;

.field private static final synthetic h:[Lbth;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lbth;

    const-string v1, "APP_DATA"

    invoke-direct {v0, v1, v3}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->a:Lbth;

    new-instance v0, Lbth;

    const-string v1, "PARANOID_CHECKS"

    invoke-direct {v0, v1, v4}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->b:Lbth;

    new-instance v0, Lbth;

    const-string v1, "STICKY_HEADERS"

    invoke-direct {v0, v1, v5}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->c:Lbth;

    new-instance v0, Lbth;

    const-string v1, "SYNC_MORE"

    invoke-direct {v0, v1, v6}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->d:Lbth;

    new-instance v0, Lbth;

    const-string v1, "_TEST1"

    invoke-direct {v0, v1, v7}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->e:Lbth;

    new-instance v0, Lbth;

    const-string v1, "_TEST2"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->f:Lbth;

    new-instance v0, Lbth;

    const-string v1, "WORK_AROUND_APP_DATA_SYNC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbth;->g:Lbth;

    const/4 v0, 0x7

    new-array v0, v0, [Lbth;

    sget-object v1, Lbth;->a:Lbth;

    aput-object v1, v0, v3

    sget-object v1, Lbth;->b:Lbth;

    aput-object v1, v0, v4

    sget-object v1, Lbth;->c:Lbth;

    aput-object v1, v0, v5

    sget-object v1, Lbth;->d:Lbth;

    aput-object v1, v0, v6

    sget-object v1, Lbth;->e:Lbth;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbth;->f:Lbth;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbth;->g:Lbth;

    aput-object v2, v0, v1

    sput-object v0, Lbth;->h:[Lbth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbth;
    .locals 1

    const-class v0, Lbth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbth;

    return-object v0
.end method

.method public static values()[Lbth;
    .locals 1

    sget-object v0, Lbth;->h:[Lbth;

    invoke-virtual {v0}, [Lbth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbth;

    return-object v0
.end method
