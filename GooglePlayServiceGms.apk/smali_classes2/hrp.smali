.class Lhrp;
.super Lhrv;
.source "SourceFile"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[B

.field private final h:Landroid/os/PowerManager;

.field private final i:Landroid/content/Context;

.field private volatile j:Lhrd;

.field private volatile k:Z

.field private final l:Ljava/lang/String;

.field private volatile m:Lhra;

.field private n:Ljava/lang/Object;

.field private final o:Lhrs;

.field private p:Lhrt;

.field private volatile q:Z

.field private final r:Lilx;


# direct methods
.method constructor <init>(Landroid/content/Context;Lhrs;Lhsd;Ljava/lang/String;Ljava/lang/String;[BLhqq;Ljava/lang/String;Limb;Lilx;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p7, p9, p3}, Lhrv;-><init>(Lhqq;Limb;Lhsd;)V

    iput-boolean v1, p0, Lhrp;->k:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhrp;->n:Ljava/lang/Object;

    iput-boolean v1, p0, Lhrp;->q:Z

    const-string v0, "Session id should not be null. Please make sure you called the correct constructor."

    invoke-static {p8, v0}, Lhsn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lhrp;->i:Landroid/content/Context;

    iput-object p4, p0, Lhrp;->f:Ljava/lang/String;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lhrp;->h:Landroid/os/PowerManager;

    iput-object p5, p0, Lhrp;->e:Ljava/lang/String;

    iput-object p6, p0, Lhrp;->g:[B

    iput-object p8, p0, Lhrp;->l:Ljava/lang/String;

    iput-object p2, p0, Lhrp;->o:Lhrs;

    iput-object p10, p0, Lhrp;->r:Lilx;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lhrs;Lhsd;Ljava/lang/String;[BLhqq;Limb;Lilx;)V
    .locals 11

    const/4 v4, 0x0

    invoke-static {}, Lhrp;->b()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lhrp;-><init>(Landroid/content/Context;Lhrs;Lhsd;Ljava/lang/String;Ljava/lang/String;[BLhqq;Ljava/lang/String;Limb;Lilx;)V

    return-void
.end method

.method static synthetic a(Lhrp;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lhrp;->h:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic a(Livi;Livi;Ljava/lang/String;)Livi;
    .locals 1

    invoke-static {p0, p1, p2}, Lhrp;->b(Livi;Livi;Ljava/lang/String;)Livi;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lhrp;Livi;Lhrw;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lhrp;->a(Livi;Lhrw;ILjava/lang/String;)V

    return-void
.end method

.method private a(Livi;Lhrw;ILjava/lang/String;)V
    .locals 9

    const/4 v6, 0x6

    const/4 v5, 0x3

    iget-object v0, p0, Lhrp;->a:Lhqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrp;->a:Lhqq;

    iget-object v1, p2, Lhrw;->d:Ljava/lang/String;

    invoke-interface {v0, p4, p3, v1}, Lhqq;->a(Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhrp;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrp;->b:Limb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No backup data path specified, dropping data seqNum:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Livi;->f(I)Livi;

    move-result-object v2

    invoke-virtual {v2, v5}, Livi;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhrp;->b:Limb;

    const-string v2, "%s: Backing up %s/%s to path: %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p1, v6}, Livi;->f(I)Livi;

    move-result-object v4

    invoke-virtual {v4, v5}, Livi;->c(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    aput-object p4, v3, v0

    iget-object v0, p0, Lhrp;->e:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Limb;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v8, p0, Lhrp;->n:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v0, p0, Lhrp;->m:Lhra;

    if-nez v0, :cond_4

    new-instance v0, Lhra;

    iget-object v1, p0, Lhrp;->h:Landroid/os/PowerManager;

    iget-object v2, p0, Lhrp;->e:Ljava/lang/String;

    iget-object v3, p0, Lhrp;->g:[B

    iget-object v4, p0, Lhrp;->a:Lhqq;

    iget-object v5, p0, Lhrp;->b:Limb;

    const/4 v6, 0x0

    iget-object v7, p0, Lhrp;->r:Lilx;

    invoke-direct/range {v0 .. v7}, Lhra;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLhqq;Limb;Lhsd;Lilx;)V

    iput-object v0, p0, Lhrp;->m:Lhra;

    :cond_4
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p4, :cond_5

    iget-object v0, p0, Lhrp;->m:Lhra;

    invoke-virtual {v0, p4}, Lhra;->a(Ljava/lang/String;)Lhrw;

    move-result-object v0

    iget-boolean v0, v0, Lhrw;->a:Z

    if-nez v0, :cond_5

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhrp;->b:Limb;

    const-string v1, "Failed to write session ID, will try later."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lhrp;->m:Lhra;

    invoke-virtual {v0, p1}, Lhra;->a(Livi;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method static synthetic b(Lhrp;)Lilx;
    .locals 1

    iget-object v0, p0, Lhrp;->r:Lilx;

    return-object v0
.end method

.method private static b(Livi;Livi;Ljava/lang/String;)Livi;
    .locals 1

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Livi;->b(ILivi;)Livi;

    return-object p0
.end method

.method static b()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lhrp;)Lhrt;
    .locals 1

    iget-object v0, p0, Lhrp;->p:Lhrt;

    return-object v0
.end method

.method static synthetic d(Lhrp;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhrp;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lhrp;)Z
    .locals 1

    iget-boolean v0, p0, Lhrp;->k:Z

    return v0
.end method

.method static synthetic f(Lhrp;)Lhrd;
    .locals 1

    iget-object v0, p0, Lhrp;->j:Lhrd;

    return-object v0
.end method

.method static synthetic g(Lhrp;)Lhra;
    .locals 1

    iget-object v0, p0, Lhrp;->m:Lhra;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrp;->b:Limb;

    const-string v1, "RemoteScanResultWriter.workerThread will be closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhrp;->p:Lhrt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrp;->p:Lhrt;

    invoke-virtual {v0}, Lhrt;->a()V

    :cond_1
    return-void
.end method

.method protected final declared-synchronized a(Livi;Livi;)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhrp;->q:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrp;->q:Z

    new-instance v0, Lhrd;

    iget-object v1, p0, Lhrp;->i:Landroid/content/Context;

    iget-object v2, p0, Lhrp;->b:Limb;

    invoke-direct {v0, v1, v2}, Lhrd;-><init>(Landroid/content/Context;Limb;)V

    iput-object v0, p0, Lhrp;->j:Lhrd;

    new-instance v0, Lhrr;

    const-string v1, "RemoteScanResultWriter.workerThread"

    invoke-direct {v0, p0, v1}, Lhrr;-><init>(Lhrp;Ljava/lang/String;)V

    invoke-virtual {v0}, Lhrr;->start()V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrp;->b:Limb;

    const-string v2, "Waiting for the RemoteScanResultWriter.workerThread to start."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lhrr;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lhrt;

    invoke-direct {v1, p0, v0}, Lhrt;-><init>(Lhrp;Landroid/os/Looper;)V

    iput-object v1, p0, Lhrp;->p:Lhrt;

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrp;->b:Limb;

    const-string v1, "RemoteScanResultWriter.workerThread started."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhrp;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lhrp;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_2
    iget-object v0, p0, Lhrp;->o:Lhrs;

    sget-object v1, Lhrs;->a:Lhrs;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lhrp;->p:Lhrt;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lhrt;->a(Livi;Livi;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lhrp;->l:Ljava/lang/String;

    invoke-static {p1, p2, v1}, Lhrp;->b(Livi;Livi;Ljava/lang/String;)Livi;

    move-result-object v1

    new-instance v2, Lhrw;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "To many data in upload queue."

    invoke-direct {v2, v3, v4, v5}, Lhrw;-><init>(ZLivi;Ljava/lang/String;)V

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Livi;->f(I)Livi;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Livi;->c(I)I

    move-result v3

    iget-object v4, p0, Lhrp;->l:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3, v4}, Lhrp;->a(Livi;Lhrw;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    :goto_0
    monitor-exit p0

    return v0

    :cond_4
    :try_start_1
    iget-object v0, p0, Lhrp;->p:Lhrt;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lhrt;->a(Livi;Livi;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrp;->b:Limb;

    const-string v1, "RemoteScanResultWriter interrupted."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrp;->k:Z

    return-void
.end method
