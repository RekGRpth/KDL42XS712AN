.class public final Lhmt;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a([D)Lhmw;
    .locals 8

    const-wide/16 v1, 0x0

    array-length v5, p0

    const/4 v0, 0x0

    move-wide v3, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-wide v6, p0, v0

    add-double/2addr v3, v6

    mul-double/2addr v6, v6

    add-double/2addr v1, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v0, p0

    int-to-double v5, v0

    div-double v5, v3, v5

    array-length v0, p0

    if-nez v0, :cond_1

    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :goto_1
    new-instance v2, Lhmw;

    invoke-direct {v2, v5, v6, v0, v1}, Lhmw;-><init>(DD)V

    return-object v2

    :cond_1
    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    int-to-double v3, v0

    div-double v0, v1, v3

    goto :goto_1
.end method

.method static a(Ljava/util/List;)[D
    .locals 14

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [D

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    const-wide/16 v3, 0x0

    iget-object v7, v0, Lhng;->b:[F

    array-length v8, v7

    move v0, v2

    :goto_1
    if-ge v0, v8, :cond_0

    aget v9, v7, v0

    float-to-double v10, v9

    float-to-double v12, v9

    mul-double v9, v10, v12

    add-double/2addr v3, v9

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    aput-wide v3, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v5
.end method

.method public static a(Ljava/util/List;JZ)[D
    .locals 12

    const/4 v11, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v6, v0, [D

    if-eqz p3, :cond_2

    const/4 v0, -0x1

    move v4, v0

    :goto_0
    if-eqz p3, :cond_3

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    move v3, v2

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    if-ltz v1, :cond_4

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    if-nez p3, :cond_0

    iget-wide v7, v0, Lhng;->a:J

    cmp-long v7, v7, p1

    if-gtz v7, :cond_4

    :cond_0
    if-eqz p3, :cond_1

    iget-wide v7, v0, Lhng;->a:J

    cmp-long v7, v7, p1

    if-ltz v7, :cond_4

    :cond_1
    aget-wide v7, v6, v2

    iget-object v9, v0, Lhng;->b:[F

    aget v9, v9, v2

    float-to-double v9, v9

    add-double/2addr v7, v9

    aput-wide v7, v6, v2

    aget-wide v7, v6, v5

    iget-object v9, v0, Lhng;->b:[F

    aget v9, v9, v5

    float-to-double v9, v9

    add-double/2addr v7, v9

    aput-wide v7, v6, v5

    aget-wide v7, v6, v11

    iget-object v0, v0, Lhng;->b:[F

    aget v0, v0, v11

    float-to-double v9, v0

    add-double/2addr v7, v9

    aput-wide v7, v6, v11

    add-int/lit8 v3, v3, 0x1

    add-int v0, v1, v4

    move v1, v0

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    move v1, v2

    move v3, v2

    goto :goto_1

    :cond_4
    aget-wide v0, v6, v2

    int-to-double v7, v3

    div-double/2addr v0, v7

    aput-wide v0, v6, v2

    aget-wide v0, v6, v5

    int-to-double v7, v3

    div-double/2addr v0, v7

    aput-wide v0, v6, v5

    aget-wide v0, v6, v11

    int-to-double v2, v3

    div-double/2addr v0, v2

    aput-wide v0, v6, v11

    return-object v6
.end method

.method static a([DD)[D
    .locals 6

    array-length v0, p0

    new-array v1, v0, [D

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget-wide v2, p0, v0

    sub-double/2addr v2, p1

    aput-wide v2, v1, v0

    aget-wide v2, v1, v0

    aget-wide v4, v1, v0

    mul-double/2addr v2, v4

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static a([DJ)[D
    .locals 8

    const/4 v1, 0x0

    const/4 v6, 0x5

    new-instance v2, Ljava/util/PriorityQueue;

    invoke-direct {v2, v6}, Ljava/util/PriorityQueue;-><init>(I)V

    move v0, v1

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    aget-wide v3, p0, v0

    new-instance v5, Lhmu;

    invoke-direct {v5, v0, v3, v4, v1}, Lhmu;-><init>(IDB)V

    invoke-virtual {v2, v5}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v3

    if-le v3, v6, :cond_0

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-array v3, v6, [D

    long-to-double v0, p1

    const-wide v4, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v0, v4

    array-length v4, p0

    add-int/lit8 v4, v4, 0x1

    int-to-double v5, v4

    div-double v0, v5, v0

    mul-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    div-double v4, v0, v4

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmu;

    invoke-static {v0}, Lhmu;->a(Lhmu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-double v6, v0

    mul-double/2addr v6, v4

    aput-wide v6, v3, v1

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method static b([DD)D
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    aget-wide v3, p0, v2

    cmpl-double v0, v3, p1

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    array-length v5, p0

    move v4, v2

    move v3, v2

    :goto_1
    if-ge v4, v5, :cond_3

    aget-wide v6, p0, v4

    if-eqz v0, :cond_2

    cmpg-double v8, v6, p1

    if-gez v8, :cond_2

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    cmpl-double v6, v6, p1

    if-ltz v6, :cond_0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v1

    goto :goto_2

    :cond_3
    int-to-double v0, v3

    array-length v2, p0

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method
