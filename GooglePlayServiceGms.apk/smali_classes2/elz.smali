.class public final Lelz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lelt;

.field public final b:Lelw;


# direct methods
.method public constructor <init>(Lelt;Lelw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lelz;->a:Lelt;

    iput-object p2, p0, Lelz;->b:Lelw;

    return-void
.end method

.method private final a()Lelx;
    .locals 1

    iget-object v0, p0, Lelz;->a:Lelt;

    iget-object v0, v0, Lelt;->a:Lelu;

    iget-object v0, v0, Lelu;->d:Lelx;

    return-object v0
.end method

.method private static a(Lels;)V
    .locals 3

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing GSAI for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; no longer in resources"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->a(Ljava/lang/String;)I

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    :try_start_0
    invoke-static {v0, v1, v2}, Lema;->b(Ljava/lang/Object;J)Lema;

    move-result-object v0

    invoke-virtual {p0, v0}, Lels;->a(Lema;)V
    :try_end_0
    .catch Lemb; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lemd;

    const-string v1, "Could not clear GSAI"

    invoke-direct {v0, v1}, Lemd;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lels;Landroid/content/pm/PackageInfo;)V
    .locals 8

    iget-object v0, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lelz;->a:Lelt;

    iget-object v1, v1, Lelt;->a:Lelu;

    iget-object v1, v1, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lelz;->a:Lelt;

    iget-object v2, v2, Lelt;->a:Lelu;

    iget-object v2, v2, Lelu;->b:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lelp;->a(Landroid/content/pm/PackageManager;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;)Lelp;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1}, Lels;->b(Ljava/lang/String;)V

    iget-object v1, p1, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lelp;->a(Ljava/lang/String;)Lelo;

    move-result-object v1

    invoke-direct {p0}, Lelz;->a()Lelx;

    move-result-object v0

    iget-object v2, p0, Lelz;->a:Lelt;

    iget-object v3, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Lelt;->a(Landroid/content/pm/ApplicationInfo;)Lely;

    move-result-object v2

    invoke-interface {v0, v2}, Lelx;->a(Lely;)Ljava/util/Set;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    iget-object v3, v1, Lelo;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, v1, Lelo;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v3, v1, Lelo;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    aget-object v3, v3, v0
    :try_end_0
    .catch Lelr; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lemb; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v4, p0, Lelz;->b:Lelw;

    iget-object v5, p0, Lelz;->a:Lelt;

    iget-object v6, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v6}, Lelt;->a(Landroid/content/pm/ApplicationInfo;)Lely;

    move-result-object v5

    iget-wide v6, p2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-interface {v4, v5, v3, v6, v7}, Lelw;->a(Lely;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V
    :try_end_1
    .catch Lemb; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lelr; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lemb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "From "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " resources: problem with corpus "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lemb;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lelr; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lemb; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lelr;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->d(Ljava/lang/String;)I

    invoke-virtual {v0}, Lelr;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lels;->b(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_3
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lelz;->a(Lels;Ljava/lang/String;)V
    :try_end_3
    .catch Lelr; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lemb; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lemb;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->d(Ljava/lang/String;)I

    invoke-virtual {v0}, Lemb;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lels;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_4
    iget-object v0, v1, Lelo;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    if-eqz v0, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting GSAI for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->a(Ljava/lang/String;)I
    :try_end_4
    .catch Lelr; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lemb; {:try_start_4 .. :try_end_4} :catch_2

    :try_start_5
    iget-wide v1, p2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v0, v1, v2}, Lema;->b(Ljava/lang/Object;J)Lema;

    move-result-object v0

    invoke-virtual {p1, v0}, Lels;->a(Lema;)V
    :try_end_5
    .catch Lemb; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lelr; {:try_start_5 .. :try_end_5} :catch_1

    :goto_3
    :try_start_6
    invoke-virtual {p1}, Lels;->i()V

    goto :goto_1

    :catch_3
    move-exception v0

    new-instance v0, Lemd;

    const-string v1, "Could not set GSAI from resources"

    invoke-direct {v0, v1}, Lemd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {p1}, Lelz;->a(Lels;)V
    :try_end_6
    .catch Lelr; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lemb; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3
.end method

.method private a(Lels;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lelz;->b:Lelw;

    invoke-interface {v0, p1, p2}, Lelw;->a(Lels;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/pm/PackageInfo;)V
    .locals 4

    iget-object v0, p0, Lelz;->a:Lelt;

    iget-object v1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lels;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_3

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "com.google.android.gms.appdatasearch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p0, Lelz;->a:Lelt;

    iget-object v1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v0

    invoke-virtual {v0}, Lels;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: need to parse "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->c(Ljava/lang/String;)I

    invoke-direct {p0, v0, p1}, Lelz;->a(Lels;Landroid/content/pm/PackageInfo;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: up to date:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->a(Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    const-string v0, "Package %s is not allowed to use icing"

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_2
    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: resources removed:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->b(Ljava/lang/String;)I

    invoke-static {v1}, Lelz;->a(Lels;)V

    invoke-direct {p0}, Lelz;->a()Lelx;

    move-result-object v0

    iget-object v2, p0, Lelz;->a:Lelt;

    iget-object v3, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Lelt;->a(Landroid/content/pm/ApplicationInfo;)Lely;

    move-result-object v2

    invoke-interface {v0, v2}, Lelx;->a(Lely;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lelz;->a(Lels;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    const-string v0, "Package %s has no appdatasearch metadata"

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Lels;->i()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lelz;->a:Lelt;

    iget-object v0, v0, Lelt;->a:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lelz;->a(Landroid/content/pm/PackageInfo;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const-string v1, "Package %s is disabled"

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v1, p0, Lelz;->b:Lelw;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v0}, Lelw;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method
