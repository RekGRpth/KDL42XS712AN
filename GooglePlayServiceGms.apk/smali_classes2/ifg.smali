.class final Lifg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liza;


# instance fields
.field final synthetic a:Life;

.field private final b:Lifh;

.field private final c:Ljava/lang/String;

.field private d:Lifi;

.field private volatile e:J


# direct methods
.method constructor <init>(Life;Lifh;)V
    .locals 2

    iput-object p1, p0, Lifg;->a:Life;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lifg;->e:J

    iput-object p2, p0, Lifg;->b:Lifh;

    sget-object v0, Liff;->a:[I

    iget-object v1, p0, Lifg;->b:Lifh;

    invoke-virtual {v1}, Lifh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "GlsClient"

    :goto_0
    iput-object v0, p0, Lifg;->c:Ljava/lang/String;

    return-void

    :pswitch_0
    const-string v0, "GlsClient-query"

    goto :goto_0

    :pswitch_1
    const-string v0, "GlsClient-upload"

    goto :goto_0

    :pswitch_2
    const-string v0, "GlsClient-model-query"

    goto :goto_0

    :pswitch_3
    const-string v0, "GlsClient-device-location-query"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a()Z
    .locals 2

    iget-object v0, p0, Lifg;->b:Lifh;

    sget-object v1, Lifh;->a:Lifh;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lifg;->b:Lifh;

    sget-object v1, Lifh;->b:Lifh;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Liff;->a:[I

    iget-object v1, p0, Lifg;->b:Lifh;

    invoke-virtual {v1}, Lifh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lifg;->b:Lifh;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->a(Livi;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->d(Livi;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->b(Livi;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->c(Livi;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method final declared-synchronized a(Livi;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lifg;->d:Lifi;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Gls request still outstanding."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Life;->a(Ljava/util/Locale;)Livi;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Livi;->b(ILivi;)Livi;

    invoke-direct {p0}, Lifg;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    iget-object v1, p0, Lifg;->a:Life;

    invoke-static {v1}, Life;->a(Life;)Lids;

    move-result-object v1

    invoke-virtual {v1}, Lids;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Livi;->k(I)I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Livi;->c(II)Livi;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {}, Life;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x63

    invoke-virtual {v1, v0}, Livi;->f(I)Livi;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Livi;

    sget-object v2, Lihj;->o:Livk;

    invoke-direct {v0, v2}, Livi;-><init>(Livk;)V

    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;

    :cond_2
    iget-object v1, p0, Lifg;->a:Life;

    invoke-static {v1, v0}, Life;->a(Life;Livi;)V

    :cond_3
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p1, v1}, Livi;->a(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v2, Lifi;

    sget-object v0, Liff;->a:[I

    iget-object v3, p0, Lifg;->b:Lifh;

    invoke-virtual {v3}, Lifh;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lifg;->b:Lifh;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lifg;->c:Ljava/lang/String;

    const-string v2, "query(): unable to write request to payload"

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    invoke-direct {p0}, Lifg;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :pswitch_0
    :try_start_4
    const-string v0, "g:loc/ql"

    :goto_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lifi;-><init>(Ljava/lang/String;[B)V

    iput-object v2, p0, Lifg;->d:Lifi;

    iget-object v0, p0, Lifg;->d:Lifi;

    invoke-virtual {v0, p0}, Lifi;->a(Liza;)V

    iget-object v0, p0, Lifg;->d:Lifi;

    invoke-virtual {v0}, Lifi;->h()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v1, p0, Lifg;->d:Lifi;

    monitor-enter v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v0, p0, Lifg;->d:Lifi;

    invoke-virtual {v0}, Lifi;->c()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lifg;->e:J

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_2
    :try_start_7
    iget-object v0, p0, Lifg;->b:Lifh;

    sget-object v1, Lifh;->b:Lifh;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->b(Life;)Liha;

    move-result-object v0

    iget-wide v1, p0, Lifg;->e:J

    invoke-virtual {v0, v1, v2}, Liha;->a(J)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    iput-object v0, p0, Lifg;->d:Lifi;

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lifg;->c:Ljava/lang/String;

    const-string v1, "Collection Policy prohibits upload."

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lifg;->b()V

    goto :goto_0

    :pswitch_1
    const-string v0, "g:loc/ul"

    goto :goto_1

    :pswitch_2
    const-string v0, "g:loc/dl"
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v1

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_1
    move-exception v0

    const-wide/16 v0, 0x0

    :try_start_9
    iput-wide v0, p0, Lifg;->e:J

    goto :goto_2

    :cond_6
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lifg;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lifg;->e:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes to MASF."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-static {}, Lhsy;->a()Lhsy;

    move-result-object v0

    iget-object v1, p0, Lifg;->d:Lifi;

    invoke-virtual {v0, v1}, Lhsy;->a(Liyz;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Liyz;Lizb;)V
    .locals 5

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lifg;->d:Lifi;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-eqz p2, :cond_7

    :try_start_1
    invoke-virtual {p2}, Lizb;->Z_()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    :try_start_2
    iget v0, p2, Lizb;->d:I

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_2

    sget-object v0, Lihj;->T:Livk;

    invoke-static {v1, v0}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lifg;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Livi;->e()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {v2}, Life;->e(Livi;)V

    invoke-direct {p0}, Lifg;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->a(Life;)Lids;

    move-result-object v0

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Livi;->f(I)Livi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lids;->a(Livi;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    :goto_1
    :try_start_4
    sget-object v0, Liff;->a:[I

    iget-object v1, p0, Lifg;->b:Lifh;

    invoke-virtual {v1}, Lifh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lifg;->b:Lifh;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_5
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lifg;->c:Ljava/lang/String;

    const-string v4, "requestCompleted()"

    invoke-static {v3, v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_4
    if-eqz v1, :cond_3

    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_5

    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_5
    :goto_4
    :try_start_8
    throw v0

    :pswitch_0
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->a(Livi;)V

    :cond_6
    :goto_5
    const/4 v0, 0x0

    iput-object v0, p0, Lifg;->d:Lifi;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_1
    :try_start_9
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->d(Livi;)V

    iget-wide v0, p0, Lifg;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->b(Life;)Liha;

    move-result-object v0

    iget-wide v1, p0, Lifg;->e:J

    invoke-virtual {v0, v1, v2}, Liha;->b(J)V

    goto :goto_5

    :pswitch_2
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->b(Livi;)V

    goto :goto_5

    :pswitch_3
    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->c(Life;)Liev;

    move-result-object v0

    invoke-virtual {v0, v2}, Liev;->c(Livi;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    :catch_2
    move-exception v0

    goto/16 :goto_1

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized a(Liyz;Ljava/lang/Exception;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lifg;->d:Lifi;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lifg;->c:Ljava/lang/String;

    const-string v1, "requestFailed"

    invoke-static {v0, v1, p2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    iget-object v0, p0, Lifg;->b:Lifh;

    sget-object v1, Lifh;->b:Lifh;

    if-ne v0, v1, :cond_2

    instance-of v0, p2, Lixt;

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lifg;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Lifg;->a:Life;

    invoke-static {v0}, Life;->b(Life;)Liha;

    move-result-object v0

    iget-wide v1, p0, Lifg;->e:J

    invoke-virtual {v0, v1, v2}, Liha;->b(J)V

    :cond_2
    invoke-direct {p0}, Lifg;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lifg;->d:Lifi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method
