.class public final Lilt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final c:Ljava/util/Comparator;


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lilu;

    invoke-direct {v0}, Lilu;-><init>()V

    sput-object v0, Lilt;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 4

    const/4 v2, 0x0

    invoke-static {p1, p2, v2, v2}, Lilv;->a(IIII)J

    move-result-wide v0

    invoke-static {p3, v2, v2, v2}, Lilv;->a(IIII)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lilt;-><init>(JJ)V

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lilt;->a:J

    iput-wide p3, p0, Lilt;->b:J

    cmp-long v0, p3, p1

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid time span."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget-wide v0, p0, Lilt;->b:J

    iget-wide v2, p0, Lilt;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(Lilt;)Lilt;
    .locals 7

    iget-wide v0, p0, Lilt;->a:J

    iget-wide v2, p1, Lilt;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-wide v3, p0, Lilt;->b:J

    iget-wide v5, p1, Lilt;->b:J

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lilt;

    invoke-direct {v0, v1, v2, v3, v4}, Lilt;-><init>(JJ)V

    goto :goto_0
.end method

.method public final a(Lilt;Ljava/util/List;)V
    .locals 5

    iget-wide v0, p1, Lilt;->b:J

    iget-wide v2, p0, Lilt;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Lilt;->a:J

    iget-wide v2, p0, Lilt;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-wide v0, p1, Lilt;->a:J

    iget-wide v2, p0, Lilt;->a:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    iget-wide v0, p0, Lilt;->b:J

    iget-wide v2, p1, Lilt;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Lilt;

    iget-wide v1, p1, Lilt;->b:J

    iget-wide v3, p0, Lilt;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lilt;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lilt;->b:J

    iget-wide v2, p1, Lilt;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    new-instance v0, Lilt;

    iget-wide v1, p0, Lilt;->a:J

    iget-wide v3, p1, Lilt;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lilt;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lilt;

    iget-wide v1, p1, Lilt;->b:J

    iget-wide v3, p0, Lilt;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lilt;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    new-instance v0, Lilt;

    iget-wide v1, p0, Lilt;->a:J

    iget-wide v3, p1, Lilt;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lilt;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 2

    iget-wide v0, p0, Lilt;->b:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Calendar;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {p1}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lilt;->b(J)Z

    move-result v4

    if-nez v4, :cond_0

    iget-wide v4, p0, Lilt;->a:J

    cmp-long v2, v4, v2

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final b(J)Z
    .locals 2

    iget-wide v0, p0, Lilt;->a:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lilt;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/Calendar;)Z
    .locals 4

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lilt;->b:J

    invoke-static {v0, v1}, Lilv;->a(J)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(J)J
    .locals 2

    iget-wide v0, p0, Lilt;->a:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lilt;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lilt;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    iget-wide v0, p0, Lilt;->b:J

    sub-long/2addr v0, p1

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/util/Calendar;)Z
    .locals 2

    invoke-static {p1}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lilt;->b(J)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lilt;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lilt;

    iget-wide v1, p0, Lilt;->a:J

    iget-wide v3, p1, Lilt;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lilt;->b:J

    iget-wide v3, p1, Lilt;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "TimeSpan: [%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lilt;->a:J

    invoke-static {v3, v4}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lilt;->b:J

    invoke-static {v3, v4}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
