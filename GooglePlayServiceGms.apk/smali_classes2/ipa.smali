.class public final Lipa;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lioq;

.field public b:Linv;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lipa;->a:Lioq;

    iput-object v0, p0, Lipa;->b:Linv;

    const-string v0, ""

    iput-object v0, p0, Lipa;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lipa;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lipa;->b:Linv;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lipa;->b:Linv;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lipa;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lipa;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lipa;->a:Lioq;

    if-eqz v1, :cond_2

    const/16 v1, 0x8

    iget-object v2, p0, Lipa;->a:Lioq;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lipa;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lipa;->b:Linv;

    if-nez v0, :cond_1

    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    iput-object v0, p0, Lipa;->b:Linv;

    :cond_1
    iget-object v0, p0, Lipa;->b:Linv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lipa;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lipa;->a:Lioq;

    if-nez v0, :cond_2

    new-instance v0, Lioq;

    invoke-direct {v0}, Lioq;-><init>()V

    iput-object v0, p0, Lipa;->a:Lioq;

    :cond_2
    iget-object v0, p0, Lipa;->a:Lioq;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Lipa;->b:Linv;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lipa;->b:Linv;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Lipa;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Lipa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lipa;->a:Lioq;

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    iget-object v1, p0, Lipa;->a:Lioq;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lipa;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lipa;

    iget-object v2, p0, Lipa;->a:Lioq;

    if-nez v2, :cond_3

    iget-object v2, p1, Lipa;->a:Lioq;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lipa;->a:Lioq;

    iget-object v3, p1, Lipa;->a:Lioq;

    invoke-virtual {v2, v3}, Lioq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lipa;->b:Linv;

    if-nez v2, :cond_5

    iget-object v2, p1, Lipa;->b:Linv;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lipa;->b:Linv;

    iget-object v3, p1, Lipa;->b:Linv;

    invoke-virtual {v2, v3}, Linv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lipa;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lipa;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lipa;->c:Ljava/lang/String;

    iget-object v3, p1, Lipa;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lipa;->a:Lioq;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lipa;->b:Linv;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lipa;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lipa;->a:Lioq;

    invoke-virtual {v0}, Lioq;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lipa;->b:Linv;

    invoke-virtual {v0}, Linv;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lipa;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method
