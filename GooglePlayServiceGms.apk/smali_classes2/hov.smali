.class public final Lhov;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Lhom;

.field final c:Lhow;


# direct methods
.method constructor <init>(ILhom;Lhow;)V
    .locals 2

    const/high16 v0, 0x3f400000    # 0.75f

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lhov;->a:I

    iput-object p2, p0, Lhov;->b:Lhom;

    const/4 v0, 0x0

    iput-object v0, p0, Lhov;->c:Lhow;

    return-void
.end method


# virtual methods
.method protected final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2

    invoke-virtual {p0}, Lhov;->size()I

    move-result v0

    iget v1, p0, Lhov;->a:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lhov;->b:Lhom;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lhom;->a(I)V

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhov;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhov;->c:Lhow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhov;->c:Lhow;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
