.class final Liwt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liws;


# instance fields
.field a:I

.field b:J

.field final synthetic c:Liwp;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Liwp;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Liwt;->c:Liwp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Liwt;->d:I

    iput v2, p0, Liwt;->a:I

    const v0, 0x7fffffff

    iput v0, p0, Liwt;->e:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liwt;->b:J

    iput v2, p0, Liwt;->f:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final a(J)Liws;
    .locals 7

    const/4 v6, 0x2

    iget-wide v0, p0, Liwt;->b:J

    const-wide v2, 0x37e11d600L

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Liwr;

    iget-object v1, p0, Liwt;->c:Liwp;

    invoke-direct {v0, v1}, Liwr;-><init>(Liwp;)V

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget v0, p0, Liwt;->f:I

    if-lez v0, :cond_2

    new-instance v0, Liwu;

    iget-object v1, p0, Liwt;->c:Liwp;

    invoke-direct {v0, v1}, Liwu;-><init>(Liwp;)V

    move-object p0, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Liwt;->c:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    sub-long v0, p1, v0

    long-to-double v0, v0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    iget-object v4, p0, Liwt;->c:Liwp;

    iget-wide v4, v4, Liwp;->d:J

    long-to-double v4, v4

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Liwt;->c:Liwp;

    iget-wide v0, v0, Liwp;->g:J

    iget-object v2, p0, Liwt;->c:Liwp;

    iget-wide v2, v2, Liwp;->e:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    :cond_3
    iget v0, p0, Liwt;->a:I

    if-ge v0, v6, :cond_4

    iget v0, p0, Liwt;->d:I

    if-lt v0, v6, :cond_0

    iget v0, p0, Liwt;->a:I

    if-lez v0, :cond_0

    :cond_4
    new-instance v0, Liwv;

    iget-object v1, p0, Liwt;->c:Liwp;

    invoke-direct {v0, v1}, Liwv;-><init>(Liwp;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(JLiwe;)V
    .locals 7

    const-wide/16 v0, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    sget-object v2, Liwq;->a:[I

    iget-object v3, p3, Liwe;->b:Liwf;

    invoke-virtual {v3}, Liwf;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Liwt;->c:Liwp;

    iget-object v2, v2, Liwp;->c:Liwg;

    invoke-virtual {v2}, Liwg;->c()D

    move-result-wide v2

    iget-object v4, p0, Liwt;->c:Liwp;

    iget-object v4, v4, Liwp;->b:Lixa;

    iget-object v4, v4, Lixa;->b:Liwm;

    invoke-virtual {v4}, Liwm;->e()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    iput v6, p0, Liwt;->a:I

    :cond_1
    iget-object v2, p0, Liwt;->c:Liwp;

    iget-wide v2, v2, Liwp;->e:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_2

    iget-object v0, p0, Liwt;->c:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    sub-long v0, p1, v0

    :cond_2
    iput-wide v0, p0, Liwt;->b:J

    return-void

    :pswitch_1
    iget v2, p3, Liwe;->e:I

    iget v3, p0, Liwt;->e:I

    if-le v2, v3, :cond_4

    iget v2, p0, Liwt;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Liwt;->d:I

    :goto_1
    iget v2, p3, Liwe;->e:I

    iput v2, p0, Liwt;->e:I

    iget-object v2, p0, Liwt;->c:Liwp;

    iget-object v2, v2, Liwp;->i:Liws;

    invoke-interface {v2}, Liws;->a()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    iget v2, p0, Liwt;->a:I

    if-gt v2, v4, :cond_3

    iget v2, p0, Liwt;->d:I

    if-gt v2, v4, :cond_3

    iget-object v2, p0, Liwt;->c:Liwp;

    iget-object v2, v2, Liwp;->b:Lixa;

    invoke-virtual {v2}, Lixa;->c()V

    iget-object v2, p0, Liwt;->c:Liwp;

    iget-object v2, v2, Liwp;->b:Lixa;

    invoke-virtual {v2, p1, p2, p3}, Lixa;->a(JLiwe;)V

    :cond_3
    iget v2, p3, Liwe;->f:F

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    iget v2, p0, Liwt;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Liwt;->f:I

    goto :goto_0

    :cond_4
    iput v6, p0, Liwt;->d:I

    goto :goto_1

    :pswitch_2
    iget v2, p0, Liwt;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Liwt;->a:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Liwz;)V
    .locals 2

    sget-object v0, Liwq;->b:[I

    invoke-virtual {p1}, Liwz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Liwt;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwt;->f:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Liwt;->f:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Liwy;
    .locals 1

    iget-object v0, p0, Liwt;->c:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    return-object v0
.end method
