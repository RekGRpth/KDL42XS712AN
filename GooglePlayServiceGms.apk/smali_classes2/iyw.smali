.class public final Liyw;
.super Lizc;
.source "SourceFile"


# instance fields
.field a:Lixv;

.field b:[B


# direct methods
.method private constructor <init>(Ljava/lang/String;ILixv;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lizc;-><init>(Ljava/lang/String;I)V

    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Liyw;->b(I)V

    iput-object p3, p0, Liyw;->a:Lixv;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 2

    const/4 v0, 0x0

    new-instance v1, Lixs;

    invoke-direct {v1, p2}, Lixs;-><init>([B)V

    invoke-direct {p0, p1, v0, v1}, Liyw;-><init>(Ljava/lang/String;ILixv;)V

    return-void
.end method

.method private l()V
    .locals 3

    iget-object v0, p0, Liyw;->b:[B

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Liyw;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0}, Liyw;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {p0}, Liyw;->k()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v2, p0, Liyw;->a:Lixv;

    if-eqz v2, :cond_1

    iget-object v2, p0, Liyw;->a:Lixv;

    invoke-interface {v2}, Lixv;->Y_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    :goto_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Liyw;->b:[B

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lizc;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Liyw;->b:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lizc;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Liyw;->b:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized b()Ljava/io/InputStream;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Liyw;->l()V

    iget-object v0, p0, Liyw;->a:Lixv;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Liyw;->b:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Livd;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Liyw;->b:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Liyw;->a:Lixv;

    invoke-interface {v2}, Lixv;->Z_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Livd;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized c()I
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Liyw;->l()V

    iget-object v0, p0, Liyw;->b:[B

    array-length v0, v0

    iget-object v1, p0, Liyw;->a:Lixv;

    if-eqz v1, :cond_0

    iget-object v1, p0, Liyw;->a:Lixv;

    invoke-interface {v1}, Lixv;->Y_()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
