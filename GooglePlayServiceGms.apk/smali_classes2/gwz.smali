.class public final Lgwz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 0

    iput-object p1, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lgza;

    move-result-object v0

    iget-object v1, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lgza;->a(Landroid/widget/TextView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lgza;

    move-result-object v0

    invoke-virtual {v0}, Lgza;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lgza;

    move-result-object v1

    invoke-virtual {v1}, Lgza;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lgwz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    goto :goto_0
.end method
