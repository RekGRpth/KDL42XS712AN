.class public final enum Lcev;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcev;

.field public static final enum b:Lcev;

.field private static final synthetic c:[Lcev;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcev;

    const-string v1, "AND"

    invoke-direct {v0, v1, v2}, Lcev;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcev;->a:Lcev;

    new-instance v0, Lcev;

    const-string v1, "OR"

    invoke-direct {v0, v1, v3}, Lcev;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcev;->b:Lcev;

    const/4 v0, 0x2

    new-array v0, v0, [Lcev;

    sget-object v1, Lcev;->a:Lcev;

    aput-object v1, v0, v2

    sget-object v1, Lcev;->b:Lcev;

    aput-object v1, v0, v3

    sput-object v0, Lcev;->c:[Lcev;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcev;
    .locals 1

    const-class v0, Lcev;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcev;

    return-object v0
.end method

.method public static values()[Lcev;
    .locals 1

    sget-object v0, Lcev;->c:[Lcev;

    invoke-virtual {v0}, [Lcev;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcev;

    return-object v0
.end method


# virtual methods
.method public final varargs a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->d()Lceu;

    move-result-object v1

    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    invoke-virtual {v1, p0, v3}, Lceu;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lceu;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lceu;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->d()Lceu;

    move-result-object v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {v2, p0, v0}, Lceu;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lceu;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lceu;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method
