.class abstract Lhks;
.super Lhla;
.source "SourceFile"

# interfaces
.implements Lhkp;


# instance fields
.field protected a:Z

.field final synthetic b:Lhkr;


# direct methods
.method private constructor <init>(Lhkr;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lhks;->b:Lhkr;

    invoke-direct {p0, p1, v0}, Lhla;-><init>(Lhkr;B)V

    iput-boolean v0, p0, Lhks;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lhkr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhks;-><init>(Lhkr;)V

    return-void
.end method


# virtual methods
.method protected U_()V
    .locals 3

    iget-object v0, p0, Lhks;->b:Lhkr;

    iget-object v1, p0, Lhks;->b:Lhkr;

    invoke-static {v1}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v1

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lhkr;->a(Lhkr;J)J

    iget-object v0, p0, Lhks;->b:Lhkr;

    iget-object v1, p0, Lhks;->b:Lhkr;

    invoke-static {v1}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v1

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lhkr;->b(Lhkr;J)J

    return-void
.end method

.method public final V_()V
    .locals 3

    iget-boolean v0, p0, Lhks;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    const-string v1, "Insufficient samples"

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lhkr;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    :cond_2
    iget-object v0, p0, Lhks;->b:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhks;->b:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Lhks;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFatalError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lhkr;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    :cond_2
    iget-object v0, p0, Lhks;->b:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhks;->b:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0
.end method

.method protected abstract b()Lhms;
.end method

.method public b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 6

    invoke-virtual {p0, p1}, Lhks;->c(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-boolean v0, p0, Lhks;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "ActivityScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reporting tilting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lhks;->b:Lhkr;

    invoke-static {v1, v0}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method final c(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 2

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->q(Lhkr;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkx;

    invoke-interface {v0, p1}, Lhkx;->a_(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 0

    invoke-virtual {p0}, Lhks;->g()V

    return-void
.end method

.method protected final e()V
    .locals 1

    invoke-super {p0}, Lhla;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhks;->a:Z

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->n(Lhkr;)Lhko;

    move-result-object v0

    invoke-virtual {v0}, Lhko;->a()V

    invoke-virtual {p0}, Lhks;->U_()V

    return-void
.end method

.method protected g()V
    .locals 10

    invoke-virtual {p0}, Lhks;->b()Lhms;

    move-result-object v7

    invoke-virtual {p0}, Lhks;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v1

    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->o(Lhkr;)I

    move-result v6

    new-instance v0, Lidc;

    sget-object v2, Licn;->Q:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v6}, Lidc;-><init>(Licp;Licn;JLjava/lang/String;I)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting activity detection at: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhks;->b:Lhkr;

    invoke-static {v2}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v2

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with detector: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at delay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhks;->b:Lhkr;

    invoke-static {v2}, Lhkr;->o(Lhkr;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhks;->b:Lhkr;

    invoke-static {v0}, Lhkr;->n(Lhkr;)Lhko;

    move-result-object v0

    iget-object v1, p0, Lhks;->b:Lhkr;

    invoke-static {v1}, Lhkr;->o(Lhkr;)I

    move-result v1

    invoke-interface {v7}, Lhms;->a()J

    move-result-wide v2

    invoke-virtual {p0}, Lhks;->h()I

    move-result v4

    invoke-virtual {p0}, Lhks;->i()D

    move-result-wide v5

    iget-object v8, p0, Lhks;->b:Lhkr;

    invoke-static {v8}, Lhkr;->p(Lhkr;)Lilx;

    move-result-object v9

    move-object v8, p0

    invoke-virtual/range {v0 .. v9}, Lhko;->a(IJIDLhms;Lhkp;Lilx;)V

    return-void
.end method

.method protected h()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method protected i()D
    .locals 2

    const-wide v0, 0x4041800000000000L    # 35.0

    return-wide v0
.end method
