.class final Lhle;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lhou;


# direct methods
.method public constructor <init>(Lhou;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhle;->a:Lhou;

    return-void
.end method

.method private static a(Lhls;Z)Lhug;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lhls;->b:Lhud;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, v1, Lhud;->c:Lhtd;

    :goto_1
    if-eqz v1, :cond_0

    iget-object v0, v1, Lhtw;->c:Lhug;

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lhud;->b:Lhuu;

    goto :goto_1
.end method

.method static a(Lhls;Lhls;Z)Ljava/lang/Double;
    .locals 2

    invoke-static {p0, p2}, Lhle;->a(Lhls;Z)Lhug;

    move-result-object v0

    invoke-static {p1, p2}, Lhle;->a(Lhls;Z)Lhug;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0, v1}, Liba;->a(Lhug;Lhug;)I

    move-result v1

    if-eqz p2, :cond_2

    const/16 v0, 0x1388

    :goto_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x3e8

    goto :goto_1
.end method

.method static a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;
    .locals 5

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    div-double v0, v1, v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Lhuv;)Ljava/util/Set;
    .locals 4

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lhuv;->a(I)Lhut;

    move-result-object v2

    iget-wide v2, v2, Lhut;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method static a(Ljava/util/Map;Lhlq;Ljava/lang/Double;)V
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Set;)Ljava/lang/Double;
    .locals 7

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Lhle;->a:Lhou;

    invoke-virtual {v3, v0}, Lhou;->a(Ljava/lang/Object;)Lhno;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lhno;->d:Ljava/lang/Object;

    check-cast v0, Lhuq;

    iget v0, v0, Lhuq;->f:I

    div-int/lit16 v0, v0, 0x3e8

    if-eqz v1, :cond_0

    int-to-double v3, v0

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_2

    :cond_0
    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
