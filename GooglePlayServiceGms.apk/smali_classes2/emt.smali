.class public final Lemt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Litt;

.field private c:J

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "query"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "query_universal"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest"

    aput-object v2, v0, v1

    sput-object v0, Lemt;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Litt;

    invoke-direct {v0}, Litt;-><init>()V

    iput-object v0, p0, Lemt;->b:Litt;

    iget-object v0, p0, Lemt;->b:Litt;

    iput p1, v0, Litt;->a:I

    iget-object v0, p0, Lemt;->b:Litt;

    iput p2, v0, Litt;->c:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lemt;->c:J

    const/4 v0, 0x0

    iput v0, p0, Lemt;->d:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIII)Litt;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lemt;->d:I

    if-ne v0, v8, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-object v0, p0, Lemt;->b:Litt;

    iput p5, v0, Litt;->b:I

    iget-object v0, p0, Lemt;->b:Litt;

    iget v5, v0, Litt;->i:I

    iget-wide v6, p0, Lemt;->c:J

    sub-long/2addr v3, v6

    long-to-int v3, v3

    add-int/2addr v3, v5

    iput v3, v0, Litt;->i:I

    iget-object v0, p0, Lemt;->b:Litt;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, v0, Litt;->d:I

    iget-object v0, p0, Lemt;->b:Litt;

    iput p2, v0, Litt;->e:I

    iget-object v0, p0, Lemt;->b:Litt;

    iput p3, v0, Litt;->f:I

    iget-object v0, p0, Lemt;->b:Litt;

    iput p4, v0, Litt;->g:I

    iput v9, p0, Lemt;->d:I

    const-string v0, "%s [%s] req/res/sco %d/%d/%d in %d+%d+%d = %d ms"

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Lemt;->a:[Ljava/lang/String;

    iget-object v5, p0, Lemt;->b:Litt;

    iget v5, v5, Litt;->a:I

    aget-object v4, v4, v5

    aput-object v4, v3, v2

    aput-object p1, v3, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lemt;->b:Litt;

    iget v2, v2, Litt;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    iget-object v1, p0, Lemt;->b:Litt;

    iget v1, v1, Litt;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v8

    iget-object v1, p0, Lemt;->b:Litt;

    iget v1, v1, Litt;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v9

    const/4 v1, 0x5

    iget-object v2, p0, Lemt;->b:Litt;

    iget v2, v2, Litt;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lemt;->b:Litt;

    iget v2, v2, Litt;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lemt;->b:Litt;

    iget v2, v2, Litt;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lemt;->b:Litt;

    iget v2, v2, Litt;->h:I

    iget-object v4, p0, Lemt;->b:Litt;

    iget v4, v4, Litt;->i:I

    add-int/2addr v2, v4

    iget-object v4, p0, Lemt;->b:Litt;

    iget v4, v4, Litt;->j:I

    add-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lehe;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lemt;->b:Litt;

    return-object v0

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 6

    const/4 v1, 0x1

    iget v0, p0, Lemt;->d:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lemt;->b:Litt;

    iget-wide v4, p0, Lemt;->c:J

    sub-long v4, v2, v4

    long-to-int v4, v4

    iput v4, v0, Litt;->h:I

    iput-wide v2, p0, Lemt;->c:J

    iput v1, p0, Lemt;->d:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    const/4 v0, 0x1

    iget v1, p0, Lemt;->d:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lemt;->b:Litt;

    iget-wide v3, p0, Lemt;->c:J

    sub-long v3, v0, v3

    long-to-int v3, v3

    iput v3, v2, Litt;->i:I

    iput-wide v0, p0, Lemt;->c:J

    const/4 v0, 0x2

    iput v0, p0, Lemt;->d:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    iget v0, p0, Lemt;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lemt;->b:Litt;

    iget-wide v3, p0, Lemt;->c:J

    sub-long v3, v0, v3

    long-to-int v3, v3

    iput v3, v2, Litt;->j:I

    iput-wide v0, p0, Lemt;->c:J

    const/4 v0, 0x3

    iput v0, p0, Lemt;->d:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
