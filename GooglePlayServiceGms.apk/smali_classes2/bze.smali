.class public final Lbze;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Landroid/view/View$OnClickListener;

.field final c:Landroid/database/DataSetObserver;

.field final d:I

.field e:Z

.field f:Z

.field g:Ljava/lang/String;

.field h:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;Landroid/database/DataSetObserver;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lbze;->e:Z

    iput-boolean v0, p0, Lbze;->f:Z

    const-string v0, ""

    iput-object v0, p0, Lbze;->g:Ljava/lang/String;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lbze;->a:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lbze;->b:Landroid/view/View$OnClickListener;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    iput-object v0, p0, Lbze;->c:Landroid/database/DataSetObserver;

    const v0, 0x7f04004e    # com.google.android.gms.R.layout.drive_doc_list_view_sync_more_button

    iput v0, p0, Lbze;->d:I

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lbze;->h:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbze;->h:Landroid/widget/Button;

    iget-object v1, p0, Lbze;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbze;->h:Landroid/widget/Button;

    iget-boolean v1, p0, Lbze;->f:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method
