.class public final Lbab;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Lazy;

.field public b:Lbaa;

.field public c:Lazz;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v0, p0, Lbab;->a:Lazy;

    iput-object v0, p0, Lbab;->b:Lbaa;

    iput-object v0, p0, Lbab;->c:Lazz;

    const/4 v0, -0x1

    iput v0, p0, Lbab;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbab;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lbab;->b()I

    :cond_0
    iget v0, p0, Lbab;->g:I

    return v0
.end method

.method public final a(Lazy;)Lbab;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbab;->d:Z

    iput-object p1, p0, Lbab;->a:Lazy;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lazy;

    invoke-direct {v0}, Lazy;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lbab;->a(Lazy;)Lbab;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lbaa;

    invoke-direct {v0}, Lbaa;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Lbab;->e:Z

    iput-object v0, p0, Lbab;->b:Lbaa;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lazz;

    invoke-direct {v0}, Lazz;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Lbab;->f:Z

    iput-object v0, p0, Lbab;->c:Lazz;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lbab;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lbab;->a:Lazy;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Lbab;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lbab;->b:Lbaa;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Lbab;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lbab;->c:Lazz;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lbab;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lbab;->a:Lazy;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lbab;->e:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lbab;->b:Lbaa;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lbab;->f:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lbab;->c:Lazz;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lbab;->g:I

    return v0
.end method
