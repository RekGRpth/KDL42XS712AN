.class final Lnr;
.super Lna;
.source "SourceFile"

# interfaces
.implements Lnt;


# instance fields
.field l:Landroid/widget/ListAdapter;

.field final synthetic m:Lno;

.field private n:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lno;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    iput-object p1, p0, Lnr;->m:Lno;

    invoke-direct {p0, p2, p3, p4}, Lna;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lna;->h:Landroid/view/View;

    invoke-virtual {p0}, Lnr;->a()V

    const/4 v0, 0x0

    iput v0, p0, Lna;->g:I

    new-instance v0, Lmv;

    new-instance v1, Lns;

    invoke-direct {v1, p0, p1}, Lns;-><init>(Lnr;Lno;)V

    invoke-direct {v0, p1, v1}, Lmv;-><init>(Lms;Lmu;)V

    iput-object v0, p0, Lna;->i:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ListAdapter;)V
    .locals 0

    invoke-super {p0, p1}, Lna;->a(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Lnr;->l:Landroid/widget/ListAdapter;

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lnr;->n:Ljava/lang/CharSequence;

    return-void
.end method

.method public final b()V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-virtual {v0}, Lno;->getPaddingLeft()I

    move-result v2

    iget-object v0, p0, Lnr;->m:Lno;

    iget v0, v0, Lno;->E:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-virtual {v0}, Lno;->getWidth()I

    move-result v3

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-virtual {v0}, Lno;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lnr;->m:Lno;

    iget-object v0, p0, Lnr;->l:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    iget-object v6, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lno;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    sub-int/2addr v3, v2

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lnr;->a(I)V

    :goto_0
    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lnr;->m:Lno;

    invoke-static {v3}, Lno;->a(Lno;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    :goto_1
    add-int/2addr v0, v2

    iput v0, p0, Lna;->c:I

    invoke-virtual {p0}, Lnr;->d()V

    invoke-super {p0}, Lna;->b()V

    iget-object v0, p0, Lna;->b:Lnd;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-virtual {v0}, Lno;->d()I

    move-result v0

    iget-object v2, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    invoke-static {v2, v1}, Lnd;->a(Lnd;Z)Z

    invoke-virtual {v2, v0}, Lnd;->setSelection(I)V

    invoke-virtual {v2}, Lnd;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v0, v7}, Lnd;->setItemChecked(IZ)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lnr;->m:Lno;

    iget v0, v0, Lno;->E:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lnr;->m:Lno;

    invoke-virtual {v0}, Lno;->getWidth()I

    move-result v0

    iget-object v3, p0, Lnr;->m:Lno;

    invoke-virtual {v3}, Lno;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v2

    sub-int/2addr v0, v3

    invoke-virtual {p0, v0}, Lnr;->a(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lnr;->m:Lno;

    iget v0, v0, Lno;->E:I

    invoke-virtual {p0, v0}, Lnr;->a(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
