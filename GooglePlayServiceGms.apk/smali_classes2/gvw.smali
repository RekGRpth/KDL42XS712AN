.class public final Lgvw;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    const v0, 0x7f04013d    # com.google.android.gms.R.layout.wallet_row_address_spinner

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgvw;->b:Landroid/view/LayoutInflater;

    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    instance-of v0, p0, Lipv;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lgvv;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v3, 0x0

    const v9, 0x7f0a0343    # com.google.android.gms.R.id.address_details

    const/16 v8, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-static {v1}, Lgvw;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lgvw;->a(Ljava/lang/Object;)I

    move-result v2

    if-eq v0, v2, :cond_0

    move-object p3, v3

    :cond_0
    iget-object v0, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->isEnabled()Z

    move-result v4

    instance-of v0, v1, Lipv;

    if-eqz v0, :cond_f

    move-object v0, v1

    check-cast v0, Lipv;

    if-eqz p3, :cond_1

    invoke-static {p3}, Lgvw;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-eqz p2, :cond_7

    iget-object v2, p0, Lgvw;->b:Landroid/view/LayoutInflater;

    const v7, 0x7f04013e    # com.google.android.gms.R.layout.wallet_row_address_spinner_drop_down

    invoke-virtual {v2, v7, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_2
    :goto_0
    if-eqz v4, :cond_8

    iget-object v2, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Lipv;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v4, v5

    :goto_1
    iget-object v2, v0, Lipv;->a:Lixo;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lipv;->a:Lixo;

    move-object v7, v2

    :goto_2
    if-eqz p2, :cond_e

    invoke-virtual {p3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const-string v3, "\n"

    invoke-static {v7, v3}, Lgvs;->a(Lixo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0a0344    # com.google.android.gms.R.id.phone_details

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v0, Lipv;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    const v2, 0x7f0a0345    # com.google.android.gms.R.id.error

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Lipv;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-static {v0}, Lgth;->c(Lipv;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v0}, Lgth;->b(Lipv;)Z

    move-result v7

    if-nez v7, :cond_b

    :cond_3
    const v0, 0x7f0b0173    # com.google.android.gms.R.string.wallet_min_address_editable

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    :goto_4
    if-eqz p3, :cond_6

    if-nez p2, :cond_5

    iget-object v0, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;)I

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0a0112    # com.google.android.gms.R.id.label

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-static {p3, v4}, Lbqc;->a(Landroid/view/View;Z)V

    :cond_6
    return-object p3

    :cond_7
    iget-object v2, p0, Lgvw;->b:Landroid/view/LayoutInflater;

    const v7, 0x7f04013d    # com.google.android.gms.R.layout.wallet_row_address_spinner

    invoke-virtual {v2, v7, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    :cond_8
    move v4, v6

    goto/16 :goto_1

    :cond_9
    new-instance v2, Lixo;

    invoke-direct {v2}, Lixo;-><init>()V

    move-object v7, v2

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_b
    if-eqz v3, :cond_c

    iget-object v7, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;)Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-static {v0}, Lgth;->a(Lipv;)Z

    move-result v7

    if-nez v7, :cond_c

    const v0, 0x7f0b0171    # com.google.android.gms.R.string.wallet_missing_phone_editable

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_c
    if-nez v3, :cond_d

    iget-object v3, v0, Lipv;->i:[I

    array-length v3, v3

    if-ne v3, v5, :cond_d

    iget-object v0, v0, Lipv;->i:[I

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lboz;->a([II)Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x7f0b0172    # com.google.android.gms.R.string.wallet_unsupported_country_for_merchant

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_d
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_e
    const v0, 0x7f0a0342    # com.google.android.gms.R.id.recipient

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v5, v7, Lixo;->s:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ", "

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c()[C

    move-result-object v5

    invoke-static {v7, v0, v3, v5}, Lgvs;->a(Lixo;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_f
    instance-of v0, v1, Lgvv;

    if-eqz v0, :cond_4

    if-eqz p3, :cond_10

    invoke-static {p3}, Lgvw;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    if-eqz p2, :cond_12

    iget-object v0, p0, Lgvw;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f04013b    # com.google.android.gms.R.layout.wallet_row_action_item_drop_down

    invoke-virtual {v0, v2, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_11
    :goto_5
    move-object v0, v1

    check-cast v0, Lgvv;

    const v2, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v0, Lgvv;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, v0, Lgvv;->c:I

    invoke-virtual {v2, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_4

    :cond_12
    iget-object v0, p0, Lgvw;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f04013a    # com.google.android.gms.R.layout.wallet_row_action_item

    invoke-virtual {v0, v2, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto :goto_5
.end method

.method public static synthetic a(Lgvw;)Ljava/util/ArrayList;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lgvw;->getCount()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lgvw;->getCount()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lipv;

    if-eqz v4, :cond_0

    check-cast v0, Lipv;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private static a(Landroid/view/View;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lipv;)I
    .locals 6

    const/4 v2, -0x1

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lgvw;->getCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lipv;

    if-eqz v4, :cond_1

    check-cast v0, Lipv;

    iget-object v4, p1, Lipv;->b:Ljava/lang/String;

    iget-object v5, v0, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v3, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Lipv;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lgvw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvx;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lgvx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0, p1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lgvv;

    if-eqz v1, :cond_0

    check-cast v0, Lgvv;

    iget-boolean v0, v0, Lgvv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lgvw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lgvw;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lgvw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvx;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lgvx;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgvw;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lgvw;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    instance-of v2, v0, Lipv;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lgvw;->a:Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;

    check-cast v0, Lipv;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Lipv;)Z

    move-result v0

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lgvv;

    if-eqz v2, :cond_2

    check-cast v0, Lgvv;

    iget-boolean v0, v0, Lgvv;->b:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
