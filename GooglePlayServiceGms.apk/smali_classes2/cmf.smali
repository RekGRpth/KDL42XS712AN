.class public final Lcmf;
.super Lclt;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final c:J

.field private final d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 8

    sget-object v7, Lcms;->a:Lcms;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcmf;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V
    .locals 6

    sget-object v1, Lcmr;->j:Lcmr;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-wide p3, p0, Lcmf;->c:J

    iput-object p5, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->e:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmf;->c:J

    const-string v0, "metadata"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcjc;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-void
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmf;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 4

    iget-wide v0, p0, Lcmf;->c:J

    invoke-interface {p1, v0, v1}, Lcfz;->b(J)Lcge;

    move-result-object v0

    iget-object v0, v0, Lcge;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcfp;->s(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcfp;->k()V

    new-instance v0, Lcmj;

    iget-object v1, p3, Lbsp;->a:Lcfc;

    iget-object v2, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcms;->b:Lcms;

    invoke-direct {v0, v1, v2, v3}, Lcmj;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 7

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcmf;->b(Lcfz;)Lbsp;

    move-result-object v6

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcmf;->a(Lcfz;)Lcfp;

    move-result-object v0

    iget-wide v2, p0, Lcmf;->c:J

    iget-object v4, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const/4 v5, 0x1

    move-object v1, p3

    invoke-static/range {v0 .. v6}, Lbty;->a(Lcfp;Lcoy;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLbsp;)Lbty;

    move-result-object v1

    new-instance v2, Lbtv;

    invoke-direct {v2, p3}, Lbtv;-><init>(Lcoy;)V

    :try_start_0
    new-instance v3, Lbtu;

    invoke-direct {v3}, Lbtu;-><init>()V

    invoke-virtual {v2, v1, v3}, Lbtv;->a(Lbty;Lbtw;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/File;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfp;->n(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfp;->k()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbua; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbub; {:try_start_0 .. :try_end_0} :catch_2

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v0

    iget-wide v1, p0, Lcmf;->c:J

    invoke-interface {v0, v1, v2}, Lcfz;->b(J)Lcge;

    move-result-object v0

    invoke-virtual {v0}, Lcge;->l()V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "pendingUploadSqlId"

    iget-wide v2, p0, Lcmf;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "metadata"

    iget-object v2, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcjc;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcmf;

    iget-wide v2, p0, Lcmf;->c:J

    iget-wide v4, p1, Lcmf;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget-wide v0, p0, Lcmf;->c:J

    iget-wide v2, p0, Lcmf;->c:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CreateFileOp[%s, pendingUploadSqlId=%d, initialMetadata=%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcmf;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcmf;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcmf;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
