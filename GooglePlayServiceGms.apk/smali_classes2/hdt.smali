.class final Lhdt;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhdj;


# direct methods
.method constructor <init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhdt;->f:Lhdj;

    iput-object p4, p0, Lhdt;->d:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    iput-object p5, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/4 v0, 0x2

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 11

    iget-object v0, p0, Lhdt;->d:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->b()Lipb;

    move-result-object v5

    iget-object v0, v5, Lipb;->b:Linx;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lipb;->b:Linx;

    iget-object v0, v0, Linx;->c:Linv;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lipb;->b:Linx;

    iget-object v0, v0, Linx;->c:Linv;

    iget v1, v0, Linv;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create profile currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v0, Linv;->b:Lint;

    iget-object v0, v0, Lint;->a:Linu;

    invoke-static {v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v5

    check-cast v5, Lipb;

    iget-object v1, v5, Lipb;->b:Linx;

    iget-object v1, v1, Linx;->c:Linv;

    iget-object v1, v1, Linv;->b:Lint;

    const/4 v2, 0x0

    iput-object v2, v1, Lint;->a:Linu;

    iget-object v1, p0, Lhdt;->f:Lhdj;

    invoke-static {v1}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v2, p0, Lhdt;->f:Lhdj;

    iget-object v2, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Linu;->a:Ljava/lang/String;

    iget-object v7, v0, Linu;->b:Ljava/lang/String;

    iget-object v0, p0, Lhdt;->a:Landroid/accounts/Account;

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v9

    iget-object v0, v5, Lipb;->b:Linx;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lipb;->b:Linx;

    iget-object v0, v0, Linx;->c:Linv;

    invoke-static {v0}, Lhcv;->a(Linv;)V

    :cond_1
    iget-object v10, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhdg;

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lhdg;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Lipb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_profile"

    invoke-static {v10, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lhdt;->f:Lhdj;

    invoke-static {v0}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v0, p0, Lhdt;->f:Lhdj;

    iget-object v0, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lhdt;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdt;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhdh;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lhdh;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Lipb;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_profile"

    invoke-static {v8, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
