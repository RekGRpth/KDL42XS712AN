.class public Lbvy;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lcov;

.field private final b:Lcll;

.field private final c:Lcfz;

.field private final d:Ljava/util/concurrent/BlockingQueue;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;

.field private final f:Lcom/google/android/gms/common/server/ClientContext;

.field private final g:Lcfc;

.field private final h:Lbuw;

.field private final i:I

.field private final j:I

.field private volatile k:Z


# direct methods
.method public constructor <init>(Lcoy;Ljava/util/concurrent/BlockingQueue;Lbuw;Ljava/lang/String;II)V
    .locals 8

    new-instance v7, Lcbf;

    invoke-virtual {p1}, Lcoy;->a()Lcon;

    move-result-object v0

    sget-object v1, Lbqs;->j:Lbfy;

    sget-object v2, Lbqs;->k:Lbfy;

    invoke-direct {v7, v0, v1, v2}, Lcbf;-><init>(Lcon;Lbfy;Lbfy;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lbvy;-><init>(Lcoy;Ljava/util/concurrent/BlockingQueue;Lbuw;Ljava/lang/String;IILcov;)V

    return-void
.end method

.method private constructor <init>(Lcoy;Ljava/util/concurrent/BlockingQueue;Lbuw;Ljava/lang/String;IILcov;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvy;->k:Z

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-interface {v0, p4}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    iput-object v0, p0, Lbvy;->g:Lcfc;

    iget-object v0, p0, Lbvy;->g:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    invoke-virtual {v0}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iput-object v0, p0, Lbvy;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Lbvy;->g:Lcfc;

    new-instance v1, Lbsp;

    sget-object v2, Lbqr;->b:Lbqr;

    sget-object v3, Lbqr;->d:Lbqr;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lbsp;-><init>(Lcfc;Ljava/util/Set;)V

    invoke-virtual {v1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iput-object v0, p0, Lbvy;->f:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcov;

    iput-object v0, p0, Lbvy;->a:Lcov;

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v0

    iput-object v0, p0, Lbvy;->b:Lcll;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbvy;->c:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/BlockingQueue;

    iput-object v0, p0, Lbvy;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuw;

    iput-object v0, p0, Lbvy;->h:Lbuw;

    iput p5, p0, Lbvy;->i:I

    iput p6, p0, Lbvy;->j:I

    const-class v0, Lbvy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbvy;->setName(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-virtual {p0}, Lbvy;->start()V

    return-void
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, Lbvy;->join()V

    return-void
.end method

.method public final c()Z
    .locals 1

    invoke-virtual {p0}, Lbvy;->isAlive()Z

    move-result v0

    return v0
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvy;->k:Z

    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 11

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v7, p0, Lbvy;->h:Lbuw;

    move v10, v9

    :goto_0
    :try_start_0
    invoke-virtual {v7}, Lbuw;->c()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lbvy;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_4

    iget v1, p0, Lbvy;->i:I

    if-ge v10, v1, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, p0, Lbvy;->a:Lcov;

    invoke-interface {v1}, Lcov;->d()V

    iget-object v1, v7, Lbuw;->a:Lbuv;

    iget-object v1, v1, Lbuv;->c:Lbux;

    sget-object v2, Lbvz;->a:[I

    invoke-virtual {v1}, Lbux;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported feed type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lbvy;->a:Lcov;

    invoke-interface {v2}, Lcov;->e()V

    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v1

    move-object v1, v7

    :goto_1
    const-string v2, "ResultsPageProducer"

    const-string v3, "Process was interrupted. Cannot fetch %s."

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v1, v1, Lbuw;->a:Lbuv;

    aput-object v1, v4, v9

    invoke-static {v2, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_2
    return-void

    :pswitch_0
    :try_start_3
    iget-object v1, v7, Lbuw;->a:Lbuv;

    instance-of v1, v1, Lbvc;

    invoke-static {v1}, Lbkm;->b(Z)V

    iget-object v1, v7, Lbuw;->a:Lbuv;

    check-cast v1, Lbvc;

    iget-object v2, p0, Lbvy;->b:Lcll;

    iget-object v3, p0, Lbvy;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, v1, Lbvc;->b:Ljava/util/Set;

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v5

    iget-boolean v1, v1, Lbvc;->a:Z

    invoke-interface {v2, v3, v4, v5, v1}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Z)Lclm;

    move-result-object v1

    :goto_3
    iget-object v2, p0, Lbvy;->d:Ljava/util/concurrent/BlockingQueue;

    iget v3, p0, Lbvy;->j:I

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lbwa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v3, v1, v5, v6}, Lbwa;-><init>(ILclm;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v5, 0x7fffffffffffffffL

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v6, v3}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    invoke-interface {v1}, Lclm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lbuw;->a(Lbuw;Ljava/lang/String;)Lbuw;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    add-int/lit8 v2, v10, 0x1

    :try_start_4
    iget-object v3, p0, Lbvy;->a:Lcov;

    invoke-interface {v3}, Lcov;->e()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move v10, v2

    move-object v7, v1

    goto/16 :goto_0

    :pswitch_1
    :try_start_5
    iget-object v1, v7, Lbuw;->a:Lbuv;

    instance-of v1, v1, Lbuu;

    invoke-static {v1}, Lbkm;->b(Z)V

    iget-object v1, v7, Lbuw;->a:Lbuv;

    move-object v0, v1

    check-cast v0, Lbuu;

    move-object v5, v0

    iget-object v1, p0, Lbvy;->b:Lcll;

    iget-object v2, p0, Lbvy;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, v5, Lbuu;->b:Ljava/util/Set;

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v4

    iget-wide v5, v5, Lbuu;->a:J

    invoke-interface/range {v1 .. v6}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;J)Lclm;

    move-result-object v1

    goto :goto_3

    :pswitch_2
    iget-object v1, v7, Lbuw;->a:Lbuv;

    move-object v0, v1

    check-cast v0, Lbvd;

    move-object v5, v0

    iget-object v1, p0, Lbvy;->b:Lcll;

    iget-object v2, p0, Lbvy;->e:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v5, Lbvd;->a:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface/range {v1 .. v6}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;

    move-result-object v1

    goto :goto_3

    :pswitch_3
    iget-object v1, v7, Lbuw;->a:Lbuv;

    move-object v0, v1

    check-cast v0, Lbut;

    move-object v3, v0

    sget-object v1, Lbth;->g:Lbth;

    invoke-static {v1}, Lbti;->a(Lbth;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v3, Lbut;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v8, :cond_1

    move v1, v8

    :goto_4
    invoke-static {v1}, Lbkm;->b(Z)V

    iget-object v1, v3, Lbut;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lbvy;->c:Lcfz;

    iget-object v4, p0, Lbvy;->g:Lcfc;

    invoke-interface {v3, v4, v1, v2}, Lcfz;->a(Lcfc;J)Lbsp;

    move-result-object v6

    if-nez v6, :cond_2

    new-instance v1, Ljava/io/IOException;

    const-string v2, "App that was sync no longer exists"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move v1, v9

    goto :goto_4

    :cond_2
    invoke-virtual {v6}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lbqr;->b:Lbqr;

    invoke-virtual {v4}, Lbqr;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    iget-object v1, p0, Lbvy;->b:Lcll;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface/range {v1 .. v6}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;

    move-result-object v1

    goto/16 :goto_3

    :cond_3
    iget-object v1, p0, Lbvy;->b:Lcll;

    iget-object v2, p0, Lbvy;->f:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, v3, Lbut;->a:Ljava/util/Set;

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v1 .. v6}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v1

    goto/16 :goto_3

    :cond_4
    :try_start_6
    invoke-virtual {v7}, Lbuw;->b()Z

    move-result v1

    invoke-static {v1}, Lbkm;->b(Z)V

    iget-object v1, p0, Lbvy;->d:Ljava/util/concurrent/BlockingQueue;

    iget v2, p0, Lbvy;->j:I

    invoke-virtual {v7}, Lbuw;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lbwa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v2, v5, v6, v3}, Lbwa;-><init>(ILclm;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v2, 0x7fffffffffffffffL

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v4, v2, v3, v5}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v1

    :goto_5
    const-string v2, "ResultsPageProducer"

    const-string v3, "Error fetching %s."

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v7, Lbuw;->a:Lbuv;

    aput-object v5, v4, v9

    invoke-static {v2, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v2, p0, Lbvy;->k:Z

    if-nez v2, :cond_0

    :try_start_7
    iget-object v2, p0, Lbvy;->d:Ljava/util/concurrent/BlockingQueue;

    iget v3, p0, Lbvy;->j:I

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lbwa;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v1, v6}, Lbwa;-><init>(ILclm;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v5, 0x7fffffffffffffffL

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v6, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v1

    const-string v1, "ResultsPageProducer"

    const-string v2, "Producer cannot be terminated gracefully %s."

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, v7, Lbuw;->a:Lbuv;

    aput-object v4, v3, v9

    invoke-static {v1, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_2

    :catch_3
    move-exception v2

    move-object v7, v1

    move-object v1, v2

    goto :goto_5

    :catch_4
    move-exception v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
