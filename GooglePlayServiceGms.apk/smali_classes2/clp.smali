.class public final Lclp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcll;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/Map;

.field private static final c:Ljava/util/Map;


# instance fields
.field private d:Lcls;

.field private e:Lcls;

.field private final f:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "application/vnd.google-apps.folder"

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "mimeType = \'%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lclp;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lclp;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lclp;->c:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lclp;->f:Landroid/content/Context;

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcls;
    .locals 2

    new-instance v1, Lcls;

    sget-object v0, Lbqs;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, p0, v0, p1}, Lcls;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Ljava/lang/Class;Z)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    if-eqz p1, :cond_1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v4, "authorizedAppIds"

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->j()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ApiaryRemoteResourceAccessor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to create instance:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_1
    move-exception v0

    const-string v1, "ApiaryRemoteResourceAccessor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to create instance:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method private static a(Ljava/util/Set;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-interface {p0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;
    .locals 10

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v1

    new-instance v9, Lcoe;

    invoke-direct {v9, v1}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v1, Lcoi;

    invoke-direct {v1}, Lcoi;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/FileList;

    invoke-static {p1}, Lclp;->c(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v2, v0}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcoi;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lcoi;

    invoke-static {p2}, Lclp;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v8}, Lcoe;->a(Lcoi;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v9, Lcoe;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/FileList;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/FileList;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileList;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/internal/model/File;

    new-instance v4, Lclo;

    invoke-direct {v4, v1, p5}, Lclo;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    :try_start_1
    new-instance v1, Lclq;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileList;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lclq;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;
    .locals 2

    iget-object v0, p0, Lclp;->d:Lcls;

    if-nez v0, :cond_0

    iget-object v0, p0, Lclp;->f:Landroid/content/Context;

    const-string v1, "/drive/v2/"

    invoke-static {v0, v1}, Lclp;->a(Landroid/content/Context;Ljava/lang/String;)Lcls;

    move-result-object v0

    iput-object v0, p0, Lclp;->d:Lcls;

    :cond_0
    iget-object v0, p0, Lclp;->e:Lcls;

    if-nez v0, :cond_1

    iget-object v0, p0, Lclp;->f:Landroid/content/Context;

    const-string v1, "/drive/v2internal/"

    invoke-static {v0, v1}, Lclp;->a(Landroid/content/Context;Ljava/lang/String;)Lcls;

    move-result-object v0

    iput-object v0, p0, Lclp;->e:Lcls;

    :cond_1
    invoke-static {p1}, Lclp;->c(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lclp;->e:Lcls;

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lclp;->d:Lcls;

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 6

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v1

    new-instance v0, Lcoe;

    invoke-direct {v0, v1}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v1, Lcog;

    invoke-direct {v1}, Lcog;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/File;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcog;->a(Ljava/lang/String;)Lbmg;

    move-result-object v5

    check-cast v5, Lcog;

    invoke-static {p3}, Lclp;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcoe;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcog;)Lcom/google/android/gms/drive/internal/model/File;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b(Ljava/lang/Class;Z)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    sget-object v0, Lclp;->c:Ljava/util/Map;

    move-object v1, v0

    :goto_0
    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lclp;->a(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    sget-object v0, Lclp;->b:Ljava/util/Map;

    move-object v1, v0

    goto :goto_0
.end method

.method private static c(Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)J
    .locals 6

    :try_start_0
    new-instance v0, Lcoa;

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v1

    invoke-direct {v0, v1}, Lcoa;-><init>(Lbmi;)V

    new-instance v1, Lcob;

    invoke-direct {v1}, Lcob;-><init>()V

    const-string v2, "self"

    invoke-static {v1, v2}, Lcoa;->a(Lcob;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcoa;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/App;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/App;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/App;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;JJ)Lclj;
    .locals 6

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v1, Lcny;

    invoke-direct {v1, v0}, Lcny;-><init>(Lbmi;)V

    new-instance v0, Lcnz;

    invoke-direct {v0}, Lcnz;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/About;

    invoke-static {v2, v3}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcnz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lcnz;

    const/4 v2, 0x1

    :try_start_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lcny;->a(Lcnz;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcny;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/About;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/About;

    new-instance v1, Lcln;

    invoke-direct {v1, v0}, Lcln;-><init>(Lcom/google/android/gms/drive/internal/model/About;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Ljava/lang/String;Lbsp;)Lclk;
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcle;->c:Lcje;

    invoke-virtual {p2, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, Lcle;->c:Lcje;

    const-string v3, "application/vnd.google-apps.folder"

    invoke-virtual {p2, v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_0
    invoke-static {p2}, Lcjc;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v9

    const-string v2, "root"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz p3, :cond_2

    if-nez v2, :cond_2

    const-string v2, "appdata"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p3, "appdata"

    :cond_1
    new-instance v2, Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-direct {v2}, Lcom/google/android/gms/drive/internal/model/ParentReference;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/model/ParentReference;->f()V

    invoke-virtual {v2, p3}, Lcom/google/android/gms/drive/internal/model/ParentReference;->e(Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9, v3}, Lcom/google/android/gms/drive/internal/model/File;->a(Ljava/util/List;)V

    :cond_2
    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v2

    new-instance v10, Lcoe;

    invoke-direct {v10, v2}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v2, Lcoh;

    invoke-direct {v2}, Lcoh;-><init>()V

    const-class v3, Lcom/google/android/gms/drive/internal/model/File;

    invoke-static {p1}, Lclp;->c(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v4

    if-nez v4, :cond_3

    :goto_0
    invoke-static {v3, v0}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcoh;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lcoh;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcoe;->a(Lcoh;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v10, Lcoe;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    new-instance v1, Lclo;

    invoke-direct {v1, v0, p4}, Lclo;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :cond_3
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lclk;
    .locals 6

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v1, Lcoe;

    invoke-direct {v1, v0}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v0, Lcof;

    invoke-direct {v0}, Lcof;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/File;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcof;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lcof;

    invoke-static {v0, p3, p2}, Lcoe;->a(Lcof;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcoe;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    new-instance v1, Lclo;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lclo;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lclk;
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    new-instance v1, Lclo;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lclo;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;J)Lclm;
    .locals 8

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v7, Lcoc;

    invoke-direct {v7, v0}, Lcoc;-><init>(Lbmi;)V

    :try_start_0
    new-instance v0, Lcod;

    invoke-direct {v0}, Lcod;-><init>()V

    const-class v1, Lcom/google/android/gms/drive/internal/model/ChangeList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lclp;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcod;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lcod;

    invoke-static {p2}, Lclp;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcoc;->a(Lcod;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v7, Lcoc;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/ChangeList;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/ChangeList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/internal/model/Change;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lclr;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lclr;-><init>(Ljava/lang/String;)V

    move-object v1, v2

    :goto_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    new-instance v2, Lclo;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->e()Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    const/4 v5, 0x0

    invoke-direct {v2, v1, v5}, Lclo;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V

    move-object v1, v2

    goto :goto_1

    :cond_1
    new-instance v1, Lclq;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v1, v3, v2, v0}, Lclq;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;
    .locals 1

    invoke-direct/range {p0 .. p5}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Z)Lclm;
    .locals 6

    const/4 v5, 0x0

    if-eqz p4, :cond_0

    sget-object v4, Lclp;->a:Ljava/lang/String;

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lbsp;)Lclm;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v4, v5

    goto :goto_0
.end method

.method public final a(Lbsp;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    invoke-direct {p0, v1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v2

    new-instance v0, Lcoe;

    invoke-direct {v0, v2}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v2, Lcog;

    invoke-direct {v2}, Lcog;-><init>()V

    const-string v3, "id"

    invoke-virtual {v2, v3}, Lcog;->a(Ljava/lang/String;)Lbmg;

    move-result-object v5

    check-cast v5, Lcog;

    const-string v2, "appdata"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcoe;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcog;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->q()Ljava/lang/String;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v1, Lcoe;

    invoke-direct {v1, v0}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v0, Lcoj;

    invoke-direct {v0}, Lcoj;-><init>()V

    invoke-static {v0, p2}, Lcoe;->a(Lcoj;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcoe;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 17

    invoke-direct/range {p0 .. p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v15, Lcoe;

    invoke-direct {v15, v0}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v0, Lcol;

    invoke-direct {v0}, Lcol;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-static/range {p3 .. p3}, Lcjc;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v16

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v1, p2

    invoke-static/range {v0 .. v14}, Lcoe;->a(Lcol;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v15, Lcoe;->a:Lbmi;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object/from16 v1, p1

    move-object/from16 v4, v16

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0, p1}, Lclp;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcls;

    move-result-object v0

    new-instance v1, Lcoe;

    invoke-direct {v1, v0}, Lcoe;-><init>(Lbmi;)V

    :try_start_0
    new-instance v0, Lcok;

    invoke-direct {v0}, Lcok;-><init>()V

    invoke-static {v0, p2}, Lcoe;->a(Lcok;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcoe;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
