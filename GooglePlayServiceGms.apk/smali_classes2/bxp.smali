.class final Lbxp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lbxn;


# direct methods
.method constructor <init>(Lbxn;)V
    .locals 0

    iput-object p1, p0, Lbxp;->a:Lbxn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcbm;

    iget-object v1, p0, Lbxp;->a:Lbxn;

    iget-object v1, v1, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbm;-><init>(Lcoy;)V

    iget-object v1, v0, Lcbm;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->g()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v0, v0, Lcbm;->b:Lcfz;

    invoke-interface {v0}, Lcfz;->g()V

    :cond_0
    iget-object v0, p0, Lbxp;->a:Lbxn;

    iget-object v0, v0, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    const v1, 0x7f0b0072    # com.google.android.gms.R.string.drive_storage_management_unpin_all_success

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lbxp;->a:Lbxn;

    iget-object v0, v0, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    new-instance v1, Lbxq;

    iget-object v2, p0, Lbxp;->a:Lbxn;

    iget-object v2, v2, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-direct {v1, v2, v5}, Lbxq;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->a(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;Lbxq;)Lbxq;

    iget-object v0, p0, Lbxp;->a:Lbxn;

    iget-object v0, v0, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->b(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Lbxq;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbxq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
