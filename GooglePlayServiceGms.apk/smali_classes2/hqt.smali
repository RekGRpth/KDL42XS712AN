.class public final Lhqt;
.super Lhpr;
.source "SourceFile"


# instance fields
.field private final c:Lhqm;

.field private final d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field private e:I

.field private f:J


# direct methods
.method public constructor <init>(Lhrx;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhqm;)V
    .locals 2

    invoke-direct {p0, p1}, Lhpr;-><init>(Lhrx;)V

    const/4 v0, 0x0

    iput v0, p0, Lhqt;->e:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhqt;->f:J

    iput-object p3, p0, Lhqt;->c:Lhqm;

    iput-object p2, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lhqt;->c:Lhqm;

    iget-object v1, p0, Lhqt;->a:Ljava/lang/Runnable;

    iget-object v2, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lhqm;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final a(Lhrz;J)V
    .locals 6

    sget-object v0, Lhrz;->d:Lhrz;

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lhqt;->e:I

    iget-object v1, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v1}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lhqt;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhqt;->e:I

    iget v0, p0, Lhqt;->e:I

    iget-object v1, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v1}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lhqt;->c:Lhqm;

    iget-object v1, p0, Lhqt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhqm;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lhqt;->c:Lhqm;

    iget-object v1, p0, Lhqt;->a:Ljava/lang/Runnable;

    iget-object v2, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lhqm;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lhqt;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iput-wide p2, p0, Lhqt;->f:J

    goto :goto_0

    :cond_3
    iget-wide v0, p0, Lhqt;->f:J

    sub-long v0, p2, v0

    iget-object v2, p0, Lhqt;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lhqt;->c:Lhqm;

    iget-object v1, p0, Lhqt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhqm;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhqt;->c:Lhqm;

    iget-object v1, p0, Lhqt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhqm;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
