.class public final Lgsh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljai;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v0

    invoke-direct {p0, v1, v1, v0}, Lgsh;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljai;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljai;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgsh;->a:Ljava/lang/String;

    iput-object p2, p0, Lgsh;->b:Ljava/lang/String;

    if-nez p3, :cond_0

    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lgsh;->c:[Ljai;

    return-void
.end method

.method static a(Ljava/lang/String;)Lgsh;
    .locals 5

    invoke-static {p0}, Lgtq;->a(Ljava/lang/String;)Lgtr;

    move-result-object v0

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lgtr;->c()Ljava/lang/String;

    invoke-virtual {v0}, Lgtr;->b()Ljava/lang/String;

    move-result-object v2

    const-class v3, Ljai;

    invoke-static {}, Ljai;->c()[Ljai;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lgtr;->a(Ljava/lang/Class;[Lizs;)[Lizs;

    move-result-object v0

    check-cast v0, [Ljai;

    new-instance v3, Lgsh;

    invoke-direct {v3, v1, v2, v0}, Lgsh;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljai;)V

    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgsh;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "SHA-256"

    invoke-static {p1, p2, v0, v1}, Lhbq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lgtq;->a()Lgts;

    move-result-object v0

    iget-object v1, p0, Lgsh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgts;->a(I)Lgts;

    move-result-object v0

    iget-object v1, p0, Lgsh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v0

    iget-object v1, p0, Lgsh;->c:[Ljai;

    if-nez v1, :cond_0

    iget-object v1, v0, Lgts;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v0}, Lgts;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v2, v0, Lgts;->a:Ljava/util/ArrayList;

    const-string v3, "\u203d"

    const/4 v4, 0x1

    invoke-static {v3, v1, v4}, Lgtq;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
