.class public final Lfyk;
.super Lfye;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    iget-boolean v0, p0, Lfyk;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400e8    # com.google.android.gms.R.layout.plus_oob_field_info_setup_wizard

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400e7    # com.google.android.gms.R.layout.plus_oob_field_info

    goto :goto_0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0b0333    # com.google.android.gms.R.string.plus_oob_field_view_tag_info

    invoke-virtual {p0, v0}, Lfyk;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfyk;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfyk;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lfyk;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lfyk;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfyk;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lgfm;
    .locals 2

    invoke-virtual {p0}, Lfyk;->i()Lgfn;

    move-result-object v0

    new-instance v1, Lgfu;

    invoke-direct {v1}, Lgfu;-><init>()V

    invoke-virtual {v1}, Lgfu;->a()Lgft;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    return-object v0
.end method

.method protected final g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    const-string v0, "domainInfo"

    iget-object v1, p0, Lfyk;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbcq;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "termsOfService"

    iget-object v1, p0, Lfyk;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbcq;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
