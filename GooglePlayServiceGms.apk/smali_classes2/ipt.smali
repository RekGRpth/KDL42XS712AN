.class public final Lipt;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lioh;

.field public b:Z

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lipt;->a:Lioh;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lipt;->b:Z

    const/4 v0, 0x1

    iput v0, p0, Lipt;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lipt;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lipt;->a:Lioh;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lipt;->a:Lioh;

    invoke-static {v3, v1}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-boolean v1, p0, Lipt;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lipt;->b:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lipt;->c:I

    if-eq v1, v3, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lipt;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lipt;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lipt;->a:Lioh;

    if-nez v0, :cond_1

    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    iput-object v0, p0, Lipt;->a:Lioh;

    :cond_1
    iget-object v0, p0, Lipt;->a:Lioh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lipt;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lipt;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lipt;->a:Lioh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lipt;->a:Lioh;

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_0
    iget-boolean v0, p0, Lipt;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lipt;->b:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_1
    iget v0, p0, Lipt;->c:I

    if-eq v0, v2, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lipt;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lipt;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lipt;

    iget-object v2, p0, Lipt;->a:Lioh;

    if-nez v2, :cond_3

    iget-object v2, p1, Lipt;->a:Lioh;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lipt;->a:Lioh;

    iget-object v3, p1, Lipt;->a:Lioh;

    invoke-virtual {v2, v3}, Lioh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lipt;->b:Z

    iget-boolean v3, p1, Lipt;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lipt;->c:I

    iget v3, p1, Lipt;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lipt;->a:Lioh;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lipt;->b:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lipt;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lipt;->a:Lioh;

    invoke-virtual {v0}, Lioh;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method
