.class public final Link;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Link;->b:Ljava/lang/String;

    iput-boolean v1, p0, Link;->d:Z

    iput v1, p0, Link;->f:I

    const/4 v0, -0x1

    iput v0, p0, Link;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Link;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Link;->b()I

    :cond_0
    iget v0, p0, Link;->g:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Link;->a:Z

    iput-object v0, p0, Link;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v1, p0, Link;->c:Z

    iput-boolean v0, p0, Link;->d:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v1, p0, Link;->e:Z

    iput v0, p0, Link;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Link;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Link;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Link;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Link;->d:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Link;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Link;->f:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Link;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Link;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Link;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Link;->d:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Link;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Link;->f:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Link;->g:I

    return v0
.end method
