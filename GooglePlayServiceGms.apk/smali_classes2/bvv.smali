.class public abstract Lbvv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbvs;


# instance fields
.field final a:Lbvs;


# direct methods
.method public constructor <init>(Lbvs;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvs;

    iput-object v0, p0, Lbvv;->a:Lbvs;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lbvv;->a:Lbvs;

    invoke-interface {v0}, Lbvs;->a()V

    return-void
.end method

.method public a(Lclm;)V
    .locals 1

    iget-object v0, p0, Lbvv;->a:Lbvs;

    invoke-interface {v0, p1}, Lbvs;->a(Lclm;)V

    return-void
.end method

.method public a(Lclm;Lclk;)V
    .locals 1

    iget-object v0, p0, Lbvv;->a:Lbvs;

    invoke-interface {v0, p1, p2}, Lbvs;->a(Lclm;Lclk;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbvv;->a:Lbvs;

    invoke-interface {v0, p1}, Lbvs;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lclm;)V
    .locals 1

    iget-object v0, p0, Lbvv;->a:Lbvs;

    invoke-interface {v0, p1}, Lbvs;->b(Lclm;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ForwardingFeedProcessor[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbvv;->a:Lbvs;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
