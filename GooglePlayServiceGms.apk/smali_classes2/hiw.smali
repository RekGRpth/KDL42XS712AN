.class final enum Lhiw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhiw;

.field public static final enum b:Lhiw;

.field public static final enum c:Lhiw;

.field private static final synthetic d:[Lhiw;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhiw;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v2}, Lhiw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiw;->a:Lhiw;

    new-instance v0, Lhiw;

    const-string v1, "ON_GPS"

    invoke-direct {v0, v1, v3}, Lhiw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiw;->b:Lhiw;

    new-instance v0, Lhiw;

    const-string v1, "ON_UPLOAD"

    invoke-direct {v0, v1, v4}, Lhiw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiw;->c:Lhiw;

    const/4 v0, 0x3

    new-array v0, v0, [Lhiw;

    sget-object v1, Lhiw;->a:Lhiw;

    aput-object v1, v0, v2

    sget-object v1, Lhiw;->b:Lhiw;

    aput-object v1, v0, v3

    sget-object v1, Lhiw;->c:Lhiw;

    aput-object v1, v0, v4

    sput-object v0, Lhiw;->d:[Lhiw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhiw;
    .locals 1

    const-class v0, Lhiw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhiw;

    return-object v0
.end method

.method public static values()[Lhiw;
    .locals 1

    sget-object v0, Lhiw;->d:[Lhiw;

    invoke-virtual {v0}, [Lhiw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhiw;

    return-object v0
.end method
