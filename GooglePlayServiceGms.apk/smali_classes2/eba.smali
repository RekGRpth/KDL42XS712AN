.class public final Leba;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/view/LayoutInflater;

.field private final i:Lebg;


# direct methods
.method public constructor <init>(Ldvn;Lebg;)V
    .locals 2

    sget v0, Lxb;->j:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ldwx;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Leba;->g:Landroid/content/Context;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leba;->h:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebg;

    iput-object v0, p0, Leba;->i:Lebg;

    return-void
.end method

.method static synthetic a(Leba;)Lebg;
    .locals 1

    iget-object v0, p0, Leba;->i:Lebg;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 6

    const/4 v3, 0x0

    check-cast p4, Lcom/google/android/gms/games/request/GameRequest;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebb;

    iget-object v1, v0, Lebb;->a:Landroid/widget/ImageView;

    sget v2, Lwz;->E:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, v0, Lebb;->i:Leba;

    iget-object v1, v1, Leba;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->j()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, v0, Lebb;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->l:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebb;->e:Landroid/widget/TextView;

    sget v2, Lxf;->bb:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    iget-object v2, v0, Lebb;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v3

    sget v4, Lwz;->f:I

    iget-object v5, v0, Lebb;->i:Leba;

    iget-boolean v5, v5, Ldvj;->d:Z

    if-eqz v5, :cond_0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_1
    iget-object v2, v0, Lebb;->d:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebb;->f:Landroid/widget/TextView;

    iget-object v2, v0, Lebb;->i:Leba;

    iget-object v2, v2, Leba;->g:Landroid/content/Context;

    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->l()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Leee;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebb;->g:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lebb;->h:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :pswitch_1
    iget-object v1, v0, Lebb;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->m:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebb;->e:Landroid/widget/TextView;

    sget v2, Lxf;->bc:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lbgo;)V
    .locals 1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lbhb;

    invoke-static {v0}, Lbkm;->a(Z)V

    :cond_0
    invoke-super {p0, p1}, Ldwx;->a(Lbgo;)V

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Leba;->h:Landroid/view/LayoutInflater;

    sget v1, Lxc;->v:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lebb;

    invoke-direct {v1, p0, v0}, Lebb;-><init>(Leba;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
