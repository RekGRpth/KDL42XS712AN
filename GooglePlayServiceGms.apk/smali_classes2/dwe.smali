.class final Ldwe;
.super Ldwj;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldvv;


# direct methods
.method public constructor <init>(Ldvv;)V
    .locals 3

    iput-object p1, p0, Ldwe;->a:Ldvv;

    const/4 v0, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Ldwj;-><init>(Ldvv;IIZ)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 7

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Ldwe;->a:Ldvv;

    const v2, 0x7f0b020a    # com.google.android.gms.R.string.games_gcore_build_version_title

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Ldwe;->a:Ldvv;

    const v6, 0x7f0b046a    # com.google.android.gms.R.string.common_games_settings_title

    invoke-virtual {v5, v6}, Ldvv;->b(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ldvv;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0082    # com.google.android.gms.R.id.summary

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Ldwe;->a:Ldvv;

    invoke-static {v1}, Ldvv;->c(Ldvv;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
