.class public final enum Liwz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Liwz;

.field public static final enum b:Liwz;

.field public static final enum c:Liwz;

.field public static final enum d:Liwz;

.field public static final enum e:Liwz;

.field private static final synthetic f:[Liwz;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Liwz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Liwz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liwz;->a:Liwz;

    new-instance v0, Liwz;

    const-string v1, "STILL"

    invoke-direct {v0, v1, v3}, Liwz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liwz;->b:Liwz;

    new-instance v0, Liwz;

    const-string v1, "ON_FOOT"

    invoke-direct {v0, v1, v4}, Liwz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liwz;->c:Liwz;

    new-instance v0, Liwz;

    const-string v1, "ON_BICYCLE"

    invoke-direct {v0, v1, v5}, Liwz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liwz;->d:Liwz;

    new-instance v0, Liwz;

    const-string v1, "IN_VEHICLE"

    invoke-direct {v0, v1, v6}, Liwz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liwz;->e:Liwz;

    const/4 v0, 0x5

    new-array v0, v0, [Liwz;

    sget-object v1, Liwz;->a:Liwz;

    aput-object v1, v0, v2

    sget-object v1, Liwz;->b:Liwz;

    aput-object v1, v0, v3

    sget-object v1, Liwz;->c:Liwz;

    aput-object v1, v0, v4

    sget-object v1, Liwz;->d:Liwz;

    aput-object v1, v0, v5

    sget-object v1, Liwz;->e:Liwz;

    aput-object v1, v0, v6

    sput-object v0, Liwz;->f:[Liwz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Liwz;
    .locals 1

    const-class v0, Liwz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Liwz;

    return-object v0
.end method

.method public static values()[Liwz;
    .locals 1

    sget-object v0, Liwz;->f:[Liwz;

    invoke-virtual {v0}, [Liwz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liwz;

    return-object v0
.end method
