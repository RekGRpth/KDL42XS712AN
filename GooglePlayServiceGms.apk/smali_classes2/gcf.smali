.class public final Lgcf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lfsy;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lgcf;->c:I

    iput p3, p0, Lgcf;->d:I

    iput p4, p0, Lgcf;->e:I

    iput-object p5, p0, Lgcf;->f:Ljava/lang/String;

    iput-object p6, p0, Lgcf;->b:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    const-string v0, "ListPeople"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    :try_start_0
    iget-object v1, p0, Lgcf;->f:Ljava/lang/String;

    if-nez v1, :cond_1

    iget v1, p0, Lgcf;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget v1, p0, Lgcf;->e:I

    if-gez v1, :cond_1

    if-eqz v0, :cond_0

    const-string v0, "ListPeople"

    const-string v1, "Running people.list locally"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x0

    iget v3, p0, Lgcf;->d:I

    invoke-static {p1, v0, v1, v2, v3}, Lfey;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZI)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p1, v1}, Lfey;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    sget-object v0, Lfsr;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v5, v0

    cmp-long v0, v3, v5

    if-gez v0, :cond_1

    sget-object v0, Lfnx;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbdd;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v1, v0, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-object v0, p0, Lgcf;->b:Lfsy;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lgcf;->c:I

    iget v3, p0, Lgcf;->d:I

    iget v0, p0, Lgcf;->e:I

    if-gez v0, :cond_2

    const/16 v4, 0x64

    :goto_1
    iget-object v5, p0, Lgcf;->f:Ljava/lang/String;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lfrx;->a(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lgcf;->b:Lfsy;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcf;->b:Lfsy;

    invoke-static {v9, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget v4, p0, Lgcf;->e:I
    :try_end_1
    .catch Lane; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcf;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcf;->b:Lfsy;

    invoke-static {v9, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v7}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcf;->b:Lfsy;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lgcf;->b:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcf;->b:Lfsy;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
