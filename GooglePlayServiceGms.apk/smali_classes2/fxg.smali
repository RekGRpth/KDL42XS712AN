.class public final enum Lfxg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfxg;

.field public static final enum b:Lfxg;

.field public static final enum c:Lfxg;

.field private static final synthetic d:[Lfxg;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lfxg;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lfxg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxg;->a:Lfxg;

    new-instance v0, Lfxg;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v3}, Lfxg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxg;->b:Lfxg;

    new-instance v0, Lfxg;

    const-string v1, "FADED"

    invoke-direct {v0, v1, v4}, Lfxg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxg;->c:Lfxg;

    const/4 v0, 0x3

    new-array v0, v0, [Lfxg;

    sget-object v1, Lfxg;->a:Lfxg;

    aput-object v1, v0, v2

    sget-object v1, Lfxg;->b:Lfxg;

    aput-object v1, v0, v3

    sget-object v1, Lfxg;->c:Lfxg;

    aput-object v1, v0, v4

    sput-object v0, Lfxg;->d:[Lfxg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfxg;
    .locals 1

    const-class v0, Lfxg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfxg;

    return-object v0
.end method

.method public static values()[Lfxg;
    .locals 1

    sget-object v0, Lfxg;->d:[Lfxg;

    invoke-virtual {v0}, [Lfxg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfxg;

    return-object v0
.end method
