.class final Lhmm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0xb

    const-wide v7, 0x3fc3333333333333L    # 0.15

    const/4 v6, 0x5

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fee6351deefe500L    # 0.949624

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd222d5171e29b7L    # 0.283376

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd77e62dc6e2a80L    # 0.367089

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd67f38c5436b90L    # 0.351515

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3beb18116ebd4dL    # 4.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f057eed45e9185dL    # 4.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f759b3d07c84b5eL    # 0.005275

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f1bef49cf56fL    # 0.155815

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0de26916440f24L    # 5.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f13ec460ed80a18L    # 7.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4615ebfa8f7db7L    # 6.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f092a737110e454L    # 4.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd3e032db1e9f27L    # 0.310559

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024f3d102bc72e2L    # 10.476204

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe22af1455219a8L    # 0.567742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f476577531db446L    # 7.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde0a6b93ccd0ffL    # 0.469386

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6567dbb16c1e36L    # 0.002613

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0ffb480a5accd5L    # 6.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd8a55d1c3ac92aL    # 0.385093

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef6052502eec7c9L    # 2.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef1d3671ac14c66L    # 1.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f53a92a30553261L    # 0.0012

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fe02647fdc5931dL    # 0.504673

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5e0828c36da87aL    # 0.001833

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7273d5bab21816L    # 0.004505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff8eeff5c6c11a1L    # 1.558349

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f578705425f2021L    # 0.001436

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f18a43bb40b34e7L    # 9.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f521ab4b72c5198L    # 0.001105

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef3ec460ed80a18L    # 1.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0040bfe3b03e21L    # 3.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe4c1bb8c32a8caL    # 0.648649

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2083dbc23315d7L    # 1.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0cd5f99c38b04bL    # 5.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0040bfe3b03e21L    # 3.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f157eed45e9185dL    # 8.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7247cb70ac3a86L    # 0.004463

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c77ff151e753L    # 9.889648

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe0800000000000L    # 0.515625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f310a137f38c543L    # 2.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffdad1f1cfbb949L    # 1.854766

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44488c60cbf2b2L    # 6.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe050507a6bd6e9L    # 0.509804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40217de76427c7c5L    # 8.745906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9e1d2178f68be3L    # 0.029408

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40f4517614595L    # 0.156716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400184a515ce9e5eL    # 2.189768

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd4892ab68cef67L    # 0.320872

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffb6595bbbe8790L    # 1.712301

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc237ef5a964e8bL    # 0.142332

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1322f2734f82f5L    # 7.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde1a437824d4ccL    # 0.470353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffbaf112fd32c62L    # 1.730241

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff900379314445bL    # 1.562553

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f68201cd5f99c39L    # 0.002945

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40217125bb7b6bb1L    # 8.720991

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fce36113404ea4bL    # 0.236025

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6615ebfa8f7db7L    # 0.002696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff76949a5657fb7L    # 1.463205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3376d549731099L    # 2.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff206f7a0b5ed8dL    # 1.126701

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c92ddbdb5d895L    # 2.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f70b417ca2120e2L    # 0.004078

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7351159c497742L    # 0.004716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f02599ed7c6fbd2L    # 3.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4dd1a21ea35936L    # 9.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f42bb6672fbaL    # 0.155889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f54cec41dd1a21fL    # 0.00127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022dccc03793144L    # 9.431244

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f305186db50f40eL    # 2.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa2d6451b93037dL    # 0.036791

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0403361565c2dL    # 0.507837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402429a1554fbdadL    # 10.081309

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ee2dfd694ccab3fL    # 9.0E-6

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f421ee675147f13L    # 5.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5ac9afe1da7b0bL    # 0.001635

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef81e03f705857bL    # 2.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31904b3c3e74b0L    # 2.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3040bfe3b03e21L    # 2.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7633482be8bc17L    # 0.00542

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f192a737110e454L    # 9.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22dfd694ccab3fL    # 1.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc414b599aa6091L    # 0.156882

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6a52263d816acfL    # 0.003213

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef2dfd694ccab3fL    # 1.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb593e5fb71fbc6L    # 0.084288

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4019be6f71a7e308L    # 6.435972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4693c03bc4d22dL    # 6.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd86bca5375c8daL    # 0.381579

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f51233df2a9d628L    # 0.001046

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f85bccaf709b739L    # 0.010614

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f59da9c99285a92L    # 0.001578

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4026ea8c9b845565L    # 11.458104

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd54434e3369b9dL    # 0.332288

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff85f676ea4228aL    # 1.523292

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3450efdc9c4da9L    # 3.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2669ced0b30b5bL    # 1.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6040bfe3b03e21L    # 0.001984

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc3d1c3ac929aa2L    # 0.154839

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a236bb1290258L    # 13.069181

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2adea897635e74L    # 2.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3700cd855970b5L    # 3.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f41c2a023209678L    # 5.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3b54195cc857f3L    # 4.17E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4008fe62dc6e2a80L    # 3.124212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f589374bc6a7efaL    # 0.0015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a1554fbdad752L    # 3.98E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a4cf11b60ae97L    # 13.150277

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc62fc69728a611L    # 0.173333

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f316ebd4cfd08d5L    # 2.66E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb6a10e02214270L    # 0.088395

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40230873de1e2de8L    # 9.516509

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff466a98244e93eL    # 1.275064

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fda7b95a294141fL    # 0.413793

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1322f2734f82f5L    # 7.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44163779e9d0eaL    # 6.13E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ed3953dea465aL    # 0.003763

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f54cec41dd1a21fL    # 0.00127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b003686a4ca4fL    # 2.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbd512ec6bce853L    # 0.11452

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff82bd6e8af8162L    # 1.510703

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e4712e40852b5L    # 4.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7d6cf850df15a5L    # 0.007184

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f32be48a58b3f64L    # 2.86E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f01322f27350L    # 0.155764

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f58eb8950763a19L    # 0.001521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9916ca46e08f21L    # 0.024501

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ca5bd944aa540L    # 0.003497

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5209edbf8b9baaL    # 0.001101

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401c1bb7b6bb1290L    # 7.027068

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402291fab96f21f7L    # 9.285116

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5b43526527a205L    # 0.001664

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9358f2e05ccc8aL    # 0.018894

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4dc93ea2d2fe3fL    # 9.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6fd7a13c254a3cL    # 0.003887

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f759a30984e3fffL    # 0.005274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4025f97204295a6cL    # 10.987198

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdd28d8665e02ebL    # 0.455618

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402584c447c30d30L    # 10.75931

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdef7ec3547e069L    # 0.483882

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7bd512ec6bce85L    # 0.006795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff907b352a84381L    # 1.56438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024e88db0574b40L    # 10.454206

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a36e2eb1c432dL    # 2.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40cbf2b239a39L    # 0.156639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b64e054690de1L    # 2.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe10da3c21187e8L    # 0.532915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd87e6b3fe9fadbL    # 0.382716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe00ceb356da017L    # 0.501577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4003f3e78e1932d7L    # 2.494094

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd8682f944241c4L    # 0.381359

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f40d7be9856a37bL    # 5.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f78298cc144028eL    # 0.005899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f20624dd2f1a9fcL    # 1.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff63ef49cf56eadL    # 1.39037

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021b7c393682731L    # 8.858914

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc908e581cf7879L    # 0.195584

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f75f56a7ac81d3bL    # 0.005361

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc2f6837f7be122L    # 0.148148

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc879fec99f1ae3L    # 0.191223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5f9f01b866e43bL    # 0.00193

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f65b35b0bbf50e3L    # 0.002649

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcec8e68e3ef284L    # 0.240506

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402099d19157abb9L    # 8.300427

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d94d0dcfcc5b9L    # 0.003611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9182a9930be0dfL    # 0.0171

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f651c5c5718eb89L    # 0.002577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f76a593a2df9379L    # 0.005529

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd4f0307f23cc8eL    # 0.32716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd7a919695d91bL    # 0.230303

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe40dda48b65237L    # 0.626691

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb0e054690de093L    # 0.065923

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd5cf7446f9b995L    # 0.340787

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27fc7607c419a0L    # 1.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc808d08919ef95L    # 0.187769

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2ffb480a5accd5L    # 2.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ffe3c58ab92c062L    # 1.889733

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2733226c3b927dL    # 1.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f12dfd694ccab3fL    # 7.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0cd5f99c38b04bL    # 5.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f94c1ebc83a96d5L    # 0.020271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024dff9f87f023fL    # 10.437454

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f71526d8b1dd5d4L    # 0.004229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f36ce789e774eecL    # 3.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402850240b780347L    # 12.156525

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e7967caea747eL    # 4.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdacccccccccccdL    # 0.41875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fff94b7b28954a8L    # 1.973808

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fe9fadafd114L    # 0.156208

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba779207d4e098L    # 0.103387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f729cbab649d389L    # 0.004544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd9ef459d99029bL    # 0.405229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9018a43bb40b35L    # 0.015719

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f804816f0068db9L    # 0.00795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40023e0b4e11dbcbL    # 2.280295

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402330f217093101L    # 9.595597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f121682f944241cL    # 6.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f6a0dbad3a60L    # 0.155964

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f757abb8800eae2L    # 0.005244

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f359ff4fd6d8L    # 0.155864

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5611ba3ca7503cL    # 0.001347

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fbd129888f861a6L    # 0.113565

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd292fb19e731d3L    # 0.290221

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb704ab606b7aa2L    # 0.089915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f489bd8383ad9f1L    # 7.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa68e60807357e6L    # 0.044055

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0b215fcc1871eL    # 0.521739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8cd4ed2cbea4ecL    # 0.014078

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa985058dde7a74L    # 0.049843

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62641b328b6d87L    # 0.002245

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9713ad5bee3d60L    # 0.022536

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f908dd1e53a81dcL    # 0.016166

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4002d04ab606b7aaL    # 2.351705

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f316e3715400L    # 0.155856

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc3b317efe0ce0cL    # 0.153903

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc403254e6e221dL    # 0.156346

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5fe21d96e9bbf1L    # 0.001946

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbad666a98244e9L    # 0.104834

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe04ccccccccccdL    # 0.509375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fda333333333333L    # 0.409375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40b912dba4d6eL    # 0.156603

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd1af294dd72368L    # 0.276316

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f92ca148ba83f4fL    # 0.018349

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402842ed352220bcL    # 12.130716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f221682f944241cL    # 1.38E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f24f8b588e368f1L    # 1.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffefe74afd54541L    # 1.937123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b6805a2d72ffdL    # 0.006691

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f52dfd694ccab3fL    # 0.001152

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc920fb224aada3L    # 0.196319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9fe36d22424a27L    # 0.031141

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f38c5c9a34ca0c3L    # 3.78E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f355d5f56a7ac82L    # 3.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3450efdc9c4da9L    # 3.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f727807789a4591L    # 0.004509

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f27b95a294141eaL    # 1.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f87985271bcdbbeL    # 0.011521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400b432b12d3415bL    # 3.407797

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f73d31b9b66f933L    # 0.00484

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4101b003686a4dL    # 5.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f56aceaaf35e311L    # 0.001384

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5d68c692f6e829L    # 0.001795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f54940bbb1f255fL    # 0.001256

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4dd1a21ea35936L    # 9.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f760a63305100a4L    # 0.005381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3a47a9e2bcf91aL    # 4.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402222c386d2ed78L    # 9.067898

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9cbc8c0ce91c8fL    # 0.028063

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b10fd7e45803dL    # 0.003304

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc403ab862b27ccL    # 0.156362

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4102ff8ec0f88L    # 0.156744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f60a569b17481b2L    # 0.002032

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40222a66772d5e07L    # 9.082813

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f66787ce95faa8bL    # 0.002743

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc3d1c3ac929aa2L    # 0.154839

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402faebd9018e758L    # 15.84129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb90ff972474539L    # 0.0979

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdad6b65a9a8049L    # 0.419355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffe364cf8d716d3L    # 1.888257

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402152732df505d1L    # 8.661035

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d4b6a619da9caL    # 4.47E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1eeed8904f6dfcL    # 1.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2d19157abb8801L    # 2.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d316b97fe8ee7L    # 14.596524

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76872b020c49baL    # 0.0055

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fe9d375c8d9f905L    # 0.807063

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe1ab302f72b453L    # 0.552147

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8bd4067cf1c326L    # 0.013588

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f32dfd694ccab3fL    # 2.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40308be36d22424aL    # 16.546439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8a437824d4cb9fL    # 0.012824

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc5668c26139000L    # 0.167192

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3fea8112ba16e8L    # 4.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5e364bec679cc7L    # 0.001844

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    iget-wide v0, p1, Lhmx;->f:D

    const-wide/high16 v2, 0x3fd6000000000000L    # 0.34375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4cffeb074a771dL    # 8.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4cef240fa9c12fL    # 8.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8ab5c39bcba301L    # 0.013042

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcaa65cf67b1c00L    # 0.208202

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff3003ff69014b6L    # 1.187561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f385058dde7a744L    # 3.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f312ba16e7a311fL    # 2.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4fea8112ba16e8L    # 9.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa9ce8d972cd7cfL    # 0.050404

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e175d13d74d59L    # 0.014693

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe3a51e321a2e7fL    # 0.613906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40345698cc144029L    # 20.338269

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc642c7fbacb429L    # 0.173913

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0dba908a265f1L    # 0.526814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400a42046c764ae0L    # 3.282235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7b3397dd00f777L    # 0.006641

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde2b55ef1fddecL    # 0.471395

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf9c17225b749bL    # 0.493902

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f296d8f4f93bc0aL    # 1.94E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f65810624dd2f1bL    # 0.002625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f27dae81882adc5L    # 1.82E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe90678c0053e2dL    # 0.78204

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7c8a7a41e57d9eL    # 0.006968

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f60f301eabbcb1dL    # 0.002069

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2882adc4c9c90cL    # 1.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5593e5fb71fbc6L    # 0.001317

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f47a89331a08bfcL    # 7.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024a294dd72367eL    # 10.317542

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd37a7008a697afL    # 0.304348

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4b1feeb2d0a24L    # 0.161682

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f42dfd694ccab3fL    # 5.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4008c4f765fd8adbL    # 3.096175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4085b18548a9cL    # 0.156505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f53cee9dd7ecbb8L    # 0.001209

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f495cc857f3061cL    # 7.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd20d1fa333764fL    # 0.282051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4010345d41fa7653L    # 4.051137

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb502363b256ffcL    # 0.082065

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff40dd93c46d82cL    # 1.253381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023acd6e47dc37aL    # 9.837577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fd4368f08461f9fL    # 0.31583

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff023b9ae0c1765L    # 1.008722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb1289dadfb506eL    # 0.067026

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffced6238da3c21L    # 1.807955

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa5601ffb480a5bL    # 0.041749

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde03254e6e221dL    # 0.468942

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402c17e1da7b0b39L    # 14.046645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd2580c308feac4L    # 0.286624

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5aeb3dd11be6e6L    # 0.001643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faaf922962cfd8fL    # 0.052682

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8dbfceb78897eaL    # 0.014526

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f947a17f4128bf4L    # 0.019997

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4037f17b634dad32L    # 23.943289

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c8eabffcdab19L    # 0.001743

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fdb061c79b34a45L    # 0.422248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4faacd9e83e42L    # 0.040975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f74003254e6e222L    # 0.004883

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002d10f51ac9afeL    # 2.35208

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f74b6a619da9c99L    # 0.005057

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb8f42fe82517e7L    # 0.097476

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f904577d955714cL    # 0.01589

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fce7064ece9a2c6L    # 0.237805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4003e7e62dc6e2a8L    # 2.488232

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9806290eed02cdL    # 0.023461

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402142fdfc19c172L    # 8.630844

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa688df37329c34L    # 0.044013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43f4a98aa8650eL    # 6.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d11be6e653869L    # 0.007097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f40624dd2f1a9fcL    # 5.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc60346dc5d6388L    # 0.171975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fca6d04e618ce2dL    # 0.206452

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc17d9dba908a26L    # 0.136646

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402101a500d5e8d5L    # 8.503212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40337cbf4cb1897aL    # 19.487294

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4035209667b5f1bfL    # 21.127295

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032714003254e6eL    # 18.442383

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe1b1422ccb3a26L    # 0.552888

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff8fe45803cd142L    # 1.562078

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff7324c8366516eL    # 1.44978

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd314ad362e9036L    # 0.298137

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffb653c9abb01c9L    # 1.712216

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6efb6dca07f66fL    # 0.003782

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6633482be8bc17L    # 0.00271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40201a2a2c237479L    # 8.051103

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4030b75bc44bf4cbL    # 16.716244

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcaa65cf67b1c00L    # 0.208202

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f60bc7b45f17bd9L    # 0.002043

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4035d31c432ca57aL    # 21.82465

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd406466b1e5c0cL    # 0.312883

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd1f48c2e770bd0L    # 0.280551

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43c254a3c64346L    # 6.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84690de093532eL    # 0.009966

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f638583621fafc9L    # 0.002383

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f44488c60cbf2b2L    # 6.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fd6e1932d6ece14L    # 0.357518

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f55bdd76683c298L    # 0.001327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4001885122318330L    # 2.191561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8f0563ed0f6274L    # 0.015147

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f866e86c6583e85L    # 0.010953

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd3868b9fdbd2faL    # 0.305087

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb523f67f4dbdf9L    # 0.08258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403abb21f6cacd18L    # 26.730987

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91d81f10667f91L    # 0.017426

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe4e91c8eabffceL    # 0.653456

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401e0643cc07aaefL    # 7.506118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd7e75360d02470L    # 0.373494

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8095af294dd723L    # 0.008098

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020ce312f4cf4a5L    # 8.402719

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403e70ab1d4f9c20L    # 30.440111

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2ba7fc32ebe597L    # 2.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4045f56ea4228998L    # 43.917439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f6cacd184c27L    # 0.155969

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4030d040f2389720L    # 16.813491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4017823f67f4dbe0L    # 5.877195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4030510bc7b45f18L    # 16.316586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9438d1d8a54824L    # 0.019748

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400edb866e43aa7aL    # 3.85719

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd0d7cf5f4e4431L    # 0.263172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd27b5aea3161a2L    # 0.288779

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8102363b256ffcL    # 0.008305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40113b2074ea8da8L    # 4.307741

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f864b662fdfc19cL    # 0.010886

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa8943e10060781L    # 0.048006

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f609289dadfb507L    # 0.002023

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f87a1c25d074214L    # 0.011539

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405c88dfa00e27e1L    # 114.138649

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc6aefb2aae2974L    # 0.177215

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40005c8b86b15f89L    # 2.045188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ffbbf3bea91d9b2L    # 1.734188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f2eb5b2d4d402L    # 7.795615

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f95f633ce63a5c2L    # 0.021447

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb9d798d8a979e1L    # 0.100946

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc0f301eabbcb1dL    # 0.132416

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7cf355cd91eeaaL    # 0.007068

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40306a2b6ae7d567L    # 16.414725

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fffbdb8fde2ef4eL    # 1.983819

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f69b0ab2e1693c0L    # 0.003136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9376922d948dc1L    # 0.019007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc86bc621b7e0acL    # 0.190789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f87fae3608d0892L    # 0.011709

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f92f76e6106ab15L    # 0.018522

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a768709741d08L    # 13.231499

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fae933a0407cc7dL    # 0.059717

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033d62e4d1a6506L    # 19.836644

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4038e28283d35eb7L    # 24.884804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde094e5d5b24eaL    # 0.469318

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f30e8858ff75968L    # 2.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcde81450efdc9cL    # 0.233645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d2ae8e1d6494dL    # 0.007121

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc1851654d61b2aL    # 0.136874

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcb7c1376d54973L    # 0.214724

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc4754f3775b813L    # 0.15983

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd18b8f14db5958L    # 0.274143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403a7327243137b0L    # 26.449816

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0bb0a2ca9ac36L    # 0.065354

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc10acc0bdcad15L    # 0.133142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6a21ea35935fc4L    # 0.00319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3cd5f99c38b04bL    # 4.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdf63779e9d0e99L    # 0.490446

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x401982f33ca31e7eL    # 6.377881

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa70b8cfbfc6541L    # 0.04501

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f71ff7164c729f6L    # 0.004394

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31c2a023209678L    # 2.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40226f29f59ccfafL    # 9.217117

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31f4f50a02b841L    # 2.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4df3300de4c511L    # 9.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f212ba16e7a311fL    # 1.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdc2d16b97fe8eeL    # 0.440252

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4248d7e02645e5L    # 5.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4101b003686a4dL    # 5.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f89f0e4da09ccL    # 7.884708

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40203523f67f4dbeL    # 8.10379

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
