.class public final Lgzq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;

.field public static final d:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "wallet.min_full_wallet_wait_time_millis"

    const-wide/16 v1, 0x1f4

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzq;->a:Lbfy;

    const-string v0, "wallet.full_wallet_pre_fetch_multiplier"

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzq;->b:Lbfy;

    const-string v0, "wallet.full_wallet_pre_fetch_pad"

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzq;->c:Lbfy;

    const-string v0, "wallet.full_wallet_cart_total_price_max"

    const/16 v1, 0x708

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzq;->d:Lbfy;

    return-void
.end method
