.class public abstract Lbyr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbzf;


# instance fields
.field protected final a:Lcfz;

.field protected final b:Lcom/google/android/gms/drive/data/view/DocListView;

.field protected final c:Landroid/widget/ListView;

.field protected final d:Lcom/google/android/gms/drive/data/view/StickyHeaderView;


# direct methods
.method protected constructor <init>(Lcfz;Lcom/google/android/gms/drive/data/view/DocListView;Landroid/widget/ListView;Lcom/google/android/gms/drive/data/view/StickyHeaderView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbyr;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/DocListView;

    iput-object v0, p0, Lbyr;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbyr;->c:Landroid/widget/ListView;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    iput-object v0, p0, Lbyr;->d:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lbyr;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method public final a(Lbzb;)V
    .locals 1

    invoke-virtual {p0}, Lbyr;->d()Lbzp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lbzp;->a(Lbzb;)V

    :cond_0
    return-void
.end method

.method protected final a(Lbzp;)V
    .locals 2

    sget-object v0, Lbth;->c:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbzc;

    iget-object v1, p0, Lbyr;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/data/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lbzc;-><init>(Landroid/content/Context;Lbzp;)V

    iget-object v1, p0, Lbyr;->d:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a(Lcdb;)V

    iget-object v0, p0, Lbyr;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v1, p0, Lbyr;->d:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbyr;->d:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lbyr;->d()Lbzp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lbzp;->a(ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    invoke-virtual {p0}, Lbyr;->d()Lbzp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lbzp;->b()V

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    invoke-virtual {p0}, Lbyr;->d()Lbzp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lbzp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract d()Lbzp;
.end method
