.class public final Lenl;
.super Laji;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lemu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemu;)V
    .locals 0

    invoke-direct {p0}, Laji;-><init>()V

    iput-object p1, p0, Lenl;->a:Landroid/content/Context;

    iput-object p2, p0, Lenl;->b:Lemu;

    return-void
.end method

.method static synthetic a(Lenl;)Z
    .locals 3

    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    const-string v1, "lightweight-appdatasearch"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "usage_reporting_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lenl;)Lemu;
    .locals 1

    iget-object v0, p0, Lenl;->b:Lemu;

    return-object v0
.end method

.method static synthetic c(Lenl;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lajk;)V
    .locals 2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lenn;

    invoke-direct {v0, p0, p1}, Lenn;-><init>(Lenl;Lajk;)V

    iget-object v1, p0, Lenl;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lajk;Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/UsageInfo;)V
    .locals 7

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "packageName empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null usageInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p2}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Lenm;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lenm;-><init>(Lenl;[Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLajk;)V

    iget-object v1, p0, Lenl;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lajk;Z)V
    .locals 2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    new-instance v1, Lenp;

    invoke-direct {v1, p0, p2, p1}, Lenp;-><init>(Lenl;ZLajk;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Lajk;)V
    .locals 2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lenl;->a:Landroid/content/Context;

    new-instance v1, Leno;

    invoke-direct {v1, p0, p1}, Leno;-><init>(Lenl;Lajk;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method
