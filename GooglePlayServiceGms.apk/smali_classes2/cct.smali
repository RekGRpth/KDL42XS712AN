.class public final Lcct;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/data/view/DocListView;

.field private final b:Z

.field private final c:Lcfc;

.field private final d:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/data/view/DocListView;ZLcfc;Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V
    .locals 1

    iput-object p1, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean p2, p0, Lcct;->b:Z

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lcct;->c:Lcfc;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    iput-object v0, p0, Lcct;->d:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    const/4 v5, 0x0

    iget-object v0, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->b(Lcom/google/android/gms/drive/data/view/DocListView;)Lbvo;

    move-result-object v6

    iget-object v2, p0, Lcct;->c:Lcfc;

    iget-object v0, p0, Lcct;->d:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    move-result-object v0

    iget-object v1, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/data/view/DocListView;)Lcfz;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->b(Lcfz;)Lbvi;

    move-result-object v0

    iget-object v1, v6, Lbvo;->b:Lcfz;

    iget-object v3, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    invoke-virtual {v1}, Lcfe;->g()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1}, Lcfe;->f()Ljava/util/Date;

    move-result-object v1

    invoke-static {v3, v1}, Lbvi;->a(Ljava/util/Date;Ljava/util/Date;)Lbvi;

    move-result-object v4

    invoke-virtual {v0, v4}, Lbvi;->a(Lbvi;)Lbvi;

    move-result-object v4

    iget-object v0, v4, Lbvi;->c:Ljava/lang/Object;

    check-cast v0, Lbvh;

    invoke-static {}, Lbvh;->b()Lbvh;

    move-result-object v7

    invoke-static {v0, v7}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v7

    iget-object v0, v4, Lbvi;->d:Ljava/lang/Object;

    check-cast v0, Lbvh;

    invoke-static {}, Lbvh;->c()Lbvh;

    move-result-object v4

    invoke-static {v0, v4}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v0

    invoke-virtual {v6, v2, v7, v3}, Lbvo;->a(Lcfc;Lbvh;Ljava/util/Date;)Lcgb;

    move-result-object v3

    invoke-virtual {v6, v2, v0, v1}, Lbvo;->a(Lcfc;Lbvh;Ljava/util/Date;)Lcgb;

    move-result-object v0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-eqz v3, :cond_0

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_2
    new-instance v3, Landroid/content/SyncResult;

    invoke-direct {v3}, Landroid/content/SyncResult;-><init>()V

    new-instance v0, Lbwh;

    iget-object v1, v6, Lbvo;->a:Lcoy;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lbwh;-><init>(Lcoy;Lcfc;Landroid/content/SyncResult;Ljava/lang/Boolean;Z)V

    new-instance v4, Lbvm;

    iget-object v5, v6, Lbvo;->a:Lcoy;

    move-object v6, v2

    move-object v8, v0

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, Lbvm;-><init>(Lcoy;Lcfc;Ljava/util/List;Lbvs;Landroid/content/SyncResult;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lbvk;

    iget-object v0, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->c(Lcom/google/android/gms/drive/data/view/DocListView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcct;->c:Lcfc;

    iget-object v1, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v1}, Lcom/google/android/gms/drive/data/view/DocListView;->d(Lcom/google/android/gms/drive/data/view/DocListView;)Lcfc;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcct;->d:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v1}, Lcom/google/android/gms/drive/data/view/DocListView;->e(Lcom/google/android/gms/drive/data/view/DocListView;)Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0, p1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/data/view/DocListView;Lbvk;)Lbvk;

    iget-object v0, p0, Lcct;->a:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-boolean v1, p0, Lcct;->b:Z

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/data/view/DocListView;Z)V

    goto :goto_0
.end method
