.class public final Ljdm;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljdj;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljdk;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v1, p0, Ljdm;->a:Ljdj;

    const-string v0, ""

    iput-object v0, p0, Ljdm;->b:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Ljdm;->c:I

    const-string v0, ""

    iput-object v0, p0, Ljdm;->d:Ljava/lang/String;

    iput-object v1, p0, Ljdm;->e:Ljdk;

    const/4 v0, -0x1

    iput v0, p0, Ljdm;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ljdm;->a:Ljdj;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljdm;->a:Ljdj;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljdm;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljdm;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljdm;->c:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljdm;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljdm;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljdm;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljdm;->e:Ljdk;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdm;->e:Ljdk;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Ljdm;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljdm;->a:Ljdj;

    if-nez v0, :cond_1

    new-instance v0, Ljdj;

    invoke-direct {v0}, Ljdj;-><init>()V

    iput-object v0, p0, Ljdm;->a:Ljdj;

    :cond_1
    iget-object v0, p0, Ljdm;->a:Ljdj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdm;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdm;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdm;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljdm;->e:Ljdk;

    if-nez v0, :cond_2

    new-instance v0, Ljdk;

    invoke-direct {v0}, Ljdk;-><init>()V

    iput-object v0, p0, Ljdm;->e:Ljdk;

    :cond_2
    iget-object v0, p0, Ljdm;->e:Ljdk;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Ljdm;->a:Ljdj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljdm;->a:Ljdj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Ljdm;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljdm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljdm;->c:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljdm;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    iget-object v0, p0, Ljdm;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljdm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljdm;->e:Ljdk;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljdm;->e:Ljdk;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljdm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljdm;

    iget-object v2, p0, Ljdm;->a:Ljdj;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljdm;->a:Ljdj;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljdm;->a:Ljdj;

    iget-object v3, p1, Ljdm;->a:Ljdj;

    invoke-virtual {v2, v3}, Ljdj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljdm;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Ljdm;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljdm;->b:Ljava/lang/String;

    iget-object v3, p1, Ljdm;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Ljdm;->c:I

    iget v3, p1, Ljdm;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljdm;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Ljdm;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljdm;->d:Ljava/lang/String;

    iget-object v3, p1, Ljdm;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljdm;->e:Ljdk;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljdm;->e:Ljdk;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljdm;->e:Ljdk;

    iget-object v3, p1, Ljdm;->e:Ljdk;

    invoke-virtual {v2, v3}, Ljdk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljdm;->a:Ljdj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljdm;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ljdm;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljdm;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljdm;->e:Ljdk;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljdm;->a:Ljdj;

    invoke-virtual {v0}, Ljdj;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljdm;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljdm;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ljdm;->e:Ljdk;

    invoke-virtual {v1}, Ljdk;->hashCode()I

    move-result v1

    goto :goto_3
.end method
