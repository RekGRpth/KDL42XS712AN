.class final Lfrf;
.super Lfqe;
.source "SourceFile"


# instance fields
.field final synthetic e:Lfre;


# direct methods
.method public constructor <init>(Lfre;Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v5, 0x0

    iput-object p1, p0, Lfrf;->e:Lfre;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v8}, Lfqe;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lfqe;->a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-static {v1}, Lfre;->a(Lfre;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-virtual {v1}, Lfre;->K()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-virtual {v1}, Lfre;->K()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Lfqe;->a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    invoke-interface {p1}, Lfea;->d()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c()V

    :goto_0
    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-static {v1}, Lfre;->a(Lfre;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-virtual {v1}, Lfre;->K()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setVisibility(I)V

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lfrf;->e:Lfre;

    invoke-virtual {v1}, Lfre;->K()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b(Lbgo;)V
    .locals 4

    new-instance v0, Lbhd;

    invoke-static {}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->q()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lbhd;-><init>(Lbgo;Ljava/util/Comparator;)V

    invoke-super {p0, v0}, Lfqe;->b(Lbgo;)V

    iget-object v0, p0, Lfrf;->e:Lfre;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0, v1}, Lfre;->a(Lfre;Ljava/util/HashSet;)Ljava/util/HashSet;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lbgo;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfea;

    iget-object v2, p0, Lfrf;->e:Lfre;

    invoke-static {v2}, Lfre;->b(Lfre;)Ljava/util/HashSet;

    move-result-object v2

    invoke-interface {v0}, Lfea;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lfea;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfrf;->e:Lfre;

    invoke-static {v0}, Lfre;->c(Lfre;)V

    return-void
.end method
