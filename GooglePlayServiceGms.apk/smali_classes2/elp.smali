.class public final Lelp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/pm/ApplicationInfo;

.field private final b:Landroid/content/Context;

.field private final c:Lorg/xmlpull/v1/XmlPullParser;

.field private final d:Landroid/util/TypedValue;


# direct methods
.method private constructor <init>(Landroid/content/pm/ApplicationInfo;Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lelp;->a:Landroid/content/pm/ApplicationInfo;

    iput-object p2, p0, Lelp;->b:Landroid/content/Context;

    iput-object p3, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Lelp;->d:Landroid/util/TypedValue;

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)D
    .locals 5

    const-string v0, "factor"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Section feature "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " needs parameter factor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v3, v1, v3

    if-lez v3, :cond_1

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v1, v3

    if-lez v3, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Factor not in range: Must be > 0 and <= 1 "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parameter factor="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be a number > 0 and <= 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_2
    return-wide v1
.end method

.method private a(Landroid/util/AttributeSet;I)I
    .locals 3

    const/4 v1, -0x1

    invoke-interface {p1, p2, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    move-result v0

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be a resource reference."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Lelp;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    return v0
.end method

.method private a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 11

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    const-string v2, "0"

    move v4, v5

    move v0, v6

    move-object v1, v7

    move-object v3, v7

    :goto_0
    invoke-interface {v8}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v9

    if-ge v4, v9, :cond_6

    invoke-interface {v8, v4}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "corpusId"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-direct {p0, v8, v4}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const-string v10, "corpusVersion"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-direct {p0, v8, v4}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string v10, "contentProviderUri"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-direct {p0, v8, v4}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    move-object v1, v7

    goto :goto_1

    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_4
    const-string v10, "trimmable"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v8, v4, v0}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v0

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_6
    if-nez v3, :cond_7

    const-string v0, "No corpus ID specified."

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_7
    if-nez v1, :cond_8

    const-string v0, "No content provider URI specified."

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_8
    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->a(Ljava/lang/String;)Laih;

    move-result-object v3

    iput-object v2, v3, Laih;->a:Ljava/lang/String;

    iput-object v1, v3, Laih;->b:Landroid/net/Uri;

    iput-boolean v0, v3, Laih;->e:Z

    new-instance v0, Lelq;

    iget-object v1, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v1}, Lelq;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_2
    invoke-virtual {v0}, Lelq;->a()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lelq;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Section"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0}, Lelp;->b()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v1

    iget-object v2, v3, Laih;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    const-string v2, "GlobalSearchCorpus"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    if-eqz v5, :cond_a

    const-string v0, "Duplicate element GlobalSearchCorpus"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_a
    invoke-direct {p0}, Lelp;->c()Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    move-result-object v1

    iput-object v1, v3, Laih;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    move v5, v6

    goto :goto_2

    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid tag "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside Corpus; expected Section"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " or GlobalSearchCorpus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_c
    invoke-virtual {v3}, Laih;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/pm/PackageManager;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;)Lelp;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    const-string v1, "com.google.android.gms.appdatasearch"

    invoke-virtual {p2, p0, v1}, Landroid/content/pm/ApplicationInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v1, "Failed to read %s meta data from %s; could not create XML parser"

    const-string v2, "com.google.android.gms.appdatasearch"

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lelp;

    invoke-direct {v1, p2, v2, v3}, Lelp;-><init>(Landroid/content/pm/ApplicationInfo;Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get context for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get context for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-nez p2, :cond_0

    :goto_0
    :pswitch_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lelp;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lelp;->d:Landroid/util/TypedValue;

    const/4 v3, 0x1

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    if-nez p3, :cond_1

    iget-object v1, p0, Lelp;->d:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->changingConfigurations:I

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not change between configurations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resource not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lelr;

    iget-object v3, p0, Lelp;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v2, v3, v4, v1, v0}, Lelr;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    :try_start_1
    iget-object v1, p0, Lelp;->d:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->type:I

    packed-switch v1, :pswitch_data_0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not refer to a string resource"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :pswitch_2
    iget-object v0, p0, Lelp;->d:Landroid/util/TypedValue;

    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    invoke-interface {v3}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v4

    if-ge v2, v4, :cond_3

    invoke-interface {v3, v2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "paramName"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3, v2}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "paramValue"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v3, v2}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_3
    if-eqz v1, :cond_4

    if-nez v0, :cond_5

    :cond_4
    const-string v0, "Both name and value must be specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_5
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, -0x1

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v4

    if-nez v4, :cond_0

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    const-string v1, ""

    const/4 v0, 0x0

    move-object v3, v1

    move v1, v2

    :goto_0
    invoke-interface {v4}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-interface {v4, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "featureType"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_2
    new-instance v0, Lelq;

    iget-object v4, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v4}, Lelq;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    :goto_1
    invoke-virtual {v0}, Lelq;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lelq;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "FeatureParam"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-direct {p0, v4}, Lelp;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside SectionFeature; expected FeatureParam"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_4
    if-ne v1, v2, :cond_5

    const-string v0, "No type specified."

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_5
    const/4 v0, 0x1

    if-ne v1, v0, :cond_7

    invoke-virtual {v4}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "Section feature match_global_nicknames does not take set"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_6
    invoke-static {}, Lait;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_7
    if-ne v1, v7, :cond_8

    const-string v0, "demote_common_words"

    invoke-direct {p0, v0, v4}, Lelp;->a(Ljava/lang/String;Landroid/os/Bundle;)D

    move-result-wide v0

    new-instance v2, Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v2, v7}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    const-string v3, "factor"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    :cond_8
    if-ne v1, v8, :cond_9

    const-string v0, "rfc822"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "demote_rfc822_hostnames"

    invoke-direct {p0, v0, v4}, Lelp;->a(Ljava/lang/String;Landroid/os/Bundle;)D

    move-result-wide v0

    new-instance v2, Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v2, v8}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    const-string v3, "factor"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid section feature of type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside section with format "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0
.end method

.method private b()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 11

    const/4 v0, 0x0

    const/4 v7, 0x0

    iget-object v1, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    const-string v4, "plain"

    const/4 v2, 0x1

    move v6, v7

    move v1, v7

    move v3, v7

    move-object v5, v0

    :goto_0
    invoke-interface {v8}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v9

    if-ge v6, v9, :cond_7

    invoke-interface {v8, v6}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "sectionId"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-direct {p0, v8, v6}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v5

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const-string v10, "sectionFormat"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v8, v6, v7}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const-string v0, "Invalid section format"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :pswitch_0
    const-string v4, "plain"

    goto :goto_1

    :pswitch_1
    const-string v4, "html"

    goto :goto_1

    :pswitch_2
    const-string v4, "rfc822"

    goto :goto_1

    :cond_2
    const-string v10, "noIndex"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v8, v6, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v3

    goto :goto_1

    :cond_3
    const-string v10, "sectionWeight"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v8, v6, v2}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v2

    goto :goto_1

    :cond_4
    const-string v10, "indexPrefixes"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v8, v6, v1}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v1

    goto :goto_1

    :cond_5
    const-string v0, "subsectionSeparator"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v8, v6}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_7
    if-nez v5, :cond_8

    const-string v0, "No section ID specified."

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_8
    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(Ljava/lang/String;)Laij;

    move-result-object v5

    iput-object v4, v5, Laij;->a:Ljava/lang/String;

    iput-boolean v3, v5, Laij;->b:Z

    iput v2, v5, Laij;->c:I

    iput-boolean v1, v5, Laij;->d:Z

    iput-object v0, v5, Laij;->e:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Lelq;

    iget-object v2, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v1, v2}, Lelq;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_2
    invoke-virtual {v1}, Lelq;->a()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lelq;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SectionFeature"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0, v4}, Lelp;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v2

    iget v3, v2, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v0, "Duplicate feature defined for section"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_9
    invoke-virtual {v5, v2}, Laij;->a(Lcom/google/android/gms/appdatasearch/Feature;)Laij;

    iget v2, v2, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside Section; expected SectionFeature"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_b
    invoke-virtual {v5}, Laij;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/util/AttributeSet;I)Ljava/lang/String;
    .locals 3

    const/4 v1, -0x1

    invoke-interface {p1, p2, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lelp;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 12

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_0
    const/4 v1, 0x1

    move-object v7, v4

    move-object v6, v4

    move-object v5, v4

    move v3, v0

    move v2, v0

    move v11, v0

    move v0, v1

    move v1, v11

    :goto_0
    invoke-interface {v8}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v9

    if-ge v1, v9, :cond_7

    invoke-interface {v8, v1}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "searchEnabled"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v8, v1, v0}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v10, "searchLabel"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-direct {p0, v8, v1}, Lelp;->a(Landroid/util/AttributeSet;I)I

    move-result v2

    goto :goto_1

    :cond_2
    const-string v10, "settingsDescription"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-direct {p0, v8, v1}, Lelp;->a(Landroid/util/AttributeSet;I)I

    move-result v3

    goto :goto_1

    :cond_3
    const-string v10, "defaultIntentAction"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-direct {p0, v8, v1}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_4
    const-string v10, "defaultIntentData"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-direct {p0, v8, v1}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_5
    const-string v7, "defaultIntentActivity"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v8, v1}, Lelp;->b(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_7
    if-nez v2, :cond_8

    const-string v0, "No label specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_8
    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v1, p0, Lelp;->a:Landroid/content/pm/ApplicationInfo;

    iget v4, v1, Landroid/content/pm/ApplicationInfo;->icon:I

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-object v0

    :cond_9
    move-object v0, v4

    goto :goto_2
.end method

.method private c()Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;
    .locals 12

    const/4 v6, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v4

    const/4 v0, 0x1

    if-eqz v4, :cond_2

    move v2, v1

    move v3, v0

    move v0, v1

    :goto_0
    invoke-interface {v4}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v5

    if-ge v0, v5, :cond_3

    invoke-interface {v4, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "searchEnabled"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4, v0, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v3

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v7, "allowShortcuts"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4, v0, v2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v2

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_2
    move v2, v1

    move v3, v0

    :cond_3
    invoke-static {}, Laia;->a()I

    move-result v0

    new-array v7, v0, [I

    new-instance v8, Lelq;

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v8, v0}, Lelq;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    :goto_2
    invoke-virtual {v8}, Lelq;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {v8}, Lelq;->b()Ljava/lang/String;

    move-result-object v0

    const-string v4, "GlobalSearchSection"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside GlobalSearchCorpus; expected GlobalSearchSection"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_4
    iget-object v0, v8, Lelq;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v9

    if-nez v9, :cond_5

    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_5
    move v0, v1

    move v4, v1

    move v5, v6

    :goto_3
    invoke-interface {v9}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v0, v10, :cond_8

    invoke-interface {v9, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "sectionType"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v9, v0, v5}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v5

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    const-string v4, "sectionContent"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-direct {p0, v9, v0}, Lelp;->a(Landroid/util/AttributeSet;I)I

    move-result v4

    goto :goto_4

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_8
    if-ne v5, v6, :cond_9

    const-string v0, "No sectionId specified"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_9
    if-ltz v5, :cond_a

    invoke-static {}, Laia;->a()I

    move-result v0

    if-le v5, v0, :cond_b

    :cond_a
    const-string v0, "Section ID out of range; badly formed XML?"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_b
    aput v4, v7, v5

    goto/16 :goto_2

    :cond_c
    if-eqz v3, :cond_e

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_d

    invoke-static {}, Lahy;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    new-instance v1, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v1, v7, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    move-object v0, v1

    :goto_5
    return-object v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private d(Ljava/lang/String;)Lelr;
    .locals 3

    new-instance v0, Lelr;

    iget-object v1, p0, Lelp;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v1, v2, p1}, Lelr;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lelo;
    .locals 6

    const/4 v3, 0x2

    const/4 v1, 0x0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v3, :cond_0

    :cond_1
    if-eq v0, v3, :cond_2

    const-string v0, "No start tag found!"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to read search meta data from package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lelp;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AppDataSearch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid root tag "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; expected AppDataSearch"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to read search meta data from package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lelp;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0

    :cond_3
    :try_start_2
    new-instance v3, Lelq;

    iget-object v0, p0, Lelp;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v3, v0}, Lelq;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v1

    :goto_1
    invoke-virtual {v3}, Lelq;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v3}, Lelq;->b()Ljava/lang/String;

    move-result-object v0

    const-string v5, "Corpus"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-direct {p0}, Lelp;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v5, "GlobalSearch"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    if-eqz v2, :cond_5

    const-string v0, "Duplicate element GlobalSearch"

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_5
    invoke-direct {p0, p1}, Lelp;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid tag "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " inside AppDataSearch; expected Corpus"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " or GlobalSearch"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lelp;->d(Ljava/lang/String;)Lelr;

    move-result-object v0

    throw v0

    :cond_7
    new-instance v0, Lelo;

    iget-object v3, p0, Lelp;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v2}, Lelo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
