.class public final Leyv;
.super Leyq;
.source "SourceFile"


# instance fields
.field private final k:F

.field private l:Lezw;

.field private m:Lfaf;

.field private n:[[Leyn;

.field private final o:Leyx;

.field private p:I


# direct methods
.method public constructor <init>(Leyx;)V
    .locals 1

    invoke-direct {p0}, Leyq;-><init>()V

    const v0, 0x409ccccd    # 4.9f

    iput v0, p0, Leyv;->k:F

    iput-object p1, p0, Leyv;->o:Leyx;

    return-void
.end method


# virtual methods
.method protected final a(Leyk;F)V
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Leyv;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leyv;->p:I

    rem-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput v1, p0, Leyv;->p:I

    move v0, v1

    :goto_0
    iget-object v2, p0, Leyv;->n:[[Leyn;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Leyv;->n:[[Leyn;

    aget-object v4, v2, v0

    move v2, v1

    :goto_1
    array-length v3, v4

    if-ge v2, v3, :cond_4

    aget-object v5, v4, v2

    iput-boolean v1, v5, Leyn;->g:Z

    move v3, v1

    :goto_2
    iget-object v6, v5, Leyn;->f:[Leyk;

    array-length v6, v6

    if-ge v3, v6, :cond_2

    iget-object v6, v5, Leyn;->f:[Leyk;

    aget-object v6, v6, v3

    invoke-virtual {v6, p1}, Leyk;->a(Leyk;)F

    move-result v6

    cmpl-float v6, v6, p2

    if-ltz v6, :cond_3

    const/4 v3, 0x1

    iput-boolean v3, v5, Leyn;->g:Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lezw;)V
    .locals 24

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->o:Leyx;

    invoke-virtual {v3}, Leyx;->a()V

    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Leyv;->p:I

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Leyv;->l:Lezw;

    move-object/from16 v0, p1

    iget-object v3, v0, Lezw;->a:Lfaf;

    move-object/from16 v0, p0

    iput-object v3, v0, Leyv;->m:Lfaf;

    move-object/from16 v0, p1

    iget-object v3, v0, Lezw;->i:Lezp;

    iget v3, v3, Lezp;->n:F

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->i:Lezp;

    iget v4, v4, Lezp;->o:F

    move-object/from16 v0, p1

    iget-object v5, v0, Lezw;->i:Lezp;

    iget v5, v5, Lezp;->p:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Leyv;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->m:Lfaf;

    if-nez v3, :cond_2

    const-string v3, "PartialSphere"

    const-string v4, "tile provider is null. Cannot load textures"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lezw;->i:Lezp;

    iget v3, v3, Lezp;->a:I

    int-to-float v3, v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->i:Lezp;

    iget v4, v4, Lezp;->h:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->a:Lfaf;

    invoke-interface {v4}, Lfaf;->f()F

    move-result v4

    mul-float/2addr v3, v4

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->a:Lfaf;

    invoke-interface {v4}, Lfaf;->a()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lezw;->i:Lezp;

    iget v5, v5, Lezp;->j:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v4, v4

    div-float/2addr v4, v3

    move-object/from16 v0, p1

    iput v4, v0, Lezw;->d:F

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->a:Lfaf;

    invoke-interface {v4}, Lfaf;->d()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lezw;->i:Lezp;

    iget v5, v5, Lezp;->j:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v4, v4

    div-float/2addr v4, v3

    move-object/from16 v0, p1

    iput v4, v0, Lezw;->g:F

    move-object/from16 v0, p1

    iget-object v4, v0, Lezw;->a:Lfaf;

    invoke-interface {v4}, Lfaf;->e()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lezw;->i:Lezp;

    iget v5, v5, Lezp;->k:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    double-to-float v4, v4

    div-float v3, v4, v3

    move-object/from16 v0, p1

    iput v3, v0, Lezw;->h:F

    move-object/from16 v0, p0

    iget v8, v0, Leyv;->k:F

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->d:F

    const v4, 0x3df5c28f    # 0.12f

    div-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v10, v3

    const-string v3, "PartialSphere"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tesselation factor: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->m:Lfaf;

    invoke-interface {v3}, Lfaf;->c()I

    move-result v3

    mul-int/2addr v3, v10

    add-int/lit8 v9, v3, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->m:Lfaf;

    invoke-interface {v3}, Lfaf;->b()I

    move-result v3

    mul-int/2addr v3, v10

    add-int/lit8 v11, v3, 0x1

    mul-int v3, v9, v11

    add-int/lit8 v4, v9, -0x1

    add-int/lit8 v5, v11, -0x1

    mul-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Leyv;->a(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->d:F

    int-to-float v4, v10

    div-float v12, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->d:F

    int-to-float v4, v10

    div-float v13, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->h:F

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->l:Lezw;

    iget v4, v4, Lezw;->d:F

    div-float/2addr v3, v4

    mul-float v14, v12, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->g:F

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->l:Lezw;

    iget v4, v4, Lezw;->d:F

    div-float/2addr v3, v4

    mul-float v15, v13, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->e:F

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->l:Lezw;

    iget v4, v4, Lezw;->c:F

    add-float/2addr v3, v4

    const v4, 0x3fc90fdb

    sub-float v16, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->l:Lezw;

    iget v3, v3, Lezw;->f:F

    neg-float v3, v3

    const v4, 0x40490fdb    # (float)Math.PI

    sub-float v17, v3, v4

    sub-int v3, v11, v10

    add-int/lit8 v18, v3, -0x1

    filled-new-array {v11, v9}, [I

    move-result-object v3

    const-class v4, Lezc;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[Lezc;

    const/4 v4, 0x0

    move v7, v4

    :goto_0
    if-ge v7, v9, :cond_7

    if-ge v7, v10, :cond_5

    int-to-float v4, v7

    mul-float/2addr v4, v14

    sub-float v4, v4, v16

    :goto_1
    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v11, :cond_6

    int-to-float v5, v6

    mul-float/2addr v5, v13

    move/from16 v0, v18

    if-le v6, v0, :cond_1

    move/from16 v0, v18

    int-to-float v5, v0

    mul-float/2addr v5, v13

    sub-int v19, v6, v18

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v15

    add-float v5, v5, v19

    :cond_1
    const v19, 0x3fc90fdb

    sub-float v5, v5, v19

    sub-float v5, v5, v17

    float-to-double v0, v4

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->sin(D)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-float v0, v0

    move/from16 v19, v0

    float-to-double v0, v5

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    float-to-double v0, v4

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->cos(D)D

    move-result-wide v21

    move-wide/from16 v0, v21

    double-to-float v0, v0

    move/from16 v21, v0

    float-to-double v0, v5

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v5, v0

    mul-float v5, v5, v21

    mul-float/2addr v5, v8

    mul-float v19, v19, v8

    mul-float v20, v20, v21

    mul-float v20, v20, v8

    aget-object v21, v3, v6

    new-instance v22, Lezc;

    move-object/from16 v0, v22

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v5, v1, v2}, Lezc;-><init>(FFF)V

    aput-object v22, v21, v7

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->d:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Leza;

    invoke-interface {v3}, Leza;->c()V

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->d:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    const/4 v8, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->m:Lfaf;

    invoke-interface {v3}, Lfaf;->b()I

    move-result v3

    if-ge v8, v3, :cond_0

    const/4 v9, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Leyv;->m:Lfaf;

    invoke-interface {v3}, Lfaf;->c()I

    move-result v3

    if-ge v9, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Leyv;->d:Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->o:Leyx;

    new-instance v3, Leyz;

    iget-object v5, v4, Leyx;->a:Ljava/util/concurrent/Semaphore;

    iget-object v6, v4, Leyx;->e:Lezl;

    iget-object v7, v4, Leyx;->c:Leym;

    invoke-direct/range {v3 .. v9}, Leyz;-><init>(Leyx;Ljava/util/concurrent/Semaphore;Lezl;Leym;II)V

    iget-object v4, v4, Leyx;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v10, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_5
    int-to-float v4, v7

    mul-float/2addr v4, v12

    sub-float v4, v4, v16

    move-object/from16 v0, p0

    iget-object v5, v0, Leyv;->l:Lezw;

    iget v5, v5, Lezw;->d:F

    move-object/from16 v0, p0

    iget-object v6, v0, Leyv;->l:Lezw;

    iget v6, v6, Lezw;->h:F

    sub-float/2addr v5, v6

    sub-float/2addr v4, v5

    goto/16 :goto_1

    :cond_6
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->m:Lfaf;

    invoke-interface {v4}, Lfaf;->b()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->m:Lfaf;

    invoke-interface {v4}, Lfaf;->c()I

    move-result v12

    filled-new-array {v11, v12}, [I

    move-result-object v4

    const-class v5, Leyn;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Leyn;

    move-object/from16 v0, p0

    iput-object v4, v0, Leyv;->n:[[Leyn;

    const/4 v5, 0x0

    const/4 v4, 0x0

    move v8, v4

    move v9, v5

    :goto_6
    if-ge v8, v12, :cond_b

    const/4 v5, 0x0

    const/4 v4, 0x0

    move v6, v4

    move v7, v5

    :goto_7
    if-ge v6, v11, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->n:[[Leyn;

    aget-object v4, v4, v6

    new-instance v5, Leyn;

    mul-int v13, v6, v12

    add-int/2addr v13, v8

    invoke-direct {v5, v13, v10}, Leyn;-><init>(II)V

    aput-object v5, v4, v8

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    move v5, v4

    :goto_8
    add-int/lit8 v4, v10, 0x1

    if-ge v5, v4, :cond_9

    const/4 v4, 0x0

    :goto_9
    add-int/lit8 v14, v10, 0x1

    if-ge v4, v14, :cond_8

    add-int v14, v7, v4

    aget-object v14, v3, v14

    add-int v15, v9, v5

    aget-object v14, v14, v15

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_8

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Leyv;->n:[[Leyn;

    aget-object v4, v4, v6

    aget-object v5, v4, v8

    const/4 v4, 0x0

    new-array v4, v4, [Lezc;

    invoke-interface {v13, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lezc;

    invoke-virtual {v5, v4}, Leyn;->a([Lezc;)V

    add-int v5, v7, v10

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v7, v5

    goto :goto_7

    :cond_a
    add-int v5, v9, v10

    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v9, v5

    goto :goto_6

    :cond_b
    return-void
.end method

.method public final a([F)V
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Leyv;->h:Leyw;

    invoke-virtual {v0}, Leyw;->a()V

    const/4 v0, 0x1

    move v1, v2

    :goto_0
    iget-object v3, p0, Leyv;->n:[[Leyn;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Leyv;->n:[[Leyn;

    aget-object v5, v3, v1

    move v4, v2

    move v3, v0

    :goto_1
    array-length v0, v5

    if-ge v4, v0, :cond_2

    aget-object v6, v5, v4

    iget-object v0, p0, Leyv;->d:Ljava/util/Vector;

    iget v7, v6, Leyn;->a:I

    invoke-virtual {v0, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leza;

    if-eqz v3, :cond_0

    invoke-interface {v0}, Leza;->d()Z

    move-result v7

    if-eqz v7, :cond_0

    move v3, v2

    :cond_0
    iget-boolean v7, v6, Leyn;->g:Z

    if-eqz v7, :cond_1

    invoke-interface {v0}, Leza;->a()Leyr;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v8, p0, Leyv;->h:Leyw;

    invoke-virtual {v7}, Leyr;->a()V

    iget-object v7, p0, Leyv;->h:Leyw;

    invoke-virtual {v7, p1}, Leyw;->a([F)V

    invoke-interface {v0}, Leza;->e()F

    move-result v7

    iget-object v0, p0, Leyv;->h:Leyw;

    check-cast v0, Lezk;

    invoke-virtual {v0, v7}, Lezk;->a(F)V

    iget-object v0, p0, Leyv;->h:Leyw;

    iget-object v7, v6, Leyn;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {v7, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v7, v6, Leyn;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v7}, Leyw;->a(Ljava/nio/FloatBuffer;)V

    iget-object v7, v6, Leyn;->d:Ljava/nio/FloatBuffer;

    invoke-virtual {v7, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v7, v6, Leyn;->d:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v7}, Leyw;->b(Ljava/nio/FloatBuffer;)V

    iget-object v0, v6, Leyn;->e:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v0, 0x4

    iget v7, v6, Leyn;->b:I

    const/16 v8, 0x1403

    iget-object v6, v6, Leyn;->e:Ljava/nio/ShortBuffer;

    invoke-static {v0, v7, v8, v6}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_0

    :cond_3
    iget-object v0, p0, Leyv;->h:Leyw;

    check-cast v0, Lezk;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lezk;->a(F)V

    return-void
.end method
