.class public final Lead;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Landroid/view/View;

.field final d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final h:Landroid/widget/TextView;

.field final i:Landroid/database/CharArrayBuffer;

.field final j:Landroid/widget/TextView;

.field final k:Landroid/widget/TextView;

.field final l:Landroid/widget/TextView;

.field final m:Ljava/util/ArrayList;

.field final n:Landroid/view/View;

.field final o:Landroid/widget/TextView;

.field final p:Landroid/view/View;

.field final q:Landroid/view/View;

.field final synthetic r:Leaa;


# direct methods
.method public constructor <init>(Leaa;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lead;->r:Leaa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lead;->m:Ljava/util/ArrayList;

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->A:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lead;->c:Landroid/view/View;

    sget v0, Lxa;->B:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->C:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->e:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->y:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->z:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lead;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lead;->h:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lead;->i:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->o:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lead;->j:Landroid/widget/TextView;

    sget v0, Lxa;->G:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lead;->k:Landroid/widget/TextView;

    sget v0, Lxa;->ao:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lead;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lead;->m:Ljava/util/ArrayList;

    sget v0, Lxa;->av:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lead;->m:Ljava/util/ArrayList;

    sget v0, Lxa;->aw:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lead;->m:Ljava/util/ArrayList;

    sget v0, Lxa;->ax:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lead;->m:Ljava/util/ArrayList;

    sget v0, Lxa;->ay:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v0, Lxa;->u:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lead;->n:Landroid/view/View;

    sget v0, Lxa;->a:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lead;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lead;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->n:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lead;->p:Landroid/view/View;

    iget-object v0, p0, Lead;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lead;->q:Landroid/view/View;

    iget-object v0, p0, Lead;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-virtual {v0}, Leaa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 13

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Leee;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->b(Leaa;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldgd;->a(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v9

    add-int v10, v8, v9

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v9, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->f:I

    invoke-virtual {p0, v0, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->a(Leaa;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lead;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lead;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lead;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lead;->i:Landroid/database/CharArrayBuffer;

    invoke-interface {v4, v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v0, p0, Lead;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lead;->i:Landroid/database/CharArrayBuffer;

    iget-object v1, v1, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v2, 0x0

    iget-object v4, p0, Lead;->i:Landroid/database/CharArrayBuffer;

    iget v4, v4, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/TextView;->setText([CII)V

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->a(Leaa;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->c(Leaa;)Ldvn;

    move-result-object v0

    sget v1, Lxf;->bj:I

    invoke-virtual {v0, v1}, Ldvn;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v1, p0, Lead;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->c(Leaa;)Ldvn;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->h()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Leee;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lead;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->c(Leaa;)Ldvn;

    move-result-object v0

    sget v1, Lxf;->bl:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ldvn;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lead;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lead;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_4
    if-ge v2, v4, :cond_6

    iget-object v0, p0, Lead;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    add-int/lit8 v5, v2, 0x2

    if-le v10, v5, :cond_5

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    sget v5, Lwz;->f:I

    invoke-virtual {p0, v0, v1, v5}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTag(Ljava/lang/Object;)V

    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_4

    :cond_3
    iget-object v0, p0, Lead;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v0, p0, Lead;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->e:I

    invoke-virtual {p0, v0, v1, v2}, Lead;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    goto/16 :goto_2

    :cond_4
    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->c(Leaa;)Ldvn;

    move-result-object v0

    sget v1, Lxf;->bi:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ldvn;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_5
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    move-object v0, v1

    goto :goto_5

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lead;->r:Leaa;

    invoke-static {v5}, Leaa;->d(Leaa;)I

    move-result v5

    iget v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_7
    const/4 v0, 0x6

    if-le v10, v0, :cond_8

    iget-object v0, p0, Lead;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lead;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lead;->n:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_6
    iget-object v0, p0, Lead;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    sget v0, Lxf;->bg:I

    :goto_7
    iget-object v1, p0, Lead;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lead;->p:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lead;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_8
    iget-object v0, p0, Lead;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :cond_9
    sget v0, Lxf;->bk:I

    goto :goto_7
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v3, v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    if-eqz v3, :cond_4

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxa;->ah:I

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->f(Leaa;)Leac;

    move-result-object v2

    invoke-interface {v2, v0}, Leac;->b(Lcom/google/android/gms/games/Game;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget v4, Lxa;->ad:I

    if-ne v3, v4, :cond_2

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->g(Leaa;)Ldzz;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldzz;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-virtual {v2}, Leaa;->notifyDataSetChanged()V

    instance-of v2, v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->f(Leaa;)Leac;

    move-result-object v2

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-interface {v2, v0}, Leac;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->f(Leaa;)Leac;

    move-result-object v2

    invoke-interface {v2, v0}, Leac;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_1

    :cond_2
    sget v4, Lxa;->ai:I

    if-ne v3, v4, :cond_3

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->f(Leaa;)Leac;

    move-result-object v2

    iget-object v3, p0, Lead;->r:Leaa;

    invoke-static {v3}, Leaa;->e(Leaa;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lead;->r:Leaa;

    invoke-static {v4}, Leaa;->b(Leaa;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v3, v4}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lxa;->a:I

    if-ne v2, v3, :cond_3

    instance-of v1, v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->f(Leaa;)Leac;

    move-result-object v1

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->e(Leaa;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lead;->r:Leaa;

    invoke-static {v3}, Leaa;->b(Leaa;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->f(Leaa;)Leac;

    move-result-object v1

    invoke-interface {v1, v0}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lead;->r:Leaa;

    invoke-static {v0}, Leaa;->f(Leaa;)Leac;

    move-result-object v0

    invoke-interface {v0, v1}, Leac;->a_(Lcom/google/android/gms/games/Game;)V

    goto :goto_0

    :cond_3
    sget v3, Lxa;->n:I

    if-ne v2, v3, :cond_5

    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->g(Leaa;)Ldzz;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldzz;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    iget-object v1, p0, Lead;->r:Leaa;

    invoke-virtual {v1}, Leaa;->notifyDataSetChanged()V

    instance-of v1, v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->f(Leaa;)Leac;

    move-result-object v1

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-interface {v1, v0}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->f(Leaa;)Leac;

    move-result-object v1

    invoke-interface {v1, v0}, Leac;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    :cond_5
    sget v3, Lxa;->ap:I

    if-ne v2, v3, :cond_7

    new-instance v1, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    instance-of v0, v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v0, :cond_6

    sget v0, Lxd;->a:I

    invoke-virtual {v1, v0}, Lov;->a(I)V

    :goto_1
    new-instance v0, Ledk;

    invoke-direct {v0, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v0, v1, Lov;->c:Lox;

    iget-object v0, v1, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto :goto_0

    :cond_6
    sget v0, Lxd;->b:I

    invoke-virtual {v1, v0}, Lov;->a(I)V

    goto :goto_1

    :cond_7
    sget v3, Lxa;->aB:I

    if-eq v2, v3, :cond_8

    sget v3, Lxa;->av:I

    if-eq v2, v3, :cond_8

    sget v3, Lxa;->aw:I

    if-eq v2, v3, :cond_8

    sget v3, Lxa;->ax:I

    if-eq v2, v3, :cond_8

    sget v3, Lxa;->ay:I

    if-eq v2, v3, :cond_8

    sget v3, Lxa;->u:I

    if-ne v2, v3, :cond_9

    :cond_8
    iget-object v1, p0, Lead;->r:Leaa;

    invoke-static {v1}, Leaa;->f(Leaa;)Leac;

    move-result-object v1

    iget-object v2, p0, Lead;->r:Leaa;

    invoke-static {v2}, Leaa;->e(Leaa;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lead;->r:Leaa;

    invoke-static {v3}, Leaa;->b(Leaa;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "InvitationListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
