.class public enum Lbzk;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbzk;

.field public static final enum b:Lbzk;

.field public static final enum c:Lbzk;

.field public static final enum d:Lbzk;

.field public static final enum e:Lbzk;

.field public static final enum f:Lbzk;

.field private static final synthetic i:[Lbzk;


# instance fields
.field private final g:Lbvi;

.field private final h:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    new-instance v0, Lbzk;

    const-string v1, "NONE"

    sget-object v3, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Lbvi;->a:Lbvi;

    invoke-direct/range {v0 .. v5}, Lbzk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/SqlWhereClause;ZLbvi;)V

    sput-object v0, Lbzk;->a:Lbzk;

    new-instance v5, Lbzk;

    const-string v6, "STARRED"

    sget-object v0, Lceg;->n:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, v11, v12}, Lcdp;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v8

    invoke-static {}, Lbvi;->c()Lbvi;

    move-result-object v10

    move v7, v4

    move v9, v4

    invoke-direct/range {v5 .. v10}, Lbzk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/SqlWhereClause;ZLbvi;)V

    sput-object v5, Lbzk;->b:Lbzk;

    new-instance v0, Lbzl;

    const-string v1, "PINNED"

    sget-object v3, Lceg;->p:Lceg;

    invoke-virtual {v3}, Lceg;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3, v11, v12}, Lcdp;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    sget-object v5, Lbvi;->b:Lbvi;

    invoke-direct {v0, v1, v3, v5}, Lbzl;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lbvi;)V

    sput-object v0, Lbzk;->c:Lbzk;

    new-instance v0, Lbzm;

    const-string v1, "UPLOADS"

    sget-object v3, Lcez;->e:Lcez;

    invoke-virtual {v3}, Lcez;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3, v11, v12}, Lcdp;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    sget-object v5, Lbvi;->b:Lbvi;

    invoke-direct {v0, v1, v3, v5}, Lbzm;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lbvi;)V

    sput-object v0, Lbzk;->d:Lbzk;

    new-instance v0, Lbzn;

    const-string v1, "SHARED_WITH_ME"

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lceg;->e:Lceg;

    invoke-virtual {v6}, Lceg;->a()Lcdp;

    move-result-object v6

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " IS NOT NULL"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbvi;->e()Lbvi;

    move-result-object v5

    invoke-direct {v0, v1, v3, v5}, Lbzn;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lbvi;)V

    sput-object v0, Lbzk;->e:Lbzk;

    new-instance v0, Lbzo;

    const-string v1, "MY_DRIVE"

    const-string v3, "root"

    invoke-static {v3}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-static {v2, v3}, Lcdk;->a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    invoke-static {}, Lbvi;->d()Lbvi;

    move-result-object v5

    invoke-direct {v0, v1, v3, v5}, Lbzo;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lbvi;)V

    sput-object v0, Lbzk;->f:Lbzk;

    const/4 v0, 0x6

    new-array v0, v0, [Lbzk;

    sget-object v1, Lbzk;->a:Lbzk;

    aput-object v1, v0, v2

    sget-object v1, Lbzk;->b:Lbzk;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, Lbzk;->c:Lbzk;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbzk;->d:Lbzk;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbzk;->e:Lbzk;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbzk;->f:Lbzk;

    aput-object v2, v0, v1

    sput-object v0, Lbzk;->i:[Lbzk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/SqlWhereClause;ZLbvi;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    if-eqz p4, :cond_0

    sget-object v0, Lceg;->o:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v1, Lcev;->a:Lcev;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, p3, v2}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lbzk;->h:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvi;

    iput-object v0, p0, Lbzk;->g:Lbvi;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/SqlWhereClause;ZLbvi;B)V
    .locals 6

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbzk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/SqlWhereClause;ZLbvi;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbzk;
    .locals 1

    const-class v0, Lbzk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbzk;

    return-object v0
.end method

.method public static values()[Lbzk;
    .locals 1

    sget-object v0, Lbzk;->i:[Lbzk;

    invoke-virtual {v0}, [Lbzk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbzk;

    return-object v0
.end method


# virtual methods
.method public final a()Lbvi;
    .locals 1

    iget-object v0, p0, Lbzk;->g:Lbvi;

    return-object v0
.end method

.method public b()Lcat;
    .locals 1

    sget-object v0, Lcat;->b:Lcat;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lbzk;->h:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method
