.class public final Lfac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field b:Landroid/graphics/PointF;

.field c:Landroid/graphics/PointF;

.field private d:Lfad;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfac;->a:J

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lfac;->b:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lfac;->c:Landroid/graphics/PointF;

    const/4 v0, 0x0

    iput-object v0, p0, Lfac;->d:Lfad;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lfac;->d:Lfad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/PointF;)Z
    .locals 7

    const/4 v0, 0x0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lfac;->d:Lfad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iget-object v3, p0, Lfac;->d:Lfad;

    iget-wide v3, v3, Lfad;->a:J

    sub-long/2addr v1, v3

    long-to-double v1, v1

    const-wide v3, 0x41bdcd6500000000L    # 5.0E8

    div-double/2addr v1, v3

    sub-double v1, v5, v1

    const-wide/high16 v3, 0x4008000000000000L    # 3.0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    sub-double v1, v5, v1

    cmpl-double v3, v1, v5

    if-ltz v3, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lfac;->d:Lfad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lfac;->d:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-double v3, v0

    iget-object v0, p0, Lfac;->d:Lfad;

    iget-object v0, v0, Lfad;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-double v5, v0

    mul-double/2addr v5, v1

    add-double/2addr v3, v5

    double-to-float v0, v3

    iget-object v3, p0, Lfac;->d:Lfad;

    iget-object v3, v3, Lfad;->b:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-double v3, v3

    iget-object v5, p0, Lfac;->d:Lfad;

    iget-object v5, v5, Lfad;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-double v5, v5

    mul-double/2addr v1, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v0, p1, Landroid/graphics/PointF;->x:F

    iput v1, p1, Landroid/graphics/PointF;->y:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final declared-synchronized b(Landroid/graphics/PointF;)Z
    .locals 5

    const/high16 v4, 0x3e000000    # 0.125f

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfac;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    iget-object v2, p0, Lfac;->c:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lfac;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v4

    iget-object v2, p0, Lfac;->c:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v4

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v1, Lfad;

    invoke-direct {v1, p1, v0}, Lfad;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    iput-object v1, p0, Lfac;->d:Lfad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
