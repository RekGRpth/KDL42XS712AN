.class public final Lebf;
.super Ldwz;
.source "SourceFile"


# instance fields
.field g:Ljava/lang/String;

.field private final h:Landroid/content/Context;

.field private final i:Landroid/view/LayoutInflater;

.field private final j:Lebh;

.field private k:Leax;

.field private l:Lbgo;


# direct methods
.method public constructor <init>(Ldvn;Lebh;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lebf;-><init>(Ldvn;Lebh;I)V

    return-void
.end method

.method public constructor <init>(Ldvn;Lebh;I)V
    .locals 1

    sget v0, Lxb;->j:I

    invoke-direct {p0, p1, v0, p3}, Ldwz;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Lebf;->h:Landroid/content/Context;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lebf;->i:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebh;

    iput-object v0, p0, Lebf;->j:Lebh;

    return-void
.end method

.method static synthetic a(Lebf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebf;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lebf;)Lebh;
    .locals 1

    iget-object v0, p0, Lebf;->j:Lebh;

    return-object v0
.end method

.method static synthetic c(Lebf;)Leax;
    .locals 1

    iget-object v0, p0, Lebf;->k:Leax;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 5

    const/4 v3, 0x0

    check-cast p4, Lcom/google/android/gms/games/request/GameRequest;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebi;

    instance-of v1, p4, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    if-eqz v1, :cond_0

    check-cast p4, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iget-object v1, v0, Lebi;->a:Landroid/widget/ImageView;

    sget v2, Lwz;->F:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p4}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->j()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, v0, Lebi;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->o:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebi;->e:Landroid/widget/TextView;

    sget v2, Lxf;->aZ:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    invoke-virtual {p4}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    iget-object v2, v0, Lebi;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v3

    sget v4, Lwz;->f:I

    invoke-virtual {v0, v2, v3, v4}, Lebi;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v2, v0, Lebi;->d:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebi;->f:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebi;->g:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lebi;->h:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    return-void

    :pswitch_1
    iget-object v1, v0, Lebi;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->n:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebi;->e:Landroid/widget/TextView;

    sget v2, Lxf;->ba:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lebi;->a:Landroid/widget/ImageView;

    sget v2, Lwz;->E:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, v0, Lebi;->i:Lebf;

    iget-object v1, v1, Lebf;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->j()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    iget-object v1, v0, Lebi;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->l:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebi;->e:Landroid/widget/TextView;

    sget v2, Lxf;->bb:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    iget-object v2, v0, Lebi;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v3

    sget v4, Lwz;->f:I

    invoke-virtual {v0, v2, v3, v4}, Lebi;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v2, v0, Lebi;->d:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebi;->f:Landroid/widget/TextView;

    iget-object v2, v0, Lebi;->i:Lebf;

    iget-object v2, v2, Lebf;->h:Landroid/content/Context;

    invoke-interface {p4}, Lcom/google/android/gms/games/request/GameRequest;->l()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Leee;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebi;->g:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lebi;->h:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, v0, Lebi;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->m:I

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Lebi;->e:Landroid/widget/TextView;

    sget v2, Lxf;->bc:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V
    .locals 7

    if-eqz p2, :cond_1

    iget-object v0, p0, Lebf;->k:Leax;

    invoke-virtual {v0, p1}, Leax;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    :cond_0
    invoke-virtual {p0}, Lebf;->notifyDataSetChanged()V

    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lebf;->l:Lbgo;

    invoke-virtual {v1}, Lbgo;->a()I

    move-result v4

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lebf;->l:Lbgo;

    invoke-virtual {v0, v3}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_1
    if-lez v2, :cond_3

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->a(Ljava/util/ArrayList;)V

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0
.end method

.method public final a(Leax;)V
    .locals 3

    iput-object p1, p0, Lebf;->k:Leax;

    iget-object v0, p1, Leax;->b:Lbhb;

    iput-object v0, p0, Lebf;->l:Lbgo;

    const/4 v0, 0x2

    new-array v0, v0, [Lbgo;

    const/4 v1, 0x0

    iget-object v2, p1, Leax;->a:Lbhb;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lebf;->l:Lbgo;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lebf;->a([Lbgo;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lebf;->g:Ljava/lang/String;

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lebf;->i:Landroid/view/LayoutInflater;

    sget v1, Lxc;->v:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lebi;

    invoke-direct {v1, p0, v0}, Lebi;-><init>(Lebf;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
