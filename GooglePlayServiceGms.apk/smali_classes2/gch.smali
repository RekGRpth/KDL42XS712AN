.class public final Lgch;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lbhw;

.field private final b:Landroid/net/Uri;

.field private final c:I

.field private final d:Lfsy;


# direct methods
.method public constructor <init>(Lbhw;Landroid/net/Uri;ILfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgch;->a:Lbhw;

    iput-object p2, p0, Lgch;->b:Landroid/net/Uri;

    iput p3, p0, Lgch;->c:I

    iput-object p4, p0, Lgch;->d:Lfsy;

    return-void
.end method

.method private a(ILandroid/os/ParcelFileDescriptor;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lgch;->d:Lfsy;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "LoadImageOperation"

    const-string v2, "Failed close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz p2, :cond_1

    :try_start_2
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v1

    const-string v2, "LoadImageOperation"

    const-string v3, "Failed close"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 3

    iget-object v0, p0, Lgch;->b:Landroid/net/Uri;

    iget v1, p0, Lgch;->c:I

    if-eqz v1, :cond_0

    iget-object v0, p0, Lgch;->b:Landroid/net/Uri;

    iget v1, p0, Lgch;->c:I

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "bounding_box"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lgch;->a:Lbhw;

    invoke-virtual {v1, p1, v0}, Lbhw;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgch;->a(ILandroid/os/ParcelFileDescriptor;)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lgch;->a(ILandroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgch;->d:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgch;->d:Lfsy;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    :cond_0
    return-void
.end method
