.class final Liai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;

.field private f:I

.field private g:I

.field private h:I

.field private i:Lilx;

.field private j:Lilx;

.field private k:I

.field private l:Z

.field private m:J

.field private n:Lhon;

.field private o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "Gms"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NetworkLocationListeners"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Liai;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "Gmm"

    goto :goto_0
.end method

.method constructor <init>(ILhon;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    const v3, 0x7fffffff

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liai;->c:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liai;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liai;->e:Ljava/util/Map;

    iput v3, p0, Liai;->f:I

    iput v3, p0, Liai;->g:I

    iput v2, p0, Liai;->h:I

    iput-object v4, p0, Liai;->i:Lilx;

    iput-object v4, p0, Liai;->j:Lilx;

    iput v3, p0, Liai;->k:I

    iput-boolean v2, p0, Liai;->l:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Liai;->m:J

    iput v2, p0, Liai;->o:I

    iput v2, p0, Liai;->p:I

    iput p1, p0, Liai;->b:I

    iput-object p2, p0, Liai;->n:Lhon;

    return-void
.end method

.method private a(Ljava/lang/String;)Lial;
    .locals 2

    iget-object v0, p0, Liai;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lial;

    if-nez v0, :cond_0

    iget-object v0, p0, Liai;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x28

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lial;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lial;-><init>(Liai;B)V

    iget-object v1, p0, Liai;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 5

    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    const-wide/16 v2, 0x3c

    div-long v2, v0, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sec. ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " min.)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Liai;Liak;J)V
    .locals 12

    iget-object v0, p1, Liak;->a:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Liai;->a(Ljava/lang/String;)Lial;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v0, p1, Liak;->a:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v5

    iget v6, p1, Liak;->c:I

    iget-wide v7, p1, Liak;->f:J

    iget-object v0, v3, Lial;->c:Liai;

    invoke-direct {v0, v5}, Liai;->a(Ljava/lang/String;)Lial;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Liak;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, v3, Lial;->b:Ljava/util/Map;

    move-object v1, v0

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_5

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v2, 0x5

    if-ge v0, v2, :cond_3

    const-wide/16 v9, 0x0

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v2, v0

    :goto_1
    iget-object v0, v3, Lial;->c:Liai;

    const-wide v3, 0x7fffffffffffffffL

    iget-object v0, v0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    if-eq v0, p1, :cond_0

    iget v10, v0, Liak;->c:I

    if-ne v6, v10, :cond_0

    iget-object v10, v0, Liak;->e:Ljava/lang/String;

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-wide v10, v0, Liak;->f:J

    cmp-long v10, v10, v3

    if-gez v10, :cond_0

    iget-wide v3, v0, Liak;->f:J

    goto :goto_2

    :cond_1
    iget-object v0, v3, Lial;->a:Ljava/util/Map;

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-wide v9, 0x7fffffffffffffffL

    cmp-long v0, v3, v9

    if-nez v0, :cond_4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v4, p2, v7

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void

    :cond_4
    cmp-long v0, v3, v7

    if-ltz v0, :cond_3

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long v2, v3, v7

    add-long/2addr v2, v9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    :cond_5
    move-object v2, v0

    goto :goto_1
.end method

.method private static a(Ljava/io/PrintWriter;JILjava/lang/String;Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NLPClient: duration="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Liai;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " period: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " low power: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method private a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    move-object v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    if-nez v0, :cond_0

    invoke-direct {p0}, Liai;->k()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    iget-boolean v4, v0, Liak;->j:Z

    if-ne p5, v4, :cond_5

    if-eqz p6, :cond_1

    iget-boolean v4, v0, Liak;->k:Z

    if-eqz v4, :cond_5

    :cond_1
    iget-wide v4, v0, Liak;->f:J

    cmp-long v4, v4, p1

    if-gez v4, :cond_4

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_2

    sget-object v4, Liai;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Delivering a location to a listener registered at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Liak;->f:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", location timestamp is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p3, v1}, Liak;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_5

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_3

    sget-object v2, Liai;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Liak;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_4
    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_5

    sget-object v4, Liai;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Not delivering a location to a listener registered at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Liak;->f:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", location timestamp is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    return v2
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    sget-object v0, Liai;->a:Ljava/lang/String;

    return-object v0
.end method

.method private k()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.location.internal.EXTRA_RELEASE_VERSION"

    iget v2, p0, Liai;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private l()V
    .locals 6

    const v5, 0x7fffffff

    iput v5, p0, Liai;->f:I

    iput v5, p0, Liai;->g:I

    iput v5, p0, Liai;->h:I

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    sget-object v2, Liai;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Still have pending intent "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Liak;->a:Landroid/app/PendingIntent;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-boolean v2, v0, Liak;->k:Z

    if-eqz v2, :cond_1

    iget v2, p0, Liai;->g:I

    iget v3, v0, Liak;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Liai;->g:I

    :goto_1
    iget v2, p0, Liai;->h:I

    iget v0, v0, Liak;->d:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Liai;->h:I

    goto :goto_0

    :cond_1
    iget v2, p0, Liai;->f:I

    iget v3, v0, Liak;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Liai;->f:I

    goto :goto_1

    :cond_2
    iget v0, p0, Liai;->f:I

    iget v1, p0, Liai;->g:I

    if-gt v0, v1, :cond_3

    iput v5, p0, Liai;->g:I

    :cond_3
    iget v0, p0, Liai;->h:I

    if-gez v0, :cond_4

    const/4 v0, 0x0

    iput v0, p0, Liai;->h:I

    :cond_4
    invoke-static {}, Lilx;->a()Lilx;

    move-result-object v0

    iput-object v0, p0, Liai;->i:Lilx;

    iget v0, p0, Liai;->f:I

    iget v1, p0, Liai;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v1, v0, 0x2

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    iget v3, v0, Liak;->c:I

    if-ge v3, v1, :cond_5

    iget-object v3, v0, Liak;->h:Lilx;

    if-eqz v3, :cond_5

    iget-object v3, p0, Liai;->i:Lilx;

    iget-object v0, v0, Liak;->h:Lilx;

    invoke-virtual {v3, v0}, Lilx;->a(Lilx;)V

    goto :goto_2

    :cond_6
    return-void
.end method

.method private m()V
    .locals 6

    const/4 v1, 0x0

    const v0, 0x7fffffff

    iput v0, p0, Liai;->k:I

    iput-boolean v1, p0, Liai;->l:Z

    iget-object v0, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaj;

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    sget-object v3, Liai;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Still have activity pending intent "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Liaj;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v3, p0, Liai;->k:I

    iget v4, v0, Liaj;->c:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Liai;->k:I

    iget-boolean v3, p0, Liai;->l:Z

    if-nez v3, :cond_1

    iget-boolean v0, v0, Liaj;->g:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Liai;->l:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {}, Lilx;->a()Lilx;

    move-result-object v0

    iput-object v0, p0, Liai;->j:Lilx;

    iget v0, p0, Liai;->k:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    int-to-long v1, v0

    iget-object v0, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaj;

    iget v4, v0, Liaj;->c:I

    int-to-long v4, v4

    cmp-long v4, v4, v1

    if-gez v4, :cond_4

    iget-object v4, v0, Liaj;->h:Lilx;

    if-eqz v4, :cond_4

    iget-object v4, p0, Liai;->j:Lilx;

    iget-object v0, v0, Liaj;->h:Lilx;

    invoke-virtual {v4, v0}, Lilx;->a(Lilx;)V

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Liai;->f:I

    return v0
.end method

.method final a(JLandroid/content/Context;Landroid/location/Location;Landroid/location/Location;Z)Ljava/util/List;
    .locals 9

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Liai;->a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z

    move-result v8

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Liai;->a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z

    move-result v0

    or-int/2addr v0, v8

    if-eqz v0, :cond_0

    invoke-direct {p0}, Liai;->l()V

    :cond_0
    return-object v7
.end method

.method final a(Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Liak;->a()V

    invoke-direct {p0}, Liai;->l()V

    :cond_0
    return-void
.end method

.method final a(Landroid/content/Context;II)V
    .locals 6

    iput p2, p0, Liai;->o:I

    iput p3, p0, Liai;->p:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    iget v4, v0, Liak;->l:I

    if-ne v4, p2, :cond_1

    iget v4, v0, Liak;->m:I

    if-eq v4, p3, :cond_0

    :cond_1
    iput p2, v0, Liak;->l:I

    iput p3, v0, Liak;->m:I

    if-nez v1, :cond_2

    invoke-direct {p0}, Liai;->k()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "com.google.android.location.internal.WIFI_LOCATION_STATUS"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.google.android.location.internal.CELL_LOCATION_STATUS"

    invoke-virtual {v1, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v0, p1, v1}, Liak;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_3

    sget-object v2, Liai;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Liak;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    invoke-direct {p0}, Liai;->l()V

    :cond_5
    return-void
.end method

.method final a(Landroid/content/Context;Landroid/app/PendingIntent;IIZZLilx;)V
    .locals 10

    iget-wide v1, p0, Liai;->m:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Liai;->m:J

    :cond_0
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    new-instance v6, Lhsk;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NLP PendingIntent client in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lhsk;->b:[S

    invoke-direct {v6, v1, v2, v3}, Lhsk;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[S)V

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Lhsk;->a(Lilx;)V

    new-instance v1, Liak;

    move-object v2, p0

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Liak;-><init>(Liai;Landroid/app/PendingIntent;IILhsk;ZZLilx;)V

    iget-object v2, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liak;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Liak;->a()V

    :cond_1
    invoke-direct {p0}, Liai;->l()V

    iget v1, p0, Liai;->o:I

    iget v2, p0, Liai;->p:I

    invoke-virtual {p0, p1, v1, v2}, Liai;->a(Landroid/content/Context;II)V

    return-void
.end method

.method final a(Landroid/content/Context;Landroid/app/PendingIntent;IZLilx;)V
    .locals 8

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    new-instance v5, Lhsk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NLP ActivityPendingIntent client in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lhsk;->b:[S

    invoke-direct {v5, v0, v1, v2}, Lhsk;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[S)V

    invoke-virtual {v5, p5}, Lhsk;->a(Lilx;)V

    new-instance v0, Liaj;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v6, p5

    move v7, p4

    invoke-direct/range {v0 .. v7}, Liaj;-><init>(Liai;Landroid/app/PendingIntent;IILhsk;Lilx;Z)V

    iget-object v1, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Liai;->m()V

    return-void
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    move-object v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_0

    invoke-direct {p0}, Liai;->k()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaj;

    iget-boolean v4, v0, Liaj;->g:Z

    if-nez v4, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    const/4 v5, 0x6

    if-eq v4, v5, :cond_6

    :cond_1
    invoke-virtual {v0, p1, v1}, Liaj;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    sget-object v2, Liai;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Liaj;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v2, 0x1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    iget-object v0, v0, Liaj;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Liai;->n:Lhon;

    invoke-virtual {v0, v4}, Lhon;->b(Landroid/os/Parcelable;)V

    :cond_3
    move v0, v2

    move v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    invoke-direct {p0}, Liai;->m()V

    :cond_5
    return-void

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method final a(Landroid/content/Context;Z)V
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_0

    invoke-direct {p0}, Liai;->k()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "providerEnabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaj;

    invoke-virtual {v0, p1, v1}, Liaj;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    sget-object v2, Liai;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Liaj;->a:Landroid/app/PendingIntent;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-direct {p0}, Liai;->l()V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 11

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    iget-wide v0, p0, Liai;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string v0, "0"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "####NLP PendingIntent Location Client Stats: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    const-string v0, "-Currently connected location PendingIntents-"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    iget-boolean v1, v0, Liak;->k:Z

    if-eqz v1, :cond_1

    move-object v1, v7

    :goto_2
    iget-object v2, v0, Liak;->e:Ljava/lang/String;

    iget v3, v0, Liak;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-wide v1, v0, Liak;->f:J

    sub-long v2, v9, v1

    iget-object v1, p0, Liai;->e:Ljava/util/Map;

    iget-object v4, v0, Liak;->e:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lial;

    if-eqz v1, :cond_9

    iget v4, v0, Liak;->c:I

    iget-boolean v5, v0, Liak;->k:Z

    if-eqz v5, :cond_2

    iget-object v1, v1, Lial;->b:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    :goto_3
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long v1, v2, v4

    :goto_4
    iget v3, v0, Liak;->c:I

    iget-object v4, v0, Liak;->e:Ljava/lang/String;

    iget-boolean v5, v0, Liak;->k:Z

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Liai;->a(Ljava/io/PrintWriter;JILjava/lang/String;Z)V

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "totalTime="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Liai;->m:J

    sub-long v1, v9, v1

    invoke-static {v1, v2}, Liai;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move-object v1, v8

    goto :goto_2

    :cond_2
    iget-object v1, v1, Lial;->a:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    goto :goto_3

    :cond_3
    const-string v0, "-Previous packages-"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Liai;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lial;

    iget-object v0, v6, Lial;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Liai;->a(Ljava/io/PrintWriter;JILjava/lang/String;Z)V

    goto :goto_5

    :cond_6
    iget-object v0, v6, Lial;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v5, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Liai;->a(Ljava/io/PrintWriter;JILjava/lang/String;Z)V

    goto :goto_6

    :cond_8
    const-string v0, "####Finished NLP PendingIntent Location Client Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_9
    move-wide v1, v2

    goto/16 :goto_4
.end method

.method final b()I
    .locals 1

    iget v0, p0, Liai;->g:I

    return v0
.end method

.method final b(Landroid/app/PendingIntent;)V
    .locals 3

    iget-object v0, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Liai;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to find the activity detection pendingIntent to remove: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Liai;->m()V

    return-void
.end method

.method final c()I
    .locals 1

    iget v0, p0, Liai;->h:I

    return v0
.end method

.method final d()Lilx;
    .locals 1

    iget-object v0, p0, Liai;->i:Lilx;

    return-object v0
.end method

.method final e()Lilx;
    .locals 1

    iget-object v0, p0, Liai;->j:Lilx;

    return-object v0
.end method

.method final f()I
    .locals 1

    iget v0, p0, Liai;->k:I

    return v0
.end method

.method final g()Z
    .locals 1

    iget-boolean v0, p0, Liai;->l:Z

    return v0
.end method

.method final h()I
    .locals 1

    iget-object v0, p0, Liai;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method final i()Ljava/util/Map;
    .locals 8

    const v3, 0x7fffffff

    new-instance v4, Ljava/util/HashMap;

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    iget-object v0, p0, Liai;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liak;

    iget-object v1, v0, Liak;->e:Ljava/lang/String;

    iget-object v1, v0, Liak;->h:Lilx;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lilx;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-le v2, v6, :cond_4

    move v2, v3

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v2, v7, :cond_3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_3
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    iget v0, v0, Liak;->c:I

    move v2, v0

    goto :goto_0

    :cond_5
    return-object v4
.end method
