.class public final Lcgu;
.super Lcfl;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/auth/AppIdentity;

.field public final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final d:I


# direct methods
.method public constructor <init>(Lcdu;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;ILjava/lang/String;)V
    .locals 2

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcgu;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    iput p4, p0, Lcgu;->d:I

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcgu;->b:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcgu;
    .locals 7

    sget-object v0, Lcex;->a:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcex;->c:Lcex;

    invoke-virtual {v2}, Lcex;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    sget-object v2, Lcex;->e:Lcex;

    invoke-virtual {v2}, Lcex;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    sget-object v2, Lcex;->b:Lcex;

    invoke-virtual {v2}, Lcex;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v4

    sget-object v2, Lcex;->d:Lcex;

    invoke-virtual {v2}, Lcex;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-static {v3, v6}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v3

    new-instance v0, Lcgu;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcgu;-><init>(Lcdu;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;ILjava/lang/String;)V

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v1

    invoke-virtual {v1}, Lcew;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcgu;->d(J)V

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcex;->a:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgu;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcex;->c:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcex;->e:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcex;->b:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcgu;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Lcex;->d:Lcex;

    invoke-virtual {v0}, Lcex;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Subscription [entrySpec="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcgu;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscribingApp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcgu;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serviceClassName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
