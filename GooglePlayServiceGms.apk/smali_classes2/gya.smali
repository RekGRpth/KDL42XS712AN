.class public final Lgya;
.super Lboc;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final Y:Ljava/lang/Object;

.field static final Z:Ljava/lang/Object;


# instance fields
.field private aa:I

.field private ab:Lioj;

.field private ac:Lipv;

.field private ad:[Lioj;

.field private ae:Ljava/util/ArrayList;

.field private af:Z

.field private ag:Z

.field private ah:[I

.field private ai:[I

.field private aj:Ljava/util/ArrayList;

.field private ak:Lgyc;

.field private al:Lhgu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgya;->Y:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgya;->Z:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lboc;-><init>()V

    return-void
.end method

.method private static a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 4

    new-instance v0, Lgya;

    invoke-direct {v0}, Lgya;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "mode"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "instrument"

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "address"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "instruments"

    invoke-static {p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([Lizs;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v2, "existingAddresses"

    invoke-static {v1, v2, p4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v2, "requiresFullBillingAddress"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "phoneNumberRequired"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "disallowedPaymentInstrumentTypes"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "allowedShippingCountryCodes"

    invoke-virtual {v1, v2, p9}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lgya;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/16 v0, 0x9

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lioj;[Lioj;Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v2, 0x0

    const/4 v0, 0x3

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, p4

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lipv;Ljava/util/List;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v5, 0x1

    move-object v2, p0

    move-object v3, v1

    move-object v4, p1

    move v6, p3

    move-object v7, v1

    move-object v8, v1

    move-object v9, p2

    move-object v10, p4

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v1, 0x0

    const/16 v0, 0x66

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, p0

    move v6, p1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p2

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lioj;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/16 v0, 0x65

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p1

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lgya;Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v1, :cond_0

    instance-of v1, p1, Lioj;

    if-eqz v1, :cond_1

    check-cast p1, Lioj;

    iget-object v0, p1, Lioj;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v1, p1, Lipv;

    if-eqz v1, :cond_4

    check-cast p1, Lipv;

    invoke-static {p1}, Lgth;->c(Lipv;)Z

    move-result v1

    iget v2, p0, Lgya;->aa:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    if-eqz v1, :cond_2

    const v0, 0x7f0b010a    # com.google.android.gms.R.string.wallet_dialog_required_action_complete_shipping_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v0, 0x7f0b0109    # com.google.android.gms.R.string.wallet_dialog_required_action_assign_shipping_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    if-eqz v1, :cond_3

    const v0, 0x7f0b010d    # com.google.android.gms.R.string.wallet_dialog_required_action_complete_billing_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x7f0b010c    # com.google.android.gms.R.string.wallet_dialog_required_action_assign_billing_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p1, Lipv;->a:Lixo;

    const-string v1, ", "

    invoke-static {v0, v1}, Lgvs;->a(Lixo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v1, Lgya;->Z:Ljava/lang/Object;

    if-ne p1, v1, :cond_5

    iget v1, p0, Lgya;->aa:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0b010f    # com.google.android.gms.R.string.wallet_dialog_required_action_use_other_payment_method

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    if-ne p1, v1, :cond_0

    iget v1, p0, Lgya;->aa:I

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b010b    # com.google.android.gms.R.string.wallet_dialog_required_action_add_new_shipping_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b010e    # com.google.android.gms.R.string.wallet_dialog_required_action_add_new_billing_address

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b0110    # com.google.android.gms.R.string.wallet_dialog_required_action_add_new_payment_method

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/ArrayList;Lipv;Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 8

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_4

    if-eqz p2, :cond_5

    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    move-object v1, v0

    :goto_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipv;

    invoke-static {v0}, Lgth;->b(Lipv;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v0}, Lgth;->c(Lipv;)Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz p1, :cond_1

    iget-object v6, p1, Lipv;->b:Ljava/lang/String;

    iget-object v7, v0, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v6, v0, Lipv;->a:Lixo;

    iget-object v6, v6, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_2
    iget-object v6, v0, Lipv;->a:Lixo;

    invoke-virtual {v4, v6, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v2}, Lgty;->a(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixo;

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    return-object v3

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lgya;Landroid/widget/ImageView;Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p2, Lioj;

    if-eqz v0, :cond_0

    check-cast p2, Lioj;

    iget-object v0, p0, Lgya;->al:Lhgu;

    invoke-static {p1, p2, v0}, Lgth;->a(Landroid/widget/ImageView;Lioj;Lhgu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lgya;Lioj;)Z
    .locals 1

    invoke-direct {p0, p1}, Lgya;->a(Lioj;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lgya;Lipv;)Z
    .locals 1

    invoke-direct {p0, p1}, Lgya;->a(Lipv;)Z

    move-result v0

    return v0
.end method

.method private a(Lioj;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lgya;->ai:[I

    iget v3, p1, Lioj;->l:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lgya;->ah:[I

    iget v3, p1, Lioj;->c:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lgth;->d(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lgth;->e(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private a(Lipv;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lgth;->b(Lipv;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgya;->aj:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgya;->aj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p1, Lipv;->a:Lixo;

    iget-object v4, v4, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/16 v0, 0xa

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v1, 0x0

    const/16 v0, 0x68

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, p0

    move v6, p1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p2

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static b([Lioj;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/16 v0, 0x67

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p1

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lgya;Ljava/lang/Object;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v1, :cond_0

    instance-of v1, p1, Lioj;

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lioj;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v2, 0x1

    iget-boolean v3, p0, Lgya;->af:Z

    iget-boolean v4, p0, Lgya;->ag:Z

    iget-object v5, p0, Lgya;->ah:[I

    iget-object v6, p0, Lgya;->ai:[I

    invoke-static/range {v0 .. v6}, Lgth;->a(Landroid/content/Context;Lioj;ZZZ[I[I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v1, p1, Lipv;

    if-eqz v1, :cond_0

    check-cast p1, Lipv;

    iget v1, p0, Lgya;->aa:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v0, p1, Lipv;->a:Lixo;

    const-string v1, ", "

    invoke-static {v0, v1}, Lgvs;->a(Lixo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, p1}, Lgya;->a(Lipv;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lgth;->c(Lipv;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v0, 0x7f0b0173    # com.google.android.gms.R.string.wallet_min_address_editable

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lgya;->ag:Z

    if-eqz v1, :cond_0

    invoke-static {p1}, Lgth;->a(Lipv;)Z

    move-result v1

    if-nez v1, :cond_0

    const v0, 0x7f0b0171    # com.google.android.gms.R.string.wallet_missing_phone_editable

    invoke-virtual {p0, v0}, Lgya;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/16 v0, 0xb

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method private c(I)Ljava/lang/Object;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lk;->f:Landroid/app/Dialog;

    instance-of v2, v0, Landroid/app/AlertDialog;

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static d(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x5

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v5, v0

    move v6, v0

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lgya;->a(ILioj;Lipv;[Lioj;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lboc;->a(Landroid/app/Activity;)V

    new-instance v0, Lhgu;

    invoke-direct {v0, p1}, Lhgu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgya;->al:Lhgu;

    iget-object v0, p0, Lgya;->al:Lhgu;

    invoke-static {p1}, Lhgs;->a(Landroid/content/Context;)Lhgs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgu;->a(Lhgs;)V

    return-void
.end method

.method public final a(Lgyc;)V
    .locals 0

    iput-object p1, p0, Lgya;->ak:Lgyc;

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lboc;->a_(Landroid/os/Bundle;)V

    iget-object v5, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "mode"

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgya;->aa:I

    const-string v0, "instrument"

    const-class v1, Lioj;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lgya;->ab:Lioj;

    const-string v0, "address"

    const-class v1, Lipv;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, p0, Lgya;->ac:Lipv;

    const-string v0, "instruments"

    const-class v6, Lioj;

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-nez v7, :cond_1

    move-object v0, v2

    :cond_0
    check-cast v0, [Lioj;

    iput-object v0, p0, Lgya;->ad:[Lioj;

    const-string v0, "existingAddresses"

    const-class v1, Lipv;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgya;->ae:Ljava/util/ArrayList;

    const-string v0, "requiresFullBillingAddress"

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgya;->af:Z

    const-string v0, "phoneNumberRequired"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgya;->ag:Z

    const-string v0, "disallowedPaymentInstrumentTypes"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lgya;->ah:[I

    const-string v0, "allowedShippingCountryCodes"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgya;->aj:Ljava/util/ArrayList;

    const-string v0, "disallowedCardCategories"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lgya;->ai:[I

    return-void

    :cond_1
    invoke-virtual {v7}, Landroid/os/Bundle;->size()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizs;

    move v3, v4

    :goto_0
    if-ge v3, v8, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v1

    :goto_1
    aput-object v1, v0, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13

    const/4 v12, 0x7

    const/4 v11, 0x1

    const/4 v1, 0x0

    const/4 v10, -0x1

    const v9, 0x7f0b017b    # com.google.android.gms.R.string.wallet_ok

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "buyFlowConfig"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v2, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "required_action_dialog"

    new-array v5, v11, [Ljava/lang/Object;

    iget v0, p0, Lgya;->aa:I

    sparse-switch v0, :sswitch_data_0

    const-string v6, "RequiredActionDialogFra"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No mode string found for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mode_unknown"

    :goto_0
    aput-object v0, v5, v1

    invoke-static {v2, v3, v4, v5}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget v0, p0, Lgya;->aa:I

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal dialog mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgya;->aa:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "mode_new_instrument"

    goto :goto_0

    :sswitch_1
    const-string v0, "mode_new_shipping_address"

    goto :goto_0

    :sswitch_2
    const-string v0, "mode_full_billing_address"

    goto :goto_0

    :sswitch_3
    const-string v0, "mode_selected_instrument_not_found"

    goto :goto_0

    :sswitch_4
    const-string v0, "mode_selected_address_not_found"

    goto :goto_0

    :sswitch_5
    const-string v0, "mode_selected_prepaid_disallowed"

    goto :goto_0

    :sswitch_6
    const-string v0, "mode_selected_debit_disallowed"

    goto :goto_0

    :sswitch_7
    const-string v0, "mode_selected_amex_disallowed"

    goto :goto_0

    :sswitch_8
    const-string v0, "mode_accept_tos"

    goto :goto_0

    :sswitch_9
    const-string v0, "mode_instrument_selector"

    goto :goto_0

    :sswitch_a
    const-string v0, "mode_shipping_address_selector"

    goto :goto_0

    :sswitch_b
    const-string v0, "mode_upgrade_instrument"

    goto :goto_0

    :sswitch_c
    const-string v0, "mode_update_shipping_address"

    goto :goto_0

    :sswitch_d
    const-string v0, "mode_choose_alternative_instrument"

    goto :goto_0

    :sswitch_e
    const-string v0, "mode_choose_alternative_shipping_address"

    goto :goto_0

    :sswitch_f
    const v0, 0x7f0b0101    # com.google.android.gms.R.string.wallet_dialog_required_action_title_payment_option_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b0102    # com.google.android.gms.R.string.wallet_dialog_required_action_message_payment_option_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_1
    return-object v0

    :sswitch_10
    const v0, 0x7f0b0103    # com.google.android.gms.R.string.wallet_dialog_required_action_title_shipping_address_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lgya;->ae:Ljava/util/ArrayList;

    iget-object v1, p0, Lgya;->ac:Lipv;

    iget-object v3, p0, Lgya;->aj:Ljava/util/ArrayList;

    invoke-static {v0, v1, v3}, Lgya;->a(Ljava/util/ArrayList;Lipv;Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0b0104    # com.google.android.gms.R.string.wallet_dialog_required_action_message_shipping_address_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lgya;->ac:Lipv;

    invoke-direct {p0, v0}, Lgya;->a(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgya;->ac:Lipv;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v0, Lgya;->Y:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgyb;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, p0, v3, v1}, Lgyb;-><init>(Lgya;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v10, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    :sswitch_11
    const v0, 0x7f0b0105    # com.google.android.gms.R.string.wallet_dialog_required_action_title_full_billing_address

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lgya;->ae:Ljava/util/ArrayList;

    const/4 v3, 0x0

    new-array v4, v11, [Ljava/lang/String;

    iget-object v5, p0, Lgya;->ab:Lioj;

    iget-object v5, v5, Lioj;->e:Lipv;

    iget-object v5, v5, Lipv;->a:Lixo;

    iget-object v5, v5, Lixo;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lgya;->a(Ljava/util/ArrayList;Lipv;Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f0b0106    # com.google.android.gms.R.string.wallet_dialog_required_action_message_full_billing_address

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_3
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lgya;->ab:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lgya;->ad:[Lioj;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgya;->ad:[Lioj;

    array-length v0, v0

    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    sget-object v0, Lgya;->Z:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    sget-object v0, Lgya;->Y:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgyb;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, p0, v3, v1}, Lgyb;-><init>(Lgya;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v10, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_3

    :sswitch_12
    const v0, 0x7f0b00fb    # com.google.android.gms.R.string.wallet_dialog_required_action_title_alternative_card_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b00fc    # com.google.android.gms.R.string.wallet_dialog_required_action_message_alternative_card_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_13
    const v0, 0x7f0b00fd    # com.google.android.gms.R.string.wallet_dialog_required_action_title_alternative_address_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b00fe    # com.google.android.gms.R.string.wallet_dialog_required_action_message_alternative_address_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_14
    const v0, 0x7f0b00f9    # com.google.android.gms.R.string.wallet_dialog_required_action_title_prepaid_disallowed

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b00e9    # com.google.android.gms.R.string.wallet_prepaid_disallowed_info_body

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_15
    const v0, 0x7f0b00fa    # com.google.android.gms.R.string.wallet_dialog_required_action_title_debit_disallowed

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b00ea    # com.google.android.gms.R.string.wallet_debit_disallowed_info_body

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_16
    const v0, 0x7f0b00e7    # com.google.android.gms.R.string.wallet_error_creditcard_amex_disallowed

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b00e8    # com.google.android.gms.R.string.wallet_amex_disallowed_info_body

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_17
    const v0, 0x7f0b00ff    # com.google.android.gms.R.string.wallet_dialog_required_action_title_accept_tos

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b0100    # com.google.android.gms.R.string.wallet_dialog_required_action_message_accept_tos

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v9, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_18
    const v0, 0x7f0b0107    # com.google.android.gms.R.string.wallet_dialog_required_action_title_update_payment_information

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iput v12, p0, Lgya;->aa:I

    :goto_4
    iget v0, p0, Lgya;->aa:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown dialog mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgya;->aa:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_19
    const v0, 0x7f0b00fb    # com.google.android.gms.R.string.wallet_dialog_required_action_title_alternative_card_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iput v12, p0, Lgya;->aa:I

    goto :goto_4

    :sswitch_1a
    const v0, 0x7f0b0108    # com.google.android.gms.R.string.wallet_dialog_required_action_title_update_shipping_address

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/16 v0, 0x8

    iput v0, p0, Lgya;->aa:I

    goto :goto_4

    :sswitch_1b
    const v0, 0x7f0b00fd    # com.google.android.gms.R.string.wallet_dialog_required_action_title_alternative_address_required

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/16 v0, 0x8

    iput v0, p0, Lgya;->aa:I

    goto :goto_4

    :pswitch_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lgya;->ad:[Lioj;

    if-eqz v0, :cond_5

    iget-object v4, p0, Lgya;->ad:[Lioj;

    array-length v5, v4

    move v0, v1

    :goto_5
    if-ge v0, v5, :cond_5

    aget-object v1, v4, v0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    sget-object v0, Lgya;->Y:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgyb;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, p0, v1, v3}, Lgyb;-><init>(Lgya;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v10, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lgya;->ae:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgya;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_6
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lgyb;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v1, p0, v3, v0}, Lgyb;-><init>(Lgya;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v1, v10, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_8
        0x7 -> :sswitch_9
        0x8 -> :sswitch_a
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0xb -> :sswitch_7
        0x65 -> :sswitch_b
        0x66 -> :sswitch_c
        0x67 -> :sswitch_d
        0x68 -> :sswitch_e
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_10
        0x3 -> :sswitch_11
        0x4 -> :sswitch_12
        0x5 -> :sswitch_13
        0x6 -> :sswitch_17
        0x9 -> :sswitch_14
        0xa -> :sswitch_15
        0xb -> :sswitch_16
        0x65 -> :sswitch_18
        0x66 -> :sswitch_1a
        0x67 -> :sswitch_19
        0x68 -> :sswitch_1b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    invoke-super {p0, p1}, Lboc;->onCancel(Landroid/content/DialogInterface;)V

    iget-object v0, p0, Lgya;->ak:Lgyc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0}, Lgyc;->b()V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const/4 v1, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lgya;->aa:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0}, Lgyc;->a()V

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0}, Lgyc;->b()V

    goto :goto_0

    :pswitch_2
    if-ne p2, v1, :cond_3

    iget-object v0, p0, Lgya;->ac:Lipv;

    invoke-direct {p0, v0}, Lgya;->a(Lipv;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgya;->ak:Lgyc;

    iget-object v1, p0, Lgya;->ac:Lipv;

    invoke-interface {v0, v1}, Lgyc;->b(Lipv;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0, v2}, Lgyc;->a(Lipv;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p2}, Lgya;->c(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lipv;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgya;->ak:Lgyc;

    check-cast v0, Lipv;

    invoke-interface {v1, v0}, Lgyc;->a(Lipv;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0, v2}, Lgyc;->a(Lipv;)V

    goto :goto_0

    :pswitch_3
    if-ne p2, v1, :cond_5

    iget-object v0, p0, Lgya;->ak:Lgyc;

    iget-object v1, p0, Lgya;->ab:Lioj;

    invoke-interface {v0, v1, v2}, Lgyc;->a(Lioj;Lipv;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, p2}, Lgya;->c(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lgya;->Z:Ljava/lang/Object;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0}, Lgyc;->b()V

    goto :goto_0

    :cond_6
    instance-of v1, v0, Lipv;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lgya;->ak:Lgyc;

    iget-object v2, p0, Lgya;->ab:Lioj;

    check-cast v0, Lipv;

    invoke-interface {v1, v2, v0}, Lgyc;->a(Lioj;Lipv;)V

    goto :goto_0

    :cond_7
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    iget-object v1, p0, Lgya;->ab:Lioj;

    invoke-interface {v0, v1, v2}, Lgyc;->a(Lioj;Lipv;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p2}, Lgya;->c(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lioj;

    if-eqz v1, :cond_8

    check-cast v0, Lioj;

    invoke-direct {p0, v0}, Lgya;->a(Lioj;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgya;->ak:Lgyc;

    invoke-interface {v1, v0}, Lgyc;->a(Lioj;)V

    goto/16 :goto_0

    :cond_8
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0}, Lgyc;->a()V

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p2}, Lgya;->c(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lipv;

    if-eqz v1, :cond_9

    check-cast v0, Lipv;

    invoke-direct {p0, v0}, Lgya;->a(Lipv;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgya;->ak:Lgyc;

    invoke-interface {v1, v0}, Lgyc;->b(Lipv;)V

    goto/16 :goto_0

    :cond_9
    sget-object v1, Lgya;->Y:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgya;->ak:Lgyc;

    invoke-interface {v0, v2}, Lgyc;->a(Lipv;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
