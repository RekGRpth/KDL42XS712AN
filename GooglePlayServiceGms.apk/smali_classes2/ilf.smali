.class public Lilf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;


# instance fields
.field private final b:[I

.field private c:J

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lilf;->a:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p1, [I

    iput-object v0, p0, Lilf;->b:[I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lilf;->c:J

    const/4 v0, 0x0

    iput v0, p0, Lilf;->d:I

    return-void
.end method

.method public static varargs a([Lilf;)Ljava/lang/Iterable;
    .locals 1

    new-instance v0, Lilg;

    invoke-direct {v0, p0}, Lilg;-><init>([Lilf;)V

    return-object v0
.end method

.method private b(I)V
    .locals 3

    if-ltz p1, :cond_0

    iget v0, p0, Lilf;->d:I

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not in [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lilf;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private f(I)I
    .locals 2

    iget v0, p0, Lilf;->e:I

    sub-int/2addr v0, p1

    if-gez v0, :cond_0

    iget-object v1, p0, Lilf;->b:[I

    array-length v1, v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lilf;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Cookie = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lilf;->c(I)B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(BJ)V
    .locals 7

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lilf;->c:J

    sub-long v0, p2, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const-wide/32 v4, 0x1000000

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    :cond_0
    iput-wide p2, p0, Lilf;->c:J

    move-wide v0, v2

    :cond_1
    iget v2, p0, Lilf;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lilf;->e:I

    iget v2, p0, Lilf;->e:I

    iget-object v3, p0, Lilf;->b:[I

    array-length v3, v3

    if-lt v2, v3, :cond_2

    const/4 v2, 0x0

    iput v2, p0, Lilf;->e:I

    :cond_2
    iget v2, p0, Lilf;->d:I

    iget-object v3, p0, Lilf;->b:[I

    array-length v3, v3

    if-ge v2, v3, :cond_3

    iget v2, p0, Lilf;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lilf;->d:I

    :cond_3
    iget-object v2, p0, Lilf;->b:[I

    iget v3, p0, Lilf;->e:I

    shl-int/lit8 v4, p1, 0x18

    const-wide/32 v5, 0xffffff

    and-long/2addr v0, v5

    long-to-int v0, v0

    or-int/2addr v0, v4

    aput v0, v2, v3

    return-void
.end method

.method public final c(I)B
    .locals 2

    invoke-direct {p0, p1}, Lilf;->b(I)V

    iget-object v0, p0, Lilf;->b:[I

    invoke-direct {p0, p1}, Lilf;->f(I)I

    move-result v1

    aget v0, v0, v1

    const/high16 v1, -0x1000000

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x18

    int-to-byte v0, v0

    return v0
.end method

.method public final d(I)J
    .locals 4

    invoke-direct {p0, p1}, Lilf;->b(I)V

    iget-object v0, p0, Lilf;->b:[I

    invoke-direct {p0, p1}, Lilf;->f(I)I

    move-result v1

    aget v0, v0, v1

    int-to-long v0, v0

    const-wide/32 v2, 0xffffff

    and-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lilf;->c:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final e(I)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0, p1}, Lilf;->d(I)J

    move-result-wide v0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    sget-object v0, Lilf;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lilf;->d:I

    return v0
.end method
