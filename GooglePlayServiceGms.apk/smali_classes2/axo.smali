.class public final Laxo;
.super Laxb;
.source "SourceFile"


# static fields
.field private static final c:Laye;


# instance fields
.field private final d:Ljava/util/List;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/Thread;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbbj;

    const-string v1, "MdnsDeviceScanner"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Laxo;->c:Laye;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Laxb;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laxo;->d:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b030b    # com.google.android.gms.R.string.generic_cast_device_model_name

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laxo;->f:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Laxo;)V
    .locals 12

    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_0
    iget-boolean v0, p0, Laxo;->h:Z

    if-nez v0, :cond_2

    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget-object v4, p0, Laxo;->e:Ljava/util/Map;

    monitor-enter v4

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxs;

    iget-wide v8, v0, Laxs;->b:J

    sub-long v8, v5, v8

    const-wide/16 v10, 0x5dc0

    cmp-long v1, v8, v10

    if-ltz v1, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_1

    iget-object v0, v0, Laxs;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v1, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v8, Laxr;

    invoke-direct {v8, p0, v0}, Laxr;-><init>(Laxo;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    sget-object v1, Laxo;->c:Laye;

    const-string v8, "expired record for %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v1, v8, v9}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :catch_0
    move-exception v0

    iget-boolean v0, p0, Laxo;->h:Z

    if-eqz v0, :cond_0

    :cond_2
    sget-object v0, Laxo;->c:Laye;

    const-string v1, "refreshLoop exiting"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic a(Laxo;Laxv;)V
    .locals 14

    const/4 v2, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    sget-object v0, Laxo;->c:Laye;

    invoke-virtual {v0}, Laye;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "FQDN: %s"

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p1, Laxv;->a:Ljava/lang/String;

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p1, Laxv;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    sget-object v3, Laxo;->c:Laye;

    const-string v4, "IPv4 address: %s"

    new-array v5, v13, [Ljava/lang/Object;

    aput-object v0, v5, v12

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p1, Laxv;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Inet6Address;

    sget-object v3, Laxo;->c:Laye;

    const-string v4, "IPv6 address: %s"

    new-array v5, v13, [Ljava/lang/Object;

    aput-object v0, v5, v12

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service name: %s"

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p1, Laxv;->d:Ljava/lang/String;

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service host: %s"

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p1, Laxv;->f:Ljava/lang/String;

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service proto: %d"

    new-array v3, v13, [Ljava/lang/Object;

    iget v4, p1, Laxv;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service port: %d"

    new-array v3, v13, [Ljava/lang/Object;

    iget v4, p1, Laxv;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service priority: %d"

    new-array v3, v13, [Ljava/lang/Object;

    iget v4, p1, Laxv;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "service weight: %d"

    new-array v3, v13, [Ljava/lang/Object;

    iget v4, p1, Laxv;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p1, Laxv;->k:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Laxo;->c:Laye;

    const-string v4, "text string: %s"

    new-array v5, v13, [Ljava/lang/Object;

    aput-object v0, v5, v12

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    sget-object v0, Laxo;->c:Laye;

    const-string v1, "TTL: %d"

    new-array v3, v13, [Ljava/lang/Object;

    iget-wide v4, p1, Laxv;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p1, Laxv;->k:Ljava/util/List;

    if-eqz v0, :cond_b

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v7, 0x3d

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    if-lez v7, :cond_4

    invoke-virtual {v0, v12, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v7, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    move-object v5, v0

    goto :goto_3

    :cond_5
    const-string v7, "md"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    const-string v4, "(Eureka|Chromekey)( Dongle)?"

    const-string v7, "Chromecast"

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_3

    :cond_6
    const-string v7, "ve"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    move-object v3, v0

    goto :goto_3

    :cond_7
    const-string v7, "ic"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    :goto_4
    move-object v1, v0

    goto :goto_3

    :cond_8
    if-eqz v5, :cond_b

    if-nez v4, :cond_9

    iget-object v4, p0, Laxo;->f:Ljava/lang/String;

    :cond_9
    iget-object v7, p0, Laxo;->e:Ljava/util/Map;

    monitor-enter v7

    :try_start_0
    iget-object v0, p1, Laxv;->b:Ljava/util/List;

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_a
    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v7

    :cond_b
    :goto_5
    return-void

    :cond_c
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v9, 0x2f

    invoke-virtual {v6, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    if-ltz v9, :cond_d

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    :cond_d
    const-string v9, "http://%s:8008%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v6, 0x1

    aput-object v1, v10, v6

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v6, Lcom/google/android/gms/common/images/WebImage;

    invoke-direct {v6, v1}, Lcom/google/android/gms/common/images/WebImage;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    invoke-static {v5, v0}, Lcom/google/android/gms/cast/CastDevice;->a(Ljava/lang/String;Ljava/net/Inet4Address;)Lawa;

    move-result-object v0

    iget-object v1, p1, Laxv;->e:Ljava/lang/String;

    iget-object v6, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v6, v1}, Lcom/google/android/gms/cast/CastDevice;->b(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v1, v4}, Lcom/google/android/gms/cast/CastDevice;->c(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v1, v3}, Lcom/google/android/gms/cast/CastDevice;->d(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    iget v1, p1, Laxv;->h:I

    iget-object v3, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v3, v1}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;I)I

    iget-object v1, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v1, v8}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/List;)Ljava/util/List;

    iget-object v1, v0, Lawa;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxs;

    if-eqz v0, :cond_11

    iget-object v2, v0, Laxs;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/CastDevice;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-boolean v1, v0, Laxs;->d:Z

    if-nez v1, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Laxs;->b:J

    :cond_f
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_10
    :try_start_1
    iget-object v2, v0, Laxs;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    new-instance v3, Laxs;

    iget-wide v8, p1, Laxv;->l:J

    invoke-direct {v3, p0, v1, v8, v9}, Laxs;-><init>(Laxo;Lcom/google/android/gms/cast/CastDevice;J)V

    invoke-interface {v0, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_12

    invoke-virtual {p0, v2}, Laxo;->a(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_12
    if-eqz v1, :cond_b

    sget-object v0, Laxb;->a:Laye;

    const-string v2, "notifyDeviceOnline: %s"

    new-array v3, v13, [Ljava/lang/Object;

    aput-object v1, v3, v12

    invoke-virtual {v0, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0}, Laxb;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v2, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v3, Laxd;

    invoke-direct {v3, p0, v0, v1}, Laxd;-><init>(Laxb;Ljava/util/List;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_5

    :cond_13
    move-object v0, v1

    goto/16 :goto_4
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Laxo;->e:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxs;

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v0, Laxs;->b:J

    const/4 v1, 0x1

    iput-boolean v1, v0, Laxs;->d:Z

    iget-object v0, v0, Laxs;->a:Lcom/google/android/gms/cast/CastDevice;

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Laxo;->a(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Ljava/util/List;)V
    .locals 6

    const/4 v5, 0x0

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "startScanInternal"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Laxo;->c:Laye;

    const-string v1, "No network interfaces to scan on!"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    new-instance v2, Laxp;

    const-string v3, "_googlecast._tcp.local."

    invoke-direct {v2, p0, v3, v0}, Laxp;-><init>(Laxo;Ljava/lang/String;Ljava/net/NetworkInterface;)V

    :try_start_0
    invoke-virtual {v2}, Laxj;->a()V

    iget-object v3, p0, Laxo;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    sget-object v2, Laxo;->c:Laye;

    const-string v3, "Couldn\'t start MDNS client for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iput-boolean v5, p0, Laxo;->h:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Laxq;

    invoke-direct {v1, p0}, Laxq;-><init>(Laxo;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    iget-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected final c()V
    .locals 2

    iget-object v0, p0, Laxo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Laxo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxj;

    invoke-virtual {v0}, Laxj;->b()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Laxo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Laxo;->h:Z

    iget-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    :goto_1
    :try_start_0
    iget-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    iget-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    iput-object v0, p0, Laxo;->g:Ljava/lang/Thread;

    :cond_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Laxo;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Laxo;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-super {p0}, Laxb;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v2, Laxf;

    invoke-direct {v2, p0, v0}, Laxf;-><init>(Laxb;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
