.class public final Lgzd;
.super Lgzo;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final c:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;[I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgzo;-><init>(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lgzd;->a:Landroid/content/Context;

    iput-object p2, p0, Lgzd;->c:[I

    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lgzd;->c:[I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lgzd;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget v5, v3, v2

    if-ne p1, v5, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public final a(Landroid/widget/TextView;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lgzd;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lgzd;->a:Landroid/content/Context;

    const v1, 0x7f0b014e    # com.google.android.gms.R.string.wallet_error_creditcard_default_disallowed

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzd;->b:Ljava/lang/CharSequence;

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :pswitch_0
    iget-object v0, p0, Lgzd;->a:Landroid/content/Context;

    const v1, 0x7f0b00e7    # com.google.android.gms.R.string.wallet_error_creditcard_amex_disallowed

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzd;->b:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
