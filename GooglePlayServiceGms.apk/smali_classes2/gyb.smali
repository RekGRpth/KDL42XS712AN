.class final Lgyb;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lgya;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lgya;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lgyb;->a:Lgya;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgyb;->b:Landroid/view/LayoutInflater;

    return-void
.end method

.method private a(ILandroid/view/View;)Landroid/view/View;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lgyb;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04014f    # com.google.android.gms.R.layout.wallet_row_required_action_dialog_list

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lgyb;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1}, Lgyb;->isEnabled(I)Z

    move-result v2

    const v0, 0x7f0a005a    # com.google.android.gms.R.id.image

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lgyb;->a:Lgya;

    invoke-static {v3, v0, v1}, Lgya;->a(Lgya;Landroid/widget/ImageView;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgyb;->a:Lgya;

    invoke-static {v3, v1}, Lgya;->a(Lgya;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const v0, 0x7f0a0356    # com.google.android.gms.R.id.metadata

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgyb;->a:Lgya;

    invoke-static {v3, v1}, Lgya;->b(Lgya;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-object p2

    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2}, Lgyb;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2}, Lgyb;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lgyb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lioj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgyb;->a:Lgya;

    check-cast v0, Lioj;

    invoke-static {v1, v0}, Lgya;->a(Lgya;Lioj;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    instance-of v1, v0, Lipv;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgyb;->a:Lgya;

    check-cast v0, Lipv;

    invoke-static {v1, v0}, Lgya;->a(Lgya;Lipv;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
