.class public final Lepd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leox;


# instance fields
.field private final a:Lepf;

.field private final b:[B


# direct methods
.method public constructor <init>(Lepf;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lepd;->a:Lepf;

    iput-object p2, p0, Lepd;->b:[B

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lepd;->a:Lepf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lepf;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;)V

    return-void
.end method

.method public final a(Leok;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lepd;->a:Lepf;

    iget-object v3, p0, Lepd;->b:[B

    const-string v0, "Encrypted bytes must not be null."

    invoke-static {v3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, v3

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v4, "Encrypted bytes must not be empty."

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-static {v3}, Leok;->a([B)Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-interface {v2, v0}, Lepf;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Leok;->a(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v0}, Leok;->b(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Lcom/google/android/gms/identity/accounts/api/AccountData;

    move-result-object v0

    goto :goto_1
.end method
