.class public final Lapv;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 5

    invoke-static {}, Lapr;->a()Lapr;

    move-result-object v0

    const-string v1, "data_api"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "r_requesting_module"

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v3, Lapt;->G:Lapt;

    invoke-virtual {v3}, Lapt;->a()Ljava/lang/String;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, ""

    const-string v3, "country_lookup"

    const-string v4, "https://android.clients.google.com/auth/recovery/countrylookup"

    invoke-virtual {v0, v1, v2, v3, v4}, Lapr;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1, v0}, Lapw;->a(Ljava/lang/String;Ljava/util/Map;)Lapw;

    move-result-object v0

    new-instance v1, Laqf;

    invoke-direct {v1}, Laqf;-><init>()V

    iget-object v2, v0, Lapw;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "GLSUser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while fetching country list: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lapw;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lapw;->j:Ljava/lang/String;

    iput-object v0, v1, Laqf;->g:Ljava/lang/String;

    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    :try_start_0
    invoke-static {v2, v0}, Lapv;->a(Ljava/util/List;Lapw;)V

    invoke-virtual {v1, v2}, Laqf;->a(Ljava/util/List;)Laqf;

    move-result-object v2

    iget-object v0, v0, Lapw;->i:Ljava/lang/String;

    iput-object v0, v2, Laqf;->f:Ljava/lang/String;

    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GLSUser"

    const-string v3, "Error while parsing countryList json"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "BadResponse"

    iput-object v0, v1, Laqf;->g:Ljava/lang/String;

    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 6

    invoke-static {}, Lapr;->a()Lapr;

    move-result-object v0

    const-string v1, "data_api"

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2, p2}, Lapr;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lapw;

    move-result-object v0

    new-instance v1, Laqf;

    invoke-direct {v1}, Laqf;-><init>()V

    iget-object v2, v0, Lapw;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lapw;->j:Ljava/lang/String;

    iput-object v0, v1, Laqf;->g:Ljava/lang/String;

    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error code sent by server: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v3, v0, Lapw;->h:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {v2, v0}, Lapv;->a(Ljava/util/List;Lapw;)V

    :cond_1
    iget-object v3, v0, Lapw;->f:Ljava/lang/String;

    iput-object v3, v1, Laqf;->d:Ljava/lang/String;

    iget-object v3, v0, Lapw;->g:Ljava/lang/String;

    iput-object v3, v1, Laqf;->e:Ljava/lang/String;

    iget-object v3, v0, Lapw;->i:Ljava/lang/String;

    iput-object v3, v1, Laqf;->f:Ljava/lang/String;

    iget-object v3, v0, Lapw;->d:Lapx;

    invoke-virtual {v3}, Lapx;->name()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Laqf;->b:Ljava/lang/String;

    iget-object v3, v0, Lapw;->e:Lapy;

    invoke-virtual {v3}, Lapy;->name()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Laqf;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laqf;->a(Ljava/util/List;)Laqf;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;

    iget-boolean v4, v0, Lapw;->b:Z

    iget-boolean v5, v0, Lapw;->a:Z

    iget-boolean v0, v0, Lapw;->c:Z

    invoke-direct {v3, p0, v4, v5, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;-><init>(Ljava/lang/String;ZZZ)V

    iput-object v3, v2, Laqf;->a:Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;

    if-eqz p1, :cond_2

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lapz;->a(Ljava/lang/String;)Laqa;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, v2, Laqa;->c:Z

    invoke-virtual {v0, v2}, Lapz;->a(Laqa;)V

    :cond_2
    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GLSUser"

    const-string v3, "Error while parsing countryList json"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "BadResponse"

    iput-object v0, v1, Laqf;->g:Ljava/lang/String;

    invoke-virtual {v1}, Laqf;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lapw;)V
    .locals 5

    new-instance v1, Lorg/json/JSONArray;

    iget-object v0, p1, Lapw;->h:Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/auth/Country;

    invoke-direct {v3}, Lcom/google/android/gms/auth/Country;-><init>()V

    const-string v4, "r_country_code"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/auth/Country;->c:Ljava/lang/String;

    const-string v4, "r_country_name"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/gms/auth/Country;->b:Ljava/lang/String;

    iget-object v2, v3, Lcom/google/android/gms/auth/Country;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lcom/google/android/gms/auth/Country;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
