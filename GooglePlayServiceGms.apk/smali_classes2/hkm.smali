.class public final Lhkm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Ljava/util/Comparator;


# instance fields
.field private final a:Ljava/util/List;

.field private b:J

.field private c:J

.field private d:Ljava/lang/Integer;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhkn;

    invoke-direct {v0}, Lhkn;-><init>()V

    sput-object v0, Lhkm;->f:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhkm;->a:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lhkm;->d:Ljava/lang/Integer;

    const/4 v0, 0x0

    iput v0, p0, Lhkm;->e:I

    return-void
.end method

.method static a(Ljava/util/Map;IDZ)Ljava/util/List;
    .locals 8

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    int-to-double v0, p1

    div-double v3, v0, p2

    const-wide/16 v0, 0x0

    cmpl-double v0, v3, v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v1, 0x4

    const/16 v3, 0x64

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v6, v1

    div-double/2addr v6, v3

    double-to-int v1, v6

    new-instance v6, Lcom/google/android/gms/location/DetectedActivity;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v6, v0, v1}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    if-eqz p4, :cond_2

    sget-object v0, Lhkm;->f:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 7

    const/4 v2, 0x0

    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    iget-object v0, p0, Lhkm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    iget-object v5, p0, Lhkm;->d:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v5

    iget-object v6, p0, Lhkm;->d:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eq v5, v6, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v5

    add-int/2addr v1, v5

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move v0, v1

    move v1, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lhkm;->e:I

    rsub-int/lit8 v0, v0, 0x64

    int-to-double v4, v0

    invoke-static {v3, v1, v4, v5, v2}, Lhkm;->a(Ljava/util/Map;IDZ)Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lhkm;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/gms/location/DetectedActivity;

    iget-object v3, p0, Lhkm;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lhkm;->e:I

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_3
    sget-object v0, Lhkm;->f:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    iget-wide v2, p0, Lhkm;->b:J

    iget-wide v4, p0, Lhkm;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    return-object v0
.end method

.method public final a(J)Lhkm;
    .locals 0

    iput-wide p1, p0, Lhkm;->b:J

    return-object p0
.end method

.method public final a(Ljava/lang/Integer;I)Lhkm;
    .locals 0

    if-nez p1, :cond_0

    const/4 p2, 0x0

    :cond_0
    iput-object p1, p0, Lhkm;->d:Ljava/lang/Integer;

    iput p2, p0, Lhkm;->e:I

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lhkm;
    .locals 1

    iget-object v0, p0, Lhkm;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final b(J)Lhkm;
    .locals 0

    iput-wide p1, p0, Lhkm;->c:J

    return-object p0
.end method
