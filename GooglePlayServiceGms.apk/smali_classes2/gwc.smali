.class final Lgwc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lgwb;


# direct methods
.method constructor <init>(Lgwb;)V
    .locals 0

    iput-object p1, p0, Lgwc;->a:Lgwb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->a(Lgwb;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->a(Lgwb;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->b(Lgwb;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->c(Lgwb;)I

    move-result v0

    if-eq p3, v0, :cond_1

    iget-object v0, p0, Lgwc;->a:Lgwb;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lgwb;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->a(Lgwb;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwc;->a:Lgwb;

    invoke-static {v0}, Lgwb;->a(Lgwb;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    :cond_0
    return-void
.end method
