.class public final Lhki;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhqf;


# instance fields
.field final a:Lidu;

.field b:J

.field c:Z

.field d:Z

.field e:J

.field private f:Z

.field private g:Lhkk;

.field private h:J

.field private i:Ljava/util/List;

.field private j:J

.field private k:Lhqx;

.field private final l:Lhjf;

.field private final m:Lhkd;


# direct methods
.method public constructor <init>(Lidu;Lhjf;Lhkd;)V
    .locals 9

    const-wide/16 v3, 0x0

    const-wide/16 v1, -0x1

    const/4 v8, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Lhki;->b:J

    iput-boolean v8, p0, Lhki;->c:Z

    iput-boolean v8, p0, Lhki;->d:Z

    iput-boolean v8, p0, Lhki;->f:Z

    sget-object v0, Lhkk;->a:Lhkk;

    iput-object v0, p0, Lhki;->g:Lhkk;

    iput-wide v1, p0, Lhki;->h:J

    iput-wide v3, p0, Lhki;->e:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhki;->i:Ljava/util/List;

    iput-wide v3, p0, Lhki;->j:J

    iput-object p1, p0, Lhki;->a:Lidu;

    iput-object p2, p0, Lhki;->l:Lhjf;

    iput-object p3, p0, Lhki;->m:Lhkd;

    invoke-virtual {p2}, Lhjf;->c()J

    move-result-wide v0

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    sub-long v4, v2, v0

    const-wide/32 v6, 0x6cf2a0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    const-wide/32 v0, 0x6ddd00

    sub-long v0, v2, v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    :cond_0
    invoke-direct {p0, v0, v1, v8}, Lhki;->a(JZ)V

    return-void
.end method

.method private a(J)V
    .locals 12

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lhki;->j:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0x36ee80

    cmp-long v0, v3, v5

    if-gez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "SensorUploader"

    const-string v3, "Attempting an old session cleanup"

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput-wide p1, p0, Lhki;->j:J

    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->s()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Lhki;->a(Ljava/io/File;J)Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    :cond_4
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "SensorUploader"

    const-string v7, "%s removed: Session directory too old."

    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-static {v6}, Lilv;->a(Ljava/io/File;)Z

    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_7
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_9

    array-length v10, v9

    move v0, v2

    :goto_2
    if-ge v0, v10, :cond_9

    aget-object v11, v9, v0

    invoke-static {v11, v7, v8}, Lhki;->a(Ljava/io/File;J)Z

    move-result v11

    if-eqz v11, :cond_8

    move v0, v1

    goto :goto_1

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method private a(JZ)V
    .locals 7

    iput-wide p1, p0, Lhki;->h:J

    iget-wide v0, p0, Lhki;->h:J

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    if-eqz p3, :cond_0

    iget-object v2, p0, Lhki;->l:Lhjf;

    iget-object v3, p0, Lhki;->a:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->c()J

    move-result-wide v3

    add-long/2addr v3, p1

    iget-object v5, v2, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v2, v2, Lhjf;->c:Livi;

    const/4 v6, 0x3

    invoke-virtual {v2, v6, v3, v4}, Livi;->a(IJ)Livi;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "SensorUploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Next session upload attempt will be at: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, Lhki;->a:Lidu;

    invoke-interface {v5}, Lidu;->j()Licm;

    move-result-object v5

    invoke-interface {v5}, Licm;->c()J

    move-result-wide v5

    add-long/2addr v0, v5

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0
.end method

.method private static a(Ljava/io/File;J)Z
    .locals 10

    const-wide/32 v8, 0x36ee80

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    sub-long v4, p1, v2

    const-wide/32 v6, 0x240c8400

    cmp-long v6, v4, v6

    if-gtz v6, :cond_1

    add-long v6, p1, v8

    cmp-long v2, v2, v6

    if-gtz v2, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".lck"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    cmp-long v2, v4, v8

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    const-string v3, "SensorUploader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Found old lock file in directory. Age = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method private b(J)V
    .locals 5

    iget-wide v0, p0, Lhki;->b:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Resetting alarm to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lhki;->a:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->c()J

    move-result-wide v3

    add-long/2addr v3, p1

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-wide p1, p0, Lhki;->b:J

    iget-object v0, p0, Lhki;->a:Lidu;

    const/4 v1, 0x7

    iget-wide v2, p0, Lhki;->b:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    :cond_1
    return-void
.end method

.method private d()Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Lhki;->g()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SensorUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Conditions not good for collection. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhki;->m:Lhkd;

    invoke-virtual {v3}, Lhkd;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhki;->f()V

    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lhki;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lhki;->h:J

    const-wide/16 v6, 0x3a98

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    invoke-direct {p0}, Lhki;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lhkk;->c:Lhkk;

    iput-object v2, p0, Lhki;->g:Lhkk;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhki;->i:Ljava/util/List;

    iget-object v2, p0, Lhki;->a:Lidu;

    invoke-interface {v2}, Lidu;->s()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lhki;->i:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lhki;->e()V

    goto :goto_0

    :cond_4
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_5

    const-string v1, "SensorUploader"

    const-string v2, "WiFi not connected. Canceling upload attempt"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lhki;->f()V

    goto :goto_0

    :cond_6
    invoke-direct {p0, v4, v5}, Lhki;->b(J)V

    move v0, v1

    goto :goto_0
.end method

.method private e()V
    .locals 4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    const-string v1, "uploadNext()"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhki;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lhki;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhki;->g:Lhkk;

    sget-object v1, Lhkk;->c:Lhkk;

    if-ne v0, v1, :cond_4

    :cond_1
    :goto_0
    iget-object v0, p0, Lhki;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lhki;->i:Ljava/util/List;

    iget-object v1, p0, Lhki;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhru;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_1

    const-string v1, "SensorUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " locked."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "SensorUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Handling session: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lhki;->a:Lidu;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SensorUploader"

    invoke-interface {v1, v0, p0, v2}, Lidu;->a(Ljava/lang/String;Lhqf;Ljava/lang/String;)Lhqx;

    move-result-object v0

    iput-object v0, p0, Lhki;->k:Lhqx;

    iget-object v0, p0, Lhki;->k:Lhqx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhki;->k:Lhqx;

    invoke-interface {v0}, Lhqx;->a()V

    :goto_1
    return-void

    :cond_4
    invoke-direct {p0}, Lhki;->f()V

    goto :goto_1
.end method

.method private f()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    const-string v1, "Returning to the IDLE state"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhki;->k:Lhqx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhki;->k:Lhqx;

    invoke-interface {v0}, Lhqx;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhki;->k:Lhqx;

    :cond_1
    iget-object v0, p0, Lhki;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-boolean v0, p0, Lhki;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhki;->a:Lidu;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lidu;->b(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhki;->f:Z

    :cond_2
    sget-object v0, Lhkk;->a:Lhkk;

    iput-object v0, p0, Lhki;->g:Lhkk;

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lhki;->b(J)V

    return-void
.end method

.method private g()Z
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhki;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhki;->m:Lhkd;

    invoke-virtual {v0, v1}, Lhkd;->a(Z)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private h()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lhki;->a:Lidu;

    invoke-interface {v1}, Lidu;->s()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private i()Z
    .locals 1

    iget-boolean v0, p0, Lhki;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhki;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 4

    invoke-direct {p0}, Lhki;->g()Z

    move-result v0

    invoke-direct {p0}, Lhki;->i()Z

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "SensorUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Aborting upload. wifiConnected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhki;->m:Lhkd;

    invoke-virtual {v2}, Lhkd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lhki;->f()V

    :cond_2
    return-void
.end method


# virtual methods
.method final a()V
    .locals 14

    const-wide/32 v12, 0x6ddd00

    const-wide/16 v10, 0x1388

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lhki;->m:Lhkd;

    invoke-virtual {v0}, Lhkd;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhki;->g:Lhkk;

    sget-object v1, Lhkk;->a:Lhkk;

    if-eq v0, v1, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t schedule due to collectionPolicy. Returning to idle "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhki;->m:Lhkd;

    invoke-virtual {v2}, Lhkd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhki;->f()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhki;->a(J)V

    goto :goto_0

    :cond_2
    move v0, v1

    :cond_3
    iget-object v3, p0, Lhki;->g:Lhkk;

    sget-object v4, Lhkj;->a:[I

    iget-object v5, p0, Lhki;->g:Lhkk;

    invoke-virtual {v5}, Lhkk;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_1
    iget-object v4, p0, Lhki;->g:Lhkk;

    if-eq v3, v4, :cond_4

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_4

    const-string v4, "SensorUploader"

    const-string v5, "uploader state changed from %s to %s."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v1

    iget-object v3, p0, Lhki;->g:Lhkk;

    aput-object v3, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    if-nez v0, :cond_3

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lhki;->h:J

    add-long/2addr v6, v12

    invoke-direct {p0}, Lhki;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    cmp-long v0, v4, v6

    if-ltz v0, :cond_d

    invoke-direct {p0}, Lhki;->h()Z

    move-result v0

    if-nez v0, :cond_6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "SensorUploader"

    const-string v6, "No sessions found."

    invoke-static {v0, v6}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0, v4, v5, v2}, Lhki;->a(JZ)V

    move v0, v2

    goto :goto_1

    :cond_6
    iget-wide v6, p0, Lhki;->e:J

    sub-long v6, v4, v6

    cmp-long v0, v6, v10

    if-gez v0, :cond_8

    move v0, v2

    :goto_2
    if-eqz v0, :cond_9

    add-long v6, v4, v10

    invoke-direct {p0, v6, v7}, Lhki;->b(J)V

    :cond_7
    :goto_3
    invoke-direct {p0, v4, v5}, Lhki;->a(J)V

    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    iget-boolean v0, p0, Lhki;->f:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lhki;->a:Lidu;

    const/4 v4, 0x7

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Lidu;->a(ILilx;)V

    iget-boolean v0, p0, Lhki;->d:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->C()Z

    move-result v0

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_a

    const-string v4, "SensorUploader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Wifi reconnect "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_4
    iput-boolean v2, p0, Lhki;->f:Z

    :cond_b
    sget-object v0, Lhkk;->b:Lhkk;

    iput-object v0, p0, Lhki;->g:Lhkk;

    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v2}, Lhki;->a(JZ)V

    move v0, v2

    goto/16 :goto_1

    :cond_c
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_a

    const-string v0, "SensorUploader"

    const-string v4, "No WiFi reconnect needed since already on WiFi"

    invoke-static {v0, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_d
    cmp-long v0, v6, v4

    if-gtz v0, :cond_f

    iget-wide v6, p0, Lhki;->b:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    iget-wide v6, p0, Lhki;->b:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_7

    :cond_e
    add-long v6, v4, v12

    invoke-direct {p0, v6, v7}, Lhki;->b(J)V

    goto :goto_3

    :cond_f
    invoke-direct {p0, v6, v7}, Lhki;->b(J)V

    goto :goto_3

    :pswitch_1
    invoke-direct {p0}, Lhki;->d()Z

    move-result v0

    goto/16 :goto_1

    :pswitch_2
    iget-wide v4, p0, Lhki;->h:J

    const-wide/32 v6, 0x124f80

    add-long/2addr v4, v6

    iget-object v0, p0, Lhki;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-ltz v0, :cond_12

    move v0, v2

    :goto_5
    invoke-direct {p0}, Lhki;->g()Z

    move-result v6

    invoke-direct {p0}, Lhki;->i()Z

    move-result v7

    if-nez v0, :cond_10

    if-eqz v6, :cond_10

    if-nez v7, :cond_13

    :cond_10
    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_11

    const-string v4, "SensorUploader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Canceling upload. timeout: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " goodConditions: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " wifiConnected: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    invoke-direct {p0}, Lhki;->f()V

    move v0, v2

    goto/16 :goto_1

    :cond_12
    move v0, v1

    goto :goto_5

    :cond_13
    invoke-direct {p0, v4, v5}, Lhki;->b(J)V

    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lhsm;)V
    .locals 2

    iget v0, p1, Lhsm;->b:I

    if-nez v0, :cond_1

    iget v0, p1, Lhsm;->f:I

    if-nez v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    const-string v1, "Batch upload succesfully."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lhki;->e()V

    return-void

    :cond_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    const-string v1, "Batch upload failed partially."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to batch upload: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhki;->e()V

    return-void
.end method

.method public final a(Ljava/lang/String;Lhsm;)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SensorUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "File complete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " summary: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lhsm;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lhki;->j()V

    return-void
.end method

.method public final c()V
    .locals 0

    invoke-direct {p0}, Lhki;->j()V

    return-void
.end method
