.class public final Lfrp;
.super Lfrn;
.source "SourceFile"


# instance fields
.field private b:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Lbgn;

.field private k:Lfrq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lfrn;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lfrp;)Lbgn;
    .locals 1

    iget-object v0, p0, Lfrp;->j:Lbgn;

    return-object v0
.end method

.method static synthetic a(Lfrp;Lbgn;)Lbgn;
    .locals 0

    iput-object p1, p0, Lfrp;->j:Lbgn;

    return-object p1
.end method

.method static synthetic a(Lfrp;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lfrp;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lfrp;Lbbo;Lbgo;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lfrp;->a(Lbbo;Lbgo;Z)V

    return-void
.end method

.method static synthetic b(Lfrp;)Lfrq;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfrp;->k:Lfrq;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lfrp;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lfrp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrp;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iput-object v1, p0, Lfrp;->h:Ljava/lang/String;

    iput-object v1, p0, Lfrp;->j:Lbgn;

    :cond_1
    const/16 v0, 0x14

    iput v0, p0, Lfrp;->i:I

    iput-object p1, p0, Lfrp;->b:Ljava/lang/String;

    invoke-super {p0, p1}, Lfrn;->a(Ljava/lang/String;)Lfrn;

    :goto_0
    return-object p0

    :cond_2
    iget-object v0, p0, Lfrp;->k:Lfrq;

    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method protected final synthetic a(Lbbq;)V
    .locals 5

    check-cast p1, Lfaj;

    iget-object v0, p0, Lfrp;->k:Lfrq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrp;->k:Lfrq;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfrq;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lfrp;->k:Lfrq;

    :cond_0
    iget-object v0, p0, Lfrn;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lfrq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfrq;-><init>(Lfrp;B)V

    iput-object v0, p0, Lfrp;->k:Lfrq;

    iget-object v1, p0, Lfrp;->k:Lfrq;

    iget-object v2, p0, Lfrn;->e:Ljava/lang/String;

    iget-object v3, p0, Lfrn;->f:Ljava/lang/String;

    new-instance v0, Lfak;

    invoke-direct {v0}, Lfak;-><init>()V

    iget-object v4, p0, Lfrn;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lfak;->a(Ljava/lang/String;)Lfak;

    move-result-object v0

    iget v4, p0, Lfrp;->i:I

    invoke-virtual {v0, v4}, Lfak;->a(I)Lfak;

    move-result-object v0

    iget-object v4, p0, Lfrp;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lfak;->b(Ljava/lang/String;)Lfak;

    move-result-object v0

    iget-object v4, p1, Lfaj;->a:Lfch;

    if-nez v0, :cond_1

    sget-object v0, Lfak;->a:Lfak;

    :cond_1
    invoke-virtual {v4, v1, v2, v3, v0}, Lfch;->a(Lfay;Ljava/lang/String;Ljava/lang/String;Lfak;)V

    :cond_2
    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lfrp;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    invoke-virtual {p0}, Lfrp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrp;->b:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-virtual {p0, v0, v1}, Lfrp;->a(Ljava/lang/String;I)Lfrp;

    :cond_0
    return-void
.end method
