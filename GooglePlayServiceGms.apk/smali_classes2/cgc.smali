.class public final Lcgc;
.super Lcfl;
.source "SourceFile"


# instance fields
.field public final a:Lcfc;

.field public b:Ljava/lang/String;

.field public c:I

.field private final d:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcdu;Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcgc;-><init>(Lcdu;Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;I)V

    return-void
.end method

.method private constructor <init>(Lcdu;Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;I)V
    .locals 2

    invoke-static {}, Lcep;->a()Lcep;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lcgc;->a:Lcfc;

    const-string v0, "null payload"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcgc;->b:Ljava/lang/String;

    iput-object p4, p0, Lcgc;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput p5, p0, Lcgc;->c:I

    return-void
.end method

.method public static a(Lcfc;Lcdu;Landroid/database/Cursor;)Lcgc;
    .locals 7

    sget-object v0, Lceq;->a:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    iget-wide v2, p0, Lcfc;->b:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Cursor has account ID %d, but account parameter has SQL ID %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-wide v5, p0, Lcfc;->b:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    sget-object v0, Lceq;->b:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lceq;->c:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    long-to-int v5, v0

    sget-object v0, Lceq;->d:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    :cond_1
    new-instance v0, Lcgc;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcgc;-><init>(Lcdu;Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;I)V

    invoke-static {}, Lcep;->a()Lcep;

    move-result-object v1

    invoke-virtual {v1}, Lcep;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcgc;->d(J)V

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lceq;->a:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgc;->a:Lcfc;

    iget-wide v1, v1, Lcfc;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceq;->b:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceq;->c:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcgc;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "PendingOperation[account=%s, payload=%s, sqlId=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcgc;->a:Lcfc;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcgc;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcfl;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
