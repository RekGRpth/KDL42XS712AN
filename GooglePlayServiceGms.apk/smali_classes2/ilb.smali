.class public final Lilb;
.super Lila;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/AppOpsManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lila;-><init>()V

    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lilb;->a:Landroid/app/AppOpsManager;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lilb;->a:Landroid/app/AppOpsManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/AppOpsManager;->startOpNoThrow(Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lilb;->a:Landroid/app/AppOpsManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/AppOpsManager;->finishOp(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public final c(Ljava/lang/String;ILjava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lilb;->a:Landroid/app/AppOpsManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/AppOpsManager;->noteOpNoThrow(Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    return v0
.end method
