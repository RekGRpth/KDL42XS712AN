.class public final Lgoc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Landroid/os/Parcel;I)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->b()I

    move-result v2

    invoke-static {p1, v1, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-static {p1, v4, v1, p2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x6

    invoke-static {p1, v2, v4}, Lbkr;->a(Landroid/os/Parcel;II)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :cond_0
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/Boolean;)V

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/Boolean;)V

    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 13

    const/4 v10, 0x0

    invoke-static {p1}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v11

    const/4 v1, 0x0

    move-object v9, v10

    move-object v8, v10

    move-object v7, v10

    move-object v6, v10

    move-object v5, v10

    move-object v4, v10

    move-object v3, v10

    move-object v2, v10

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v11, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v12, 0xffff

    and-int/2addr v12, v0

    sparse-switch v12, :sswitch_data_0

    invoke-static {p1, v0}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p1, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v0}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :sswitch_3
    sget-object v4, Lcom/google/android/gms/common/people/data/Audience;->CREATOR:Lbkx;

    invoke-static {p1, v0, v4}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    move-object v4, v0

    goto :goto_0

    :sswitch_4
    sget-object v5, Lcom/google/android/gms/common/people/data/Audience;->CREATOR:Lbkx;

    invoke-static {p1, v0, v5}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    move-object v5, v0

    goto :goto_0

    :sswitch_5
    sget-object v6, Lcom/google/android/gms/plus/sharebox/Circle;->CREATOR:Lgoi;

    invoke-static {p1, v0, v6}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v6

    goto :goto_0

    :sswitch_6
    invoke-static {p1, v0}, Lbkp;->a(Landroid/os/Parcel;I)I

    move-result v0

    if-nez v0, :cond_0

    move-object v7, v10

    goto :goto_0

    :cond_0
    invoke-static {p1, v0}, Lbkp;->c(Landroid/os/Parcel;I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_0

    :sswitch_7
    invoke-static {p1, v0}, Lbkp;->e(Landroid/os/Parcel;I)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_0

    :sswitch_8
    invoke-static {p1, v0}, Lbkp;->e(Landroid/os/Parcel;I)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v11, :cond_2

    new-instance v0, Lbkq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method
