.class final Lhrt;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lhqg;


# instance fields
.field final synthetic a:Lhrp;

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Ljava/util/concurrent/locks/Condition;

.field private volatile d:I

.field private volatile e:Z

.field private final f:Lhrq;

.field private final g:Lhqs;

.field private volatile h:Z


# direct methods
.method public constructor <init>(Lhrp;Landroid/os/Looper;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lhrt;->a:Lhrp;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lhrt;->c:Ljava/util/concurrent/locks/Condition;

    iput v1, p0, Lhrt;->d:I

    iput-boolean v1, p0, Lhrt;->e:Z

    new-instance v0, Lhrq;

    invoke-direct {v0}, Lhrq;-><init>()V

    iput-object v0, p0, Lhrt;->f:Lhrq;

    new-instance v0, Lhqs;

    invoke-direct {v0}, Lhqs;-><init>()V

    iput-object v0, p0, Lhrt;->g:Lhqs;

    iput-boolean v1, p0, Lhrt;->h:Z

    return-void
.end method

.method private a(Livi;Lhrw;)V
    .locals 6

    const/4 v2, 0x3

    :try_start_0
    iget-object v0, p2, Lhrw;->b:Livi;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    iget-boolean v1, p2, Lhrw;->a:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Uploaded #%d/%s to MASF successfully."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lhrt;->a:Lhrp;

    invoke-static {v5}, Lhrp;->d(Lhrp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->a:Lhqq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->a:Lhqq;

    iget-object v2, p0, Lhrt;->a:Lhrp;

    invoke-static {v2}, Lhrp;->d(Lhrp;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lhrw;->b:Livi;

    invoke-interface {v1, v2, v0, v3}, Lhqq;->a(Ljava/lang/String;ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget v0, p0, Lhrt;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhrt;->d:I

    invoke-direct {p0}, Lhrt;->b()Z

    iget-object v0, p0, Lhrt;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :cond_2
    :try_start_1
    iget-object v1, p0, Lhrt;->g:Lhqs;

    invoke-virtual {v1}, Lhqs;->a()V

    iget-object v1, p0, Lhrt;->g:Lhqs;

    invoke-virtual {v1}, Lhqs;->b()I

    move-result v1

    if-le v1, v2, :cond_4

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Too many failures in last minutes. No future requests are allowed."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhrt;->h:Z

    :cond_4
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Failed to upload #%d: %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p2, Lhrw;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v2, p0, Lhrt;->a:Lhrp;

    invoke-static {v2}, Lhrp;->d(Lhrp;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v0, v2}, Lhrp;->a(Lhrp;Livi;Lhrw;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget v1, p0, Lhrt;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lhrt;->d:I

    invoke-direct {p0}, Lhrt;->b()Z

    iget-object v1, p0, Lhrt;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private b()Z
    .locals 1

    iget v0, p0, Lhrt;->d:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhrt;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lhrt;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrt;->e:Z

    invoke-direct {p0}, Lhrt;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrt;->a:Lhrp;

    iget-object v0, v0, Lhrp;->b:Limb;

    const-string v1, "Waiting for %d pending requests."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lhrt;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Livi;

    check-cast p2, Lhrw;

    const/4 v0, 0x2

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhrt;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    move-result v0

    const-string v1, "There is pending result before handler thread exits."

    invoke-static {v0, v1}, Lhsn;->b(ZLjava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized a(Livi;Livi;Z)Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-boolean v1, p0, Lhrt;->e:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Logical error: Task submission after shutting down."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :try_start_2
    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    :try_start_3
    iget-boolean v1, p0, Lhrt;->h:Z

    if-eqz v1, :cond_3

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Will not submit upload request: Too many server errors."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_2
    :try_start_4
    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_5
    iget v1, p0, Lhrt;->d:I

    const/16 v2, 0x14

    if-ge v1, v2, :cond_5

    const/4 v1, 0x1

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lhrt;->a:Lhrp;

    invoke-static {v2}, Lhrp;->c(Lhrp;)Lhrt;

    move-result-object v2

    iget-object v3, p0, Lhrt;->f:Lhrq;

    invoke-virtual {v3}, Lhrq;->a()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, Lhrt;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v2, p0, Lhrt;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhrt;->d:I

    iget-object v2, p0, Lhrt;->f:Lhrq;

    invoke-virtual {v2}, Lhrq;->b()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_4
    :try_start_6
    iget-object v0, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v1

    goto :goto_1

    :cond_5
    if-nez p3, :cond_7

    :try_start_7
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "Upload queue too long, dropping #%d request immediately."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual {p2, v5}, Livi;->c(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_6
    :try_start_8
    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :cond_7
    :try_start_9
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    const-string v2, "%d pending requests, waiting for available queue."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lhrt;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lhrt;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :try_start_a
    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lhrt;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Livi;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Livi;

    iget-object v4, p0, Lhrt;->a:Lhrp;

    iget-object v4, p0, Lhrt;->a:Lhrp;

    invoke-static {v4}, Lhrp;->d(Lhrp;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lhrp;->a(Livi;Livi;Ljava/lang/String;)Livi;

    move-result-object v1

    iget-boolean v0, p0, Lhrt;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lhrt;->a:Lhrp;

    invoke-static {v0}, Lhrp;->e(Lhrp;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Will not send to MASF: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lhrt;->h:Z

    if-eqz v0, :cond_2

    const-string v0, "Too many server errors."

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lhrw;

    invoke-direct {v2, v3, v6, v0}, Lhrw;-><init>(ZLivi;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lhrt;->a(Livi;Lhrw;)V

    goto :goto_0

    :cond_2
    const-string v0, "Interrupted by client."

    goto :goto_1

    :cond_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhrt;->a:Lhrp;

    iget-object v0, v0, Lhrp;->b:Limb;

    const-string v4, "Sending %d bytes to MASF in asynchronized way."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Livi;->e()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhrt;->a:Lhrp;

    invoke-static {v0}, Lhrp;->f(Lhrp;)Lhrd;

    move-result-object v0

    invoke-virtual {v0, v1, p0}, Lhrd;->a(Livi;Lhqg;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lhrw;

    const-string v2, "Can not send to MASF."

    invoke-direct {v0, v3, v6, v2}, Lhrw;-><init>(ZLivi;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lhrt;->a(Livi;Lhrw;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Livi;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lhrw;

    iget-object v4, p0, Lhrt;->a:Lhrp;

    invoke-static {v4}, Lhrp;->d(Lhrp;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    :goto_2
    const-string v3, "session ID should not be null in asynchronized mode."

    invoke-static {v2, v3}, Lhsn;->b(ZLjava/lang/Object;)V

    invoke-direct {p0, v1, v0}, Lhrt;->a(Livi;Lhrw;)V

    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto :goto_2

    :pswitch_2
    iget v0, p0, Lhrt;->d:I

    if-nez v0, :cond_9

    :goto_3
    const-string v0, "pending requests are not 0 before quiting."

    invoke-static {v2, v0}, Lhsn;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhrt;->a:Lhrp;

    invoke-static {v0}, Lhrp;->g(Lhrp;)Lhra;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhrt;->a:Lhrp;

    invoke-static {v0}, Lhrp;->g(Lhrp;)Lhra;

    move-result-object v0

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->d:Lhsd;

    invoke-virtual {v0, v1}, Lhra;->a(Lhsd;)V

    iget-object v0, p0, Lhrt;->a:Lhrp;

    invoke-static {v0}, Lhrp;->g(Lhrp;)Lhra;

    move-result-object v0

    invoke-virtual {v0}, Lhra;->d()V

    :cond_6
    iget-object v0, p0, Lhrt;->a:Lhrp;

    iget-object v0, v0, Lhrp;->a:Lhqq;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhrt;->a:Lhrp;

    iget-object v0, v0, Lhrp;->a:Lhqq;

    invoke-interface {v0}, Lhqq;->i()V

    :cond_7
    invoke-virtual {p0}, Lhrt;->getLooper()Landroid/os/Looper;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhrt;->a:Lhrp;

    iget-object v1, v1, Lhrp;->b:Limb;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " terminated."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    :cond_9
    move v2, v3

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
