.class public final Lcvq;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Ldou;

.field private final f:Ljava/util/Random;

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvq;->a:Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "request_sync_token"

    aput-object v2, v0, v1

    sput-object v0, Lcvq;->b:[Ljava/lang/String;

    sget-object v0, Lczx;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcvq;->c:[Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v2, Lczx;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcvq;->c:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sender_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lczx;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lczn;->a:[Ljava/lang/String;

    array-length v0, v0

    sget-object v2, Lcvq;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v0, v2

    sget-object v2, Lczz;->a:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v0, v2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcvq;->d:[Ljava/lang/String;

    sget-object v0, Lczn;->a:[Ljava/lang/String;

    sget-object v2, Lcvq;->d:[Ljava/lang/String;

    sget-object v3, Lczn;->a:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v0, Lcvq;->c:[Ljava/lang/String;

    sget-object v2, Lcvq;->d:[Ljava/lang/String;

    sget-object v3, Lczn;->a:[Ljava/lang/String;

    array-length v3, v3

    sget-object v4, Lczx;->a:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v0, Lczz;->a:[Ljava/lang/String;

    sget-object v2, Lcvq;->d:[Ljava/lang/String;

    sget-object v3, Lczn;->a:[Ljava/lang/String;

    array-length v3, v3

    sget-object v4, Lczx;->a:[Ljava/lang/String;

    array-length v4, v4

    add-int/2addr v3, v4

    sget-object v4, Lczz;->a:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;)V
    .locals 3

    const-string v0, "RequestAgent"

    sget-object v1, Lcvq;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcvq;->g:J

    new-instance v0, Ldou;

    invoke-direct {v0, p2}, Ldou;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvq;->e:Ldou;

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcvq;->f:Ljava/util/Random;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;JJLjava/util/ArrayList;)I
    .locals 7

    const/4 v1, -0x1

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-virtual {p3}, Ldom;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ldom;->d()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_0

    invoke-static {p2, v0}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {p8}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lcum;->a(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p3}, Ldom;->getInboundRequestInfo()Ldna;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ldna;->getSenderPlayer()Ldnz;

    move-result-object v4

    invoke-virtual {p8}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v4, v4, Lbni;->a:Landroid/content/ContentValues;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v5

    invoke-static {p1, p2, v4, v5, v6}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v2, Landroid/content/ContentValues;

    iget-object v4, p3, Lbni;->a:Landroid/content/ContentValues;

    invoke-direct {v2, v4}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    const-string v4, "external_game_id"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v4, "game_id"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p8}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {p2}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    if-ne v0, v1, :cond_1

    const-string v0, "sender_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :goto_2
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldjq;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v3, :cond_2

    new-instance v0, Landroid/content/ContentValues;

    iget-object v1, v3, Lbni;->a:Landroid/content/ContentValues;

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    const-string v1, "player_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "request_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    move v1, v4

    goto/16 :goto_0

    :cond_1
    const-string v1, "sender_id"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Ldom;->getOutboundRequestInfo()Ldnv;

    move-result-object v3

    const-string v0, "Request has no outbound or inbound info"

    invoke-static {v3, v0}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, p1

    move-object v1, p2

    move-object v5, p8

    invoke-static/range {v0 .. v5}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/net/Uri;Ldnv;ILjava/util/ArrayList;)V

    goto :goto_3

    :cond_3
    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    sget-object v1, Lcvq;->d:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v7

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Ldjp;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    new-instance v1, Lblt;

    invoke-direct {v1, v2}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v3, "type"

    move/from16 v0, p3

    invoke-virtual {v1, v3, v0}, Lblt;->a(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lblt;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lblt;->c:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p0

    move/from16 v6, p4

    invoke-static/range {v1 .. v6}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    new-instance v4, Ldkz;

    invoke-direct {v4, v1}, Ldkz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v4}, Ldkz;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, v1}, Lcvq;->a(Ljava/lang/String;Lcom/google/android/gms/games/request/GameRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcvr;

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v9

    iget-object v3, v2, Lcvr;->c:Ljava/util/Set;

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, v2, Lcvr;->b:Landroid/content/ContentValues;

    const-string v10, "player_count"

    iget-object v11, v2, Lcvr;->c:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->j()I

    move-result v3

    const/4 v10, 0x2

    if-ne v3, v10, :cond_2

    const-string v3, "wish_count"

    :goto_1
    iget-object v10, v2, Lcvr;->b:Landroid/content/ContentValues;

    invoke-virtual {v10, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    iget-object v11, v2, Lcvr;->b:Landroid/content/ContentValues;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v11, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->l()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Lcvr;->a(J)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcvr;->a:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Lcvr;->b:Landroid/content/ContentValues;

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/PlayerRef;->a(Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    :cond_1
    invoke-virtual {v8, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v4}, Ldkz;->b()V

    throw v1

    :cond_2
    :try_start_1
    const-string v3, "gift_count"

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/games/GameRef;->a(Lcom/google/android/gms/games/Game;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v2, "sender_"

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/PlayerRef;->a(Ljava/lang/String;Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->j()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    move v3, v2

    :goto_2
    const-string v11, "wish_count"

    if-eqz v3, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "gift_count"

    if-eqz v3, :cond_6

    const/4 v2, 0x0

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "expiration_timestamp"

    invoke-interface {v1}, Lcom/google/android/gms/games/request/GameRequest;->l()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v1, Lcvr;

    invoke-direct {v1, v10, p2}, Lcvr;-><init>(Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v2, v1, Lcvr;->c:Ljava/util/Set;

    invoke-interface {v2, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v2, "player_count"

    iget-object v3, v1, Lcvr;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v8, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    move v3, v2

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    const/4 v2, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v4}, Ldkz;->b()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcvr;

    iget-object v1, v1, Lcvr;->b:Landroid/content/ContentValues;

    invoke-virtual {v7, v1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    goto :goto_5

    :cond_8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    move/from16 v0, p4

    invoke-static {v1, v0}, Lcum;->a(II)I

    move-result v1

    invoke-virtual {v7, v1}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    add-int/lit8 v0, p5, -0x1

    and-int/2addr v0, p5

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Request type must be a single bit value!"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-static {p1}, Ldjp;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Lblt;

    invoke-direct {v3, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v0, "type"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "external_game_id"

    invoke-virtual {v3, v0, p2}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    packed-switch p6, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sort order "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const-string v4, "expiration_timestamp ASC"

    :goto_1
    invoke-static {p0, p1, p3}, Lcum;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v0

    packed-switch p4, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request direction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v4, "sender_is_in_circles DESC, CASE WHEN sender_is_in_circles=0 THEN next_expiring_request ELSE NULL END ASC, CASE WHEN sender_is_in_circles=0 THEN sender_id ELSE NULL END,expiration_timestamp ASC"

    goto :goto_1

    :pswitch_2
    const-string v2, "player_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    iget-object v1, v3, Lblt;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Lblt;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lblt;->c:[Ljava/lang/String;

    move-object v0, p0

    move v5, p7

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :pswitch_3
    const-string v2, "sender_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v5, Ldoq;

    iget-object v3, p0, Lcvq;->f:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    packed-switch p6, :pswitch_data_0

    const-string v3, "RequestUpdateType"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unknown request update state: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "UNKNOWN_UPDATE"

    :goto_1
    invoke-direct {v5, v1, v6, v3}, Ldoq;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v3, 0x19

    if-lt v1, v3, :cond_16

    new-instance v1, Ldor;

    move-object/from16 v0, p3

    invoke-direct {v1, v2, v0}, Ldor;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    move-object v2, v1

    goto :goto_0

    :pswitch_0
    const-string v3, "ACCEPT"

    goto :goto_1

    :pswitch_1
    const-string v3, "DISMISS"

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Ldor;

    move-object/from16 v0, p3

    invoke-direct {v1, v2, v0}, Ldor;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v7, 0x0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v8, v1

    :goto_3
    if-ge v8, v11, :cond_2

    :try_start_0
    iget-object v1, p0, Lcvq;->e:Ldou;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldor;

    const-string v4, "requests"

    iget-object v1, v1, Ldou;->a:Lbmi;

    const/4 v3, 0x2

    const-class v6, Ldot;

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldot;

    invoke-virtual {v1}, Ldot;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v7

    :goto_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v7, v1

    goto :goto_3

    :catch_0
    move-exception v1

    const-string v2, "RequestAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to send an update: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_4

    :cond_2
    new-instance v4, Ldey;

    invoke-direct {v4}, Ldey;-><init>()V

    const/4 v1, 0x0

    iput v1, v4, Ldey;->b:I

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v1, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    :cond_3
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldos;

    invoke-virtual {v1}, Ldos;->getUpdatedRequest()Ldom;

    move-result-object v9

    invoke-virtual {v1}, Ldos;->c()Ljava/lang/String;

    move-result-object v3

    const-string v10, "SUCCESS"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v3, 0x0

    :goto_6
    if-nez v3, :cond_e

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10, v3}, Ldey;->a(Ljava/lang/String;I)Ldey;

    packed-switch p6, :pswitch_data_1

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v9, v0}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_4
    const-string v10, "ALREADY_UPDATED"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v3, 0x1

    goto :goto_6

    :cond_5
    const-string v10, "NOT_FOUND"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v3, 0x1

    goto :goto_6

    :cond_6
    const-string v10, "INVALID_ID"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v3, 0x1

    goto :goto_6

    :cond_7
    const-string v10, "INVALID_OPERATION"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    const/4 v3, 0x1

    goto :goto_6

    :cond_8
    const-string v10, "DUPLICATE_UPDATE"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    const/4 v3, 0x1

    goto :goto_6

    :cond_9
    const-string v10, "INCONSISTENT_UPDATE"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    const/4 v3, 0x1

    goto :goto_6

    :cond_a
    const-string v10, "MALFORMED"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    const/4 v3, 0x1

    goto :goto_6

    :cond_b
    const-string v10, "PERMISSION_DENIED"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    const/4 v3, 0x1

    goto :goto_6

    :cond_c
    const-string v10, "REQUEST_EXPIRED"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const/4 v3, 0x3

    goto :goto_6

    :cond_d
    const-string v10, "RequestUpdateResultOutcome"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unknown outcome type string: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v10, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, -0x1

    goto/16 :goto_6

    :pswitch_2
    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v3, v1, v9, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_5

    :cond_e
    const/4 v9, 0x3

    if-ne v3, v9, :cond_f

    const-string v3, "RequestAgent"

    const-string v9, "Request %s was already expired; deleting."

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v3, v10, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v4, v1, v3}, Ldey;->a(Ljava/lang/String;I)Ldey;

    goto/16 :goto_5

    :cond_f
    const-string v3, "RequestAgent"

    const-string v9, "Failed to update request %s: %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual {v1}, Ldos;->c()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ldos;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v4, v1, v3}, Ldey;->a(Ljava/lang/String;I)Ldey;

    goto/16 :goto_5

    :cond_10
    if-nez v2, :cond_11

    if-eqz v7, :cond_11

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_7
    return-object v1

    :cond_11
    if-ge v6, v5, :cond_13

    const/16 v1, 0x7d0

    iput v1, v4, Ldey;->b:I

    const/4 v1, 0x0

    move v2, v1

    :goto_8
    if-ge v2, v5, :cond_14

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, v4, Ldey;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const/4 v3, 0x2

    invoke-virtual {v4, v1, v3}, Ldey;->a(Ljava/lang/String;I)Ldey;

    :cond_12
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    :cond_13
    if-ge v2, v6, :cond_14

    if-nez v2, :cond_15

    const/16 v1, 0x7d1

    iput v1, v4, Ldey;->b:I

    :cond_14
    :goto_9
    invoke-virtual {v4}, Ldey;->a()Ldex;

    move-result-object v1

    invoke-static {v1}, Ldex;->a(Ldex;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_7

    :cond_15
    const/16 v1, 0x7d0

    iput v1, v4, Ldey;->b:I

    goto :goto_9

    :cond_16
    move-object v1, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    invoke-static {p1}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcvq;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;)Ljava/util/HashMap;
    .locals 5

    iget-object v2, p2, Lcvs;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldon;

    invoke-virtual {v0}, Ldon;->getRequest()Ldom;

    move-result-object v0

    invoke-virtual {v0}, Ldom;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, v4}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;J)V
    .locals 12

    const/4 v6, 0x0

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcwh;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const-wide/16 v7, -0x1

    const-string v11, "1"

    invoke-static {p1}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Lblt;

    invoke-direct {v0, v1}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v2, "status"

    invoke-virtual {v0, v2, v11}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sender_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcvt;->a:[Ljava/lang/String;

    invoke-virtual {v0}, Lblt;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lblt;->c:[Ljava/lang/String;

    const-string v5, "game_id,creation_timestamp DESC"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v5

    move v0, v6

    move-wide v1, v7

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    cmp-long v7, v3, v1

    if-eqz v7, :cond_1

    move v0, v6

    move-wide v1, v3

    :cond_1
    if-ge v0, v10, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p1}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    new-instance v6, Lblt;

    invoke-direct {v6, v1}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v7, "status"

    invoke-virtual {v6, v7, v11}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "creation_timestamp"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "<=?"

    invoke-virtual {v6, v7, v4, v5}, Lblt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "game_id"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v4, v0}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sender_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v6}, Lblt;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v4, v6, Lblt;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v2, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_5
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/net/Uri;Ldnv;ILjava/util/ArrayList;)V
    .locals 9

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-virtual {p3}, Ldnv;->getRecipients()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldoo;

    invoke-virtual {v0}, Ldoo;->getRecipientPlayer()Ldnz;

    move-result-object v5

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v5, v5, Lbni;->a:Landroid/content/ContentValues;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v7

    invoke-static {p0, p1, v5, v7, v8}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p5, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    iget-object v0, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "player_id"

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "request_id"

    invoke-virtual {v0, v5, p4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;Z)V
    .locals 12

    invoke-static {p0, p1}, Lcvq;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ldjm;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p2, Lcvs;->a:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v0

    :goto_0
    if-ge v2, v7, :cond_4

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldon;

    invoke-virtual {v0}, Ldon;->getRequest()Ldom;

    move-result-object v8

    invoke-virtual {v8}, Ldom;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ldom;->b()Ljava/lang/String;

    move-result-object v10

    if-eqz p3, :cond_2

    invoke-virtual {v8}, Ldom;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v11, 0x3e8

    if-ne v1, v11, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {p0, p1, v10, v9, v1}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "RequestAgent"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v11, "Notification "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " consumed by listener for game "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ". Deleting."

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v9, v1, v8

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    const-string v9, "external_sub_id=?"

    invoke-virtual {v8, v9, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ldon;->getNotification()Ldmt;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x6

    invoke-virtual {v0}, Ldmt;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v8, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ldon;->getNotification()Ldmt;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v9, v0, v1

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v10, "external_sub_id=?"

    invoke-virtual {v1, v10, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v8}, Ldom;->getInboundRequestInfo()Ldna;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_3
    const-string v8, "image_id"

    invoke-virtual {v1, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v4, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_5
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;J)Z
    .locals 19

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcvs;->b:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-static/range {p2 .. p2}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "request_sync_token"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static/range {p1 .. p3}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;)Ljava/util/HashMap;

    move-result-object v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lcvs;->a:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    move v15, v4

    :goto_0
    move/from16 v0, v18

    if-ge v15, v0, :cond_3

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Ldon;

    invoke-virtual {v13}, Ldon;->getRequest()Ldom;

    move-result-object v7

    invoke-virtual {v7}, Ldom;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Ljava/lang/Long;

    if-nez v14, :cond_2

    const-string v4, "RequestAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "No game found matching external game ID "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    goto :goto_0

    :cond_2
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-direct/range {v4 .. v12}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;JJLjava/util/ArrayList;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-virtual {v13}, Ldon;->getNotification()Ldmt;

    move-result-object v6

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual {v7}, Ldom;->c()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide v7, v13

    invoke-static/range {v4 .. v10}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmt;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v4, 0x1

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "RequestAgent"

    invoke-static {v4, v12, v5}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v4

    :cond_4
    if-nez v4, :cond_5

    const-string v5, "RequestAgent"

    const-string v6, "Failed to store requests"

    invoke-static {v5, v6}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-wide/from16 v2, p4

    invoke-static {v0, v1, v2, v3}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;J)V

    return v4
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;Ljava/lang/String;)Z
    .locals 11

    const-wide/16 v2, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p3}, Ldom;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-nez v1, :cond_0

    const-string v1, "RequestAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No game found matching external game ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2, p4}, Lcum;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_1

    const-string v0, "RequestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No player found matching external player ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    goto :goto_0

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ldom;->d()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    invoke-virtual {p3}, Ldom;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v8, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move v0, v10

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;JJLjava/util/ArrayList;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    move v0, v9

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "RequestAgent"

    invoke-static {v1, v8, v2}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    if-nez v0, :cond_4

    const-string v0, "RequestAgent"

    const-string v1, "Failed to store data for request"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    goto/16 :goto_0

    :cond_4
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    const-string v0, "RequestAgent"

    const-string v1, "Failed to store data for newly created entity"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    goto/16 :goto_0

    :cond_5
    move v0, v10

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p3}, Ldjp;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcux;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/games/request/GameRequest;)Z
    .locals 2

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvs;
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_0
    :try_start_0
    invoke-static {p2}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v0, p0, Lcvq;->e:Ldou;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ANDROID"

    const/4 v4, 0x0

    invoke-static {v2, v4, v3, p4, p3}, Ldou;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldou;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldop;

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldop;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ldop;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0}, Ldop;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ldop;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1, p2, v2, p4}, Lcvq;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvs;

    move-result-object v0

    iget v6, v0, Lcvs;->c:I

    if-nez v6, :cond_0

    iget-object v2, v0, Lcvs;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, v0, Lcvs;->b:Ljava/lang/String;

    move-object v2, v0

    :cond_0
    if-nez v1, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    new-instance v1, Lcvs;

    invoke-direct {v1, v0, v2, v6}, Lcvs;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_3
    return-object v0

    :catch_0
    move-exception v0

    const/16 v1, 0x19a

    invoke-static {v0, v1}, Lbng;->a(Lsp;I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {p2}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "request_sync_token"

    invoke-virtual {v1, v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldjr;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "RequestAgent"

    invoke-static {v1, v0, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    const-string v0, "RequestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid. Retrying with no token."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p3, v7

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "RequestAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_2
    new-instance v0, Lcvs;

    invoke-direct {v0}, Lcvs;-><init>()V

    goto :goto_3

    :cond_3
    move v0, v6

    goto/16 :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 5

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Ldkz;

    invoke-static {p1}, Ldjp;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {p0, v0, v3}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Ldkz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v2}, Ldkz;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldkz;->b()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Ldkz;->b()V

    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcvq;->g:J

    sub-long v1, v0, v2

    sget-object v0, Lcwh;->q:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-gtz v0, :cond_1

    const-string v0, "RequestAgent"

    const-string v1, "Returning cached entities"

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-static {p1, p2, p3}, Lcum;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_2

    const-string v0, "RequestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find local player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while syncing!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v6

    goto :goto_0

    :cond_2
    invoke-static {p1, p2}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lcvq;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvs;

    move-result-object v3

    const-string v0, "RequestAgent"

    const-string v1, "Received %s requests during sync"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v8, v3, Lcvs;->a:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, v3, Lcvs;->c:I

    if-eqz v0, :cond_3

    iget v7, v3, Lcvs;->c:I

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;J)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p4, :cond_4

    move v0, v6

    :goto_1
    invoke-static {p1, p2, v3, v0}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvs;Z)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcvq;->g:J

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v0, 0x0

    if-nez p7, :cond_0

    move-object v3, v0

    :goto_0
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    move-object v2, v0

    :goto_1
    new-instance v0, Ldpn;

    iget-object v1, p0, Lcvq;->f:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p5}, Lded;->a(I)Ljava/lang/String;

    move-result-object v6

    move-object v1, p3

    move-object v4, p8

    invoke-direct/range {v0 .. v6}, Ldpn;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Long;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcvq;->e:Ldou;

    const-string v4, "requests/send"

    iget-object v1, v1, Ldou;->a:Lbmi;

    const/4 v3, 0x2

    const-class v6, Ldom;

    move-object v2, p2

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldom;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0, p1, p2, v0, p4}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldom;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    invoke-static {p7}, Lbpd;->b([B)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RequestAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to send a request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ldom;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Ldjp;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v2, p6

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcvq;->g:J

    return-void
.end method

.method public final bridge synthetic a([Lcve;)V
    .locals 0

    invoke-super {p0, p1}, Lcve;->a([Lcve;)V

    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcvq;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b([Lcve;)V
    .locals 0

    invoke-super {p0, p1}, Lcve;->b([Lcve;)V

    return-void
.end method

.method public final bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcve;->c()V

    return-void
.end method

.method public final bridge synthetic d()V
    .locals 0

    invoke-super {p0}, Lcve;->d()V

    return-void
.end method

.method public final bridge synthetic e()Z
    .locals 1

    invoke-super {p0}, Lcve;->e()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lcve;->f()V

    return-void
.end method

.method public final bridge synthetic g()V
    .locals 0

    invoke-super {p0}, Lcve;->g()V

    return-void
.end method
