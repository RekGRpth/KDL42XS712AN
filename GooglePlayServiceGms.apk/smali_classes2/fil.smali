.class abstract Lfil;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Lfdx;

.field protected final c:Ljava/util/Set;

.field final synthetic d:Lfie;


# direct methods
.method public constructor <init>(Lfie;)V
    .locals 1

    iput-object p1, p0, Lfil;->d:Lfie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfdx;

    invoke-direct {v0}, Lfdx;-><init>()V

    iput-object v0, p0, Lfil;->b:Lfdx;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfil;->c:Ljava/util/Set;

    invoke-static {p1}, Lfie;->f(Lfie;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfil;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V
    .locals 7

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->f(Lfie;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lfhr;->b(Ljava/util/List;)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lfil;->d:Lfie;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    iget-object v5, p0, Lfil;->c:Ljava/util/Set;

    iget-object v6, p0, Lfil;->b:Lfdx;

    invoke-virtual {v4, v0, v5, v6}, Lfie;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Set;Lfdx;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lfil;->b()V

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method protected abstract b(Ljava/lang/String;)Ljava/util/List;
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method public final d()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->f(Lfie;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lfil;->c()V

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->d(Lfie;)Lffh;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v2, v1}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->d(Lfie;)Lffh;

    move-result-object v0

    invoke-virtual {v0}, Lffh;->b()V

    goto :goto_0
.end method

.method protected final e()V
    .locals 14

    const-wide/16 v12, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    iget-object v0, p0, Lfil;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lfil;->b:Lfdx;

    invoke-virtual {v7, v0}, Lfdx;->a(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {p0, v0}, Lfil;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move v5, v3

    :goto_2
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v0, v5}, Lfdx;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfho;

    iget-object v2, v2, Lfho;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    :cond_2
    invoke-static {v1}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfil;->d:Lfie;

    invoke-static {v2}, Lfie;->a(Lfie;)Lfip;

    move-result-object v2

    invoke-virtual {v2, v1}, Lfip;->l(Ljava/lang/String;)V

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->c(Lfie;)Landroid/content/SyncResult;

    move-result-object v1

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v1, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v10, v12

    iput-wide v10, v1, Landroid/content/SyncStats;->numDeletes:J

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->b(Lfie;)Lfid;

    move-result-object v1

    iget v2, v1, Lfid;->q:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lfid;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->a(Lfie;)Lfip;

    move-result-object v1

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_3
    :try_start_1
    invoke-virtual {p0, v0}, Lfil;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lfil;->b:Lfdx;

    invoke-virtual {v1, v0}, Lfdx;->a(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v1, v0, Lfid;->v:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->v:I

    goto/16 :goto_0

    :cond_4
    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_0

    iget-object v1, p0, Lfil;->b:Lfdx;

    invoke-virtual {v1, v0, v2}, Lfdx;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfho;

    iget-object v7, p0, Lfil;->d:Lfie;

    invoke-static {v7}, Lfie;->a(Lfie;)Lfip;

    move-result-object v7

    iget-object v8, v1, Lfho;->b:Ljava/lang/String;

    iget-object v9, v1, Lfho;->a:Ljava/lang/String;

    iget v1, v1, Lfho;->c:I

    invoke-virtual {v7, v0, v8, v9, v1}, Lfip;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->c(Lfie;)Landroid/content/SyncResult;

    move-result-object v1

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v7, v1, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v7, v12

    iput-wide v7, v1, Landroid/content/SyncStats;->numInserts:J

    add-int/lit8 v1, v4, 0x1

    const/16 v4, 0x32

    if-le v1, v4, :cond_5

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->a(Lfie;)Lfip;

    move-result-object v1

    invoke-virtual {v1}, Lfip;->r()V

    iget-object v1, p0, Lfil;->d:Lfie;

    invoke-static {v1}, Lfie;->e(Lfie;)V

    move v1, v3

    :cond_5
    add-int/lit8 v2, v2, 0x1

    move v4, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->s()V

    iget-object v0, p0, Lfil;->d:Lfie;

    invoke-static {v0}, Lfie;->e(Lfie;)V

    return-void
.end method
