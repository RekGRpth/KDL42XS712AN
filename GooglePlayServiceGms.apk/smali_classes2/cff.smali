.class final Lcff;
.super Lcfl;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field final c:J

.field private final d:Lcfc;


# direct methods
.method public constructor <init>(Lcdu;Lcfc;JLcom/google/android/gms/drive/auth/AppIdentity;J)V
    .locals 2

    invoke-static {}, Lcde;->a()Lcde;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lcff;->d:Lcfc;

    iput-wide p3, p0, Lcff;->a:J

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcff;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-wide p6, p0, Lcff;->c:J

    return-void
.end method

.method public static a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcff;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcdf;->a:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v5, p1, Lcfc;->b:J

    cmp-long v0, v5, v3

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v6, "Account ID from cursor (%d) does not match ID of account passed to cursor (%d)."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v7, v2

    iget-wide v2, p1, Lcfc;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Lcdf;->b:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sget-object v0, Lcdf;->d:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcdf;->e:Lcdf;

    invoke-virtual {v1}, Lcdf;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v5

    sget-object v0, Lcdf;->c:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    new-instance v0, Lcff;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcff;-><init>(Lcdu;Lcfc;JLcom/google/android/gms/drive/auth/AppIdentity;J)V

    invoke-static {}, Lcde;->a()Lcde;

    move-result-object v1

    invoke-virtual {v1}, Lcde;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcff;->d(J)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcdf;->a:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcff;->d:Lcfc;

    iget-wide v1, v1, Lcfc;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdf;->b:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcff;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdf;->d:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcff;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcdf;->e:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcff;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcdf;->c:Lcdf;

    invoke-virtual {v0}, Lcdf;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcff;->c:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "AuthenticatedApp [appId=%s, appIdentity=%s, scope=%s, expiryTimestamp=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcff;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcff;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcff;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
