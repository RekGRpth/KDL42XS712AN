.class public final Lhnq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:J

.field static final b:Livi;


# instance fields
.field final c:Lidu;

.field final d:Lhof;

.field final e:Lhoj;

.field final f:Lids;

.field final g:Ljava/util/List;

.field final h:Ljava/util/List;

.field i:I

.field j:I

.field k:I

.field l:Z

.field m:Lhns;

.field private n:Lilx;

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-wide/32 v2, 0x2932e00

    const/4 v4, -0x1

    const-wide/32 v0, 0x5265c00

    invoke-static {v2, v3, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sput-wide v0, Lhnq;->a:J

    new-instance v0, Livi;

    sget-object v1, Lihj;->j:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    sput-object v0, Lhnq;->b:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Livi;->e(II)Livi;

    sget-object v0, Lhnq;->b:Livi;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Livi;->e(II)Livi;

    return-void
.end method

.method public constructor <init>(Lidu;Lhof;Lhoj;Lids;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhnq;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhnq;->h:Ljava/util/List;

    iput v1, p0, Lhnq;->i:I

    iput v1, p0, Lhnq;->j:I

    iput v1, p0, Lhnq;->k:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhnq;->l:Z

    sget-object v0, Lhns;->a:Lhns;

    iput-object v0, p0, Lhnq;->m:Lhns;

    const/4 v0, 0x0

    iput-object v0, p0, Lhnq;->n:Lilx;

    iput-object p1, p0, Lhnq;->c:Lidu;

    iput-object p2, p0, Lhnq;->d:Lhof;

    iput-object p3, p0, Lhnq;->e:Lhoj;

    iput-object p4, p0, Lhnq;->f:Lids;

    return-void
.end method

.method private static a(JZ)J
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    const-wide v2, 0x41224f8000000000L    # 600000.0

    mul-double/2addr v0, v2

    double-to-long v2, v0

    if-eqz p2, :cond_0

    const-wide/32 v0, 0x5265c00

    :goto_0
    add-long/2addr v0, p0

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    sget-wide v0, Lhnq;->a:J

    goto :goto_0
.end method

.method private static a(JI)Livi;
    .locals 3

    new-instance v0, Livi;

    sget-object v1, Lihj;->f:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p0, p1}, Livi;->a(IJ)Livi;

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    if-lez p2, :cond_0

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p2}, Livi;->e(II)Livi;

    :cond_0
    return-object v0
.end method

.method private a(Z)V
    .locals 3

    iget-object v0, p0, Lhnq;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-object v2, p0, Lhnq;->d:Lhof;

    invoke-virtual {v2, v0, v1, p1}, Lhof;->a(JZ)V

    iget-object v2, p0, Lhnq;->e:Lhoj;

    invoke-virtual {v2, v0, v1, p1}, Lhoj;->a(JZ)V

    return-void
.end method

.method private b()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CacheUpdater"

    const-string v1, "Failed to get latest NlpParameters."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lhns;->a:Lhns;

    iput-object v0, p0, Lhnq;->m:Lhns;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhnq;->a(Z)V

    invoke-direct {p0}, Lhnq;->h()V

    invoke-direct {p0}, Lhnq;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Lhnq;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhnq;->c:Lidu;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lidu;->b(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhnq;->l:Z

    :cond_0
    return-void
.end method

.method private d()V
    .locals 14

    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    iput-boolean v4, p0, Lhnq;->o:Z

    iget-object v0, p0, Lhnq;->d:Lhof;

    invoke-virtual {v0}, Lhof;->d()Lhou;

    move-result-object v0

    iget-object v1, p0, Lhnq;->d:Lhof;

    invoke-virtual {v1}, Lhof;->c()Lhou;

    move-result-object v2

    iget-object v1, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput v4, p0, Lhnq;->k:I

    iget-object v1, p0, Lhnq;->c:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->b()J

    move-result-wide v5

    if-eqz v0, :cond_2

    iget-object v1, p0, Lhnq;->f:Lids;

    invoke-virtual {v1}, Lids;->d()I

    move-result v7

    iget-object v0, v0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhno;

    iget v1, v1, Lhno;->c:I

    if-ge v1, v7, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhtf;->a(Ljava/lang/String;)Livi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    iput-wide v5, v0, Lhno;->a:J

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhnq;->f:Lids;

    invoke-virtual {v0}, Lids;->e()I

    move-result v7

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    if-eqz v2, :cond_5

    iget-object v0, v2, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhno;

    iget-object v2, v1, Lhno;->d:Ljava/lang/Object;

    check-cast v2, Lhuq;

    iget v1, v1, Lhno;->c:I

    if-lt v1, v7, :cond_3

    invoke-virtual {v2}, Lhuq;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2}, Lhuq;->e()I

    move-result v2

    iget-object v10, p0, Lhnq;->h:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lhnq;->a(JI)Livi;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    iput-wide v5, v0, Lhno;->a:J

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhnq;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v5

    iget-object v0, p0, Lhnq;->e:Lhoj;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhnq;->e:Lhoj;

    iget-object v0, v0, Lhoj;->b:Lhok;

    invoke-virtual {v0}, Lhok;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhol;

    iget-object v11, v1, Lhol;->a:Livi;

    invoke-virtual {v11, v13}, Livi;->i(I)Z

    move-result v11

    if-eqz v11, :cond_7

    iget-object v1, v1, Lhol;->a:Livi;

    invoke-virtual {v1, v13}, Livi;->c(I)I

    move-result v1

    :goto_3
    if-ge v1, v7, :cond_8

    move v1, v3

    :goto_4
    if-eqz v1, :cond_9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_9

    iget-object v0, p0, Lhnq;->h:Ljava/util/List;

    invoke-static {v9, v10, v4}, Lhnq;->a(JI)Livi;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    const/4 v1, -0x1

    goto :goto_3

    :cond_8
    move v1, v4

    goto :goto_4

    :cond_9
    if-nez v1, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    invoke-virtual {v0, v5, v6}, Lhol;->a(J)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lhnq;->f:Lids;

    invoke-virtual {v0}, Lids;->p()I

    move-result v0

    iput v0, p0, Lhnq;->i:I

    iget-object v0, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    int-to-double v5, v1

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v5, v7

    iget v0, p0, Lhnq;->i:I

    int-to-double v7, v0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v0, v5

    iput v0, p0, Lhnq;->j:I

    iget-object v0, p0, Lhnq;->d:Lhof;

    invoke-virtual {v0}, Lhof;->e()Lhod;

    move-result-object v0

    invoke-interface {v0}, Lhod;->a()Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    iget v2, p0, Lhnq;->j:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lhnq;->j:I

    :cond_b
    if-lez v1, :cond_c

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_c

    const-string v1, "CacheUpdater"

    const-string v2, "Will refresh %d cell and %d WIFI (stats: %b) using %d RPCs"

    new-array v5, v12, [Ljava/lang/Object;

    iget-object v6, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v4, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v3

    const/4 v3, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v3

    const/4 v0, 0x3

    iget v3, p0, Lhnq;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    invoke-direct {p0}, Lhnq;->f()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0}, Lhnq;->e()V

    iget-object v0, p0, Lhnq;->c:Lidu;

    iget-object v1, p0, Lhnq;->c:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    iget v3, p0, Lhnq;->j:I

    int-to-long v3, v3

    const-wide/16 v5, 0x2710

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    iget-object v3, p0, Lhnq;->n:Lilx;

    invoke-interface {v0, v12, v1, v2, v3}, Lidu;->a(IJLilx;)V

    sget-object v0, Lhns;->c:Lhns;

    iput-object v0, p0, Lhnq;->m:Lhns;

    :cond_d
    return-void

    :cond_e
    move v0, v4

    goto :goto_5
.end method

.method private e()V
    .locals 3

    iget-boolean v0, p0, Lhnq;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhnq;->c:Lidu;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lidu;->a(ILilx;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhnq;->l:Z

    :cond_0
    return-void
.end method

.method private f()Z
    .locals 13

    const/4 v4, 0x0

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lhnq;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v6

    iget-object v0, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f

    new-instance v3, Livi;

    sget-object v0, Lihj;->k:Livk;

    invoke-direct {v3, v0}, Livi;-><init>(Livk;)V

    sget-object v0, Lhnq;->b:Livi;

    invoke-virtual {v3, v5, v0}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v3, v11, v6, v7}, Livi;->a(IJ)Livi;

    iget-object v0, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhnq;->i:I

    if-ge v1, v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    invoke-virtual {v3, v12, v0}, Livi;->a(ILivi;)V

    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v1

    move-object v1, v3

    :goto_1
    iget v3, p0, Lhnq;->i:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v4, Livi;

    sget-object v3, Lihj;->h:Livk;

    invoke-direct {v4, v3}, Livi;-><init>(Livk;)V

    invoke-virtual {v4, v5, v6, v7}, Livi;->a(IJ)Livi;

    iget-object v3, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lhnq;->i:I

    if-ge v3, v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    invoke-virtual {v4, v11, v0}, Livi;->a(ILivi;)V

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v3, v0

    :cond_2
    iget-object v0, p0, Lhnq;->d:Lhof;

    invoke-virtual {v0}, Lhof;->e()Lhod;

    move-result-object v7

    invoke-interface {v7}, Lhod;->a()Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lhnq;->o:Z

    if-nez v0, :cond_6

    move v0, v5

    :goto_3
    if-gtz v3, :cond_3

    if-eqz v0, :cond_7

    :cond_3
    move v6, v5

    :goto_4
    if-eqz v6, :cond_e

    new-instance v8, Livi;

    sget-object v9, Lihj;->Q:Livk;

    invoke-direct {v8, v9}, Livi;-><init>(Livk;)V

    new-instance v9, Livi;

    sget-object v10, Lihj;->F:Livk;

    invoke-direct {v9, v10}, Livi;-><init>(Livk;)V

    if-eqz v1, :cond_4

    invoke-virtual {v9, v5, v1}, Livi;->b(ILivi;)Livi;

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v9, v11, v4}, Livi;->b(ILivi;)Livi;

    :cond_5
    if-eqz v0, :cond_d

    invoke-interface {v7}, Lhod;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    const/16 v7, 0xc

    invoke-virtual {v9, v7, v0}, Livi;->a(ILivi;)V

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v6, v2

    goto :goto_4

    :cond_8
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_6
    invoke-virtual {v8, v12, v9}, Livi;->a(ILivi;)V

    iget-object v1, p0, Lhnq;->c:Lidu;

    invoke-interface {v1, v8}, Lidu;->d(Livi;)V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_9

    const-string v1, "CacheUpdater"

    const-string v4, "Sent %d cache items and %d model stats for refresh."

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v5

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    :goto_7
    if-lez v0, :cond_a

    move v2, v5

    :cond_a
    iput-boolean v2, p0, Lhnq;->p:Z

    if-lez v0, :cond_b

    iput-boolean v5, p0, Lhnq;->o:Z

    :cond_b
    if-nez v6, :cond_c

    invoke-direct {p0}, Lhnq;->g()V

    :cond_c
    return v6

    :cond_d
    move v0, v2

    goto :goto_6

    :cond_e
    move v0, v2

    goto :goto_7

    :cond_f
    move-object v1, v4

    move v0, v2

    goto/16 :goto_1
.end method

.method private g()V
    .locals 7

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lhnq;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lhnq;->k:I

    int-to-float v0, v0

    iget v3, p0, Lhnq;->j:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    const v3, 0x3f333333    # 0.7f

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget v3, p0, Lhnq;->j:I

    if-nez v3, :cond_3

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "CacheUpdater"

    const-string v4, "Cache up-to-date. Wifi database: %d, Cell database: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lhnq;->f:Lids;

    invoke-virtual {v6}, Lids;->e()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v1, p0, Lhnq;->f:Lids;

    invoke-virtual {v1}, Lids;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lhnq;->a(Z)V

    invoke-direct {p0}, Lhnq;->h()V

    invoke-direct {p0}, Lhnq;->c()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "CacheUpdater"

    const-string v4, "Cache refreshed successfully. Wifi database: %d, Cell database: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lhnq;->f:Lids;

    invoke-virtual {v6}, Lids;->e()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v1, p0, Lhnq;->f:Lids;

    invoke-virtual {v1}, Lids;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "CacheUpdater"

    const-string v4, "Failed to refresh cache. Total RPCs: %d, successful RPC: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lhnq;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget v1, p0, Lhnq;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private h()V
    .locals 5

    const/4 v1, -0x1

    iget-object v0, p0, Lhnq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lhnq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput v1, p0, Lhnq;->i:I

    iput v1, p0, Lhnq;->j:I

    iput v1, p0, Lhnq;->k:I

    sget-object v0, Lhns;->a:Lhns;

    iput-object v0, p0, Lhnq;->m:Lhns;

    iget-object v0, p0, Lhnq;->d:Lhof;

    invoke-virtual {v0}, Lhof;->a()J

    move-result-wide v0

    iget-object v2, p0, Lhnq;->d:Lhof;

    invoke-virtual {v2}, Lhof;->b()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lhnq;->a(JZ)J

    move-result-wide v0

    iget-object v2, p0, Lhnq;->e:Lhoj;

    iget-wide v2, v2, Lhoj;->e:J

    iget-object v4, p0, Lhnq;->e:Lhoj;

    iget-boolean v4, v4, Lhoj;->f:Z

    invoke-static {v2, v3, v4}, Lhnq;->a(JZ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lhnq;->c:Lidu;

    const/4 v3, 0x4

    iget-object v4, p0, Lhnq;->n:Lilx;

    invoke-interface {v2, v3, v0, v1, v4}, Lidu;->a(IJLilx;)V

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "CacheUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cache refresh scheduled at "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lhnq;->h()V

    return-void
.end method

.method public final a(I)V
    .locals 6

    const/4 v5, 0x4

    if-eq p1, v5, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lhnr;->a:[I

    iget-object v1, p0, Lhnq;->m:Lhns;

    invoke-virtual {v1}, Lhns;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "CacheUpdater"

    const-string v1, "Starting a new refresh."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhnq;->f:Lids;

    invoke-virtual {v0}, Lids;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "CacheUpdater"

    const-string v1, "NlpParameters expired, need to sync with server."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Livi;

    sget-object v1, Lihj;->Q:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iget-object v1, p0, Lhnq;->c:Lidu;

    invoke-interface {v1, v0}, Lidu;->d(Livi;)V

    invoke-direct {p0}, Lhnq;->e()V

    iget-object v0, p0, Lhnq;->c:Lidu;

    iget-object v1, p0, Lhnq;->c:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x2710

    add-long/2addr v1, v3

    iget-object v3, p0, Lhnq;->n:Lilx;

    invoke-interface {v0, v5, v1, v2, v3}, Lidu;->a(IJLilx;)V

    sget-object v0, Lhns;->b:Lhns;

    iput-object v0, p0, Lhnq;->m:Lhns;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lhnq;->d()V

    goto :goto_0

    :pswitch_1
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_4

    const-string v0, "CacheUpdater"

    const-string v1, "CacheUpdater terminated early when refreshing parameters."

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lhnq;->b()V

    goto :goto_0

    :pswitch_2
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_5

    const-string v0, "CacheUpdater"

    const-string v1, "CacheUpdater terminated early when refreshing cache."

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lhnq;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lilx;)V
    .locals 0

    iput-object p1, p0, Lhnq;->n:Lilx;

    return-void
.end method

.method public final a(Livi;)V
    .locals 14

    iget-object v0, p0, Lhnq;->m:Lhns;

    sget-object v1, Lhns;->b:Lhns;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lhnq;->c:Lidu;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lidu;->a(I)V

    invoke-static {p1}, Lilv;->a(Livi;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhnq;->f:Lids;

    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lids;->a(Livi;)V

    invoke-direct {p0}, Lhnq;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lhnq;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhnq;->m:Lhns;

    sget-object v1, Lhns;->c:Lhns;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhnq;->d:Lhof;

    const/4 v1, 0x0

    iget-object v2, p0, Lhnq;->c:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, Lhof;->a(Livi;ZJ)V

    iget-object v5, p0, Lhnq;->e:Lhoj;

    invoke-static {p1}, Lilv;->b(Livi;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v5, Lhoj;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v6

    iget-object v0, v5, Lhoj;->d:Lids;

    invoke-virtual {v0}, Lids;->e()I

    move-result v8

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Livi;->k(I)I

    move-result v9

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v9, :cond_8

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, Livi;->c(II)Livi;

    move-result-object v10

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    const/4 v0, 0x3

    invoke-virtual {v10, v0}, Livi;->k(I)I

    move-result v0

    if-ge v1, v0, :cond_7

    const/4 v0, 0x3

    invoke-virtual {v10, v0, v1}, Livi;->c(II)Livi;

    move-result-object v11

    const/4 v0, 0x3

    invoke-virtual {v11, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    invoke-virtual {v11, v0}, Livi;->f(I)Livi;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Livi;->d(I)J

    move-result-wide v2

    :goto_3
    const-wide/16 v12, -0x1

    cmp-long v0, v2, v12

    if-eqz v0, :cond_3

    iget-object v0, v5, Lhoj;->b:Lhok;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v6, v7}, Lhol;->a(J)V

    iget-object v2, v0, Lhol;->a:Livi;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, Livi;->e(II)Livi;

    const/4 v2, 0x4

    invoke-virtual {v11, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    invoke-virtual {v11, v2}, Livi;->e(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lhol;->a(F)V

    :goto_4
    const/4 v2, 0x5

    invoke-virtual {v11, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x5

    invoke-virtual {v11, v2}, Livi;->e(I)F

    move-result v2

    iget-object v0, v0, Lhol;->a:Livi;

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v2}, Livi;->a(IF)Livi;

    :cond_3
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Livi;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhuw;->a(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_3

    :cond_5
    iget-object v2, v0, Lhol;->a:Livi;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lilv;->a(Livi;I)V

    goto :goto_4

    :cond_6
    iget-object v0, v0, Lhol;->a:Livi;

    const/4 v2, 0x7

    invoke-static {v0, v2}, Lilv;->a(Livi;I)V

    goto :goto_5

    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    :cond_8
    invoke-static {p1}, Lilv;->a(Livi;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lhnq;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhnq;->k:I

    iget-boolean v0, p0, Lhnq;->p:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhnq;->d:Lhof;

    invoke-virtual {v0}, Lhof;->e()Lhod;

    move-result-object v0

    invoke-interface {v0}, Lhod;->c()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_9

    const-string v0, "CacheUpdater"

    const-string v1, "Clearing stats after successful upload"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-direct {p0}, Lhnq;->f()Z

    goto/16 :goto_0
.end method
