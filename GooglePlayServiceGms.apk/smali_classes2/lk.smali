.class public final Llk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lmb;


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/view/LayoutInflater;

.field c:Llm;

.field d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field e:I

.field f:I

.field g:Lll;

.field private h:I

.field private i:Lmc;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Llk;->f:I

    iput p2, p0, Llk;->e:I

    return-void
.end method

.method static synthetic a(Llk;)I
    .locals 1

    iget v0, p0, Llk;->h:I

    return v0
.end method


# virtual methods
.method public final a()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Llk;->g:Lll;

    if-nez v0, :cond_0

    new-instance v0, Lll;

    invoke-direct {v0, p0}, Lll;-><init>(Llk;)V

    iput-object v0, p0, Llk;->g:Lll;

    :cond_0
    iget-object v0, p0, Llk;->g:Lll;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Lmd;
    .locals 3

    iget-object v0, p0, Llk;->g:Lll;

    if-nez v0, :cond_0

    new-instance v0, Lll;

    invoke-direct {v0, p0}, Lll;-><init>(Llk;)V

    iput-object v0, p0, Llk;->g:Lll;

    :cond_0
    iget-object v0, p0, Llk;->g:Lll;

    invoke-virtual {v0}, Lll;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Llk;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_1

    iget-object v0, p0, Llk;->b:Landroid/view/LayoutInflater;

    sget v1, Lkn;->j:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Llk;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v0, p0, Llk;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Llk;->g:Lll;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Llk;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_1
    iget-object v0, p0, Llk;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Llm;)V
    .locals 2

    iget v0, p0, Llk;->e:I

    if-eqz v0, :cond_2

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Llk;->e:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Llk;->a:Landroid/content/Context;

    iget-object v0, p0, Llk;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llk;->b:Landroid/view/LayoutInflater;

    :cond_0
    :goto_0
    iput-object p2, p0, Llk;->c:Llm;

    iget-object v0, p0, Llk;->g:Lll;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llk;->g:Lll;

    invoke-virtual {v0}, Lll;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Llk;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iput-object p1, p0, Llk;->a:Landroid/content/Context;

    iget-object v0, p0, Llk;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    iget-object v0, p0, Llk;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llk;->b:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final a(Llm;Z)V
    .locals 1

    iget-object v0, p0, Llk;->i:Lmc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llk;->i:Lmc;

    invoke-interface {v0, p1, p2}, Lmc;->a(Llm;Z)V

    :cond_0
    return-void
.end method

.method public final a(Lmc;)V
    .locals 0

    iput-object p1, p0, Llk;->i:Lmc;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Llk;->g:Lll;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llk;->g:Lll;

    invoke-virtual {v0}, Lll;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a(Lmh;)Z
    .locals 1

    invoke-virtual {p1}, Lmh;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Llp;

    invoke-direct {v0, p1}, Llp;-><init>(Llm;)V

    invoke-virtual {v0}, Llp;->a()V

    iget-object v0, p0, Llk;->i:Lmc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llk;->i:Lmc;

    invoke-interface {v0, p1}, Lmc;->b(Llm;)Z

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Llk;->c:Llm;

    iget-object v1, p0, Llk;->g:Lll;

    invoke-virtual {v1, p3}, Lll;->a(I)Llq;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Llm;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method
