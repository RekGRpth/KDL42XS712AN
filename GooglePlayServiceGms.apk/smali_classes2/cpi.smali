.class final Lcpi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcqy;


# instance fields
.field final synthetic a:Lcqi;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcph;


# direct methods
.method constructor <init>(Lcph;Lcqi;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcpi;->d:Lcph;

    iput-object p2, p0, Lcpi;->a:Lcqi;

    iput-object p3, p0, Lcpi;->b:Ljava/lang/String;

    iput-object p4, p0, Lcpi;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, [B

    :try_start_0
    iget-object v0, p0, Lcpi;->a:Lcqi;

    invoke-interface {v0, p1}, Lcqi;->a([B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    :try_start_0
    new-instance v0, Lcpr;

    iget-object v1, p0, Lcpi;->b:Ljava/lang/String;

    iget-object v2, p0, Lcpi;->d:Lcph;

    iget-object v2, v2, Lcph;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {v0, v1, v2, p1}, Lcpr;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcpi;->a:Lcqi;

    iget-object v2, p0, Lcpi;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcpr;->a(Ljava/util/Map;[B)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcqi;->a([B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
