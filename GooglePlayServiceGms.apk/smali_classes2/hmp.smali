.class final Lhmp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0xb

    const/4 v8, 0x3

    const-wide v6, 0x3fc3333333333333L    # 0.15

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fee64b2314013ecL    # 0.949792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f924d8fd5cb7910L    # 0.017874

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4003e6a98244e93eL    # 2.487628

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c84e831ad213L    # 9.891224

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f564840e1719f80L    # 0.00136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4102ff8ec0f88L    # 0.156744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3eb851eb851ecL    # 0.155625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f164840e1719f80L    # 8.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fabecaab8a5ce5bL    # 0.05454

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef1d3671ac14c66L    # 1.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f43e3e29307af21L    # 6.07E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f10c6f7a0b5ed8dL    # 6.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe097b5286b5914L    # 0.518519

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2de26916440f24L    # 2.28E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f533c1ce6c093d9L    # 0.001174

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2711947cfa26a2L    # 1.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023ce8f4b61fe22L    # 9.903437

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f95ba6efc371da3L    # 0.021219

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3efe68a0d349be90L    # 2.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5055b89939218aL    # 9.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb3966be7afa722L    # 0.076514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f057eed45e9185dL    # 4.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f312ba16e7a311fL    # 2.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2450efdc9c4da9L    # 1.55E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2797cc39ffd60fL    # 1.8E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eed5c31593e5fb7L    # 1.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa577b7c7820a31L    # 0.041929

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ee92a737110e454L    # 1.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5233df2a9d627cL    # 0.001111

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f02dfd694ccab3fL    # 3.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fff42ea960b6fa0L    # 1.953837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe06bca5375c8daL    # 0.513158

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba7913e81450f0L    # 0.10341

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f79806f262888b5L    # 0.006226

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff984c271fff79dL    # 1.594912

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL    # 1.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f16ce789e774eecL    # 8.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f192a737110e454L    # 9.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f212ba16e7a311fL    # 1.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f20624dd2f1a9fcL    # 1.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f014d2f5dbb9cfaL    # 3.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f40be9424e59296L    # 5.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff610f733a8a3f9L    # 1.379142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8f7af640639d6L    # 0.780235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2ba7fc32ebe597L    # 2.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd019220ff54089L    # 0.251534

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f42b5e529bae46dL    # 5.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1083dbc23315d7L    # 6.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a1554fbdad752L    # 7.96E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc407cc7d1bb491L    # 0.156488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f32f09d8c6d612cL    # 2.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff2206d938151a4L    # 1.132917

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022c56366d7a56eL    # 9.385524

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc404af922962d0L    # 0.156393

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd78eeae9ee45c3L    # 0.368098

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4026eab884406c01L    # 11.458439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc70e4da09cc31aL    # 0.180124

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fab50b0f27bb2ffL    # 0.05335

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f49220ff540895dL    # 7.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31b1d92b7fe08bL    # 2.7E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb5f666234a87e4L    # 0.085791

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a5870da5daf08L    # 2.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff0fa93af74cd31L    # 1.061176

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b089a02752546L    # 8.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fac426351deefe5L    # 0.055194

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f516659d12caddeL    # 0.001062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fedf59ccfaeff5cL    # 0.936232

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3376d549731099L    # 2.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40275e5471715c64L    # 11.684238

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7f02c4d65e4606L    # 0.007571

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2ffb480a5accd5L    # 2.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4e92923e5b8562L    # 9.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40215f4517614595L    # 8.686074

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f78255b035bd513L    # 0.005895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd3680e06530058L    # 0.303226

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f856084a515ce9eL    # 0.010438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7999999999999aL    # 0.00625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f348344c37e6f72L    # 3.13E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcce6ea854477ffL    # 0.225797

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4024b17614595358L    # 10.346604

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fbfb2031ceaf252L    # 0.12381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b003686a4ca4fL    # 1.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40264743c7d5ed07L    # 11.139189

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26052502eec7c9L    # 1.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f564840e1719f80L    # 0.00136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4009f50c1b97353bL    # 3.244652

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5198aeb80ecfa7L    # 0.001074

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76b11c6d1e108cL    # 0.00554

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f87d5a9eb2074ebL    # 0.011638

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f697e56473471f8L    # 0.003112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f448344c37e6f72L    # 6.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd01d8a54823854L    # 0.251803

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2669ced0b30b5bL    # 1.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f65f676ea422899L    # 0.002681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61fd5885d31338L    # 0.002196

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f340dd3fe1975f3L    # 3.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f660956c0d6f545L    # 0.00269

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd56ebd4cfd08d5L    # 0.334884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL    # 1.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f868b5cbff47736L    # 0.011008

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401620de8f6cefedL    # 5.532099

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa9c8216c61522aL    # 0.050355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffcb685985ad539L    # 1.794561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ca3a4b5568e82L    # 4.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb17d9dba908a26L    # 0.068323

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402154484944ed70L    # 8.664614

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc6e9dd7ecbb7faL    # 0.179012

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcd4aa10e022142L    # 0.22884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002c0d23d4f15e8L    # 2.344151

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5f8e3ac0c62e4dL    # 0.001926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401d7d423d9231c6L    # 7.372323

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f36ce789e774eecL    # 3.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f42a51e321a2e7fL    # 5.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f74ee392e1ef73cL    # 0.00511

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8d88c1db0142f6L    # 0.014421

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffdf865d7cb2d90L    # 1.873144

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f48f81e8a2ec28bL    # 7.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fd3f5b5fa227L    # 0.156166

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc89d8c6d612c6bL    # 0.192308

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d14e3bcd35a86L    # 0.00355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd5cdee34fc610fL    # 0.340694

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4b64e054690de1L    # 8.36E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd40cf5b1c86488L    # 0.313291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f31d3671ac14c66L    # 2.72E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5ab0856e696a27L    # 0.001629

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb13b4b2fa93af7L    # 0.067311

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4198aeb80ecfa7L    # 5.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f43a0c6b484d76bL    # 5.99E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4029867d77fae361L    # 12.762676

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37a89331a08bfcL    # 3.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4eabbcb1cc9646L    # 9.36E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f83eab367a0f909L    # 0.009725

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5f4b1ee2435697L    # 0.00191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcddddb1209edc0L    # 0.233333

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c4b90214ad363L    # 0.001727

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f353bd1676640a7L    # 3.24E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1e68a0d349be90L    # 1.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8bb0e5e679463dL    # 0.013521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f86fd21ff2e48e9L    # 0.011225

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe18c62e4d1a650L    # 0.548387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0f75104d551d69L    # 6.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f164840e1719f80L    # 8.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd476c4827b6fe3L    # 0.319749

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f092a737110e454L    # 4.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd55bccaf709b74L    # 0.333728

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f16052502eec7c9L    # 8.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f12599ed7c6fbd2L    # 7.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2c0ca600b02928L    # 2.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc9ce39b456b442L    # 0.201606

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdf026cc1ca3a4bL    # 0.484523

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff18549b62c7757L    # 1.095041

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f340dd3fe1975f3L    # 3.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdcc26dce39b457L    # 0.449367

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef6052502eec7c9L    # 2.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8febf22c01e69L    # 0.781097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f51c6d1e108c3f4L    # 0.001085

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40713f077ccc0L    # 0.156466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5ab8e8ea39c51eL    # 0.001631

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc8c62e4d1a6506L    # 0.193548

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3615ebfa8f7db7L    # 3.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2040bfe3b03e21L    # 1.24E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6d4738a3b57c4eL    # 0.003574

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a1554fbdad752L    # 1.99E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd1826aa8eb4635L    # 0.273585

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f20c6f7a0b5ed8dL    # 1.28E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe10da3c21187e8L    # 0.532915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f32dfd694ccab3fL    # 2.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f7d73c9257860L    # 9.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402c77c6327ed84dL    # 14.233934

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbaa8112ba16e7aL    # 0.104127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fac6ef3d3a1d324L    # 0.055534

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd2bef06b37867fL    # 0.292904

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9fc0d2c386d2edL    # 0.031009

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26ce789e774eecL    # 1.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f312ba16e7a311fL    # 2.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5f3a57eaa2a0a9L    # 0.001906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b866e43aa79bcL    # 2.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1797cc39ffd60fL    # 9.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0de26916440f24L    # 5.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd1af294dd72368L    # 0.276316

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f129cbab649d389L    # 7.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d5c31593e5fb7L    # 1.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1cd5f99c38b04bL    # 1.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e03f705857affL    # 2.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86eefa1e3eaf68L    # 0.011198

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f55336deb95e5b0L    # 0.001294

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3e5eaab042529L    # 0.155454

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4797cc39ffd60fL    # 7.2E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024f94ccab3edd9L    # 10.486914

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a79fec99f1ae3L    # 1.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc408d08919ef95L    # 0.156519

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f212ba16e7a311fL    # 1.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4c92ddbdb5d895L    # 8.72E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f11904b3c3e74b0L    # 6.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcbe9af5ba2be06L    # 0.218069

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe1068fd199bb28L    # 0.532051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa36f3b213e3e29L    # 0.037958

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f90e06530058149L    # 0.016481

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400a8be61cffeb07L    # 3.31831

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1d5c31593e5fb7L    # 1.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f58d25edd052935L    # 0.001515

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d8e8640208180L    # 4.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f889df1172ef0aeL    # 0.01202

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f539c94f69ca9efL    # 0.001197

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcbd377e1b8ed1cL    # 0.217391

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef81e03f705857bL    # 2.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4028c8539fba450bL    # 12.391263

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc35219a847b246L    # 0.150943

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4d8e8640208180L    # 9.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbadd377e1b8ed2L    # 0.104938

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03660e51d25aabL    # 3.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fda12f4cf4a558fL    # 0.407407

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b0ab2e1693c04L    # 0.006602

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc6e4f3343fa2adL    # 0.178862

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27fc7607c419a0L    # 1.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f755d5f56a7ac82L    # 0.005216

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fbc71c53f39d1b3L    # 0.111111

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f29d2391d57ff9bL    # 1.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe09d8a54823854L    # 0.519231

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f47d2849cb252ceL    # 7.27E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff683c6c97d8cf4L    # 1.407172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa6d56b00ffda40L    # 0.044597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f557abb8800eae2L    # 0.001311

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3094a2b9d3cbc5L    # 2.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1322f2734f82f5L    # 7.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402bb2469d7342eeL    # 13.848195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5554fbdad7518bL    # 0.001302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6c7368ad688377L    # 0.003473

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3acde19fc2a887L    # 4.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc721ee675147f1L    # 0.180723

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc409f623076c05L    # 0.156554

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8c74fb549f9485L    # 0.013895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe317a02fb5d031L    # 0.596634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd16af038e29f9dL    # 0.272152

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f910b630a91537aL    # 0.016645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2cb46bacf74470L    # 2.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa02ababead4f59L    # 0.031576

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4aac53b0813cacL    # 8.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f33b9f127f5e84fL    # 3.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9a543f1c75818cL    # 0.025712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40217442c7fbacb4L    # 8.727072

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdd05dd8f92af9bL    # 0.453483

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fdd2a62aa19439eL    # 0.455712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4046e236006d0d4aL    # 45.767273

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5858bc59b8023aL    # 0.001486

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4003ec5da6a44418L    # 2.490413

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d13fd71b04684L    # 14.539043

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fda1af3a14cec42L    # 0.407895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c254a3c64346L    # 9.879552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc9852f7f498c3bL    # 0.199377

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc579fa97e132b5L    # 0.167785

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcb1d81f10667f9L    # 0.211838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d41fa76534374L    # 0.007143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4bd1ed9dfdac69L    # 8.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4049bd2502eec7c9L    # 51.477692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe3fddca4b124d1L    # 0.624739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3871e6cd29131fL    # 3.73E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403245c5b4aa9718L    # 18.272548

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f78fb43d89ce4a8L    # 0.006099

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f623a29c779a6b5L    # 0.002225

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffe0370cdc8754fL    # 1.87584

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f24b599aa60913aL    # 1.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7edd052934acb0L    # 0.007535

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa4dad31fcd24e1L    # 0.040732

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde0357a355043eL    # 0.468954

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fff18244e93e1caL    # 1.943394

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9df69878316a05L    # 0.029261

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f7d73c9257860L    # 9.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9b1572580c3090L    # 0.026449

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f33cab81f969e3dL    # 3.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f71dffc5479d4d8L    # 0.004364

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc7f2cb641700ceL    # 0.187097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb357f737da61e1L    # 0.075561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fa8374ff865d7cbL    # 0.047297

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402f20ed80a17b0fL    # 15.564312

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f378705425f2021L    # 3.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff1caa326e11559L    # 1.111972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f53c68661ae70c1L    # 0.001207

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f80079a2834d270L    # 0.007827

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8d9988d2a1f8e4L    # 0.014453

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f73a4f8726d04e6L    # 0.004796

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbf6577531db446L    # 0.122642

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402255bff04577d9L    # 9.16748

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc89b068123810fL    # 0.192231

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f478f68be2f7b18L    # 7.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40a9de8b3b320L    # 0.156574

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f624af0bf1a5ca3L    # 0.002233

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f88adab9f559b3dL    # 0.01205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    iget-wide v0, p1, Lhmx;->c:D

    const-wide/high16 v2, 0x3fe1000000000000L    # 0.53125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe6bdfd2630ec31L    # 0.710692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde295a6c5d206dL    # 0.471274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023fadc8fb86f48L    # 9.989964

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd15885d31337ebL    # 0.271028

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0f9c1f85d744fL    # 0.530488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb3877ab324851bL    # 0.076286

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f45fcc1871e6cd3L    # 6.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f385058dde7a744L    # 3.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4196d8f4f93bcL    # 0.157026

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62f4cf4a558ea8L    # 0.002314

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f6cacd184c272L    # 9.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f49220ff540895dL    # 7.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5efb6dca07f66fL    # 0.001891

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc460fe47991bc5L    # 0.15921

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f55bdd76683c298L    # 0.001327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0eeed8904f6dfcL    # 5.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4009010385c67dfeL    # 3.125495

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e4712e40852b5L    # 4.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f44727dcbddb984L    # 6.24E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd0e7d9988d2a20L    # 0.264151

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4010345d41fa7653L    # 4.051137

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb490c4dec1c1d7L    # 0.080334

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff40f9f44d44567L    # 1.253814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023acd6e47dc37aL    # 9.837577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405407547e06961cL    # 80.114532

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff405e4e69f05eaL    # 1.251439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffc6931876188b1L    # 1.775682

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6a847b24638c97L    # 0.003237

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd37a7008a697afL    # 0.304348

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f67481b21c475e6L    # 0.002842

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff19e6256366d7aL    # 1.101168

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f809917939a8L    # 0.156007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde16c1e364bec6L    # 0.470139

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc80dbad3a604e2L    # 0.187919

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9c226809d49518L    # 0.027475

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400365db76b3bb84L    # 2.424735

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdc1cb46bacf744L    # 0.439252

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa1a2834d26fa40L    # 0.034443

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f863dc486ad2dcbL    # 0.01086

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f48a43bb40b34e7L    # 7.52E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91b10fd7e45804L    # 0.017277

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fec6cf206423960L    # 0.888299

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400176d549731099L    # 2.183024

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9fd50225742dcfL    # 0.031086

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff67062d40aaeb0L    # 1.402438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc292f6e82949a5L    # 0.14511

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f937fbefd00713fL    # 0.019042

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc970e6f2e8c048L    # 0.198758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f50d7be9856a37bL    # 0.001028

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f84b72c5197a249L    # 0.010115

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f18e757928e0c9eL    # 9.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f42b5e529bae46dL    # 5.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbfcea6c1a048e0L    # 0.124247

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402110ccdd93c46eL    # 8.532813

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f797e56473471f8L    # 0.006224

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8474107314ca92L    # 0.009987

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dac57e23f24eL    # 2.356822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6dcd7060bb2bbbL    # 0.003638

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc3716d2aa5c5f8L    # 0.151899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401eb1e96c3fc43bL    # 7.673742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc5aaef2c732592L    # 0.169279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ecaab8a5ce5bL    # 0.15566

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f94d099e0e73605L    # 0.020327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f837b8d3f1844L    # 7.878401

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f85bdd76683c298L    # 0.010616

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8dae81882adc4dL    # 0.014493

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4031b3c2fc69728aL    # 17.702194

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f666be7afa72218L    # 0.002737

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4fea8112ba16e8L    # 9.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6f2778140dd3feL    # 0.003803

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4008fc01a36e2eb2L    # 3.12305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcae789e774eebfL    # 0.210191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff923a7daa4fca4L    # 1.571205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f94fbdad7518b0dL    # 0.020492

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb6b26bf8769ec3L    # 0.08866

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff418b3f63c31dfL    # 1.256031

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f78d045fe111277L    # 0.006058

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc410060780fdc1L    # 0.156739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbe6ea854477ff1L    # 0.118876

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f457eed45e9185dL    # 6.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404091aafff36ac6L    # 33.138031

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd8ec28b2a6b0d9L    # 0.389414

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fbe1e1d2178f68cL    # 0.117647

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff72defc7a39820L    # 1.448715

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9240b780346dc6L    # 0.017825

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f84057082491afcL    # 0.009776

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff669b499d0203eL    # 1.400807

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc5668c26139000L    # 0.167192

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fafbeb9e492bc30L    # 0.062002

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa5c78a6dacabc5L    # 0.042538

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d4977ea1c68ecL    # 14.643493

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f537f38c5436b90L    # 0.00119

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fb8e68e3ef284L    # 7.930567

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62ab68cef672b9L    # 0.002279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400e1294dd72367eL    # 3.759073

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e7fb267c6b8b7L    # 0.014892

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401d2f4f50a02b84L    # 7.296201

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86b441bb8c32a9L    # 0.011086

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e7e1fc08fa7a8L    # 0.014889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffb7c30d306a2b1L    # 1.71782

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcb26c7eae5bc88L    # 0.212121

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40208ac5197a2489L    # 8.271035

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f42bb6672fbaL    # 0.155889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400a3330941c8217L    # 3.274995

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40377a35e74299d9L    # 23.477385

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400b9f8f4730403aL    # 3.45291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fba75579af1886eL    # 0.103353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffe0a30db6a1e82L    # 1.877488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff76e8e1d6494d5L    # 1.464491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa05f28848387dfL    # 0.031976

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc2bf11f926c7ebL    # 0.146456

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fcd0856e696a26eL    # 0.226817

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7e02ea960b6fa0L    # 0.007327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401a4d8904f6dfc6L    # 6.575718

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8757d5a9eb2075L    # 0.011398

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb794317acc4ef9L    # 0.092105

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f214d2f5dbb9cfaL    # 1.32E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400171971c10d7bfL    # 2.180464

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401429e060fe4799L    # 5.040895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403150964628027eL    # 17.314793

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405bc31461b6d43dL    # 111.048119

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401dab3c81908e58L    # 7.417223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9559f6ec5b078eL    # 0.020851

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4e51d25aab474L    # 0.163242

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbb810e8858ff76L    # 0.107438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4035feeae9ee45c3L    # 21.995772

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40181ddc6195464eL    # 6.029161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403186671ef30a4eL    # 17.525011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7413122b7baecdL    # 0.004901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4000773c0c1fc8f3L    # 2.05822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f129d40ee06d9L    # 7.768178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc9c193b3a68b1aL    # 0.20122

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404edb61dc93ea2dL    # 61.713924

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4024a951c5c5718fL    # 10.330702

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9054690de09353L    # 0.015947

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eff75104d551d69L    # 3.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    invoke-virtual {p1, v8}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401ac4c6e6d9be4dL    # 6.692165

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa73e681a9b8cb9L    # 0.045398

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7116a8b8f14db6L    # 0.004172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f435daad601ffb5L    # 5.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc29a8049667b5fL    # 0.14534

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f51b60ae9680e06L    # 0.001081

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f667ec7863beec4L    # 0.002746

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40377afa0d77b7c8L    # 23.480378

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402074660a201472L    # 8.227341

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7532617c1bda51L    # 0.005175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa0153bd1676641L    # 0.031412

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb1e19fc2a8869cL    # 0.069849

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d1

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc13198288051caL    # 0.134326

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f93d29563a9f384L    # 0.019358

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f708a265f0f5a10L    # 0.004038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4111276fb0920L    # 0.156771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f526634117f8445L    # 0.001123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x4014883be6601bcaL    # 5.133041

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d0

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa5084e831ad213L    # 0.041079

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033d0b8ae31d713L    # 19.815318

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f88b97785729bL    # 0.030795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cd

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f462f166e008e9bL    # 6.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f365907d912556dL    # 3.41E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ef1fddebd902L    # 0.155735

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f79e0e736049ecbL    # 0.006318

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4344806290eed0L    # 5.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ca

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe241f644955b46L    # 0.570552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f21f4f50a02b841L    # 1.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f700e6afcce1c58L    # 0.00392

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ca
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f229cbab649d389L    # 1.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cc

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdaed45e9185ceeL    # 0.420732

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cd
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd2e63a5c1c6089L    # 0.295302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cf

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc400fba8826aa9L    # 0.15628

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ce
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
