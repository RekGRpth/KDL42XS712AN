.class final Leol;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljavax/crypto/SecretKey;

.field b:Ljavax/crypto/SecretKey;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Leos;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Leos;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Preferences must not be null."

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Leol;->c:Landroid/content/SharedPreferences;

    const-string v0, "Secret key wrapper must not be null."

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leos;

    iput-object v0, p0, Leol;->d:Leos;

    const-string v0, "Secret key wrapper must not be null."

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leol;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Leol;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Leol;->d:Leos;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    iget-object v2, p0, Leol;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Leos;->a([BLjava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljavax/crypto/SecretKey;)V
    .locals 3

    iget-object v0, p0, Leol;->d:Leos;

    invoke-interface {v0, p2}, Leos;->a(Ljavax/crypto/SecretKey;)[B

    move-result-object v0

    iget-object v1, p0, Leol;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
