.class public final Lidx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lidz;


# instance fields
.field private final b:Lidq;

.field private final c:Ljavax/crypto/SecretKey;

.field private final d:Lhpk;

.field private final e:Ljava/io/File;

.field private final f:I

.field private final g:I

.field private volatile h:I

.field private final i:Livk;

.field private final j:Lidz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lidy;

    invoke-direct {v0}, Lidy;-><init>()V

    sput-object v0, Lidx;->a:Lidz;

    return-void
.end method

.method public constructor <init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lidx;->h:I

    iput-object p5, p0, Lidx;->i:Livk;

    iput-object p8, p0, Lidx;->b:Lidq;

    iput-object p6, p0, Lidx;->e:Ljava/io/File;

    iput p1, p0, Lidx;->f:I

    iput p3, p0, Lidx;->g:I

    iput-object p2, p0, Lidx;->c:Ljavax/crypto/SecretKey;

    if-eqz p4, :cond_0

    invoke-static {p4, v1}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v0

    iput-object v0, p0, Lidx;->d:Lhpk;

    :goto_0
    iput-object p7, p0, Lidx;->j:Lidz;

    return-void

    :cond_0
    iput-object v1, p0, Lidx;->d:Lhpk;

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;)Livi;
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lidx;->h:I

    iget v0, p0, Lidx;->h:I

    iget v3, p0, Lidx;->f:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lidx;->h:I

    iget v3, p0, Lidx;->g:I

    if-eq v0, v3, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid version, desired = %d or %d, actual = %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lidx;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lidx;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lidx;->h:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v2}, Lilv;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    :try_start_2
    iget v0, p0, Lidx;->h:I

    iget v3, p0, Lidx;->f:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lidx;->c:Ljavax/crypto/SecretKey;

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v3, v0}, Licl;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v2

    :goto_1
    iget-object v0, p0, Lidx;->i:Livk;

    invoke-static {v2, v0}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v0

    iget-object v3, p0, Lidx;->j:Lidz;

    invoke-interface {v3, v0}, Lidz;->a(Livi;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid file format."

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lidx;->d:Lhpk;

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v3, "No cipher key specified."

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lidx;->d:Lhpk;

    invoke-virtual {v0, v1}, Lhpk;->a(Ljava/io/DataInputStream;)Lhue;

    move-result-object v0

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v2, v3

    goto :goto_1

    :cond_3
    invoke-static {v2}, Lilv;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private a(Livi;Ljava/io/OutputStream;)V
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v0, p0, Lidx;->g:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, p0, Lidx;->d:Lhpk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {p1}, Livi;->f()[B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    iget-object v2, p0, Lidx;->d:Lhpk;

    invoke-virtual {v2, v1, v0}, Lhpk;->a(Ljava/io/DataOutputStream;[B)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_4
    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Runtime while writing protobuf to bytes."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    :try_start_5
    new-instance v0, Ljava/io/IOException;

    const-string v2, "No cipher specified."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Livi;
    .locals 2

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lidx;->e:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, v0}, Lidx;->a(Ljava/io/InputStream;)Livi;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)V
    .locals 3

    const-string v0, "output buffer can not be null."

    invoke-static {p1, v0}, Lilv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v0, p0, Lidx;->b:Lidq;

    iget-object v2, p0, Lidx;->e:Ljava/io/File;

    invoke-interface {v0, v2}, Lidq;->a(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lilv;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Livi;)[B
    .locals 1

    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lilv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0, p1, v0}, Lidx;->a(Livi;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lidx;->h:I

    return v0
.end method

.method public final b(Livi;)V
    .locals 2

    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lilv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v0, p0, Lidx;->b:Lidq;

    iget-object v1, p0, Lidx;->e:Ljava/io/File;

    invoke-interface {v0, v1}, Lidq;->a(Ljava/io/File;)V

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lidx;->e:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p1, v0}, Lidx;->a(Livi;Ljava/io/OutputStream;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SimpleProtoBufStore loading "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lidx;->i:Livk;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from file "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lidx;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
