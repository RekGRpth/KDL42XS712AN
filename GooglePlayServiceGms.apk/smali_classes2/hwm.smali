.class public final Lhwm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lhwm;


# instance fields
.field public final b:J

.field public final c:J

.field public final d:F

.field public final e:Z

.field public final f:Ljava/util/Collection;

.field public final g:Ljava/util/Collection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lhwm;->a(Ljava/util/Collection;)Lhwm;

    move-result-object v0

    sput-object v0, Lhwm;->a:Lhwm;

    return-void
.end method

.method private constructor <init>(JJFZLjava/util/Collection;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lhwm;->b:J

    iput-wide p3, p0, Lhwm;->c:J

    iput p5, p0, Lhwm;->d:F

    iput-boolean p6, p0, Lhwm;->e:Z

    iput-object p7, p0, Lhwm;->f:Ljava/util/Collection;

    iput-object p8, p0, Lhwm;->g:Ljava/util/Collection;

    return-void
.end method

.method public static a(Ljava/util/Collection;)Lhwm;
    .locals 12

    const-wide v1, 0x7fffffffffffffffL

    const-wide v3, 0x7fffffffffffffffL

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v6, 0x0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v0

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwl;

    iget-object v7, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v7}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v7

    iget-object v9, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v9}, Lcom/google/android/gms/location/LocationRequest;->g()F

    move-result v9

    const/4 v11, 0x0

    cmpl-float v11, v9, v11

    if-nez v11, :cond_0

    cmp-long v11, v7, v3

    if-gez v11, :cond_0

    move-wide v3, v7

    :cond_0
    cmp-long v11, v7, v1

    if-gez v11, :cond_8

    :goto_1
    const/4 v1, 0x0

    cmpl-float v1, v9, v1

    if-eqz v1, :cond_7

    cmpg-float v1, v9, v5

    if-gez v1, :cond_7

    move v1, v9

    :goto_2
    iget-boolean v0, v0, Lhwl;->b:Z

    if-eqz v0, :cond_1

    const/4 v6, 0x1

    :cond_1
    move v5, v1

    move-wide v1, v7

    goto :goto_0

    :cond_2
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v5, v0

    if-eqz v0, :cond_3

    cmp-long v0, v1, v3

    if-nez v0, :cond_4

    :cond_3
    const/4 v5, 0x0

    :cond_4
    const-wide v7, 0x7fffffffffffffffL

    cmp-long v0, v1, v7

    if-nez v0, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    :goto_3
    cmp-long v0, v3, v1

    if-gtz v0, :cond_6

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    :goto_4
    new-instance v0, Lhwm;

    invoke-direct/range {v0 .. v8}, Lhwm;-><init>(JJFZLjava/util/Collection;Ljava/util/Collection;)V

    return-object v0

    :cond_5
    const/4 v0, 0x0

    invoke-static {p0, v1, v2, v0}, Lhwm;->a(Ljava/util/Collection;JZ)Ljava/util/List;

    move-result-object v7

    goto :goto_3

    :cond_6
    const/4 v0, 0x1

    invoke-static {p0, v3, v4, v0}, Lhwm;->a(Ljava/util/Collection;JZ)Ljava/util/List;

    move-result-object v8

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2

    :cond_8
    move-wide v7, v1

    goto :goto_1
.end method

.method private static a(Ljava/util/Collection;JZ)Ljava/util/List;
    .locals 8

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-wide/16 v2, 0x3e8

    add-long/2addr v2, p1

    const-wide/16 v4, 0x3

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwl;

    iget-object v5, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v5}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gez v6, :cond_0

    if-eqz p3, :cond_1

    invoke-virtual {v5}, Lcom/google/android/gms/location/LocationRequest;->g()F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    :cond_1
    iget-object v0, v0, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lhwm;)Lhwm;
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p1, Lhwm;->b:J

    iget-wide v5, p0, Lhwm;->b:J

    cmp-long v0, v3, v5

    if-gtz v0, :cond_0

    iget v0, p1, Lhwm;->d:F

    iget v3, p0, Lhwm;->d:F

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    iget-wide v3, p1, Lhwm;->c:J

    iget-wide v5, p0, Lhwm;->c:J

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    sget-object v0, Lhwm;->a:Lhwm;

    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    new-instance v0, Lhwm;

    iget-wide v1, p0, Lhwm;->c:J

    iget-wide v3, p0, Lhwm;->c:J

    const/4 v5, 0x0

    iget-boolean v6, p0, Lhwm;->e:Z

    iget-object v7, p0, Lhwm;->g:Ljava/util/Collection;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lhwm;-><init>(JJFZLjava/util/Collection;Ljava/util/Collection;)V

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    new-instance v0, Lhwm;

    iget-wide v1, p0, Lhwm;->b:J

    const-wide v3, 0x7fffffffffffffffL

    iget v5, p0, Lhwm;->d:F

    iget-boolean v6, p0, Lhwm;->e:Z

    iget-object v7, p0, Lhwm;->f:Ljava/util/Collection;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lhwm;-><init>(JJFZLjava/util/Collection;Ljava/util/Collection;)V

    goto :goto_2

    :cond_4
    move-object v0, p0

    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lhwm;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lhwm;

    iget-wide v1, p0, Lhwm;->b:J

    iget-wide v3, p1, Lhwm;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lhwm;->c:J

    iget-wide v3, p1, Lhwm;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget v1, p0, Lhwm;->d:F

    iget v2, p1, Lhwm;->d:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lhwm;->e:Z

    iget-boolean v2, p1, Lhwm;->e:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lhwm;->f:Ljava/util/Collection;

    iget-object v2, p1, Lhwm;->f:Ljava/util/Collection;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhwm;->g:Ljava/util/Collection;

    iget-object v2, p1, Lhwm;->g:Ljava/util/Collection;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lhwm;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lhwm;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lhwm;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lhwm;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lhwm;->f:Ljava/util/Collection;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lhwm;->g:Ljava/util/Collection;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LocationRequestSummary["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "minIntervalMs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lhwm;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", minIntervalNoDistanceMs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lhwm;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", minDistanceMeters "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lhwm;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", requestDebugInfo "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lhwm;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", blameClients "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhwm;->f:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", blameNoDistanceClients "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhwm;->g:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
