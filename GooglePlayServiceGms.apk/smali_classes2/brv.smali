.class public final Lbrv;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/CreateFileRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/CreateFileRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    const-string v1, "Invalid create request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    const-string v1, "Invalid create request: no contents"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid create request: invalid contents"

    invoke-static {v0, v1}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v1, "Invalid create request: no metadata"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid create request: no parent"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrv;->b:Lbrc;

    iget-object v1, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget-object v3, p0, Lbrv;->c:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lbrc;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v0, p0, Lbrv;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v0, v2}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
