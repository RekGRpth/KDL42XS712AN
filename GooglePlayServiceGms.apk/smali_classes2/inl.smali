.class public final Linl;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ling;

.field public c:Ljava/lang/String;

.field public d:I

.field private e:Z

.field private f:J

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Linf;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Z

.field private u:Z

.field private v:J

.field private w:Z

.field private x:J

.field private y:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-wide v2, p0, Linl;->f:J

    iput v1, p0, Linl;->a:I

    const-string v0, ""

    iput-object v0, p0, Linl;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linl;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linl;->m:Ljava/lang/String;

    iput-object v4, p0, Linl;->o:Linf;

    iput-object v4, p0, Linl;->b:Ling;

    const-string v0, ""

    iput-object v0, p0, Linl;->c:Ljava/lang/String;

    iput v1, p0, Linl;->s:I

    iput v1, p0, Linl;->d:I

    iput-wide v2, p0, Linl;->v:J

    iput-wide v2, p0, Linl;->x:J

    const/4 v0, -0x1

    iput v0, p0, Linl;->y:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Linl;->y:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Linl;->b()I

    :cond_0
    iget v0, p0, Linl;->y:I

    return v0
.end method

.method public final a(I)Linl;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->g:Z

    iput p1, p0, Linl;->a:I

    return-object p0
.end method

.method public final a(J)Linl;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->w:Z

    iput-wide p1, p0, Linl;->x:J

    return-object p0
.end method

.method public final a(Ling;)Linl;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->p:Z

    iput-object p1, p0, Linl;->b:Ling;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Linl;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->h:Z

    iput-object p1, p0, Linl;->i:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Linl;->e:Z

    iput-wide v0, p0, Linl;->f:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Linl;->a(I)Linl;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linl;->a(Ljava/lang/String;)Linl;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Linl;->j:Z

    iput-object v0, p0, Linl;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Linl;->l:Z

    iput-object v0, p0, Linl;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Linf;

    invoke-direct {v0}, Linf;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Linl;->n:Z

    iput-object v0, p0, Linl;->o:Linf;

    goto :goto_0

    :sswitch_7
    new-instance v0, Ling;

    invoke-direct {v0}, Ling;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Linl;->a(Ling;)Linl;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linl;->b(Ljava/lang/String;)Linl;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v2, p0, Linl;->r:Z

    iput v0, p0, Linl;->s:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Linl;->b(I)Linl;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Linl;->u:Z

    iput-wide v0, p0, Linl;->v:J

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Linl;->a(J)Linl;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Linl;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Linl;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_0
    iget-boolean v0, p0, Linl;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Linl;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-boolean v0, p0, Linl;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Linl;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Linl;->j:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Linl;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Linl;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Linl;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Linl;->n:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Linl;->o:Linf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    iget-boolean v0, p0, Linl;->p:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Linl;->b:Ling;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    iget-boolean v0, p0, Linl;->q:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Linl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_7
    iget-boolean v0, p0, Linl;->r:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v1, p0, Linl;->s:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_8
    iget-boolean v0, p0, Linl;->t:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Linl;->d:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_9
    iget-boolean v0, p0, Linl;->u:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-wide v1, p0, Linl;->v:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_a
    iget-boolean v0, p0, Linl;->w:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-wide v1, p0, Linl;->x:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_b
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Linl;->e:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Linl;->f:J

    invoke-static {v0, v1, v2}, Lizh;->c(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Linl;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Linl;->a:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Linl;->h:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Linl;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Linl;->j:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Linl;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Linl;->l:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Linl;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Linl;->n:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Linl;->o:Linf;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Linl;->p:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Linl;->b:Ling;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Linl;->q:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Linl;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Linl;->r:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget v2, p0, Linl;->s:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Linl;->t:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget v2, p0, Linl;->d:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Linl;->u:Z

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-wide v2, p0, Linl;->v:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Linl;->w:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-wide v2, p0, Linl;->x:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iput v0, p0, Linl;->y:I

    return v0
.end method

.method public final b(I)Linl;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->t:Z

    iput p1, p0, Linl;->d:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Linl;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->q:Z

    iput-object p1, p0, Linl;->c:Ljava/lang/String;

    return-object p0
.end method
