.class public final Lhyg;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lhyf;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:I

.field public g:Z

.field public h:I

.field public i:Z

.field public j:J

.field public k:Z

.field public l:Z

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhyg;->b:Lhyf;

    const-string v0, ""

    iput-object v0, p0, Lhyg;->d:Ljava/lang/String;

    iput v2, p0, Lhyg;->f:I

    iput v2, p0, Lhyg;->h:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhyg;->j:J

    iput-boolean v2, p0, Lhyg;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Lhyg;->m:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lhyg;->m:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lhyg;->b()I

    :cond_0
    iget v0, p0, Lhyg;->m:I

    return v0
.end method

.method public final a(I)Lhyg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->e:Z

    iput p1, p0, Lhyg;->f:I

    return-object p0
.end method

.method public final a(J)Lhyg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->i:Z

    iput-wide p1, p0, Lhyg;->j:J

    return-object p0
.end method

.method public final a(Lhyf;)Lhyg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->a:Z

    iput-object p1, p0, Lhyg;->b:Lhyf;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lhyg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->c:Z

    iput-object p1, p0, Lhyg;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lhyg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->k:Z

    iput-boolean p1, p0, Lhyg;->l:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lhyf;

    invoke-direct {v0}, Lhyf;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lhyg;->a(Lhyf;)Lhyg;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhyg;->a(Ljava/lang/String;)Lhyg;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyg;->a(I)Lhyg;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyg;->b(I)Lhyg;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyg;->a(J)Lhyg;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lhyg;->a(Z)Lhyg;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Lhyg;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhyg;->b:Lhyf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Lhyg;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhyg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lhyg;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lhyg;->f:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_2
    iget-boolean v0, p0, Lhyg;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lhyg;->h:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_3
    iget-boolean v0, p0, Lhyg;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lhyg;->j:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_4
    iget-boolean v0, p0, Lhyg;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lhyg;->l:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_5
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhyg;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhyg;->b:Lhyf;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lhyg;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhyg;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lhyg;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lhyg;->f:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lhyg;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lhyg;->h:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lhyg;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lhyg;->j:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lhyg;->k:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lhyg;->l:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lhyg;->m:I

    return v0
.end method

.method public final b(I)Lhyg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyg;->g:Z

    iput p1, p0, Lhyg;->h:I

    return-object p0
.end method
