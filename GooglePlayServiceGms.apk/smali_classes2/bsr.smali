.class public final Lbsr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcfz;

.field final b:Lcby;

.field private final c:Lbst;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:Lcce;


# direct methods
.method public constructor <init>(Lcfz;Lcby;Lbst;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbsr;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbsr;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    iput-object v0, p0, Lbsr;->b:Lcby;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbst;

    iput-object v0, p0, Lbsr;->c:Lbst;

    sget-object v1, Lccg;->a:Lccf;

    new-instance v2, Lbss;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lbss;-><init>(Lbsr;B)V

    sget-object v0, Lbqs;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lbsr;->d:Ljava/util/concurrent/ExecutorService;

    const-string v4, "ContentMaintenance"

    invoke-interface {v1, v2, v0, v3, v4}, Lccf;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lcce;

    move-result-object v0

    iput-object v0, p0, Lbsr;->e:Lcce;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lbsr;->e:Lcce;

    invoke-interface {v0}, Lcce;->a()V

    return-void
.end method

.method final a(Lcfx;)V
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p1, Lcfx;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "ContentMaintenance"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Evicting from internal storage (will remain in shared storage): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :try_start_0
    iget-object v2, p0, Lbsr;->a:Lcfz;

    invoke-interface {v2}, Lcfz;->c()V

    iget-object v4, p0, Lbsr;->c:Lbst;

    iget-object v5, p1, Lcfx;->a:Ljava/lang/String;

    iget-object v2, v4, Lbst;->b:Lcby;

    invoke-interface {v2}, Lcby;->d()Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "ContentManager"

    const-string v1, "Shared storage is not available; not moving content with hash: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V
    :try_end_0
    .catch Lbud; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-void

    :cond_0
    const-string v2, "ContentMaintenance"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Moving from internal to shared storage: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v2, v4, Lbst;->a:Lcfz;

    invoke-interface {v2}, Lcfz;->c()V
    :try_end_1
    .catch Lbud; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iget-object v2, v4, Lbst;->a:Lcfz;

    invoke-interface {v2, v5}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v0, "ContentManager"

    const-string v1, "Cannot move to shared storage. No content with hash: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V

    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V
    :try_end_3
    .catch Lbud; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "ContentMaintenance"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to move content to shared storage: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    :try_start_4
    iget-object v3, v2, Lcfx;->b:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v3, :cond_3

    :try_start_5
    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V

    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V
    :try_end_5
    .catch Lbud; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "ContentMaintenance"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to move content to shared storage: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    :try_start_6
    iget-object v3, v2, Lcfx;->c:Ljava/lang/String;

    if-eqz v3, :cond_4

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcfx;->a(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcfx;->k()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V

    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    goto/16 :goto_1

    :cond_4
    iget-object v3, v4, Lbst;->a:Lcfz;

    invoke-interface {v3}, Lcfz;->f()V

    iget-object v3, v4, Lbst;->a:Lcfz;

    invoke-interface {v3}, Lcfz;->d()V

    sget-object v3, Lcbh;->a:Ljavax/crypto/KeyGenerator;

    if-nez v3, :cond_5

    new-instance v0, Lbud;

    const-string v1, "KeyGenerator not initialized."

    invoke-direct {v0, v1}, Lbud;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    iget-object v1, v4, Lbst;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->f()V

    iget-object v1, v4, Lbst;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_5
    sget-object v3, Lcbh;->a:Ljavax/crypto/KeyGenerator;

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v6

    iget-object v7, v2, Lcfx;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v2, v4, Lbst;->a:Lcfz;

    invoke-interface {v2, v8}, Lcfz;->e(Ljava/lang/String;)V
    :try_end_7
    .catch Lbud; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {v4, v7, v2}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v4, v8, v3}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-static {v6, v2}, Lcbr;->a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;

    move-result-object v2

    invoke-static {v9, v2}, Lcbr;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iget-object v2, v4, Lbst;->a:Lcfz;

    invoke-interface {v2, v5}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v9

    if-nez v6, :cond_7

    move v3, v0

    :goto_3
    if-nez v8, :cond_8

    move v2, v0

    :goto_4
    if-ne v3, v2, :cond_9

    move v2, v0

    :goto_5
    const-string v3, "encryptionKey must be set if and only if sharedFilename is set."

    invoke-static {v2, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    if-nez v8, :cond_6

    iget-object v2, v9, Lcfx;->b:Ljava/lang/String;

    if-eqz v2, :cond_a

    :goto_6
    const-string v1, "internal and shared filenames cannot both be null"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_6
    iput-object v8, v9, Lcfx;->c:Ljava/lang/String;

    iput-object v6, v9, Lcfx;->d:Ljavax/crypto/SecretKey;

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcfx;->a(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcfx;->k()V

    const-string v0, "ContentManager"

    const-string v1, "Moved from internal storage (filename %s) to shared storage (filename %s): %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    const/4 v3, 0x1

    aput-object v8, v2, v3

    const/4 v3, 0x2

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    iget-object v0, v4, Lbst;->a:Lcfz;

    invoke-interface {v0, v8}, Lcfz;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    move v3, v1

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_4

    :cond_9
    move v2, v1

    goto :goto_5

    :cond_a
    move v0, v1

    goto :goto_6

    :catchall_1
    move-exception v0

    iget-object v1, v4, Lbst;->a:Lcfz;

    invoke-interface {v1, v8}, Lcfz;->f(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catch Lbud; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
.end method

.method final b()V
    .locals 9

    iget-object v0, p0, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->d()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->j()J

    move-result-wide v1

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->p()J

    move-result-wide v3

    const-string v0, "ContentMaintenance"

    const-string v5, "Shared cache bytes used: %d; limit: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v5, v6}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    cmp-long v0, v3, v1

    if-lez v0, :cond_0

    const-string v0, "ContentMaintenance"

    const-string v3, "Evicting LRU items from shared cache..."

    invoke-static {v0, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->o()Lcgs;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfx;

    iget-object v5, p0, Lbsr;->a:Lcfz;

    invoke-interface {v5}, Lcfz;->p()J

    move-result-wide v5

    cmp-long v5, v5, v1

    if-gtz v5, :cond_2

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v3}, Lcgs;->close()V

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v5, p0, Lbsr;->b:Lcby;

    invoke-interface {v5}, Lcby;->d()Ljava/io/File;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v5, "ContentMaintenance"

    const-string v6, "External storage removed while pruning shared cache; aborting."

    invoke-static {v5, v6}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Evicting from shared cache: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcfx;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Lcgs;->close()V

    iget-object v1, p0, Lbsr;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_4
    :try_start_2
    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v3}, Lcgs;->close()V

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 8

    const/4 v1, 0x0

    const-string v0, "ContentMaintenance"

    const-string v2, "Beginning garbage collection."

    invoke-static {v0, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "collectGarbage() must not be run while in a database transaction"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    iget-object v5, p0, Lbsr;->a:Lcfz;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcfz;->g(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Deleting (internal): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Keeping (internal): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "ContentMaintenance"

    const-string v3, "Unable to open internal content directory; skipping internal content garbage collection."

    invoke-static {v2, v0, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->d()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v2, v0

    :goto_3
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    iget-object v4, p0, Lbsr;->a:Lcfz;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcfz;->g(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    const-string v4, "ContentMaintenance"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Deleting (shared): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    const-string v4, "ContentMaintenance"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Keeping (shared): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_4
    const-string v0, "ContentMaintenance"

    const-string v1, "Finished garbage collection."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
