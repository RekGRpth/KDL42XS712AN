.class public final Lbty;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/InputStream;

.field public final b:Lbsp;

.field final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field final d:Z

.field final e:Lcom/google/android/gms/drive/DriveId;

.field final f:J

.field final g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field private final i:Lcfz;

.field private final j:J


# direct methods
.method private constructor <init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lbsp;ZLcom/google/android/gms/drive/DriveId;JLjava/lang/String;Ljava/lang/String;Lcfz;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbty;->a:Ljava/io/InputStream;

    iput-object p2, p0, Lbty;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object p3, p0, Lbty;->b:Lbsp;

    iput-boolean p4, p0, Lbty;->d:Z

    iput-object p5, p0, Lbty;->e:Lcom/google/android/gms/drive/DriveId;

    iput-wide p6, p0, Lbty;->f:J

    iput-object p8, p0, Lbty;->g:Ljava/lang/String;

    iput-object p9, p0, Lbty;->h:Ljava/lang/String;

    iput-object p10, p0, Lbty;->i:Lcfz;

    iput-wide p11, p0, Lbty;->j:J

    return-void
.end method

.method public static a(Lcfp;Lcoy;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLbsp;)Lbty;
    .locals 15

    invoke-virtual/range {p1 .. p1}, Lcoy;->f()Lcfz;

    move-result-object v12

    move-wide/from16 v0, p2

    invoke-interface {v12, v0, v1}, Lcfz;->b(J)Lcge;

    move-result-object v4

    iget-object v2, v4, Lcge;->a:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcoy;->n()Lbst;

    move-result-object v3

    invoke-virtual {v3, v2}, Lbst;->c(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {p0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v7

    :goto_0
    iget-object v2, v4, Lcge;->a:Ljava/lang/String;

    invoke-interface {v12, v2}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v2

    iget-wide v8, v2, Lcfx;->h:J

    invoke-virtual {p0}, Lcfp;->e()Ljava/lang/String;

    move-result-object v10

    new-instance v2, Lbty;

    iget-object v11, v4, Lcge;->b:Ljava/lang/String;

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move/from16 v6, p5

    move-wide/from16 v13, p2

    invoke-direct/range {v2 .. v14}, Lbty;-><init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lbsp;ZLcom/google/android/gms/drive/DriveId;JLjava/lang/String;Ljava/lang/String;Lcfz;J)V

    return-object v2

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lbty;->h:Ljava/lang/String;

    iget-object v0, p0, Lbty;->i:Lcfz;

    iget-wide v1, p0, Lbty;->j:J

    invoke-interface {v0, v1, v2}, Lcfz;->b(J)Lcge;

    move-result-object v0

    iget-object v1, p0, Lbty;->h:Ljava/lang/String;

    iput-object v1, v0, Lcge;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcge;->k()V

    return-void
.end method
