.class public final Lhmr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhms;


# instance fields
.field private final a:Lhmt;

.field private final b:Lhna;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhmt;

    invoke-direct {v0}, Lhmt;-><init>()V

    iput-object v0, p0, Lhmr;->a:Lhmt;

    invoke-static {}, Lhna;->a()Lhna;

    move-result-object v0

    iput-object v0, p0, Lhmr;->b:Lhna;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    const-wide v0, 0xbebc2000L

    return-wide v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 23

    move-object/from16 v0, p0

    iget-object v2, v0, Lhmr;->a:Lhmt;

    invoke-static/range {p1 .. p1}, Lhmt;->a(Ljava/util/List;)[D

    move-result-object v15

    invoke-static {v15}, Lhmt;->a([D)Lhmw;

    move-result-object v16

    move-object/from16 v0, v16

    iget-wide v2, v0, Lhmw;->a:D

    invoke-static {v15, v2, v3}, Lhmt;->b([DD)D

    move-result-wide v7

    move-object/from16 v0, v16

    iget-wide v2, v0, Lhmw;->b:D

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    move-object/from16 v0, v16

    iget-wide v4, v0, Lhmw;->a:D

    div-double v3, v2, v4

    array-length v2, v15

    const/4 v5, 0x2

    if-gt v2, v5, :cond_0

    const-wide/16 v13, 0x0

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x3

    filled-new-array {v2, v5}, [I

    move-result-object v2

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    new-instance v10, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v10, v5}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhng;

    const/4 v9, 0x0

    :goto_2
    const/4 v11, 0x3

    if-ge v9, v11, :cond_5

    aget-object v11, v2, v6

    iget-object v12, v5, Lhng;->b:[F

    aget v12, v12, v9

    float-to-double v0, v12

    move-wide/from16 v17, v0

    aput-wide v17, v11, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_0
    const/4 v2, 0x1

    aget-wide v5, v15, v2

    const/4 v2, 0x0

    aget-wide v9, v15, v2

    sub-double/2addr v5, v9

    const-wide/16 v9, 0x0

    cmpl-double v2, v5, v9

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    :goto_3
    const/4 v5, 0x1

    aget-wide v9, v15, v5

    const/4 v6, 0x0

    const/4 v5, 0x2

    move/from16 v22, v5

    move v5, v6

    move v6, v2

    move/from16 v2, v22

    :goto_4
    array-length v11, v15

    if-ge v2, v11, :cond_4

    aget-wide v11, v15, v2

    sub-double v9, v11, v9

    const-wide/16 v13, 0x0

    cmpl-double v9, v9, v13

    if-ltz v9, :cond_3

    const/4 v9, 0x1

    :goto_5
    if-eq v6, v9, :cond_1

    add-int/lit8 v5, v5, 0x1

    move v6, v9

    :cond_1
    add-int/lit8 v2, v2, 0x1

    move-wide v9, v11

    goto :goto_4

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    const/4 v9, 0x0

    goto :goto_5

    :cond_4
    int-to-double v5, v5

    array-length v2, v15

    int-to-double v9, v2

    div-double v13, v5, v9

    goto :goto_0

    :cond_5
    iget-wide v11, v5, Lhng;->a:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v10, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    :cond_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    const/4 v5, 0x0

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v5, v11, v5

    new-instance v9, Lhne;

    invoke-direct {v9}, Lhne;-><init>()V

    invoke-virtual {v9, v2, v10, v5, v6}, Lhne;->a([[DLjava/util/List;J)[D

    move-result-object v2

    const/16 v9, 0x40

    new-array v9, v9, [D

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x40

    invoke-static {v2, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v10, v2

    add-int/lit8 v10, v10, -0x40

    new-array v11, v10, [D

    const/16 v12, 0x40

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v2, v12, v11, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v18, Lhmv;

    move-object/from16 v0, v18

    invoke-direct {v0, v9, v11, v5, v6}, Lhmv;-><init>([D[DJ)V

    move-object/from16 v0, v18

    iget-object v2, v0, Lhmv;->a:[D

    invoke-static {v2}, Lhmt;->a([D)Lhmw;

    move-result-object v2

    iget-wide v5, v2, Lhmw;->b:D

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    iget-wide v9, v2, Lhmw;->a:D

    div-double v11, v5, v9

    move-object/from16 v0, v18

    iget-object v2, v0, Lhmv;->a:[D

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v2, v2, [D

    move-object/from16 v0, v18

    iget-object v5, v0, Lhmv;->a:[D

    const/4 v6, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, v18

    iget-object v10, v0, Lhmv;->a:[D

    array-length v10, v10

    add-int/lit8 v10, v10, -0x1

    invoke-static {v5, v6, v2, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, v18

    iget-wide v5, v0, Lhmv;->c:J

    invoke-static {v2, v5, v6}, Lhmt;->a([DJ)[D

    move-result-object v19

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhng;

    iget-wide v5, v2, Lhng;->a:J

    const-wide/32 v9, 0x3b9aca00

    add-long/2addr v5, v9

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6, v2}, Lhmt;->a(Ljava/util/List;JZ)[D

    move-result-object v20

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhng;

    iget-wide v5, v2, Lhng;->a:J

    const-wide/32 v9, 0x3b9aca00

    sub-long/2addr v5, v9

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6, v2}, Lhmt;->a(Ljava/util/List;JZ)[D

    move-result-object v21

    move-object/from16 v0, v16

    iget-wide v5, v0, Lhmw;->a:D

    invoke-static {v15, v5, v6}, Lhmt;->a([DD)[D

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lhmt;->a([D)Lhmw;

    move-result-object v2

    iget-wide v5, v2, Lhmw;->b:D

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    iget-wide v9, v2, Lhmw;->a:D

    div-double/2addr v5, v9

    iget-wide v9, v2, Lhmw;->a:D

    move-object/from16 v0, v16

    invoke-static {v0, v9, v10}, Lhmt;->b([DD)D

    move-result-wide v9

    invoke-static {v15}, Ljava/util/Arrays;->sort([D)V

    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->sort([D)V

    new-instance v2, Lhmx;

    move-object/from16 v0, v18

    iget-object v0, v0, Lhmv;->a:[D

    move-object/from16 v17, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lhmv;->b:[D

    move-object/from16 v18, v0

    invoke-direct/range {v2 .. v21}, Lhmx;-><init>(DDDDDD[D[D[D[D[D[D[D)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhmr;->b:Lhna;

    invoke-virtual {v3, v2}, Lhna;->a(Lhmx;)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method
