.class public abstract Lhlz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic b:Lhlu;


# direct methods
.method constructor <init>(Lhlu;)V
    .locals 0

    iput-object p1, p0, Lhlz;->b:Lhlu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 0

    return-void
.end method

.method protected a(Lhlz;)V
    .locals 5

    const-wide/16 v3, -0x1

    iget-object v0, p0, Lhlz;->b:Lhlu;

    iget-wide v1, v0, Lhlu;->e:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "VehExitDetector"

    const-string v2, "Alarm canceled"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, v0, Lhlu;->a:Lidu;

    const/16 v2, 0xb

    invoke-interface {v1, v2}, Lidu;->a(I)V

    iput-wide v3, v0, Lhlu;->e:J

    :cond_1
    return-void
.end method

.method protected a(Z)V
    .locals 4

    if-nez p1, :cond_0

    iget-object v0, p0, Lhlz;->b:Lhlu;

    new-instance v1, Lhly;

    iget-object v2, p0, Lhlz;->b:Lhlu;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lhly;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    :cond_0
    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected c()V
    .locals 0

    return-void
.end method
