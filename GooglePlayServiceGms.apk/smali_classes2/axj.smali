.class abstract Laxj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Laye;


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Ljava/net/MulticastSocket;

.field private final d:[B

.field private e:Ljava/lang/Thread;

.field private f:Ljava/lang/Thread;

.field private volatile g:Z

.field private final h:Ljava/net/NetworkInterface;

.field private final i:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbbj;

    const-string v1, "MdnsClient"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Laxj;->a:Laye;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/net/NetworkInterface;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, Laxj;->d:[B

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Laxj;->i:Ljava/util/Set;

    iput-object p1, p0, Laxj;->b:Ljava/lang/String;

    iput-object p2, p0, Laxj;->h:Ljava/net/NetworkInterface;

    return-void
.end method

.method static synthetic a(Laxj;)V
    .locals 22

    const/4 v3, 0x4

    new-array v5, v3, [B

    const/16 v3, 0x10

    new-array v6, v3, [B

    new-instance v7, Ljava/net/DatagramPacket;

    move-object/from16 v0, p0

    iget-object v3, v0, Laxj;->d:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Laxj;->d:[B

    array-length v4, v4

    invoke-direct {v7, v3, v4}, Ljava/net/DatagramPacket;-><init>([BI)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Laxj;->g:Z

    if-nez v3, :cond_e

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Laxj;->c:Ljava/net/MulticastSocket;

    invoke-virtual {v3, v7}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    sget-object v3, Laxj;->a:Laye;

    const-string v4, "received a packet of length %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getLength()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v4, v8}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v8, Laxt;

    invoke-direct {v8, v7}, Laxt;-><init>(Ljava/net/DatagramPacket;)V

    invoke-virtual {v8}, Laxt;->b()I

    invoke-virtual {v8}, Laxt;->b()I

    invoke-virtual {v8}, Laxt;->b()I

    move-result v3

    invoke-virtual {v8}, Laxt;->b()I

    move-result v9

    invoke-virtual {v8}, Laxt;->b()I

    move-result v10

    invoke-virtual {v8}, Laxt;->b()I

    move-result v11

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    new-instance v3, Ljava/io/IOException;

    const-string v4, "invalid response"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Laxj;->g:Z

    if-nez v4, :cond_0

    sget-object v4, Laxj;->a:Laye;

    const-string v8, "while receiving packet"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v3, v8, v9}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v3, "."

    invoke-virtual {v8}, Laxt;->c()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Laxt;->b()I

    invoke-virtual {v8}, Laxt;->b()I

    new-instance v12, Laxw;

    invoke-direct {v12, v3}, Laxw;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    add-int v3, v9, v10

    add-int/2addr v3, v11

    if-ge v4, v3, :cond_d

    invoke-virtual {v8}, Laxt;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Laxt;->b()I

    move-result v13

    invoke-virtual {v8}, Laxt;->b()I

    const/4 v14, 0x4

    invoke-virtual {v8, v14}, Laxt;->a(I)V

    iget-object v14, v8, Laxt;->a:[B

    iget v15, v8, Laxt;->b:I

    add-int/lit8 v16, v15, 0x1

    move/from16 v0, v16

    iput v0, v8, Laxt;->b:I

    aget-byte v14, v14, v15

    and-int/lit16 v14, v14, 0xff

    int-to-long v14, v14

    const/16 v16, 0x18

    shl-long v14, v14, v16

    iget-object v0, v8, Laxt;->a:[B

    move-object/from16 v16, v0

    iget v0, v8, Laxt;->b:I

    move/from16 v17, v0

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v18

    iput v0, v8, Laxt;->b:I

    aget-byte v16, v16, v17

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    const/16 v18, 0x10

    shl-long v16, v16, v18

    or-long v14, v14, v16

    iget-object v0, v8, Laxt;->a:[B

    move-object/from16 v16, v0

    iget v0, v8, Laxt;->b:I

    move/from16 v17, v0

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v18

    iput v0, v8, Laxt;->b:I

    aget-byte v16, v16, v17

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    const/16 v18, 0x8

    shl-long v16, v16, v18

    or-long v14, v14, v16

    iget-object v0, v8, Laxt;->a:[B

    move-object/from16 v16, v0

    iget v0, v8, Laxt;->b:I

    move/from16 v17, v0

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v18

    iput v0, v8, Laxt;->b:I

    aget-byte v16, v16, v17

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    or-long v14, v14, v16

    invoke-virtual {v8}, Laxt;->b()I

    move-result v16

    if-ge v4, v9, :cond_2

    const-wide/16 v17, 0x0

    cmp-long v17, v14, v17

    if-lez v17, :cond_2

    const-wide/32 v17, 0x93a80

    cmp-long v17, v14, v17

    if-gez v17, :cond_2

    iget v0, v8, Laxt;->b:I

    move/from16 v17, v0

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v18

    add-int v19, v17, v16

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Laxj;->i:Ljava/util/Set;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/ref/SoftReference;

    new-instance v20, Laxm;

    long-to-int v0, v14

    move/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Laxm;-><init>([BI)V

    invoke-direct/range {v19 .. v20}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface/range {v18 .. v19}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, v12, Laxw;->a:Laxv;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Laxv;->l:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-ltz v18, :cond_3

    move-object/from16 v0, v17

    iget-wide v0, v0, Laxv;->l:J

    move-wide/from16 v18, v0

    cmp-long v18, v14, v18

    if-gez v18, :cond_4

    :cond_3
    move-object/from16 v0, v17

    iput-wide v14, v0, Laxv;->l:J

    :cond_4
    sparse-switch v13, :sswitch_data_0

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Laxt;->a(I)V

    iget v3, v8, Laxt;->b:I

    add-int v3, v3, v16

    iput v3, v8, Laxt;->b:I

    :cond_5
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_1

    :sswitch_0
    invoke-virtual {v8, v5}, Laxt;->a([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-static {v5}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v3

    check-cast v3, Ljava/net/Inet4Address;

    iget-object v13, v12, Laxw;->a:Laxv;

    iget-object v14, v13, Laxv;->b:Ljava/util/List;

    if-nez v14, :cond_6

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    iput-object v14, v13, Laxv;->b:Ljava/util/List;

    :cond_6
    iget-object v13, v13, Laxv;->b:Ljava/util/List;

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_2

    :sswitch_1
    :try_start_3
    invoke-virtual {v8, v6}, Laxt;->a([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    invoke-static {v6}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v3

    check-cast v3, Ljava/net/Inet6Address;

    iget-object v13, v12, Laxw;->a:Laxv;

    iget-object v14, v13, Laxv;->c:Ljava/util/List;

    if-nez v14, :cond_7

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    iput-object v14, v13, Laxv;->c:Ljava/util/List;

    :cond_7
    iget-object v13, v13, Laxv;->c:Ljava/util/List;

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_2
    move-exception v3

    goto :goto_2

    :sswitch_2
    :try_start_5
    invoke-virtual {v8}, Laxt;->c()[Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    invoke-virtual {v8}, Laxt;->b()I

    move-result v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput v13, v14, Laxv;->i:I

    invoke-virtual {v8}, Laxt;->b()I

    move-result v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput v13, v14, Laxv;->j:I

    invoke-virtual {v8}, Laxt;->b()I

    move-result v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput v13, v14, Laxv;->h:I

    const-string v13, "."

    invoke-virtual {v8}, Laxt;->c()[Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput-object v13, v14, Laxv;->f:Ljava/lang/String;

    array-length v13, v3

    const/4 v14, 0x4

    if-eq v13, v14, :cond_8

    new-instance v3, Ljava/io/IOException;

    const-string v4, "invalid name in SRV record"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    const/4 v13, 0x0

    aget-object v13, v3, v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput-object v13, v14, Laxv;->e:Ljava/lang/String;

    const/4 v13, 0x1

    aget-object v13, v3, v13

    iget-object v14, v12, Laxw;->a:Laxv;

    iput-object v13, v14, Laxv;->d:Ljava/lang/String;

    const/4 v13, 0x2

    aget-object v3, v3, v13

    const-string v13, "_tcp"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Laxw;->a(I)Laxw;

    goto/16 :goto_2

    :cond_9
    const-string v13, "_udp"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x2

    invoke-virtual {v12, v3}, Laxw;->a(I)Laxw;

    goto/16 :goto_2

    :sswitch_4
    if-ltz v16, :cond_a

    iget v3, v8, Laxt;->b:I

    add-int v3, v3, v16

    iput v3, v8, Laxt;->c:I

    :cond_a
    :goto_3
    invoke-virtual {v8}, Laxt;->a()I

    move-result v3

    if-lez v3, :cond_c

    invoke-virtual {v8}, Laxt;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v13, v12, Laxw;->a:Laxv;

    iget-object v14, v13, Laxv;->k:Ljava/util/List;

    if-nez v14, :cond_b

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    iput-object v14, v13, Laxv;->k:Ljava/util/List;

    :cond_b
    iget-object v13, v13, Laxv;->k:Ljava/util/List;

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_c
    const/4 v3, -0x1

    iput v3, v8, Laxt;->c:I

    goto/16 :goto_2

    :cond_d
    iget-object v3, v12, Laxw;->a:Laxv;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Laxj;->a(Laxv;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    :cond_e
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xc -> :sswitch_2
        0x10 -> :sswitch_4
        0x1c -> :sswitch_1
        0x21 -> :sswitch_3
    .end sparse-switch
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Laxj;)V
    .locals 13

    const/4 v1, 0x1

    const/16 v0, 0x3e8

    move v2, v1

    move v1, v0

    :goto_0
    iget-boolean v0, p0, Laxj;->g:Z

    if-nez v0, :cond_a

    :try_start_0
    iget-object v4, p0, Laxj;->b:Ljava/lang/String;

    new-instance v5, Laxu;

    invoke-direct {v5}, Laxu;-><init>()V

    const/16 v0, 0x400

    iget-object v3, p0, Laxj;->i:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x400

    if-gt v3, v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxm;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, v0, Laxm;->b:J

    sub-long/2addr v7, v9

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    long-to-double v7, v7

    iget v0, v0, Laxm;->c:I

    int-to-double v9, v0

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    div-double/2addr v9, v11

    cmpl-double v0, v7, v9

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    :cond_1
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    iget-object v0, p0, Laxj;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    const-string v0, "\\."

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_4

    aget-object v6, v3, v0

    sget-object v7, Laxn;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    array-length v7, v6

    invoke-virtual {v5, v7}, Laxu;->a(I)V

    invoke-virtual {v5, v6}, Laxu;->a([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Laxu;->a(I)V

    const/16 v0, 0xc

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    if-eqz v2, :cond_6

    const v0, 0x8000

    :goto_4
    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v5, v0}, Laxu;->b(I)V

    iget-object v0, p0, Laxj;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxm;

    if-eqz v0, :cond_5

    iget-object v0, v0, Laxm;->a:[B

    invoke-virtual {v5, v0}, Laxu;->a([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    :goto_6
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    int-to-long v3, v1

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v0, 0x3e80

    if-ge v1, v0, :cond_b

    mul-int/lit8 v0, v1, 0x2

    :goto_7
    move v1, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    :try_start_2
    invoke-virtual {v5}, Laxu;->a()Ljava/net/DatagramPacket;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v5

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getLength()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    array-length v0, v5

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_8
    add-int/lit8 v8, v6, 0x0

    if-ge v0, v8, :cond_9

    const-string v8, "%02X "

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aget-byte v11, v5, v0

    and-int/lit16 v11, v11, 0xff

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v8, v3, 0x8

    if-nez v8, :cond_8

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    sget-object v0, Laxj;->a:Laye;

    const-string v3, "packet:\n%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;

    invoke-virtual {v0, v4}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v2, 0x0

    goto :goto_6

    :cond_a
    return-void

    :catch_1
    move-exception v0

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto :goto_7
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Laxj;->g:Z

    new-instance v0, Ljava/net/MulticastSocket;

    invoke-direct {v0}, Ljava/net/MulticastSocket;-><init>()V

    iput-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;

    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    iget-object v0, p0, Laxj;->h:Ljava/net/NetworkInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;

    iget-object v1, p0, Laxj;->h:Ljava/net/NetworkInterface;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Laxk;

    invoke-direct {v1, p0}, Laxk;-><init>(Laxj;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Laxj;->e:Ljava/lang/Thread;

    iget-object v0, p0, Laxj;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Laxl;

    invoke-direct {v1, p0}, Laxl;-><init>(Laxj;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Laxj;->f:Ljava/lang/Thread;

    iget-object v0, p0, Laxj;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(Laxv;)V
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Laxj;->g:Z

    iget-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;

    invoke-virtual {v0}, Ljava/net/MulticastSocket;->close()V

    iget-object v0, p0, Laxj;->e:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laxj;->e:Ljava/lang/Thread;

    invoke-static {v0}, Laxj;->a(Ljava/lang/Thread;)V

    const/4 v0, 0x0

    iput-object v0, p0, Laxj;->e:Ljava/lang/Thread;

    :cond_1
    iget-object v0, p0, Laxj;->f:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Laxj;->f:Ljava/lang/Thread;

    invoke-static {v0}, Laxj;->a(Ljava/lang/Thread;)V

    const/4 v0, 0x0

    iput-object v0, p0, Laxj;->f:Ljava/lang/Thread;

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Laxj;->c:Ljava/net/MulticastSocket;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
