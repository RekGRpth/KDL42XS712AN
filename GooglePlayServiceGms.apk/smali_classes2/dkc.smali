.class final Ldkc;
.super Lbqh;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldkb;


# direct methods
.method private constructor <init>(Ldkb;)V
    .locals 0

    iput-object p1, p0, Ldkc;->a:Ldkb;

    invoke-direct {p0}, Lbqh;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldkb;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldkc;-><init>(Ldkb;)V

    return-void
.end method

.method private b(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldkg;

    iget-object v1, p0, Ldkc;->a:Ldkb;

    iget-object v2, v0, Ldkg;->d:Ldkh;

    invoke-static {v1, v2}, Ldkb;->a(Ldkb;Ldkh;)Ldkh;

    iget-object v1, p0, Ldkc;->a:Ldkb;

    iget-boolean v2, v0, Ldkg;->c:Z

    invoke-static {v1, v2}, Ldkb;->a(Ldkb;Z)Z

    iget-object v1, p0, Ldkc;->a:Ldkb;

    iget-object v2, v0, Ldkg;->a:Ljava/lang/String;

    iput-object v2, v1, Ldkb;->f:Ljava/lang/String;

    iget-object v1, p0, Ldkc;->a:Ldkb;

    iget-object v0, v0, Ldkg;->b:Ljava/lang/String;

    iput-object v0, v1, Ldkb;->g:Ljava/lang/String;

    iget-object v0, p0, Ldkc;->a:Ldkb;

    invoke-static {v0}, Ldkb;->a(Ldkb;)Ldkk;

    move-result-object v0

    iget-object v1, p0, Ldkc;->a:Ldkb;

    iget-object v1, v1, Ldkb;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldkk;->e(Ljava/lang/String;)V

    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v1, p0, Ldkc;->a:Ldkb;

    invoke-static {v1}, Ldkb;->e(Ldkb;)Ldke;

    move-result-object v1

    invoke-static {v0, v1}, Ldkb;->e(Ldkb;Lbqg;)V

    return-void
.end method

.method private d()V
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Ldkc;->a:Ldkb;

    invoke-static {v0}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v0

    invoke-virtual {v0}, Ldkh;->a()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldki;

    iget-object v5, v0, Ldki;->a:Ljava/lang/String;

    iget-object v6, p0, Ldkc;->a:Ldkb;

    iget-object v6, v6, Ldkb;->g:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget v5, v0, Ldki;->c:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldkc;->a:Ldkb;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Ldkb;->a(ILjava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 5

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v0, v0, Ldkb;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkc;->a:Ldkb;

    const-string v2, "REALTIME_ABANDONED"

    invoke-static {v0, p1, v2}, Ldkb;->a(Ldkb;Landroid/os/Message;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v2, p0, Ldkc;->a:Ldkb;

    invoke-static {v2}, Ldkb;->d(Ldkb;)Ldkd;

    move-result-object v2

    invoke-static {v0, v2}, Ldkb;->b(Ldkb;Lbqg;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v0, v0, Ldkb;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkc;->a:Ldkb;

    const-string v2, "REALTIME_SERVER_CONNECTION_FAILURE"

    invoke-static {v0, p1, v2}, Ldkb;->a(Ldkb;Landroid/os/Message;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v2, p0, Ldkc;->a:Ldkb;

    invoke-static {v2}, Ldkb;->d(Ldkb;)Ldkd;

    move-result-object v2

    invoke-static {v0, v2}, Ldkb;->c(Ldkb;Lbqg;)V

    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldjw;

    iget-object v0, v0, Ldjw;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v2, p0, Ldkc;->a:Ldkb;

    iget-object v2, v2, Ldkb;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Ldkc;->a:Ldkb;

    const-string v2, "PLAYER_LEFT"

    invoke-static {v0, p1, v2}, Ldkb;->a(Ldkb;Landroid/os/Message;Ljava/lang/String;)V

    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v2, p0, Ldkc;->a:Ldkb;

    invoke-static {v2}, Ldkb;->d(Ldkb;)Ldkd;

    move-result-object v2

    invoke-static {v0, v2}, Ldkb;->d(Ldkb;Lbqg;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The room id is not the current room: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Ldkc;->a:Ldkb;

    invoke-static {v0, p1}, Ldkb;->a(Ldkb;Landroid/os/Message;)V

    move v0, v1

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Ldkc;->b(Landroid/os/Message;)V

    move v0, v1

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Ldkc;->b(Landroid/os/Message;)V

    invoke-direct {p0}, Ldkc;->d()V

    move v0, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Ldkc;->a:Ldkb;

    iget-object v0, v0, Ldkb;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
