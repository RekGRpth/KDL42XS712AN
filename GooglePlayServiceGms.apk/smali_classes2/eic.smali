.class public final Leic;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile b:[Leic;


# instance fields
.field public a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Leic;->a:[I

    const/4 v0, -0x1

    iput v0, p0, Leic;->C:I

    return-void
.end method

.method public static c()[Leic;
    .locals 2

    sget-object v0, Leic;->b:[Leic;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Leic;->b:[Leic;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Leic;

    sput-object v0, Leic;->b:[Leic;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Leic;->b:[Leic;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v2

    iget-object v1, p0, Leic;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Leic;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    move v1, v0

    :goto_0
    iget-object v3, p0, Leic;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Leic;->a:[I

    aget v3, v3, v0

    invoke-static {v3}, Lizn;->d(I)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int v0, v2, v1

    iget-object v1, p0, Leic;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :goto_1
    iput v0, p0, Leic;->C:I

    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 5

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leic;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Leic;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lizm;->g()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Leic;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Leic;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lizm;->g()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lizm;->e(I)V

    iget-object v2, p0, Leic;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Leic;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Leic;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Leic;->a:[I

    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Leic;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Leic;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Leic;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Leic;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lizn;->c(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leic;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leic;

    iget-object v2, p0, Leic;->a:[I

    iget-object v3, p1, Leic;->a:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Leic;->a:[I

    invoke-static {v0}, Lizq;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    return v0
.end method
