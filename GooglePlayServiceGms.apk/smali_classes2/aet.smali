.class final Laet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laeo;
.implements Laep;
.implements Lafs;


# instance fields
.field volatile a:J

.field volatile b:Laew;

.field final c:Ljava/util/Queue;

.field volatile d:Ljava/util/Timer;

.field e:Lbpe;

.field f:J

.field private volatile g:Lael;

.field private final h:Laeq;

.field private final i:Landroid/content/Context;

.field private volatile j:I

.field private volatile k:Ljava/util/Timer;

.field private volatile l:Ljava/util/Timer;

.field private m:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Laeq;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Laet;->c:Ljava/util/Queue;

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Laet;->f:J

    iput-object p1, p0, Laet;->i:Landroid/content/Context;

    iput-object p2, p0, Laet;->h:Laeq;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    iput-object v0, p0, Laet;->e:Lbpe;

    const/4 v0, 0x0

    iput v0, p0, Laet;->j:I

    sget-object v0, Laew;->f:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    return-void
.end method

.method private static a(Ljava/util/Timer;)Ljava/util/Timer;
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Timer;->cancel()V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Laet;->k:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->k:Ljava/util/Timer;

    iget-object v0, p0, Laet;->l:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->l:Ljava/util/Timer;

    iget-object v0, p0, Laet;->d:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->d:Ljava/util/Timer;

    return-void
.end method

.method private i()V
    .locals 4

    iget-object v0, p0, Laet;->k:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->k:Ljava/util/Timer;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Service Reconnect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Laet;->k:Ljava/util/Timer;

    iget-object v0, p0, Laet;->k:Ljava/util/Timer;

    new-instance v1, Lafa;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lafa;-><init>(Laet;B)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laet;->l:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->l:Ljava/util/Timer;

    const/4 v0, 0x0

    iput v0, p0, Laet;->j:I

    const-string v0, "Connected to service"

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    sget-object v0, Laew;->b:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    invoke-virtual {p0}, Laet;->d()V

    iget-object v0, p0, Laet;->d:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Laet;->d:Ljava/util/Timer;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "disconnect check"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Laet;->d:Ljava/util/Timer;

    iget-object v0, p0, Laet;->d:Ljava/util/Timer;

    new-instance v1, Laex;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Laex;-><init>(Laet;B)V

    iget-wide v2, p0, Laet;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connection to service failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lafl;->e(Ljava/lang/String;)I

    sget-object v0, Laew;->d:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    iget v0, p0, Laet;->j:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Laet;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Laet;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .locals 7

    const-string v0, "putHit called"

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    iget-object v6, p0, Laet;->c:Ljava/util/Queue;

    new-instance v0, Laez;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Laez;-><init>(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Laet;->d()V

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laet;->b:Laew;

    sget-object v1, Laew;->e:Laew;

    if-ne v0, v1, :cond_0

    const-string v0, "Disconnected from service"

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    invoke-direct {p0}, Laet;->h()V

    sget-object v0, Laew;->f:Laew;

    iput-object v0, p0, Laet;->b:Laew;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "Unexpected disconnect."

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    sget-object v0, Laew;->d:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    iget v0, p0, Laet;->j:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    invoke-direct {p0}, Laet;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Laet;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Laet;->g:Lael;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Laem;

    iget-object v1, p0, Laet;->i:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Laem;-><init>(Landroid/content/Context;Laeo;Laep;)V

    iput-object v0, p0, Laet;->g:Lael;

    invoke-virtual {p0}, Laet;->f()V

    goto :goto_0
.end method

.method final declared-synchronized d()V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Laet;->h:Laeq;

    invoke-interface {v2}, Laeq;->b()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Laet;->h:Laeq;

    invoke-interface {v1}, Laeq;->a()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v1

    new-instance v2, Laeu;

    invoke-direct {v2, p0}, Laeu;-><init>(Laet;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v1, p0, Laet;->m:Z

    if-eqz v1, :cond_2

    const-string v1, "clearHits called"

    invoke-static {v1}, Lafl;->d(Ljava/lang/String;)I

    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    sget-object v1, Laev;->a:[I

    iget-object v2, p0, Laet;->b:Laew;

    invoke-virtual {v2}, Laew;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Laet;->m:Z

    :cond_2
    :goto_1
    sget-object v1, Laev;->a:[I

    iget-object v2, p0, Laet;->b:Laew;

    invoke-virtual {v2}, Laew;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :goto_2
    :pswitch_0
    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Laez;

    move-object v6, v0

    const-string v1, "Sending hit to service"

    invoke-static {v1}, Lafl;->d(Ljava/lang/String;)I

    iget-object v1, p0, Laet;->g:Lael;

    iget-object v2, v6, Laez;->a:Ljava/util/Map;

    iget-wide v3, v6, Laez;->b:J

    iget-object v5, v6, Laez;->c:Ljava/lang/String;

    iget-object v6, v6, Laez;->d:Ljava/util/List;

    invoke-interface/range {v1 .. v6}, Lael;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_1
    :try_start_2
    iget-object v1, p0, Laet;->g:Lael;

    invoke-interface {v1}, Lael;->a()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Laet;->m:Z

    goto :goto_1

    :pswitch_2
    const-string v1, "analytics is blocked, discarding queued hits."

    invoke-static {v1}, Lafl;->d(Ljava/lang/String;)I

    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Laet;->e:Lbpe;

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    iput-wide v1, p0, Laet;->a:J

    goto :goto_0

    :pswitch_3
    const-string v1, "Need to reconnect"

    invoke-static {v1}, Lafl;->d(Ljava/lang/String;)I

    iget-object v1, p0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Laet;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final declared-synchronized e()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laet;->b:Laew;

    sget-object v1, Laew;->c:Laew;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Laet;->h()V

    const-string v0, "connection attempts failed, disabling analytics."

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    sget-object v0, Laew;->c:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    invoke-virtual {p0}, Laet;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laet;->g:Lael;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laet;->b:Laew;

    sget-object v1, Laew;->c:Laew;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    :try_start_1
    iget v0, p0, Laet;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laet;->j:I

    iget-object v0, p0, Laet;->l:Ljava/util/Timer;

    invoke-static {v0}, Laet;->a(Ljava/util/Timer;)Ljava/util/Timer;

    sget-object v0, Laew;->a:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Failed Connect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Laet;->l:Ljava/util/Timer;

    iget-object v0, p0, Laet;->l:Ljava/util/Timer;

    new-instance v1, Laey;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Laey;-><init>(Laet;B)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const-string v0, "connecting to Analytics service"

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    iget-object v0, p0, Laet;->g:Lael;

    invoke-interface {v0}, Lael;->b()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "security exception on connectToService"

    invoke-static {v0}, Lafl;->e(Ljava/lang/String;)I

    invoke-virtual {p0}, Laet;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    const-string v0, "client not initialized."

    invoke-static {v0}, Lafl;->e(Ljava/lang/String;)I

    invoke-virtual {p0}, Laet;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final declared-synchronized g()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laet;->g:Lael;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laet;->b:Laew;

    sget-object v1, Laew;->b:Laew;

    if-ne v0, v1, :cond_0

    sget-object v0, Laew;->e:Laew;

    iput-object v0, p0, Laet;->b:Laew;

    iget-object v0, p0, Laet;->g:Lael;

    invoke-interface {v0}, Lael;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
