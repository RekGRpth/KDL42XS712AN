.class final Ligb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhuf;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Liga;->c()Lhuf;

    move-result-object v0

    invoke-interface {v0, p1}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    invoke-static {v0}, Lige;->a(Livi;)Liga;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 12

    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    check-cast p1, Liga;

    invoke-virtual {p1}, Liga;->a()Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v2

    new-instance v3, Livi;

    sget-object v0, Lihj;->ai:Livk;

    invoke-direct {v3, v0}, Livi;-><init>(Livk;)V

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v7, v0}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v8, v0}, Livi;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Livi;

    sget-object v6, Lihj;->ae:Livk;

    invoke-direct {v5, v6}, Livi;-><init>(Livk;)V

    invoke-virtual {v5, v7, v1}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v5, v8, v0}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v3, v10, v5}, Livi;->a(ILivi;)V

    goto :goto_1

    :cond_1
    new-instance v0, Livi;

    sget-object v1, Lihj;->af:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->d()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-static {v1}, Lige;->a(Lcom/google/android/gms/maps/model/LatLng;)Livi;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->e()F

    move-result v1

    const/4 v4, 0x0

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->e()F

    move-result v1

    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v8, v1}, Livi;->e(II)Livi;

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->f()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->f()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    new-instance v4, Livi;

    sget-object v5, Lihj;->l:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    iget-object v5, v1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v5}, Lige;->a(Lcom/google/android/gms/maps/model/LatLng;)Livi;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, Livi;->b(ILivi;)Livi;

    iget-object v1, v1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lige;->a(Lcom/google/android/gms/maps/model/LatLng;)Livi;

    move-result-object v1

    invoke-virtual {v4, v8, v1}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v0, v9, v4}, Livi;->b(ILivi;)Livi;

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v10, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_4
    invoke-virtual {v3, v11, v0}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->g()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->g()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    invoke-virtual {v3, v0, v7}, Livi;->a(IZ)Livi;

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->i()F

    move-result v0

    float-to-double v0, v0

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-ltz v0, :cond_7

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->i()F

    move-result v0

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/16 v1, 0xb

    invoke-virtual {v3, v1, v0}, Livi;->e(II)Livi;

    :cond_7
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->j()I

    move-result v0

    if-ltz v0, :cond_8

    const/16 v0, 0xc

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->j()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Livi;->e(II)Livi;

    :cond_8
    const/16 v0, 0xe

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->k()J

    move-result-wide v1

    invoke-virtual {v3, v0, v1, v2}, Livi;->a(IJ)Livi;

    invoke-virtual {p1}, Liga;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;

    new-instance v4, Livi;

    sget-object v5, Lihj;->ad:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    invoke-virtual {v4, v7, v1}, Livi;->b(ILjava/lang/String;)Livi;

    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->b:Ljava/lang/String;

    invoke-static {v1}, Lige;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->b:Ljava/lang/String;

    invoke-virtual {v4, v8, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_9
    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->c:Ljava/lang/String;

    invoke-static {v1}, Lige;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->c:Ljava/lang/String;

    invoke-virtual {v4, v9, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_a
    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->d:Ljava/lang/String;

    invoke-static {v1}, Lige;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->d:Ljava/lang/String;

    invoke-virtual {v4, v10, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_b
    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->e:Ljava/lang/String;

    invoke-static {v1}, Lige;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->e:Ljava/lang/String;

    invoke-virtual {v4, v11, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_c
    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x7

    invoke-virtual {v4, v5, v0}, Livi;->a(ILjava/lang/String;)V

    goto :goto_3

    :cond_d
    invoke-virtual {v3, v9, v4}, Livi;->a(ILivi;)V

    goto :goto_2

    :cond_e
    invoke-static {}, Liga;->c()Lhuf;

    move-result-object v0

    invoke-interface {v0, v3, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    return-void
.end method
