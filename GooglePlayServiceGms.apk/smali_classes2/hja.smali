.class public final Lhja;
.super Lhin;
.source "SourceFile"


# static fields
.field static g:Z


# instance fields
.field A:Lhix;

.field B:J

.field private C:J

.field private D:J

.field h:J

.field i:J

.field j:Lidr;

.field k:J

.field l:I

.field m:Z

.field n:Z

.field o:Z

.field p:Lhtf;

.field q:Lhuv;

.field r:Lhjc;

.field s:Livi;

.field t:Z

.field u:J

.field v:Z

.field w:J

.field x:Z

.field y:Lidr;

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lhja;->g:Z

    return-void
.end method

.method public constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Liha;Lhkr;)V
    .locals 7

    const-string v1, "BurstCollector"

    sget-object v6, Lhir;->b:Lhir;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    const-wide/32 v0, 0xdbba0

    iput-wide v0, p0, Lhja;->C:J

    const-wide/32 v0, 0x75300

    iput-wide v0, p0, Lhja;->D:J

    const-wide/32 v0, 0x222e0

    iput-wide v0, p0, Lhja;->h:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhja;->i:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->j:Lidr;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhja;->k:J

    const/4 v0, 0x0

    iput v0, p0, Lhja;->l:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->m:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->p:Lhtf;

    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->q:Lhuv;

    new-instance v0, Lhjc;

    invoke-direct {v0}, Lhjc;-><init>()V

    iput-object v0, p0, Lhja;->r:Lhjc;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->t:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhja;->u:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->v:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhja;->w:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->x:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->y:Lidr;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->z:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhja;->B:J

    new-instance v0, Lhix;

    iget-object v1, p2, Lhof;->c:Lids;

    invoke-direct {v0, p1, p5, v1, p6}, Lhix;-><init>(Lidu;Liha;Lids;Lhkr;)V

    iput-object v0, p0, Lhja;->A:Lhix;

    return-void
.end method

.method private g()V
    .locals 2

    const/4 v1, 0x2

    iget-boolean v0, p0, Lhja;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0, v1}, Lidu;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhja;->v:Z

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0, v1}, Lidu;->b(I)V

    :cond_0
    return-void
.end method

.method private g(J)Z
    .locals 6

    iget-boolean v0, p0, Lhja;->n:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lhja;->D:J

    :goto_0
    iget-wide v2, p0, Lhja;->k:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lhja;->k:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget-wide v0, p0, Lhja;->C:J

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private h(J)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lhjb;->a:[I

    iget-object v3, p0, Lhja;->f:Lhir;

    invoke-virtual {v3}, Lhir;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    move v2, v1

    goto :goto_0

    :pswitch_1
    iget-wide v3, p0, Lhja;->w:J

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_3

    iget-wide v3, p0, Lhja;->w:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0xea60

    cmp-long v0, v3, v5

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    iget-wide v3, p0, Lhja;->i:J

    sub-long v3, p1, v3

    iget-wide v5, p0, Lhja;->h:J

    cmp-long v3, v3, v5

    if-ltz v3, :cond_7

    move v3, v1

    :goto_2
    iget-object v4, p0, Lhja;->b:Lidu;

    invoke-interface {v4}, Lidu;->k()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    if-eqz v3, :cond_8

    :cond_1
    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lhja;->a:Ljava/lang/String;

    const-string v5, "gpsTimeout: %s, burstTimeout: %s."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhja;->r:Lhjc;

    invoke-virtual {v0}, Lhjc;->a()Lidr;

    move-result-object v0

    if-nez v0, :cond_5

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhja;->a:Ljava/lang/String;

    const-string v3, "Invalid state in GPS time out. Returning false."

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-interface {v0}, Lidr;->f()J

    move-result-wide v3

    sub-long v3, p1, v3

    const-wide/16 v5, 0x55f0

    cmp-long v0, v3, v5

    if-ltz v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_2

    :cond_8
    :pswitch_2
    iget-boolean v0, p0, Lhja;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhja;->m:Z

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private i(J)V
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lhja;->f:Lhir;

    sget-object v1, Lhir;->c:Lhir;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhja;->f:Lhir;

    sget-object v1, Lhir;->f:Lhir;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lhja;->y:Lidr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhja;->j:Lidr;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhja;->y:Lidr;

    iget-object v1, p0, Lhja;->j:Lidr;

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lhja;->a(Lidr;Lidr;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lhja;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhja;->l:I

    :cond_1
    :goto_0
    iput-wide p1, p0, Lhja;->k:J

    :cond_2
    iput-object v4, p0, Lhja;->y:Lidr;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhja;->i:J

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhja;->f:Lhir;

    iput-object v4, p0, Lhja;->s:Livi;

    iput-boolean v3, p0, Lhja;->z:Z

    iput-wide v5, p0, Lhja;->B:J

    iput-wide v5, p0, Lhja;->w:J

    iput-boolean v3, p0, Lhja;->x:Z

    iget-object v0, p0, Lhja;->b:Lidu;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lidu;->a(I)V

    return-void

    :cond_3
    iget-object v0, p0, Lhja;->y:Lidr;

    iput-object v0, p0, Lhja;->j:Lidr;

    const/4 v0, 0x1

    iput v0, p0, Lhja;->l:I

    goto :goto_0
.end method


# virtual methods
.method final a(I)V
    .locals 2

    iget-object v0, p0, Lhja;->A:Lhix;

    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    invoke-virtual {v0}, Lhix;->a()V

    :cond_0
    return-void
.end method

.method final a(IIZ)V
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2}, Lilv;->a(II)F

    move-result v3

    cmpl-float v0, v3, v8

    if-ltz v0, :cond_1

    if-nez p3, :cond_0

    float-to-double v4, v3

    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lhja;->m:Z

    if-eqz p3, :cond_6

    float-to-double v3, v3

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    cmpl-double v0, v3, v5

    if-ltz v0, :cond_6

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lhja;->n:Z

    :cond_1
    iput-boolean p3, p0, Lhja;->o:Z

    iget-object v0, p0, Lhja;->A:Lhix;

    invoke-static {p1, p2}, Lilv;->a(II)F

    move-result v3

    cmpl-float v4, v3, v8

    if-ltz v4, :cond_4

    if-eqz p3, :cond_2

    float-to-double v4, v3

    const-wide v6, 0x3fc99999a0000000L    # 0.20000000298023224

    cmpl-double v4, v4, v6

    if-gez v4, :cond_3

    :cond_2
    if-nez p3, :cond_7

    float-to-double v3, v3

    const-wide v5, 0x3fe3333340000000L    # 0.6000000238418579

    cmpl-double v3, v3, v5

    if-ltz v3, :cond_7

    :cond_3
    :goto_2
    iput-boolean v2, v0, Lhix;->e:Z

    :cond_4
    invoke-virtual {v0}, Lhix;->a()V

    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v2, v1

    goto :goto_2
.end method

.method final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    invoke-super {p0, p1}, Lhin;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-object v0, p0, Lhja;->A:Lhix;

    iget-object v1, v0, Lhix;->b:Lhiz;

    sget-object v2, Lhiz;->b:Lhiz;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lhix;->b:Lhiz;

    sget-object v2, Lhiz;->c:Lhiz;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lhix;->b:Lhiz;

    sget-object v2, Lhiz;->d:Lhiz;

    if-ne v1, v2, :cond_3

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    iput v1, v0, Lhix;->i:I

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "BurstCollectionTrigger"

    const-string v2, "IN_CAR detected!"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Lhix;->h:Z

    :cond_2
    invoke-virtual {v0}, Lhix;->a()V

    :cond_3
    return-void
.end method

.method final a(Lhtf;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhtf;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lhja;->p:Lhtf;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->p:Lhtf;

    goto :goto_0
.end method

.method final a(Lidr;)V
    .locals 14

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhja;->c:Lhof;

    iget-object v0, v0, Lhof;->c:Lids;

    invoke-virtual {v0}, Lids;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-boolean v0, Lhja;->g:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_1
    if-eqz v0, :cond_8

    move v0, v2

    :goto_2
    iget-object v1, p0, Lhja;->f:Lhir;

    sget-object v4, Lhir;->b:Lhir;

    if-ne v1, v4, :cond_2

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lhja;->f:Lhir;

    sget-object v1, Lhir;->c:Lhir;

    if-ne v0, v1, :cond_0

    :cond_3
    iget-object v0, p0, Lhja;->j:Lidr;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhja;->j:Lidr;

    const/16 v1, 0x19

    invoke-static {v0, p1, v1}, Lhja;->a(Lidr;Lidr;I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lhja;->l:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    :cond_4
    iget-object v4, p0, Lhja;->r:Lhjc;

    if-eqz p1, :cond_6

    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, v2, :cond_9

    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "BurstCollector"

    const-string v1, "Added location@ %d to buffer."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lidr;->f()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_3
    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, v4, Lhjc;->e:I

    if-le v0, v1, :cond_6

    invoke-virtual {v4, v3}, Lhjc;->a(I)V

    :cond_6
    iput-boolean v2, p0, Lhja;->z:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhja;->w:J

    goto :goto_0

    :cond_7
    move v0, v3

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhja;->g(J)Z

    move-result v0

    goto :goto_2

    :cond_9
    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v5

    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    add-int/lit8 v1, v5, -0x1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    iget-object v1, v4, Lhjc;->a:Ljava/util/LinkedList;

    add-int/lit8 v6, v5, -0x2

    invoke-virtual {v1, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lidr;

    invoke-interface {v0}, Lidr;->f()J

    move-result-wide v6

    invoke-interface {v1}, Lidr;->f()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-interface {p1}, Lidr;->f()J

    move-result-wide v8

    invoke-interface {v0}, Lidr;->f()J

    move-result-wide v10

    sub-long/2addr v8, v10

    add-long v10, v6, v8

    iget v1, v4, Lhjc;->f:I

    add-int/lit16 v1, v1, 0x1f4

    int-to-long v12, v1

    cmp-long v1, v10, v12

    if-lez v1, :cond_a

    iget v1, v4, Lhjc;->g:I

    int-to-long v10, v1

    cmp-long v1, v6, v10

    if-ltz v1, :cond_b

    iget v1, v4, Lhjc;->g:I

    int-to-long v6, v1

    cmp-long v1, v8, v6

    if-ltz v1, :cond_b

    :cond_a
    iget-object v1, v4, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, v4, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    add-int/lit8 v0, v5, -0x1

    invoke-virtual {v4, v0}, Lhjc;->a(I)V

    :cond_b
    iget-object v0, v4, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "BurstCollector"

    const-string v1, "Added location@ %d to buffer."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lidr;->f()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method final a(Lids;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v5, 0x3e8

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v3, p0, Lhja;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Idle time collection: "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lhja;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhja;->c:Lhof;

    iget-object v0, v0, Lhof;->c:Lids;

    invoke-virtual {v0}, Lids;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhja;->A:Lhix;

    invoke-virtual {v0}, Lhix;->b()V

    invoke-virtual {p1}, Lids;->m()I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v5

    iput-wide v3, p0, Lhja;->C:J

    invoke-virtual {p1}, Lids;->n()I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v5

    iput-wide v3, p0, Lhja;->D:J

    invoke-virtual {p1}, Lids;->o()I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v5

    iput-wide v3, p0, Lhja;->h:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhja;->a:Ljava/lang/String;

    const-string v3, "Burst collector params: minIdleTimeOnBattery=%d, minIdleTimeOnExtPower=%d, maxBurstTime=%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v5, p0, Lhja;->C:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    iget-wide v5, p0, Lhja;->D:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x2

    iget-wide v5, p0, Lhja;->h:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method final a(Lidw;Ljava/lang/Object;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lhin;->a(Lidw;Ljava/lang/Object;)V

    iget-object v0, p0, Lhja;->f:Lhir;

    sget-object v1, Lhir;->b:Lhir;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhja;->B:J

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhja;->a:Ljava/lang/String;

    const-string v1, "Ignoring active burst collection because passive burst collector is running."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhja;->A:Lhix;

    invoke-virtual {v0}, Lhix;->c()V

    goto :goto_0
.end method

.method final a(Livi;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhja;->s:Livi;

    return-void
.end method

.method final a(Z)V
    .locals 0

    return-void
.end method

.method final b(Lhuv;)V
    .locals 0

    iput-object p1, p0, Lhja;->q:Lhuv;

    return-void
.end method

.method final b(Z)V
    .locals 1

    iget-object v0, p0, Lhja;->A:Lhix;

    iput-boolean p1, v0, Lhix;->d:Z

    invoke-virtual {v0}, Lhix;->a()V

    return-void
.end method

.method protected final b(J)Z
    .locals 12

    const-wide/32 v10, 0xea60

    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lhja;->r:Lhjc;

    invoke-virtual {v2}, Lhjc;->a()Lidr;

    move-result-object v6

    invoke-direct {p0, p1, p2}, Lhja;->h(J)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lhja;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lhja;->B:J

    sub-long v2, p1, v2

    cmp-long v2, v2, v10

    if-gtz v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_7

    iput-wide v4, p0, Lhja;->B:J

    iget-object v2, p0, Lhja;->A:Lhix;

    iget-object v3, v2, Lhix;->b:Lhiz;

    sget-object v6, Lhiz;->d:Lhiz;

    if-ne v3, v6, :cond_4

    iget-object v3, v2, Lhix;->a:Lidu;

    invoke-interface {v3}, Lidu;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    iput-boolean v1, v2, Lhix;->g:Z

    invoke-virtual {v2}, Lhix;->a()V

    move v2, v1

    :goto_1
    if-eqz v2, :cond_5

    iput-wide p1, p0, Lhja;->w:J

    iput-boolean v1, p0, Lhja;->x:Z

    move v0, v1

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    sget-object v1, Lhir;->c:Lhir;

    iput-object v1, p0, Lhja;->f:Lhir;

    iput-wide p1, p0, Lhja;->i:J

    :cond_1
    iget-wide v1, p0, Lhja;->B:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    iget-wide v1, p0, Lhja;->B:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v10

    if-lez v1, :cond_2

    if-nez v0, :cond_2

    iput-wide v4, p0, Lhja;->B:J

    iget-object v1, p0, Lhja;->A:Lhix;

    invoke-virtual {v1}, Lhix;->c()V

    :cond_2
    return v0

    :cond_3
    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhja;->a:Ljava/lang/String;

    const-string v2, "Unable to turn on GPS due to bad device conditions."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lhja;->A:Lhix;

    invoke-virtual {v1}, Lhix;->c()V

    goto :goto_2

    :cond_7
    invoke-direct {p0, p1, p2}, Lhja;->g(J)Z

    move-result v2

    if-eqz v2, :cond_a

    if-eqz v6, :cond_a

    invoke-interface {v6}, Lidr;->f()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/16 v7, 0x7530

    cmp-long v2, v2, v7

    if-gez v2, :cond_a

    invoke-interface {v6}, Lidr;->f()J

    move-result-wide v7

    iget-object v2, p0, Lhja;->e:Lhiq;

    iget-object v3, v2, Lhiq;->a:Lidr;

    if-nez v3, :cond_9

    move-wide v2, v4

    :goto_3
    cmp-long v2, v7, v2

    if-lez v2, :cond_a

    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    iput-wide v4, p0, Lhja;->w:J

    iput-boolean v0, p0, Lhja;->x:Z

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lhja;->a:Ljava/lang/String;

    const-string v3, "Started a new burst at %d with location at %d."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v0

    invoke-interface {v6}, Lidr;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move v0, v1

    goto/16 :goto_2

    :cond_9
    iget-object v2, v2, Lhiq;->a:Lidr;

    invoke-interface {v2}, Lidr;->f()J

    move-result-wide v2

    goto :goto_3

    :cond_a
    move v2, v0

    goto :goto_4
.end method

.method public final c(Z)V
    .locals 1

    iput-boolean p1, p0, Lhja;->t:Z

    iget-object v0, p0, Lhja;->A:Lhix;

    iput-boolean p1, v0, Lhix;->c:Z

    invoke-virtual {v0}, Lhix;->a()V

    return-void
.end method

.method protected final c(J)Z
    .locals 13

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v0, p0, Lhja;->r:Lhjc;

    invoke-virtual {v0}, Lhjc;->a()Lidr;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lhja;->h(J)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lhja;->A:Lhix;

    invoke-virtual {v0}, Lhix;->c()V

    iget-object v0, p0, Lhja;->r:Lhjc;

    iget-boolean v1, p0, Lhja;->x:Z

    iget-object v2, p0, Lhja;->b:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lhja;->i:J

    iget-boolean v6, p0, Lhja;->o:Z

    iget-object v7, p0, Lhja;->j:Lidr;

    invoke-virtual/range {v0 .. v7}, Lhjc;->a(ZJJZLidr;)Livi;

    move-result-object v0

    iput-object v0, p0, Lhja;->s:Livi;

    iget-object v0, p0, Lhja;->s:Livi;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhja;->d:Lhkl;

    iget-object v1, p0, Lhja;->b:Lidu;

    iget-object v2, p0, Lhja;->s:Livi;

    invoke-virtual {v0, v1, v2}, Lhkl;->a(Lidu;Livi;)V

    iget-object v0, p0, Lhja;->b:Lidu;

    iget-object v1, p0, Lhja;->s:Livi;

    invoke-interface {v0, v1}, Lidu;->b(Livi;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhja;->a:Ljava/lang/String;

    const-string v1, "Sent %d locations."

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lhja;->r:Lhjc;

    iget-object v3, v3, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lhir;->f:Lhir;

    iput-object v0, p0, Lhja;->f:Lhir;

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0, v12, v11}, Lidu;->a(ILilx;)V

    iput-boolean v8, p0, Lhja;->v:Z

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhja;->u:J

    iget-object v0, p0, Lhja;->r:Lhjc;

    invoke-virtual {v0}, Lhjc;->a()Lidr;

    move-result-object v0

    iput-object v0, p0, Lhja;->y:Lidr;

    move v0, v8

    :goto_0
    iget-object v1, p0, Lhja;->r:Lhjc;

    iget-object v2, v1, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    iget-object v2, v1, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    iget-object v2, v1, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lhjc;->d:J

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2}, Lhja;->i(J)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhja;->a:Ljava/lang/String;

    const-string v1, "Burst terminated early."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lhja;->f:Lhir;

    sget-object v1, Lhir;->c:Lhir;

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lhja;->w:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    iget-object v0, p0, Lhja;->b:Lidu;

    invoke-interface {v10}, Lidr;->f()J

    move-result-wide v1

    const-wide/16 v3, 0x55f0

    add-long/2addr v1, v3

    invoke-interface {v0, v12, v1, v2, v11}, Lidu;->a(IJLilx;)V

    :cond_2
    :goto_2
    iput-boolean v9, p0, Lhja;->z:Z

    return v8

    :cond_3
    iget-boolean v0, p0, Lhja;->z:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhja;->r:Lhjc;

    iget-object v1, p0, Lhja;->r:Lhjc;

    invoke-virtual {v1}, Lhjc;->a()Lidr;

    move-result-object v3

    iget-object v4, p0, Lhja;->e:Lhiq;

    iget-object v5, p0, Lhja;->p:Lhtf;

    iget-object v6, p0, Lhja;->q:Lhuv;

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lhjc;->a(JLidr;Lhiq;Lhtf;Lhuv;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v11, p0, Lhja;->q:Lhuv;

    :cond_4
    move v8, v9

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhja;->b:Lidu;

    iget-wide v1, p0, Lhja;->w:J

    const-wide/32 v3, 0xea60

    add-long/2addr v1, v3

    invoke-interface {v0, v12, v1, v2, v11}, Lidu;->a(IJLilx;)V

    goto :goto_2

    :cond_6
    move v0, v9

    goto :goto_0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lhin;->f()V

    return-void
.end method

.method protected final f(J)Z
    .locals 7

    const-wide/16 v5, 0x3a98

    const/4 v0, 0x0

    iget-object v1, p0, Lhja;->s:Livi;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhja;->a:Ljava/lang/String;

    const-string v2, "Finished a burst."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lhja;->i(J)V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhja;->u:J

    invoke-direct {p0}, Lhja;->g()V

    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lhja;->u:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v5

    if-ltz v1, :cond_2

    invoke-direct {p0}, Lhja;->g()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhja;->b:Lidu;

    const/4 v2, 0x2

    iget-wide v3, p0, Lhja;->u:J

    add-long/2addr v3, v5

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lidu;->a(IJLilx;)V

    goto :goto_0
.end method
