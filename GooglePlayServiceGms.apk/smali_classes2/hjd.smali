.class public final Lhjd;
.super Lhin;
.source "SourceFile"


# instance fields
.field final g:Lhjf;

.field h:J

.field i:J

.field j:J

.field k:Lhqy;

.field l:Livi;

.field m:I

.field n:I

.field private final o:Lhkd;


# direct methods
.method public constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Livi;Lhkd;Lhjf;)V
    .locals 7

    const-string v1, "CalibrationCollector"

    sget-object v6, Lhir;->a:Lhir;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjd;->h:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjd;->j:J

    iput-object p6, p0, Lhjd;->o:Lhkd;

    iput-object p5, p0, Lhjd;->l:Livi;

    iput-object p7, p0, Lhjd;->g:Lhjf;

    invoke-direct {p0, p5}, Lhjd;->b(Livi;)J

    move-result-wide v0

    iput-wide v0, p0, Lhjd;->h:J

    invoke-direct {p0}, Lhjd;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lhjd;->i:J

    return-void
.end method

.method static synthetic a(Lhjd;)J
    .locals 2

    invoke-direct {p0}, Lhjd;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lhjd;Livi;)J
    .locals 2

    invoke-direct {p0, p1}, Lhjd;->b(Livi;)J

    move-result-wide v0

    return-wide v0
.end method

.method static a(Lidu;)Livi;
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lidu;->s()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    const-string v3, "calibration"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "CalibrationCollector"

    const-string v2, "No calibration file present"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    sget-object v1, Lihj;->v:Livk;

    invoke-static {v3, v1}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v1

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-boolean v2, Licj;->e:Z

    if-eqz v2, :cond_0

    const-string v2, "CalibrationCollector"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)[[D
    .locals 33

    if-eqz p0, :cond_0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    :cond_0
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "CalibrationCollector"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not enough gyro samples: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_2
    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v15

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide/from16 v21, v2

    move-wide/from16 v23, v4

    move-wide/from16 v3, v21

    move-wide/from16 v25, v6

    move-wide/from16 v5, v23

    move-wide/from16 v27, v8

    move-wide/from16 v7, v25

    move-wide/from16 v29, v10

    move-wide/from16 v9, v27

    move-wide/from16 v31, v12

    move-wide/from16 v13, v31

    move-wide/from16 v11, v29

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Livi;

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v13, v13, v17

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v11, v11, v17

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v9, v9, v17

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v7, v7, v17

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v5, v5, v17

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Livi;->e(I)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v2, v3, v17

    move-wide v3, v2

    goto :goto_1

    :cond_3
    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v13, v13, v16

    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v9, v9, v16

    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v5, v5, v16

    const/4 v2, 0x2

    new-array v2, v2, [[D

    const/16 v16, 0x0

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-wide v13, v17, v18

    const/16 v18, 0x1

    aput-wide v9, v17, v18

    const/16 v18, 0x2

    aput-wide v5, v17, v18

    aput-object v17, v2, v16

    const/16 v16, 0x1

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    int-to-double v0, v15

    move-wide/from16 v19, v0

    div-double v11, v11, v19

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    move-wide/from16 v0, v19

    invoke-static {v13, v14, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    sub-double/2addr v11, v13

    aput-wide v11, v17, v18

    const/4 v11, 0x1

    int-to-double v12, v15

    div-double/2addr v7, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v9, v10, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    sub-double/2addr v7, v9

    aput-wide v7, v17, v11

    const/4 v7, 0x2

    int-to-double v8, v15

    div-double/2addr v3, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    sub-double/2addr v3, v5

    aput-wide v3, v17, v7

    aput-object v17, v2, v16

    goto/16 :goto_0
.end method

.method private b(Livi;)J
    .locals 7

    const/4 v1, 0x1

    const-wide/16 v2, -0x1

    if-eqz p1, :cond_3

    invoke-virtual {p1, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v1}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v0

    iget-object v4, p0, Lhjd;->b:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->b()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    move-wide v0, v2

    :cond_0
    :goto_0
    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lhjd;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Last successful calibration occurred at: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    const-string v2, ""

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-wide v0

    :cond_2
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method

.method static synthetic b(Lhjd;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lhjd;->g(J)V

    return-void
.end method

.method private g()J
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget-wide v0, p0, Lhjd;->h:J

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lhjd;->h:J

    iget-object v3, p0, Lhjd;->b:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->b()J

    move-result-wide v3

    cmp-long v0, v0, v3

    if-lez v0, :cond_4

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x7

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lhjd;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_1
    const/16 v1, 0xb

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    :cond_2
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lhjd;->b:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhjd;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Next calibration time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, Lhjd;->b:Lidu;

    invoke-interface {v5}, Lidu;->j()Licm;

    move-result-object v5

    invoke-interface {v5}, Licm;->c()J

    move-result-wide v5

    add-long/2addr v5, v0

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-wide v0

    :cond_4
    iget-wide v0, p0, Lhjd;->h:J

    goto/16 :goto_0
.end method

.method private g(J)V
    .locals 5

    iget-wide v0, p0, Lhjd;->j:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    iput-wide p1, p0, Lhjd;->j:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjd;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lhjd;->b:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->c()J

    move-result-wide v3

    add-long/2addr v3, p1

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhjd;->b:Lidu;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, p2, v2}, Lidu;->a(IJLilx;)V

    :cond_1
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 2

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjd;->j:J

    :cond_0
    return-void
.end method

.method final a(IIZ)V
    .locals 0

    return-void
.end method

.method final a(Lhtf;)V
    .locals 0

    return-void
.end method

.method final a(Lidr;)V
    .locals 0

    return-void
.end method

.method final a(Lids;)V
    .locals 0

    return-void
.end method

.method final a(Livi;)V
    .locals 0

    return-void
.end method

.method final a(Z)V
    .locals 0

    return-void
.end method

.method protected final a(J)Z
    .locals 1

    iget-object v0, p0, Lhjd;->o:Lhkd;

    invoke-virtual {v0}, Lhkd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhjd;->f:Lhir;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Lhuv;)V
    .locals 0

    return-void
.end method

.method final b(Z)V
    .locals 0

    return-void
.end method

.method protected final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final b(J)Z
    .locals 9

    iget-object v0, p0, Lhjd;->o:Lhkd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhkd;->a(Z)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-wide v2, p0, Lhjd;->i:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const-wide/32 v4, 0x36ee80

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    sget-object v0, Lhir;->g:Lhir;

    iput-object v0, p0, Lhjd;->f:Lhir;

    iget-object v0, p0, Lhjd;->g:Lhjf;

    invoke-virtual {v0}, Lhjf;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhjd;->g:Lhjf;

    invoke-virtual {v0}, Lhjf;->d()I

    move-result v0

    :goto_2
    iget-object v1, p0, Lhjd;->g:Lhjf;

    invoke-virtual {v1}, Lhjf;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhjd;->g:Lhjf;

    invoke-virtual {v1}, Lhjf;->e()I

    move-result v1

    :goto_3
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhjd;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Starting calibration with delays: gyro: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " accel:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput v0, p0, Lhjd;->m:I

    iput v1, p0, Lhjd;->n:I

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v3, Lhrz;->e:Lhrz;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhrz;->d:Lhrz;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhjd;->b:Lidu;

    const/4 v1, 0x2

    new-array v1, v1, [Lhrz;

    const/4 v3, 0x0

    sget-object v4, Lhrz;->e:Lhrz;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lhrz;->d:Lhrz;

    aput-object v4, v1, v3

    invoke-static {v1}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v1

    const-wide/16 v3, 0x3a98

    const/4 v5, 0x0

    new-instance v6, Lhje;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lhje;-><init>(Lhjd;B)V

    iget-object v7, p0, Lhjd;->a:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-interface/range {v0 .. v8}, Lidu;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhqq;Ljava/lang/String;Lilx;)Lhqy;

    move-result-object v0

    iput-object v0, p0, Lhjd;->k:Lhqy;

    iget-object v0, p0, Lhjd;->k:Lhqy;

    invoke-interface {v0}, Lhqy;->a()V

    const/4 v0, 0x1

    :goto_4
    return v0

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    iget-wide v0, p0, Lhjd;->i:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_7

    invoke-direct {p0}, Lhjd;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lhjd;->i:J

    :cond_6
    iget-wide v0, p0, Lhjd;->i:J

    invoke-direct {p0, v0, v1}, Lhjd;->g(J)V

    :goto_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    iget-wide v0, p0, Lhjd;->i:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_6

    iget-wide v0, p0, Lhjd;->i:J

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lhjd;->g(J)V

    goto :goto_5
.end method

.method final c(Z)V
    .locals 0

    return-void
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lhin;->f()V

    return-void
.end method
