.class public final Lhof;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lhtw;

.field public static final f:Lhuf;

.field public static final g:Lhuf;


# instance fields
.field public b:Lhtw;

.field public final c:Lids;

.field public final d:Lhou;

.field public final e:Lhou;

.field private h:J

.field private i:Z

.field private final j:Lhod;

.field private k:Lhpk;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lhtw;

    const/4 v1, 0x0

    sget-object v2, Lhty;->b:Lhty;

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lhtw;-><init>(Lhug;Lhty;J)V

    sput-object v0, Lhof;->a:Lhtw;

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    sput-object v0, Lhof;->f:Lhuf;

    new-instance v0, Lhoh;

    invoke-direct {v0}, Lhoh;-><init>()V

    sput-object v0, Lhof;->g:Lhuf;

    return-void
.end method

.method public constructor <init>(Lids;JLhod;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lhof;->a:Lhtw;

    iput-object v0, p0, Lhof;->b:Lhtw;

    iput-object p1, p0, Lhof;->c:Lids;

    iput-object p4, p0, Lhof;->j:Lhod;

    new-instance v0, Lhou;

    const/16 v1, 0x32

    sget-object v2, Lhuk;->a:Lhuf;

    sget-object v3, Lhof;->f:Lhuf;

    invoke-direct {v0, v1, v2, v3}, Lhou;-><init>(ILhuf;Lhuf;)V

    iput-object v0, p0, Lhof;->d:Lhou;

    new-instance v0, Lhou;

    const/16 v1, 0x3e8

    sget-object v2, Lhuk;->b:Lhuf;

    sget-object v3, Lhof;->g:Lhuf;

    invoke-direct {v0, v1, v2, v3}, Lhou;-><init>(ILhuf;Lhuf;)V

    iput-object v0, p0, Lhof;->e:Lhou;

    invoke-direct {p0, p2, p3}, Lhof;->b(J)V

    return-void
.end method

.method private static a(JJJJ)J
    .locals 2

    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(J)V
    .locals 6

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    sget-object v0, Lhof;->a:Lhtw;

    iput-object v0, p0, Lhof;->b:Lhtw;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x4038000000000000L    # 24.0

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    mul-double/2addr v0, v4

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    sub-long v0, p1, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lhof;->a(JZ)V

    iget-object v0, p0, Lhof;->d:Lhou;

    iget-object v0, v0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->clear()V

    iget-object v0, p0, Lhof;->e:Lhou;

    iget-object v0, v0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->clear()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lhof;->h:J

    return-wide v0
.end method

.method public final a(J)V
    .locals 5

    const-wide/32 v0, 0x240c8400

    sub-long v0, p1, v0

    iget-object v2, p0, Lhof;->d:Lhou;

    iget-object v3, p0, Lhof;->c:Lids;

    invoke-virtual {v3}, Lids;->v()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lhou;->a(JJ)V

    iget-object v2, p0, Lhof;->e:Lhou;

    iget-object v3, p0, Lhof;->c:Lids;

    invoke-virtual {v3}, Lids;->u()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lhou;->a(JJ)V

    iget-object v0, p0, Lhof;->j:Lhod;

    invoke-interface {v0, p1, p2}, Lhod;->c(J)V

    return-void
.end method

.method public final a(JZ)V
    .locals 0

    iput-wide p1, p0, Lhof;->h:J

    iput-boolean p3, p0, Lhof;->i:Z

    return-void
.end method

.method public final a(Lidu;)V
    .locals 14

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lidu;->a()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->c()J

    move-result-wide v4

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v6

    iget-object v1, p0, Lhof;->j:Lhod;

    invoke-interface {v1, v6, v7}, Lhod;->b(J)V

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-direct {v9, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-interface {p1}, Lidu;->g()Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-interface {p1}, Lidu;->i()[B
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :try_start_1
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v9}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    sget-boolean v8, Licj;->b:Z

    if-eqz v8, :cond_0

    const-string v8, "PersistentState"

    const-string v10, "Persistent state version: %d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/16 v8, 0xb

    if-eq v3, v8, :cond_1

    const/16 v8, 0xa

    if-ne v3, v8, :cond_a

    :cond_1
    const/16 v8, 0xa

    if-ne v3, v8, :cond_6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "PersistentState"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "Reading compatible, non signed version: "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Ljava/io/DataInputStream;

    invoke-static {v2, v1}, Licl;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v8, v0

    :goto_0
    invoke-interface {v8}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    invoke-interface {v8}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    invoke-static/range {v0 .. v7}, Lhof;->a(JJJJ)J

    move-result-wide v2

    invoke-interface {v8}, Ljava/io/DataInput;->readBoolean()Z

    move-result v10

    invoke-virtual {p0, v2, v3, v10}, Lhof;->a(JZ)V

    invoke-interface {v8}, Ljava/io/DataInput;->readBoolean()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lhtw;->f:Lhuf;

    invoke-interface {v2, v8}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhtw;

    iput-object v2, p0, Lhof;->b:Lhtw;

    iget-object v2, p0, Lhof;->b:Lhtw;

    iget-wide v2, v2, Lhtw;->e:J

    invoke-static/range {v0 .. v7}, Lhof;->a(JJJJ)J

    move-result-wide v0

    new-instance v2, Lhtw;

    iget-object v3, p0, Lhof;->b:Lhtw;

    iget-object v3, v3, Lhtw;->c:Lhug;

    iget-object v10, p0, Lhof;->b:Lhtw;

    iget-object v10, v10, Lhtw;->d:Lhty;

    invoke-direct {v2, v3, v10, v0, v1}, Lhtw;-><init>(Lhug;Lhty;J)V

    iput-object v2, p0, Lhof;->b:Lhtw;

    :cond_3
    iget-object v0, p0, Lhof;->d:Lhou;

    invoke-virtual {v0, v8}, Lhou;->b(Ljava/io/DataInput;)Lhou;

    iget-object v0, p0, Lhof;->e:Lhou;

    invoke-virtual {v0, v8}, Lhou;->b(Ljava/io/DataInput;)Lhou;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "PersistentState"

    const-string v1, "Loaded %d cell, %d wifi. Last refresh time: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v8, p0, Lhof;->d:Lhou;

    iget-object v8, v8, Lhou;->a:Lhov;

    invoke-virtual {v8}, Lhov;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v3

    const/4 v3, 0x1

    iget-object v8, p0, Lhof;->e:Lhou;

    iget-object v8, v8, Lhou;->a:Lhov;

    invoke-virtual {v8}, Lhov;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v3

    const/4 v3, 0x2

    new-instance v8, Ljava/util/Date;

    iget-wide v10, p0, Lhof;->h:J

    add-long/2addr v10, v4

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    aput-object v8, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_5
    :goto_2
    return-void

    :cond_6
    const/16 v1, 0xb

    if-ne v3, v1, :cond_8

    :try_start_3
    iget-object v1, p0, Lhof;->k:Lhpk;

    if-nez v1, :cond_7

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v0

    iput-object v0, p0, Lhof;->k:Lhpk;

    :cond_7
    iget-object v0, p0, Lhof;->k:Lhpk;

    invoke-virtual {v0, v2}, Lhpk;->a(Ljava/io/DataInputStream;)Lhue;

    move-result-object v0

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v8, v1

    goto/16 :goto_0

    :cond_8
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incompatible version."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_0
    move-exception v0

    :try_start_4
    invoke-direct {p0, v6, v7}, Lhof;->b(J)V

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_1
    move-exception v0

    invoke-direct {p0, v6, v7}, Lhof;->b(J)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_9

    const-string v0, "PersistentState"

    const-string v1, "Clearing persistent state, last refresh: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v6, Ljava/util/Date;

    iget-wide v7, p0, Lhof;->h:J

    add-long/2addr v4, v7

    invoke-direct {v6, v4, v5}, Ljava/util/Date;-><init>(J)V

    aput-object v6, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_5

    const-string v0, "PersistentState"

    const-string v1, "No existing nlp persistent state."

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    :try_start_5
    invoke-direct {p0, v6, v7}, Lhof;->b(J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-direct {p0, v6, v7}, Lhof;->b(J)V

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_5

    const-string v1, "PersistentState"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-direct {p0, v6, v7}, Lhof;->b(J)V

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_5

    const-string v1, "PersistentState"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method

.method public final a(Livi;ZJ)V
    .locals 19

    invoke-static/range {p1 .. p1}, Lilv;->b(Livi;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lhof;->c:Lids;

    invoke-virtual {v1}, Lids;->d()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lhof;->c:Lids;

    invoke-virtual {v1}, Lids;->e()I

    move-result v15

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v16

    const/4 v1, 0x0

    move v14, v1

    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, Livi;->c(II)Livi;

    move-result-object v17

    const/4 v1, 0x0

    move v13, v1

    :goto_1
    const/4 v1, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v1

    if-ge v13, v1, :cond_b

    const/4 v1, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v13}, Livi;->c(II)Livi;

    move-result-object v18

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, -0x1

    const/4 v6, -0x1

    const v10, 0x9c40

    const/4 v7, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Livi;->i(I)Z

    move-result v7

    if-eqz v7, :cond_c

    const/4 v7, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Livi;->f(I)Livi;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Livi;->f(I)Livi;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v5

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Livi;->f(I)Livi;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v3

    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Livi;->c(I)I

    move-result v1

    mul-int/lit16 v2, v1, 0x3e8

    const/4 v1, 0x4

    invoke-virtual {v7, v1}, Livi;->c(I)I

    move-result v1

    :cond_2
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_3

    const/16 v6, 0x8

    invoke-virtual {v7, v6}, Livi;->c(I)I

    move-result v6

    :cond_3
    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_c

    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Livi;->c(I)I

    move-result v7

    mul-int/lit16 v10, v7, 0x3e8

    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    :goto_2
    const/4 v1, 0x2

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    if-eq v6, v1, :cond_4

    const/4 v1, 0x2

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-static {v1}, Lhtf;->b(Livi;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lhug;

    invoke-direct {v5, v12, v11, v8, v9}, Lhug;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lhof;->d:Lhou;

    move/from16 v2, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v1 .. v7}, Lhou;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_4
    const/4 v1, 0x3

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x3

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Livi;->f(I)Livi;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Livi;->d(I)J

    move-result-wide v1

    move-wide v2, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lhof;->e:Lhou;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Lhou;->a(Ljava/lang/Object;)Lhno;

    move-result-object v6

    if-nez v6, :cond_9

    if-eqz p2, :cond_9

    const/4 v1, 0x1

    move v5, v1

    :goto_4
    if-eqz v6, :cond_a

    if-eqz p2, :cond_5

    iget-object v1, v6, Lhno;->d:Ljava/lang/Object;

    check-cast v1, Lhuq;

    invoke-virtual {v1}, Lhuq;->f()Z

    move-result v1

    if-nez v1, :cond_a

    :cond_5
    const/4 v1, 0x1

    :goto_5
    if-nez v5, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    new-instance v5, Lhuq;

    move v6, v12

    move v7, v11

    invoke-direct/range {v5 .. v10}, Lhuq;-><init>(IIIII)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lhof;->e:Lhou;

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move v9, v15

    move-object v10, v5

    move-wide/from16 v11, p3

    invoke-virtual/range {v6 .. v12}, Lhou;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_7
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lhuw;->a(Ljava/lang/String;)J

    move-result-wide v1

    move-wide v2, v1

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    move v5, v1

    goto :goto_4

    :cond_a
    const/4 v1, 0x0

    goto :goto_5

    :cond_b
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto/16 :goto_0

    :cond_c
    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    goto/16 :goto_2
.end method

.method public final b(Lidu;)V
    .locals 6

    invoke-interface {p1}, Lidu;->a()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/io/File;

    const-string v2, "nlp_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lhof;->j:Lhod;

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lhod;->a(J)V

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface {p1, v1}, Lidu;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {p1}, Lidu;->i()[B

    move-result-object v0

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0xb

    invoke-virtual {v4, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v5, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-wide v2, p0, Lhof;->h:J

    invoke-virtual {v5, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-boolean v2, p0, Lhof;->i:Z

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v2, p0, Lhof;->b:Lhtw;

    sget-object v3, Lhof;->a:Lhtw;

    if-eq v2, v3, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    sget-object v2, Lhtw;->f:Lhuf;

    iget-object v3, p0, Lhof;->b:Lhtw;

    invoke-interface {v2, v3, v5}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :goto_1
    iget-object v2, p0, Lhof;->d:Lhou;

    iget-object v3, p0, Lhof;->d:Lhou;

    invoke-virtual {v2, v3, v5}, Lhou;->a(Lhou;Ljava/io/DataOutput;)V

    iget-object v2, p0, Lhof;->e:Lhou;

    iget-object v3, p0, Lhof;->e:Lhou;

    invoke-virtual {v2, v3, v5}, Lhou;->a(Lhou;Ljava/io/DataOutput;)V

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v2, p0, Lhof;->k:Lhpk;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v0

    iput-object v0, p0, Lhof;->k:Lhpk;

    :cond_2
    iget-object v0, p0, Lhof;->k:Lhpk;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lhpk;->a(Ljava/io/DataOutputStream;[B)V

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "PersistentState"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "PersistentState"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "PersistentState"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lhof;->i:Z

    return v0
.end method

.method public final c()Lhou;
    .locals 1

    iget-object v0, p0, Lhof;->e:Lhou;

    return-object v0
.end method

.method public final d()Lhou;
    .locals 1

    iget-object v0, p0, Lhof;->d:Lhou;

    return-object v0
.end method

.method public final e()Lhod;
    .locals 1

    iget-object v0, p0, Lhof;->j:Lhod;

    return-object v0
.end method
