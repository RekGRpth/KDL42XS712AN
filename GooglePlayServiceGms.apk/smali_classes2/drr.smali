.class final Ldrr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Ldrq;


# direct methods
.method private constructor <init>(Ldrq;)V
    .locals 0

    iput-object p1, p0, Ldrr;->a:Ldrq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldrq;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldrr;-><init>(Ldrq;)V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {v0}, Ldrq;->a(Ldrq;)Ldrq;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling wait on previous RoomServiceClient instance: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldrr;->a:Ldrq;

    invoke-static {v2}, Ldrq;->a(Ldrq;)Ldrq;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {v0}, Ldrq;->a(Ldrq;)Ldrq;

    move-result-object v0

    invoke-static {v0}, Ldrq;->b(Ldrq;)V

    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {v0}, Ldrq;->c(Ldrq;)Ldrq;

    :cond_0
    invoke-static {}, Ldrq;->c()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    const-string v0, "RoomServiceClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Room android service is connected. RoomServiceClient instance: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ldrr;->a:Ldrq;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {p2}, Ldaq;->a(Landroid/os/IBinder;)Ldap;

    move-result-object v2

    invoke-static {v0, v2}, Ldrq;->a(Ldrq;Ldap;)Ldap;

    const-string v0, "RoomServiceClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RoomAndroidService binder instance: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ldrr;->a:Ldrq;

    invoke-static {v3}, Ldrq;->d(Ldrq;)Ldap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {v0}, Ldrq;->d(Ldrq;)Ldap;

    move-result-object v0

    iget-object v2, p0, Ldrr;->a:Ldrq;

    invoke-interface {v0, v2}, Ldap;->a(Ldas;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    iget-object v0, p0, Ldrr;->a:Ldrq;

    invoke-static {v0}, Ldrq;->e(Ldrq;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrs;

    if-eqz v0, :cond_1

    iget-object v1, p0, Ldrr;->a:Ldrq;

    invoke-interface {v0, v1}, Ldrs;->a(Ldrq;)V

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    invoke-static {}, Ldrq;->c()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    const-string v0, "RoomServiceClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Room android service is disconnected. RoomServiceClient instance: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ldrr;->a:Ldrq;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrr;->a:Ldrq;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ldrq;->a(Ldrq;Ldap;)Ldap;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
