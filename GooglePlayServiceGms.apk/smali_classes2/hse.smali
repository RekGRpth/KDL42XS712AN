.class public final Lhse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhqy;


# instance fields
.field private final a:Limb;

.field private final b:Landroid/content/Context;

.field private c:Ljava/util/concurrent/CountDownLatch;

.field private d:Z

.field private e:Lhsf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;Limb;)V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhse;->d:Z

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lhpo;->a(Ljava/lang/String;Lhqk;)V

    invoke-static/range {p8 .. p8}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhse;->a:Limb;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lhse;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhse;->b:Landroid/content/Context;

    new-instance v0, Lhsf;

    iget-object v1, p0, Lhse;->b:Landroid/content/Context;

    iget-object v3, p0, Lhse;->c:Ljava/util/concurrent/CountDownLatch;

    iget-object v9, p0, Lhse;->a:Limb;

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p6

    move-object v7, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lhsf;-><init>(Landroid/content/Context;Lhqk;Ljava/util/concurrent/CountDownLatch;Lhsu;Liea;Livi;Ljava/lang/Integer;Lhqq;Limb;)V

    iput-object v0, p0, Lhse;->e:Lhsf;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lhse;->d:Z

    if-nez v1, :cond_2

    :goto_0
    const-string v1, "Start should be called only once!"

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhse;->d:Z

    iget-object v0, p0, Lhse;->e:Lhsf;

    invoke-virtual {v0}, Lhsf;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhse;->a:Limb;

    const-string v1, "Waiting for the ScannerThread to start."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhse;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhse;->a:Limb;

    const-string v1, "Thread interrupted during waiting for ScannerThread to start."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Livi;)Z
    .locals 1

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhse;->e:Lhsf;

    invoke-virtual {v0, p1}, Lhsf;->a(Livi;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lhse;->e:Lhsf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhse;->e:Lhsf;

    invoke-static {v0}, Lhsf;->a(Lhsf;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhse;->e:Lhsf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhse;->e:Lhsf;

    invoke-virtual {v0}, Lhsf;->a()V

    :cond_0
    return-void
.end method
