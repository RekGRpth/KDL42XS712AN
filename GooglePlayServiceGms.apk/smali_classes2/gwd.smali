.class public final Lgwd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgwi;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgwd;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lgwd;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a()I

    move-result v2

    if-eq v2, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lgwd;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lgth;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lgth;->b(I)I

    move-result v4

    invoke-static {v3}, Lgth;->c(I)I

    move-result v3

    if-ne v4, v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lgwd;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lgwd;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
