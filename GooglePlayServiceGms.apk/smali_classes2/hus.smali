.class public final enum Lhus;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhus;

.field public static final enum b:Lhus;

.field public static final enum c:Lhus;

.field public static final enum d:Lhus;

.field private static final synthetic e:[Lhus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhus;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lhus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhus;->a:Lhus;

    new-instance v0, Lhus;

    const-string v1, "LOW_CONFIDENCE"

    invoke-direct {v0, v1, v3}, Lhus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhus;->b:Lhus;

    new-instance v0, Lhus;

    const-string v1, "MEDIUM_CONFIDENCE"

    invoke-direct {v0, v1, v4}, Lhus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhus;->c:Lhus;

    new-instance v0, Lhus;

    const-string v1, "HIGH_CONFIDENCE"

    invoke-direct {v0, v1, v5}, Lhus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhus;->d:Lhus;

    const/4 v0, 0x4

    new-array v0, v0, [Lhus;

    sget-object v1, Lhus;->a:Lhus;

    aput-object v1, v0, v2

    sget-object v1, Lhus;->b:Lhus;

    aput-object v1, v0, v3

    sget-object v1, Lhus;->c:Lhus;

    aput-object v1, v0, v4

    sget-object v1, Lhus;->d:Lhus;

    aput-object v1, v0, v5

    sput-object v0, Lhus;->e:[Lhus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()Lhus;
    .locals 1

    sget-object v0, Lhus;->d:Lhus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lhus;
    .locals 1

    const-class v0, Lhus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhus;

    return-object v0
.end method

.method public static values()[Lhus;
    .locals 1

    sget-object v0, Lhus;->e:[Lhus;

    invoke-virtual {v0}, [Lhus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhus;

    return-object v0
.end method
