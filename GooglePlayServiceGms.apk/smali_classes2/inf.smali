.class public final Linf;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ling;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Linf;->a:I

    const-string v0, ""

    iput-object v0, p0, Linf;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linf;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Linf;->h:Ling;

    const/4 v0, -0x1

    iput v0, p0, Linf;->i:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Linf;->i:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Linf;->b()I

    :cond_0
    iget v0, p0, Linf;->i:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    iput-boolean v1, p0, Linf;->c:Z

    iput v0, p0, Linf;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Linf;->d:Z

    iput-object v0, p0, Linf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Linf;->e:Z

    iput-object v0, p0, Linf;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    new-instance v0, Ling;

    invoke-direct {v0}, Ling;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v1, p0, Linf;->g:Z

    iput-object v0, p0, Linf;->h:Ling;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Linf;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Linf;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Linf;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Linf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Linf;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Linf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Linf;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Linf;->h:Ling;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Linf;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Linf;->a:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Linf;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Linf;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Linf;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Linf;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Linf;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Linf;->h:Ling;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Linf;->i:I

    return v0
.end method
