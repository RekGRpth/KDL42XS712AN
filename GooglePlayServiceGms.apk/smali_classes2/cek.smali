.class final enum Lcek;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcek;

.field public static final enum b:Lcek;

.field public static final enum c:Lcek;

.field public static final enum d:Lcek;

.field private static final synthetic f:[Lcek;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcek;

    const-string v1, "INTEGER"

    const-string v2, "value_int"

    invoke-direct {v0, v1, v3, v2}, Lcek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcek;->a:Lcek;

    new-instance v0, Lcek;

    const-string v1, "REAL"

    const-string v2, "value_real"

    invoke-direct {v0, v1, v4, v2}, Lcek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcek;->b:Lcek;

    new-instance v0, Lcek;

    const-string v1, "TEXT"

    const-string v2, "value_txt"

    invoke-direct {v0, v1, v5, v2}, Lcek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcek;->c:Lcek;

    new-instance v0, Lcek;

    const-string v1, "BLOB"

    const-string v2, "value_blob"

    invoke-direct {v0, v1, v6, v2}, Lcek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcek;->d:Lcek;

    const/4 v0, 0x4

    new-array v0, v0, [Lcek;

    sget-object v1, Lcek;->a:Lcek;

    aput-object v1, v0, v3

    sget-object v1, Lcek;->b:Lcek;

    aput-object v1, v0, v4

    sget-object v1, Lcek;->c:Lcek;

    aput-object v1, v0, v5

    sget-object v1, Lcek;->d:Lcek;

    aput-object v1, v0, v6

    sput-object v0, Lcek;->f:[Lcek;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcek;->e:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcek;
    .locals 1

    const-class v0, Lcek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcek;

    return-object v0
.end method

.method public static values()[Lcek;
    .locals 1

    sget-object v0, Lcek;->f:[Lcek;

    invoke-virtual {v0}, [Lcek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcek;

    return-object v0
.end method
