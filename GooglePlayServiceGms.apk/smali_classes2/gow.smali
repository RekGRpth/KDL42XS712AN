.class public final Lgow;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

.field private b:Lgox;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)V
    .locals 1

    iput-object p1, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lgox;

    invoke-direct {v0}, Lgox;-><init>()V

    iput-object v0, p0, Lgow;->b:Lgox;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgow;-><init>(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    const/4 v1, 0x0

    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_2

    check-cast p1, Landroid/text/Spannable;

    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)Ljava/util/ArrayList;

    move-result-object v3

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {p1, p2, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v4, v0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    invoke-static {v5}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a(Landroid/text/style/URLSpan;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p1, v5}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    const/4 v1, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_2
    iget-object v0, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d017f    # com.google.android.gms.R.dimen.plus_sharebox_mention_suggestion_popup_offset

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setDropDownVerticalOffset(I)V

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    iget-object v0, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lgow;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lgow;->b:Lgox;

    invoke-virtual {v2, p1, v0}, Lgox;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
