.class public final Lhjm;
.super Lhis;
.source "SourceFile"


# instance fields
.field private final f:Lilt;

.field private final g:I

.field private final h:Lhjf;


# direct methods
.method public constructor <init>(Lidu;Lilt;Lhjf;Lhkd;)V
    .locals 6

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lhjm;-><init>(Lidu;Lilt;Lhjf;Lhkd;Ljava/util/Random;)V

    return-void
.end method

.method private constructor <init>(Lidu;Lilt;Lhjf;Lhkd;Ljava/util/Random;)V
    .locals 4

    const-string v0, "InOutScheduler"

    invoke-direct {p0, v0, p1, p4, p5}, Lhis;-><init>(Ljava/lang/String;Lidu;Lhkd;Ljava/util/Random;)V

    iput-object p2, p0, Lhjm;->f:Lilt;

    invoke-virtual {p2}, Lilt;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x6

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lhjm;->g:I

    iput-object p3, p0, Lhjm;->h:Lhjf;

    return-void
.end method

.method private a(Ljava/util/Calendar;I)Lhue;
    .locals 8

    const/4 v7, 0x0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjm;->a:Ljava/lang/String;

    const-string v1, "minStartTime %s, collection left: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v1

    rsub-int/lit8 v0, p2, 0x6

    iget v3, p0, Lhjm;->g:I

    mul-int/2addr v0, v3

    int-to-long v3, v0

    iget-object v0, p0, Lhjm;->f:Lilt;

    iget-wide v5, v0, Lilt;->a:J

    add-long/2addr v3, v5

    cmp-long v0, v3, v1

    if-gtz v0, :cond_2

    iget-object v0, p0, Lhjm;->f:Lilt;

    invoke-virtual {v0, v1, v2}, Lilt;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v3, p0, Lhjm;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    :goto_0
    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget-object v3, p0, Lhjm;->c:Ljava/util/Random;

    invoke-virtual {v3, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    int-to-long v3, v0

    add-long v0, v1, v3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {v2, v0, v1}, Lilv;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lhjm;->h:Lhjf;

    iget-object v1, v0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, v0, Lhjf;->c:Livi;

    const/4 v4, 0x7

    invoke-virtual {v0}, Lhjf;->l()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v4, v0}, Livi;->e(II)Livi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sget-object v2, Lhit;->a:Lhit;

    invoke-virtual {p0, v0, v1, v2}, Lhjm;->a(JLhit;)Lhue;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lhjm;->f:Lilt;

    iget-wide v3, v0, Lilt;->a:J

    sub-long v3, v1, v3

    long-to-int v0, v3

    iget v3, p0, Lhjm;->g:I

    div-int v3, v0, v3

    iget v4, p0, Lhjm;->g:I

    iget v5, p0, Lhjm;->g:I

    rem-int/2addr v0, v5

    sub-int v0, v4, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lhjm;->g:I

    move-wide v1, v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a(Ljava/util/Calendar;)Lhue;
    .locals 4

    const/4 v1, 0x6

    iget-object v0, p0, Lhjm;->f:Lilt;

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lhit;->a:Lhit;

    invoke-static {v0, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v0}, Lhjf;->k()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v0, p1}, Lhjf;->a(Ljava/util/Calendar;)V

    move v0, v1

    :goto_1
    if-lez v0, :cond_3

    iget-object v2, p0, Lhjm;->f:Lilt;

    invoke-virtual {v2, p1}, Lilt;->b(Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, p1, v0}, Lhjm;->a(Ljava/util/Calendar;I)Lhue;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {v2, p1}, Lilv;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v0, p1}, Lhjf;->a(Ljava/util/Calendar;)V

    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v0}, Lhjf;->l()I

    move-result v0

    const/4 v2, 0x0

    rsub-int/lit8 v0, v0, 0x6

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    iget-object v2, p0, Lhjm;->f:Lilt;

    iget-wide v2, v2, Lilt;->a:J

    invoke-static {v0, v2, v3}, Lilv;->a(Ljava/util/Calendar;J)V

    iget-object v2, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v2, v0}, Lhjf;->a(Ljava/util/Calendar;)V

    invoke-direct {p0, v0, v1}, Lhjm;->a(Ljava/util/Calendar;I)Lhue;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 3

    iget-object v0, p0, Lhjm;->h:Lhjf;

    iget-object v1, v0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lhjf;->c:Livi;

    const/16 v2, 0x8

    invoke-virtual {v0, v2, p1, p2}, Livi;->a(IJ)Livi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b()Lhue;
    .locals 2

    iget-object v0, p0, Lhjm;->h:Lhjf;

    invoke-virtual {v0}, Lhjf;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lhit;->a:Lhit;

    invoke-static {v0, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    return-object v0
.end method
