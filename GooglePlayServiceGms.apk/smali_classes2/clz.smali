.class public final Lclz;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private f:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V
    .locals 6

    sget-object v1, Lcmr;->e:Lcmr;

    sget-object v5, Lcms;->a:Lcms;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-object p4, p0, Lclz;->c:Ljava/lang/String;

    if-nez p5, :cond_0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object p5

    :cond_0
    iput-object p5, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-wide p6, p0, Lclz;->d:J

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->e:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lclz;->f:Ljava/lang/Long;

    :cond_0
    const-string v0, "contentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lclz;->c:Ljava/lang/String;

    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lclz;->d:J

    :goto_0
    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcjc;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lclz;->d:J

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lclz;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 12

    iget-object v0, p0, Lclz;->f:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v1, p0, Lclz;->c:Ljava/lang/String;

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    iget-object v3, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v4, p0, Lclz;->d:J

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcfz;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcge;

    move-result-object v0

    iget-wide v0, v0, Lcfl;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lclz;->f:Ljava/lang/Long;

    :cond_0
    invoke-virtual {p2}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcfp;->L()J

    move-result-wide v10

    iget-object v0, p0, Lclz;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcfp;->s(Ljava/lang/String;)V

    iget-object v0, p0, Lclz;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lclz;->c:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Content does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lclz;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-wide v0, v1, Lcfx;->h:J

    invoke-virtual {p2, v0, v1}, Lcfp;->a(J)V

    :cond_1
    iget-object v0, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p2, v0}, Lcjc;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v5

    invoke-virtual {p2}, Lcfp;->Y()V

    invoke-virtual {p2}, Lcfp;->k()V

    invoke-virtual {p0, p1}, Lclz;->b(Lcfz;)Lbsp;

    move-result-object v2

    new-instance v0, Lcmy;

    iget-object v1, v2, Lbsp;->a:Lcfc;

    iget-object v2, v2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-wide v6, p0, Lclz;->d:J

    iget-object v8, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct/range {v0 .. v11}, Lcmy;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JJJ)V

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 9

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v7

    new-instance v8, Lbtv;

    invoke-direct {v8, p3}, Lbtv;-><init>(Lcoy;)V

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lclz;->a(Lcfz;)Lcfp;

    move-result-object v0

    iget-object v1, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const/4 v5, 0x0

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v1

    invoke-virtual {p0, v1}, Lclz;->b(Lcfz;)Lbsp;

    move-result-object v6

    move-object v1, p3

    invoke-static/range {v0 .. v6}, Lbty;->a(Lcfp;Lcoy;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLbsp;)Lbty;

    move-result-object v0

    :try_start_0
    new-instance v1, Lbtu;

    invoke-direct {v1}, Lbtu;-><init>()V

    invoke-virtual {v8, v0, v1}, Lbtv;->a(Lbty;Lbtw;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    invoke-virtual {p0, v7}, Lclz;->a(Lcfz;)Lcfp;

    move-result-object v1

    invoke-static {v0}, Lcbk;->a(Lcom/google/android/gms/drive/internal/model/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcfp;->q(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcfp;->r(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcfp;->k()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbua; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbub; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v0, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {v7, v0, v1}, Lcfz;->b(J)Lcge;

    move-result-object v0

    invoke-virtual {v0}, Lcge;->l()V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lclz;->f:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, "pendingUploadSqlId"

    iget-object v2, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v1, "contentHash"

    iget-object v2, p0, Lclz;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "writeOpenTime"

    iget-wide v2, p0, Lclz;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v1, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v1, :cond_1

    const-string v1, "metadataDelta"

    iget-object v2, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcjc;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lclz;

    iget-object v2, p0, Lclz;->f:Ljava/lang/Long;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lclz;->f:Ljava/lang/Long;

    iget-object v3, p1, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    iget-object v2, p0, Lclz;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lclz;->c:Ljava/lang/String;

    iget-object v3, p1, Lclz;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lclz;->f:Ljava/lang/Long;

    if-nez v2, :cond_4

    :cond_6
    iget-object v2, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lclz;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    goto :goto_1

    :cond_8
    iget-object v2, p1, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_0

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lclz;->f:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lclz;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lclz;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lclz;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ContentAndMetadataOp[%s, pendingUploadSqlId=%d,  contentHash=%s, metadataChangeSet=%s]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lclz;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lclz;->f:Ljava/lang/Long;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lclz;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lclz;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
