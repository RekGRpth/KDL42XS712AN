.class public final Lcbm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcdu;

.field public final b:Lcfz;

.field private final c:Lcby;


# direct methods
.method public constructor <init>(Lcoy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcoy;->d()Lcdu;

    move-result-object v0

    iput-object v0, p0, Lcbm;->a:Lcdu;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lcbm;->b:Lcfz;

    invoke-virtual {p1}, Lcoy;->i()Lcby;

    move-result-object v0

    iput-object v0, p0, Lcbm;->c:Lcby;

    return-void
.end method


# virtual methods
.method public final a()Lcbn;
    .locals 7

    const-wide/16 v1, 0x0

    iget-object v0, p0, Lcbm;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->g()J

    move-result-wide v3

    sget-object v0, Lcds;->a:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v3, Lcbn;

    invoke-direct {v3}, Lcbn;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-gez v4, :cond_0

    move-wide v0, v1

    :goto_0
    iput-wide v0, v3, Lcbn;->a:J

    iget-object v0, p0, Lcbm;->b:Lcfz;

    invoke-interface {v0}, Lcfz;->n()J

    move-result-wide v0

    iget-object v2, p0, Lcbm;->b:Lcfz;

    invoke-interface {v2}, Lcfz;->p()J

    move-result-wide v4

    add-long/2addr v0, v4

    iput-wide v0, v3, Lcbn;->b:J

    iget-object v0, p0, Lcbm;->b:Lcfz;

    invoke-interface {v0}, Lcfz;->q()J

    move-result-wide v0

    iput-wide v0, v3, Lcbn;->c:J

    iget-wide v0, v3, Lcbn;->a:J

    iget-wide v4, v3, Lcbn;->b:J

    add-long/2addr v0, v4

    iget-wide v4, v3, Lcbn;->c:J

    add-long/2addr v0, v4

    iput-wide v0, v3, Lcbn;->d:J

    return-object v3

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcbm;->a:Lcdu;

    invoke-virtual {v2}, Lcdu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcea;->values()[Lcea;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-static {v5}, Lcea;->a(Lcea;)Lcdt;

    move-result-object v5

    invoke-virtual {v5}, Lcdt;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v6, v6}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    return v1

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcbm;->a:Lcdu;

    iget-object v0, v0, Lcdu;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcou;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcou;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_3
    iget-object v0, p0, Lcbm;->c:Lcby;

    invoke-interface {v0}, Lcby;->a()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, Lbpi;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    :try_start_0
    iget-object v2, p0, Lcbm;->c:Lcby;

    invoke-interface {v2}, Lcby;->c()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lbpi;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    :cond_4
    iget-object v2, p0, Lcbm;->c:Lcby;

    invoke-interface {v2}, Lcby;->d()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcbm;->c:Lcby;

    invoke-interface {v2}, Lcby;->d()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lbpi;->a(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    :cond_5
    :goto_4
    move v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_3
.end method
