.class public final Lgcl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lftb;

.field private final c:Lcom/google/android/gms/plus/model/posts/Post;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgcl;->b:Lftb;

    iput-object p3, p0, Lgcl;->c:Lcom/google/android/gms/plus/model/posts/Post;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lgcl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgcl;->c:Lcom/google/android/gms/plus/model/posts/Post;

    iget-object v2, p2, Lfrx;->c:Lfsj;

    invoke-virtual {v2, p1, v0, v1}, Lfsj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Post;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcl;->b:Lftb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcl;->b:Lftb;

    invoke-interface {v0, v6, v1, v4}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcl;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcl;->b:Lftb;

    invoke-interface {v1, v6, v0, v4}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "PlusInternalClient"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    iget-object v0, p0, Lgcl;->b:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgcl;->b:Lftb;

    if-eqz v0, :cond_0

    const-string v0, "PlusInternalClient"

    const-string v1, "Unable to load post settings"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lgcl;->b:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
