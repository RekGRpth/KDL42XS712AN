.class public final Lema;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lema;


# instance fields
.field private b:Ljava/lang/Object;

.field private c:I

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lema;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lema;-><init>(Ljava/lang/Object;IJ)V

    sput-object v0, Lema;->a:Lema;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;IJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lema;->b:Ljava/lang/Object;

    iput p2, p0, Lema;->c:I

    iput-wide p3, p0, Lema;->d:J

    return-void
.end method

.method public static a()Lema;
    .locals 1

    sget-object v0, Lema;->a:Lema;

    return-object v0
.end method

.method public static a(ILjava/lang/Object;J)Lema;
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    new-instance v0, Lema;

    invoke-direct {v0, p1, p0, p2, p3}, Lema;-><init>(Ljava/lang/Object;IJ)V

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;J)Lema;
    .locals 2

    new-instance v0, Lema;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1, p2}, Lema;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;J)Lema;
    .locals 2

    new-instance v0, Lema;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1, p1, p2}, Lema;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "unset"

    goto :goto_0

    :pswitch_1
    const-string v0, "set from runtime API"

    goto :goto_0

    :pswitch_2
    const-string v0, "set from resources"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lema;
    .locals 4

    new-instance v0, Lema;

    invoke-virtual {p0}, Lema;->b()I

    move-result v1

    iget-wide v2, p0, Lema;->d:J

    invoke-direct {v0, p1, v1, v2, v3}, Lema;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method public final declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lema;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;IJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Lema;->a(I)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    iput p2, p0, Lema;->c:I

    iput-object p1, p0, Lema;->b:Ljava/lang/Object;

    iput-wide p3, p0, Lema;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget v2, p0, Lema;->c:I

    if-eqz v2, :cond_0

    iget v2, p0, Lema;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v1, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    :try_start_1
    iget v2, p0, Lema;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    if-eq p1, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid source value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lema;)Z
    .locals 1

    invoke-virtual {p1}, Lema;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lema;->a(I)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lema;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lema;)V
    .locals 4

    invoke-virtual {p1}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lema;->b()I

    move-result v1

    invoke-virtual {p1}, Lema;->c()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lema;->a(Ljava/lang/Object;IJ)V

    return-void
.end method

.method public final declared-synchronized c()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lema;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/Object;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lema;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lema;->b()I

    move-result v0

    invoke-static {v0}, Lema;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lema;

    if-eqz v2, :cond_3

    check-cast p1, Lema;

    iget v2, p1, Lema;->c:I

    iget v3, p0, Lema;->c:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lema;->b:Ljava/lang/Object;

    iget-object v3, p1, Lema;->b:Ljava/lang/Object;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lema;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x353

    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, Lema;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lema;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
