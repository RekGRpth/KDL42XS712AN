.class final Lhzh;
.super Lhyx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhzh;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyx;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "UnknownActivityState"

    return-object v0
.end method

.method protected final d()I
    .locals 1

    const/16 v0, 0xb4

    return v0
.end method

.method protected final e()I
    .locals 8

    iget-object v0, p0, Lhzh;->c:Lhys;

    invoke-virtual {v0}, Lhys;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhzh;->c:Lhys;

    iget-object v0, v0, Lhys;->d:Lhxw;

    iget-object v1, p0, Lhzh;->a:Lhyt;

    invoke-static {v1}, Lhyt;->s(Lhyt;)Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->b()J

    move-result-wide v1

    const-wide/32 v3, 0xdbba0

    const-wide/32 v5, 0xafc80

    const/4 v7, 0x6

    invoke-virtual/range {v0 .. v7}, Lhxw;->a(JJJI)Z

    move-result v0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v1, "GeofencerStateMachine"

    const-string v2, "Might be stationary according to location history, doubling min location update interval."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v0, :cond_2

    const-wide/16 v0, 0x78

    :goto_1
    const/16 v2, 0x708

    iget-object v3, p0, Lhzh;->c:Lhys;

    iget v3, v3, Lhys;->a:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x3c

    goto :goto_1
.end method

.method protected final f()I
    .locals 2

    iget-object v0, p0, Lhzh;->c:Lhys;

    iget v0, v0, Lhys;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method protected final i()D
    .locals 2

    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    return-wide v0
.end method
