.class public final Lemy;
.super Liri;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Iterator;

.field private final b:Landroid/os/ParcelFileDescriptor;

.field private final c:Lemx;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Landroid/os/ParcelFileDescriptor;Lemx;)V
    .locals 0

    invoke-direct {p0}, Liri;-><init>()V

    iput-object p1, p0, Lemy;->a:Ljava/util/Iterator;

    iput-object p2, p0, Lemy;->b:Landroid/os/ParcelFileDescriptor;

    iput-object p3, p0, Lemy;->c:Lemx;

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 8

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_0
    iget-object v0, p0, Lemy;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lemy;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leis;

    iget-object v4, p0, Lemy;->c:Lemx;

    iget-object v5, v0, Leis;->a:Ljava/lang/String;

    iget-object v1, v0, Leis;->c:Ljava/lang/String;

    iget-wide v6, v0, Leis;->b:J

    invoke-virtual {v4, v5, v6, v7}, Lemx;->a(Ljava/lang/String;J)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-wide v6, v0, Leis;->f:J

    invoke-virtual {v4, v1, v6, v7}, Lemx;->a(Ljava/lang/String;J)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "Invalid usage report: package reinstalled since report"

    invoke-static {v1}, Lehe;->e(Ljava/lang/String;)I

    move-object v1, v2

    :cond_1
    :goto_1
    if-eqz v1, :cond_9

    iget-wide v2, v1, Lehh;->j:J

    new-instance v1, Leia;

    invoke-direct {v1}, Leia;-><init>()V

    iput-wide v2, v1, Leia;->a:J

    iget-object v2, v0, Leis;->e:Ljava/lang/String;

    iput-object v2, v1, Leia;->b:Ljava/lang/String;

    iget-wide v2, v0, Leis;->f:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Leia;->d:I

    iget v0, v0, Leis;->g:I

    iput v0, v1, Leia;->e:I

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    iget v1, v0, Leis;->g:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_4

    invoke-virtual {v4, v5}, Lemx;->a(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_3
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Illegal usage type: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Leis;->g:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " from "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->e(Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_1

    :cond_3
    iget-object v6, v4, Lemx;->c:Lelt;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v6, v1, v5}, Lelt;->a(ILjava/lang/String;)Lely;

    move-result-object v1

    iget-boolean v1, v1, Lelv;->c:Z

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v6, v0, Leis;->c:Ljava/lang/String;

    iget-object v7, v0, Leis;->d:Ljava/lang/String;

    invoke-direct {v1, v6, v7}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v4, Lemx;->d:Leiy;

    invoke-virtual {v6, v1}, Leiy;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, v4, Lemx;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v4, v5}, Lemx;->a(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    :goto_4
    iget-object v7, v4, Lemx;->b:Ljava/util/Map;

    invoke-interface {v7, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Invalid usage report: no access"

    invoke-static {v1}, Lehe;->e(Ljava/lang/String;)I

    move-object v1, v2

    goto/16 :goto_1

    :cond_6
    iget-object v7, v4, Lemx;->c:Lelt;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v7, v1, v5}, Lelt;->a(ILjava/lang/String;)Lely;

    move-result-object v1

    iget-object v7, v4, Lemx;->d:Leiy;

    invoke-virtual {v7, v1, v2, v3}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v1

    goto :goto_4

    :cond_7
    iget-object v1, v4, Lemx;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    goto :goto_5

    :cond_8
    iget-object v1, v4, Lemx;->d:Leiy;

    invoke-virtual {v1, v6}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "Invalid usage report: missing config"

    invoke-static {v1}, Lehe;->e(Ljava/lang/String;)I

    move-object v1, v2

    goto/16 :goto_1

    :cond_9
    const-string v1, "UsageReport from %s ignored -- did not pass authentication."

    iget-object v0, v0, Leis;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lehe;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lemy;->c()Ljava/lang/Object;

    move-object v0, v2

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lemy;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lemy;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lemy;->c:Lemx;

    iget-object v1, v0, Lemx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v0, v0, Lemx;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "Failed to close file descriptor."

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    goto :goto_0
.end method
