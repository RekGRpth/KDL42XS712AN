.class public final Litr;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lits;

.field public b:Litu;

.field public c:Litt;

.field public d:Litv;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Litr;->a:Lits;

    iput-object v0, p0, Litr;->b:Litu;

    iput-object v0, p0, Litr;->c:Litt;

    iput-object v0, p0, Litr;->d:Litv;

    const/4 v0, -0x1

    iput v0, p0, Litr;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Litr;->a:Lits;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Litr;->a:Lits;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Litr;->b:Litu;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Litr;->b:Litu;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Litr;->c:Litt;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Litr;->c:Litt;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Litr;->d:Litv;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Litr;->d:Litv;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Litr;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Litr;->a:Lits;

    if-nez v0, :cond_1

    new-instance v0, Lits;

    invoke-direct {v0}, Lits;-><init>()V

    iput-object v0, p0, Litr;->a:Lits;

    :cond_1
    iget-object v0, p0, Litr;->a:Lits;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Litr;->b:Litu;

    if-nez v0, :cond_2

    new-instance v0, Litu;

    invoke-direct {v0}, Litu;-><init>()V

    iput-object v0, p0, Litr;->b:Litu;

    :cond_2
    iget-object v0, p0, Litr;->b:Litu;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Litr;->c:Litt;

    if-nez v0, :cond_3

    new-instance v0, Litt;

    invoke-direct {v0}, Litt;-><init>()V

    iput-object v0, p0, Litr;->c:Litt;

    :cond_3
    iget-object v0, p0, Litr;->c:Litt;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Litr;->d:Litv;

    if-nez v0, :cond_4

    new-instance v0, Litv;

    invoke-direct {v0}, Litv;-><init>()V

    iput-object v0, p0, Litr;->d:Litv;

    :cond_4
    iget-object v0, p0, Litr;->d:Litv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Litr;->a:Lits;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Litr;->a:Lits;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Litr;->b:Litu;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Litr;->b:Litu;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Litr;->c:Litt;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Litr;->c:Litt;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Litr;->d:Litv;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Litr;->d:Litv;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Litr;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Litr;

    iget-object v2, p0, Litr;->a:Lits;

    if-nez v2, :cond_3

    iget-object v2, p1, Litr;->a:Lits;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Litr;->a:Lits;

    iget-object v3, p1, Litr;->a:Lits;

    invoke-virtual {v2, v3}, Lits;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Litr;->b:Litu;

    if-nez v2, :cond_5

    iget-object v2, p1, Litr;->b:Litu;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Litr;->b:Litu;

    iget-object v3, p1, Litr;->b:Litu;

    invoke-virtual {v2, v3}, Litu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Litr;->c:Litt;

    if-nez v2, :cond_7

    iget-object v2, p1, Litr;->c:Litt;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Litr;->c:Litt;

    iget-object v3, p1, Litr;->c:Litt;

    invoke-virtual {v2, v3}, Litt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Litr;->d:Litv;

    if-nez v2, :cond_9

    iget-object v2, p1, Litr;->d:Litv;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Litr;->d:Litv;

    iget-object v3, p1, Litr;->d:Litv;

    invoke-virtual {v2, v3}, Litv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Litr;->a:Lits;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Litr;->b:Litu;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Litr;->c:Litt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Litr;->d:Litv;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Litr;->a:Lits;

    invoke-virtual {v0}, Lits;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Litr;->b:Litu;

    invoke-virtual {v0}, Litu;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Litr;->c:Litt;

    invoke-virtual {v0}, Litt;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Litr;->d:Litv;

    invoke-virtual {v1}, Litv;->hashCode()I

    move-result v1

    goto :goto_3
.end method
