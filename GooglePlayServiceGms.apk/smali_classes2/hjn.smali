.class public final Lhjn;
.super Lhin;
.source "SourceFile"


# static fields
.field static final h:Ljava/util/Set;


# instance fields
.field final g:Lhqq;

.field i:Lhqy;

.field j:J

.field k:J

.field private final l:Lhjf;

.field private final m:Lhkd;

.field private final n:Lhjm;

.field private final o:Lhjp;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lhrz;

    const/4 v1, 0x0

    sget-object v2, Lhrz;->h:Lhrz;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lhrz;->g:Lhrz;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lhrz;->e:Lhrz;

    aput-object v2, v0, v1

    invoke-static {v0}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lhjn;->h:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;)V
    .locals 9

    invoke-static {}, Lhkd;->a()Lhkd;

    move-result-object v6

    new-instance v7, Lhjm;

    sget-object v0, Lhkg;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-static {}, Lhkd;->a()Lhkd;

    move-result-object v1

    invoke-direct {v7, p1, v0, p5, v1}, Lhjm;-><init>(Lidu;Lilt;Lhjf;Lhkd;)V

    new-instance v8, Lhjp;

    invoke-direct {v8, p5, p1}, Lhjp;-><init>(Lhjf;Lidu;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lhjn;-><init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhkd;Lhjm;Lhjp;)V

    return-void
.end method

.method private constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhkd;Lhjm;Lhjp;)V
    .locals 7

    const-string v1, "IOCollector"

    sget-object v6, Lhir;->b:Lhir;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    new-instance v0, Lhjo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhjo;-><init>(Lhjn;B)V

    iput-object v0, p0, Lhjn;->g:Lhqq;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjn;->j:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lhjn;->k:J

    iput-object p5, p0, Lhjn;->l:Lhjf;

    iput-object p6, p0, Lhjn;->m:Lhkd;

    iput-object p7, p0, Lhjn;->n:Lhjm;

    iput-object p8, p0, Lhjn;->o:Lhjp;

    return-void
.end method

.method static synthetic a(Lhjn;)Lhjp;
    .locals 1

    iget-object v0, p0, Lhjn;->o:Lhjp;

    return-object v0
.end method


# virtual methods
.method protected final b(J)Z
    .locals 13

    const-wide/16 v3, 0x2710

    const/4 v5, 0x0

    const-wide v11, 0x7fffffffffffffffL

    const/4 v10, 0x0

    const/4 v9, 0x1

    iget-wide v0, p0, Lhjn;->k:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lhjn;->k:J

    sub-long/2addr v0, v3

    cmp-long v0, v0, p1

    if-gtz v0, :cond_3

    iget-wide v0, p0, Lhjn;->k:J

    const-wide/32 v6, 0x2bf20

    add-long/2addr v0, v6

    cmp-long v0, p1, v0

    if-gtz v0, :cond_3

    move v0, v9

    :goto_0
    if-eqz v0, :cond_9

    iget-object v0, p0, Lhjn;->m:Lhkd;

    invoke-virtual {v0, v10}, Lhkd;->a(Z)Lhue;

    move-result-object v0

    iget-object v1, p0, Lhjn;->b:Lidu;

    invoke-interface {v1}, Lidu;->k()Z

    move-result v1

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    move v0, v9

    :goto_1
    if-eqz v0, :cond_8

    iput-wide v11, p0, Lhjn;->k:J

    iget-object v0, p0, Lhjn;->l:Lhjf;

    invoke-virtual {v0}, Lhjf;->j()V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v9}, Ljava/util/HashMap;-><init>(I)V

    sget-object v0, Lhrz;->e:Lhrz;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhjn;->b:Lidu;

    sget-object v1, Lhjn;->h:Ljava/util/Set;

    iget-object v6, p0, Lhjn;->g:Lhqq;

    iget-object v7, p0, Lhjn;->a:Ljava/lang/String;

    move-object v8, v5

    invoke-interface/range {v0 .. v8}, Lidu;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhqq;Ljava/lang/String;Lilx;)Lhqy;

    move-result-object v0

    iput-object v0, p0, Lhjn;->i:Lhqy;

    iget-object v0, p0, Lhjn;->i:Lhqy;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhjn;->i:Lhqy;

    invoke-interface {v0}, Lhqy;->a()V

    iput-wide p1, p0, Lhjn;->j:J

    :cond_0
    :goto_2
    iget-object v0, p0, Lhjn;->i:Lhqy;

    if-eqz v0, :cond_6

    move v0, v9

    :goto_3
    if-eqz v0, :cond_7

    sget-object v0, Lhir;->i:Lhir;

    iput-object v0, p0, Lhjn;->f:Lhir;

    :cond_1
    :goto_4
    iget-object v0, p0, Lhjn;->f:Lhir;

    sget-object v1, Lhir;->i:Lhir;

    if-eq v0, v1, :cond_2

    iget-wide v0, p0, Lhjn;->k:J

    cmp-long v0, v0, v11

    if-nez v0, :cond_2

    iget-object v0, p0, Lhjn;->m:Lhkd;

    invoke-virtual {v0}, Lhkd;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhjn;->n:Lhjm;

    invoke-virtual {v0}, Lhjm;->a()Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lhjn;->k:J

    :cond_2
    iget-object v0, p0, Lhjn;->f:Lhir;

    sget-object v1, Lhir;->b:Lhir;

    if-eq v0, v1, :cond_b

    move v0, v9

    :goto_5
    return v0

    :cond_3
    move v0, v10

    goto/16 :goto_0

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Failed to create signal collector."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v0, v10

    goto :goto_3

    :cond_7
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Unable to start collection. Terminate early."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Bad phone conditions, waiting it to be good."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    iget-wide v0, p0, Lhjn;->k:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lhjn;->k:J

    const-wide/32 v2, 0x2bf20

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Alarm timed out, will schedule another attempt."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iput-wide v11, p0, Lhjn;->k:J

    iget-object v0, p0, Lhjn;->l:Lhjf;

    invoke-virtual {v0}, Lhjf;->j()V

    goto :goto_4

    :cond_b
    move v0, v10

    goto :goto_5
.end method

.method protected final d()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lhjn;->i:Lhqy;

    if-nez v0, :cond_0

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhjn;->f:Lhir;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhjn;->m:Lhkd;

    invoke-virtual {v0, v1}, Lhkd;->a(Z)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhjn;->a:Ljava/lang/String;

    const-string v2, "Bad device conditions, stopping IOCollector."

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhjn;->i:Lhqy;

    invoke-interface {v0}, Lhqy;->b()V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lhin;->f()V

    return-void
.end method
