.class public final Lhce;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhce;->a:Landroid/content/Context;

    iput-object p2, p0, Lhce;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    const/4 v7, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lhce;->a:Landroid/content/Context;

    invoke-static {v1}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v2, 0x0

    :try_start_0
    const-string v1, "get_auth_token"

    move v8, v0

    move-object v0, v2

    move-object v2, v1

    move v1, v8

    :goto_1
    if-gt v1, v7, :cond_0

    new-instance v0, Luu;

    const-string v3, "get_auth_tokens"

    invoke-direct {v0, v3}, Luu;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v3

    iget-object v4, p0, Lhce;->a:Landroid/content/Context;

    invoke-virtual {p1, v4}, Lhcc;->a(Landroid/content/Context;)Lhgm;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v0, v3, v5}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v2, p0, Lhce;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lgsp;->a(Landroid/content/Context;Luu;)V

    invoke-virtual {p1, v4}, Lhcc;->a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhcc;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    iget-object v0, p0, Lhce;->a:Landroid/content/Context;

    invoke-virtual {v4}, Lhgm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "get_fresh_auth_token"
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lhce;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhce;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lhce;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
