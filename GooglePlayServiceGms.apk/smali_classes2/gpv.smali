.class final Lgpv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field final synthetic a:Lgpr;


# direct methods
.method private constructor <init>(Lgpr;)V
    .locals 0

    iput-object p1, p0, Lgpv;->a:Lgpr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgpr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgpv;-><init>(Lgpr;)V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->k(Lgpr;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->i(Lgpr;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->b(Lgpr;)Lftx;

    move-result-object v0

    invoke-interface {v0}, Lftx;->a()V

    :cond_1
    return-void
.end method

.method public final a(Lbbo;)V
    .locals 4

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lgpv;->a:Lgpr;

    sget-object v0, Lfsr;->ad:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lgpr;->a(Lgpr;J)J

    :cond_0
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    invoke-interface {v0, p1}, Lgpw;->a(Lbbo;)V

    :cond_1
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lgpv;->a:Lgpr;

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->b(Lgpr;)Lftx;

    move-result-object v2

    invoke-interface {v2}, Lftx;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lgpr;->a(Lgpr;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->a:Ljava/lang/String;

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    :goto_0
    iget-object v2, p0, Lgpv;->a:Lgpr;

    const/4 v4, -0x1

    invoke-static {v2, v4}, Lgpr;->a(Lgpr;I)I

    array-length v4, v0

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    iget-object v6, p0, Lgpv;->a:Lgpr;

    invoke-static {v6}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v4, p0, Lgpv;->a:Lgpr;

    invoke-static {v4, v2}, Lgpr;->a(Lgpr;I)I

    :cond_0
    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->f(Lgpr;)I

    move-result v2

    if-gez v2, :cond_1

    const-string v2, "ShareBox"

    const-string v4, "Resolved account name not found among share eligable accounts"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2, v3}, Lgpr;->a(Lgpr;I)I

    :cond_1
    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v2

    iget-object v3, p0, Lgpv;->a:Lgpr;

    invoke-static {v3}, Lgpr;->f(Lgpr;)I

    move-result v3

    invoke-interface {v2, v0, v3}, Lgpw;->a([Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->g(Lgpr;)V

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_3
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->i(Lgpr;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->b(Lgpr;)Lftx;

    move-result-object v0

    iget-object v1, p0, Lgpv;->a:Lgpr;

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->j(Lgpr;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lftx;->a(Lfun;Lcom/google/android/gms/plus/model/posts/Post;)V

    :goto_2
    return-void

    :cond_4
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->e(Lgpr;)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgpv;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_3
    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->g()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, p0, Lgpv;->a:Lgpr;

    invoke-static {v1}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->p:Lgqa;

    iget-object v1, v1, Lgqa;->a:Landroid/os/Bundle;

    :cond_7
    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpv;->a:Lgpr;

    invoke-static {v3}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v3

    iget-object v3, v3, Lgpx;->q:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v4, p0, Lgpv;->a:Lgpr;

    invoke-static {v4}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v4

    iget-object v4, v4, Lgpx;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/model/posts/Post;->a(Landroid/net/Uri;Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/String;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v0

    iget-object v1, p0, Lgpv;->a:Lgpr;

    invoke-static {v1}, Lgpr;->k(Lgpr;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lgpv;->a:Lgpr;

    invoke-static {v1}, Lgpr;->b(Lgpr;)Lftx;

    move-result-object v1

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-interface {v1, v2, v0}, Lftx;->a(Lfum;Lcom/google/android/gms/plus/model/posts/Post;)V

    :goto_4
    iget-object v1, p0, Lgpv;->a:Lgpr;

    invoke-static {v1}, Lgpr;->l(Lgpr;)Lfst;

    move-result-object v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lgpv;->a:Lgpr;

    invoke-static {v1}, Lgpr;->b(Lgpr;)Lftx;

    move-result-object v1

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-interface {v1, v2, v0}, Lftx;->a(Lful;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    iget-object v1, p0, Lgpv;->a:Lgpr;

    sget-object v2, Lbbo;->a:Lbbo;

    iget-object v3, p0, Lgpv;->a:Lgpr;

    invoke-static {v3}, Lgpr;->k(Lgpr;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lgpr;->a(Lbbo;Lcom/google/android/gms/plus/model/posts/Settings;)V

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lgpv;->a:Lgpr;

    sget-object v1, Lbbo;->a:Lbbo;

    iget-object v2, p0, Lgpv;->a:Lgpr;

    invoke-static {v2}, Lgpr;->l(Lgpr;)Lfst;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgpr;->a(Lbbo;Lfst;)V

    goto/16 :goto_2
.end method
