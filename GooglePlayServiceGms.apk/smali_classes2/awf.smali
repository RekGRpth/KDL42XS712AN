.class final Lawf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lawe;


# direct methods
.method constructor <init>(Lawe;)V
    .locals 0

    iput-object p1, p0, Lawf;->a:Lawe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    const/4 v13, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lawf;->a:Lawe;

    iget-object v0, v0, Lawe;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v4, p0, Lawf;->a:Lawe;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    iget-boolean v0, v4, Lawe;->f:Z

    if-nez v0, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v0, v4, Lawe;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v8, v4, Lawe;->d:Ljava/util/LinkedList;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v4, Lawe;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Lawc;->a()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    iget-object v10, v4, Lawe;->e:Ljava/nio/channels/Selector;

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->attach(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v4, Lawe;->k:Landroid/content/Context;

    iget-object v10, v4, Lawe;->l:Landroid/content/Intent;

    invoke-virtual {v1, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    iget-object v10, v4, Lawe;->b:Laye;

    const-string v11, "Error while connecting socket."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v1, v11, v12}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v8

    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_1
    move-exception v0

    :try_start_5
    iget-object v1, p0, Lawf;->a:Lawe;

    iget-object v1, v1, Lawe;->b:Laye;

    const-string v2, "Unexpected throwable in selector loop"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v4}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lawf;->a:Lawe;

    iput-object v0, v1, Lawe;->j:Ljava/lang/Throwable;

    iget-object v0, p0, Lawf;->a:Lawe;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lawe;->g:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    iget-object v0, p0, Lawf;->a:Lawe;

    iget-object v0, v0, Lawe;->b:Laye;

    const-string v1, "**** selector loop thread exiting"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lawf;->a:Lawe;

    invoke-static {v0}, Lawe;->a(Lawe;)V

    iget-object v0, p0, Lawf;->a:Lawe;

    iput-object v13, v0, Lawe;->h:Ljava/lang/Thread;

    :goto_1
    return-void

    :cond_1
    :try_start_6
    iget-object v0, v4, Lawe;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_2
    :try_start_7
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawc;

    invoke-virtual {v0}, Lawc;->i()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lawf;->a:Lawe;

    iget-object v1, v1, Lawe;->b:Laye;

    const-string v2, "**** selector loop thread exiting"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lawf;->a:Lawe;

    invoke-static {v1}, Lawe;->a(Lawe;)V

    iget-object v1, p0, Lawf;->a:Lawe;

    iput-object v13, v1, Lawe;->h:Ljava/lang/Thread;

    throw v0

    :cond_3
    :try_start_8
    invoke-interface {v5}, Ljava/util/List;->clear()V

    :cond_4
    iget-object v0, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v3

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawc;

    invoke-virtual {v0}, Lawc;->l()Ljava/nio/channels/SocketChannel;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v10, v4, Lawe;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v9, v10}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v10

    if-eqz v10, :cond_5

    iget-object v10, v4, Lawe;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v9, v10}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v9

    invoke-virtual {v0, v9, v6, v7}, Lawc;->a(Ljava/nio/channels/SelectionKey;J)Z

    move-result v9

    if-nez v9, :cond_6

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, Lawc;->d()Z

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v0}, Lawc;->e()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_7
    move v0, v2

    :goto_4
    move v1, v0

    goto :goto_3

    :cond_8
    iget-object v0, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v4, Lawe;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    iget-object v0, v4, Lawe;->k:Landroid/content/Context;

    iget-object v6, v4, Lawe;->l:Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_9
    :try_start_9
    invoke-virtual {v4}, Lawe;->d()Z

    iget-object v6, v4, Lawe;->e:Ljava/nio/channels/Selector;

    if-eqz v1, :cond_e

    const-wide/16 v0, 0x3e8

    :goto_6
    invoke-virtual {v6, v0, v1}, Ljava/nio/channels/Selector;->select(J)I

    move-result v0

    invoke-virtual {v4}, Lawe;->c()V
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_0

    :goto_7
    :try_start_a
    iget-boolean v0, v4, Lawe;->f:Z

    if-nez v0, :cond_f

    iget-object v0, v4, Lawe;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    :try_start_b
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lawc;

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v1}, Lawc;->h()Z

    move-result v7

    if-nez v7, :cond_a

    iget-object v7, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :cond_a
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-virtual {v1}, Lawc;->j()Z

    move-result v7

    if-nez v7, :cond_b

    iget-object v7, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :cond_b
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {v1}, Lawc;->k()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, v4, Lawe;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_c
    :goto_9
    :try_start_c
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_8

    :cond_d
    move v0, v3

    goto :goto_5

    :cond_e
    const-wide/16 v0, 0x0

    goto :goto_6

    :cond_f
    iget-object v0, p0, Lawf;->a:Lawe;

    iget-object v0, v0, Lawe;->b:Laye;

    const-string v1, "**** selector loop thread exiting"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lawf;->a:Lawe;

    invoke-static {v0}, Lawe;->a(Lawe;)V

    iget-object v0, p0, Lawf;->a:Lawe;

    iput-object v13, v0, Lawe;->h:Ljava/lang/Thread;

    goto/16 :goto_1

    :catch_2
    move-exception v0

    goto :goto_9

    :catch_3
    move-exception v0

    goto :goto_7

    :cond_10
    move v0, v1

    goto/16 :goto_4
.end method
