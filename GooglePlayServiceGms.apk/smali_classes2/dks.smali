.class final Ldks;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldkn;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldkn;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldks;->a:Ldkn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ldks;->b:Ljava/lang/String;

    iput-object p3, p0, Ldks;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Ldks;->a:Ldkn;

    invoke-static {v0}, Ldkn;->a(Ldkn;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldks;->a:Ldkn;

    invoke-static {v0}, Ldkn;->b(Ldkn;)Ldkj;

    move-result-object v0

    iget-object v1, p0, Ldks;->b:Ljava/lang/String;

    iget-object v2, p0, Ldks;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldkj;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ldkn;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ignoring onReceiveBuzzNotification() callback."

    invoke-static {v0, v1}, Ldav;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
