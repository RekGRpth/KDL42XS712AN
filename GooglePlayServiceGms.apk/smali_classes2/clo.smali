.class public final Lclo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lclk;


# instance fields
.field private final a:Lcom/google/android/gms/drive/internal/model/File;

.field private final b:Lbsp;

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/internal/model/File;Lbsp;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    iput-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    iput-object p2, p0, Lclo;->b:Lbsp;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->z()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "root"

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lclo;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final A()J
    .locals 2

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public final B()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->g()Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->d()Z

    move-result v0

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final E()J
    .locals 2

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->A()J

    move-result-wide v0

    return-wide v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->G()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->D()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final I()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->e()Z

    move-result v0

    return v0
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->e()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->B()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lbsp;
    .locals 1

    iget-object v0, p0, Lclo;->b:Lbsp;

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lclo;->c:Ljava/util/Set;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->C()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->E()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->f()Z

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->n()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->k()Z

    move-result v0

    return v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->y()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Z
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->g()Z

    move-result v0

    return v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lclo;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
