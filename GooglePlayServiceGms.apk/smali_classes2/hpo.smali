.class final Lhpo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "com.google.android.apps.maps.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lhpo;->a:Ljava/util/regex/Pattern;

    new-instance v0, Lhpp;

    const-string v1, "com.google.android.apps.modis"

    sget-object v4, Lhrz;->l:Ljava/util/Set;

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lhpp;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    new-instance v4, Lhpp;

    const-string v5, "com.google.android.apps.maps"

    sget-object v8, Lhrz;->l:Ljava/util/Set;

    move v6, v2

    move v7, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lhpp;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    new-instance v5, Lhpp;

    const-string v6, "com.google.android.gms"

    sget-object v9, Lhrz;->l:Ljava/util/Set;

    move v7, v2

    move v8, v3

    move v10, v2

    invoke-direct/range {v5 .. v10}, Lhpp;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    const/16 v1, 0x8

    new-array v1, v1, [Lhrz;

    sget-object v6, Lhrz;->a:Lhrz;

    aput-object v6, v1, v2

    sget-object v6, Lhrz;->b:Lhrz;

    aput-object v6, v1, v3

    const/4 v6, 0x2

    sget-object v7, Lhrz;->d:Lhrz;

    aput-object v7, v1, v6

    const/4 v6, 0x3

    sget-object v7, Lhrz;->g:Lhrz;

    aput-object v7, v1, v6

    const/4 v6, 0x4

    sget-object v7, Lhrz;->h:Lhrz;

    aput-object v7, v1, v6

    const/4 v6, 0x5

    sget-object v7, Lhrz;->d:Lhrz;

    aput-object v7, v1, v6

    const/4 v6, 0x6

    sget-object v7, Lhrz;->e:Lhrz;

    aput-object v7, v1, v6

    const/4 v6, 0x7

    sget-object v7, Lhrz;->f:Lhrz;

    aput-object v7, v1, v6

    invoke-static {v1}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v10

    new-instance v6, Lhpp;

    const-string v7, "com.google.location.lbs.collectionlib"

    move v8, v3

    move v9, v2

    move v11, v3

    invoke-direct/range {v6 .. v11}, Lhpp;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    invoke-static {}, Lhsn;->d()Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lhpp;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v4, Lhpp;->a:Ljava/lang/String;

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Lhpp;->a:Ljava/lang/String;

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lhpp;->a:Ljava/lang/String;

    invoke-interface {v1, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lhpo;->b:Ljava/util/Map;

    return-void
.end method

.method public static a(Ljava/lang/String;Lhqk;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lhpo;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "com.google.android.apps.maps"

    :cond_0
    sget-object v0, Lhpo;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpp;

    if-nez v0, :cond_1

    new-instance v0, Lhpq;

    const-string v1, "%s cannot access to this library. Please contact lbs-team@google.com."

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lhpq;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1}, Lhqk;->d()Lhql;

    move-result-object v1

    sget-object v4, Lhql;->c:Lhql;

    if-ne v1, v4, :cond_6

    move v1, v2

    :goto_0
    invoke-interface {p1}, Lhqk;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-interface {p1}, Lhqk;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lhpp;->a(Ljava/lang/String;)Z

    move-result v4

    :goto_1
    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lhpp;->b:Z

    if-eqz v4, :cond_5

    :cond_2
    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lhpp;->c:Z

    if-eqz v1, :cond_5

    :cond_3
    invoke-interface {p1}, Lhqk;->i()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, v0, Lhpp;->e:Z

    if-eqz v1, :cond_5

    :cond_4
    iget-object v0, v0, Lhpp;->d:Ljava/util/Set;

    invoke-interface {p1}, Lhqk;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v3, v2

    :cond_5
    if-nez v3, :cond_7

    new-instance v0, Lhpq;

    const-string v1, "Some features are prohibited from use by this application. Please contact lbs-team@google.com"

    invoke-direct {v0, v1}, Lhpq;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v1, v3

    goto :goto_0

    :cond_7
    return-void

    :cond_8
    move v4, v3

    goto :goto_1
.end method
