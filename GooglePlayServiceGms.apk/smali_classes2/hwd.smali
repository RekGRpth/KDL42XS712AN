.class final Lhwd;
.super Lhws;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhwb;


# direct methods
.method public constructor <init>(Lhwb;Lhxf;)V
    .locals 1

    iput-object p1, p0, Lhwd;->a:Lhwb;

    new-instance v0, Lhuz;

    invoke-direct {v0}, Lhuz;-><init>()V

    invoke-direct {p0, p2, v0}, Lhws;-><init>(Lhxf;Lhuz;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lhwd;->a:Lhwb;

    invoke-static {v0}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v0

    invoke-virtual {v0}, Lhxc;->c()V

    return-void
.end method

.method public final a(Z)V
    .locals 6

    const-wide/32 v4, 0x5265c00

    iget-object v0, p0, Lhws;->f:Lhww;

    invoke-virtual {v0}, Lhww;->d()J

    move-result-wide v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lhwd;->a:Lhwb;

    invoke-static {v2}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lhxc;->a(J)V

    iget-object v2, p0, Lhwd;->a:Lhwb;

    invoke-static {v2}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v2

    iget-object v3, p0, Lhws;->e:Lhwm;

    iget-boolean v3, v3, Lhwm;->e:Z

    invoke-virtual {v2, v3}, Lhxc;->b(Z)V

    iget-object v2, p0, Lhwd;->a:Lhwb;

    invoke-static {v2}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v2

    if-eqz p1, :cond_0

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lhxa;->b:Z

    iget-object v0, p0, Lhwd;->a:Lhwb;

    invoke-static {v0}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v0

    iget-object v1, p0, Lhws;->f:Lhww;

    invoke-virtual {v1}, Lhww;->e()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhxc;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lhwd;->a:Lhwb;

    invoke-static {v0}, Lhwb;->d(Lhwb;)Lhxc;

    move-result-object v0

    invoke-virtual {v0}, Lhxc;->b()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
