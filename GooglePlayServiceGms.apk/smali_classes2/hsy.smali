.class public final Lhsy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lhsy;


# instance fields
.field private final b:Lixw;


# direct methods
.method private constructor <init>(Lixw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhsy;->b:Lixw;

    return-void
.end method

.method public static declared-synchronized a()Lhsy;
    .locals 3

    const-class v1, Lhsy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhsy;->a:Lhsy;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Please call init() before calling getInstance."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lhsy;->a:Lhsy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Liya;)V
    .locals 3

    const-class v1, Lhsy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhsy;->a:Lhsy;

    if-nez v0, :cond_0

    invoke-static {p0}, Liun;->a(Landroid/content/Context;)Liun;

    invoke-static {p1}, Lixw;->a(Liya;)V

    new-instance v0, Lhsy;

    invoke-static {}, Lixw;->b()Lixw;

    move-result-object v2

    invoke-direct {v0, v2}, Lhsy;-><init>(Lixw;)V

    sput-object v0, Lhsy;->a:Lhsy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Liyz;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhsy;->b:Lixw;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lixw;->a(Liyz;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
