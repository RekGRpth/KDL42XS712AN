.class final Lfgp;
.super Lfgu;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p8, p0, Lfgp;->c:Ljava/lang/String;

    iput-object p9, p0, Lfgp;->d:Ljava/lang/String;

    iput-object p10, p0, Lfgp;->e:Ljava/lang/String;

    iput-object p11, p0, Lfgp;->f:Ljava/lang/String;

    iput-object p12, p0, Lfgp;->g:Ljava/lang/String;

    iput-object p13, p0, Lfgp;->h:Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lfgp;->c:Ljava/lang/String;

    iget-object v1, p0, Lfgp;->d:Ljava/lang/String;

    invoke-virtual {p2, p3, v0, v1}, Lffe;->a(Lffa;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfgp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfgp;->f:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lfgp;->g:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v2, v6, v3}, Lffu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_0
    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-ne v0, v6, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "circle_id"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_name"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-object v0

    :cond_1
    iget-object v0, p0, Lfgp;->e:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iget-object v2, p0, Lfgp;->g:Ljava/lang/String;

    iget-object v3, p0, Lfgp;->h:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :cond_3
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->d:Lffc;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2
.end method
