.class public Lgtu;
.super Lsc;
.source "SourceFile"


# instance fields
.field protected final f:Landroid/content/Context;

.field private final g:Lsk;

.field private final h:Ljava/lang/String;

.field private final i:Luu;

.field private final j:Lut;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://i18napis.appspot.com/address/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-object p1, p0, Lgtu;->f:Landroid/content/Context;

    iput-object p3, p0, Lgtu;->g:Lsk;

    iput-object p2, p0, Lgtu;->h:Ljava/lang/String;

    new-instance v0, Luu;

    const-string v1, "get_address_metadata"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgtu;->i:Luu;

    iget-object v0, p0, Lgtu;->i:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lgtu;->j:Lut;

    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4

    const/4 v2, -0x1

    if-nez p1, :cond_0

    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    const-string v0, "id"

    invoke-static {p1, v0}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v0, p0, Lgtu;->h:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    const-string v1, "key"

    invoke-static {p1, v1}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_3

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    :goto_1
    if-eq v1, v2, :cond_2

    :try_start_1
    const-string v2, "key"

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    return-object p1

    :catch_0
    move-exception v1

    const-string v1, "AddressMetadataRetrievalRequest"

    const-string v3, "Could not specify key... results unknown"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v0, "AddressMetadataRetrievalRequest"

    const-string v1, "Could not specify key... results unknown"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method protected a(Lrz;)Lsi;
    .locals 7

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lrz;->b:[B

    iget-object v2, p1, Lrz;->c:Ljava/util/Map;

    invoke-static {v2}, Ltb;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-direct {p0, v1}, Lgtu;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    iget-object v0, p0, Lgtu;->i:Luu;

    iget-object v2, p0, Lgtu;->j:Lut;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "rpc"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgtu;->f:Landroid/content/Context;

    iget-object v2, p0, Lgtu;->i:Luu;

    invoke-static {v0, v2}, Lgsp;->a(Landroid/content/Context;Luu;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Lrp;

    invoke-direct {v4}, Lrp;-><init>()V

    iget-object v0, p1, Lrz;->b:[B

    iput-object v0, v4, Lrp;->a:[B

    const/4 v0, 0x0

    iput-object v0, v4, Lrp;->b:Ljava/lang/String;

    iput-wide v2, v4, Lrp;->c:J

    sget-object v0, Lgzp;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v5, v0

    add-long/2addr v2, v5

    iput-wide v2, v4, Lrp;->d:J

    iget-object v0, p1, Lrz;->c:Ljava/util/Map;

    iput-object v0, v4, Lrp;->f:Ljava/util/Map;

    iget-wide v2, v4, Lrp;->d:J

    iput-wide v2, v4, Lrp;->e:J

    invoke-static {v1, v4}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AddressMetadataRetrievalRequest"

    const-string v2, "Could not parse server response"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lsb;

    invoke-direct {v1, v0}, Lsb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lsi;->a(Lsp;)Lsi;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lsb;

    invoke-direct {v1, v0}, Lsb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lsi;->a(Lsp;)Lsi;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lorg/json/JSONObject;

    iget-object v0, p0, Lgtu;->g:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final o()Lse;
    .locals 1

    sget-object v0, Lse;->c:Lse;

    return-object v0
.end method
