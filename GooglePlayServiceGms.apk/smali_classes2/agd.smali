.class final enum Lagd;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lagd;

.field public static final enum b:Lagd;

.field public static final enum c:Lagd;

.field private static final synthetic d:[Lagd;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lagd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lagd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagd;->a:Lagd;

    new-instance v0, Lagd;

    const-string v1, "GZIP"

    invoke-direct {v0, v1, v3}, Lagd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagd;->b:Lagd;

    new-instance v0, Lagd;

    const-string v1, "DEFLATE"

    invoke-direct {v0, v1, v4}, Lagd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagd;->c:Lagd;

    const/4 v0, 0x3

    new-array v0, v0, [Lagd;

    sget-object v1, Lagd;->a:Lagd;

    aput-object v1, v0, v2

    sget-object v1, Lagd;->b:Lagd;

    aput-object v1, v0, v3

    sget-object v1, Lagd;->c:Lagd;

    aput-object v1, v0, v4

    sput-object v0, Lagd;->d:[Lagd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Ljava/lang/String;)Lagd;
    .locals 1

    const-string v0, "GZIP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lagd;->b:Lagd;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "DEFLATE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lagd;->c:Lagd;

    goto :goto_0

    :cond_1
    sget-object v0, Lagd;->a:Lagd;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lagd;
    .locals 1

    const-class v0, Lagd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lagd;

    return-object v0
.end method

.method public static values()[Lagd;
    .locals 1

    sget-object v0, Lagd;->d:[Lagd;

    invoke-virtual {v0}, [Lagd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lagd;

    return-object v0
.end method
