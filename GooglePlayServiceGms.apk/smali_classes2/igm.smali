.class final Ligm;
.super Lerr;
.source "SourceFile"


# instance fields
.field final synthetic a:Ligi;


# direct methods
.method private constructor <init>(Ligi;)V
    .locals 0

    iput-object p1, p0, Ligm;->a:Ligi;

    invoke-direct {p0}, Lerr;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ligi;B)V
    .locals 0

    invoke-direct {p0, p1}, Ligm;-><init>(Ligi;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "PlaceReporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "place estimation failed, status = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Ligm;->a:Ligi;

    invoke-static {}, Lill;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v6, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, v6, Ligi;->c:Landroid/content/pm/PackageManager;

    iget-object v3, v1, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    iget-object v1, v1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-static {v1, v2}, Lilm;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v1

    invoke-static {v3}, Ligt;->a(Lcom/google/android/gms/location/places/PlaceRequest;)I

    move-result v2

    if-lt v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-nez v0, :cond_3

    const-string v0, "PlaceReporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cancelling place request that lacks required permissions: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v6, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, v2, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v6, Ligi;->g:Ligk;

    invoke-interface {v0, v6}, Ligk;->a(Ligi;)V

    :cond_2
    :goto_1
    const-string v0, "PlaceReporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "received place estimate for subscription: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ligm;->a:Ligi;

    iget-object v2, v2, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {v2}, Lcom/google/android/location/places/PlaceSubscription;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    :try_start_0
    iget-object v0, v6, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v0, v0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    iget-object v1, v6, Ligi;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v4, v6, Ligi;->j:Landroid/app/PendingIntent$OnFinished;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlaceReporter"

    const-string v1, "pending intent cancelled by client"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v6, Ligi;->g:Ligk;

    invoke-interface {v0, v6}, Ligk;->a(Ligi;)V

    goto :goto_1
.end method
