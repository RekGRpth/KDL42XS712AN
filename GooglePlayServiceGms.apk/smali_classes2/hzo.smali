.class public final Lhzo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Ljava/text/SimpleDateFormat;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation
.end field


# instance fields
.field a:Lhzp;

.field b:Ljava/util/LinkedList;

.field final c:Ljava/util/LinkedList;

.field private final e:Lbpe;

.field private final f:Lhyt;

.field private final g:Lhyn;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy.MM.dd HH:mm:ss "

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhzo;->d:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Lhyn;Lbpe;Lhyt;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lhzp;->a:Lhzp;

    iput-object v0, p0, Lhzo;->a:Lhzp;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    iput-object p1, p0, Lhzo;->g:Lhyn;

    iput-object p2, p0, Lhzo;->e:Lbpe;

    iput-object p3, p0, Lhzo;->f:Lhyt;

    return-void
.end method

.method private declared-synchronized a(J)Landroid/util/Pair;
    .locals 12

    const/4 v3, 0x0

    const/4 v9, 0x3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    move-wide v4, p1

    :goto_0
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move-object v1, v3

    :goto_1
    if-ltz v2, :cond_4

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v6

    cmp-long v6, v6, p1

    if-gtz v6, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-ltz v6, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v6

    if-ne v6, v9, :cond_5

    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move-wide v1, p1

    :goto_3
    if-ltz v4, :cond_3

    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    iget-wide v5, v0, Lhyd;->b:J

    cmp-long v5, v5, p1

    if-gtz v5, :cond_a

    iget-object v5, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_2

    iget-boolean v5, v0, Lhyd;->c:Z

    if-nez v5, :cond_2

    iget-wide v0, v0, Lhyd;->b:J

    :cond_1
    :goto_4
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move-wide v10, v0

    move-wide v1, v10

    goto :goto_3

    :cond_2
    iget-wide v5, v0, Lhyd;->d:J

    sub-long v5, v1, v5

    const-wide/32 v7, 0xa1220

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    iget-wide v0, v0, Lhyd;->b:J

    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "MovementManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Merging periods with small gaps, lastDetectionStart = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-wide v4, v1

    goto/16 :goto_0

    :cond_4
    move-object v0, v3

    :cond_5
    :try_start_1
    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_6

    const-string v2, "MovementManager"

    const-string v3, "lastDetectionStart=%d, checkTime=%d, firstStill=%s, lastNonStill=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v6, v4

    const/4 v4, 0x2

    aput-object v1, v6, v4

    const/4 v4, 0x3

    aput-object v0, v6, v4

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-wide/16 v2, 0x0

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v2

    sub-long v2, p1, v2

    move-wide v4, v2

    :goto_5
    const-wide v2, 0x7fffffffffffffffL

    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v6

    sub-long v0, v1, v6

    :goto_6
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_7
    move-wide v0, v2

    goto :goto_6

    :cond_8
    move-wide v4, v2

    goto :goto_5

    :cond_9
    move-object v0, v1

    goto/16 :goto_2

    :cond_a
    move-wide v0, v1

    goto/16 :goto_4
.end method

.method private static a(Lcom/google/android/gms/location/ActivityRecognitionResult;I)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 4

    new-instance v0, Lhkm;

    invoke-direct {v0}, Lhkm;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhkm;->b(J)Lhkm;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lhkm;->a(J)Lhkm;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhkm;->a(Ljava/util/List;)Lhkm;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x46

    invoke-virtual {v1, v2, v3}, Lhkm;->a(Ljava/lang/Integer;I)Lhkm;

    invoke-virtual {v0}, Lhkm;->a()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/LinkedList;)V
    .locals 6

    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    iget-boolean v2, v0, Lhyd;->c:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhzo;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->b()J

    move-result-wide v2

    iget-wide v4, v0, Lhyd;->d:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xa1220

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(J)Lhzp;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lhzp;->a:Lhzp;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    if-ne v4, v8, :cond_1

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/32 v5, 0xa1220

    cmp-long v0, v0, v5

    if-lez v0, :cond_2

    sget-object v0, Lhzp;->a:Lhzp;

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    if-eqz v4, :cond_3

    if-ne v4, v2, :cond_4

    :cond_3
    sget-object v0, Lhzp;->e:Lhzp;

    goto :goto_0

    :cond_4
    if-ne v4, v7, :cond_5

    sget-object v0, Lhzp;->d:Lhzp;

    goto :goto_0

    :cond_5
    if-ne v4, v9, :cond_8

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v0

    iget-object v2, p0, Lhzo;->a:Lhzp;

    sget-object v3, Lhzp;->c:Lhzp;

    if-eq v2, v3, :cond_7

    iget-object v2, p0, Lhzo;->a:Lhzp;

    sget-object v3, Lhzp;->e:Lhzp;

    if-ne v2, v3, :cond_6

    const/16 v2, 0xa

    if-le v1, v2, :cond_6

    sget-object v0, Lhzp;->e:Lhzp;

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lhzo;->a:Lhzp;

    sget-object v2, Lhzp;->d:Lhzp;

    if-ne v1, v2, :cond_7

    const/16 v1, 0xa

    if-le v0, v1, :cond_7

    sget-object v0, Lhzp;->d:Lhzp;

    goto :goto_0

    :cond_7
    sget-object v0, Lhzp;->b:Lhzp;

    goto :goto_0

    :cond_8
    if-ne v4, v8, :cond_b

    invoke-direct {p0, p1, p2}, Lhzo;->a(J)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0xdbba0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_9

    sget-object v0, Lhzp;->c:Lhzp;

    goto/16 :goto_0

    :cond_9
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa1220

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    sget-object v0, Lhzp;->a:Lhzp;

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lhzo;->a:Lhzp;

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lhzp;->b:Lhzp;

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 7

    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v1

    :goto_0
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v3

    sub-long v3, v1, v3

    const-wide/32 v5, 0xbbaee0

    cmp-long v0, v3, v5

    if-lez v0, :cond_0

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method final declared-synchronized a()Lhye;
    .locals 6

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_3

    new-instance v1, Lhye;

    invoke-direct {v1}, Lhye;-><init>()V

    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v2

    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lhye;->a(J)Lhye;

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-static {v0}, Lhxz;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Lihh;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lhye;->a(Lihh;)Lhye;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    invoke-virtual {v1, v0}, Lhye;->a(Lhyd;)Lhye;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_2
    move-object v0, v1

    :cond_3
    monitor-exit p0

    return-object v0
.end method

.method final declared-synchronized a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 10

    const/16 v9, 0xa

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MovementManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    :cond_1
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "MovementManager"

    const-string v1, "Ignoring same result."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_4

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "MovementManager"

    const-string v1, "Ignoring low confidence tilting results."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    iget-object v3, p0, Lhzo;->g:Lhyn;

    iget-boolean v3, v3, Lhyn;->a:Z

    if-eqz v3, :cond_5

    if-eq v0, v7, :cond_5

    if-eqz v0, :cond_5

    if-eq v0, v2, :cond_5

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "MovementManager"

    const-string v1, "Ignoring non-obvious activity when screen is on."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v3

    if-eq v3, v8, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    const/16 v3, 0x32

    if-ge v0, v3, :cond_8

    :cond_6
    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_9

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lhzo;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;I)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object p1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inferred activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_2
    invoke-direct {p0, p1}, Lhzo;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-object v0, p0, Lhzo;->f:Lhyt;

    invoke-virtual {v0}, Lhyt;->e()V

    invoke-virtual {p0}, Lhzo;->e()V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v1, 0x19

    if-ge v0, v1, :cond_a

    if-lt v0, v9, :cond_b

    iget-object v0, p0, Lhzo;->a:Lhzp;

    sget-object v1, Lhzp;->e:Lhzp;

    if-ne v0, v1, :cond_b

    :cond_a
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lhzo;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;I)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object p1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inferred activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_b
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v0

    const/16 v1, 0x19

    if-ge v0, v1, :cond_c

    if-lt v0, v9, :cond_7

    iget-object v0, p0, Lhzo;->a:Lhzp;

    sget-object v1, Lhzp;->d:Lhzp;

    if-ne v0, v1, :cond_7

    :cond_c
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lhzo;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;I)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object p1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inferred activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method final declared-synchronized a(Lhye;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lhye;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lhye;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    iget-boolean v2, v0, Lhyd;->a:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p1, Lhye;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihh;

    invoke-static {v0}, Lhxz;->a(Lihh;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lhzo;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lhzo;->b(J)Lhzp;

    move-result-object v0

    iput-object v0, p0, Lhzo;->a:Lhzp;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhzo;->b(J)Lhzp;

    move-result-object v0

    iput-object v0, p0, Lhzo;->a:Lhzp;

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " activity results. Current movement: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhzo;->a:Lhzp;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    iget-object v1, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    if-nez v1, :cond_4

    const-string v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v1, 0x5b

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lhyd;->b:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, v0, Lhyd;->c:Z

    if-eqz v1, :cond_5

    iget-wide v0, v0, Lhyd;->d:J

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :goto_3
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    :cond_5
    const-string v0, "now"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_6
    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Loaded periods:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    monitor-exit p0

    return-void
.end method

.method public final a(Z)V
    .locals 5

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MovementManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setActivityDetectionEnabled: enabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz p1, :cond_3

    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lhyd;->c:Z

    if-nez v1, :cond_2

    const-string v1, "MovementManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last period not closed. Start="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Lhyd;->b:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    goto :goto_0

    :cond_2
    new-instance v0, Lhyd;

    invoke-direct {v0}, Lhyd;-><init>()V

    iget-object v1, p0, Lhzo;->e:Lbpe;

    invoke-interface {v1}, Lbpe;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhyd;->a(J)Lhyd;

    iget-object v1, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhzo;->c:Ljava/util/LinkedList;

    invoke-direct {p0, v0}, Lhzo;->a(Ljava/util/LinkedList;)V

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    iget-boolean v1, v0, Lhyd;->c:Z

    if-eqz v1, :cond_5

    :cond_4
    const-string v1, "MovementManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last period already closed. Start="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Lhyd;->b:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v0, Lhyd;->d:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lhzo;->e:Lbpe;

    invoke-interface {v1}, Lbpe;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhyd;->b(J)Lhyd;

    goto :goto_1
.end method

.method final declared-synchronized b()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lhzo;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()Lhzp;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhzo;->b(J)Lhzp;

    move-result-object v0

    iput-object v0, p0, Lhzo;->a:Lhzp;

    iget-object v0, p0, Lhzo;->a:Lhzp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhzo;->a(J)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    const-wide/32 v2, 0xb28720

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhzo;->e:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhzo;->b(J)Lhzp;

    move-result-object v0

    iget-object v1, p0, Lhzo;->a:Lhzp;

    if-eq v0, v1, :cond_1

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MovementManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mCurrentMovement="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhzo;->a:Lhzp;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " newMovement="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhzo;->f:Lhyt;

    iget-object v2, p0, Lhzo;->a:Lhzp;

    invoke-virtual {v1, v2, v0}, Lhyt;->a(Lhzp;Lhzp;)V

    iput-object v0, p0, Lhzo;->a:Lhzp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
