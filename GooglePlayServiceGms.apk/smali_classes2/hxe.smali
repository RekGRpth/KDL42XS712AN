.class public abstract Lhxe;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field g:Z

.field public h:Z

.field i:Ljava/util/Collection;

.field j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lhxe;->g:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->h:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhxe;->a:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhxe;->i:Ljava/util/Collection;

    iput-boolean v2, p0, Lhxe;->j:Z

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 3

    const-string v0, "requested="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lhxe;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", enabled="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lhxe;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", request elapsed realtime="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhxe;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", clients="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhxe;->i:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lhxe;->i:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhxe;->i:Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lhxe;->i:Ljava/util/Collection;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lhxe;->i:Ljava/util/Collection;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->j:Z

    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lhxe;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->j:Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lhxe;->g:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lhxe;->g:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lhxe;->a:J

    iput-boolean v2, p0, Lhxe;->j:Z

    :cond_0
    iget-boolean v0, p0, Lhxe;->j:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhxe;->j:Z

    invoke-virtual {p0}, Lhxe;->a()V

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhxe;->g:Z

    invoke-virtual {p0}, Lhxe;->a()V

    :cond_0
    return-void
.end method
