.class public final Lgmt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/os/Parcel;I)V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/Set;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c()I

    move-result v2

    invoke-static {p1, v4, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v3, v2, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v5, v2, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v6, v2, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v7, v2, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_4
    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_5
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_6
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_7
    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_8
    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_9
    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_a
    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_b
    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_c
    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_d
    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_e
    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_f
    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_10
    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    move-result-object v3

    invoke-static {p1, v2, v3, p2, v4}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_11
    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_12
    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v3

    invoke-static {p1, v2, v3, p2, v4}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_13
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_14
    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_15
    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_16
    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x18

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_17
    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x1b

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    :cond_18
    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x1a

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_19
    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x1d

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1a
    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v2, 0x1c

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1b
    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/16 v2, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1c
    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const/16 v2, 0x1e

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1d
    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    const/16 v2, 0x22

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J()Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v2, v3, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_1e
    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    const/16 v2, 0x20

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v3

    invoke-static {p1, v2, v3, p2, v4}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    :cond_1f
    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/16 v1, 0x21

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    :cond_20
    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 39

    invoke-static/range {p1 .. p1}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v37

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v37

    if-ge v2, v0, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const v38, 0xffff

    and-int v38, v38, v2

    packed-switch v38, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v4

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    sget-object v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Abouts;->CREATOR:Lgmu;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v5}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    sget-object v6, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->CREATOR:Lgmv;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v6}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_5
    sget-object v8, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->CREATOR:Lgmw;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v8}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_6
    sget-object v9, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$BraggingRights;->CREATOR:Lgmx;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v9}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_7
    sget-object v10, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->CREATOR:Lgmy;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v10

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_8
    sget-object v11, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CustomFields;->CREATOR:Lgmz;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v11}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v11

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_9
    sget-object v12, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->CREATOR:Lgna;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v12}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v12

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_a
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v13

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_b
    sget-object v14, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Events;->CREATOR:Lgnb;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v14}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v14

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_c
    sget-object v15, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;->CREATOR:Lgnc;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v15}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v15

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_d
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v16

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_e
    sget-object v17, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->CREATOR:Lgnd;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v17

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_f
    sget-object v18, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->CREATOR:Lgne;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v18

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_10
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v19

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_11
    sget-object v21, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lgmt;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v21

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_12
    sget-object v20, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;->CREATOR:Lgnf;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v2, v1}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    const/16 v20, 0x12

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v20, v2

    goto/16 :goto_0

    :pswitch_13
    sget-object v23, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->CREATOR:Lgnh;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v23

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_14
    sget-object v22, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->CREATOR:Lgng;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v2, v1}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    const/16 v22, 0x14

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v22, v2

    goto/16 :goto_0

    :pswitch_15
    sget-object v25, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Occupations;->CREATOR:Lgnj;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v25

    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_16
    sget-object v24, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Nicknames;->CREATOR:Lgni;

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v24

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_17
    sget-object v27, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->CREATOR:Lgnl;

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v27

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_18
    sget-object v26, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Organizations;->CREATOR:Lgnk;

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v26

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_19
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v29

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1a
    sget-object v28, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PlacesLived;->CREATOR:Lgnm;

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v28

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1b
    sget-object v31, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipInterests;->CREATOR:Lgno;

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v31

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1c
    sget-object v30, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Relations;->CREATOR:Lgnn;

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v30

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1d
    sget-object v33, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Skills;->CREATOR:Lgnq;

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v33

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1e
    sget-object v32, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipStatuses;->CREATOR:Lgnp;

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v32

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1f
    sget-object v36, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Urls;->CREATOR:Lgnt;

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v36

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_20
    sget-object v34, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;->CREATOR:Lgnr;

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v2, v1}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    const/16 v34, 0x20

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v34, v2

    goto/16 :goto_0

    :pswitch_21
    sget-object v35, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;->CREATOR:Lgns;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v2, v1}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v35

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v37

    if-eq v2, v0, :cond_1

    new-instance v2, Lbkq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Overread allowed size end="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-direct {v2, v3, v0}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-direct/range {v2 .. v36}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;-><init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;Ljava/util/List;Ljava/util/List;)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_12
        :pswitch_11
        :pswitch_14
        :pswitch_13
        :pswitch_16
        :pswitch_15
        :pswitch_18
        :pswitch_17
        :pswitch_1a
        :pswitch_19
        :pswitch_1c
        :pswitch_1b
        :pswitch_1e
        :pswitch_1d
        :pswitch_20
        :pswitch_21
        :pswitch_1f
    .end packed-switch
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    return-object v0
.end method
