.class public final Lfsl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgdq;

.field private final b:Lgeb;

.field private final c:Lgau;


# direct methods
.method public constructor <init>(Lgdq;Lgeb;Lgau;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfsl;->a:Lgdq;

    iput-object p2, p0, Lfsl;->b:Lgeb;

    iput-object p3, p0, Lfsl;->c:Lgau;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, ""

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9

    const/4 v5, 0x0

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;)Lfsd;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfrw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lfsl;->b:Lgeb;

    invoke-static {p3}, Lfsl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "native:android_app"

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lgeb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgea;

    move-result-object v0

    invoke-virtual {v0}, Lgea;->b()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdz;

    invoke-virtual {v0}, Lgdz;->b()Lgdy;

    move-result-object v0

    invoke-static {v0}, Lfsc;->a(Lgdy;)Z

    move-result v0

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3, v0}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;Z)Lgdl;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v1, Lgdm;

    invoke-direct {v1}, Lgdm;-><init>()V

    invoke-virtual {v1, p3}, Lgdm;->a(Ljava/lang/String;)Lgdm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgdm;->a(Z)Lgdm;

    move-result-object v0

    invoke-virtual {v0}, Lgdm;->a()Lgdl;

    move-result-object v4

    :cond_0
    sget-object v8, Lbbo;->a:Lbbo;

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0338    # com.google.android.gms.R.string.plus_one_self_removed

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lfsc;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lgdl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfsu;

    move-result-object v0

    iget-object v0, v0, Lfsu;->a:Landroid/os/Bundle;

    invoke-static {v8, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v0}, Lfsd;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lfsb;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PosAgent"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PosAgent"

    const-string v2, "Network error deleting +1."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/Pair;
    .locals 12

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfrw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v10

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    move v8, v1

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    move/from16 v0, p5

    if-eq v0, v2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1, p3}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;)Lfsd;

    move-result-object v1

    move-object v2, v1

    :goto_1
    if-nez v2, :cond_1

    :try_start_0
    iget-object v11, p0, Lfsl;->a:Lgdq;

    invoke-static {p3}, Lfsl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const-string v7, "native:android_app"

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v4, v9

    invoke-static/range {v1 .. v7}, Lgdq;->a(Lgdr;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v11, Lgdq;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    new-instance v2, Lfsd;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v1, v3, v4}, Lfsd;-><init>(Lgdl;J)V

    move-object v1, v2

    :goto_2
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2, p3, v1}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;Lfsd;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v5, v1, Lfsd;->a:Lgdl;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v3, v8

    move-object v4, p3

    move-object/from16 v8, p4

    invoke-static/range {v1 .. v8}, Lfsc;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lgdl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfsu;

    move-result-object v1

    iget-object v1, v1, Lfsu;->a:Landroid/os/Bundle;

    sget-object v2, Lbbo;->a:Lbbo;

    invoke-static {v2, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "https://www.googleapis.com/auth/pos"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lfsl;->c:Lgau;

    invoke-static {p1}, Lfrx;->a(Landroid/content/Context;)Lfrx;

    sget-object v3, Lbje;->e:[Ljava/lang/String;

    invoke-interface {v2, p1, v1, p2, v3}, Lgau;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lbbo;

    move-result-object v1

    invoke-virtual {v1}, Lbbo;->b()Z

    move-result v1

    move v8, v1

    goto/16 :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lfsd;

    iget-object v2, v2, Lfsd;->a:Lgdl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lfsd;-><init>(Lgdl;J)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2, p3}, Lfsb;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PosAgent"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PosAgent"

    invoke-virtual {v1}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v1

    :cond_3
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 19

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;)Lfsd;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfrw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :try_start_0
    new-instance v1, Lgdm;

    invoke-direct {v1}, Lgdm;-><init>()V

    invoke-static/range {p3 .. p3}, Lfsl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgdm;->a(Ljava/lang/String;)Lgdm;

    move-result-object v1

    move-object/from16 v0, p4

    iput-object v0, v1, Lgdm;->a:Ljava/lang/String;

    iget-object v2, v1, Lgdm;->b:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lgdm;->a(Z)Lgdm;

    move-result-object v1

    invoke-virtual {v1}, Lgdm;->a()Lgdl;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfsl;->a:Lgdq;

    move-object/from16 v18, v0

    invoke-static/range {p3 .. p3}, Lfsl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v15, "native:android_app"

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p4

    invoke-static/range {v1 .. v16}, Lgdq;->a(Lgdt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    iget-object v1, v0, Lgdq;->a:Lbmi;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object/from16 v2, p2

    move-object/from16 v5, v17

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lfsc;->a(Lgdl;)Z

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Lfsb;->a(Ljava/lang/String;Ljava/lang/String;Z)Lgdl;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    if-nez v5, :cond_0

    move-object v5, v1

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0339    # com.google.android.gms.R.string.plus_one_self

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b033a    # com.google.android.gms.R.string.plus_one_public

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v1, p1

    move-object/from16 v4, p3

    move-object/from16 v8, p5

    invoke-static/range {v1 .. v8}, Lfsc;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lgdl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfsu;

    move-result-object v1

    iget-object v1, v1, Lfsu;->a:Landroid/os/Bundle;

    sget-object v2, Lbbo;->a:Lbbo;

    invoke-static {v2, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :cond_1
    invoke-virtual {v1}, Lfsd;->a()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Lfsb;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PosAgent"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PosAgent"

    const-string v3, "Network error inserting +1."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lfsv;
    .locals 9

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfrw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfsl;->a:Lgdq;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "native:android_app"

    const/4 v4, 0x0

    invoke-static {v4, v0, v2, v3}, Lgdq;->a(Lgds;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lgdq;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/pos/GetsignupstateEntity;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/pos/GetsignupstateEntity;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "display_name"

    invoke-interface {v0}, Lgdi;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "signedUp"

    invoke-interface {v0}, Lgdi;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    new-instance v2, Lbki;

    invoke-interface {v0}, Lgdi;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbki;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0d016a    # com.google.android.gms.R.dimen.plus_one_avatar_size

    invoke-virtual {v2, p1, v0}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "profile_image_url"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "account_name"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const-string v0, "com.google.android.gms.plus"

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/provider/PlusProvider;->a(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lgag;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    :try_start_3
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PosAgent"

    const-string v2, "Hit data removed condition"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_0
    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    move-object v1, v6

    goto :goto_0

    :cond_1
    :try_start_5
    const-string v0, "display_name"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "profile_image_url"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "signedUp"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v7

    :goto_1
    new-instance v4, Lfsv;

    invoke-direct {v4, v2, v3, v0}, Lfsv;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_2
    return-object v4

    :cond_3
    move v0, v8

    goto :goto_1
.end method
