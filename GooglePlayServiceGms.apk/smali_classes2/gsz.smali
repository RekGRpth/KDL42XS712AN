.class public final enum Lgsz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgsz;

.field public static final enum b:Lgsz;

.field public static final enum c:Lgsz;

.field private static final synthetic d:[Lgsz;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lgsz;

    const-string v1, "INAPP"

    invoke-direct {v0, v1, v2}, Lgsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgsz;->a:Lgsz;

    new-instance v0, Lgsz;

    const-string v1, "DONATIONS"

    invoke-direct {v0, v1, v3}, Lgsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgsz;->b:Lgsz;

    new-instance v0, Lgsz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lgsz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgsz;->c:Lgsz;

    const/4 v0, 0x3

    new-array v0, v0, [Lgsz;

    sget-object v1, Lgsz;->a:Lgsz;

    aput-object v1, v0, v2

    sget-object v1, Lgsz;->b:Lgsz;

    aput-object v1, v0, v3

    sget-object v1, Lgsz;->c:Lgsz;

    aput-object v1, v0, v4

    sput-object v0, Lgsz;->d:[Lgsz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgsz;
    .locals 1

    const-class v0, Lgsz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgsz;

    return-object v0
.end method

.method public static values()[Lgsz;
    .locals 1

    sget-object v0, Lgsz;->d:[Lgsz;

    invoke-virtual {v0}, [Lgsz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgsz;

    return-object v0
.end method
