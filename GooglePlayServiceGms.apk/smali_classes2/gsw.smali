.class public final Lgsw;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 7

    const/4 v3, 0x3

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    if-eq v2, v3, :cond_0

    const-string v2, "JwtParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrong number of components in jwt, expected 3, got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x1

    aget-object v0, v0, v2

    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "typ"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const-string v2, "JwtTypeParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected 5 components in \"typ\" field: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_1
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Lgsy;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-static {v4}, Lgtc;->a(Ljava/lang/String;)Lgsz;

    move-result-object v4

    const/4 v5, 0x3

    aget-object v5, v2, v5

    invoke-static {v5}, Lgtc;->b(Ljava/lang/String;)Lgta;

    move-result-object v5

    const/4 v6, 0x4

    aget-object v2, v2, v6

    invoke-static {v2}, Lgtc;->c(Ljava/lang/String;)Lgtb;

    move-result-object v2

    invoke-direct {v0, v4, v5, v2}, Lgsy;-><init>(Lgsz;Lgta;Lgtb;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "JwtParser"

    const-string v2, "Invalid JSON in Jwt"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v2, Lgsx;->a:[I

    iget-object v4, v0, Lgsy;->a:Lgta;

    invoke-virtual {v4}, Lgta;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    if-nez p1, :cond_3

    move-object v0, v1

    goto :goto_0

    :pswitch_0
    const-string v2, "request"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2, v0}, Lgsw;->b(Lorg/json/JSONObject;Lgsy;)Liod;

    move-result-object v0

    new-instance v2, Liob;

    invoke-direct {v2}, Liob;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Liod;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iput-object v3, v2, Liob;->b:[Liod;

    invoke-static {v2}, Lgth;->a(Liob;)V

    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    const-string v3, ""

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Liob;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v2, "JwtParser"

    const-string v3, "Invalid numeric field in JWT item"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_1
    :try_start_2
    invoke-static {v3, v0}, Lgsw;->a(Lorg/json/JSONObject;Lgsy;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.google.android.libraries.inapp.EXTRA_DISPLAY_HINTS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Landroid/os/Bundle;

    if-eqz v2, :cond_4

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lgsr;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lgsr;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    if-eqz v0, :cond_6

    const-string v0, "JwtParser"

    const-string v2, "Display hints object is not a Bundle or Json String"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lorg/json/JSONObject;Lgsy;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 8

    const/4 v1, 0x0

    const-string v0, "request"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "items"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    new-instance v4, Liob;

    invoke-direct {v4}, Liob;-><init>()V

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "currencyCode"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    new-array v0, v6, [Liod;

    iput-object v0, v4, Liob;->b:[Liod;

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0, p1}, Lgsw;->b(Lorg/json/JSONObject;Lgsy;)Liod;

    move-result-object v7

    iget-object v0, v7, Liod;->c:Lioh;

    if-eqz v0, :cond_1

    iget-object v0, v7, Liod;->c:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Jwt items don\'t have consistent currency code!"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, v7, Liod;->c:Lioh;

    iget-object v0, v0, Lioh;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, v4, Liob;->b:[Liod;

    aput-object v7, v0, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    invoke-static {v4}, Lgth;->a(Liob;)V

    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    const-string v1, ""

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Liob;)V

    goto :goto_0
.end method

.method private static b(Lorg/json/JSONObject;Lgsy;)Liod;
    .locals 8

    const/4 v4, 0x1

    const/4 v1, 0x0

    sget-object v0, Lgsx;->a:[I

    iget-object v2, p1, Lgsy;->a:Lgta;

    invoke-virtual {v2}, Lgta;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :goto_0
    sget-object v2, Lgsx;->a:[I

    iget-object v3, p1, Lgsy;->a:Lgta;

    invoke-virtual {v3}, Lgta;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    move-object v2, v1

    :goto_1
    const-string v3, "quantity"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    :goto_2
    new-instance v5, Liod;

    invoke-direct {v5}, Liod;-><init>()V

    iput-object v2, v5, Liod;->b:Ljava/lang/String;

    iput-object v0, v5, Liod;->a:Ljava/lang/String;

    iput v3, v5, Liod;->e:I

    const-string v0, "currencyCode"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "price"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v0, v2}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;)Lioh;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-object v1, v5, Liod;->c:Lioh;

    :cond_0
    if-le v3, v4, :cond_1

    const-string v2, "net_cost"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0, v2}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;)Lioh;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, v5, Liod;->d:Lioh;

    :cond_1
    :goto_3
    return-object v5

    :pswitch_2
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const-string v0, "unsignedName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const-string v2, "description"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_5
    const-string v2, "unsignedDescription"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_1

    new-instance v2, Lioh;

    invoke-direct {v2}, Lioh;-><init>()V

    iput-object v2, v5, Liod;->d:Lioh;

    iget-object v2, v5, Liod;->d:Lioh;

    iput-object v0, v2, Lioh;->b:Ljava/lang/String;

    iget-object v0, v5, Liod;->d:Lioh;

    int-to-long v2, v3

    iget-wide v6, v1, Lioh;->a:J

    mul-long v1, v2, v6

    iput-wide v1, v0, Lioh;->a:J

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
