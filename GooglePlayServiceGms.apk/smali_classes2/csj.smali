.class public final Lcsj;
.super Lcta;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/feedback/ErrorReport;

.field private b:Landroid/app/Service;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 0

    invoke-direct {p0}, Lcta;-><init>()V

    iput-object p1, p0, Lcsj;->b:Landroid/app/Service;

    return-void
.end method

.method private a()Z
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v1, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p0, Lcsj;->b:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    if-nez v3, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v3, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    aget-object v4, v1, v0

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    const/16 v4, 0xb

    iput v4, v3, Landroid/app/ApplicationErrorReport;->type:I

    iget-object v3, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    aget-object v0, v1, v0

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    invoke-direct {p0}, Lcsj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-class v0, Landroid/view/Surface;

    const-string v1, "screenshot"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    move-result-object v0

    iget-object v1, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcsj;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    iput-object v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    :cond_1
    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    array-length v1, v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    array-length v2, v2

    invoke-static {v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;

    move-result-object v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/FeedbackSession;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/FeedbackSession$Screenshot;)V

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcsj;->a:Lcom/google/android/gms/feedback/ErrorReport;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcsj;->b:Landroid/app/Service;

    const-class v3, Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ErrorReport;->a()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcsj;->b:Landroid/app/Service;

    invoke-virtual {v0, v1}, Landroid/app/Service;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcsj;->b:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    const/4 v0, 0x1

    goto :goto_0
.end method
