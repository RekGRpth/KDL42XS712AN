.class Libd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Libw;


# static fields
.field static final a:[[I

.field private static final b:Ljava/util/logging/Logger;

.field private static final c:Libw;

.field private static final f:Ljava/util/Comparator;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field private final d:Ljava/util/List;

.field private final e:Libi;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0x78

    const-class v0, Libd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Libd;->b:Ljava/util/logging/Logger;

    filled-new-array {v5, v5}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    sput-object v0, Libd;->a:[[I

    new-instance v0, Liby;

    invoke-direct {v0}, Liby;-><init>()V

    sput-object v0, Libd;->c:Libw;

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_0

    sget-object v3, Libd;->a:[[I

    aget-object v3, v3, v2

    invoke-static {v2, v0}, Libd;->a(II)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    new-instance v0, Libe;

    invoke-direct {v0}, Libe;-><init>()V

    sput-object v0, Libd;->f:Ljava/util/Comparator;

    new-instance v0, Libf;

    invoke-direct {v0}, Libf;-><init>()V

    sput-object v0, Libd;->g:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Libi;

    invoke-direct {v0}, Libi;-><init>()V

    iput-object v0, p0, Libd;->e:Libi;

    iput-object p1, p0, Libd;->d:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lhuq;)D
    .locals 2

    invoke-static {p0}, Libd;->b(Lhuq;)D

    move-result-wide v0

    return-wide v0
.end method

.method static a(II)I
    .locals 6

    const-wide/high16 v4, 0x400e000000000000L    # 3.75

    int-to-double v0, p0

    mul-double/2addr v0, v4

    int-to-double v2, p1

    mul-double/2addr v2, v4

    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;Lhug;Libi;)Lhue;
    .locals 10

    const/4 v5, 0x0

    const-wide/16 v2, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    iget-object v8, v0, Lhuq;->b:Lhus;

    sget-object v9, Lhus;->d:Lhus;

    if-ne v8, v9, :cond_1

    new-instance v8, Lhue;

    invoke-direct {v8, v1, v0}, Lhue;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Libd;->b(Lhuq;)D

    goto :goto_0

    :cond_1
    sget-object v9, Lhus;->c:Lhus;

    if-ne v8, v9, :cond_0

    new-instance v8, Lhue;

    invoke-direct {v8, v1, v0}, Lhue;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Libd;->b:Ljava/util/logging/Logger;

    const-string v1, "No lre nor minK results found. Returning matrixCenter"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    new-instance v0, Lhue;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lhue;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object v0

    :cond_3
    sget-object v0, Libd;->g:Ljava/util/Comparator;

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    iget-object v1, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Lhuq;

    invoke-static {v1}, Libd;->b(Lhuq;)D

    move-result-wide v8

    add-double/2addr v2, v8

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0, p3, p4}, Libd;->a(Lhuq;ILhug;Libi;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    const-wide/high16 v7, 0x4020000000000000L    # 8.0

    cmpg-double v1, v2, v7

    if-gez v1, :cond_5

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    const/4 v4, 0x1

    iget-object v1, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Lhuq;

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0, p3, p4}, Libd;->a(Lhuq;ILhug;Libi;)V

    move v0, v4

    goto :goto_3

    :cond_5
    invoke-virtual {p4, p3}, Libi;->a(Lhug;)Lhui;

    move-result-object v1

    if-nez v1, :cond_6

    sget-object v0, Libd;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as unable to find dominant circle."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    move-object v0, v5

    goto :goto_1

    :cond_6
    invoke-static {v1, v6}, Libd;->a(Lhui;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v0, :cond_7

    iget v0, v1, Lhui;->c:I

    int-to-double v6, v0

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v6, v8

    double-to-int v0, v6

    iput v0, v1, Lhui;->c:I

    :cond_7
    invoke-virtual {v1}, Lhui;->a()Lhug;

    move-result-object v0

    invoke-static {v0, p1}, Libd;->a(Lhug;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v0, Libd;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as no APs within 75 meters of location."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    move-object v0, v5

    goto/16 :goto_1

    :cond_8
    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private static a(Ljava/util/Map;)Lhug;
    .locals 14

    const-wide v10, 0x416312d000000000L    # 1.0E7

    const-wide v8, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const-wide/16 v0, 0x0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v3, v0

    move-wide v12, v0

    move-wide v1, v12

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    iget v6, v0, Lhuq;->d:I

    int-to-double v6, v6

    mul-double/2addr v6, v8

    add-double/2addr v3, v6

    iget v0, v0, Lhuq;->e:I

    int-to-double v6, v0

    mul-double/2addr v6, v8

    add-double v0, v1, v6

    move-wide v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double/2addr v3, v5

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double v0, v1, v5

    new-instance v2, Lhug;

    mul-double/2addr v3, v10

    double-to-int v3, v3

    mul-double/2addr v0, v10

    double-to-int v0, v0

    const v1, 0x124f8

    invoke-direct {v2, v3, v0, v1}, Lhug;-><init>(III)V

    return-object v2
.end method

.method private static a(Ljava/util/List;)Libg;
    .locals 4

    new-instance v1, Libg;

    invoke-direct {v1}, Libg;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libg;

    iget-object v3, v1, Libg;->a:Ljava/util/List;

    iget-object v0, v0, Libg;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a()Libx;
    .locals 4

    new-instance v0, Libx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Libx;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Libx;-><init>(Lhug;ILjava/util/Set;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private a(Lhuq;ILhug;Libi;)V
    .locals 14

    invoke-static {p1}, Liba;->b(Lhug;)D

    move-result-wide v1

    invoke-static/range {p3 .. p3}, Liba;->b(Lhug;)D

    move-result-wide v3

    sub-double/2addr v1, v3

    invoke-static {p1}, Liba;->a(Lhug;)D

    move-result-wide v3

    invoke-static/range {p3 .. p3}, Liba;->a(Lhug;)D

    move-result-wide v5

    add-double/2addr v3, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v1, v3

    const-wide v3, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x400e000000000000L    # 3.75

    div-double/2addr v1, v3

    double-to-int v10, v1

    invoke-static {p1}, Liba;->a(Lhug;)D

    move-result-wide v1

    invoke-static/range {p3 .. p3}, Liba;->a(Lhug;)D

    move-result-wide v3

    sub-double/2addr v1, v3

    const-wide v3, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x400e000000000000L    # 3.75

    div-double/2addr v1, v3

    double-to-int v11, v1

    new-instance v1, Liaz;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    move/from16 v0, p2

    invoke-direct {v1, v0, v2, v3, v4}, Liaz;-><init>(III[F)V

    iget-object v2, p0, Libd;->d:Ljava/util/List;

    sget-object v3, Libd;->f:Ljava/util/Comparator;

    invoke-static {v2, v1, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    if-gez v1, :cond_4

    const/4 v1, 0x0

    move-object v9, v1

    :goto_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz v9, :cond_0

    iget-object v4, v9, Liaz;->d:[F

    array-length v4, v4

    if-lez v4, :cond_0

    iget-object v1, v9, Liaz;->d:[F

    array-length v3, v1

    iget v2, v9, Liaz;->c:I

    const/4 v1, 0x0

    :cond_0
    const/16 v4, -0x28

    move v8, v4

    :goto_1
    const/16 v4, 0x28

    if-gt v8, v4, :cond_c

    sub-int v4, v8, v10

    if-gez v4, :cond_1

    neg-int v4, v4

    :cond_1
    const/16 v5, -0x28

    move v7, v5

    :goto_2
    const/16 v5, 0x28

    if-gt v7, v5, :cond_b

    sub-int v5, v7, v11

    if-gez v5, :cond_2

    neg-int v5, v5

    :cond_2
    const/16 v6, 0x78

    if-ge v5, v6, :cond_3

    const/16 v6, 0x78

    if-lt v4, v6, :cond_6

    :cond_3
    invoke-static {v4, v5}, Libd;->a(II)I

    move-result v5

    :goto_3
    if-eqz v1, :cond_8

    int-to-double v5, v5

    const-wide v12, 0x4052c00000000000L    # 75.0

    cmpg-double v5, v5, v12

    if-gez v5, :cond_7

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    :goto_4
    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v7, v5, v6}, Libi;->a(IID)V

    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_2

    :cond_4
    iget-object v2, p0, Libd;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liaz;

    iget v2, v1, Liaz;->b:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_5

    const/4 v1, 0x0

    move-object v9, v1

    goto :goto_0

    :cond_5
    move-object v9, v1

    goto :goto_0

    :cond_6
    sget-object v6, Libd;->a:[[I

    aget-object v6, v6, v4

    aget v5, v6, v5

    goto :goto_3

    :cond_7
    const-wide v5, 0x3fa999999999999aL    # 0.05

    goto :goto_4

    :cond_8
    if-ge v5, v2, :cond_9

    iget-object v5, v9, Liaz;->d:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-double v5, v5

    goto :goto_4

    :cond_9
    add-int v6, v2, v3

    if-lt v5, v6, :cond_a

    iget-object v5, v9, Liaz;->d:[F

    add-int/lit8 v6, v3, -0x1

    aget v5, v5, v6

    float-to-double v5, v5

    goto :goto_4

    :cond_a
    iget-object v6, v9, Liaz;->d:[F

    sub-int/2addr v5, v2

    aget v5, v6, v5

    float-to-double v5, v5

    goto :goto_4

    :cond_b
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    :cond_c
    return-void
.end method

.method private static a(Lhug;Ljava/util/Map;)Z
    .locals 6

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    invoke-static {p0, v0}, Liba;->b(Lhug;Lhug;)D

    move-result-wide v2

    const-wide v4, 0x4052c00000000000L    # 75.0

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lhui;Ljava/util/List;)Z
    .locals 13

    const-wide v11, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const-wide/16 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v8, v0

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lhuq;

    iget v0, v6, Lhuq;->a:I

    const v1, 0x9c40

    if-ge v0, v1, :cond_2

    iget v0, p0, Lhui;->a:I

    int-to-double v0, v0

    mul-double/2addr v0, v11

    iget v2, p0, Lhui;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v11

    iget v4, v6, Lhug;->d:I

    int-to-double v4, v4

    mul-double/2addr v4, v11

    iget v6, v6, Lhug;->e:I

    int-to-double v6, v6

    mul-double/2addr v6, v11

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v0

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    :goto_1
    move-wide v8, v0

    goto :goto_0

    :cond_0
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v8

    const-wide v2, 0x3ff3333333333333L    # 1.2

    div-double/2addr v0, v2

    double-to-int v0, v0

    iget v1, p0, Lhui;->c:I

    if-le v0, v1, :cond_1

    iput v0, p0, Lhui;->c:I

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-wide v0, v8

    goto :goto_1
.end method

.method private static a(Lhuq;Libg;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Libg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Lhuq;

    invoke-static {p0, v0}, Liba;->a(Lhug;Lhug;)I

    move-result v0

    const/16 v4, 0xc8

    if-gt v0, v4, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private static b(Lhuq;)D
    .locals 4

    iget v0, p0, Lhuq;->a:I

    int-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double v0, v2, v0

    return-wide v0
.end method

.method private b(Ljava/util/Map;)Ljava/util/Set;
    .locals 9

    const/4 v8, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    new-instance v2, Libg;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v1, v7}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v1

    invoke-direct {v2, v1}, Libg;-><init>(Lhue;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Libg;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhuq;

    invoke-static {v2, v1}, Libd;->a(Lhuq;Libg;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-static {v5}, Libd;->a(Ljava/util/List;)Libg;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Ljava/util/List;->clear()V

    invoke-interface {v3}, Ljava/util/List;->clear()V

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    new-instance v0, Libh;

    invoke-direct {v0, p0, v8}, Libh;-><init>(Libd;B)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libg;

    iget-object v4, v0, Libg;->a:Ljava/util/List;

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libg;

    iget-object v0, v0, Libg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/util/Map;Ljava/util/Map;)Libx;
    .locals 8

    sget-object v0, Libd;->c:Libw;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Libw;->a(Ljava/util/Map;Ljava/util/Map;)Libx;

    move-result-object v0

    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {v0}, Libx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, p1}, Libd;->b(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-static {}, Libd;->a()Libx;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v3}, Libd;->a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Libd;->e:Libi;

    invoke-virtual {v1}, Libi;->a()V

    invoke-static {v0}, Libd;->a(Ljava/util/Map;)Lhug;

    move-result-object v1

    iget-object v2, p0, Libd;->e:Libi;

    invoke-direct {p0, v0, p2, v1, v2}, Libd;->a(Ljava/util/Map;Ljava/util/Map;Lhug;Libi;)Lhue;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Libd;->a()Libx;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Lhug;

    if-nez v0, :cond_2

    sget-object v0, Libd;->b:Ljava/util/logging/Logger;

    const-string v1, "No location found by lre localizer"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-static {}, Libd;->a()Libx;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const/16 v1, 0x50

    const-wide v6, 0x3fd3333333333333L    # 0.3

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_3

    const/16 v1, 0x4e

    :cond_3
    new-instance v2, Libx;

    invoke-direct {v2, v0, v1, v3}, Libx;-><init>(Lhug;ILjava/util/Set;)V

    move-object v0, v2

    goto :goto_0
.end method
