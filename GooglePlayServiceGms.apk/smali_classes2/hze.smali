.class final Lhze;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method private constructor <init>(Lhyt;)V
    .locals 0

    iput-object p1, p0, Lhze;->a:Lhyt;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhyt;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhze;-><init>(Lhyt;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhze;->a:Lhyt;

    invoke-static {v1}, Lhyt;->a(Lhyt;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lhze;->a:Lhyt;

    invoke-virtual {v0, p2}, Lhyt;->b(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v0, p0, Lhze;->a:Lhyt;

    invoke-static {v0}, Lhyt;->b(Lhyt;)V

    goto :goto_0

    :cond_3
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lhze;->a:Lhyt;

    invoke-static {v0, p2}, Lhyt;->a(Lhyt;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhze;->a:Lhyt;

    invoke-static {v0, p2}, Lhyt;->b(Lhyt;Landroid/content/Intent;)V

    goto :goto_0
.end method
