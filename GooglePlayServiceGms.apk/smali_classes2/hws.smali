.class public abstract Lhws;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final c:Lhxf;

.field final d:Lhuz;

.field e:Lhwm;

.field f:Lhww;


# direct methods
.method public constructor <init>(Lhxf;Lhuz;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lhwm;->a:Lhwm;

    iput-object v0, p0, Lhws;->e:Lhwm;

    new-instance v0, Lhwu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhwu;-><init>(Lhws;B)V

    iput-object v0, p0, Lhws;->f:Lhww;

    iput-object p1, p0, Lhws;->c:Lhxf;

    iput-object p2, p0, Lhws;->d:Lhuz;

    return-void
.end method

.method static synthetic a(Lhws;)Z
    .locals 6

    const-wide/16 v4, 0x4e20

    iget-object v0, p0, Lhws;->c:Lhxf;

    iget-boolean v0, v0, Lhxe;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhws;->e:Lhwm;

    iget v0, v0, Lhwm;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lhws;->e:Lhwm;

    iget-wide v0, v0, Lhwm;->b:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lhws;->e:Lhwm;

    iget-wide v0, v0, Lhwm;->c:J

    iget-object v2, p0, Lhws;->e:Lhwm;

    iget-wide v2, v2, Lhwm;->b:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lhws;->f:Lhww;

    invoke-virtual {v0, p1}, Lhww;->a(Landroid/location/Location;)V

    return-void
.end method

.method public final a(Lhwm;Z)V
    .locals 1

    iput-object p1, p0, Lhws;->e:Lhwm;

    iget-object v0, p0, Lhws;->f:Lhww;

    invoke-virtual {v0, p2}, Lhww;->a(Z)V

    return-void
.end method

.method public abstract a(Z)V
.end method
