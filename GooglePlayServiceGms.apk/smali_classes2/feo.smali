.class public final Lfeo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfeo;->d:J

    iput-object p1, p0, Lfeo;->a:Lcom/google/android/gms/people/profile/AvatarView;

    return-void
.end method

.method public static synthetic a(Lfeo;)Z
    .locals 1

    iget-boolean v0, p0, Lfeo;->e:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfeo;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfeo;->f:Z

    return-void
.end method

.method public final a(FF)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lfeo;->e:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lfeo;->d:J

    iput p1, p0, Lfeo;->b:F

    iput p2, p0, Lfeo;->c:F

    iput-boolean v0, p0, Lfeo;->f:Z

    iput-boolean v1, p0, Lfeo;->e:Z

    iget-object v0, p0, Lfeo;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    goto :goto_0
.end method

.method public final run()V
    .locals 9

    const/high16 v8, 0x447a0000    # 1000.0f

    const/4 v1, 0x0

    iget-boolean v0, p0, Lfeo;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lfeo;->d:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    iget-wide v4, p0, Lfeo;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    div-float/2addr v0, v8

    :goto_1
    iget-object v4, p0, Lfeo;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v5, p0, Lfeo;->b:F

    mul-float/2addr v5, v0

    iget v6, p0, Lfeo;->c:F

    mul-float/2addr v6, v0

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FF)Z

    move-result v4

    iput-wide v2, p0, Lfeo;->d:J

    mul-float/2addr v0, v8

    iget v2, p0, Lfeo;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_8

    iget v2, p0, Lfeo;->b:F

    sub-float/2addr v2, v0

    iput v2, p0, Lfeo;->b:F

    iget v2, p0, Lfeo;->b:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2

    iput v1, p0, Lfeo;->b:F

    :cond_2
    :goto_2
    iget v2, p0, Lfeo;->c:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_9

    iget v2, p0, Lfeo;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lfeo;->c:F

    iget v0, p0, Lfeo;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    iput v1, p0, Lfeo;->c:F

    :cond_3
    :goto_3
    iget v0, p0, Lfeo;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lfeo;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    :cond_4
    if-nez v4, :cond_6

    :cond_5
    invoke-virtual {p0}, Lfeo;->a()V

    iget-object v0, p0, Lfeo;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-static {v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;)V

    :cond_6
    iget-boolean v0, p0, Lfeo;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfeo;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    iget v2, p0, Lfeo;->b:F

    add-float/2addr v2, v0

    iput v2, p0, Lfeo;->b:F

    iget v2, p0, Lfeo;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    iput v1, p0, Lfeo;->b:F

    goto :goto_2

    :cond_9
    iget v2, p0, Lfeo;->c:F

    add-float/2addr v0, v2

    iput v0, p0, Lfeo;->c:F

    iget v0, p0, Lfeo;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iput v1, p0, Lfeo;->c:F

    goto :goto_3
.end method
