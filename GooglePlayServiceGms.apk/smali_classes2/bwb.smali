.class public final Lbwb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcll;

.field private final b:Lbvr;

.field private final c:Lbvp;


# direct methods
.method public constructor <init>(Lcll;Lcfz;Lcon;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcll;

    iput-object v0, p0, Lbwb;->a:Lcll;

    new-instance v0, Lbvr;

    invoke-direct {v0, p2, p3}, Lbvr;-><init>(Lcfz;Lcon;)V

    iput-object v0, p0, Lbwb;->b:Lbvr;

    new-instance v0, Lbvp;

    invoke-direct {v0, p2, p1}, Lbvp;-><init>(Lcfz;Lcll;)V

    iput-object v0, p0, Lbwb;->c:Lbvp;

    return-void
.end method

.method public constructor <init>(Lcoy;)V
    .locals 3

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v0

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v1

    invoke-virtual {p1}, Lcoy;->a()Lcon;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lbwb;-><init>(Lcll;Lcfz;Lcon;)V

    return-void
.end method


# virtual methods
.method public final a(Lbsp;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v4, 0x7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/drive.appdata"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lbwb;->c:Lbvp;

    invoke-virtual {v0, p1}, Lbvp;->a(Lbsp;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :try_start_1
    iget-object v0, p1, Lbsp;->a:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    invoke-virtual {v0}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v2, Lbth;->a:Lbth;

    invoke-static {v2}, Lbti;->a(Lbth;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v3, Lbqr;->b:Lbqr;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p1, Lbsp;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lbwb;->a:Lcll;

    invoke-interface {v2, v0, p2, v1}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lclk;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :try_start_2
    iget-object v1, p0, Lbwb;->b:Lbvr;

    iget-object v2, p1, Lbsp;->a:Lcfc;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lbvr;->a(Lcfc;Lclk;Ljava/lang/Boolean;Z)V
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-interface {v0}, Lclk;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lbrm;

    const-string v1, "Failed to retrieve item from network."

    invoke-direct {v0, v4, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "SingleItemSynchronizer"

    const-string v2, "Remote request failed"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbrm;

    const-string v1, "Failed to retrieve item from network."

    invoke-direct {v0, v4, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_2
    move-exception v0

    const-string v1, "SingleItemSynchronizer"

    const-string v2, "Processing remote result failed"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to process item."

    invoke-direct {v0, v1, v2, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
.end method
