.class public final Lcmb;
.super Lclu;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/drive/DriveId;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final e:J

.field private final f:Lcom/google/android/gms/drive/DriveId;

.field private g:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;)V
    .locals 9

    sget-object v8, Lcms;->a:Lcms;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcmb;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcms;)V

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcms;)V
    .locals 1

    sget-object v0, Lcmr;->g:Lcmr;

    invoke-direct {p0, v0, p1, p2, p8}, Lclu;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    iput-object p3, p0, Lcmb;->c:Ljava/lang/String;

    iput-object p4, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-wide p5, p0, Lcmb;->e:J

    iput-object p7, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->e:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclu;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "metadata"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcjc;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const-string v0, "contentId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcmb;->c:Ljava/lang/String;

    const-string v0, "writeOpenKey"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmb;->e:J

    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    :goto_0
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    :cond_0
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcmb;->g:Ljava/lang/Long;

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmb;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;Lbsp;)Lcml;
    .locals 7

    iget-object v0, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcle;->b:Lcje;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcle;->c:Lcje;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lclu;->b:Lcfc;

    invoke-interface {p1}, Lcfz;->s()Lcgf;

    move-result-object v3

    invoke-interface {p1, v2, v0, v1, v3}, Lcfz;->a(Lcfc;Ljava/lang/String;Ljava/lang/String;Lcgf;)Lcfp;

    move-result-object v6

    iget-object v0, p0, Lcmb;->c:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Content does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-wide v0, v1, Lcfx;->h:J

    invoke-virtual {v6, v0, v1}, Lcfp;->a(J)V

    iget-object v0, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v6, v0}, Lcjc;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v6}, Lcfp;->k()V

    iget-object v1, p0, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v6}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    iget-object v3, p2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v4, p0, Lcmb;->e:J

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcfz;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcge;

    move-result-object v1

    iget-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lclu;->b:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v2, p0, Lclu;->b:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    iget-object v2, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p1, v6, v2}, Lcfz;->a(Lcfp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfv;

    move-result-object v2

    invoke-virtual {v2}, Lcfv;->k()V

    invoke-virtual {v0}, Lcfp;->w()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcfp;->c(Ljava/lang/Long;)V

    iget-object v0, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcle;->j:Lcja;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v2, Ljava/util/HashSet;

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcle;->j:Lcja;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_1
    iget-wide v2, p2, Lbsp;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v6, v0}, Lcfz;->a(Lcfp;Ljava/util/Set;)V

    iget-object v0, p0, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcfp;->s(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcfp;->k()V

    invoke-virtual {v6}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    iget-wide v0, v1, Lcfl;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcmb;->g:Ljava/lang/Long;

    new-instance v0, Lcna;

    iget-object v1, p0, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p2, Lbsp;->a:Lcfc;

    iget-object v4, p2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0}, Lcmb;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcna;-><init>(JLcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    iget-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcoy;)V
    .locals 7

    invoke-virtual {p2}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-super {p0, v0}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v1

    invoke-virtual {p0}, Lcmb;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    invoke-virtual {p2}, Lcoy;->f()Lcfz;

    move-result-object v1

    invoke-super {p0, v1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v6

    iget-object v1, p0, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const/4 v5, 0x1

    move-object v1, p2

    invoke-static/range {v0 .. v6}, Lbty;->a(Lcfp;Lcoy;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLbsp;)Lbty;

    move-result-object v1

    new-instance v2, Lbtv;

    invoke-direct {v2, p2}, Lbtv;-><init>(Lcoy;)V

    :try_start_0
    new-instance v3, Lbtu;

    invoke-direct {v3}, Lbtu;-><init>()V

    invoke-virtual {v2, v1, v3}, Lbtv;->a(Lbty;Lbtw;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/File;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcfp;->n(Ljava/lang/String;)V

    invoke-static {v1}, Lcbk;->a(Lcom/google/android/gms/drive/internal/model/File;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfp;->q(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcfp;->r(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfp;->k()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbua; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbub; {:try_start_0 .. :try_end_0} :catch_2

    invoke-virtual {p2}, Lcoy;->f()Lcfz;

    move-result-object v0

    iget-object v1, p0, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcfz;->b(J)Lcge;

    move-result-object v0

    invoke-virtual {v0}, Lcge;->l()V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final bridge synthetic b(Lcfz;)Lbsp;
    .locals 1

    invoke-super {p0, p1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclu;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "contentId"

    iget-object v2, p0, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "metadata"

    iget-object v2, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcjc;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "writeOpenKey"

    iget-wide v2, p0, Lcmb;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v1, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    const-string v1, "parent"

    iget-object v2, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    iget-object v1, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_1

    const-string v1, "newDriveId"

    iget-object v2, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    iget-object v1, p0, Lcmb;->g:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, "pendingUploadSqlId"

    iget-object v2, p0, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcmb;

    iget-object v2, p0, Lcmb;->c:Ljava/lang/String;

    iget-object v3, p1, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_6

    :cond_8
    iget-object v2, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_9

    :cond_b
    iget-object v2, p0, Lcmb;->g:Ljava/lang/Long;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcmb;->g:Ljava/lang/Long;

    iget-object v3, p1, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcmb;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcmb;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcmb;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcmb;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CreateFileOp[%s, pendingUploadSqlId=%d, initialMetadata=%s, parent=%s, newDriveId=%s, contentId=%s]"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcmb;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcmb;->g:Ljava/lang/Long;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcmb;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcmb;->f:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcmb;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
