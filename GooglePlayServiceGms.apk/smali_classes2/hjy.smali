.class public final Lhjy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lick;
.implements Lils;


# instance fields
.field final a:Lidu;

.field final b:Lhof;

.field final c:Lhoj;

.field final d:Lhnq;

.field final e:Liha;

.field final f:Lhjx;

.field final g:Lhin;

.field final h:Lhkl;

.field final i:Lhoc;

.field final j:Lhkd;

.field final k:Lhjf;

.field final l:Lhki;

.field final m:Lhkr;

.field public final n:Lhlu;

.field final o:Lhte;


# direct methods
.method public constructor <init>(Lidu;Lids;Liha;Lhoc;Lhod;Z)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lhjy;->a:Lidu;

    new-instance v2, Lhof;

    invoke-interface/range {p1 .. p1}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-direct {v2, v0, v3, v4, v1}, Lhof;-><init>(Lids;JLhod;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->b:Lhof;

    new-instance v2, Lhoj;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lhoj;-><init>(Lidu;Lids;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->c:Lhoj;

    new-instance v2, Lhnq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjy;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjy;->c:Lhoj;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v3, v4, v1}, Lhnq;-><init>(Lidu;Lhof;Lhoj;Lids;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->d:Lhnq;

    invoke-static/range {p1 .. p1}, Lhkd;->a(Lidu;)Lhkd;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->j:Lhkd;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lhjy;->i:Lhoc;

    new-instance v12, Libt;

    invoke-interface/range {p1 .. p1}, Lidu;->j()Licm;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-direct {v12, v0, v1, v2}, Libt;-><init>(Lidu;Lhoc;Licm;)V

    new-instance v7, Lhtu;

    invoke-direct {v7}, Lhtu;-><init>()V

    new-instance v2, Lhjf;

    invoke-interface/range {p1 .. p1}, Lidu;->h()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lidu;->i()[B

    move-result-object v4

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lhjf;-><init>(Lidq;Ljavax/crypto/SecretKey;[B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->k:Lhjf;

    new-instance v2, Lhkl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjy;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjy;->c:Lhoj;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjy;->k:Lhjf;

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Lhkl;-><init>(Lidu;Lhof;Lhoj;Lhjf;Lhtu;Liha;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->h:Lhkl;

    new-instance v2, Lhte;

    invoke-direct {v2}, Lhte;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->o:Lhte;

    new-instance v8, Lhjx;

    move-object/from16 v0, p0

    iget-object v10, v0, Lhjy;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v11, v0, Lhjy;->c:Lhoj;

    move-object/from16 v0, p0

    iget-object v13, v0, Lhjy;->k:Lhjf;

    move-object/from16 v0, p0

    iget-object v15, v0, Lhjy;->h:Lhkl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lhjy;->o:Lhte;

    move-object/from16 v16, v0

    move-object/from16 v9, p1

    move-object v14, v7

    invoke-direct/range {v8 .. v16}, Lhjx;-><init>(Lidu;Lhof;Lhoj;Libt;Lhjf;Lhtu;Lhkl;Lhte;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lhjy;->f:Lhjx;

    new-instance v2, Lhkr;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lhkr;-><init>(Lidu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->m:Lhkr;

    new-instance v2, Lhlu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjy;->m:Lhkr;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3}, Lhlu;-><init>(Lidu;Lhkr;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->n:Lhlu;

    if-eqz p6, :cond_0

    new-instance v2, Lhjw;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjy;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjy;->c:Lhoj;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjy;->k:Lhjf;

    move-object/from16 v0, p0

    iget-object v7, v0, Lhjy;->h:Lhkl;

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjy;->j:Lhkd;

    move-object/from16 v0, p0

    iget-object v10, v0, Lhjy;->o:Lhte;

    move-object/from16 v0, p0

    iget-object v11, v0, Lhjy;->m:Lhkr;

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-direct/range {v2 .. v11}, Lhjw;-><init>(Lidu;Lhof;Lhoj;Lhjf;Lhkl;Lhkd;Liha;Lhte;Lhkr;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->g:Lhin;

    :goto_0
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lhjy;->e:Liha;

    new-instance v2, Lhki;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjy;->k:Lhjf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjy;->j:Lhkd;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lhki;-><init>(Lidu;Lhjf;Lhkd;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->l:Lhki;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhjy;->j:Lhkd;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lhkd;->a(Lils;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lidu;->a(Lick;)V

    return-void

    :cond_0
    new-instance v2, Lhjz;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjy;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjy;->c:Lhoj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjy;->h:Lhkl;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4, v5}, Lhjz;-><init>(Lidu;Lhof;Lhoj;Lhkl;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhjy;->g:Lhin;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v2, 0x1

    iget-object v0, p0, Lhjy;->b:Lhof;

    iget-object v1, p0, Lhjy;->a:Lidu;

    invoke-virtual {v0, v1}, Lhof;->a(Lidu;)V

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, v2}, Lhkd;->c(Z)V

    iget-object v0, p0, Lhjy;->c:Lhoj;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhjy;->c:Lhoj;

    :try_start_0
    iget-object v0, v1, Lhoj;->g:Lidx;

    invoke-virtual {v0}, Lidx;->a()Livi;

    move-result-object v0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "SeenDevicesCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Actual file version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lhoj;->g:Lidx;

    invoke-virtual {v4}, Lidx;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v0}, Lhoj;->b(Livi;)V

    invoke-virtual {v1}, Lhoj;->c()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "SeenDevicesCache"

    const-string v2, "Loaded %d entries, last refresh: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lhoj;->b:Lhok;

    invoke-virtual {v5}, Lhok;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v5, v1, Lhoj;->e:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lhjy;->d:Lhnq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhjy;->d:Lhnq;

    invoke-virtual {v0}, Lhnq;->a()V

    :cond_2
    iget-object v0, p0, Lhjy;->h:Lhkl;

    iget-object v1, p0, Lhjy;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhkl;->a(J)V

    iget-object v0, p0, Lhjy;->i:Lhoc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhjy;->i:Lhoc;

    iget-object v1, v0, Lhoc;->a:Lhnz;

    invoke-virtual {v1}, Lhnz;->a()V

    iget-object v1, v0, Lhoc;->b:Lhnt;

    invoke-virtual {v1}, Lhnt;->a()V

    iget-object v0, v0, Lhoc;->c:Lhnt;

    invoke-virtual {v0}, Lhnt;->a()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ModelState"

    const-string v1, "Done loading ModelState from disk"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lhjy;->k:Lhjf;

    iget-object v2, v1, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, v1, Lhjf;->b:Lidx;

    invoke-virtual {v0}, Lidx;->a()Livi;

    move-result-object v0

    iput-object v0, v1, Lhjf;->c:Livi;

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "CollectorState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Actual file version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lhjf;->b:Lidx;

    invoke-virtual {v4}, Lidx;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1}, Lhjf;->h()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    :goto_1
    :try_start_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_6

    const-string v0, "CollectorState"

    const-string v3, "Loaded: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lhjf;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lhoj;->b()V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "SeenDevicesCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_5

    const-string v3, "CollectorState"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(I)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0, p1}, Lhkr;->a(I)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0}, Lhjx;->a()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lhjy;->d:Lhnq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjy;->d:Lhnq;

    invoke-virtual {v0, p1}, Lhnq;->a(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lhjy;->l:Lhki;

    const/4 v1, 0x7

    if-ne p1, v1, :cond_0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "SensorUploader"

    const-string v2, "alarmRing"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lhki;->b:J

    invoke-virtual {v0}, Lhki;->a()V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lhjy;->n:Lhlu;

    invoke-virtual {v0}, Lhlu;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public final a(IIIZLilx;)V
    .locals 6

    iget-object v0, p0, Lhjy;->f:Lhjx;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lhjx;->a(IIIZLilx;)V

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0, p1}, Lhkr;->c(I)V

    iget-object v0, p0, Lhjy;->d:Lhnq;

    invoke-virtual {v0, p5}, Lhnq;->a(Lilx;)V

    return-void
.end method

.method public final a(IIZ)V
    .locals 1

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p1, p2, p3}, Lhkd;->a(IIZ)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1, p2, p3}, Lhin;->a(IIZ)V

    return-void
.end method

.method public final a(IIZZLilx;)V
    .locals 1

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0, p1, p2, p3, p5}, Lhkr;->a(IIZLilx;)V

    iget-object v0, p0, Lhjy;->n:Lhlu;

    invoke-virtual {v0, p4}, Lhlu;->a(Z)V

    return-void
.end method

.method public final a(IJ)V
    .locals 1

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0, p1, p2, p3}, Lhkr;->a(IJ)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-object v0, p0, Lhjy;->n:Lhlu;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lhlu;->d:Lhlz;

    invoke-virtual {v0, p1}, Lhlz;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    :cond_0
    return-void
.end method

.method public final a(Lhlt;)V
    .locals 1

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0, p1}, Lhkr;->a(Lhlt;)V

    return-void
.end method

.method public final a(Lhtf;)V
    .locals 3

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p1}, Lhkd;->a(Lhtf;)V

    iget-object v0, p0, Lhjy;->o:Lhte;

    iget-object v1, p0, Lhjy;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lhte;->a(Lhtf;J)V

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->a(Lhtf;)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Lhtf;)V

    return-void
.end method

.method public final a(Lhud;)V
    .locals 2

    iget-object v0, p1, Lhud;->b:Lhuu;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhud;->b:Lhuu;

    iget-object v0, v0, Lhuu;->a:Lhuv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjy;->g:Lhin;

    iget-object v1, p1, Lhud;->b:Lhuu;

    iget-object v1, v1, Lhuu;->a:Lhuv;

    invoke-virtual {v0, v1}, Lhin;->a(Lhuv;)V

    :cond_0
    return-void
.end method

.method public final a(Lidr;)V
    .locals 2

    invoke-interface {p1}, Lidr;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lidr;->d()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkProvider"

    const-string v1, "ignoring GPS location lacking satellites for 2d fix"

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->a(Lidr;)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Lidr;)V

    goto :goto_0
.end method

.method public final a(Lids;)V
    .locals 5

    iget-object v0, p0, Lhjy;->e:Liha;

    iget-object v1, v0, Liha;->f:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    iget-object v3, v0, Liha;->b:Lihd;

    if-eqz v3, :cond_0

    iget-object v3, v0, Liha;->b:Lihd;

    invoke-virtual {p1}, Lids;->q()Lidt;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, Lihd;->a(Lidt;J)V

    :cond_0
    iget-object v3, v0, Liha;->c:Lihd;

    if-eqz v3, :cond_1

    iget-object v3, v0, Liha;->c:Lihd;

    invoke-virtual {p1}, Lids;->r()Lidt;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, Lihd;->a(Lidt;J)V

    :cond_1
    iget-object v3, v0, Liha;->d:Lihd;

    if-eqz v3, :cond_2

    iget-object v3, v0, Liha;->d:Lihd;

    invoke-virtual {p1}, Lids;->s()Lidt;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, Lihd;->a(Lidt;J)V

    :cond_2
    iget-object v3, v0, Liha;->e:Lihd;

    if-eqz v3, :cond_3

    iget-object v0, v0, Liha;->e:Lihd;

    invoke-virtual {p1}, Lids;->t()Lidt;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lihd;->a(Lidt;J)V

    :cond_3
    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p1}, Lhkd;->a(Lids;)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Lids;)V

    return-void
.end method

.method public final a(Lidw;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1, p2}, Lhin;->a(Lidw;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Livi;)V
    .locals 1

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->a(Livi;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0}, Lhin;->f()V

    iget-object v0, p0, Lhjy;->l:Lhki;

    invoke-virtual {v0}, Lhki;->a()V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lhjy;->j:Lhkd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhkd;->c(Z)V

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p0}, Lhkd;->b(Lils;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhjy;->b:Lhof;

    iget-object v1, p0, Lhjy;->f:Lhjx;

    iget-object v1, v1, Lhjx;->a:Lidu;

    invoke-virtual {v0, v1}, Lhof;->b(Lidu;)V

    iget-object v0, p0, Lhjy;->i:Lhoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjy;->i:Lhoc;

    invoke-virtual {v0}, Lhoc;->a()V

    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p1, p2}, Lhkd;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->b(Z)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Z)V

    iget-object v0, p0, Lhjy;->l:Lhki;

    iput-boolean p2, v0, Lhki;->c:Z

    invoke-virtual {v0}, Lhki;->a()V

    return-void
.end method

.method public final a(ZZI)V
    .locals 3

    iget-object v0, p0, Lhjy;->e:Liha;

    iput-boolean p2, v0, Liha;->g:Z

    iget-object v0, p0, Lhjy;->l:Lhki;

    iput-boolean p1, v0, Lhki;->d:Z

    iget-object v1, v0, Lhki;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    iput-wide v1, v0, Lhki;->e:J

    invoke-virtual {v0}, Lhki;->a()V

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1, p2, p3}, Lhjx;->a(ZZI)V

    return-void
.end method

.method public final a([Lhuv;)V
    .locals 13

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_9

    aget-object v3, p1, v0

    iget-object v1, v3, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    iget-wide v5, v3, Lhuv;->a:J

    const/4 v1, 0x0

    move v12, v1

    move v1, v2

    move v2, v12

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, Lhuv;->a(I)Lhut;

    move-result-object v7

    sget-boolean v8, Licj;->b:Z

    if-eqz v8, :cond_0

    const-string v8, "NetworkProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "scanTimestamp is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and device timestamp is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v7, Lhti;->a:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and diff is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v7, Lhti;->a:J

    sub-long v10, v5, v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-wide v7, v7, Lhti;->a:J

    sub-long v7, v5, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v7

    const-wide/32 v9, 0xea60

    cmp-long v7, v7, v9

    if-ltz v7, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-lez v1, :cond_8

    if-ne v1, v4, :cond_4

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_3

    const-string v2, "NetworkProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Dropping stale wifi scan: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v2, 0x0

    aput-object v2, p1, v0

    :cond_4
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_5

    const-string v2, "NetworkProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Dropping "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stale devices from wifi scan: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    sub-int v1, v4, v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-wide v5, v3, Lhuv;->a:J

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_7

    invoke-virtual {v3, v1}, Lhuv;->a(I)Lhut;

    move-result-object v7

    iget-wide v8, v7, Lhti;->a:J

    sub-long v8, v5, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    cmp-long v8, v8, v10

    if-gez v8, :cond_6

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    new-instance v1, Lhuv;

    iget-wide v3, v3, Lhuv;->a:J

    invoke-direct {v1, v3, v4, v2}, Lhuv;-><init>(JLjava/util/ArrayList;)V

    aput-object v1, p1, v0

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->a([Lhuv;)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lhin;->b(Lhuv;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0}, Lhkr;->d()V

    return-void
.end method

.method public final b(Livi;)V
    .locals 1

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Livi;)V

    return-void
.end method

.method public final b(Z)V
    .locals 3

    iget-object v0, p0, Lhjy;->o:Lhte;

    iget-object v1, p0, Lhjy;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lhte;->a(ZJ)V

    iget-object v0, p0, Lhjy;->j:Lhkd;

    invoke-virtual {v0, p1}, Lhkd;->b(Z)V

    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->b(Z)V

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0}, Lhkr;->c()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhjy;->m:Lhkr;

    invoke-virtual {v0}, Lhkr;->e()V

    return-void
.end method

.method public final c(Livi;)V
    .locals 1

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->b(Livi;)V

    return-void
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lhjy;->f:Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->a(Z)V

    return-void
.end method

.method public final d(Livi;)V
    .locals 1

    iget-object v0, p0, Lhjy;->d:Lhnq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjy;->d:Lhnq;

    invoke-virtual {v0, p1}, Lhnq;->a(Livi;)V

    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Full collection mode changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhjy;->g:Lhin;

    invoke-virtual {v0, p1}, Lhin;->c(Z)V

    return-void
.end method
