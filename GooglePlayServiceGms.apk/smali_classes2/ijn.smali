.class public final Lijn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/location/reporting/service/ReportingConfig;
    .locals 8

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {p0}, Lbkp;->a(Landroid/os/Parcel;)I

    move-result v6

    move-object v4, v5

    move-object v3, v5

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v6, :cond_0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v7, 0xffff

    and-int/2addr v7, v0

    packed-switch v7, :pswitch_data_0

    invoke-static {p0, v0}, Lbkp;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v0}, Lbkp;->g(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :pswitch_1
    invoke-static {p0, v0}, Lbkp;->d(Landroid/os/Parcel;I)Z

    move-result v2

    goto :goto_0

    :pswitch_2
    sget-object v3, Lcom/google/android/location/reporting/service/AccountConfig;->CREATOR:Liij;

    invoke-static {p0, v0, v3}, Lbkp;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0

    :pswitch_3
    sget-object v4, Lcom/google/android/location/reporting/service/Conditions;->CREATOR:Liiq;

    invoke-static {p0, v0, v4}, Lbkp;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/Conditions;

    move-object v4, v0

    goto :goto_0

    :pswitch_4
    invoke-static {p0, v0}, Lbkp;->m(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v6, :cond_1

    new-instance v0, Lbkq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbkq;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/location/reporting/service/ReportingConfig;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/reporting/service/ReportingConfig;-><init>(IZLjava/util/List;Lcom/google/android/location/reporting/service/Conditions;Ljava/lang/String;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lcom/google/android/location/reporting/service/ReportingConfig;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbkr;->a(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->e()I

    move-result v2

    invoke-static {p1, v1, v2}, Lbkr;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->a()Z

    move-result v2

    invoke-static {p1, v1, v2}, Lbkr;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->c()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lbkr;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lbkr;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v0}, Lbkr;->b(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lijn;->a(Landroid/os/Parcel;)Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/location/reporting/service/ReportingConfig;

    return-object v0
.end method
