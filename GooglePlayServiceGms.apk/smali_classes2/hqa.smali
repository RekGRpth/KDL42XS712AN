.class final Lhqa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhqq;


# instance fields
.field final a:Lhqf;

.field final synthetic b:Lhpt;

.field private final c:Lhpy;

.field private volatile d:Z

.field private volatile e:Ljava/util/List;


# direct methods
.method private constructor <init>(Lhpt;Lhpy;Lhqf;)V
    .locals 1

    iput-object p1, p0, Lhqa;->b:Lhpt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhqa;->d:Z

    invoke-static {}, Lhsn;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhqa;->e:Ljava/util/List;

    iput-object p2, p0, Lhqa;->c:Lhpy;

    iput-object p3, p0, Lhqa;->a:Lhqf;

    return-void
.end method

.method synthetic constructor <init>(Lhpt;Lhpy;Lhqf;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lhqa;-><init>(Lhpt;Lhpy;Lhqf;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqa;->c:Lhpy;

    invoke-virtual {v0, p2}, Lhpy;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lhqa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqa;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lhqa;->c:Lhpy;

    invoke-virtual {v0}, Lhpy;->b()Lhsm;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhqa;->b:Lhpt;

    invoke-static {v1}, Lhpt;->b(Lhpt;)Limb;

    move-result-object v1

    const-string v2, "Finished all."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhqa;->d:Z

    iget-object v1, p0, Lhqa;->b:Lhpt;

    invoke-static {v1}, Lhpt;->h(Lhpt;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v1, p0, Lhqa;->a:Lhqf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhqa;->b:Lhpt;

    invoke-static {v1}, Lhpt;->e(Lhpt;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lhqe;

    invoke-direct {v2, p0, p1, v0}, Lhqe;-><init>(Lhqa;Ljava/lang/String;Lhsm;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lhsd;)V
    .locals 0

    return-void
.end method

.method public final a(Livi;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;ILivi;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqa;->c:Lhpy;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lhpy;->a(IZ)V

    iget-object v0, p0, Lhqa;->a:Lhqf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqa;->b:Lhpt;

    invoke-static {v0}, Lhpt;->e(Lhpt;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lhqc;

    invoke-direct {v1, p0, p2, p3}, Lhqc;-><init>(Lhqa;ILivi;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lhqa;->b:Lhpt;

    invoke-static {v0}, Lhpt;->g(Lhpt;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhqa;->c:Lhpy;

    invoke-virtual {v0, p1}, Lhpy;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lhqa;->b:Lhpt;

    invoke-static {v0, p1}, Lhpt;->b(Lhpt;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhqa;->b:Lhpt;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhpt;->a(Lhpt;Z)Z

    :cond_1
    invoke-direct {p0, p1, p2}, Lhqa;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqa;->c:Lhpy;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lhpy;->a(IZ)V

    iget-object v0, p0, Lhqa;->a:Lhqf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqa;->b:Lhpt;

    invoke-static {v0}, Lhpt;->e(Lhpt;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lhqb;

    invoke-direct {v1, p0, p2, p3}, Lhqb;-><init>(Lhqa;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0, p1, p2}, Lhqa;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqa;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhqa;->c:Lhpy;

    iget-object v0, v0, Lhpy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lhqa;->c:Lhpy;

    invoke-virtual {v1, p2}, Lhpy;->a(Ljava/lang/String;)Lhsm;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lhqa;->c:Lhpy;

    invoke-virtual {v2, p2}, Lhpy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhqa;->b:Lhpt;

    invoke-static {v3}, Lhpt;->b(Lhpt;)Limb;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Finished uploading GLocRequests in file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lhqa;->e:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lhqa;->a:Lhqf;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhqa;->b:Lhpt;

    invoke-static {v3}, Lhpt;->e(Lhpt;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lhqd;

    invoke-direct {v4, p0, v0, v2, v1}, Lhqd;-><init>(Lhqa;ILjava/lang/String;Lhsm;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    invoke-direct {p0, p1}, Lhqa;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(ZZ)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final e_(I)V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized i()V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method
