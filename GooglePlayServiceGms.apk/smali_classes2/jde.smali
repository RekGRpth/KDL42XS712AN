.class public final Ljde;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Lizf;

.field b:Lizf;

.field private c:Z

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljde;->a:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljde;->b:Lizf;

    const/4 v0, -0x1

    iput v0, p0, Ljde;->e:I

    return-void
.end method

.method public static a([B)Ljde;
    .locals 2

    new-instance v0, Ljde;

    invoke-direct {v0}, Ljde;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Ljde;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljde;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljde;->b()I

    :cond_0
    iget v0, p0, Ljde;->e:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljde;->a(Lizf;)Ljde;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljde;->b(Lizf;)Ljde;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizf;)Ljde;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljde;->c:Z

    iput-object p1, p0, Ljde;->a:Lizf;

    return-object p0
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljde;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljde;->a:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_0
    iget-boolean v0, p0, Ljde;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljde;->b:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljde;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljde;->a:Lizf;

    invoke-static {v0, v1}, Lizh;->b(ILizf;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljde;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljde;->b:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Ljde;->e:I

    return v0
.end method

.method public final b(Lizf;)Ljde;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljde;->d:Z

    iput-object p1, p0, Ljde;->b:Lizf;

    return-object p0
.end method
