.class public final Ljaf;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljag;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Ljaf;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljaf;->b:Ljava/lang/String;

    invoke-static {}, Ljag;->c()[Ljag;

    move-result-object v0

    iput-object v0, p0, Ljaf;->c:[Ljag;

    const/4 v0, -0x1

    iput v0, p0, Ljaf;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ljaf;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljaf;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljaf;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljaf;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljaf;->c:[Ljag;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljaf;->c:[Ljag;

    array-length v1, v1

    if-lez v1, :cond_4

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljaf;->c:[Ljag;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljaf;->c:[Ljag;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x4

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    :cond_4
    iput v0, p0, Ljaf;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaf;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljaf;->c:[Ljag;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljag;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljaf;->c:[Ljag;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljag;

    invoke-direct {v3}, Ljag;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljaf;->c:[Ljag;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljag;

    invoke-direct {v3}, Ljag;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Ljaf;->c:[Ljag;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Ljaf;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljaf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ljaf;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljaf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljaf;->c:[Ljag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljaf;->c:[Ljag;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljaf;->c:[Ljag;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ljaf;->c:[Ljag;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljaf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljaf;

    iget-object v2, p0, Ljaf;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljaf;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljaf;->a:Ljava/lang/String;

    iget-object v3, p1, Ljaf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljaf;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Ljaf;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljaf;->b:Ljava/lang/String;

    iget-object v3, p1, Ljaf;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljaf;->c:[Ljag;

    iget-object v3, p1, Ljaf;->c:[Ljag;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljaf;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljaf;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ljaf;->c:[Ljag;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljaf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljaf;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
