.class public final enum Lhuc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhuc;

.field public static final enum b:Lhuc;

.field private static final synthetic c:[Lhuc;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhuc;

    const-string v1, "LEVEL_SELECTOR"

    invoke-direct {v0, v1, v2}, Lhuc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhuc;->a:Lhuc;

    new-instance v0, Lhuc;

    const-string v1, "LEVEL"

    invoke-direct {v0, v1, v3}, Lhuc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhuc;->b:Lhuc;

    const/4 v0, 0x2

    new-array v0, v0, [Lhuc;

    sget-object v1, Lhuc;->a:Lhuc;

    aput-object v1, v0, v2

    sget-object v1, Lhuc;->b:Lhuc;

    aput-object v1, v0, v3

    sput-object v0, Lhuc;->c:[Lhuc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhuc;
    .locals 1

    const-class v0, Lhuc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhuc;

    return-object v0
.end method

.method public static values()[Lhuc;
    .locals 1

    sget-object v0, Lhuc;->c:[Lhuc;

    invoke-virtual {v0}, [Lhuc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhuc;

    return-object v0
.end method
