.class public final Lhbe;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;Landroid/content/Context;[Ljai;)V
    .locals 1

    iput-object p1, p0, Lhbe;->a:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    const v0, 0x7f04014b    # com.google.android.gms.R.layout.wallet_row_loyalty_spinner

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lhbe;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final a(Ljai;)I
    .locals 5

    const/4 v1, -0x1

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lhbe;->getCount()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lhbe;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljai;

    iget-object v0, v0, Ljai;->a:Ljava/lang/String;

    iget-object v4, p1, Ljai;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lhbe;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04014c    # com.google.android.gms.R.layout.wallet_row_loyalty_spinner_dropdown

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lhbe;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljai;

    const v1, 0x7f0a0353    # com.google.android.gms.R.id.account_identifier

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Ljai;->i:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Ljai;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v1, 0x7f0a032c    # com.google.android.gms.R.id.program_name

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Ljai;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, v0, Ljai;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final synthetic getPosition(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljai;

    invoke-virtual {p0, p1}, Lhbe;->a(Ljai;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lhbe;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04014b    # com.google.android.gms.R.layout.wallet_row_loyalty_spinner

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v0, 0x7f0a0352    # com.google.android.gms.R.id.wob_label

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lhbe;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljai;

    const-string v2, "dont_send_loyalty_wob_id"

    iget-object v3, v1, Ljai;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Ljai;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Ljai;->i:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Ljai;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v1, v1, Ljai;->i:Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhbe;->a:Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->isEnabled()Z

    move-result v0

    invoke-static {p2, v0}, Lbqc;->a(Landroid/view/View;Z)V

    return-object p2

    :cond_1
    iget-object v1, v1, Ljai;->d:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v1, v1, Ljai;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
