.class public final Lcan;
.super Lcag;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/text/Collator;

.field private f:Landroid/util/Pair;

.field private final g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcoy;)V
    .locals 2

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001b    # com.google.android.gms.R.string.drive_alphabet_set

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcan;-><init>(Lcoy;Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method private constructor <init>(Lcoy;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 2

    sget-object v0, Lceg;->a:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcah;->a:Lcah;

    invoke-direct {p0, v0, v1}, Lcag;-><init>(Ljava/lang/String;Lcah;)V

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcan;->g:Landroid/content/Context;

    iput-object p2, p0, Lcan;->d:Ljava/lang/String;

    invoke-static {p3}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcan;->e:Ljava/text/Collator;

    iget-object v0, p0, Lcan;->e:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    return-void
.end method

.method private static a(Landroid/database/Cursor;)Lcao;
    .locals 2

    sget-object v0, Lceg;->l:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcao;->a:Lcao;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcao;->b:Lcao;

    goto :goto_0
.end method

.method private g()Landroid/util/Pair;
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcan;->f:Landroid/util/Pair;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcan;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v0, v1

    move v3, v1

    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v3, v2, :cond_0

    invoke-virtual {v4, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/lit8 v2, v0, 0x1

    add-int v7, v3, v6

    invoke-virtual {v4, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    add-int v0, v3, v6

    move v3, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcan;->e:Ljava/text/Collator;

    invoke-static {v5, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lbzv;

    iget-object v3, p0, Lcan;->g:Landroid/content/Context;

    const v4, 0x7f0b004a    # com.google.android.gms.R.string.drive_fast_scroll_title_grouper_collections

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const-string v6, ""

    invoke-direct {v0, v3, v4, v6}, Lbzv;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v3, v5

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v5, v0

    new-instance v6, Lbzv;

    invoke-direct {v6, v4, v1, v4}, Lbzv;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-array v0, v1, [Lbzv;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbzv;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcan;->f:Landroid/util/Pair;

    :cond_2
    iget-object v0, p0, Lcan;->f:Landroid/util/Pair;

    return-object v0
.end method


# virtual methods
.method public final a()Lcar;
    .locals 7

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcan;->a(Landroid/database/Cursor;)Lcao;

    move-result-object v0

    iget-object v5, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    :goto_0
    iget-object v5, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v1, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-static {v1}, Lcan;->a(Landroid/database/Cursor;)Lcao;

    move-result-object v1

    iget-object v6, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToPrevious()Z

    :goto_1
    iget-object v6, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-static {v6}, Lcan;->a(Landroid/database/Cursor;)Lcao;

    move-result-object v6

    if-eqz v4, :cond_0

    if-eq v6, v0, :cond_4

    :cond_0
    move v4, v3

    :goto_2
    if-eqz v5, :cond_1

    if-eq v6, v1, :cond_5

    :cond_1
    move v0, v3

    :goto_3
    invoke-virtual {v6, v4, v0}, Lcao;->a(ZZ)Lcar;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v0, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcan;->c:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    goto :goto_1

    :cond_4
    move v4, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method public final b()Landroid/widget/SectionIndexer;
    .locals 5

    new-instance v1, Lbzt;

    iget-object v2, p0, Lcan;->c:Landroid/database/Cursor;

    iget-object v3, p0, Lcan;->e:Ljava/text/Collator;

    iget v4, p0, Lcag;->b:I

    invoke-direct {p0}, Lcan;->g()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Lbzv;

    invoke-direct {v1, v2, v3, v4, v0}, Lbzt;-><init>(Landroid/database/Cursor;Ljava/text/Collator;I[Lbzv;)V

    return-object v1
.end method

.method protected final e()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lceg;->l:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1}, Lcdp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <> \"application/vnd.google-apps.folder\", upper(trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcan;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")) COLLATE LOCALIZED, trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcan;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
