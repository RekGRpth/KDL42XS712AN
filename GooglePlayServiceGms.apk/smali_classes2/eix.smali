.class public final Leix;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Leiw;

.field private final f:Leiw;

.field private g:Landroid/content/ContentResolver;

.field private final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "doc_score"

    aput-object v1, v0, v5

    sput-object v0, Leix;->a:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "created_timestamp"

    aput-object v1, v0, v2

    sput-object v0, Leix;->b:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "tag"

    aput-object v1, v0, v5

    sput-object v0, Leix;->c:[Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Leix;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;[Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    iput-object v0, p0, Leix;->e:Leiw;

    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    iput-object v0, p0, Leix;->f:Leiw;

    iput-object p1, p0, Leix;->g:Landroid/content/ContentResolver;

    sget-object v0, Leix;->a:[Ljava/lang/String;

    array-length v0, v0

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Leix;->h:[Ljava/lang/String;

    sget-object v0, Leix;->a:[Ljava/lang/String;

    iget-object v1, p0, Leix;->h:[Ljava/lang/String;

    sget-object v2, Leix;->a:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Leix;->h:[Ljava/lang/String;

    sget-object v1, Leix;->a:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p2

    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public final a()Leiw;
    .locals 1

    iget-object v0, p0, Leix;->e:Leiw;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;J)V
    .locals 9

    iget-object v0, p0, Leix;->e:Leiw;

    invoke-virtual {v0}, Leiw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leix;->e:Leiw;

    iget-object v1, p0, Leix;->g:Landroid/content/ContentResolver;

    const-string v3, "documents"

    iget-object v4, p0, Leix;->h:[Ljava/lang/String;

    sget-object v5, Leix;->b:[Ljava/lang/String;

    const-string v8, "20"

    move-object v2, p1

    move-wide v6, p2

    invoke-virtual/range {v0 .. v8}, Leiw;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Leix;->f:Leiw;

    invoke-virtual {v0}, Leiw;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leix;->f:Leiw;

    iget-object v1, p0, Leix;->g:Landroid/content/ContentResolver;

    const-string v3, "tags"

    sget-object v4, Leix;->c:[Ljava/lang/String;

    sget-object v5, Leix;->d:[Ljava/lang/String;

    const-string v8, "100"

    move-object v2, p1

    move-wide v6, p2

    invoke-virtual/range {v0 .. v8}, Leiw;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final b()Leiw;
    .locals 1

    iget-object v0, p0, Leix;->f:Leiw;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Leix;->e:Leiw;

    iget-boolean v0, v0, Leiw;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leix;->f:Leiw;

    iget-boolean v0, v0, Leiw;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Leix;->e:Leiw;

    invoke-virtual {v0}, Leiw;->d()V

    iget-object v0, p0, Leix;->f:Leiw;

    invoke-virtual {v0}, Leiw;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Leix;->g:Landroid/content/ContentResolver;

    return-void
.end method

.method protected final finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Leix;->g:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    const-string v0, "Content cursor disposed without a closing"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Leix;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
