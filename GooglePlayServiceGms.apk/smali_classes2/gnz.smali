.class public final Lgnz;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;Ljava/lang/String;Landroid/widget/ImageView;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    iput-object p1, p0, Lgnz;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lgnz;->b:Ljava/lang/String;

    iput-object p3, p0, Lgnz;->c:Landroid/widget/ImageView;

    iput-object p4, p0, Lgnz;->d:Landroid/os/ParcelFileDescriptor;

    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgnz;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lfba;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lgnz;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgnz;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lgnz;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lgnz;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lbkt;

    move-result-object v0

    iget-object v1, p0, Lgnz;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lbkt;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgnz;->b:Ljava/lang/String;

    iget-object v1, p0, Lgnz;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgnz;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
