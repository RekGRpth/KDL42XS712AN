.class public final Ligc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final b:Lhuf;


# instance fields
.field private final c:Lhou;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x7

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ligc;->a:J

    new-instance v0, Ligd;

    invoke-direct {v0}, Ligd;-><init>()V

    sput-object v0, Ligc;->b:Lhuf;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhou;

    const/16 v1, 0x3e8

    sget-object v2, Lhuk;->a:Lhuf;

    sget-object v3, Ligc;->b:Lhuf;

    invoke-direct {v0, v1, v2, v3}, Lhou;-><init>(ILhuf;Lhuf;)V

    iput-object v0, p0, Ligc;->c:Lhou;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)Liga;
    .locals 1

    iget-object v0, p0, Ligc;->c:Lhou;

    invoke-virtual {v0, p1, p2, p3}, Lhou;->a(Ljava/lang/Object;J)Lhno;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lhno;->d:Ljava/lang/Object;

    check-cast v0, Liga;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Liga;J)V
    .locals 7

    iget-object v0, p0, Ligc;->c:Lhou;

    const/4 v1, 0x1

    invoke-virtual {p1}, Liga;->a()Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Liga;->a()Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->k()J

    move-result-wide v3

    long-to-int v3, v3

    move-object v4, p1

    move-wide v5, p2

    invoke-virtual/range {v0 .. v6}, Lhou;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    return-void
.end method
