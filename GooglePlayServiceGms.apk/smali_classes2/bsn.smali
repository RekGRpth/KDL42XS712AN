.class public final enum Lbsn;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbsn;

.field public static final enum b:Lbsn;

.field public static final enum c:Lbsn;

.field public static final enum d:Lbsn;

.field private static final synthetic f:[Lbsn;


# instance fields
.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lbsn;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2, v3}, Lbsn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbsn;->a:Lbsn;

    new-instance v0, Lbsn;

    const-string v1, "OK_EXPIRED"

    invoke-direct {v0, v1, v3, v3}, Lbsn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbsn;->b:Lbsn;

    new-instance v0, Lbsn;

    const-string v1, "FAIL_USER_CONSENT_REQUIRED"

    invoke-direct {v0, v1, v4, v2}, Lbsn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbsn;->c:Lbsn;

    new-instance v0, Lbsn;

    const-string v1, "FAIL_PERMANENT"

    invoke-direct {v0, v1, v5, v2}, Lbsn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbsn;->d:Lbsn;

    const/4 v0, 0x4

    new-array v0, v0, [Lbsn;

    sget-object v1, Lbsn;->a:Lbsn;

    aput-object v1, v0, v2

    sget-object v1, Lbsn;->b:Lbsn;

    aput-object v1, v0, v3

    sget-object v1, Lbsn;->c:Lbsn;

    aput-object v1, v0, v4

    sget-object v1, Lbsn;->d:Lbsn;

    aput-object v1, v0, v5

    sput-object v0, Lbsn;->f:[Lbsn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lbsn;->e:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbsn;
    .locals 1

    const-class v0, Lbsn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbsn;

    return-object v0
.end method

.method public static values()[Lbsn;
    .locals 1

    sget-object v0, Lbsn;->f:[Lbsn;

    invoke-virtual {v0}, [Lbsn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbsn;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lbsn;->e:Z

    return v0
.end method
