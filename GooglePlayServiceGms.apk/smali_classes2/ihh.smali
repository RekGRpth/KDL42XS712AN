.class public final Lihh;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public b:J

.field public c:J

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lihh;->a:Ljava/util/List;

    iput-wide v1, p0, Lihh;->b:J

    iput-wide v1, p0, Lihh;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lihh;->f:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lihh;->f:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lihh;->b()I

    :cond_0
    iget v0, p0, Lihh;->f:I

    return v0
.end method

.method public final a(J)Lihh;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lihh;->d:Z

    iput-wide p1, p0, Lihh;->b:J

    return-object p0
.end method

.method public final a(Lihi;)Lihh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lihh;->a:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lihi;

    invoke-direct {v0}, Lihi;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lihh;->a(Lihi;)Lihh;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lihh;->a(J)Lihh;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lihh;->b(J)Lihh;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-object v0, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihi;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lihh;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lihh;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_1
    iget-boolean v0, p0, Lihh;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lihh;->c:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihi;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lihh;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v2, p0, Lihh;->b:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_1
    iget-boolean v0, p0, Lihh;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v2, p0, Lihh;->c:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_2
    iput v1, p0, Lihh;->f:I

    return v1
.end method

.method public final b(J)Lihh;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lihh;->e:Z

    iput-wide p1, p0, Lihh;->c:J

    return-object p0
.end method
