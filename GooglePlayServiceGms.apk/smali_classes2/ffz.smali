.class final Lffz;
.super Lfgu;
.source "SourceFile"


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 7

    new-instance v6, Lgli;

    invoke-direct {v6}, Lgli;-><init>()V

    const-string v0, "circles.firstTimeAdd.needConsent"

    iput-object v0, v6, Lgli;->c:Ljava/lang/String;

    iget-object v0, v6, Lgli;->f:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, v6, Lgli;->b:Z

    iget-object v0, v6, Lgli;->f:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    iget-object v1, v6, Lgli;->f:Ljava/util/Set;

    iget-object v2, v6, Lgli;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-boolean v3, v6, Lgli;->b:Z

    iget-object v4, v6, Lgli;->c:Ljava/lang/String;

    iget v5, v6, Lgli;->d:I

    iget-object v6, v6, Lgli;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;ZLjava/lang/String;ILjava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    const-string v1, "circles.firstTimeAdd.needConsent"

    invoke-virtual {p2, p3, v1, v0}, Lffe;->a(Lffa;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;)V

    sget-object v0, Lffc;->c:Lffc;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
