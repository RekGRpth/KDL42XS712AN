.class public Lna;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Z

.field public a:Landroid/widget/PopupWindow;

.field public b:Lnd;

.field c:I

.field d:I

.field e:Z

.field f:I

.field g:I

.field public h:Landroid/view/View;

.field public i:Landroid/widget/AdapterView$OnItemClickListener;

.field final j:Lni;

.field k:Landroid/os/Handler;

.field private l:Landroid/content/Context;

.field private m:Landroid/widget/ListAdapter;

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Landroid/view/View;

.field private s:Landroid/database/DataSetObserver;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final v:Lnh;

.field private final w:Lng;

.field private final x:Lne;

.field private y:Ljava/lang/Runnable;

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v0, -0x2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lna;->n:I

    iput v0, p0, Lna;->o:I

    iput-boolean v1, p0, Lna;->p:Z

    iput-boolean v1, p0, Lna;->q:Z

    const v0, 0x7fffffff

    iput v0, p0, Lna;->f:I

    iput v1, p0, Lna;->g:I

    new-instance v0, Lni;

    invoke-direct {v0, p0, v1}, Lni;-><init>(Lna;B)V

    iput-object v0, p0, Lna;->j:Lni;

    new-instance v0, Lnh;

    invoke-direct {v0, p0, v1}, Lnh;-><init>(Lna;B)V

    iput-object v0, p0, Lna;->v:Lnh;

    new-instance v0, Lng;

    invoke-direct {v0, p0, v1}, Lng;-><init>(Lna;B)V

    iput-object v0, p0, Lna;->w:Lng;

    new-instance v0, Lne;

    invoke-direct {v0, p0, v1}, Lne;-><init>(Lna;B)V

    iput-object v0, p0, Lna;->x:Lne;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lna;->k:Landroid/os/Handler;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lna;->z:Landroid/graphics/Rect;

    iput-object p1, p0, Lna;->l:Landroid/content/Context;

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    iget-object v0, p0, Lna;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lna;->A:Z

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lna;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lna;->z:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lna;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lna;->o:I

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lna;->o:I

    goto :goto_0
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lna;->s:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    new-instance v0, Lnf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lnf;-><init>(Lna;B)V

    iput-object v0, p0, Lna;->s:Landroid/database/DataSetObserver;

    :cond_0
    :goto_0
    iput-object p1, p0, Lna;->m:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lna;->m:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lna;->s:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    iget-object v0, p0, Lna;->b:Lnd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v1, p0, Lna;->m:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lnd;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lna;->m:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lna;->m:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lna;->s:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public b()V
    .locals 13

    const/high16 v12, -0x80000000

    const/4 v11, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    iget-object v0, p0, Lna;->b:Lnd;

    if-nez v0, :cond_7

    iget-object v4, p0, Lna;->l:Landroid/content/Context;

    new-instance v0, Lnb;

    invoke-direct {v0, p0}, Lnb;-><init>(Lna;)V

    iput-object v0, p0, Lna;->y:Ljava/lang/Runnable;

    new-instance v3, Lnd;

    iget-boolean v0, p0, Lna;->A:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0}, Lnd;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Lna;->b:Lnd;

    iget-object v0, p0, Lna;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Lnd;->setSelector(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->m:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Lnd;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->i:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Lnd;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lna;->b:Lnd;

    invoke-virtual {v0, v1}, Lnd;->setFocusable(Z)V

    iget-object v0, p0, Lna;->b:Lnd;

    invoke-virtual {v0, v1}, Lnd;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lna;->b:Lnd;

    new-instance v3, Lnc;

    invoke-direct {v3, p0}, Lnc;-><init>(Lna;)V

    invoke-virtual {v0, v3}, Lnd;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->w:Lng;

    invoke-virtual {v0, v3}, Lnd;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lna;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v3, p0, Lna;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v3}, Lnd;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_1
    iget-object v0, p0, Lna;->b:Lnd;

    iget-object v5, p0, Lna;->r:Landroid/view/View;

    if-eqz v5, :cond_1f

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v6, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iget v7, p0, Lna;->g:I

    packed-switch v7, :pswitch_data_0

    const-string v0, "ListPopupWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Invalid hint position "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lna;->g:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget v0, p0, Lna;->o:I

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, v0, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v5, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    :goto_2
    iget-object v4, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    :goto_3
    iget-object v3, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v4, p0, Lna;->z:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Lna;->z:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lna;->z:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    iget-boolean v4, p0, Lna;->e:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lna;->z:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iput v4, p0, Lna;->d:I

    :cond_2
    :goto_4
    iget-object v4, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    move v4, v1

    :goto_5
    iget-object v7, p0, Lna;->h:Landroid/view/View;

    iget v8, p0, Lna;->d:I

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v7, v9}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v5, 0x2

    new-array v10, v5, [I

    invoke-virtual {v7, v10}, Landroid/view/View;->getLocationOnScreen([I)V

    iget v5, v9, Landroid/graphics/Rect;->bottom:I

    if-eqz v4, :cond_1d

    invoke-virtual {v7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_6
    aget v5, v10, v1

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int/2addr v5, v7

    sub-int/2addr v4, v5

    sub-int/2addr v4, v8

    aget v5, v10, v1

    iget v7, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v7

    add-int/2addr v5, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v5, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v7, p0, Lna;->z:Landroid/graphics/Rect;

    invoke-virtual {v5, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v5, p0, Lna;->z:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lna;->z:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v7

    sub-int/2addr v4, v5

    :cond_3
    iget-boolean v5, p0, Lna;->p:Z

    if-nez v5, :cond_4

    iget v5, p0, Lna;->n:I

    if-ne v5, v6, :cond_a

    :cond_4
    add-int v0, v4, v3

    :goto_7
    invoke-virtual {p0}, Lna;->g()Z

    move-result v3

    iget-object v4, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_15

    iget v4, p0, Lna;->o:I

    if-ne v4, v6, :cond_c

    move v4, v6

    :goto_8
    iget v5, p0, Lna;->n:I

    if-ne v5, v6, :cond_12

    if-eqz v3, :cond_e

    move v5, v0

    :goto_9
    if-eqz v3, :cond_10

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget v3, p0, Lna;->o:I

    if-ne v3, v6, :cond_f

    :goto_a
    invoke-virtual {v0, v6, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    :goto_b
    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Lna;->q:Z

    if-nez v3, :cond_14

    iget-boolean v3, p0, Lna;->p:Z

    if-nez v3, :cond_14

    :goto_c
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lna;->h:Landroid/view/View;

    iget v2, p0, Lna;->c:I

    iget v3, p0, Lna;->d:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :cond_5
    :goto_d
    return-void

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    iget-object v3, p0, Lna;->r:Landroid/view/View;

    if-eqz v3, :cond_1e

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    goto/16 :goto_3

    :cond_8
    iget-object v3, p0, Lna;->z:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    move v3, v2

    goto/16 :goto_4

    :cond_9
    move v4, v2

    goto/16 :goto_5

    :cond_a
    iget v5, p0, Lna;->o:I

    packed-switch v5, :pswitch_data_1

    iget v5, p0, Lna;->o:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    :goto_e
    iget-object v7, p0, Lna;->b:Lnd;

    sub-int/2addr v4, v0

    invoke-virtual {v7, v5, v4}, Lnd;->a(II)I

    move-result v4

    if-lez v4, :cond_b

    add-int/2addr v0, v3

    :cond_b
    add-int/2addr v0, v4

    goto/16 :goto_7

    :pswitch_2
    iget-object v5, p0, Lna;->l:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Lna;->z:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lna;->z:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    invoke-static {v5, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto :goto_e

    :pswitch_3
    iget-object v5, p0, Lna;->l:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Lna;->z:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lna;->z:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto :goto_e

    :cond_c
    iget v4, p0, Lna;->o:I

    if-ne v4, v11, :cond_d

    iget-object v4, p0, Lna;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/16 :goto_8

    :cond_d
    iget v4, p0, Lna;->o:I

    goto/16 :goto_8

    :cond_e
    move v5, v6

    goto/16 :goto_9

    :cond_f
    move v6, v2

    goto/16 :goto_a

    :cond_10
    iget-object v3, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget v0, p0, Lna;->o:I

    if-ne v0, v6, :cond_11

    move v0, v6

    :goto_f
    invoke-virtual {v3, v0, v6}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    goto/16 :goto_b

    :cond_11
    move v0, v2

    goto :goto_f

    :cond_12
    iget v3, p0, Lna;->n:I

    if-ne v3, v11, :cond_13

    move v5, v0

    goto/16 :goto_b

    :cond_13
    iget v5, p0, Lna;->n:I

    goto/16 :goto_b

    :cond_14
    move v1, v2

    goto/16 :goto_c

    :cond_15
    iget v3, p0, Lna;->o:I

    if-ne v3, v6, :cond_18

    move v3, v6

    :goto_10
    iget v4, p0, Lna;->n:I

    if-ne v4, v6, :cond_1a

    move v0, v6

    :goto_11
    iget-object v4, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Lna;->q:Z

    if-nez v3, :cond_1c

    iget-boolean v3, p0, Lna;->p:Z

    if-nez v3, :cond_1c

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lna;->v:Lnh;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lna;->h:Landroid/view/View;

    iget v2, p0, Lna;->c:I

    iget v3, p0, Lna;->d:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    iget-object v0, p0, Lna;->b:Lnd;

    invoke-virtual {v0, v6}, Lnd;->setSelection(I)V

    iget-boolean v0, p0, Lna;->A:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Lna;->b:Lnd;

    invoke-virtual {v0}, Lnd;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_16
    invoke-virtual {p0}, Lna;->e()V

    :cond_17
    iget-boolean v0, p0, Lna;->A:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lna;->k:Landroid/os/Handler;

    iget-object v1, p0, Lna;->x:Lne;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_d

    :cond_18
    iget v3, p0, Lna;->o:I

    if-ne v3, v11, :cond_19

    iget-object v3, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget-object v4, p0, Lna;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_10

    :cond_19
    iget-object v3, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget v4, p0, Lna;->o:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_10

    :cond_1a
    iget v4, p0, Lna;->n:I

    if-ne v4, v11, :cond_1b

    iget-object v4, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto :goto_11

    :cond_1b
    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    iget v4, p0, Lna;->n:I

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto :goto_11

    :cond_1c
    move v1, v2

    goto :goto_12

    :cond_1d
    move v4, v5

    goto/16 :goto_6

    :cond_1e
    move v0, v2

    goto/16 :goto_3

    :cond_1f
    move-object v3, v0

    move v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lna;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lna;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lna;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iput-object v2, p0, Lna;->b:Lnd;

    iget-object v0, p0, Lna;->k:Landroid/os/Handler;

    iget-object v1, p0, Lna;->j:Lni;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lna;->b:Lnd;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lnd;->a(Lnd;Z)Z

    invoke-virtual {v0}, Lnd;->requestLayout()V

    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    iget-object v0, p0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
