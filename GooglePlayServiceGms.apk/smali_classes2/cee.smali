.class public final enum Lcee;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcee;

.field public static final enum b:Lcee;

.field private static final synthetic d:[Lcee;


# instance fields
.field private final c:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v0, Lcee;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lced;->d()Lced;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "entryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcee;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcee;->a:Lcee;

    new-instance v0, Lcee;

    const-string v1, "PACKAGING_SERVICE_ID"

    invoke-static {}, Lced;->d()Lced;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "scope"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    sget-object v5, Lcee;->a:Lcee;

    iget-object v5, v5, Lcee;->c:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcee;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcee;->b:Lcee;

    const/4 v0, 0x2

    new-array v0, v0, [Lcee;

    sget-object v1, Lcee;->a:Lcee;

    aput-object v1, v0, v7

    sget-object v1, Lcee;->b:Lcee;

    aput-object v1, v0, v6

    sput-object v0, Lcee;->d:[Lcee;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcee;->c:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcee;
    .locals 1

    const-class v0, Lcee;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcee;

    return-object v0
.end method

.method public static values()[Lcee;
    .locals 1

    sget-object v0, Lcee;->d:[Lcee;

    invoke-virtual {v0}, [Lcee;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcee;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcee;->c:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcee;->c:Lcdp;

    return-object v0
.end method
