.class public Lhyt;
.super Lbqi;
.source "SourceFile"


# static fields
.field static volatile f:Lhyt;


# instance fields
.field final d:Landroid/content/BroadcastReceiver;

.field volatile e:Z

.field private final g:Lhzd;

.field private final h:Lhzc;

.field private final i:Lhyz;

.field private final j:Lhzb;

.field private final k:Lhzg;

.field private final l:Lhzh;

.field private final m:Lhzf;

.field private final n:Lhzi;

.field private final o:Lhza;

.field private final p:Lhyw;

.field private final q:Ljava/lang/Object;

.field private final r:Lbpe;

.field private final s:Landroid/content/Context;

.field private final t:Lhys;

.field private final u:Landroid/content/IntentFilter;

.field private volatile v:Z

.field private final w:Ljava/util/List;

.field private final x:Lhzn;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lbpe;)V
    .locals 1

    const-class v0, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {p0, p1, p2, v0}, Lhyt;-><init>(Landroid/content/Context;Lbpe;Ljava/lang/Class;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lbpe;Ljava/lang/Class;)V
    .locals 9

    const/4 v6, 0x0

    const/16 v1, 0x64

    const/4 v8, 0x0

    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, p1}, Lbqi;-><init>(Ljava/lang/String;ZLandroid/content/Context;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhyt;->q:Ljava/lang/Object;

    iput-boolean v8, p0, Lhyt;->v:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhyt;->w:Ljava/util/List;

    iput-boolean v8, p0, Lhyt;->e:Z

    iput-object p1, p0, Lhyt;->s:Landroid/content/Context;

    iput-object p2, p0, Lhyt;->r:Lbpe;

    new-instance v0, Lhys;

    move-object v2, p2

    move-object v3, p1

    move-object v4, p0

    move-object v5, p3

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lhys;-><init>(ILbpe;Landroid/content/Context;Lhyt;Ljava/lang/Class;Lhyl;Lhzj;)V

    iput-object v0, p0, Lhyt;->t:Lhys;

    new-instance v0, Lhzd;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzd;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->g:Lhzd;

    new-instance v0, Lhyz;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhyz;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->i:Lhyz;

    new-instance v0, Lhzc;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzc;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->h:Lhzc;

    new-instance v0, Lhzb;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzb;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->j:Lhzb;

    new-instance v0, Lhzg;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzg;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->k:Lhzg;

    new-instance v0, Lhzh;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzh;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->l:Lhzh;

    new-instance v0, Lhzf;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzf;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->m:Lhzf;

    new-instance v0, Lhzi;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhzi;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->n:Lhzi;

    new-instance v0, Lhza;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhza;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->o:Lhza;

    new-instance v0, Lhyw;

    iget-object v2, p0, Lhyt;->t:Lhys;

    invoke-direct {v0, p0, v2}, Lhyw;-><init>(Lhyt;Lhys;)V

    iput-object v0, p0, Lhyt;->p:Lhyw;

    sget-boolean v0, Lhyb;->a:Z

    invoke-virtual {p0, v0}, Lhyt;->a(Z)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {p0, v1}, Lhyt;->a(I)V

    new-instance v0, Lhze;

    invoke-direct {v0, p0, v8}, Lhze;-><init>(Lhyt;B)V

    iput-object v0, p0, Lhyt;->d:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lhyt;->t:Lhys;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iput-object v0, p0, Lhyt;->u:Landroid/content/IntentFilter;

    iget-object v0, p0, Lhyt;->g:Lhzd;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->i:Lhyz;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->h:Lhzc;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->j:Lhzb;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->k:Lhzg;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->l:Lhzh;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->m:Lhzf;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->n:Lhzi;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->o:Lhza;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->p:Lhyw;

    invoke-virtual {p0, v0}, Lhyt;->a(Lbqh;)V

    iget-object v0, p0, Lhyt;->g:Lhzd;

    invoke-virtual {p0, v0}, Lhyt;->b(Lbqh;)V

    new-instance v0, Lhyu;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lhyu;-><init>(Lhyt;Landroid/os/Handler;)V

    new-instance v1, Lhzn;

    invoke-direct {v1, p1, v0}, Lhzn;-><init>(Landroid/content/Context;Landroid/database/ContentObserver;)V

    iput-object v1, p0, Lhyt;->x:Lhzn;

    return-void

    :cond_0
    const/16 v1, 0xa

    goto :goto_0
.end method

.method static synthetic a(Lhyt;)Landroid/content/IntentFilter;
    .locals 1

    iget-object v0, p0, Lhyt;->u:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lhyt;
    .locals 3

    const-class v1, Lhyt;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhyt;->f:Lhyt;

    if-eqz v0, :cond_0

    sget-object v0, Lhyt;->f:Lhyt;

    invoke-direct {v0}, Lhyt;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const-string v2, "Creating GeofencerStateMachine"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lhyt;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lhyt;-><init>(Landroid/content/Context;Lbpe;)V

    sput-object v0, Lhyt;->f:Lhyt;

    invoke-virtual {v0}, Lhyt;->b()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lhyt;->f:Lhyt;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lhyt;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "android.intent.extra.DATA_REMOVED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Package removed."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lhyt;->c(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lhyt;Landroid/os/Message;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic b(Lhyt;)V
    .locals 3

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendUserSwitched."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendUserSwitched"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lhyt;->c(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lhyt;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lhyt;->c(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lhyt;Landroid/os/Message;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic b(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic c(Lhyt;)Lbqg;
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->b(Lbql;)Lbqg;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lhyt;
    .locals 2

    const-class v1, Lhyt;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhyt;->f:Lhyt;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_2

    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App data cleared. Removing geofences from: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhzv;->a(Ljava/lang/String;Leqr;)Lhzv;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhyt;->a(Lhzv;)V

    goto :goto_0
.end method

.method static synthetic c(Lhyt;Landroid/os/Message;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic c(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic d(Lhyt;)Lhzg;
    .locals 1

    iget-object v0, p0, Lhyt;->k:Lhzg;

    return-object v0
.end method

.method static synthetic d(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic e(Lhyt;)Lhzh;
    .locals 1

    iget-object v0, p0, Lhyt;->l:Lhzh;

    return-object v0
.end method

.method static synthetic e(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic f(Lhyt;)Lhzf;
    .locals 1

    iget-object v0, p0, Lhyt;->m:Lhzf;

    return-object v0
.end method

.method static synthetic f(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic g(Lhyt;)Lhzi;
    .locals 1

    iget-object v0, p0, Lhyt;->n:Lhzi;

    return-object v0
.end method

.method static synthetic g(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method private g()Z
    .locals 2

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic h(Lhyt;)Lhza;
    .locals 1

    iget-object v0, p0, Lhyt;->o:Lhza;

    return-object v0
.end method

.method static synthetic h(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic i(Lhyt;)Lhyw;
    .locals 1

    iget-object v0, p0, Lhyt;->p:Lhyw;

    return-object v0
.end method

.method static synthetic i(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic j(Lhyt;)Lhzn;
    .locals 1

    iget-object v0, p0, Lhyt;->x:Lhzn;

    return-object v0
.end method

.method static synthetic j(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic k(Lhyt;)Lhzb;
    .locals 1

    iget-object v0, p0, Lhyt;->j:Lhzb;

    return-object v0
.end method

.method static synthetic k(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic l(Lhyt;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhyt;->d(I)V

    return-void
.end method

.method static synthetic l(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic m(Lhyt;)Lhzc;
    .locals 1

    iget-object v0, p0, Lhyt;->h:Lhzc;

    return-object v0
.end method

.method static synthetic m(Lhyt;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Lhyt;->a(Lbqg;)V

    return-void
.end method

.method static synthetic n(Lhyt;)Lhyz;
    .locals 1

    iget-object v0, p0, Lhyt;->i:Lhyz;

    return-object v0
.end method

.method static synthetic o(Lhyt;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhyt;->d(I)V

    return-void
.end method

.method static synthetic p(Lhyt;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhyt;->d(I)V

    return-void
.end method

.method static synthetic q(Lhyt;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lhyt;->s:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic r(Lhyt;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhyt;->d(I)V

    return-void
.end method

.method static synthetic s(Lhyt;)Lbpe;
    .locals 1

    iget-object v0, p0, Lhyt;->r:Lbpe;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    invoke-super {p0}, Lbqi;->a()V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v1, "onQuitting"

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhyt;->x:Lhzn;

    iget-object v1, v0, Lhzn;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, v0, Lhzn;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lhyt;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lhyt;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->v:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendInitialize called more than once."

    invoke-static {v0, v2}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_2

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring sendInitialize."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhyt;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyp;

    const/16 v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhyp;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1

    goto :goto_0

    :cond_2
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendInitialize: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lhyt;->a(ILjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyt;->v:Z

    iget-object v0, p0, Lhyt;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyp;

    invoke-virtual {v0, p0}, Lhyp;->a(Lbqi;)V

    goto :goto_2

    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendNewLocation."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendNewLocation: location="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x6

    iget-object v2, p0, Lhyt;->r:Lbpe;

    invoke-interface {v2}, Lbpe;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lhyt;->a(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lhzp;Lhzp;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendMovementChange."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendMovementChange: previousMovement="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",currentMovement="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x7

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lhyt;->a(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lhzv;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring removeGeofences."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhzv;->a(Ljava/lang/Object;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeGeofences: removeRequest="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lhyt;->v:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1, p0}, Lhzv;->a(Lbqi;)V

    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "GeofencerStateMachine"

    const-string v2, "State machine not initialized, putting request to pending requests."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lhyt;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Leqr;Landroid/app/PendingIntent;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring addGeofences."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3e8

    invoke-static {p2, v0, p1}, Lhyk;->a(Leqr;ILjava/util/List;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addGeofences: geofences="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lhyk;

    invoke-direct {v0, p1, p3, p2}, Lhyk;-><init>(Ljava/util/List;Landroid/app/PendingIntent;Leqr;)V

    iget-boolean v2, p0, Lhyt;->v:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0, p0}, Lhyk;->a(Lbqi;)V

    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_3

    const-string v2, "GeofencerStateMachine"

    const-string v3, "State machine not initialized, putting request to pending requests."

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lhyt;->w:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method protected final b(I)Ljava/lang/String;
    .locals 2

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (Message not named in getWhatToString)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "SM_QUERY_LOCATION_OPT_IN_CMD"

    goto :goto_0

    :sswitch_1
    const-string v0, "SM_INITIALIZE_CMD"

    goto :goto_0

    :sswitch_2
    const-string v0, "SM_STOP_CMD"

    goto :goto_0

    :sswitch_3
    const-string v0, "SM_ADD_GEOFENCE_LIST_CMD"

    goto :goto_0

    :sswitch_4
    const-string v0, "SM_REMOVE_GEOFENCE_CMD"

    goto :goto_0

    :sswitch_5
    const-string v0, "SM_LOCATION_CMD"

    goto :goto_0

    :sswitch_6
    const-string v0, "SM_ACTIVITY_CMD"

    goto :goto_0

    :sswitch_7
    const-string v0, "SM_UPDATE_DETECTOR_REQUIREMENT_CMD"

    goto :goto_0

    :sswitch_8
    const-string v0, "SM_SYSTEM_EVENT_CMD"

    goto :goto_0

    :sswitch_9
    const-string v0, "SM_DUMP_CMD"

    goto :goto_0

    :sswitch_a
    const-string v0, "SM_SEND_AND_WAIT_FOR_TEST_CMD"

    goto :goto_0

    :sswitch_b
    const-string v0, "SM_SAVE_ACTIVITY_STATE_CMD"

    goto :goto_0

    :sswitch_c
    const-string v0, "SM_USER_SWITCH_CMD"

    goto :goto_0

    :sswitch_d
    const-string v0, "SM_HARDWARE_GEOFENCE_CHANGED_CMD"

    goto :goto_0

    :sswitch_e
    const-string v0, "SM_HARDWARE_GEFOENCE_AVAILABILITY_CMD"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_b
        0xb -> :sswitch_c
        0xc -> :sswitch_d
        0xd -> :sswitch_e
        0x62 -> :sswitch_a
        0x63 -> :sswitch_9
    .end sparse-switch
.end method

.method public final b()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    const/4 v4, 0x1

    invoke-super {p0}, Lbqi;->b()V

    iget-object v0, p0, Lhyt;->x:Lhzn;

    iget-object v1, v0, Lhzn;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lhhv;->a:Landroid/net/Uri;

    iget-object v3, v0, Lhzn;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, v0, Lhzn;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_providers_allowed"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v0, v0, Lhzn;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lhyt;->t:Lhys;

    iget-object v1, v0, Lhys;->h:Lhxq;

    invoke-virtual {v1}, Lhxq;->a()V

    iget-object v0, v0, Lhys;->i:Lhxq;

    invoke-virtual {v0}, Lhxq;->a()V

    iget-object v0, p0, Lhyt;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lhyt;->d:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lhyt;->u:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/16 v0, 0x11

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lhyt;->s:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhyt;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lhyt;->s:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhyt;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring sendSystemEvent."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendSystemEvent: intent="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Lhyt;->a(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring dump."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "State machine quitted."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lhym;

    invoke-direct {v0, p1}, Lhym;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Lhym;->a(Lbqi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Lhym;->b()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "Dump interrupted.\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 4

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendHardwareGeofenceAvailability."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendHardwareGeofenceAvailability: availabile="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0xd

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lhyt;->a(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final d()V
    .locals 3

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendQueryLocationOptIn."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendQueryLocationOptIn"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhyt;->c(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 3

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendSaveActivityState."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendSaveActivityState"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0xa

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lhyt;->a(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 3

    iget-object v1, p0, Lhyt;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhyt;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendHardwareGeofenceChanged."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendHardwareGeofenceChanged"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lhyt;->c(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
