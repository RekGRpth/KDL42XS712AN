.class public final Lcmj;
.super Lclu;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V
    .locals 1

    sget-object v0, Lcmr;->a:Lcmr;

    invoke-direct {p0, v0, p1, p2, p3}, Lclu;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 1

    sget-object v0, Lcmr;->a:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclu;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmj;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcfz;Lbsp;)Lcml;
    .locals 0

    return-object p0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcoy;)V
    .locals 0

    return-void
.end method

.method public final bridge synthetic b(Lcfz;)Lbsp;
    .locals 1

    invoke-super {p0, p1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()Lorg/json/JSONObject;
    .locals 1

    invoke-super {p0}, Lclu;->b()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcmj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcmj;

    invoke-virtual {p0, p1}, Lcmj;->a(Lclu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcmj;->c()I

    move-result v0

    mul-int/lit8 v0, v0, 0x11

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NullOp[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcmj;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
