.class final Lfpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lfpk;

.field private final b:Ljava/lang/String;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lfpk;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lfpl;->a:Lfpk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lfpl;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lfpl;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lfpl;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lfpl;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfpl;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    iget-object v0, p0, Lfpl;->a:Lfpk;

    invoke-static {v0}, Lfpk;->a(Lfpk;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01d3    # com.google.android.gms.R.dimen.plus_apps_max_icon_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lfpl;->b:Ljava/lang/String;

    invoke-static {v1}, Lbit;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lbit;->a(ILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lfpl;->a:Lfpk;

    invoke-static {v1}, Lfpk;->c(Lfpk;)Lbmi;

    move-result-object v1

    iget-object v2, p0, Lfpl;->a:Lfpk;

    invoke-static {v2}, Lfpk;->b(Lfpk;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lbmi;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfpl;->a:Lfpk;

    invoke-static {v0}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpl;->a:Lfpk;

    invoke-static {v1}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    invoke-static {v0, v1}, Lbke;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    array-length v2, v0

    iget-object v3, p0, Lfpl;->a:Lfpk;

    invoke-static {v3}, Lfpk;->e(Lfpk;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lfpl;->c:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lfpl;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    iget-object v0, p0, Lfpl;->a:Lfpk;

    invoke-static {v0}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpl;->a:Lfpk;

    invoke-static {v1}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Lfpl;->a:Lfpk;

    invoke-static {v0}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpl;->a:Lfpk;

    invoke-static {v1}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, Lfpl;->a:Lfpk;

    invoke-static {v0}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpl;->a:Lfpk;

    invoke-static {v1}, Lfpk;->d(Lfpk;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
