.class final Lhbv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lhbu;


# direct methods
.method constructor <init>(Lhbu;)V
    .locals 0

    iput-object p1, p0, Lhbv;->a:Lhbu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, Lhbv;->a:Lhbu;

    invoke-static {p2}, Lhct;->a(Landroid/os/IBinder;)Lhcs;

    move-result-object v1

    iput-object v1, v0, Lhbu;->j:Lhcs;

    iget-object v0, p0, Lhbv;->a:Lhbu;

    iget-object v0, v0, Lhbu;->m:Landroid/os/Handler;

    const/16 v1, 0x7fff

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lhbv;->a:Lhbu;

    iget-object v1, v1, Lhbu;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhbv;->a:Lhbu;

    iget-object v0, v0, Lhbu;->m:Landroid/os/Handler;

    const v1, 0x8000

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lhbv;->a:Lhbu;

    iget-object v1, v1, Lhbu;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    iget-object v0, p0, Lhbv;->a:Lhbu;

    const/4 v1, 0x0

    iput-object v1, v0, Lhbu;->j:Lhcs;

    return-void
.end method
