.class public final Lebj;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/view/LayoutInflater;

.field private final i:Lebk;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldvn;Lebk;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lebj;-><init>(Ldvn;Lebk;I)V

    return-void
.end method

.method public constructor <init>(Ldvn;Lebk;I)V
    .locals 1

    sget v0, Lxb;->j:I

    invoke-direct {p0, p1, v0, p3}, Ldwx;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Lebj;->g:Landroid/content/Context;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lebj;->h:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lebj;->i:Lebk;

    return-void
.end method

.method static synthetic a(Lebj;)Lebk;
    .locals 1

    iget-object v0, p0, Lebj;->i:Lebk;

    return-object v0
.end method

.method static synthetic b(Lebj;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebj;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lebj;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebj;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    check-cast p4, Ldlb;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lebl;

    iget-object v1, v0, Lebl;->j:Lebj;

    iget-object v1, v1, Lebj;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {p4}, Ldlb;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    :cond_0
    iget-object v4, v0, Lebl;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v5, Lwz;->e:I

    invoke-virtual {v0, v4, v1, v5}, Lebl;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lebl;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Lebl;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lebl;->c:Landroid/database/CharArrayBuffer;

    iget-object v3, v3, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v4, v0, Lebl;->c:Landroid/database/CharArrayBuffer;

    iget v4, v4, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v3, v7, v4}, Landroid/widget/TextView;->setText([CII)V

    invoke-interface {p4, v8}, Ldlb;->a(I)I

    move-result v1

    const/4 v3, 0x2

    invoke-interface {p4, v3}, Ldlb;->a(I)I

    move-result v3

    iget-object v4, v0, Lebl;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    if-lez v1, :cond_1

    sget v4, Lxe;->f:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, v0, Lebl;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-lez v3, :cond_3

    iget-object v1, v0, Lebl;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, v0, Lebl;->e:Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    sget v1, Lxe;->h:I

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lebl;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, v0, Lebl;->d:Landroid/widget/TextView;

    iget-object v3, v0, Lebl;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {p4}, Ldlb;->b()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {p4}, Ldlb;->c()I

    move-result v3

    iget-object v4, v0, Lebl;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v5

    sget v6, Lwz;->f:I

    invoke-virtual {v0, v4, v5, v6}, Lebl;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v4, v0, Lebl;->g:Landroid/widget/TextView;

    sget v5, Lxe;->g:I

    new-array v6, v8, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-virtual {v2, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lebl;->h:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lebl;->i:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lebj;->j:Ljava/lang/String;

    iput-object p2, p0, Lebj;->k:Ljava/lang/String;

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lebj;->h:Landroid/view/LayoutInflater;

    sget v1, Lxc;->w:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lebl;

    invoke-direct {v1, p0, v0}, Lebl;-><init>(Lebj;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
