.class public final Ling;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lizf;

.field private c:Z

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ling;->a:I

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ling;->b:Lizf;

    const/4 v0, -0x1

    iput v0, p0, Ling;->e:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ling;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ling;->b()I

    :cond_0
    iget v0, p0, Ling;->e:I

    return v0
.end method

.method public final a(I)Ling;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ling;->c:Z

    iput p1, p0, Ling;->a:I

    return-object p0
.end method

.method public final a(Lizf;)Ling;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ling;->d:Z

    iput-object p1, p0, Ling;->b:Lizf;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ling;->a(I)Ling;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ling;->a(Lizf;)Ling;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ling;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ling;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Ling;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ling;->b:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ling;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ling;->a:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ling;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ling;->b:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Ling;->e:I

    return v0
.end method
