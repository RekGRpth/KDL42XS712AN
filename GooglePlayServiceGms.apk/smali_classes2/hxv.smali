.class public final Lhxv;
.super Lhxs;
.source "SourceFile"


# instance fields
.field d:D

.field private final e:D


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V
    .locals 4

    invoke-direct {p0, p1}, Lhxs;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V

    const-wide v0, 0x400921fb54442d18L    # Math.PI

    iget-object v2, p0, Lhxv;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v2}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iget-object v2, p0, Lhxv;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v2}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lhxv;->e:D

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 19

    invoke-super/range {p0 .. p1}, Lhxs;->a(Landroid/location/Location;)V

    const-wide v1, 0x3fc999999999999aL    # 0.2

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lhxv;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v3}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v3

    const-wide v7, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v7, v1

    mul-double/2addr v7, v1

    move-object/from16 v0, p0

    iget-wide v1, v0, Lhxv;->c:D

    add-double v9, v5, v3

    cmpl-double v1, v1, v9

    if-ltz v1, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    div-double/2addr v1, v7

    move-object/from16 v0, p0

    iput-wide v1, v0, Lhxv;->d:D

    move-object/from16 v0, p0

    iget-object v1, v0, Lhxv;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v1}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_2

    const-wide/high16 v1, 0x3fe8000000000000L    # 0.75

    :goto_1
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhxv;->d:D

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v3, v5

    if-ltz v3, :cond_4

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhxv;->d:D

    cmpl-double v1, v3, v1

    if-ltz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-byte v1, v0, Lhxv;->b:B

    :goto_3
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lhxv;->c:D

    sub-double v9, v5, v3

    cmpg-double v1, v1, v9

    if-gtz v1, :cond_1

    move-object/from16 v0, p0

    iget-wide v1, v0, Lhxv;->e:D

    invoke-static {v7, v8, v1, v2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-wide v1, v0, Lhxv;->c:D

    mul-double v9, v3, v3

    mul-double v11, v1, v1

    mul-double v13, v3, v3

    add-double/2addr v11, v13

    mul-double v13, v5, v5

    sub-double/2addr v11, v13

    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    mul-double/2addr v13, v1

    mul-double/2addr v13, v3

    div-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->acos(D)D

    move-result-wide v11

    mul-double/2addr v9, v11

    mul-double v11, v5, v5

    mul-double v13, v1, v1

    mul-double v15, v5, v5

    add-double/2addr v13, v15

    mul-double v15, v3, v3

    sub-double/2addr v13, v15

    const-wide/high16 v15, 0x4000000000000000L    # 2.0

    mul-double/2addr v15, v1

    mul-double/2addr v15, v5

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->acos(D)D

    move-result-wide v13

    mul-double/2addr v11, v13

    const-wide/high16 v13, 0x3fe0000000000000L    # 0.5

    neg-double v15, v1

    add-double/2addr v15, v3

    add-double/2addr v15, v5

    add-double v17, v1, v3

    sub-double v17, v17, v5

    mul-double v15, v15, v17

    sub-double v17, v1, v3

    add-double v17, v17, v5

    mul-double v15, v15, v17

    add-double/2addr v1, v3

    add-double/2addr v1, v5

    mul-double/2addr v1, v15

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    mul-double/2addr v1, v13

    add-double v3, v9, v11

    sub-double v1, v3, v1

    goto/16 :goto_0

    :cond_2
    const-wide v1, 0x3feb333333333333L    # 0.85

    goto :goto_1

    :cond_3
    const/4 v1, 0x3

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhxv;->d:D

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double v1, v5, v1

    cmpg-double v1, v3, v1

    if-gtz v1, :cond_5

    const/4 v1, 0x2

    :goto_4
    move-object/from16 v0, p0

    iput-byte v1, v0, Lhxv;->b:B

    goto :goto_3

    :cond_5
    const/4 v1, 0x4

    goto :goto_4
.end method
