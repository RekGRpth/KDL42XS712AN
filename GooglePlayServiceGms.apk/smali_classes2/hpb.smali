.class abstract Lhpb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhpc;


# instance fields
.field private final a:I

.field private final b:Livk;

.field private final c:Livk;

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method protected constructor <init>(ILivk;Livk;III)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lhpb;->a:I

    iput-object p2, p0, Lhpb;->b:Livk;

    iput-object p3, p0, Lhpb;->c:Livk;

    iput v0, p0, Lhpb;->d:I

    const/4 v0, 0x3

    iput v0, p0, Lhpb;->e:I

    const/4 v0, 0x4

    iput v0, p0, Lhpb;->f:I

    return-void
.end method


# virtual methods
.method protected abstract a(Livi;)Ljava/lang/Object;
.end method

.method public final a(Lhox;Ljava/io/InputStream;)V
    .locals 10

    invoke-virtual {p2}, Ljava/io/InputStream;->read()I

    move-result v0

    iget v1, p0, Lhpb;->a:I

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch while reading LRU cache file (expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lhpb;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lhpb;->b:Livk;

    invoke-static {p2, v0}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v1

    iget v0, p0, Lhpb;->d:I

    invoke-virtual {v1, v0}, Livi;->k(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget v3, p0, Lhpb;->d:I

    invoke-virtual {v1, v3, v0}, Livi;->c(II)Livi;

    move-result-object v3

    invoke-virtual {p0, v3}, Lhpb;->b(Livi;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v3}, Lhpb;->a(Livi;)Ljava/lang/Object;

    move-result-object v5

    iget v6, p0, Lhpb;->e:I

    invoke-virtual {v3, v6}, Livi;->d(I)J

    move-result-wide v6

    iget v8, p0, Lhpb;->f:I

    invoke-virtual {v3, v8}, Livi;->d(I)J

    move-result-wide v8

    new-instance v3, Lhuo;

    invoke-direct {v3, v5, v6, v7}, Lhuo;-><init>(Ljava/lang/Object;J)V

    iput-wide v8, v3, Lhuo;->c:J

    invoke-virtual {p1, v4, v3}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lhox;Ljava/io/OutputStream;)V
    .locals 7

    new-instance v1, Livi;

    iget-object v0, p0, Lhpb;->b:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    invoke-virtual {p1}, Lhox;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuo;

    new-instance v4, Livi;

    iget-object v5, p0, Lhpb;->c:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    iget-object v5, v0, Lhuo;->a:Ljava/lang/Object;

    invoke-virtual {p0, v4, v3, v5}, Lhpb;->a(Livi;Ljava/lang/Object;Ljava/lang/Object;)V

    iget v3, p0, Lhpb;->e:I

    iget-wide v5, v0, Lhuo;->b:J

    invoke-virtual {v4, v3, v5, v6}, Livi;->a(IJ)Livi;

    iget v3, p0, Lhpb;->f:I

    iget-wide v5, v0, Lhuo;->c:J

    invoke-virtual {v4, v3, v5, v6}, Livi;->a(IJ)Livi;

    iget v0, p0, Lhpb;->d:I

    invoke-virtual {v1, v0, v4}, Livi;->a(ILivi;)V

    goto :goto_0

    :cond_0
    iget v0, p0, Lhpb;->a:I

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v1, p2}, Livi;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method protected abstract a(Livi;Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method protected abstract b(Livi;)Ljava/lang/Object;
.end method
