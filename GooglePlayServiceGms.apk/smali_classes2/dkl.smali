.class final Ldkl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:Landroid/os/Message;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Ldkl;->c:I

    iput v0, p0, Ldkl;->a:I

    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 1

    iget v0, p0, Ldkl;->c:I

    or-int/2addr v0, p1

    iput v0, p0, Ldkl;->c:I

    return-void
.end method

.method public final a()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ldkl;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Ldkl;->a(I)V

    goto :goto_0
.end method

.method final b(I)Z
    .locals 1

    iget v0, p0, Ldkl;->c:I

    and-int/2addr v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    iget v0, p0, Ldkl;->c:I

    if-nez v0, :cond_0

    const-string v0, "Initial State"

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "First Presence Received. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Second Presence Received. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "First Presence Sent. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "Second Presence Sent. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Connect called. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "Connection attempt started. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/16 v1, 0x40

    invoke-virtual {p0, v1}, Ldkl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "Connection attempt complete. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
