.class final Lcvp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field b:Ljava/util/HashMap;

.field private c:Lbhk;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcvp;->a:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcvp;->a(Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    iget-object v0, p0, Lcvp;->c:Lbhk;

    invoke-virtual {v0}, Lbhk;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcvp;->c:Lbhk;

    invoke-virtual {v0}, Lbhk;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/util/HashMap;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcvp;->b:Ljava/util/HashMap;

    new-instance v0, Lbhk;

    sget-object v1, Lcvo;->c:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v2, v2}, Lbhk;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcvp;->c:Lbhk;

    iget-object v0, p0, Lcvp;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iget-object v2, p0, Lcvp;->c:Lbhk;

    invoke-virtual {v2, v0}, Lbhk;->a(Landroid/content/ContentValues;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcvp;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
