.class public final Lbvm;
.super Lbvf;
.source "SourceFile"


# instance fields
.field private final b:Lcoy;

.field private final c:Lcfc;

.field private final d:Ljava/util/List;

.field private final e:J

.field private final f:Lbvs;

.field private final g:Landroid/content/SyncResult;

.field private final h:Lbwd;


# direct methods
.method public constructor <init>(Lcoy;Lcfc;Ljava/util/List;Lbvs;Landroid/content/SyncResult;)V
    .locals 9

    invoke-direct {p0}, Lbvf;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoy;

    iput-object v0, p0, Lbvm;->b:Lcoy;

    iput-object p2, p0, Lbvm;->c:Lcfc;

    const-wide/high16 v0, -0x8000000000000000L

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    iget-object v3, v0, Lcgb;->a:Lbuw;

    invoke-virtual {v3}, Lbuw;->c()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lcgb;->b:Ljava/lang/Long;

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lbkm;->b(Z)V

    iget-object v3, v0, Lcgb;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-gtz v3, :cond_0

    const-string v3, "PreparedSyncMore"

    const-string v5, "Unexpectedly low clipTime in %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v3, v5, v6}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-wide v0, v1

    move-wide v1, v0

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    iput-wide v1, p0, Lbvm;->e:J

    iput-object p3, p0, Lbvm;->d:Ljava/util/List;

    iput-object p4, p0, Lbvm;->f:Lbvs;

    iput-object p5, p0, Lbvm;->g:Landroid/content/SyncResult;

    invoke-virtual {p1}, Lcoy;->k()Lbwd;

    move-result-object v0

    iput-object v0, p0, Lbvm;->h:Lbwd;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "PreparedSyncMore"

    const-string v1, "Error syncing more."

    invoke-static {v0, p0, v1}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lbvm;->c:Lcfc;

    iget-object v3, v0, Lcfc;->a:Ljava/lang/String;

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->c(Ljava/lang/String;)V

    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lbvm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    new-instance v6, Lbvn;

    iget-object v7, p0, Lbvm;->c:Lcfc;

    iget-object v8, p0, Lbvm;->f:Lbvs;

    invoke-direct {v6, v7, v8, v0, p1}, Lbvn;-><init>(Lcfc;Lbvs;Lcgb;I)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljdu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljdw; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lbue; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lbvm;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    :goto_1
    move v0, v2

    :goto_2
    return v0

    :cond_0
    :try_start_2
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwz;

    :goto_3
    new-instance v4, Lbvt;

    iget-object v5, p0, Lbvm;->b:Lcoy;

    invoke-direct {v4, v5}, Lbvt;-><init>(Lcoy;)V

    iget-object v5, p0, Lbvm;->g:Landroid/content/SyncResult;

    invoke-interface {v0, v4, v5}, Lbwz;->a(Lbvt;Landroid/content/SyncResult;)V

    iget-object v5, p0, Lbvm;->g:Landroid/content/SyncResult;

    invoke-virtual {v4, v5}, Lbvt;->a(Landroid/content/SyncResult;)V

    iget-object v4, p0, Lbvm;->g:Landroid/content/SyncResult;

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Lbwz;->a(Landroid/content/SyncResult;Z)V
    :try_end_2
    .catch Ljdu; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljdw; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lbue; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    :cond_1
    :try_start_3
    new-instance v0, Lbxc;

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Lbxc;-><init>(Ljava/util/Collection;B)V
    :try_end_3
    .catch Ljdu; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljdw; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lbue; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lbvm;->a(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lbvm;->a(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    goto :goto_1

    :catch_3
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lbvm;->a(Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    goto :goto_1

    :catch_4
    move-exception v0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    :catch_5
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lbvm;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    iget-object v0, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v0, v3}, Lbwd;->d(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbvm;->h:Lbwd;

    invoke-virtual {v1, v3}, Lbwd;->d(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()J
    .locals 2

    invoke-virtual {p0}, Lbvm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not clipped"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-wide v0, p0, Lbvm;->e:J

    return-wide v0
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lbvm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    invoke-virtual {v0}, Lbuw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbvm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, Lbvm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    invoke-virtual {v0}, Lbuw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v1, "PreparedSyncMore[%s%d feeds, clipTime=%s, %s]"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbvm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "running, "

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lbvm;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-wide v3, p0, Lbvm;->e:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lbvm;->c:Lcfc;

    iget-object v3, v3, Lcfc;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
