.class public final Lbrq;
.super Lbrp;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbrp;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    iget-object v0, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    const-string v1, "Invalid authorize access request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid authorize access request: app id is zero"

    invoke-static {v0, v1}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid authorize access request: no drive id"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrq;->b:Lbrc;

    iget-object v1, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->a()J

    move-result-wide v1

    iget-object v3, p0, Lbrq;->c:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lbrc;->a(JLcom/google/android/gms/drive/DriveId;)V

    iget-object v0, p0, Lbrq;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->a()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
