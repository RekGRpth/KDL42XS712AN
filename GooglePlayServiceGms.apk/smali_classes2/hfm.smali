.class final Lhfm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbpj;


# instance fields
.field final synthetic a:[Landroid/accounts/Account;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Landroid/os/Bundle;

.field final synthetic e:Lhfk;


# direct methods
.method constructor <init>(Lhfk;[Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lhfm;->e:Lhfk;

    iput-object p2, p0, Lhfm;->a:[Landroid/accounts/Account;

    iput p3, p0, Lhfm;->b:I

    iput-object p4, p0, Lhfm;->c:Ljava/lang/String;

    iput-object p5, p0, Lhfm;->d:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    iget-object v2, p0, Lhfm;->a:[Landroid/accounts/Account;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    iget-object v0, p0, Lhfm;->e:Lhfk;

    invoke-static {v0}, Lhfk;->c(Lhfk;)Lgsg;

    move-result-object v0

    iget v5, p0, Lhfm;->b:I

    iget-object v6, p0, Lhfm;->c:Ljava/lang/String;

    invoke-virtual {v0, v5, v4, v6}, Lgsg;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lgsh;

    move-result-object v0

    iget-object v5, p0, Lhfm;->e:Lhfk;

    invoke-static {v5}, Lhfk;->b(Lhfk;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lhfm;->c:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lgsh;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhfm;->e:Lhfk;

    iget v5, p0, Lhfm;->b:I

    iget-object v6, p0, Lhfm;->c:Ljava/lang/String;

    invoke-static {v0, v5, v6, v4}, Lhfk;->a(Lhfk;ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v0, "NetworkOwService"

    const-string v4, "Failure to retrieve auth token. Continuing."

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhfm;->d:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
