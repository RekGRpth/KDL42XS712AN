.class public final Lecn;
.super Leco;
.source "SourceFile"


# instance fields
.field private final f:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Leco;-><init>(Landroid/content/Context;Ldax;)V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lecn;->f:Landroid/os/Bundle;

    return-void
.end method

.method public static a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V
    .locals 4

    new-instance v0, Lecn;

    invoke-direct {v0, p0, p1, p2}, Lecn;-><init>(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    sget-object v1, Lecn;->a:Lecq;

    sget-object v2, Lecn;->a:Lecq;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lecq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lecq;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    iget-object v0, p0, Lecn;->e:Landroid/view/View;

    const v1, 0x7f0a0155    # com.google.android.gms.R.id.popup_text_data

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lecn;->f:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.games.extra.name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lecn;->e:Landroid/view/View;

    const v1, 0x7f0a0147    # com.google.android.gms.R.id.popup_text_label

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0293    # com.google.android.gms.R.string.games_achievement_popup_label

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lecn;->e:Landroid/view/View;

    const v1, 0x7f0a0145    # com.google.android.gms.R.id.popup_icon

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lecn;->f:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.games.extra.imageUri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lecn;->a(Landroid/widget/ImageView;Landroid/net/Uri;I)V

    return-void
.end method
