.class public final Lfjj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lfjk;


# direct methods
.method public constructor <init>(Lfjk;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfjj;->a:Lfjk;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lfew;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lfew;

    invoke-direct {v2}, Lfew;-><init>()V

    invoke-static {p0}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    iput v0, v2, Lfew;->a:I

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, v2, Lfew;->b:Z

    return-object v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a()Lfex;
    .locals 3

    new-instance v0, Lfex;

    invoke-direct {v0}, Lfex;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-static {v1}, Lbox;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfex;->a:Ljava/lang/String;

    invoke-static {}, Lbqe;->b()J

    move-result-wide v1

    iput-wide v1, v0, Lfex;->b:J

    invoke-static {}, Lbqe;->c()I

    move-result v1

    iput v1, v0, Lfex;->c:I

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;IIILjava/lang/String;IILjava/lang/Throwable;IJLjava/util/List;JLfeu;)V
    .locals 9

    sget-object v2, Lfdl;->f:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v3

    sget-object v2, Lfbd;->ad:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Lfet;

    invoke-direct {v4}, Lfet;-><init>()V

    const/4 v2, 0x0

    invoke-static {p1, p2, v2}, Lfjj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lfew;

    move-result-object v2

    iput-object v2, v4, Lfet;->a:Lfew;

    iput p3, v4, Lfet;->b:I

    invoke-static {p6}, Lfjj;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lfet;->e:I

    iput p4, v4, Lfet;->c:I

    iput p5, v4, Lfet;->d:I

    move/from16 v0, p7

    iput v0, v4, Lfet;->f:I

    move-wide/from16 v0, p14

    iput-wide v0, v4, Lfet;->m:J

    move-object/from16 v0, p16

    iput-object v0, v4, Lfet;->n:Lfeu;

    const/4 v2, 0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_3

    move/from16 v0, p8

    iput v0, v4, Lfet;->g:I

    if-eqz p9, :cond_2

    invoke-virtual/range {p9 .. p9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lfet;->h:Ljava/lang/String;

    sget-object v2, Lfbd;->af:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static/range {p9 .. p9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v2, ""

    :cond_1
    :goto_1
    iput-object v2, v4, Lfet;->i:Ljava/lang/String;

    :cond_2
    move/from16 v0, p10

    iput v0, v4, Lfet;->j:I

    move-wide/from16 v0, p11

    iput-wide v0, v4, Lfet;->k:J

    :cond_3
    if-eqz p13, :cond_6

    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v2

    new-array v5, v2, [Lfer;

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    array-length v2, v5

    if-ge v3, v2, :cond_5

    move-object/from16 v0, p13

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfid;

    new-instance v6, Lfer;

    invoke-direct {v6}, Lfer;-><init>()V

    iget-object v7, v2, Lfid;->a:Ljava/lang/String;

    iget-object v8, v2, Lfid;->b:Ljava/lang/String;

    invoke-static {p1, v7, v8}, Lfjj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lfew;

    move-result-object v7

    iput-object v7, v6, Lfer;->a:Lfew;

    iget v7, v2, Lfid;->c:I

    iput v7, v6, Lfer;->b:I

    iget v7, v2, Lfid;->d:I

    iput v7, v6, Lfer;->c:I

    iget v7, v2, Lfid;->e:I

    iput v7, v6, Lfer;->d:I

    iget-boolean v7, v2, Lfid;->f:Z

    iput-boolean v7, v6, Lfer;->e:Z

    iget v7, v2, Lfid;->g:I

    iput v7, v6, Lfer;->f:I

    iget v7, v2, Lfid;->h:I

    iput v7, v6, Lfer;->g:I

    iget v7, v2, Lfid;->i:I

    iput v7, v6, Lfer;->h:I

    iget-boolean v7, v2, Lfid;->j:Z

    iput-boolean v7, v6, Lfer;->i:Z

    iget v7, v2, Lfid;->k:I

    iput v7, v6, Lfer;->j:I

    iget v7, v2, Lfid;->l:I

    iput v7, v6, Lfer;->k:I

    iget v7, v2, Lfid;->m:I

    iput v7, v6, Lfer;->l:I

    iget-boolean v7, v2, Lfid;->n:Z

    iput-boolean v7, v6, Lfer;->m:Z

    iget v7, v2, Lfid;->o:I

    iput v7, v6, Lfer;->n:I

    iget v7, v2, Lfid;->p:I

    iput v7, v6, Lfer;->o:I

    iget v7, v2, Lfid;->q:I

    iput v7, v6, Lfer;->p:I

    iget-boolean v7, v2, Lfid;->r:Z

    iput-boolean v7, v6, Lfer;->q:Z

    iget v7, v2, Lfid;->s:I

    iput v7, v6, Lfer;->r:I

    iget v7, v2, Lfid;->t:I

    iput v7, v6, Lfer;->s:I

    iget v7, v2, Lfid;->v:I

    iput v7, v6, Lfer;->t:I

    iget-boolean v7, v2, Lfid;->w:Z

    iput-boolean v7, v6, Lfer;->u:Z

    iget v7, v2, Lfid;->x:I

    iput v7, v6, Lfer;->v:I

    iget v7, v2, Lfid;->y:I

    iput v7, v6, Lfer;->w:I

    iget v7, v2, Lfid;->z:I

    iput v7, v6, Lfer;->x:I

    iget v7, v2, Lfid;->A:I

    iput v7, v6, Lfer;->y:I

    iget v7, v2, Lfid;->B:I

    iput v7, v6, Lfer;->z:I

    iget v7, v2, Lfid;->C:I

    iput v7, v6, Lfer;->A:I

    iget-wide v7, v2, Lfid;->D:J

    iput-wide v7, v6, Lfer;->B:J

    aput-object v6, v5, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_2

    :cond_4
    const-string v5, "^.*?\\s+"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "foreign key constraint failed.*?\\s+at\\s+"

    const-string v6, "FK "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, " at "

    const-string v6, " "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "People"

    const-string v6, "P"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "SQLite"

    const-string v6, "S"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\(Native Method\\)"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "SourceFile\\:"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\b(?:android|com|org)\\.(?:[a-z0-9_]+\\.)*"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_5
    iput-object v5, v4, Lfet;->l:[Lfer;

    :cond_6
    new-instance v2, Lfeq;

    invoke-direct {v2}, Lfeq;-><init>()V

    invoke-static {}, Lfjj;->a()Lfex;

    move-result-object v3

    iput-object v3, v2, Lfeq;->a:Lfex;

    iput-object v4, v2, Lfeq;->c:Lfet;

    const-string v3, "sync_result"

    invoke-virtual {p0, v3, v2}, Lfjj;->a(Ljava/lang/String;Lfeq;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Lfeq;)V
    .locals 6

    iget-object v0, p0, Lfjj;->a:Lfjk;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    invoke-static {p2}, Lizs;->a(Lizs;)[B

    move-result-object v0

    const-string v3, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PeopleAnalytics"

    const-string v4, "Payload:"

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PeopleAnalytics"

    invoke-virtual {p2}, Lfeq;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PeopleAnalytics"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MIME: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v3, Lfko;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v4

    const/16 v5, 0x10

    invoke-direct {v3, v4, v5}, Lfko;-><init>(Landroid/content/Context;I)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, p1, v0, v4}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v3}, Lfko;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method
