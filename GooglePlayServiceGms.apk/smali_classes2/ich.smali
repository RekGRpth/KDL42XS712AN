.class public final Lich;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/network/NetworkLocationProvider;

.field private b:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/android/location/network/NetworkLocationProvider;)V
    .locals 1

    iput-object p1, p0, Lich;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lich;->b:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    const-wide/16 v1, 0x4e20

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, "GmsNetworkLocationProvi"

    const-string v1, "ENABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const-string v0, "GmsNetworkLocationProvi"

    const-string v1, "DISABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "GmsNetworkLocationProvi"

    const-string v3, "SET-REQUEST"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lici;

    iget-object v5, v0, Lici;->a:Lcom/android/location/provider/ProviderRequestUnbundled;

    invoke-virtual {v5}, Lcom/android/location/provider/ProviderRequestUnbundled;->getInterval()J

    move-result-wide v3

    cmp-long v6, v3, v1

    if-gez v6, :cond_2

    :goto_1
    const-string v3, "GmsNetworkLocationProvi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "in Handler: ProviderRequestUnbundled, reportLocation is "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/android/location/provider/ProviderRequestUnbundled;->getReportLocation()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and interval is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lich;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-static {v3}, Lcom/google/android/location/network/NetworkLocationProvider;->a(Lcom/google/android/location/network/NetworkLocationProvider;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lich;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-static {v6}, Lcom/google/android/location/network/NetworkLocationProvider;->a(Lcom/google/android/location/network/NetworkLocationProvider;)Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/location/network/NetworkLocationService;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v6, 0x8000000

    invoke-static {v3, v4, v5, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    sget-object v5, Lhpi;->b:Lhpi;

    invoke-static {v5}, Lhpg;->a(Lhpi;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lich;->b:Landroid/content/Intent;

    iget-object v4, p0, Lich;->b:Landroid/content/Intent;

    iget-object v5, p0, Lich;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-static {v5}, Lcom/google/android/location/network/NetworkLocationProvider;->a(Lcom/google/android/location/network/NetworkLocationProvider;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lich;->b:Landroid/content/Intent;

    const-string v5, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v4, p0, Lich;->b:Landroid/content/Intent;

    const-string v5, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    invoke-virtual {v4, v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x13

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lici;->b:Landroid/os/WorkSource;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lich;->b:Landroid/content/Intent;

    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    iget-object v0, v0, Lici;->b:Landroid/os/WorkSource;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lich;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-static {v0}, Lcom/google/android/location/network/NetworkLocationProvider;->a(Lcom/google/android/location/network/NetworkLocationProvider;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lich;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GmsNetworkLocationProvi"

    const-string v1, "startService returned NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/app/PendingIntent;->cancel()V

    goto/16 :goto_0

    :cond_2
    move-wide v1, v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
