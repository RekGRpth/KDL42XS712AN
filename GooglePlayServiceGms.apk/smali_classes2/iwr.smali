.class public final Liwr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liws;


# instance fields
.field final synthetic a:Liwp;

.field private final b:Liwt;

.field private final c:Liwv;


# direct methods
.method public constructor <init>(Liwp;)V
    .locals 2

    iput-object p1, p0, Liwr;->a:Liwp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Liwt;

    iget-object v1, p0, Liwr;->a:Liwp;

    invoke-direct {v0, v1}, Liwt;-><init>(Liwp;)V

    iput-object v0, p0, Liwr;->b:Liwt;

    new-instance v0, Liwv;

    iget-object v1, p0, Liwr;->a:Liwp;

    invoke-direct {v0, v1}, Liwv;-><init>(Liwp;)V

    iput-object v0, p0, Liwr;->c:Liwv;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a(J)Liws;
    .locals 6

    const-wide v4, 0x37e11d600L

    iget-object v0, p0, Liwr;->c:Liwv;

    iget v0, v0, Liwv;->a:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    invoke-virtual {v0}, Liwg;->c()D

    move-result-wide v0

    iget-object v2, p0, Liwr;->a:Liwp;

    iget-object v2, v2, Liwp;->b:Lixa;

    iget-object v2, v2, Lixa;->b:Liwm;

    invoke-virtual {v2}, Liwm;->e()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Liwr;->b:Liwt;

    iget-wide v0, v0, Liwt;->b:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    new-instance v0, Liwt;

    iget-object v1, p0, Liwr;->a:Liwp;

    invoke-direct {v0, v1}, Liwt;-><init>(Liwp;)V

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Liwr;->b:Liwt;

    iget v0, v0, Liwt;->a:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    iget-object v0, p0, Liwr;->c:Liwv;

    iget-wide v0, v0, Liwv;->b:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    new-instance v0, Liwv;

    iget-object v1, p0, Liwr;->a:Liwp;

    invoke-direct {v0, v1}, Liwv;-><init>(Liwp;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(JLiwe;)V
    .locals 1

    iget-object v0, p0, Liwr;->c:Liwv;

    invoke-virtual {v0, p1, p2, p3}, Liwv;->a(JLiwe;)V

    iget-object v0, p0, Liwr;->b:Liwt;

    invoke-virtual {v0, p1, p2, p3}, Liwt;->a(JLiwe;)V

    return-void
.end method

.method public final a(Liwz;)V
    .locals 0

    return-void
.end method

.method public final b()Liwy;
    .locals 11

    const-wide v9, 0x37e11d600L

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    iget-object v0, v0, Liwg;->a:Liwe;

    iget-object v1, p0, Liwr;->a:Liwp;

    iget-object v1, v1, Liwp;->b:Lixa;

    invoke-virtual {v1}, Lixa;->a()Liwe;

    move-result-object v1

    iget-object v2, p0, Liwr;->a:Liwp;

    iget-object v2, v2, Liwp;->a:Liwm;

    invoke-virtual {v2}, Liwm;->a()Liwe;

    move-result-object v2

    if-eqz v2, :cond_2

    iget v3, v2, Liwe;->e:I

    int-to-double v3, v3

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v3, v5

    iget-object v5, p0, Liwr;->a:Liwp;

    iget-wide v5, v5, Liwp;->h:J

    iget-object v7, p0, Liwr;->a:Liwp;

    iget-wide v7, v7, Liwp;->e:J

    add-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    iget-object v5, p0, Liwr;->a:Liwp;

    iget-wide v5, v5, Liwp;->h:J

    iget-object v7, p0, Liwr;->a:Liwp;

    iget-wide v7, v7, Liwp;->g:J

    add-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Liwe;->a(Liwe;)D

    move-result-wide v5

    cmpl-double v5, v5, v3

    if-lez v5, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Liwe;->a(Liwe;)D

    move-result-wide v5

    cmpl-double v2, v5, v3

    if-lez v2, :cond_2

    :cond_1
    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->a:Liwm;

    :goto_0
    return-object v0

    :cond_2
    if-nez v1, :cond_3

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    goto :goto_0

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->b:Lixa;

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liwr;->a:Liwp;

    iget-wide v2, v2, Liwp;->g:J

    iget-object v4, p0, Liwr;->a:Liwp;

    iget-wide v4, v4, Liwp;->e:J

    add-long/2addr v4, v9

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->b:Lixa;

    goto :goto_0

    :cond_5
    iget-object v2, p0, Liwr;->a:Liwp;

    iget-wide v2, v2, Liwp;->e:J

    iget-object v4, p0, Liwr;->a:Liwp;

    iget-wide v4, v4, Liwp;->g:J

    add-long/2addr v4, v9

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    goto :goto_0

    :cond_6
    iget v0, v0, Liwe;->e:I

    iget v1, v1, Liwe;->e:I

    if-ge v0, v1, :cond_7

    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    goto :goto_0

    :cond_7
    iget-object v0, p0, Liwr;->a:Liwp;

    iget-object v0, v0, Liwp;->b:Lixa;

    goto :goto_0
.end method
