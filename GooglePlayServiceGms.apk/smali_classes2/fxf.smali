.class public final Lfxf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Landroid/graphics/Bitmap;

.field public static b:Landroid/graphics/Bitmap;

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:Landroid/graphics/Bitmap;

.field public static g:I

.field public static h:I

.field public static i:Landroid/graphics/PointF;

.field public static j:Landroid/graphics/Paint;

.field public static k:Landroid/graphics/Paint;

.field private static p:Z

.field private static q:I

.field private static r:Landroid/graphics/Bitmap;


# instance fields
.field public l:Landroid/content/Context;

.field public m:Lfxg;

.field public n:Z

.field public o:Landroid/graphics/Bitmap;


# direct methods
.method private static a(F)Landroid/graphics/ColorMatrixColorFilter;
    .locals 2

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    invoke-virtual {v0, p0}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    const/4 v3, 0x1

    sget-boolean v0, Lfxf;->p:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sput-boolean v3, Lfxf;->p:Z

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lfxf;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    sget-object v0, Lfxf;->j:Landroid/graphics/Paint;

    const v2, 0x3f7d70a4    # 0.99f

    invoke-static {v2}, Lfxf;->a(F)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lfxf;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    sget-object v0, Lfxf;->k:Landroid/graphics/Paint;

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    sget-object v0, Lfxf;->k:Landroid/graphics/Paint;

    const v2, 0x3e4ccccd    # 0.2f

    invoke-static {v2}, Lfxf;->a(F)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    const v0, 0x7f0d01da    # com.google.android.gms.R.dimen.avatar_dimension

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxf;->c:I

    const v0, 0x7f0200bf    # com.google.android.gms.R.drawable.default_avatar

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sget v2, Lfxf;->c:I

    invoke-static {v0, v2}, Lbpc;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxf;->f:Landroid/graphics/Bitmap;

    const v0, 0x7f0201e3    # com.google.android.gms.R.drawable.ov_bubble_square

    invoke-static {v1, v0}, Lbpc;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v2, Lfxf;->c:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    sput v0, Lfxf;->d:I

    const v0, 0x7f0201e2    # com.google.android.gms.R.drawable.ov_bubble_circle

    invoke-static {v1, v0}, Lbpc;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxf;->b:Landroid/graphics/Bitmap;

    const v0, 0x7f0d01db    # com.google.android.gms.R.dimen.friend_locations_marker_badge_top_overhang

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lfxf;->e:I

    const v0, 0x7f0d01dc    # com.google.android.gms.R.dimen.friend_locations_marker_badge_right_overhang

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lfxf;->q:I

    const v0, 0x7f0d01dd    # com.google.android.gms.R.dimen.friend_locations_marker_background_bottom_padding

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const v2, 0x7f0d01de    # com.google.android.gms.R.dimen.friend_locations_marker_dot_bottom_padding

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f0201e4    # com.google.android.gms.R.drawable.ov_dot_red

    invoke-static {v1, v3}, Lbpc;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lfxf;->r:Landroid/graphics/Bitmap;

    sget-object v1, Lfxf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget v3, Lfxf;->e:I

    add-int/2addr v1, v3

    sub-int v0, v1, v0

    sget-object v3, Lfxf;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v2, v3, v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    sget-object v3, Lfxf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget v4, Lfxf;->q:I

    add-int/2addr v3, v4

    sput v3, Lfxf;->g:I

    sget-object v3, Lfxf;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sput v1, Lfxf;->h:I

    sget-object v1, Lfxf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    int-to-float v0, v0

    new-instance v2, Landroid/graphics/PointF;

    sget v3, Lfxf;->g:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    sget v3, Lfxf;->h:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v2, Lfxf;->i:Landroid/graphics/PointF;

    goto/16 :goto_0
.end method
