.class public final Liln;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldh;

.field public final b:I

.field public final c:Lhpk;


# direct methods
.method public constructor <init>(Ljava/io/File;Lhpk;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Liln;->b:I

    iput-object p2, p0, Liln;->c:Lhpk;

    new-instance v0, Ldh;

    invoke-direct {v0, p1}, Ldh;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Liln;->a:Ldh;

    return-void
.end method

.method private a(Ljava/io/InputStream;Lizk;)Lizk;
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Liln;->c:Lhpk;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No cipher specified."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iget v3, p0, Liln;->b:I

    if-eq v0, v3, :cond_1

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Invalid version, desired = %d, actual = %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Liln;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v2}, Lilv;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Liln;->c:Lhpk;

    invoke-virtual {v0, v1}, Lhpk;->a(Ljava/io/DataInputStream;)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, [B

    array-length v3, v0

    invoke-virtual {p2, v0, v3}, Lizk;->a([BI)Lizk;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2}, Lilv;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    return-object p2

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lizk;)Lizk;
    .locals 3

    iget-object v0, p0, Liln;->a:Ldh;

    iget-object v1, v0, Ldh;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldh;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    iget-object v1, v0, Ldh;->b:Ljava/io/File;

    iget-object v2, v0, Ldh;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, v0, Ldh;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, v1, p1}, Liln;->a(Ljava/io/InputStream;Lizk;)Lizk;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lizk;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Liln;->a:Ldh;

    invoke-virtual {v1}, Ldh;->a()Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    :try_start_1
    iget-object v0, p0, Liln;->c:Lhpk;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v2, "No cipher specified."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Liln;->a:Ldh;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ldh;->a(Ljava/io/FileOutputStream;)Z

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    iget-object v0, v1, Ldh;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iget-object v0, v1, Ldh;->b:Ljava/io/File;

    iget-object v1, v1, Ldh;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget v2, p0, Liln;->b:I

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p1}, Lizk;->d()[B

    move-result-object v2

    iget-object v3, p0, Liln;->c:Lhpk;

    invoke-virtual {v3, v0, v2}, Lhpk;->a(Ljava/io/DataOutputStream;[B)V

    iget-object v0, p0, Liln;->a:Ldh;

    if-eqz v1, :cond_0

    invoke-static {v1}, Ldh;->a(Ljava/io/FileOutputStream;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    iget-object v0, v0, Ldh;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_5
    const-string v2, "AtomicFile"

    const-string v3, "finishWrite: Got exception:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "AtomicFile"

    const-string v2, "failWrite: Got exception:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method
