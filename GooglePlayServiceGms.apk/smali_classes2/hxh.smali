.class public final Lhxh;
.super Lhxe;
.source "SourceFile"


# instance fields
.field private final a:Lhwx;

.field private final b:Lhux;


# direct methods
.method public constructor <init>(Lhwx;Lhux;)V
    .locals 0

    invoke-direct {p0}, Lhxe;-><init>()V

    iput-object p1, p0, Lhxh;->a:Lhwx;

    iput-object p2, p0, Lhxh;->b:Lhux;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    const/4 v2, 0x3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhxh;->a:Lhwx;

    invoke-virtual {v0}, Lhwx;->a()V

    iget-object v0, p0, Lhxh;->b:Lhux;

    invoke-virtual {v0}, Lhux;->a()V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Step detection enabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhxh;->a:Lhwx;

    invoke-virtual {v0}, Lhwx;->b()V

    iget-object v0, p0, Lhxh;->b:Lhux;

    invoke-virtual {v0}, Lhux;->b()V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Step Detection Disabled"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Step detector ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhxh;->a(Ljava/lang/StringBuilder;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
