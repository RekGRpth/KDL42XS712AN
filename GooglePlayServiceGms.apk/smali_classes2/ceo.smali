.class public final enum Lceo;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lceo;

.field public static final enum b:Lceo;

.field public static final enum c:Lceo;

.field public static final enum d:Lceo;

.field public static final enum e:Lceo;

.field public static final enum f:Lceo;

.field private static final synthetic h:[Lceo;


# instance fields
.field private final g:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v0, Lceo;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "accountId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->a:Lceo;

    new-instance v0, Lceo;

    const-string v1, "FEED_TYPE"

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "feedType"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    sget-object v4, Lbux;->c:Lbux;

    invoke-virtual {v4}, Lbux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->b:Lceo;

    new-instance v0, Lceo;

    const-string v1, "FEED_PARAMETERS"

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "feedParameters"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->c:Lceo;

    new-instance v0, Lceo;

    const-string v1, "NEXT_PAGE_TOKEN"

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "nextPageToken"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->d:Lceo;

    new-instance v0, Lceo;

    const-string v1, "CLIP_TIME"

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "clipTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->e:Lceo;

    new-instance v0, Lceo;

    const-string v1, "NUM_PAGES_RETRIEVED"

    const/4 v2, 0x5

    invoke-static {}, Lcen;->d()Lcen;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "numPagesRetrieved"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceo;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceo;->f:Lceo;

    const/4 v0, 0x6

    new-array v0, v0, [Lceo;

    sget-object v1, Lceo;->a:Lceo;

    aput-object v1, v0, v8

    sget-object v1, Lceo;->b:Lceo;

    aput-object v1, v0, v7

    sget-object v1, Lceo;->c:Lceo;

    aput-object v1, v0, v9

    sget-object v1, Lceo;->d:Lceo;

    aput-object v1, v0, v10

    sget-object v1, Lceo;->e:Lceo;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lceo;->f:Lceo;

    aput-object v2, v0, v1

    sput-object v0, Lceo;->h:[Lceo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lceo;->g:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lceo;
    .locals 1

    const-class v0, Lceo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lceo;

    return-object v0
.end method

.method public static values()[Lceo;
    .locals 1

    sget-object v0, Lceo;->h:[Lceo;

    invoke-virtual {v0}, [Lceo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lceo;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lceo;->g:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lceo;->g:Lcdp;

    return-object v0
.end method
