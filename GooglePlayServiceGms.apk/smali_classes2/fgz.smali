.class public final Lfgz;
.super Lfcd;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Lffd;

.field private final g:Lffd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lffd;Lffd;)V
    .locals 1

    invoke-direct {p0}, Lfcd;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfgz;->a:Landroid/content/Context;

    iput-object p2, p0, Lfgz;->b:Ljava/lang/String;

    iput-object p3, p0, Lfgz;->c:Ljava/lang/String;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    iput v0, p0, Lfgz;->d:I

    iput-object p4, p0, Lfgz;->e:Ljava/lang/String;

    iput-object p5, p0, Lfgz;->f:Lffd;

    iput-object p6, p0, Lfgz;->g:Lffd;

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static c()V
    .locals 2

    invoke-static {}, Lfiu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Contacts sync not supported on this platform."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 5

    const-string v0, "rawContactUri"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v4, Lfgc;

    invoke-direct {v4, v1, v2, v3, p1}, Lfgc;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/net/Uri;)V

    invoke-interface {v0, v1, v4}, Lffd;->a(Landroid/content/Context;Lffb;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lfbz;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 3

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v1

    if-eqz p2, :cond_1

    if-eqz p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "scopes"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-virtual {v1, p1, p3, p4, p5}, Lffh;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_1
    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v1, p1}, Lffh;->a(Lfbz;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    invoke-direct {p0}, Lfgz;->b()V

    invoke-virtual {p0, p1, p2}, Lfgz;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lfgz;->a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lfgz;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
    .locals 13

    invoke-direct {p0}, Lfgz;->b()V

    const-string v0, "account"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Unknown account"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v12, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    iget-object v10, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v0, Lfgb;

    move-object v4, p1

    move/from16 v5, p5

    move-object v6, p1

    move-object v7, p2

    move-wide/from16 v8, p3

    move/from16 v11, p6

    invoke-direct/range {v0 .. v11}, Lfgb;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    invoke-interface {v12, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfbz;JZ)V
    .locals 8

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfgz;->g:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    move-object v4, p1

    move-wide v5, p2

    move v7, p4

    invoke-static/range {v0 .. v7}, Lffl;->a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;JZ)V

    return-void
.end method

.method public final a(Lfbz;Landroid/os/Bundle;)V
    .locals 5

    iget-object v0, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v4, Lfgh;

    invoke-direct {v4, v1, v2, v3, p1}, Lfgh;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;)V

    invoke-interface {v0, v1, v4}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;)V
    .locals 6

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "url"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lfgz;->g:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lffl;->a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;II)V
    .locals 8

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "avatarUrl"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "avatarSize"

    invoke-static {p3, v0}, Lfbb;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lfgz;->g:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lffl;->a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;II)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v0, Lfgm;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lfgm;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    if-ltz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lfgz;->g:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lffl;->a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "avatarSize"

    invoke-static {p4, v0}, Lfbb;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lfgz;->g:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    invoke-static/range {v0 .. v8}, Lffl;->a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Lfgz;->b()V

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 14

    invoke-direct {p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "uri"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v13, p0, Lfgz;->f:Lffd;

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v4, p0, Lfgz;->b:Ljava/lang/String;

    iget v5, p0, Lfgz;->d:I

    iget-object v6, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lfgf;

    move-object v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p3

    move-object/from16 v11, p2

    move/from16 v12, p5

    invoke-direct/range {v1 .. v12}, Lfgf;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v13, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    invoke-direct {p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v14, p0, Lfgz;->f:Lffd;

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v4, p0, Lfgz;->b:Ljava/lang/String;

    iget-object v10, p0, Lfgz;->c:Ljava/lang/String;

    iget v5, p0, Lfgz;->d:I

    iget-object v6, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lffw;

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object v11, v4

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    invoke-direct/range {v1 .. v13}, Lffw;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v14, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget-object v3, p0, Lfgz;->e:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    const/4 v11, 0x0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    invoke-static/range {v0 .. v11}, Lffu;->a(Lffd;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 12

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget-object v3, p0, Lfgz;->e:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    invoke-static/range {v0 .. v11}, Lffu;->a(Lffd;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
    .locals 11

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
    .locals 17

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lfgz;->f:Lffd;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lfgz;->d:I

    new-instance v1, Lfgl;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p8

    move-object/from16 v14, p9

    move/from16 v15, p10

    invoke-direct/range {v1 .. v15}, Lfgl;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16

    invoke-direct/range {p0 .. p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "circleName"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lfgz;->f:Lffd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lfgz;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lfgz;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lfgp;

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object v12, v4

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    invoke-direct/range {v1 .. v14}, Lfgp;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    if-nez p5, :cond_0

    if-nez p6, :cond_0

    if-eqz p7, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "Nothing is changing"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lfgz;->f:Lffd;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lfgz;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lfgz;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lfgz;->e:Ljava/lang/String;

    if-nez p6, :cond_2

    const/4 v10, 0x0

    :goto_1
    new-instance v1, Lffx;

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move-object/from16 v11, p7

    move-object/from16 v12, p4

    move-object v14, v4

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    invoke-direct/range {v1 .. v16}, Lffx;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    goto :goto_2
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 14

    invoke-direct {p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "qualifiedPersonIds"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p5, :cond_1

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Duplicate qualified person ID"

    invoke-static {v2, v5}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    const-string v2, "No qualified person IDs"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v13, p0, Lfgz;->f:Lffd;

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v4, p0, Lfgz;->b:Ljava/lang/String;

    iget v5, p0, Lfgz;->d:I

    iget-object v6, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lfgo;

    move-object v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    invoke-direct/range {v1 .. v12}, Lfgo;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
    .locals 12

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    invoke-virtual/range {v0 .. v11}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
    .locals 20

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move/from16 v0, p6

    and-int/lit16 v1, v0, 0x3fff

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "projection"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p11, :cond_2

    const/4 v1, 0x1

    :goto_1
    const-string v2, "searchFields"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lfgz;->f:Lffd;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lfgz;->d:I

    new-instance v1, Lfgj;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    move-wide/from16 v13, p8

    move-object/from16 v15, p10

    move/from16 v16, p11

    invoke-direct/range {v1 .. v18}, Lfgj;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;IIZ)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 8

    invoke-direct {p0}, Lfgz;->b()V

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "qualifiedId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p5, :cond_1

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Duplicate circle ID"

    invoke-static {v2, v5}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    if-eqz p6, :cond_3

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_3
    const-string v5, "Duplicate circle ID"

    invoke-static {v2, v5}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_4

    const/4 v1, 0x1

    :goto_4
    const-string v2, "No circle IDs"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lfgz;->f:Lffd;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lfgz;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lfgz;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lfgn;

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object v13, v4

    move-object/from16 v14, p2

    move-object/from16 v15, p7

    move-object/from16 v16, p3

    invoke-direct/range {v1 .. v16}, Lfgn;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_4
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 16

    invoke-direct/range {p0 .. p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "gaiaId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lfgz;->f:Lffd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfgz;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfgz;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lfgz;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lfgz;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lfgz;->e:Ljava/lang/String;

    new-instance v1, Lfga;

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move/from16 v9, p5

    move-object/from16 v10, p4

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object v14, v4

    invoke-direct/range {v1 .. v14}, Lfga;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
    .locals 10

    const/4 v5, 0x7

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v9}, Lfgz;->a(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V

    return-void
.end method

.method public final a(Lfbz;Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 10

    invoke-static {}, Lfjs;->a()Lfjs;

    move-result-object v0

    iget-object v1, p0, Lfgz;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfjs;->a(Ljava/lang/String;)V

    invoke-static {}, Lfgz;->c()V

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v9, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v0, Lfgd;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p2

    move v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lfgd;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)V

    invoke-interface {v9, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final a(Lfbz;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lfgz;->a(Lfbz;ZZLjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Lfbz;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 13

    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p6, :cond_0

    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lbkm;->b(Z)V

    if-eqz p2, :cond_1

    const-string v1, "account"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    :cond_1
    iget-object v12, p0, Lfgz;->f:Lffd;

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v3, p0, Lfgz;->b:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    new-instance v1, Lffv;

    move-object v5, p1

    move-object/from16 v6, p4

    move v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v1 .. v11}, Lffv;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v12, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    invoke-static {}, Lfjs;->a()Lfjs;

    move-result-object v0

    iget-object v1, p0, Lfgz;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfjs;->a(Ljava/lang/String;)V

    invoke-static {}, Lfgz;->c()V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    invoke-virtual {v0}, Lfbe;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, p1, :cond_1

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfbe;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v4, Lfge;

    invoke-direct {v4, v1, v2, v3}, Lfge;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v4}, Lffd;->a(Landroid/content/Context;Lffb;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a()Z
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Lfgz;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    invoke-virtual {v0}, Lfbe;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lfgz;->a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lfbz;Landroid/os/Bundle;)V
    .locals 7

    iget-object v6, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v2, p0, Lfgz;->b:Ljava/lang/String;

    iget v3, p0, Lfgz;->d:I

    new-instance v0, Lfgi;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lfgi;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Landroid/os/Bundle;)V

    invoke-interface {v6, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final b(Lfbz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    invoke-direct {p0}, Lfgz;->b()V

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v8, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v3, p0, Lfgz;->b:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    iget-object v5, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v0, Lffy;

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lffy;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final b(Lfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 14

    invoke-direct {p0}, Lfgz;->b()V

    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "query"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v13, p0, Lfgz;->f:Lffd;

    iget-object v2, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v3, p0, Lfgz;->b:Ljava/lang/String;

    iget-object v8, p0, Lfgz;->e:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    new-instance v1, Lfgk;

    move-object v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p2

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p3

    move-object/from16 v12, p6

    invoke-direct/range {v1 .. v12}, Lfgk;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13, v2, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public final c(Lfbz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    invoke-direct {p0}, Lfgz;->b()V

    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v8, p0, Lfgz;->f:Lffd;

    iget-object v1, p0, Lfgz;->a:Landroid/content/Context;

    iget-object v3, p0, Lfgz;->b:Ljava/lang/String;

    iget v4, p0, Lfgz;->d:I

    iget-object v5, p0, Lfgz;->e:Ljava/lang/String;

    new-instance v0, Lffz;

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lffz;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method
