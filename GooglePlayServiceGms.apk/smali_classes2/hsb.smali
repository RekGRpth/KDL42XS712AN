.class final Lhsb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field final synthetic a:Lhsa;


# direct methods
.method constructor <init>(Lhsa;)V
    .locals 0

    iput-object p1, p0, Lhsb;->a:Lhsa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a([FI)F
    .locals 1

    array-length v0, p0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    :goto_0
    return v0

    :cond_1
    aget v0, p0, p1

    goto :goto_0
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 14

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    iget-object v0, p0, Lhsb;->a:Lhsa;

    invoke-virtual {v0}, Lhsa;->f()V

    iget-object v0, p0, Lhsb;->a:Lhsa;

    invoke-virtual {v0}, Lhsa;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->c:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrz;

    if-nez v0, :cond_4

    sget-object v0, Lhrz;->k:Lhrz;

    move-object v13, v0

    :goto_1
    iget-object v0, p0, Lhsb;->a:Lhsa;

    iget-object v0, v0, Lhsa;->a:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhsb;->a:Lhsa;

    iget-object v0, v0, Lhsa;->a:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v0, v13}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a(Lhrz;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/16 v0, 0xe

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lhsb;->a:Lhsa;

    iget-object v0, v0, Lhrx;->d:Lhqm;

    aget v2, v7, v4

    aget v3, v7, v6

    aget v4, v7, v8

    const/4 v5, 0x3

    aget v5, v7, v5

    const/4 v6, 0x4

    aget v6, v7, v6

    const/4 v8, 0x5

    aget v7, v7, v8

    iget v8, p1, Landroid/hardware/SensorEvent;->accuracy:I

    iget-wide v9, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-virtual/range {v0 .. v12}, Lhqm;->a(IFFFFFFIJJ)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lhsb;->a:Lhsa;

    invoke-virtual {v0, v13, v11, v12}, Lhsa;->b(Lhrz;J)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhsb;->a:Lhsa;

    iget-object v3, v0, Lhrx;->d:Lhqm;

    invoke-static {v7, v4}, Lhsb;->a([FI)F

    move-result v5

    invoke-static {v7, v6}, Lhsb;->a([FI)F

    move-result v6

    invoke-static {v7, v8}, Lhsb;->a([FI)F

    move-result v7

    iget v8, p1, Landroid/hardware/SensorEvent;->accuracy:I

    iget-wide v9, p1, Landroid/hardware/SensorEvent;->timestamp:J

    move v4, v1

    invoke-virtual/range {v3 .. v12}, Lhqm;->a(IFFFIJJ)V

    goto :goto_2

    :cond_4
    move-object v13, v0

    goto :goto_1
.end method
