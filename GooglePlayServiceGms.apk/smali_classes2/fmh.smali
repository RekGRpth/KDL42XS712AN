.class public final Lfmh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/SharedPreferences;

.field b:Ljava/lang/Long;

.field c:Ljava/lang/Long;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "PlayLogUploaderPrefs"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lfmh;->a:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method final a()J
    .locals 4

    iget-object v0, p0, Lfmh;->b:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmh;->a:Landroid/content/SharedPreferences;

    const-string v1, "a"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lfmh;->b:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lfmh;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method final b()J
    .locals 4

    iget-object v0, p0, Lfmh;->c:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmh;->a:Landroid/content/SharedPreferences;

    const-string v1, "b"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lfmh;->c:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lfmh;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
