.class public abstract Lfye;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field protected a:Z

.field protected b:Lgfm;

.field protected c:Lfyf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-boolean p2, p0, Lfye;->a:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lfye;->a()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method static a(Landroid/content/Context;ILgfm;Z)Lfye;
    .locals 8

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0}, Lwe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lwe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-interface {p2}, Lgfm;->u()Ljava/lang/String;

    move-result-object v4

    const-string v1, "button"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lfyj;

    invoke-direct {v0, p0, p3}, Lfyj;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_d

    add-int/lit16 v0, p1, 0x1388

    invoke-virtual {v1, v0}, Lfye;->setId(I)V

    :cond_0
    :goto_1
    sget-object v0, Lfsr;->O:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FieldView"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FieldView"

    const-string v4, "%s from %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v3

    aput-object p2, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1

    :cond_2
    const-string v1, "check"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewCheck;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewCheck;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    const-string v1, "birthday"

    invoke-interface {p2}, Lgfm;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    if-eqz v1, :cond_5

    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    if-eqz p2, :cond_7

    const-string v1, "date"

    invoke-interface {p2}, Lgfm;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "dayInYear"

    invoke-interface {p2}, Lgfm;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    invoke-interface {p2}, Lgfm;->p()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_3
    if-eqz v1, :cond_8

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Mandatory non-birthday date fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v1, v3

    goto :goto_3

    :cond_8
    const-string v1, "info"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v0, Lfyk;

    invoke-direct {v0, p0, p3}, Lfyk;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_9
    invoke-static {p2}, Lfzd;->a(Lgfm;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewName;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewName;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_a
    const-string v1, "option"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "gender"

    invoke-interface {p2}, Lgfm;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewGender;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewGender;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_b
    invoke-interface {p2}, Lgfm;->p()Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Mandatory non-gender option fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    const-string v1, "string"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewString;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewString;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_d
    const-string v0, "FieldView"

    const/4 v5, 0x5

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "FieldView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported field: type="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lgfm;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_e
    move-object v1, v0

    goto/16 :goto_0
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lfye;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfm;Lfyf;)V
    .locals 0

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lfye;->b:Lgfm;

    iput-object p2, p0, Lfye;->c:Lfyf;

    return-void
.end method

.method public abstract b()Z
.end method

.method public abstract c()Lgfm;
.end method

.method protected final d()Z
    .locals 1

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 1

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lfye;->d()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lfye;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method protected g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final h()Z
    .locals 1

    invoke-virtual {p0}, Lfye;->g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final i()Lgfn;
    .locals 3

    new-instance v0, Lgfn;

    invoke-direct {v0}, Lgfn;-><init>()V

    iget-object v1, p0, Lfye;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgfn;->a:Ljava/lang/String;

    iget-object v1, v0, Lgfn;->d:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lfye;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->u()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgfn;->b:Ljava/lang/String;

    iget-object v1, v0, Lgfn;->d:Ljava/util/Set;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lfye;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->y()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgfn;->c:Ljava/lang/String;

    iget-object v1, v0, Lgfn;->d:Ljava/util/Set;

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected final j()Ljava/lang/CharSequence;
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->m()Lgfq;

    move-result-object v0

    invoke-interface {v0}, Lgfq;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    iget-object v0, p0, Lfye;->c:Lfyf;

    iget-object v1, p0, Lfye;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->m()Lgfq;

    move-result-object v1

    invoke-interface {v1}, Lgfq;->h()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfye;->b:Lgfm;

    invoke-interface {v2}, Lgfm;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfye;->b:Lgfm;

    invoke-interface {v3}, Lgfm;->m()Lgfq;

    move-result-object v3

    invoke-interface {v3}, Lgfq;->d()Ljava/util/List;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lfzd;->a(Lfyf;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v1, "%s<id=\"%s\" type=\"%s\" hidden=\"%s\">"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v0, p0, Lfye;->b:Lgfm;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v3

    const/4 v3, 0x2

    iget-object v0, p0, Lfye;->b:Lgfm;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    aput-object v0, v2, v3

    const/4 v3, 0x3

    iget-object v0, p0, Lfye;->b:Lgfm;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->u()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lfye;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2
.end method
