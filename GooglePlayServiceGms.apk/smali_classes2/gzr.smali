.class public final Lgzr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "wallet.google_places_autocomplete_supported_countries"

    const-string v1, "CA,FR,DE,US"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzr;->a:Lbfy;

    const-string v0, "wallet.google_places_autocomplete_threshold_address_line_1"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzr;->b:Lbfy;

    const-string v0, "wallet.google_places_autocomplete_threshold_default"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lgzr;->c:Lbfy;

    return-void
.end method
