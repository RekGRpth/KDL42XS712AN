.class final Lhfg;
.super Lhcc;
.source "SourceFile"


# instance fields
.field final synthetic b:Ljau;

.field final synthetic c:Ljan;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Landroid/util/Pair;

.field final synthetic h:Lhfd;


# direct methods
.method constructor <init>(Lhfd;Landroid/accounts/Account;Ljau;Ljan;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;)V
    .locals 0

    iput-object p1, p0, Lhfg;->h:Lhfd;

    iput-object p3, p0, Lhfg;->b:Ljau;

    iput-object p4, p0, Lhfg;->c:Ljan;

    iput-object p5, p0, Lhfg;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p6, p0, Lhfg;->e:Ljava/lang/String;

    iput-object p7, p0, Lhfg;->f:Ljava/lang/String;

    iput-object p8, p0, Lhfg;->g:Landroid/util/Pair;

    invoke-direct {p0, p2}, Lhcc;-><init>(Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v0, p0, Lhfg;->b:Ljau;

    iget-object v1, p0, Lhfg;->c:Ljan;

    iput-object v1, v0, Ljau;->a:Ljan;

    iget-object v0, p0, Lhfg;->b:Ljau;

    invoke-static {}, Lhfx;->a()Ljbf;

    move-result-object v1

    iput-object v1, v0, Ljau;->o:Ljbf;

    iget-object v0, p0, Lhfg;->b:Ljau;

    iget-object v1, p0, Lhfg;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    invoke-static {v1}, Lhfx;->a(I)Z

    move-result v1

    iput-boolean v1, v0, Ljau;->k:Z

    sget-object v0, Lgzp;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhfg;->b:Ljau;

    iget-object v1, p0, Lhfg;->b:Ljau;

    iget-object v1, v1, Ljau;->j:[I

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lboz;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Ljau;->j:[I

    :cond_0
    iget-object v0, p0, Lhfg;->h:Lhfd;

    invoke-static {v0}, Lhfd;->a(Lhfd;)Lgtm;

    move-result-object v0

    iget-object v1, p0, Lhfg;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lhfg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgtm;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhfg;->b:Ljau;

    iput-object v0, v1, Ljau;->e:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lhfg;->b:Ljau;

    iget-object v1, p0, Lhfg;->h:Lhfd;

    invoke-static {v1}, Lhfd;->d(Lhfd;)Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v1

    iput-wide v1, v0, Ljau;->n:J

    iget-object v0, p0, Lhfg;->h:Lhfd;

    invoke-static {v0}, Lhfd;->c(Lhfd;)Lhfo;

    move-result-object v1

    iget-object v2, p0, Lhfg;->f:Ljava/lang/String;

    iget-object v4, p0, Lhfg;->g:Landroid/util/Pair;

    iget-object v3, p0, Lhfg;->b:Ljau;

    iget-object v6, v1, Lhfo;->a:Landroid/content/Context;

    new-instance v0, Lhft;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhft;-><init>(Lhfo;Ljava/lang/String;Ljau;Landroid/util/Pair;Lhgm;)V

    const-string v1, "encrypt_otp_and_get_full_wallet"

    invoke-static {v6, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhfg;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v0

    invoke-static {v0}, Lhfx;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
