.class public final Lebt;
.super Lebp;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lebp;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/Invitation;)Lebt;
    .locals 3

    new-instance v0, Lebt;

    invoke-direct {v0}, Lebt;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "invitation"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lebt;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method protected final J()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "invitation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final K()V
    .locals 3

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "invitation"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    instance-of v2, v1, Lebu;

    if-eqz v2, :cond_0

    check-cast v1, Lebu;

    :goto_0
    invoke-interface {v1, v0}, Lebu;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void

    :cond_0
    instance-of v2, v1, Lebv;

    if-eqz v2, :cond_1

    check-cast v1, Lebv;

    invoke-interface {v1}, Lebv;->d()Lebu;

    move-result-object v1

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lebu;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "InvitationChangeAccountDialogFragment must be used with a parent Activity which implements InvitationAccountSwitcher or InvitationAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
