.class public final Lemo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leji;
.implements Lfkr;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Random;

.field private final c:Lfkq;

.field private final d:Ljava/lang/Object;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lemo;->d:Ljava/lang/Object;

    iput-boolean v1, p0, Lemo;->e:Z

    iput-boolean v1, p0, Lemo;->f:Z

    iput-object p1, p0, Lemo;->a:Landroid/content/Context;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lemo;->b:Ljava/util/Random;

    new-instance v0, Lfkq;

    invoke-direct {v0, p1, p0}, Lfkq;-><init>(Landroid/content/Context;Lfkr;)V

    iput-object v0, p0, Lemo;->c:Lfkq;

    return-void
.end method

.method private a(Ljava/lang/String;Litr;)V
    .locals 5

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Lemo;->c:Lfkq;

    invoke-static {p2}, Lizs;->a(Lizs;)[B

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, p1, v3, v4}, Lfkq;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    iget-object v3, p0, Lemo;->d:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p0, Lemo;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lemo;->c:Lfkq;

    iget-object v0, v0, Lfkq;->a:Lflq;

    invoke-virtual {v0}, Lflq;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lemo;->e:Z

    :cond_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lemo;->a:Landroid/content/Context;

    invoke-static {p1}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v3, v1, v3

    if-ltz v3, :cond_0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v1, v3

    if-lez v3, :cond_2

    :cond_0
    const-string v3, "Bad sample ratio: %s"

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v3, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "Could not parse stats sample"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lemo;->b:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v3

    cmpl-double v1, v3, v1

    if-gez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v1, p0, Lemo;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lemo;->e:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lemo;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lemo;->c:Lfkq;

    iget-object v0, v0, Lfkq;->a:Lflq;

    invoke-virtual {v0}, Lflq;->j()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lemo;->f:Z

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lemo;->e:Z

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(II)V
    .locals 3

    const-string v0, "Timing: %d = %dms"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    const-string v0, "timing_stats_sample"

    const-string v1, "0.1"

    invoke-direct {p0, v0, v1}, Lemo;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Litr;

    invoke-direct {v0}, Litr;-><init>()V

    new-instance v1, Litv;

    invoke-direct {v1}, Litv;-><init>()V

    iput-object v1, v0, Litr;->d:Litv;

    iget-object v1, v0, Litr;->d:Litv;

    iput p1, v1, Litv;->a:I

    iget-object v1, v0, Litr;->d:Litv;

    iput p2, v1, Litv;->b:I

    const-string v1, "tstats"

    invoke-direct {p0, v1, v0}, Lemo;->a(Ljava/lang/String;Litr;)V

    goto :goto_0
.end method

.method public final a(IJJJZI)V
    .locals 2

    const-string v0, "default_stats_sample"

    const-string v1, "0.1"

    invoke-direct {p0, v0, v1}, Lemo;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Litr;

    invoke-direct {v0}, Litr;-><init>()V

    new-instance v1, Litu;

    invoke-direct {v1}, Litu;-><init>()V

    iput-object v1, v0, Litr;->b:Litu;

    iget-object v1, v0, Litr;->b:Litu;

    iput p1, v1, Litu;->a:I

    iget-object v1, v0, Litr;->b:Litu;

    iput-wide p2, v1, Litu;->b:J

    iget-object v1, v0, Litr;->b:Litu;

    iput-wide p4, v1, Litu;->c:J

    iget-object v1, v0, Litr;->b:Litu;

    iput-wide p6, v1, Litu;->d:J

    iget-object v1, v0, Litr;->b:Litu;

    iput-boolean p8, v1, Litu;->e:Z

    iget-object v1, v0, Litr;->b:Litu;

    iput p9, v1, Litu;->f:I

    const-string v1, "sstate"

    invoke-direct {p0, v1, v0}, Lemo;->a(Ljava/lang/String;Litr;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 1

    const-string v0, "Logging connection failed: %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    return-void
.end method

.method public final a(Litt;)V
    .locals 2

    const-string v0, "query_stats_sample"

    const-string v1, "0.1"

    invoke-direct {p0, v0, v1}, Lemo;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Litr;

    invoke-direct {v0}, Litr;-><init>()V

    iput-object p1, v0, Litr;->c:Litt;

    const-string v1, "qstats"

    invoke-direct {p0, v1, v0}, Lemo;->a(Ljava/lang/String;Litr;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "default_stats_sample"

    const-string v1, "0.1"

    invoke-direct {p0, v0, v1}, Lemo;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Litr;

    invoke-direct {v0}, Litr;-><init>()V

    new-instance v1, Lits;

    invoke-direct {v1}, Lits;-><init>()V

    iput-object v1, v0, Litr;->a:Lits;

    iget-object v1, v0, Litr;->a:Lits;

    iput-object p1, v1, Lits;->a:Ljava/lang/String;

    const-string v1, "error"

    invoke-direct {p0, v1, v0}, Lemo;->a(Ljava/lang/String;Litr;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v1, p0, Lemo;->d:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lemo;->f:Z

    iget-boolean v0, p0, Lemo;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lemo;->c:Lfkq;

    iget-object v0, v0, Lfkq;->a:Lflq;

    invoke-virtual {v0}, Lflq;->j()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lemo;->f:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 1

    const-string v0, "Logging connection failed"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    return-void
.end method
