.class public final Lfpa;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lfod;


# instance fields
.field private final a:Lfob;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Ljava/util/ArrayList;

.field private d:Lfpb;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfob;Lfpb;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfpa;->b:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lfpa;->a:Lfob;

    iput-object p3, p0, Lfpa;->d:Lfpb;

    return-void
.end method


# virtual methods
.method public final a(I)Lfxh;
    .locals 1

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    invoke-virtual {p0}, Lfpa;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lfpb;)V
    .locals 0

    iput-object p1, p0, Lfpa;->d:Lfpb;

    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 3

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean p2, p0, Lfpa;->e:Z

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iget-object v2, p0, Lfpa;->d:Lfpb;

    invoke-interface {v2, v0}, Lfpb;->a(Lfxh;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lfpa;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpa;->e:Z

    invoke-virtual {p0}, Lfpa;->notifyDataSetInvalidated()V

    return-void
.end method

.method public final getCount()I
    .locals 2

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-boolean v0, p0, Lfpa;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lfpa;->a(I)Lfxh;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    if-nez p2, :cond_0

    iget-object v0, p0, Lfpa;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400d7    # com.google.android.gms.R.layout.plus_list_apps_item

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lfpa;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iget-object v1, p0, Lfpa;->a:Lfob;

    invoke-virtual {v1}, Lfob;->J()Lfnz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v2

    const v1, 0x7f0a0172    # com.google.android.gms.R.id.app_name

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v2, Lfoa;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, v2, Lfoa;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v0}, Lfxh;->c()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, v2, Lfoa;->c:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lfpa;->a:Lfob;

    invoke-virtual {v2, p0, v0, v1}, Lfob;->a(Lfod;Lfxh;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object p2

    :cond_2
    if-nez p2, :cond_1

    iget-object v0, p0, Lfpa;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400d8    # com.google.android.gms.R.layout.plus_list_apps_item_loading

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
