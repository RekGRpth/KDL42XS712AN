.class public final Lcfi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcfz;


# instance fields
.field private final a:Lcdu;

.field private final b:Z

.field private final c:Z

.field private final d:Lcfd;


# direct methods
.method public constructor <init>(Lcdu;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcfi;->a:Lcdu;

    sget-object v0, Lbqs;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcfi;->b:Z

    sget-object v0, Lbth;->b:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    iput-boolean v0, p0, Lcfi;->c:Z

    new-instance v0, Lcfd;

    invoke-direct {v0}, Lcfd;-><init>()V

    iput-object v0, p0, Lcfi;->d:Lcfd;

    iget-object v0, p0, Lcfi;->a:Lcdu;

    new-instance v1, Lcdw;

    invoke-direct {v1, v0}, Lcdw;-><init>(Lcdu;)V

    iget-object v2, v0, Lcdu;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcdy;

    const-string v3, "Open database in background"

    invoke-direct {v2, v0, v3, v1}, Lcdy;-><init>(Lcdu;Ljava/lang/String;Lcou;)V

    invoke-virtual {v2}, Lcdy;->start()V

    :cond_0
    return-void
.end method

.method private a(Lcdt;Lcdp;J)I
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcdt;->c()[Lcou;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v0, v5, v3

    invoke-interface {v0}, Lcou;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdp;

    invoke-virtual {v0}, Lcdp;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->b(Z)V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v3, p0, Lcfi;->a:Lcdu;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, p1, v0, v4, v1}, Lcdu;->a(Lcdt;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/drive/database/SqlWhereClause;)I
    .locals 4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcde;->a()Lcde;

    move-result-object v1

    invoke-virtual {v1}, Lcde;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    if-eqz p1, :cond_0

    invoke-static {p1}, Lbzw;->a(Lcfc;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v1, Lcev;->a:Lcev;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p3

    :cond_0
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v1, p2

    move-object v2, p5

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Lcgr;)Lcgs;
    .locals 2

    new-instance v0, Lcgq;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-direct {v0, v1, p1, p2}, Lcgq;-><init>(Lcdu;Landroid/database/Cursor;Lcgr;)V

    return-object v0
.end method

.method private a(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Lcgs;
    .locals 6

    const/4 v4, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->c:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v2, p2

    :goto_0
    invoke-static {p1, p4}, Lcfi;->a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    iget-object v1, p1, Lbsp;->a:Lcfc;

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcfi;->a(Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0, p5}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p3

    goto :goto_0
.end method

.method private a(Lcfp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lcev;->a:Lcev;

    invoke-static {p1}, Lcfi;->a(Lcfp;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v3}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lced;->a()Lced;

    move-result-object v1

    invoke-virtual {v1}, Lced;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Lcgj;

    invoke-direct {v1, p1}, Lcgj;-><init>(Lcfp;)V

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgm;->a()Lcgm;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 7

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcev;->a:Lcev;

    invoke-static {p0, p1}, Lcfi;->c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lceg;->j:Lceg;

    invoke-virtual {v6}, Lceg;->a()Lcdp;

    move-result-object v6

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lceg;->k:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    const-wide/16 v5, 0x1

    invoke-virtual {v4, v5, v6}, Lcdp;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 5

    iget-object v0, p0, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->c:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    sget-object v0, Lcee;->b:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    iget-wide v1, p0, Lbsp;->b:J

    invoke-virtual {v0, v1, v2}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lbsp;->e:Ljava/util/Set;

    sget-object v2, Lbqr;->b:Lbqr;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lceg;->A:Lceg;

    invoke-virtual {v3}, Lceg;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lbsp;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcev;->b:Lcev;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :goto_1
    sget-object v1, Lcev;->a:Lcev;

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p1

    goto :goto_0

    :cond_1
    sget-object v1, Lceg;->A:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1}, Lcdp;->d()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    sget-object v2, Lcev;->a:Lcev;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lcfp;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    sget-object v0, Lcee;->a:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    iget-wide v1, p0, Lcfl;->f:J

    invoke-virtual {v0, v1, v2}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcgs;)Ljava/lang/Object;
    .locals 3

    :try_start_0
    invoke-interface {p0}, Lcgs;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcgs;->close()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {p0}, Lcgs;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcgs;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-interface {p0}, Lcgs;->close()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Result list has "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcgs;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items, but only 1 was expected."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-interface {p0}, Lcgs;->close()V

    throw v0
.end method

.method private a(Lcfc;Lcom/google/android/gms/drive/database/SqlWhereClause;)Ljava/util/List;
    .locals 11

    const/4 v2, 0x0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "AuthorizedAppScopeView"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, p1, v2}, Lcff;->a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcff;

    move-result-object v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-class v1, Lbqr;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v8, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v3, p0, Lcfi;->a:Lcdu;

    invoke-static {v3, v0, v2}, Lcfh;->a(Lcdu;Lcff;Landroid/database/Cursor;)Lcfh;

    move-result-object v0

    iget-object v0, v0, Lcfh;->a:Lbqr;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    new-instance v0, Lbsp;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcff;

    iget-wide v2, v2, Lcff;->a:J

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcff;

    iget-object v4, v4, Lcff;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcff;

    iget-wide v5, v5, Lcff;->c:J

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lbsp;-><init>(Lcfc;JLcom/google/android/gms/drive/auth/AppIdentity;JLjava/util/Set;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-object v9
.end method

.method private a(Lcfc;Ljava/lang/Boolean;Z)Ljava/util/Set;
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcdh;->b:Lcdh;

    invoke-virtual {v5}, Lcdh;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "=?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v7, p1, Lcfc;->b:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcdh;->e:Lcdh;

    invoke-virtual {v7}, Lcdh;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "=?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v7, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcev;->a:Lcev;

    new-array v7, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v5, v7, v3

    invoke-virtual {v0, v4, v7}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    if-eqz p2, :cond_0

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcdh;->c:Lcdh;

    invoke-virtual {v7}, Lcdh;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "=?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v7, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcev;->a:Lcev;

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v4, v1, v3

    invoke-virtual {v0, v5, v1}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    :cond_0
    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "AppDataScopeView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v0

    invoke-virtual {v0}, Lcdg;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfg;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, p1, v1}, Lcfg;->a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcfg;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v0, v3

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfg;

    iget-wide v3, v0, Lcfg;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    return-object v1
.end method

.method private b(Lcfc;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lbsp;
    .locals 4

    invoke-direct {p0, p1, p2}, Lcfi;->a(Lcfc;Lcom/google/android/gms/drive/database/SqlWhereClause;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsp;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Multiple apps with same account/identity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private b(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Ljava/lang/Object;
    .locals 2

    invoke-direct/range {p0 .. p5}, Lcfi;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Lcgs;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcgs;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcgs;->close()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Lcgs;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-interface {v1}, Lcgs;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcgs;->close()V

    throw v0
.end method

.method private c(Lbsp;)Lcfg;
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v6, p1, Lbsp;->a:Lcfc;

    iget-wide v4, v6, Lcfc;->b:J

    const-wide/16 v7, 0x0

    cmp-long v0, v4, v7

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v6, v5, v3

    invoke-static {v0, v4, v5}, Lbkm;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v4, Lbqr;->b:Lbqr;

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "App not authorized for appdata"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Lcev;->a:Lcev;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcdh;->b:Lcdh;

    invoke-virtual {v7}, Lcdh;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "=?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-wide v7, v6, Lcfc;->b:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcdh;->a:Lcdh;

    invoke-virtual {v8}, Lcdh;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p1, Lbsp;->b:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v3

    invoke-virtual {v0, v4, v1}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v1

    invoke-virtual {v1}, Lcdg;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "application/vnd.google-apps.folder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "appdata."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lbsp;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcfi;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;

    move-result-object v7

    iget-wide v0, p1, Lbsp;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcfp;->c(Ljava/lang/Long;)V

    invoke-virtual {v7}, Lcfp;->k()V

    new-instance v0, Lcfg;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-wide v2, v6, Lcfc;->b:J

    iget-wide v4, p1, Lbsp;->b:J

    iget-wide v6, v7, Lcfl;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcfg;-><init>(Lcdu;JJLjava/lang/Long;ZZ)V

    :goto_1
    invoke-virtual {v0}, Lcfg;->k()V

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    return-object v0

    :cond_0
    move v0, v3

    goto/16 :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v6, v9}, Lcfg;->a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcfg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0
.end method

.method private static c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lceg;->s:Lceg;

    invoke-virtual {v2}, Lceg;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 6

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcdf;->d:Lcdf;

    invoke-virtual {v2}, Lcdf;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcdf;->e:Lcdf;

    invoke-virtual {v1}, Lcdf;->a()Lcdp;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    sget-object v2, Lcev;->a:Lcev;

    iget-wide v3, p0, Lcfc;->b:J

    invoke-static {v3, v4}, Lcfi;->c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private d(J)Lcfe;
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v1

    invoke-virtual {v1}, Lcdc;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v4

    invoke-virtual {v4}, Lcdc;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v1}, Lcfe;->a(Lcdu;Landroid/database/Cursor;)Lcfe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private i(Ljava/lang/String;)Lcfe;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v1

    invoke-virtual {v1}, Lcdc;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcdd;->a:Lcdd;

    invoke-virtual {v4}, Lcdd;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v1}, Lcfe;->a(Lcdu;Landroid/database/Cursor;)Lcfe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private j(Ljava/lang/String;)J
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lbsp;J)I
    .locals 9

    iget-object v1, p1, Lbsp;->a:Lcfc;

    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcfi;->b()Lcgs;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    invoke-virtual {p0, v0}, Lcfi;->b(Lcfn;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->c()J

    move-result-wide v4

    iget-wide v7, v1, Lcfc;->b:J

    cmp-long v4, v4, v7

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcgs;->close()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Lcgs;->close()V

    sget-object v0, Lcev;->a:Lcev;

    iget-wide v1, v1, Lcfc;->b:J

    invoke-static {v1, v2}, Lcfi;->c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    sget-object v4, Lceg;->p:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v7

    invoke-virtual {v7}, Lcef;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " NOT IN (SELECT "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Lceq;->d:Lceq;

    invoke-virtual {v7}, Lceq;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " FROM "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcep;->a()Lcep;

    move-result-object v7

    invoke-virtual {v7}, Lcep;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " WHERE "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Lceq;->d:Lceq;

    invoke-virtual {v7}, Lceq;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " IS NOT NULL)"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-direct {v4, v5, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v7

    invoke-virtual {v7}, Lcef;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " NOT IN (SELECT "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Lcdh;->d:Lcdh;

    invoke-virtual {v7}, Lcdh;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " FROM "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v7

    invoke-virtual {v7}, Lcdg;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ")"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-direct {v4, v5, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lceg;->t:Lceg;

    invoke-virtual {v7}, Lceg;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "<?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lceg;->k:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    const-wide/16 v7, 0x0

    invoke-virtual {v4, v7, v8}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcgo;->a()Lcgo;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcfi;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Lcgs;

    move-result-object v1

    :try_start_1
    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Lcgs;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Lcgs;->close()V

    invoke-static {v7, v6}, Lcox;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v5

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v0

    sget-object v7, Lcfp;->b:Landroid/net/Uri;

    invoke-virtual {v4, v5, v6, v0, v7}, Lcdu;->a(JLcdt;Landroid/net/Uri;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_3

    :cond_4
    if-eq v1, v2, :cond_5

    const-string v0, "DatabaseModelLoader"

    const-string v3, "Only %d of %d obsolete entries deleted successfully"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v0, v3, v4}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_5
    return v1
.end method

.method public final a(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-object v0, Lcev;->a:Lcev;

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    const/4 v1, 0x0

    const-string v2, "EntryView"

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcfi;->a(Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcfp;JLbsj;)Lbsj;
    .locals 5

    invoke-virtual {p1}, Lcfp;->m()Z

    move-result v0

    const-string v1, "entry must be saved to database before setting auth state"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Lcee;->b:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->c()V

    :try_start_0
    sget-object v1, Lcfj;->a:[I

    invoke-virtual {p4}, Lbsj;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0

    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcfi;->a(Lcfp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :try_start_2
    invoke-interface {v1}, Lcgs;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbsj;->b:Lbsj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    :try_start_3
    invoke-interface {v1}, Lcgs;->close()V

    sget-object v1, Lbsj;->b:Lbsj;

    if-ne v0, v1, :cond_0

    new-instance v1, Lcft;

    iget-object v2, p0, Lcfi;->a:Lcdu;

    invoke-direct {v1, v2, p1, p2, p3}, Lcft;-><init>(Lcdu;Lcfp;J)V

    invoke-virtual {v1}, Lcft;->k()V

    :cond_0
    :goto_1
    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    return-object v0

    :cond_1
    :try_start_4
    sget-object v0, Lbsj;->a:Lbsj;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Lcgs;->close()V

    throw v0

    :pswitch_1
    sget-object v1, Lcev;->a:Lcev;

    invoke-static {p1}, Lcfi;->a(Lcfp;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lced;->a()Lced;

    move-result-object v2

    invoke-virtual {v2}, Lced;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lbsj;->b:Lbsj;

    goto :goto_1

    :cond_2
    sget-object v0, Lbsj;->a:Lbsj;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcfc;J)Lbsp;
    .locals 5

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcdf;->b:Lcdf;

    invoke-virtual {v2}, Lcdf;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcev;->a:Lcev;

    iget-wide v2, p1, Lcfc;->b:J

    invoke-static {v2, v3}, Lcfi;->c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcfi;->b(Lcfc;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;
    .locals 1

    invoke-static {p1, p2}, Lcfi;->c(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcfi;->b(Lcfc;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcfc;
    .locals 5

    iget-object v0, p0, Lcfi;->d:Lcfd;

    iget-object v0, v0, Lcfd;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcfi;->d(J)Lcfe;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcfc;

    invoke-virtual {v1}, Lcfe;->i()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, v1, Lcfl;->f:J

    invoke-direct {v0, v2, v3, v4}, Lcfc;-><init>(Ljava/lang/String;J)V

    iget-object v1, p0, Lcfi;->d:Lcfd;

    invoke-virtual {v1, v0}, Lcfd;->a(Lcfc;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcfc;
    .locals 3

    iget-object v0, p0, Lcfi;->d:Lcfd;

    iget-object v0, v0, Lcfd;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcfi;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    new-instance v0, Lcfc;

    iget-wide v1, v1, Lcfl;->f:J

    invoke-direct {v0, p1, v1, v2}, Lcfc;-><init>(Ljava/lang/String;J)V

    iget-object v1, p0, Lcfi;->d:Lcfd;

    invoke-virtual {v1, v0}, Lcfd;->a(Lcfc;)V

    :cond_0
    return-object v0
.end method

.method public final a(Lbsp;)Lcfg;
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->b:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v0, p1, Lbsp;->a:Lcfc;

    iget-object v0, v0, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcfi;->a(Ljava/lang/String;)Lcfc;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcdh;->a:Lcdh;

    invoke-virtual {v3}, Lcdh;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v3, p1, Lbsp;->b:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcdh;->b:Lcdh;

    invoke-virtual {v4}, Lcdh;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lbsp;->a:Lcfc;

    iget-wide v4, v4, Lcfc;->b:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcev;->a:Lcev;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "AppDataScopeView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v6, v1}, Lcfg;->a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcfg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;
    .locals 2

    invoke-virtual {p0, p1, p2}, Lcfi;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcfp;->W()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public final a(Lbsp;Ljava/lang/String;)Lcfp;
    .locals 6

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v5

    iget-object v0, p1, Lbsp;->a:Lcfc;

    iget-wide v0, v0, Lcfc;->b:J

    invoke-static {v0, v1, p2}, Lcfi;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcfi;->b(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    return-object v0
.end method

.method public final a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v6, p1, Lbsp;->a:Lcfc;

    iget-wide v0, v6, Lcfc;->b:J

    invoke-static {v0, v1, p3}, Lcfi;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcfi;->b(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    if-nez v0, :cond_2

    iget-object v0, p1, Lbsp;->a:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v1

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcfi;->b(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "App %s is not authorized to access document with resource ID %s."

    new-array v2, v9, [Ljava/lang/Object;

    aput-object p1, v2, v8

    aput-object p3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcfp;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-wide v2, v6, Lcfc;->b:J

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcfp;-><init>(Lcdu;JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfp;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v7}, Lcfp;->k(Z)V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0}, Lcfp;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "DatabaseModelLoader"

    const-string v2, "Fetching %s as MIME type %s: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcfp;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    aput-object p2, v3, v7

    aput-object p3, v3, v9

    invoke-static {v1, v2, v3}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Lcfc;Ljava/lang/String;Ljava/lang/String;Lcgf;)Lcfp;
    .locals 7

    const/4 v6, 0x1

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcfp;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-wide v2, p1, Lcfc;->b:J

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcfp;-><init>(Lcdu;JLjava/lang/String;Lcgf;)V

    invoke-virtual {v0}, Lcfp;->W()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v6

    :goto_0
    const-string v2, "kind must be a non-collection"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-virtual {v0, p2}, Lcfp;->b(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lcfp;->c(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcfp;->a(Ljava/util/Date;)V

    invoke-virtual {v0, v1}, Lcfp;->b(Ljava/util/Date;)V

    invoke-virtual {v0, v6}, Lcfp;->f(Z)V

    invoke-virtual {v0}, Lcfp;->k()V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcfp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfv;
    .locals 6

    new-instance v0, Lcfv;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-wide v2, p1, Lcfl;->f:J

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcfv;-><init>(Lcdu;JJ)V

    return-object v0
.end method

.method public final a(Lcfc;Lbuv;J)Lcgb;
    .locals 10

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-wide v4, p1, Lcfc;->b:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v0, v4, v5}, Lbkm;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcev;->a:Lcev;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lceo;->b:Lceo;

    invoke-virtual {v6}, Lceo;->a()Lcdp;

    move-result-object v6

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, Lbuv;->c:Lbux;

    invoke-virtual {v6}, Lbux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lceo;->c:Lceo;

    invoke-virtual {v8}, Lceo;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lbuv;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v3

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lceo;->a:Lceo;

    invoke-virtual {v7}, Lceo;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-wide v7, p1, Lcfc;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v5, v1

    invoke-virtual {v0, v4, v5}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v1

    invoke-virtual {v1}, Lcen;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-wide v2, p1, Lcfc;->b:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v0, Lcgb;

    const/4 v5, 0x0

    const-wide/16 v7, 0x0

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lcgb;-><init>(Lcdu;JLbuv;Ljava/lang/String;Ljava/lang/Long;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    goto/16 :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v9}, Lcgb;->a(Lcdu;Landroid/database/Cursor;)Lcgb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcge;
    .locals 7

    new-instance v0, Lcge;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lcge;-><init>(Lcdu;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)V

    invoke-virtual {v0}, Lcge;->k()V

    return-object v0
.end method

.method public final a()Lcgs;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v1

    invoke-virtual {v1}, Lcdc;->e()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgi;->a()Lcgi;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbsp;Lcfp;)Lcgs;
    .locals 6

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v0, "childId=?"

    iget-wide v1, p2, Lcfl;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ParentCollectionView"

    const-string v3, "ScopedParentCollectionView"

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcfi;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcgs;
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lces;->b:Lces;

    invoke-virtual {v0}, Lces;->a()Lcdp;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcer;->a()Lcer;

    move-result-object v1

    invoke-virtual {v1}, Lcer;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgt;->a()Lcgt;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/data/EntrySpec;I)Lcgs;
    .locals 8

    const/4 v2, 0x0

    sget-object v0, Lcev;->a:Lcev;

    sget-object v1, Lcex;->b:Lcex;

    invoke-virtual {v1}, Lcex;->a()Lcdp;

    move-result-object v1

    const-wide/16 v3, 0x1

    invoke-virtual {v1, v3, v4}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    sget-object v5, Lcex;->a:Lcex;

    invoke-virtual {v5}, Lcex;->a()Lcdp;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v1

    invoke-virtual {v1}, Lcew;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    sget-object v1, Lcgv;->a:Lcgv;

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    invoke-static {p1, p2}, Lcfi;->a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    iget-object v0, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->c:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "MetadataView"

    :goto_0
    iget-object v1, p1, Lbsp;->a:Lcfc;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcfi;->a(Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dbInstanceId"

    iget-object v3, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v3}, Lcdu;->f()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v2

    :cond_0
    const-string v2, "ScopedMetadataView"

    goto :goto_0
.end method

.method public final a(Lcfc;Lbux;)Ljava/util/List;
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-wide v4, p1, Lcfc;->b:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v0, v4, v5}, Lbkm;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcev;->a:Lcev;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lceo;->b:Lceo;

    invoke-virtual {v6}, Lceo;->a()Lcdp;

    move-result-object v6

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lbux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lceo;->a:Lceo;

    invoke-virtual {v7}, Lceo;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-wide v7, p1, Lcfc;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v3

    invoke-virtual {v0, v4, v1}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v1

    invoke-virtual {v1}, Lcen;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcfi;->a:Lcdu;

    invoke-static {v2, v1}, Lcgb;->a(Lcdu;Landroid/database/Cursor;)Lcgb;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v3

    goto/16 :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final a(Lcfc;Ljava/lang/Boolean;)Ljava/util/Set;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcfi;->a(Lcfc;Ljava/lang/Boolean;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcfc;)V
    .locals 6

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    iget-object v0, p1, Lcfc;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcfi;->i(Ljava/lang/String;)Lcfe;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-wide v2, v1, Lcfl;->f:J

    iget-wide v4, p1, Lcfc;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-virtual {v1}, Lcfe;->l()V

    :cond_0
    iget-object v0, p0, Lcfi;->d:Lcfd;

    iget-object v1, v0, Lcfd;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcfd;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v1, p1, Lcfc;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0
.end method

.method public final a(Lcfn;)V
    .locals 6

    iget-wide v1, p1, Lcfl;->f:J

    invoke-virtual {p1}, Lcfn;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v3

    sget-object v4, Lceg;->v:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-direct {p0, v3, v4, v1, v2}, Lcfi;->a(Lcdt;Lcdp;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v4

    sget-object v5, Lcec;->l:Lcec;

    invoke-virtual {v5}, Lcec;->a()Lcdp;

    move-result-object v5

    invoke-direct {p0, v4, v5, v1, v2}, Lcfi;->a(Lcdt;Lcdp;J)I

    move-result v1

    add-int/2addr v1, v3

    iget-boolean v2, p0, Lcfi;->c:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Detected more than one reference to the document content: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v0

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v3

    sget-object v4, Lcec;->l:Lcec;

    invoke-virtual {v4}, Lcec;->a()Lcdp;

    move-result-object v4

    invoke-direct {p0, v3, v4, v1, v2}, Lcfi;->a(Lcdt;Lcdp;J)I

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Lcfp;Ljava/util/Set;)V
    .locals 6

    invoke-virtual {p1}, Lcfp;->m()Z

    move-result v0

    const-string v1, "entry must be saved to database before authorized apps are set"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, p1, v0}, Lcfi;->a(Lcfp;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    :try_start_1
    invoke-interface {v2}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcft;

    iget-wide v4, v0, Lcft;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-wide v4, v0, Lcft;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcft;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Lcgs;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0

    :cond_1
    :try_start_3
    invoke-interface {v2}, Lcgs;->close()V

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v0, Lcft;

    iget-object v4, p0, Lcfi;->a:Lcdu;

    invoke-direct {v0, v4, p1, v2, v3}, Lcft;-><init>(Lcdu;Lcfp;J)V

    invoke-virtual {v0}, Lcft;->k()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;ILjava/lang/String;)V
    .locals 7

    sget-object v0, Lcev;->a:Lcev;

    sget-object v1, Lcex;->c:Lcex;

    invoke-virtual {v1}, Lcex;->a()Lcdp;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    sget-object v4, Lcex;->e:Lcex;

    invoke-virtual {v4}, Lcex;->a()Lcdp;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcex;->b:Lcex;

    invoke-virtual {v4}, Lcex;->a()Lcdp;

    move-result-object v4

    int-to-long v5, p3

    invoke-virtual {v4, v5, v6}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcex;->a:Lcex;

    invoke-virtual {v4}, Lcex;->a()Lcdp;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcex;->d:Lcex;

    invoke-virtual {v4}, Lcex;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4, p4}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v2

    invoke-virtual {v2}, Lcew;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 5

    sget-object v0, Lcdm;->b:Lcdm;

    invoke-virtual {v0}, Lcdm;->a()Lcdp;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v1, Lcdm;->a:Lcdm;

    invoke-virtual {v1}, Lcdm;->a()Lcdp;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    sget-object v2, Lcev;->a:Lcev;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v0, v3}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v2

    invoke-virtual {v2}, Lcdl;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final a(Lcov;)V
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0, p1}, Lcdu;->a(Lcov;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)Lcfe;
    .locals 2

    invoke-direct {p0, p1}, Lcfi;->i(Ljava/lang/String;)Lcfe;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    invoke-direct {p0, p1}, Lcfi;->i(Ljava/lang/String;)Lcfe;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcfe;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-direct {v0, v1, p1}, Lcfe;-><init>(Lcdu;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfe;->k()V

    :cond_0
    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0
.end method

.method public final b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;
    .locals 12

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v5

    iget-object v0, p1, Lbsp;->a:Lcfc;

    iget-wide v0, v0, Lcfc;->b:J

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v6

    sget-object v4, Lcev;->a:Lcev;

    invoke-static {v0, v1}, Lcfi;->c(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v11

    invoke-virtual {v11}, Lcef;->f()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "=? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v10, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v9, v1, v8

    invoke-virtual {v4, v0, v1}, Lcev;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcfi;->b(Lbsp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcgr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    return-object v0
.end method

.method public final b(Lbsp;Ljava/lang/String;)Lcfp;
    .locals 2

    invoke-static {p2}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "application/vnd.google-apps.folder"

    const-string v1, "root"

    invoke-virtual {p0, p1, v0, v1}, Lcfi;->a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcfp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {v0, p2}, Lcfp;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfp;->k()V

    :cond_1
    return-object v0
.end method

.method public final b(Lcfn;)Lcfp;
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "EntryView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lceg;->v:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcfl;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {v0, v1}, Lcfp;->a(Lcdu;Landroid/database/Cursor;)Lcfp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b(J)Lcge;
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcer;->a()Lcer;

    move-result-object v1

    invoke-virtual {v1}, Lcer;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcer;->a()Lcer;

    move-result-object v1

    invoke-virtual {v1}, Lcer;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgt;->a()Lcgt;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    invoke-static {v0}, Lcfi;->a(Lcgs;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcge;

    return-object v0
.end method

.method public final b()Lcgs;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v1

    invoke-virtual {v1}, Lceb;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcec;->g:Lcec;

    invoke-virtual {v4}, Lcec;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgl;->a()Lcgl;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbsp;Lcfp;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-wide v0, p2, Lcfl;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v5

    invoke-virtual {v5}, Lcef;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcdm;->a:Lcdm;

    invoke-virtual {v5}, Lcdm;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v5

    invoke-virtual {v5}, Lcdl;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcdm;->b:Lcdm;

    invoke-virtual {v5}, Lcdm;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcfi;->a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lbsp;)V
    .locals 8

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    iget-object v0, p1, Lbsp;->a:Lcfc;

    iget-object v1, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0, v0, v1}, Lcfi;->b(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)V

    new-instance v0, Lcff;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    iget-object v2, p1, Lbsp;->a:Lcfc;

    iget-wide v3, p1, Lbsp;->b:J

    iget-object v5, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v6, p1, Lbsp;->d:J

    invoke-direct/range {v0 .. v7}, Lcff;-><init>(Lcdu;Lcfc;JLcom/google/android/gms/drive/auth/AppIdentity;J)V

    invoke-virtual {v0}, Lcff;->k()V

    iget-object v1, p1, Lbsp;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbqr;

    new-instance v3, Lcfh;

    iget-object v4, p0, Lcfi;->a:Lcdu;

    invoke-direct {v3, v4, v0, v1}, Lcfh;-><init>(Lcdu;Lcff;Lbqr;)V

    invoke-virtual {v3}, Lcfh;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p1, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->b:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcfi;->c(Lbsp;)Lcfg;

    :cond_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void
.end method

.method public final b(Lcfc;)V
    .locals 4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    sget-object v1, Lcfe;->a:Landroid/net/Uri;

    iget-wide v2, p1, Lcfc;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcdu;->a(Landroid/net/Uri;J)V

    return-void
.end method

.method public final b(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)V
    .locals 1

    invoke-static {p1, p2}, Lcfi;->c(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, v0}, Lcfi;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;)I

    return-void
.end method

.method public final b(Lcov;)Z
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0, p1}, Lcdu;->b(Lcov;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcfc;)I
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p1, Lcfc;->b:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v5

    invoke-virtual {v5}, Lcen;->e()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lceo;->a:Lceo;

    invoke-virtual {v7}, Lceo;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v5, v6, v1}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    return-void
.end method

.method public final c(Lcfn;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcez;->m:Lcez;

    invoke-virtual {v2}, Lcez;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p1, Lcfl;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcey;->a()Lcey;

    move-result-object v2

    invoke-virtual {v2}, Lcey;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcec;->e:Lcec;

    invoke-virtual {v1}, Lcec;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1}, Lcdp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v1

    invoke-virtual {v1}, Lceb;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x1

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v8}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-boolean v2, p0, Lcfi;->c:Z

    if-eqz v2, :cond_0

    if-le v0, v9, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "DocumentContent file is referenced by more than one DocumentContent: [filename: %s, reference count: %s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    if-lez v0, :cond_1

    move v0, v9

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :cond_1
    move v0, v10

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcfx;
    .locals 2

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v0

    invoke-virtual {v0}, Lcel;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcem;->a:Lcem;

    invoke-virtual {v1}, Lcem;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;

    move-result-object v0

    invoke-static {v0}, Lcfi;->a(Lcgs;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfx;

    return-object v0
.end method

.method public final d(Lcfc;)Lcgs;
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lceq;->a:Lceq;

    invoke-virtual {v0}, Lceq;->a()Lcdp;

    move-result-object v0

    iget-wide v3, p1, Lcfc;->b:J

    invoke-virtual {v0, v3, v4}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcep;->a()Lcep;

    move-result-object v1

    invoke-virtual {v1}, Lcep;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Lcgd;

    invoke-direct {v1, p1}, Lcgd;-><init>(Lcfc;)V

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void
.end method

.method public final e(Lcfc;)Ljava/util/Set;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcfi;->a(Lcfc;Ljava/lang/Boolean;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcgk;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-direct {v0, v1, p1}, Lcgk;-><init>(Lcdu;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lcgk;->k()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcdo;->a:Lcdo;

    invoke-virtual {v0}, Lcdo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdn;->a()Lcdn;

    move-result-object v2

    invoke-virtual {v2}, Lcdn;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final g()V
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lceg;->p:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcdp;->b(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :try_start_0
    iget-object v2, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v2}, Lcdu;->c()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcfi;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v1

    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcfp;->d(Z)V

    invoke-virtual {v0}, Lcfp;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcgs;->close()V

    :cond_0
    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcgs;->close()V

    :cond_2
    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 6

    const/4 v2, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v0, "filename=?"

    invoke-direct {v4, v0, p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "CannotDeleteFilenameView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final h()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcem;->a:Lcem;

    invoke-virtual {v1}, Lcem;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1}, Lcdp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN WipeoutFileContentHashView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v2

    invoke-virtual {v2}, Lcel;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->c()V

    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcdf;->d:Lcdf;

    invoke-virtual {v2}, Lcdf;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcfi;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;)I

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v2

    invoke-virtual {v2}, Lcew;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    const-string v0, "DatabaseModelLoader"

    const-string v1, "Uninstalled %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->d()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->d()V

    throw v0
.end method

.method public final i()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcdn;->a()Lcdn;

    move-result-object v1

    invoke-virtual {v1}, Lcdn;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcdu;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final j()J
    .locals 2

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcdu;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final k()Lcgs;
    .locals 2

    const-string v0, "InternalContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcfi;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final l()J
    .locals 2

    const-string v0, "InternalContentSizeView"

    invoke-direct {p0, v0}, Lcfi;->j(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final m()Lcgs;
    .locals 2

    const-string v0, "InternalCachedContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcfi;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final n()J
    .locals 2

    const-string v0, "InternalCachedContentSizeView"

    invoke-direct {p0, v0}, Lcfi;->j(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final o()Lcgs;
    .locals 2

    const-string v0, "SharedCachedContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcfi;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final p()J
    .locals 2

    const-string v0, "SharedCachedContentSizeView"

    invoke-direct {p0, v0}, Lcfi;->j(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()J
    .locals 5

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT * FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v2

    invoke-virtual {v2}, Lcel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " JOIN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v2

    invoke-virtual {v2}, Lcef;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ON "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcem;->a:Lcem;

    invoke-virtual {v2}, Lcem;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lceg;->z:Lceg;

    invoke-virtual {v2}, Lceg;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lceg;->p:Lceg;

    invoke-virtual {v2}, Lceg;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const-wide/16 v0, 0x0

    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcfi;->a:Lcdu;

    invoke-static {v3, v2}, Lcfx;->a(Lcdu;Landroid/database/Cursor;)Lcfx;

    move-result-object v3

    iget-wide v3, v3, Lcfx;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v3

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final r()Z
    .locals 6

    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    iget-object v0, p0, Lcfi;->a:Lcdu;

    invoke-static {}, Lcey;->a()Lcey;

    move-result-object v1

    invoke-virtual {v1}, Lcey;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public final s()Lcgf;
    .locals 2

    new-instance v0, Lcfk;

    iget-object v1, p0, Lcfi;->a:Lcdu;

    invoke-direct {v0, v1}, Lcfk;-><init>(Lcdu;)V

    return-object v0
.end method

.method public final t()Lcgs;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcfi;->a:Lcdu;

    const-string v1, "PinnedDownloadRequiredView"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcgn;->a()Lcgn;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcfi;->a(Landroid/database/Cursor;Lcgr;)Lcgs;

    move-result-object v0

    return-object v0
.end method
