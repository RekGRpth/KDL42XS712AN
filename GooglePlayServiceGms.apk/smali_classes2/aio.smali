.class public final Laio;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Leie;Lekz;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 11

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v4

    :goto_0
    return-object v0

    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move v0, v1

    :goto_1
    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    aget-object v7, v2, v0

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lekz;->f:[Lela;

    aget-object v2, v2, v1

    iget-object v2, v2, Lela;->b:[Lelc;

    aget-object v2, v2, v0

    iget-object v8, v2, Lelc;->a:[Z

    move v2, v1

    :goto_2
    array-length v3, p3

    if-ge v2, v3, :cond_4

    aget-boolean v3, v8, v2

    if-eqz v3, :cond_3

    aget-object v9, p3, v2

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v5, v7, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v5}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v4

    goto :goto_0

    :cond_6
    move-object v0, v5

    goto :goto_0
.end method

.method public static a(Leie;Lekz;Lehh;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 14

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    new-instance v5, Ljava/util/HashSet;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/HashSet;-><init>(I)V

    new-instance v4, Landroid/os/Bundle;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v4, v1}, Landroid/os/Bundle;-><init>(I)V

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Leie;->b:[Leig;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    iget-object v2, p0, Leie;->b:[Leig;

    aget-object v2, v2, v1

    iget-object v3, p1, Lekz;->f:[Lela;

    const/4 v6, 0x0

    aget-object v3, v3, v6

    iget-object v3, v3, Lela;->a:[Lelb;

    aget-object v3, v3, v1

    move-object/from16 v0, p2

    iget-object v6, v0, Lehh;->k:[Leii;

    iget v2, v2, Leig;->a:I

    aget-object v2, v6, v2

    iget-object v6, v2, Leii;->a:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iget-object v8, v3, Lelb;->a:[I

    iget-object v9, v3, Lelb;->b:[B

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p4

    array-length v10, v0

    if-ge v2, v10, :cond_4

    aget v10, v8, v2

    if-lez v10, :cond_3

    :try_start_0
    aget-object v10, p4, v2

    new-instance v11, Ljava/lang/String;

    aget v12, v8, v2

    const-string v13, "UTF-8"

    invoke-direct {v11, v9, v3, v12, v13}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v7, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    aget v10, v8, v2

    add-int/2addr v3, v10

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Encoding utf8 not available"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    move-object v1, v4

    goto/16 :goto_0
.end method

.method public static a([Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    new-instance v1, Landroid/os/Bundle;

    array-length v0, p0

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(I)V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lehx;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 14

    if-nez p0, :cond_0

    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    const-string v1, "internal error"

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v7, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    iget-object v0, p0, Lehx;->b:[Lehu;

    array-length v0, v0

    new-array v13, v0, [Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    const/4 v0, 0x0

    move v12, v0

    :goto_1
    iget-object v0, p0, Lehx;->b:[Lehu;

    array-length v0, v0

    if-ge v12, v0, :cond_1

    iget-object v0, p0, Lehx;->b:[Lehu;

    aget-object v10, v0, v12

    iget v0, v10, Lehu;->a:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lejf;

    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    iget-object v1, v10, Lehu;->b:[B

    iget-object v2, v10, Lehu;->c:[B

    iget v3, v5, Lejf;->a:I

    iget-object v4, v5, Lejf;->c:Ljava/lang/String;

    iget-object v5, v5, Lejf;->b:Ljava/lang/String;

    iget-boolean v6, v10, Lehu;->d:Z

    iget-wide v8, v10, Lehu;->e:J

    iget-wide v10, v10, Lehu;->f:J

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/appdatasearch/PIMEUpdate;-><init>([B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJ)V

    aput-object v0, v13, v12

    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    iget-object v1, p0, Lehx;->a:Leht;

    invoke-static {v1}, Lizs;->a(Lizs;)[B

    move-result-object v1

    invoke-direct {v0, v1, v13}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>([B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    goto :goto_0
.end method

.method public static a(Leid;Lekz;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 15

    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    new-array v7, v1, [Landroid/os/Bundle;

    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    new-array v8, v1, [Landroid/os/Bundle;

    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    new-array v9, v1, [Landroid/os/Bundle;

    new-instance v5, Landroid/util/SparseIntArray;

    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    invoke-direct {v5, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p0, Leid;->a:[Leie;

    array-length v1, v1

    if-ge v2, v1, :cond_2

    iget-object v1, p0, Leid;->a:[Leie;

    aget-object v6, v1, v2

    iget v1, v6, Leie;->a:I

    invoke-virtual {v5, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget v1, v6, Leie;->a:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v1, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, "-"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v10, v1, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    aput-object v3, v8, v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    aput-object v3, v9, v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lekz;->f:[Lela;

    aget-object v10, v3, v2

    const/4 v3, 0x0

    :goto_1
    iget-object v11, v6, Leie;->b:[Leig;

    array-length v11, v11

    if-ge v3, v11, :cond_0

    iget-object v11, v6, Leie;->b:[Leig;

    aget-object v11, v11, v3

    iget-object v12, v10, Lela;->a:[Lelb;

    aget-object v12, v12, v3

    iget-object v13, v1, Lehh;->k:[Leii;

    iget v11, v11, Leig;->a:I

    aget-object v11, v13, v11

    iget-object v11, v11, Leii;->a:Ljava/lang/String;

    aget-object v13, v8, v2

    iget-object v14, v12, Lelb;->a:[I

    invoke-virtual {v13, v11, v14}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    aget-object v13, v9, v2

    iget-object v12, v12, Lelb;->b:[B

    invoke-virtual {v13, v11, v12}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    aput-object v1, v7, v2

    const/4 v1, 0x0

    :goto_2
    iget-object v3, v6, Leie;->c:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, v6, Leie;->c:[Ljava/lang/String;

    aget-object v3, v3, v1

    iget-object v11, v10, Lela;->b:[Lelc;

    aget-object v11, v11, v1

    aget-object v12, v7, v2

    iget-object v11, v11, Lelc;->a:[Z

    invoke-virtual {v12, v3, v11}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lekz;->c:[I

    const/4 v1, 0x0

    :goto_3
    move-object/from16 v0, p1

    iget v2, v0, Lekz;->a:I

    if-ge v1, v2, :cond_3

    aget v2, v3, v1

    invoke-virtual {v5, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    aput v2, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    iget-boolean v1, p0, Leid;->d:Z

    if-eqz v1, :cond_4

    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    move-object/from16 v0, p1

    iget v2, v0, Lekz;->a:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lekz;->d:[I

    move-object/from16 v0, p1

    iget-object v6, v0, Lekz;->e:[B

    move-object/from16 v0, p1

    iget-object v10, v0, Lekz;->g:[B

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;[B)V

    :goto_4
    return-object v1

    :cond_4
    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    move-object/from16 v0, p1

    iget v2, v0, Lekz;->a:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v10, v0, Lekz;->g:[B

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;[B)V

    goto :goto_4
.end method

.method public static a([Ljava/lang/String;Lekz;)[Ljava/lang/String;
    .locals 10

    const/4 v0, 0x0

    new-instance v3, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v4, p0

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, p0, v2

    iget v6, p1, Lekz;->a:I

    if-ge v0, v6, :cond_1

    :try_start_0
    new-instance v6, Ljava/lang/String;

    iget-object v7, p1, Lekz;->e:[B

    iget-object v8, p1, Lekz;->d:[I

    aget v8, v8, v0

    const-string v9, "UTF-8"

    invoke-direct {v6, v7, v1, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p1, Lekz;->d:[I

    aget v5, v5, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
