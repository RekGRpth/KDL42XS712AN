.class public final Lenb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lenf;

.field final synthetic b:Lemz;


# direct methods
.method protected constructor <init>(Lemz;Lenf;)V
    .locals 0

    iput-object p1, p0, Lenb;->b:Lemz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lenb;->a:Lenf;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "AsyncScheduler running task %s"

    iget-object v1, p0, Lenb;->a:Lenf;

    invoke-static {v0, v1}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lenb;->b:Lemz;

    iget-object v0, v0, Lemz;->d:Ljava/lang/ThreadLocal;

    iget-object v1, p0, Lenb;->a:Lenf;

    iget v1, v1, Lenf;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lenb;->a:Lenf;

    invoke-virtual {v0}, Lenf;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lenb;->b:Lemz;

    iget-object v1, p0, Lenb;->a:Lenf;

    invoke-static {v0}, Lemz;->a(Lemz;)V

    iget-object v0, p0, Lenb;->b:Lemz;

    iget-object v0, v0, Lemz;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lenb;->a:Lenf;

    iget-object v1, v0, Lenf;->i:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    const/4 v1, 0x3

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lenf;->a(II)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lenb;->b:Lemz;

    iget-object v2, p0, Lenb;->a:Lenf;

    invoke-static {v1}, Lemz;->a(Lemz;)V

    iget-object v1, p0, Lenb;->b:Lemz;

    iget-object v1, v1, Lemz;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method
