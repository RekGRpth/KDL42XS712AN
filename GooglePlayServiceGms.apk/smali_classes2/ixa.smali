.class public final Lixa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liwy;


# instance fields
.field public final a:Liwi;

.field public final b:Liwm;

.field public final c:Lixh;

.field public final d:Lixh;

.field public final e:Lixh;

.field public f:J

.field public g:F

.field public h:Liwz;

.field public i:Z


# direct methods
.method private constructor <init>(Liwi;)V
    .locals 6

    const-wide v4, 0x3fa47ae147ae147cL    # 0.04000000000000001

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lixh;

    invoke-direct {v0, v3, v2}, Lixh;-><init>(II)V

    iput-object v0, p0, Lixa;->c:Lixh;

    new-instance v0, Lixh;

    invoke-direct {v0, v3, v3}, Lixh;-><init>(II)V

    iput-object v0, p0, Lixa;->d:Lixh;

    invoke-static {}, Lixh;->a()Lixh;

    move-result-object v0

    iput-object v0, p0, Lixa;->e:Lixh;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lixa;->g:F

    sget-object v0, Liwz;->c:Liwz;

    iput-object v0, p0, Lixa;->h:Liwz;

    iput-boolean v1, p0, Lixa;->i:Z

    iput-object p1, p0, Lixa;->a:Liwi;

    new-instance v0, Liwm;

    invoke-direct {v0, p1}, Liwm;-><init>(Liwi;)V

    iput-object v0, p0, Lixa;->b:Liwm;

    iget-object v0, p0, Lixa;->d:Lixh;

    invoke-virtual {v0, v1, v1, v4, v5}, Lixh;->a(IID)V

    iget-object v0, p0, Lixa;->d:Lixh;

    invoke-virtual {v0, v2, v2, v4, v5}, Lixh;->a(IID)V

    return-void
.end method

.method public static b()Lixa;
    .locals 9

    const/4 v8, 0x5

    const/4 v5, 0x2

    new-instance v2, Lixi;

    invoke-direct {v2, v5, v5}, Lixi;-><init>(II)V

    new-instance v3, Lixi;

    invoke-direct {v3, v5, v5}, Lixi;-><init>(II)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    new-instance v4, Liwj;

    new-instance v6, Lixh;

    const/4 v7, 0x1

    invoke-direct {v6, v5, v7}, Lixh;-><init>(II)V

    new-instance v7, Lixh;

    invoke-direct {v7, v5, v5}, Lixh;-><init>(II)V

    invoke-direct {v4, v6, v7, v2, v3}, Liwj;-><init>(Lixh;Lixh;Lixi;Lixi;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Liwk;

    const/4 v2, 0x3

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    invoke-direct/range {v0 .. v5}, Liwk;-><init>(Ljava/util/Collection;IDI)V

    new-instance v1, Lixa;

    invoke-direct {v1, v0}, Lixa;-><init>(Liwi;)V

    return-object v1
.end method


# virtual methods
.method public final a()Liwe;
    .locals 4

    iget v0, p0, Lixa;->g:F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lixa;->h:Liwz;

    sget-object v1, Liwz;->c:Liwz;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lixa;->i:Z

    if-nez v0, :cond_1

    iget v0, p0, Lixa;->g:F

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    double-to-int v0, v0

    if-gez v0, :cond_0

    add-int/lit16 v0, v0, 0x168

    :cond_0
    :goto_0
    iget-object v1, p0, Lixa;->b:Liwm;

    invoke-virtual {v1, v0}, Liwm;->a(I)Liwe;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(JLiwe;)V
    .locals 1

    iget-object v0, p0, Lixa;->b:Liwm;

    invoke-virtual {v0, p1, p2, p3}, Liwm;->a(JLiwe;)V

    iput-wide p1, p0, Lixa;->f:J

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lixa;->b:Liwm;

    invoke-virtual {v0}, Liwm;->f()V

    sget-object v0, Liwz;->c:Liwz;

    iput-object v0, p0, Lixa;->h:Liwz;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lixa;->g:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lixa;->i:Z

    return-void
.end method
