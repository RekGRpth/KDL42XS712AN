.class final Lffp;
.super Lffo;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lfbz;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p6}, Lffo;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;)V

    iput-object p4, p0, Lffp;->c:Ljava/lang/String;

    iput-object p5, p0, Lffp;->d:Ljava/lang/String;

    iput p7, p0, Lffp;->e:I

    return-void
.end method


# virtual methods
.method public final c()[B
    .locals 8

    const/4 v3, 0x0

    iget-object v0, p0, Lffp;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->n()Lfhq;

    move-result-object v7

    iget-object v1, p0, Lffp;->c:Ljava/lang/String;

    iget-object v2, p0, Lffp;->d:Ljava/lang/String;

    iget v4, p0, Lffp;->e:I

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfbd;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v7, v0}, Lfhq;->a(I)I

    move-result v5

    new-instance v0, Lfip;

    iget-object v4, v7, Lfhq;->a:Landroid/content/Context;

    invoke-direct {v0, v4, v1, v3}, Lfip;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lfip;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0, v2}, Lfip;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :cond_1
    iget-object v0, v7, Lfhq;->b:Lfjn;

    invoke-static {v6}, Lfjp;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, v5}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v6, v5}, Lfjp;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v7, Lfhq;->a:Landroid/content/Context;

    invoke-static {v3}, Lfft;->a(Landroid/content/Context;)Lfft;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Lfft;->a(Ljava/lang/String;Z)[B

    move-result-object v4

    if-eqz v4, :cond_2

    sget-object v0, Lffl;->a:[B

    if-eq v4, v0, :cond_2

    sget-object v0, Lffl;->b:[B

    if-eq v4, v0, :cond_2

    iget-object v0, v7, Lfhq;->b:Lfjn;

    invoke-static {v6}, Lfjp;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BI)Z

    :cond_2
    iget-object v0, v7, Lfhq;->b:Lfjn;

    invoke-static {v6}, Lfjp;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfjn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_1
.end method
