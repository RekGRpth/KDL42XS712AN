.class public final Lcvi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "REPLACE_ME"

    aput-object v1, v0, v2

    sput-object v0, Lcvi;->a:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "external_participant_id"

    aput-object v1, v0, v2

    sput-object v0, Lcvi;->b:[Ljava/lang/String;

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I
    .locals 16

    invoke-virtual/range {p2 .. p2}, Ldms;->getRoom()Ldow;

    move-result-object v12

    if-nez v12, :cond_1

    const/4 v5, -0x1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {v12}, Ldow;->getCreationDetails()Ldpe;

    move-result-object v3

    invoke-virtual {v12}, Ldow;->getLastUpdateDetails()Ldpe;

    move-result-object v4

    invoke-virtual {v12}, Ldow;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_b

    invoke-virtual {v3}, Ldpe;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_1
    invoke-virtual {v3}, Ldpe;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    if-nez v4, :cond_2

    move-wide v4, v6

    :goto_2
    invoke-virtual {v12}, Ldow;->getParticipants()Ljava/util/ArrayList;

    move-result-object v3

    if-nez v3, :cond_3

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No participants found for invitation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ldow;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Ldpe;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-wide v4, v3

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v13

    move v11, v1

    :goto_3
    if-ge v11, v13, :cond_a

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldph;

    invoke-virtual {v1}, Ldph;->c()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    const/4 v9, 0x1

    invoke-virtual {v1}, Ldph;->getPlayer()Ldnz;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v11}, Ldnz;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11}, Ldnz;->c()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v15, v9

    move-object v9, v8

    move v8, v15

    :goto_4
    if-eqz v8, :cond_4

    if-nez v9, :cond_7

    :cond_4
    const-string v1, "MultiplayerUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No inviting player found for external ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1}, Ldph;->getAutoMatchedPlayer()Ldme;

    move-result-object v11

    if-eqz v11, :cond_a

    invoke-virtual {v1}, Ldph;->getAutoMatchedPlayer()Ldme;

    move-result-object v1

    invoke-virtual {v1}, Ldme;->c()Ljava/lang/String;

    move-result-object v1

    move v15, v8

    move v8, v9

    move-object v9, v1

    move v1, v15

    goto :goto_4

    :cond_6
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_3

    :cond_7
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "external_invitation_id"

    invoke-virtual {v12}, Ldow;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "external_inviter_id"

    invoke-virtual {v8, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "creation_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v8, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "last_modified_timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "description"

    invoke-virtual {v12}, Ldow;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "type"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "inviter_in_circles"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v12}, Ldow;->g()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_8

    const-string v1, "variant"

    invoke-virtual {v12}, Ldow;->g()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_8
    invoke-virtual {v12}, Ldow;->getAutoMatchingCriteria()Ldoy;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string v2, "has_automatch_criteria"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "automatch_min_players"

    invoke-virtual {v1}, Ldoy;->d()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "automatch_max_players"

    invoke-virtual {v1}, Ldoy;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_5
    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Adding invitation for room "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ldow;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static/range {p1 .. p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {p1 .. p1}, Ldjn;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "invitation_id=?"

    sget-object v4, Lcvi;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "invitation_id"

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;ILbpe;Ljava/util/ArrayList;)V

    invoke-virtual/range {p2 .. p2}, Ldms;->getNotification()Ldmt;

    move-result-object v8

    invoke-virtual {v12}, Ldow;->e()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-wide/from16 v9, p3

    invoke-static/range {v6 .. v12}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmt;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_9
    const-string v1, "has_automatch_criteria"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "automatch_min_players"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "automatch_max_players"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    move v1, v8

    move v8, v9

    move-object v9, v10

    goto/16 :goto_4

    :cond_b
    move-object v2, v1

    goto/16 :goto_1
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpp;JLbpe;Ljava/util/ArrayList;)I
    .locals 13

    if-nez p2, :cond_1

    const/4 v5, -0x1

    :cond_0
    :goto_0
    return v5

    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    iget-object v1, p2, Lbni;->a:Landroid/content/ContentValues;

    invoke-direct {v2, v1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual {p2}, Ldpp;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ldpp;->getCreationDetails()Ldpt;

    move-result-object v4

    invoke-virtual {v4}, Ldpt;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Ldpp;->getLastUpdateDetails()Ldpt;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v1, 0x0

    move-object v8, v1

    :goto_1
    const-string v1, "pending_participant_external"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2}, Ldpp;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    if-nez v11, :cond_3

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No participants found for match "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Ldpt;->c()Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_1

    :cond_3
    invoke-static {p1, v3}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "creator_external"

    invoke-virtual {v2, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "creation_timestamp"

    invoke-virtual {v4}, Ldpt;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "last_updater_external"

    invoke-virtual {v2, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v5, :cond_4

    const-string v1, "last_updated_timestamp"

    invoke-virtual {v5}, Ldpt;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v1, "pending_participant_external"

    invoke-virtual {v2, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ldpp;->getData()Ldpr;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v3, "data"

    invoke-virtual {v1}, Ldpr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbpd;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_5
    invoke-virtual {p2}, Ldpp;->getPreviousMatchData()Ldpr;

    move-result-object v1

    if-eqz v1, :cond_6

    const-string v3, "previous_match_data"

    invoke-virtual {v1}, Ldpr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbpd;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_2
    invoke-virtual {p2}, Ldpp;->getAutoMatchingCriteria()Ldpo;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v3, "has_automatch_criteria"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "automatch_min_players"

    invoke-virtual {v1}, Ldpo;->d()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "automatch_max_players"

    invoke-virtual {v1}, Ldpo;->c()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "automatch_bit_mask"

    invoke-virtual {v1}, Ldpo;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_3
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {p1}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Ldjn;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "match_id=?"

    sget-object v3, Lcvi;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "match_id"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpp;Ljava/lang/String;ILbpe;Ljava/util/ArrayList;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v1

    :goto_4
    if-ge v6, v7, :cond_8

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldpu;

    invoke-virtual {v1}, Ldpu;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    :goto_7
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_4

    :cond_6
    const-string v1, "previous_match_data"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    const-string v1, "has_automatch_criteria"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "automatch_min_players"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "automatch_max_players"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "automatch_bit_mask"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_8
    if-nez v2, :cond_9

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No match-creating player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto/16 :goto_0

    :cond_9
    if-eqz v8, :cond_a

    if-nez v3, :cond_a

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No match-updating player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto/16 :goto_0

    :cond_a
    if-eqz v10, :cond_0

    if-nez v4, :cond_0

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No pending player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto/16 :goto_0

    :cond_b
    move v3, v4

    goto :goto_7

    :cond_c
    move v2, v3

    goto/16 :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_5
.end method

.method static a(Ljava/lang/String;Lsp;I)I
    .locals 2

    invoke-static {p1, p0}, Lbng;->b(Lsp;Ljava/lang/String;)Lbne;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MatchCreationNotAllowed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 p2, 0x1770

    goto :goto_0

    :cond_2
    const-string v1, "TrustedTestersOnly"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 p2, 0x1771

    goto :goto_0

    :cond_3
    const-string v1, "InvalidMatchType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 p2, 0x1772

    goto :goto_0

    :cond_4
    const-string v1, "MultiplayerDisabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 p2, 0x1773

    goto :goto_0

    :cond_5
    const-string v1, "InactiveRoom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 p2, 0x1b5d

    goto :goto_0

    :cond_6
    const-string v1, "InvalidParticipantState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 p2, 0x1964

    goto :goto_0

    :cond_7
    const-string v1, "InactiveMatch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 p2, 0x1965

    goto :goto_0

    :cond_8
    const-string v1, "InvalidState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 p2, 0x1966

    goto :goto_0

    :cond_9
    const-string v1, "OutOfDateVersion"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 p2, 0x1967

    goto :goto_0

    :cond_a
    const-string v1, "InvalidMatchResults"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 p2, 0x1968

    goto :goto_0

    :cond_b
    const-string v1, "AlreadyRematched"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 p2, 0x1969

    goto :goto_0

    :cond_c
    const/16 v0, 0x190

    invoke-static {p1, v0}, Lbng;->a(Lsp;I)Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 p2, 0x1774

    goto/16 :goto_0

    :cond_d
    invoke-static {p1}, Lbng;->a(Lsp;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p2, 0x6

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ldph;)Landroid/content/ContentValues;
    .locals 5

    iget-object v0, p1, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {p1}, Ldph;->getClientAddress()Ldoz;

    move-result-object v1

    invoke-virtual {p1}, Ldph;->b()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "connected"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_0
    if-eqz v1, :cond_1

    const-string v2, "client_address"

    invoke-virtual {v1}, Ldoz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ldph;->getAutoMatchedPlayer()Ldme;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "default_display_name"

    invoke-virtual {v1}, Ldme;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "default_display_image_url"

    invoke-virtual {v1}, Ldme;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ldme;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    invoke-static {p0, v0, v1, v2}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    invoke-virtual {p1}, Ldph;->getPlayer()Ldnz;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    invoke-static {p0, v1}, Lcum;->b(Landroid/content/Context;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 19

    invoke-virtual/range {p2 .. p2}, Ldow;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    const-string v3, "MultiplayerUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Room is already deleted : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Ldow;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v7, -0x1

    cmp-long v3, v3, v7

    if-nez v3, :cond_1

    const-string v3, "MultiplayerUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No game found matching external game ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p2

    iget-object v8, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual/range {p2 .. p2}, Ldow;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Ldow;->getCreationDetails()Ldpe;

    move-result-object v5

    const-string v3, "Creation Details cannot be null for room with status: %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Ldow;->f()Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Ldpe;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Ldow;->getLastUpdateDetails()Ldpe;

    move-result-object v7

    if-nez v7, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-virtual/range {p2 .. p2}, Ldow;->getParticipants()Ljava/util/ArrayList;

    move-result-object v10

    if-nez v10, :cond_3

    const-string v3, "MultiplayerUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No participants found for room "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Ldpe;->c()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v4, "creator_external"

    invoke-virtual {v8, v4, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "creation_timestamp"

    invoke-virtual {v5}, Ldpe;->b()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "last_updater_external"

    invoke-virtual {v8, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v7, :cond_4

    const-string v3, "last_updated_timestamp"

    invoke-virtual {v7}, Ldpe;->b()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    invoke-virtual/range {p2 .. p2}, Ldow;->getAutoMatchingCriteria()Ldoy;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v4, "has_automatch_criteria"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v4, "automatch_min_players"

    invoke-virtual {v3}, Ldoy;->d()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "automatch_max_players"

    invoke-virtual {v3}, Ldoy;->c()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "automatch_bit_mask"

    invoke-virtual {v3}, Ldoy;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v8, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_2
    invoke-virtual/range {p2 .. p2}, Ldow;->getAutoMatchingStatus()Ldox;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ldox;->b()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_6

    const-string v4, "automatch_wait_estimate_sec"

    invoke-virtual {v3}, Ldox;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-static/range {p1 .. p1}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v12

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v3, 0x0

    move v7, v3

    :goto_4
    if-ge v7, v11, :cond_7

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldph;

    invoke-virtual {v3}, Ldph;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    :goto_5
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcvi;->a(Landroid/content/Context;Ldph;)Landroid/content/ContentValues;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v18, "default_display_image_url"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v12, v0, v1}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v18, "default_display_hi_res_image_url"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v12, v0, v1}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v18, "profile_icon_image_url"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v12, v0, v1}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v18, "profile_hi_res_image_url"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v12, v0, v1}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ldph;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v4

    goto/16 :goto_4

    :cond_5
    const-string v3, "has_automatch_criteria"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "automatch_min_players"

    invoke-virtual {v8, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "automatch_max_players"

    invoke-virtual {v8, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "automatch_bit_mask"

    invoke-virtual {v8, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    const-string v3, "automatch_wait_estimate_sec"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v11, :cond_8

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v11, :cond_9

    const/4 v3, 0x1

    :goto_7
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v11, :cond_a

    const/4 v3, 0x1

    :goto_8
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v11, :cond_b

    const/4 v3, 0x1

    :goto_9
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "MultiplayerUtils"

    move-object/from16 v0, v17

    invoke-static {v3, v0, v4}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v3, 0x0

    move v7, v3

    :goto_a
    if-ge v7, v11, :cond_c

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldph;

    invoke-virtual {v3}, Ldph;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    const-string v12, "default_display_image_url"

    const-string v17, "default_display_image_uri"

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v17

    invoke-static {v3, v12, v0, v8, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v12, "default_display_hi_res_image_url"

    const-string v17, "default_display_hi_res_image_uri"

    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v17

    invoke-static {v3, v12, v0, v8, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v12, "profile_icon_image_url"

    const-string v17, "profile_icon_image_uri"

    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v17

    invoke-static {v3, v12, v0, v8, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v12, "profile_hi_res_image_url"

    const-string v17, "profile_hi_res_image_uri"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v17

    invoke-static {v3, v12, v0, v8, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_a

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_9
    const/4 v3, 0x0

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    goto :goto_8

    :cond_b
    const/4 v3, 0x0

    goto :goto_9

    :cond_c
    if-nez v5, :cond_d

    const-string v3, "MultiplayerUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No room-creating player found for external ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_d
    move-object v3, v6

    goto/16 :goto_0

    :cond_e
    move v4, v5

    goto/16 :goto_5
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 12

    const/4 v6, 0x0

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcwh;->n:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const-wide/16 v7, -0x1

    const-string v11, "3"

    invoke-static {p1}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Lblt;

    invoke-direct {v0, v1}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v2, "user_match_status"

    invoke-virtual {v0, v2, v11}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcvj;->a:[Ljava/lang/String;

    invoke-virtual {v0}, Lblt;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lblt;->c:[Ljava/lang/String;

    const-string v5, "game_id,last_updated_timestamp DESC"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v5

    move v0, v6

    move-wide v1, v7

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    cmp-long v7, v3, v1

    if-eqz v7, :cond_1

    move v0, v6

    move-wide v1, v3

    :cond_1
    if-ge v0, v10, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lblt;

    invoke-direct {v1, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v6, "user_match_status"

    invoke-virtual {v1, v6, v11}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "last_updated_timestamp"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "<=?"

    invoke-virtual {v1, v6, v4, v5}, Lblt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lblt;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v1, Lblt;->c:[Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "MultiplayerUtils"

    invoke-static {v0, v2, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_5
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpp;Ljava/lang/String;ILbpe;Ljava/util/ArrayList;)V
    .locals 12

    invoke-virtual {p2}, Ldpp;->getParticipants()Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {p1}, Ldjn;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    const/4 v1, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v1

    :goto_0
    if-ge v6, v9, :cond_d

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldpu;

    invoke-virtual {v1}, Ldpu;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual {p2}, Ldpp;->getResults()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_1
    const/4 v3, -0x1

    invoke-virtual {v1}, Ldpu;->getPlayer()Ldnz;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget-object v4, v4, Lbni;->a:Landroid/content/ContentValues;

    invoke-interface/range {p5 .. p5}, Lbpe;->a()J

    move-result-wide v10

    invoke-static {p0, p1, v4, v10, v11}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v5, v1, Lbni;->a:Landroid/content/ContentValues;

    if-eqz v2, :cond_3

    const-string v10, "result_type"

    invoke-virtual {v2}, Ldnr;->d()Ljava/lang/String;

    move-result-object v4

    const-string v11, "MATCH_RESULT_WIN"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    const/4 v4, 0x0

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v10, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "placing"

    invoke-virtual {v2}, Ldnr;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    invoke-virtual {v1}, Ldpu;->getAutoMatchedPlayer()Ldme;

    move-result-object v1

    if-eqz v1, :cond_e

    const-string v2, "default_display_name"

    invoke-virtual {v1}, Ldme;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "default_display_image_url"

    invoke-virtual {v1}, Ldme;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ldme;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    invoke-static {p0, v5, v1, v2}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    :goto_3
    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, p3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, -0x1

    if-eq v3, v2, :cond_4

    const-string v2, "player_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    :cond_4
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v3, v2

    :goto_4
    if-ge v3, v10, :cond_6

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldnr;

    invoke-virtual {v2}, Ldnr;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_7
    const-string v11, "MATCH_RESULT_LOSS"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v4, 0x1

    goto :goto_2

    :cond_8
    const-string v11, "MATCH_RESULT_TIE"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    const/4 v4, 0x2

    goto/16 :goto_2

    :cond_9
    const-string v11, "MATCH_RESULT_NONE"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v4, 0x3

    goto/16 :goto_2

    :cond_a
    const-string v11, "MATCH_RESULT_DISCONNECT"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    const/4 v4, 0x4

    goto/16 :goto_2

    :cond_b
    const-string v11, "MATCH_RESULT_DISAGREED"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    const/4 v4, 0x5

    goto/16 :goto_2

    :cond_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown result string: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    return-void

    :cond_e
    move-object v1, v5

    goto/16 :goto_3
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;ILbpe;Ljava/util/ArrayList;)V
    .locals 9

    const/4 v2, -0x1

    invoke-static {p1}, Ldjn;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldph;

    invoke-virtual {v0}, Ldph;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldgd;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-virtual {v0}, Ldph;->getPlayer()Ldnz;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p6}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v3, v3, Lbni;->a:Landroid/content/ContentValues;

    invoke-interface {p5}, Lbpe;->a()J

    move-result-wide v7

    invoke-static {p0, p1, v3, v7, v8}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v3, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v0}, Ldph;->getClientAddress()Ldoz;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v8, "client_address"

    invoke-virtual {v7}, Ldoz;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ldph;->getAutoMatchedPlayer()Ldme;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v7, "default_display_name"

    invoke-virtual {v0}, Ldme;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "default_display_image_url"

    invoke-virtual {v0}, Ldme;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldme;->b()Ljava/lang/String;

    move-result-object v0

    const-string v7, "default_display_hi_res_image_url"

    invoke-static {p0, v3, v0, v7}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    :goto_2
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    if-eq v1, v2, :cond_1

    const-string v3, "player_id"

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    :cond_1
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method static a(Landroid/content/Context;Ljava/util/HashMap;Landroid/content/ContentValues;Ldpi;)V
    .locals 11

    invoke-virtual {p3}, Ldpi;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Ldpi;->c()Ljava/lang/Integer;

    move-result-object v5

    const/4 v0, -0x1

    invoke-virtual {p3}, Ldpi;->getAutoMatchingStatus()Ldox;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ldox;->b()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ldox;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    :goto_0
    invoke-virtual {p3}, Ldpi;->getParticipants()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_1

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldph;

    invoke-virtual {v0}, Ldph;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    if-eqz v1, :cond_0

    const-string v8, "status"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0, v0}, Lcvi;->a(Landroid/content/Context;Ldph;)Landroid/content/ContentValues;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    :goto_2
    invoke-virtual {v0}, Ldph;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v8, p2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v9, "external_match_id"

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "status"

    invoke-virtual {v8, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "automatch_wait_estimate_sec"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-static {p0, v0}, Lcvi;->a(Landroid/content/Context;Ldph;)Landroid/content/ContentValues;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_2

    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2

    invoke-static {p0, p1}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p0, p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I
    .locals 14

    invoke-virtual/range {p2 .. p2}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v5, -0x1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {v3}, Ldpp;->getCreationDetails()Ldpt;

    move-result-object v8

    invoke-virtual {v3}, Ldpp;->getLastUpdateDetails()Ldpt;

    move-result-object v9

    invoke-virtual {v3}, Ldpp;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    invoke-virtual {v8}, Ldpt;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_1
    invoke-virtual {v3}, Ldpp;->getParticipants()Ljava/util/ArrayList;

    move-result-object v10

    if-nez v10, :cond_2

    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No participants found for invitation "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ldpp;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v7, v1

    :goto_2
    if-ge v7, v11, :cond_8

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldpu;

    invoke-virtual {v1}, Ldpu;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    const/4 v5, 0x1

    invoke-virtual {v1}, Ldpu;->getPlayer()Ldnz;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ldnz;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Ldnz;->c()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v13, v5

    move-object v5, v4

    move v4, v13

    :goto_3
    if-eqz v4, :cond_3

    if-nez v5, :cond_6

    :cond_3
    const-string v1, "MultiplayerUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No inviting player found for external ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Ldpu;->getAutoMatchedPlayer()Ldme;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v1}, Ldpu;->getAutoMatchedPlayer()Ldme;

    move-result-object v1

    invoke-virtual {v1}, Ldme;->c()Ljava/lang/String;

    move-result-object v1

    move v13, v4

    move v4, v5

    move-object v5, v1

    move v1, v13

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_2

    :cond_6
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "external_invitation_id"

    invoke-virtual {v3}, Ldpp;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "external_inviter_id"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "creation_timestamp"

    invoke-virtual {v8}, Ldpt;->b()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "last_modified_timestamp"

    invoke-virtual {v9}, Ldpt;->b()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "inviter_in_circles"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v3}, Ldpp;->getAutoMatchingCriteria()Ldpo;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v2, "has_automatch_criteria"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "automatch_min_players"

    invoke-virtual {v1}, Ldpo;->d()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "automatch_max_players"

    invoke-virtual {v1}, Ldpo;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_4
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Ldjn;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "invitation_id=?"

    sget-object v4, Lcvi;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "invitation_id"

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpp;Ljava/lang/String;ILbpe;Ljava/util/ArrayList;)V

    invoke-virtual/range {p2 .. p2}, Ldms;->getNotification()Ldmt;

    move-result-object v8

    invoke-virtual {v3}, Ldpp;->d()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    move-object v6, p0

    move-object v7, p1

    move-wide/from16 v9, p3

    invoke-static/range {v6 .. v12}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmt;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    const-string v1, "has_automatch_criteria"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "automatch_min_players"

    invoke-virtual {v4, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "automatch_max_players"

    invoke-virtual {v4, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    move v1, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_3

    :cond_9
    move-object v2, v1

    goto/16 :goto_1
.end method

.method static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I
    .locals 8

    invoke-virtual {p2}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpp;JLbpe;Ljava/util/ArrayList;)I

    move-result v7

    const/4 v0, -0x1

    if-eq v7, v0, :cond_0

    invoke-virtual {p2}, Ldms;->getNotification()Ldmt;

    move-result-object v3

    invoke-virtual {v2}, Ldpp;->d()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, v3

    move-wide v3, p3

    invoke-static/range {v0 .. v6}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmt;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return v7
.end method
