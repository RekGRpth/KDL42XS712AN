.class final Lhag;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhad;


# direct methods
.method constructor <init>(Lhad;)V
    .locals 0

    iput-object p1, p0, Lhag;->a:Lhad;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhag;->a:Lhad;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_CANNOT_AUTHENTICATE"

    invoke-static {v0, v1}, Lhad;->e(Lhad;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lipl;)V
    .locals 14

    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->j(Lhad;)Z

    iget-object v0, p0, Lhag;->a:Lhad;

    iput-object p1, v0, Lhad;->an:Lipl;

    iget-object v0, p1, Lipl;->c:Linz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->c:Linz;

    iget-object v2, v2, Linz;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lhad;->a(Lhad;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p1, Lipl;->d:Liob;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->d:Liob;

    iget-boolean v2, v2, Liob;->h:Z

    invoke-static {v0, v2}, Lhad;->a(Lhad;Z)Z

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->d:Liob;

    iget-object v2, v2, Liob;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lhad;->b(Lhad;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-boolean v2, p1, Lipl;->h:Z

    invoke-static {v0, v2}, Lhad;->b(Lhad;Z)Z

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {p1}, Lhad;->a(Lipl;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lhad;->a(Lhad;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lhag;->a:Lhad;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2}, Lhad;->b(Lhad;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v2, p1, Lipl;->e:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    const/16 v5, 0x35

    invoke-static {v4}, Lhgq;->b(Ljava/lang/String;)I

    move-result v6

    if-eq v5, v6, :cond_2

    iget-object v5, p0, Lhag;->a:Lhad;

    invoke-static {v5}, Lhad;->i(Lhad;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lipl;->a:Liol;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->a:Liol;

    iget-object v2, v2, Liol;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lhad;->c(Lhad;Ljava/lang/String;)Ljava/lang/String;

    :cond_4
    iget v0, p1, Lipl;->i:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->l(Lhad;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->d:Liob;

    invoke-static {v0, v2}, Lhad;->a(Lhad;Liob;)V

    :cond_5
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->f:[Liof;

    iget-object v3, p1, Lipl;->j:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lhad;->a(Lhad;[Liof;Ljava/lang/String;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p1, Lipl;->b:[Lion;

    invoke-static {v0, v2}, Lhad;->a(Lhad;[Lion;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->m(Lhad;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->ac:Lion;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-virtual {v0}, Lhad;->a()V

    :cond_6
    :goto_1
    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->n(Lhad;)Luu;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->o(Lhad;)Lut;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->n(Lhad;)Luu;

    move-result-object v0

    iget-object v2, p0, Lhag;->a:Lhad;

    invoke-static {v2}, Lhad;->o(Lhad;)Lut;

    move-result-object v2

    new-array v3, v12, [Ljava/lang/String;

    const-string v4, "create_to_ui_populated"

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-virtual {v0}, Lhad;->T_()Lo;

    move-result-object v0

    iget-object v1, p0, Lhag;->a:Lhad;

    invoke-static {v1}, Lhad;->n(Lhad;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->p(Lhad;)Luu;

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->q(Lhad;)Lut;

    :cond_7
    return-void

    :pswitch_1
    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->a(Lhad;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v2, p0, Lhag;->a:Lhad;

    invoke-static {v2}, Lhad;->b(Lhad;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lhag;->a:Lhad;

    invoke-static {v3}, Lhad;->c(Lhad;)Z

    move-result v3

    iget-object v4, p0, Lhag;->a:Lhad;

    invoke-static {v4}, Lhad;->c(Lhad;)Z

    move-result v4

    iget-object v5, p0, Lhag;->a:Lhad;

    invoke-static {v5}, Lhad;->h(Lhad;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lipl;->l:[Lipj;

    invoke-static {v6}, Lgth;->a([Lipj;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lhag;->a:Lhad;

    invoke-static {v7}, Lhad;->d(Lhad;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lhag;->a:Lhad;

    invoke-static {v8}, Lhad;->e(Lhad;)Lioq;

    move-result-object v8

    iget-object v9, p0, Lhag;->a:Lhad;

    invoke-static {v9}, Lhad;->k(Lhad;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    const-string v11, "com.google.android.gms"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "com.google.android.gms.wallet.ACTION_CREATE_PROFILE"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v10, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v10, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.requiresInstrument"

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v10, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.addressHints"

    invoke-static {v10, v0, v13}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v0, "legalDocsForCountries"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v10, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    invoke-static {v10, v0, v8}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v0, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v10, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lhag;->a:Lhad;

    const/16 v2, 0x1f6

    invoke-virtual {v0, v10, v2}, Lhad;->a(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p0, Lhag;->a:Lhad;

    const v3, 0x7f0b0131    # com.google.android.gms.R.string.wallet_closed_account_error_message

    invoke-virtual {v2, v3}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v2, v3}, Lhad;->a(Lhad;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v2, p0, Lhag;->a:Lhad;

    const v3, 0x7f0b0130    # com.google.android.gms.R.string.wallet_rejected_error_message

    invoke-virtual {v2, v3}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v2, v3}, Lhad;->a(Lhad;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lipn;)V
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->m(Lhad;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget v0, p1, Lipn;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p0, Lhag;->a:Lhad;

    const v2, 0x7f0b0132    # com.google.android.gms.R.string.wallet_unknown_error_message

    invoke-virtual {v1, v2}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_CODE_UNKNOWN"

    invoke-static {v0, v1, v2}, Lhad;->a(Lhad;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p1, Lipn;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lhad;->d(Lhad;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Lipn;->c:[I

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->r(Lhad;)V

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lipn;->c:[I

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->r(Lhad;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->ac:Lion;

    iget-object v2, v0, Lion;->a:Lioj;

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->a(Lhad;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lhag;->a:Lhad;

    invoke-static {v1}, Lhad;->b(Lhad;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v4, p0, Lhag;->a:Lhad;

    invoke-static {v4}, Lhad;->d(Lhad;)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lhag;->a:Lhad;

    invoke-static {v4}, Lhad;->e(Lhad;)Lioq;

    move-result-object v6

    iget-object v4, p0, Lhag;->a:Lhad;

    invoke-static {v4}, Lhad;->f(Lhad;)Ljava/util/ArrayList;

    move-result-object v7

    move v4, v3

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhag;->a:Lhad;

    const/16 v2, 0x1fa

    invoke-virtual {v1, v0, v2}, Lhad;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v8, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v0, p0, Lhag;->a:Lhad;

    iput-boolean v3, v0, Lhad;->ag:Z

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->d:Lgxg;

    invoke-interface {v0, v8}, Lgxg;->a(Lioj;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    iput-object v8, v0, Lhad;->an:Lipl;

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->s(Lhad;)V

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p0, Lhag;->a:Lhad;

    invoke-virtual {v1}, Lhad;->T_()Lo;

    move-result-object v1

    const v2, 0x7f0b011c    # com.google.android.gms.R.string.wallet_declined_card_error_message

    invoke-virtual {v1, v2}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhad;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p0, Lhag;->a:Lhad;

    const v2, 0x7f0b0130    # com.google.android.gms.R.string.wallet_rejected_error_message

    invoke-virtual {v1, v2}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v1, v2}, Lhad;->a(Lhad;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p0, Lhag;->a:Lhad;

    const v2, 0x7f0b0131    # com.google.android.gms.R.string.wallet_closed_account_error_message

    invoke-virtual {v1, v2}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v1, v2}, Lhad;->a(Lhad;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->r(Lhad;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lhag;->a:Lhad;

    const-string v1, "com.google.android.libraries.inapp.ERROR_DELIVERY_FAILED"

    invoke-static {v0, v1}, Lhad;->e(Lhad;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lhag;->a:Lhad;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_SELF_PURCHASE"

    invoke-static {v0, v1}, Lhad;->e(Lhad;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v1, p0, Lhag;->a:Lhad;

    invoke-virtual {v1}, Lhad;->T_()Lo;

    move-result-object v1

    const v2, 0x7f0b0126    # com.google.android.gms.R.string.wallet_missing_legal_document_error_message

    invoke-virtual {v1, v2}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhad;->f(Lhad;Ljava/lang/String;)V

    iget-object v0, p1, Lipn;->d:Lipl;

    invoke-virtual {p0, v0}, Lhag;->a(Lipl;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_5
        :pswitch_9
        :pswitch_2
        :pswitch_1
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhag;->a:Lhad;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_UNKNOWN"

    invoke-static {v0, v1}, Lhad;->e(Lhad;Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->t(Lhad;)Z

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->u(Lhad;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->v(Lhad;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->w(Lhad;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lhag;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhag;->a:Lhad;

    invoke-static {v0}, Lhad;->r(Lhad;)V

    return-void
.end method
