.class public final Ldyq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/database/CharArrayBuffer;

.field final d:Landroid/widget/ImageView;

.field final e:Landroid/widget/ImageView;

.field final f:Landroid/widget/TextView;

.field final g:Landroid/widget/ImageView;

.field final h:Landroid/view/View;

.field final synthetic i:Ldyp;


# direct methods
.method public constructor <init>(Ldyp;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Ldyq;->i:Ldyp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0a016c    # com.google.android.gms.R.id.player_image

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v0, 0x7f0a016d    # com.google.android.gms.R.id.player_name

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyq;->b:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldyq;->c:Landroid/database/CharArrayBuffer;

    const v0, 0x7f0a01e1    # com.google.android.gms.R.id.is_selected

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyq;->d:Landroid/widget/ImageView;

    const v0, 0x7f0a00d9    # com.google.android.gms.R.id.overflow_menu

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyq;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Ldyq;->e:Landroid/widget/ImageView;

    invoke-static {p1}, Ldyp;->a(Ldyp;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a01e2    # com.google.android.gms.R.id.num_autopick

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyq;->f:Landroid/widget/TextView;

    const v0, 0x7f0a0181    # com.google.android.gms.R.id.in_circles_indicator

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyq;->g:Landroid/widget/ImageView;

    const v0, 0x7f0a016e    # com.google.android.gms.R.id.overlay

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldyq;->h:Landroid/view/View;

    iget-object v0, p0, Ldyq;->h:Landroid/view/View;

    invoke-static {p1}, Ldyp;->a(Ldyp;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
