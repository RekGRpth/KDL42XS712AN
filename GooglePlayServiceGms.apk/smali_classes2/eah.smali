.class final Leah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Landroid/view/ViewGroup;

.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Landroid/view/View;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/database/CharArrayBuffer;

.field final f:Landroid/widget/TextView;

.field final g:Landroid/widget/TextView;

.field final h:Landroid/widget/TextView;

.field final i:Landroid/view/View;

.field final j:Ljava/lang/StringBuilder;

.field final k:Lcom/google/android/gms/games/ui/widget/MosaicView;

.field final l:Landroid/view/View;

.field final m:Landroid/view/View;

.field final n:Landroid/widget/TextView;

.field final synthetic o:Leaf;


# direct methods
.method public constructor <init>(Leaf;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Leah;->o:Leaf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->bb:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leah;->a:Landroid/view/ViewGroup;

    sget v0, Lxa;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aX:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leah;->c:Landroid/view/View;

    sget v0, Lxa;->F:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leah;->d:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Leah;->e:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->G:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leah;->f:Landroid/widget/TextView;

    sget v0, Lxa;->ao:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leah;->g:Landroid/widget/TextView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leah;->h:Landroid/widget/TextView;

    sget v0, Lxa;->aG:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leah;->i:Landroid/view/View;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Leah;->j:Ljava/lang/StringBuilder;

    sget v0, Lxa;->aH:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MosaicView;

    iput-object v0, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leah;->l:Landroid/view/View;

    iget-object v0, p0, Leah;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leah;->m:Landroid/view/View;

    iget-object v0, p0, Leah;->m:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->aN:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leah;->n:Landroid/widget/TextView;

    iget-object v0, p0, Leah;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method final a(Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a()V

    if-eqz p4, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v2, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    sget v3, Lwz;->f:I

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a(Landroid/net/Uri;I)V

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    sget v5, Lwz;->f:I

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a(Landroid/net/Uri;I)V

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_2
    if-ge v0, p3, :cond_4

    iget-object v1, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    const/4 v2, 0x0

    sget v3, Lwz;->f:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a(Landroid/net/Uri;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->b()V

    return-void
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v3, v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    if-eqz v3, :cond_4

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxa;->ae:I

    if-ne v3, v4, :cond_1

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-virtual {v1}, Leaf;->e()Lbgo;

    move-result-object v1

    check-cast v1, Lbgx;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lbgx;->a(Ljava/lang/String;)V

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-virtual {v1}, Leaf;->notifyDataSetChanged()V

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    invoke-interface {v1, v0}, Leag;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    sget v4, Lxa;->ai:I

    if-ne v3, v4, :cond_3

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    iget-object v3, p0, Leah;->o:Leaf;

    invoke-static {v3}, Leaf;->d(Leaf;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Leah;->o:Leaf;

    invoke-static {v4}, Leaf;->a(Leaf;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v3, v4}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v1, v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lxa;->ap:I

    if-ne v1, v2, :cond_1

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    sget v1, Lxd;->c:I

    invoke-virtual {v0, v1}, Lov;->a(I)V

    new-instance v1, Ledk;

    invoke-direct {v1, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v1, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lxa;->aN:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    invoke-interface {v1, v0}, Leag;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    :cond_2
    sget v2, Lxa;->aH:I

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    iget-object v2, p0, Leah;->o:Leaf;

    invoke-static {v2}, Leaf;->d(Leaf;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Leah;->o:Leaf;

    invoke-static {v3}, Leaf;->a(Leaf;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Leah;->o:Leaf;

    invoke-static {v0}, Leaf;->b(Leaf;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxf;->aC:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Leah;->o:Leaf;

    invoke-static {v1}, Leaf;->c(Leaf;)Leag;

    move-result-object v1

    invoke-interface {v1, v0}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    :cond_5
    const-string v1, "MatchListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
