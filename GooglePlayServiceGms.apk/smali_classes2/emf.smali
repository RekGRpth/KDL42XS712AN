.class public final Lemf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lehh;


# direct methods
.method constructor <init>(Lehh;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lemf;->a:Lehh;

    return-void
.end method


# virtual methods
.method final a(Lela;)Landroid/util/Pair;
    .locals 5

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lemf;->a:Lehh;

    iget-object v3, v3, Lehh;->l:[Leim;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lemf;->a:Lehh;

    iget-object v3, v3, Lehh;->l:[Leim;

    aget-object v3, v3, v0

    iget v3, v3, Leim;->a:I

    invoke-static {v3}, Laia;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lela;->a:[Lelb;

    aget-object v4, v4, v0

    iget-object v4, v4, Lelb;->a:[I

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    iget-object v4, p1, Lela;->a:[Lelb;

    aget-object v4, v4, v0

    iget-object v4, v4, Lelb;->b:[B

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lemf;->a:Lehh;

    iget-object v1, v1, Lehh;->k:[Leii;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lemf;->a:Lehh;

    iget-object v1, v1, Lehh;->k:[Leii;

    aget-object v1, v1, v0

    iget v2, v1, Leii;->d:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    new-instance v2, Leih;

    invoke-direct {v2}, Leih;-><init>()V

    iput v0, v2, Leih;->b:I

    iget-object v3, p0, Lemf;->a:Lehh;

    iget v3, v3, Lehh;->a:I

    iput v3, v2, Leih;->a:I

    iget v1, v1, Leii;->d:I

    iput v1, v2, Leih;->c:I

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
