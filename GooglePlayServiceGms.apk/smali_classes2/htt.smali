.class public final Lhtt;
.super Lhuj;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lihj;->K:Livk;

    invoke-direct {p0, v0}, Lhuj;-><init>(Livk;)V

    return-void
.end method

.method private static b(Livi;)Lhtr;
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v1}, Livi;->f(I)Livi;

    move-result-object v1

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {v1, v2}, Livi;->f(I)Livi;

    move-result-object v2

    invoke-static {v2}, Lhua;->a(Livi;)Liap;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Livi;->f(I)Livi;

    move-result-object v3

    invoke-static {v3}, Lhua;->b(Livi;)Ljava/util/Map;

    move-result-object v3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Livi;->f(I)Livi;

    move-result-object v5

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Livi;->k(I)I

    move-result v6

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_2

    const/4 v7, 0x1

    invoke-virtual {v5, v7, v1}, Livi;->a(II)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v5, v8, v1}, Livi;->d(II)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    new-instance v5, Lhun;

    invoke-direct {v5, v3, v2}, Lhun;-><init>(Ljava/util/Map;Liap;)V

    new-instance v1, Lhtr;

    invoke-direct {v1, v5, v4}, Lhtr;-><init>(Lhun;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_0

    const-string v1, "LevelSelectorFactory"

    const-string v2, "Binary data of model corrupted."

    invoke-static {v1, v2}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Livi;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lhtt;->b(Livi;)Lhtr;

    move-result-object v0

    return-object v0
.end method
