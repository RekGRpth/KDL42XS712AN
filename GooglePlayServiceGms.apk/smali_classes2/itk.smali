.class public final Litk;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Litj;

.field public b:Litl;

.field public c:Litm;

.field public d:Lito;

.field public e:I

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Litk;->a:Litj;

    iput-object v0, p0, Litk;->b:Litl;

    iput-object v0, p0, Litk;->c:Litm;

    iput-object v0, p0, Litk;->d:Lito;

    iput v1, p0, Litk;->e:I

    iput-boolean v1, p0, Litk;->f:Z

    iput-boolean v1, p0, Litk;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Litk;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Litk;->a:Litj;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Litk;->a:Litj;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Litk;->b:Litl;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Litk;->b:Litl;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Litk;->c:Litm;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Litk;->c:Litm;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Litk;->d:Lito;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Litk;->d:Lito;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Litk;->e:I

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Litk;->e:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Litk;->f:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Litk;->f:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Litk;->g:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Litk;->g:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Litk;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Litk;->a:Litj;

    if-nez v0, :cond_1

    new-instance v0, Litj;

    invoke-direct {v0}, Litj;-><init>()V

    iput-object v0, p0, Litk;->a:Litj;

    :cond_1
    iget-object v0, p0, Litk;->a:Litj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Litk;->b:Litl;

    if-nez v0, :cond_2

    new-instance v0, Litl;

    invoke-direct {v0}, Litl;-><init>()V

    iput-object v0, p0, Litk;->b:Litl;

    :cond_2
    iget-object v0, p0, Litk;->b:Litl;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Litk;->c:Litm;

    if-nez v0, :cond_3

    new-instance v0, Litm;

    invoke-direct {v0}, Litm;-><init>()V

    iput-object v0, p0, Litk;->c:Litm;

    :cond_3
    iget-object v0, p0, Litk;->c:Litm;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Litk;->d:Lito;

    if-nez v0, :cond_4

    new-instance v0, Lito;

    invoke-direct {v0}, Lito;-><init>()V

    iput-object v0, p0, Litk;->d:Lito;

    :cond_4
    iget-object v0, p0, Litk;->d:Lito;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Litk;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Litk;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Litk;->g:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Litk;->a:Litj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Litk;->a:Litj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Litk;->b:Litl;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Litk;->b:Litl;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Litk;->c:Litm;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Litk;->c:Litm;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Litk;->d:Lito;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Litk;->d:Lito;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    iget v0, p0, Litk;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Litk;->e:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_4
    iget-boolean v0, p0, Litk;->f:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Litk;->f:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Litk;->g:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Litk;->g:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_6
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Litk;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Litk;

    iget-object v2, p0, Litk;->a:Litj;

    if-nez v2, :cond_3

    iget-object v2, p1, Litk;->a:Litj;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Litk;->a:Litj;

    iget-object v3, p1, Litk;->a:Litj;

    invoke-virtual {v2, v3}, Litj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Litk;->b:Litl;

    if-nez v2, :cond_5

    iget-object v2, p1, Litk;->b:Litl;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Litk;->b:Litl;

    iget-object v3, p1, Litk;->b:Litl;

    invoke-virtual {v2, v3}, Litl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Litk;->c:Litm;

    if-nez v2, :cond_7

    iget-object v2, p1, Litk;->c:Litm;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Litk;->c:Litm;

    iget-object v3, p1, Litk;->c:Litm;

    invoke-virtual {v2, v3}, Litm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Litk;->d:Lito;

    if-nez v2, :cond_9

    iget-object v2, p1, Litk;->d:Lito;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Litk;->d:Lito;

    iget-object v3, p1, Litk;->d:Lito;

    invoke-virtual {v2, v3}, Lito;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Litk;->e:I

    iget v3, p1, Litk;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-boolean v2, p0, Litk;->f:Z

    iget-boolean v3, p1, Litk;->f:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-boolean v2, p0, Litk;->g:Z

    iget-boolean v3, p1, Litk;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Litk;->a:Litj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Litk;->b:Litl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Litk;->c:Litm;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Litk;->d:Lito;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Litk;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Litk;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Litk;->g:Z

    if-eqz v1, :cond_5

    :goto_5
    add-int/2addr v0, v2

    return v0

    :cond_0
    iget-object v0, p0, Litk;->a:Litj;

    invoke-virtual {v0}, Litj;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Litk;->b:Litl;

    invoke-virtual {v0}, Litl;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Litk;->c:Litm;

    invoke-virtual {v0}, Litm;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Litk;->d:Lito;

    invoke-virtual {v1}, Lito;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v0, v3

    goto :goto_4

    :cond_5
    move v2, v3

    goto :goto_5
.end method
