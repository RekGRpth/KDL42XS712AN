.class public final Lbra;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lchb;


# instance fields
.field final a:Ljava/util/Set;

.field private final b:Lchd;


# direct methods
.method public constructor <init>(Lchd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbra;->a:Ljava/util/Set;

    iput-object p1, p0, Lbra;->b:Lchd;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;)V
    .locals 9

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lbra;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbra;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbre;

    invoke-virtual {v0}, Lbre;->b()Lbrc;

    move-result-object v0

    invoke-interface {v0, p1}, Lbrc;->a(Lcom/google/android/gms/drive/events/DriveEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Lbrc;->a()Lbsp;

    move-result-object v0

    iget-object v0, v0, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    :try_start_2
    iget-object v0, p0, Lbra;->b:Lchd;

    invoke-virtual {v0, p1}, Lchd;->a(Lcom/google/android/gms/drive/events/DriveEvent;)Lcgs;

    move-result-object v1

    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgu;

    iget-object v4, v0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lbra;->b:Lchd;

    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v0, Lcgu;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcgu;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5, p1}, Lcha;->a(Landroid/content/ComponentName;Lcom/google/android/gms/drive/events/DriveEvent;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    :try_start_3
    iget-object v4, v4, Lchd;->a:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v4, "SubscriptionStore"

    const-string v6, "Auto-removing subscription due to invalid target: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-static {v4, v6, v7}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcgu;->l()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v4

    :try_start_4
    const-string v4, "SubscriptionStore"

    const-string v5, "Error auto-removing subscription: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_5
    const-string v2, "ClientStore"

    const-string v3, "Error raising event to subscribers"

    invoke-static {v2, v0, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcgs;->close()V

    :cond_3
    :goto_2
    return-void

    :catch_2
    move-exception v0

    :try_start_6
    const-string v4, "SubscriptionStore"

    const-string v5, "Unable to send intent"

    invoke-static {v4, v0, v5}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcgs;->close()V

    :cond_4
    throw v0

    :cond_5
    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcgs;->close()V

    goto :goto_2
.end method
