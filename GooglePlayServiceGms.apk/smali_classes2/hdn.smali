.class final Lhdn;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

.field final synthetic f:Lhdj;


# direct methods
.method constructor <init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)V
    .locals 1

    iput-object p1, p0, Lhdn;->f:Lhdj;

    iput-object p4, p0, Lhdn;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lhdn;->e:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    const/16 v0, 0x11

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    iget-object v0, p0, Lhdn;->f:Lhdj;

    invoke-static {v0}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v0, p0, Lhdn;->f:Lhdj;

    iget-object v0, p0, Lhdn;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhdn;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lhdn;->e:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->b()Liou;

    move-result-object v5

    iget-object v0, p0, Lhdn;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdn;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhda;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lhda;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Liou;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "billing_make_payment"

    invoke-static {v8, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
