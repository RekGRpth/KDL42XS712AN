.class public final Lbwa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Lclm;

.field final c:Ljava/lang/Exception;

.field final d:Ljava/lang/String;


# direct methods
.method constructor <init>(ILclm;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_0

    if-nez p2, :cond_2

    if-nez p4, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    :cond_0
    if-eqz p2, :cond_1

    if-nez p3, :cond_3

    if-nez p4, :cond_3

    :goto_1
    invoke-static {v1}, Lbkm;->b(Z)V

    :cond_1
    iput p1, p0, Lbwa;->a:I

    iput-object p2, p0, Lbwa;->b:Lclm;

    iput-object p3, p0, Lbwa;->c:Ljava/lang/Exception;

    iput-object p4, p0, Lbwa;->d:Ljava/lang/String;

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lbwa;->b:Lclm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwa;->c:Ljava/lang/Exception;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lbwa;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lbwa;

    iget v2, p0, Lbwa;->a:I

    iget v3, p1, Lbwa;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lbwa;->b:Lclm;

    iget-object v3, p1, Lbwa;->b:Lclm;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbwa;->c:Ljava/lang/Exception;

    iget-object v3, p1, Lbwa;->c:Ljava/lang/Exception;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbwa;->d:Ljava/lang/String;

    iget-object v3, p1, Lbwa;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lbwa;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lbwa;->b:Lclm;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lbwa;->c:Ljava/lang/Exception;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lbwa;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ResultsQueueItem[id=%d, resultsPage=%s, e=%s, nextPageToken=%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lbwa;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbwa;->b:Lclm;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lbwa;->c:Ljava/lang/Exception;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lbwa;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
