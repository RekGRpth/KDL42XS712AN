.class public final Lcbx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcov;


# instance fields
.field private final a:Lcbf;

.field private final b:Lcby;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcon;Lcby;Ljava/lang/String;Lbfy;Lbfy;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcbx;->c:Ljava/lang/String;

    iput-object p2, p0, Lcbx;->b:Lcby;

    invoke-interface {p2, p3}, Lcby;->b(Ljava/lang/String;)J

    move-result-wide v5

    invoke-interface {p2, p3}, Lcby;->a(Ljava/lang/String;)I

    move-result v4

    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-ltz v0, :cond_0

    if-gez v4, :cond_1

    :cond_0
    invoke-interface {p1}, Lcon;->a()J

    move-result-wide v5

    invoke-virtual {p4}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :cond_1
    new-instance v0, Lcbf;

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v6}, Lcbf;-><init>(Lcon;Lbfy;Lbfy;IJ)V

    iput-object v0, p0, Lcbx;->a:Lcbf;

    return-void
.end method

.method private declared-synchronized a()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbx;->a:Lcbf;

    invoke-virtual {v0}, Lcbf;->b()I

    move-result v0

    iget-object v1, p0, Lcbx;->a:Lcbf;

    invoke-virtual {v1}, Lcbf;->a()J

    move-result-wide v1

    iget-object v3, p0, Lcbx;->b:Lcby;

    iget-object v4, p0, Lcbx;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v0, v1, v2}, Lcby;->a(Ljava/lang/String;IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbx;->a:Lcbf;

    invoke-virtual {v0}, Lcbf;->c()Z

    move-result v0

    invoke-direct {p0}, Lcbx;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbx;->a:Lcbf;

    invoke-virtual {v0}, Lcbf;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0}, Lcbx;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, Lcbx;->a()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbx;->a:Lcbf;

    invoke-direct {p0}, Lcbx;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbx;->a:Lcbf;

    invoke-virtual {v0}, Lcbf;->f()V

    invoke-direct {p0}, Lcbx;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "%s[%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "PersistentBucketRateLimiter"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcbx;->a:Lcbf;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
