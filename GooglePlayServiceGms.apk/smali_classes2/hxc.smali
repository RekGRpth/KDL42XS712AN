.class public Lhxc;
.super Lhxa;
.source "SourceFile"


# instance fields
.field c:Z

.field protected final d:Lhvw;

.field protected final e:Lhwp;

.field protected final f:Lhwr;


# direct methods
.method protected constructor <init>(Lhwp;Lhvw;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhxc;-><init>(Lhwp;Lhvw;Lhwr;)V

    return-void
.end method

.method public constructor <init>(Lhwp;Lhvw;Lhwr;)V
    .locals 1

    invoke-direct {p0}, Lhxa;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhxc;->c:Z

    iput-object p1, p0, Lhxc;->e:Lhwp;

    iput-object p3, p0, Lhxc;->f:Lhwr;

    iput-object p2, p0, Lhxc;->d:Lhvw;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x0

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhxc;->d:Lhvw;

    iget-wide v1, p0, Lhxa;->a:J

    invoke-virtual {v0, v1, v2}, Lhvw;->b(J)V

    iget-boolean v0, p0, Lhxa;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhxc;->f:Lhwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhxc;->f:Lhwr;

    invoke-virtual {v0}, Lhwr;->a()V

    :cond_0
    iget-object v0, p0, Lhxc;->e:Lhwp;

    iget-wide v1, p0, Lhxa;->a:J

    iget-boolean v3, p0, Lhxc;->c:Z

    iget-boolean v4, p0, Lhxa;->b:Z

    iget-object v5, p0, Lhxe;->i:Ljava/util/Collection;

    new-instance v6, Liac;

    invoke-direct {v6}, Liac;-><init>()V

    invoke-virtual {v0, v8}, Lhwp;->a(I)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v6, v1, v2, v7}, Liac;->a(JLandroid/app/PendingIntent;)Liac;

    invoke-virtual {v6, v3}, Liac;->a(Z)Liac;

    invoke-virtual {v6, v4}, Liac;->b(Z)Liac;

    invoke-static {v5}, Lile;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v1

    invoke-virtual {v6, v1}, Liac;->a(Landroid/os/WorkSource;)Liac;

    iget-object v0, v0, Lhwp;->a:Landroid/content/Context;

    invoke-virtual {v6, v0}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to bind to GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Nlp enabled with interval %s[ms]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lhxa;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lhxc;->d:Lhvw;

    invoke-virtual {v0}, Lhvw;->f()V

    iget-object v0, p0, Lhxc;->e:Lhwp;

    new-instance v1, Liac;

    invoke-direct {v1}, Liac;-><init>()V

    invoke-virtual {v0, v8}, Lhwp;->a(I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Liac;->b(Landroid/app/PendingIntent;)Liac;

    iget-object v0, v0, Lhwp;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to start the GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V

    :cond_4
    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "NLP Disabled"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/StringBuilder;)V
    .locals 2

    invoke-super {p0, p1}, Lhxa;->a(Ljava/lang/StringBuilder;)V

    const-string v0, ", debug info="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lhxc;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lhxc;->d:Lhvw;

    invoke-virtual {v0, p1}, Lhvw;->a(Z)V

    :cond_0
    invoke-super {p0, p1}, Lhxa;->a(Z)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-boolean v0, p0, Lhxc;->c:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lhxc;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxe;->j:Z

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Network location (full power) ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhxc;->a(Ljava/lang/StringBuilder;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
