.class public final Liih;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:J

.field public c:Z

.field public d:I

.field public e:Z

.field public f:I

.field public g:Z

.field public h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liih;->b:J

    iput v2, p0, Liih;->d:I

    iput v2, p0, Liih;->f:I

    iput-boolean v2, p0, Liih;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Liih;->i:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Liih;->i:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Liih;->b()I

    :cond_0
    iget v0, p0, Liih;->i:I

    return v0
.end method

.method public final a(I)Liih;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liih;->c:Z

    iput p1, p0, Liih;->d:I

    return-object p0
.end method

.method public final a(J)Liih;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liih;->a:Z

    iput-wide p1, p0, Liih;->b:J

    return-object p0
.end method

.method public final a(Z)Liih;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liih;->g:Z

    iput-boolean p1, p0, Liih;->h:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Liih;->a(J)Liih;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->g()I

    move-result v0

    invoke-virtual {p0, v0}, Liih;->a(I)Liih;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Liih;->b(I)Liih;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Liih;->a(Z)Liih;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Liih;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Liih;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_0
    iget-boolean v0, p0, Liih;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Liih;->d:I

    invoke-virtual {p1, v0, v1}, Lizh;->b(II)V

    :cond_1
    iget-boolean v0, p0, Liih;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Liih;->f:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_2
    iget-boolean v0, p0, Liih;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Liih;->h:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Liih;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Liih;->b:J

    invoke-static {v0, v1, v2}, Lizh;->c(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Liih;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Liih;->d:I

    invoke-static {v1, v2}, Lizh;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Liih;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Liih;->f:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Liih;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Liih;->h:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Liih;->i:I

    return v0
.end method

.method public final b(I)Liih;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liih;->e:Z

    iput p1, p0, Liih;->f:I

    return-object p0
.end method
