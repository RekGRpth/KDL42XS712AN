.class final enum Lhiz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhiz;

.field public static final enum b:Lhiz;

.field public static final enum c:Lhiz;

.field public static final enum d:Lhiz;

.field public static final enum e:Lhiz;

.field private static final synthetic f:[Lhiz;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhiz;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lhiz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiz;->a:Lhiz;

    new-instance v0, Lhiz;

    const-string v1, "DETECTING_ACTIVITY"

    invoke-direct {v0, v1, v3}, Lhiz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiz;->b:Lhiz;

    new-instance v0, Lhiz;

    const-string v1, "RANDOM_WAIT"

    invoke-direct {v0, v1, v4}, Lhiz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiz;->c:Lhiz;

    new-instance v0, Lhiz;

    const-string v1, "CLIENT_WAIT"

    invoke-direct {v0, v1, v5}, Lhiz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiz;->d:Lhiz;

    new-instance v0, Lhiz;

    const-string v1, "GPS_WAIT"

    invoke-direct {v0, v1, v6}, Lhiz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhiz;->e:Lhiz;

    const/4 v0, 0x5

    new-array v0, v0, [Lhiz;

    sget-object v1, Lhiz;->a:Lhiz;

    aput-object v1, v0, v2

    sget-object v1, Lhiz;->b:Lhiz;

    aput-object v1, v0, v3

    sget-object v1, Lhiz;->c:Lhiz;

    aput-object v1, v0, v4

    sget-object v1, Lhiz;->d:Lhiz;

    aput-object v1, v0, v5

    sget-object v1, Lhiz;->e:Lhiz;

    aput-object v1, v0, v6

    sput-object v0, Lhiz;->f:[Lhiz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhiz;
    .locals 1

    const-class v0, Lhiz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhiz;

    return-object v0
.end method

.method public static values()[Lhiz;
    .locals 1

    sget-object v0, Lhiz;->f:[Lhiz;

    invoke-virtual {v0}, [Lhiz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhiz;

    return-object v0
.end method
