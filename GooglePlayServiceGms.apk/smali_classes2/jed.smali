.class public final Ljed;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:[Ljef;

.field public b:[Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:Ljee;

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:[Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-direct {p0}, Lizs;-><init>()V

    invoke-static {}, Ljef;->c()[Ljef;

    move-result-object v0

    iput-object v0, p0, Ljed;->a:[Ljef;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljed;->b:[Ljava/lang/String;

    iput v2, p0, Ljed;->c:I

    const-wide/32 v0, -0x5265c00

    iput-wide v0, p0, Ljed;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljed;->e:Ljee;

    iput-boolean v3, p0, Ljed;->f:Z

    iput-boolean v3, p0, Ljed;->g:Z

    const-string v0, ""

    iput-object v0, p0, Ljed;->h:Ljava/lang/String;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljed;->i:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljed;->j:Ljava/lang/String;

    iput v2, p0, Ljed;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Ljed;->a:[Ljef;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljed;->a:[Ljef;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljed;->a:[Ljef;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Ljed;->a:[Ljef;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    const/4 v4, 0x1

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    :cond_2
    iget-object v2, p0, Ljed;->b:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljed;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_1
    iget-object v5, p0, Ljed;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_4

    iget-object v5, p0, Ljed;->b:[Ljava/lang/String;

    aget-object v5, v5, v2

    if-eqz v5, :cond_3

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lizn;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v0, v3

    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Ljed;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    const/4 v2, 0x3

    iget v3, p0, Ljed;->c:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-wide v2, p0, Ljed;->d:J

    const-wide/32 v4, -0x5265c00

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    iget-wide v3, p0, Ljed;->d:J

    invoke-static {v2, v3, v4}, Lizn;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Ljed;->e:Ljee;

    if-eqz v2, :cond_8

    const/4 v2, 0x6

    iget-object v3, p0, Ljed;->e:Ljee;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-boolean v2, p0, Ljed;->f:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x7

    iget-boolean v3, p0, Ljed;->f:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_9
    iget-boolean v2, p0, Ljed;->g:Z

    if-eqz v2, :cond_a

    const/16 v2, 0x8

    iget-boolean v3, p0, Ljed;->g:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Ljed;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const/16 v2, 0x9

    iget-object v3, p0, Ljed;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget-object v2, p0, Ljed;->i:[Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Ljed;->i:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    move v2, v1

    move v3, v1

    :goto_2
    iget-object v4, p0, Ljed;->i:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_d

    iget-object v4, p0, Ljed;->i:[Ljava/lang/String;

    aget-object v4, v4, v1

    if-eqz v4, :cond_c

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4}, Lizn;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_d
    add-int/2addr v0, v2

    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Ljed;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const/16 v1, 0xb

    iget-object v2, p0, Ljed;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Ljed;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljed;->a:[Ljef;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljef;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljed;->a:[Ljef;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljef;

    invoke-direct {v3}, Ljef;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljed;->a:[Ljef;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljef;

    invoke-direct {v3}, Ljef;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Ljed;->a:[Ljef;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljed;->b:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljed;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljed;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljed;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljed;->c:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Ljed;->d:J

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljed;->e:Ljee;

    if-nez v0, :cond_7

    new-instance v0, Ljee;

    invoke-direct {v0}, Ljee;-><init>()V

    iput-object v0, p0, Ljed;->e:Ljee;

    :cond_7
    iget-object v0, p0, Ljed;->e:Ljee;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljed;->f:Z

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljed;->g:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljed;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljed;->i:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5

    :cond_a
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljed;->i:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljed;->j:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Ljed;->a:[Ljef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljed;->a:[Ljef;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljed;->a:[Ljef;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Ljed;->a:[Ljef;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljed;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljed;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljed;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljed;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget v0, p0, Ljed;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    const/4 v0, 0x3

    iget v2, p0, Ljed;->c:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_4
    iget-wide v2, p0, Ljed;->d:J

    const-wide/32 v4, -0x5265c00

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    iget-wide v2, p0, Ljed;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    :cond_5
    iget-object v0, p0, Ljed;->e:Ljee;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v2, p0, Ljed;->e:Ljee;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_6
    iget-boolean v0, p0, Ljed;->f:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-boolean v2, p0, Ljed;->f:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Ljed;->g:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-boolean v2, p0, Ljed;->g:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_8
    iget-object v0, p0, Ljed;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x9

    iget-object v2, p0, Ljed;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_9
    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_b

    :goto_2
    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    iget-object v0, p0, Ljed;->i:[Ljava/lang/String;

    aget-object v0, v0, v1

    if-eqz v0, :cond_a

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILjava/lang/String;)V

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_b
    iget-object v0, p0, Ljed;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xb

    iget-object v1, p0, Ljed;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_c
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljed;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljed;

    iget-object v2, p0, Ljed;->a:[Ljef;

    iget-object v3, p1, Ljed;->a:[Ljef;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljed;->b:[Ljava/lang/String;

    iget-object v3, p1, Ljed;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Ljed;->c:I

    iget v3, p1, Ljed;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Ljed;->d:J

    iget-wide v4, p1, Ljed;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljed;->e:Ljee;

    if-nez v2, :cond_7

    iget-object v2, p1, Ljed;->e:Ljee;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljed;->e:Ljee;

    iget-object v3, p1, Ljed;->e:Ljee;

    invoke-virtual {v2, v3}, Ljee;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Ljed;->f:Z

    iget-boolean v3, p1, Ljed;->f:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-boolean v2, p0, Ljed;->g:Z

    iget-boolean v3, p1, Ljed;->g:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljed;->h:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Ljed;->h:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ljed;->h:Ljava/lang/String;

    iget-object v3, p1, Ljed;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Ljed;->i:[Ljava/lang/String;

    iget-object v3, p1, Ljed;->i:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Ljed;->j:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Ljed;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Ljed;->j:Ljava/lang/String;

    iget-object v3, p1, Ljed;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Ljed;->a:[Ljef;

    invoke-static {v0}, Lizq;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljed;->b:[Ljava/lang/String;

    invoke-static {v4}, Lizq;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Ljed;->c:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Ljed;->d:J

    iget-wide v6, p0, Ljed;->d:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljed;->e:Ljee;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ljed;->f:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Ljed;->g:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljed;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljed;->i:[Ljava/lang/String;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljed;->j:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljed;->e:Ljee;

    invoke-virtual {v0}, Ljee;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljed;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Ljed;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method
