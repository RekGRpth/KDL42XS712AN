.class public final Lffe;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lffg;

.field private final c:Lffg;

.field private final d:Lffg;

.field private final e:Lgel;

.field private final f:Lgex;

.field private final g:Lgem;

.field private final h:Lgei;

.field private final i:Lgfi;

.field private final j:Lglv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.media.upload"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "https://www.googleapis.com/auth/plus.profiles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "https://www.googleapis.com/auth/plus.profiles.write"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "https://www.googleapis.com/auth/plus.stream.read"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    sput-object v0, Lffe;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lffg;->c(Landroid/content/Context;)Lffg;

    move-result-object v0

    invoke-static {p1}, Lffg;->b(Landroid/content/Context;)Lffg;

    move-result-object v1

    invoke-static {p1}, Lffg;->d(Landroid/content/Context;)Lffg;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lffe;-><init>(Lffg;Lffg;Lffg;)V

    return-void
.end method

.method private constructor <init>(Lffg;Lffg;Lffg;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lffe;->b:Lffg;

    iput-object p2, p0, Lffe;->c:Lffg;

    iput-object p3, p0, Lffe;->d:Lffg;

    new-instance v0, Lgel;

    iget-object v1, p0, Lffe;->b:Lffg;

    invoke-direct {v0, v1}, Lgel;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->e:Lgel;

    new-instance v0, Lgex;

    iget-object v1, p0, Lffe;->b:Lffg;

    invoke-direct {v0, v1}, Lgex;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->f:Lgex;

    new-instance v0, Lgem;

    iget-object v1, p0, Lffe;->b:Lffg;

    invoke-direct {v0, v1}, Lgem;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->g:Lgem;

    new-instance v0, Lgei;

    iget-object v1, p0, Lffe;->b:Lffg;

    invoke-direct {v0, v1}, Lgei;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->h:Lgei;

    new-instance v0, Lgfi;

    iget-object v1, p0, Lffe;->b:Lffg;

    invoke-direct {v0, v1}, Lgfi;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->i:Lgfi;

    new-instance v0, Lglv;

    iget-object v1, p0, Lffe;->d:Lffg;

    invoke-direct {v0, v1}, Lglv;-><init>(Lbmi;)V

    iput-object v0, p0, Lffe;->j:Lglv;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lffe;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    const-string v1, "social_client_app_id"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lffa;
    .locals 2

    invoke-static {p0, p1, p3}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    new-instance v1, Lffa;

    invoke-direct {v1, v0, p2}, Lffa;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)Lffe;
    .locals 1

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->e()Lffe;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)[B
    .locals 6

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Lbpn;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lbpn;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a image mime type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lfbd;->M:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, p1, v0, v0}, Lbpn;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image decoded from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v1, 0x0

    sget-object v0, Lfbd;->N:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2, v4, v0, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not load image from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Lffe;
    .locals 1

    new-instance v0, Lffe;

    invoke-direct {v0, p0}, Lffe;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static b(Lffa;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lffa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lffa;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "me"

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffa;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;
    .locals 6

    iget-object v0, p0, Lffe;->e:Lgel;

    iget-object v1, p2, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p2}, Lffe;->b(Lffa;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lgel;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lffa;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 6

    new-instance v0, Lgia;

    invoke-direct {v0}, Lgia;-><init>()V

    iput-object p3, v0, Lgia;->a:Ljava/lang/String;

    iget-object v1, v0, Lgia;->d:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p2, v0, Lgia;->b:Ljava/lang/String;

    iget-object v1, v0, Lgia;->d:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    iget-object v1, v0, Lgia;->d:Ljava/util/Set;

    iget-object v2, v0, Lgia;->a:Ljava/lang/String;

    iget-object v3, v0, Lgia;->b:Ljava/lang/String;

    iget-object v0, v0, Lgia;->c:Ljava/lang/String;

    invoke-direct {v4, v1, v2, v3, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    iget-object v0, p0, Lffe;->g:Lgem;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lffe;->b(Lffa;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3, v2}, Lgem;->a(Lgen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgem;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    return-object v0
.end method

.method public final a(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 6

    iget-object v0, p0, Lffe;->g:Lgem;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lffa;->b:Ljava/lang/String;

    invoke-static {p3}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lgem;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lffa;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lffe;->i:Lgfi;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lffe;->b(Lffa;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "circles"

    invoke-static {v4, v2, v3, v4, v4}, Lgfi;->a(Lgfj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgfi;->a:Lbmi;

    const/4 v2, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    return-object v0
.end method

.method public final a()Lffg;
    .locals 1

    iget-object v0, p0, Lffe;->b:Lffg;

    return-object v0
.end method

.method public final a(Lffa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lgmb;
    .locals 9

    const/4 v8, 0x7

    new-instance v7, Lgmc;

    invoke-direct {v7}, Lgmc;-><init>()V

    if-eqz p3, :cond_0

    iput-object p3, v7, Lgmc;->e:Ljava/lang/String;

    iget-object v0, v7, Lgmc;->g:Ljava/util/Set;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v7, Lgmc;->b:Z

    iget-object v0, v7, Lgmc;->g:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p5, :cond_2

    iput-object p5, v7, Lgmc;->a:Ljava/lang/String;

    iget-object v0, v7, Lgmc;->g:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    iget-object v1, v7, Lgmc;->g:Ljava/util/Set;

    iget-object v2, v7, Lgmc;->a:Ljava/lang/String;

    iget-boolean v3, v7, Lgmc;->b:Z

    iget-object v4, v7, Lgmc;->c:Ljava/lang/String;

    iget-object v5, v7, Lgmc;->d:Ljava/lang/String;

    iget-object v6, v7, Lgmc;->e:Ljava/lang/String;

    iget-object v7, v7, Lgmc;->f:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;-><init>(Ljava/util/Set;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;)V

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    iget-object v0, p0, Lffe;->j:Lglv;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lffa;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p2, v2}, Lglv;->a(Lglx;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lglv;->a:Lbmi;

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    move v2, v8

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lffa;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    if-nez p4, :cond_0

    const-string p4, "me"

    :cond_0
    move-object/from16 v0, p3

    invoke-static {p1, v0}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v1

    const-string v2, "cloud"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lges;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lbmd;

    const/4 v4, 0x0

    new-instance v5, Lbmd;

    const-string v6, "image/jpeg"

    const-string v7, ""

    invoke-direct {v5, v6, v7, v1}, Lbmd;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v5, v3, v4

    const/4 v1, 0x1

    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    invoke-direct {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lbmd;

    const-string v6, "application/json"

    const/4 v7, 0x0

    new-array v7, v7, [B

    invoke-direct {v5, v6, v4, v7}, Lbmd;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v5, v3, v1

    iget-object v1, p0, Lffe;->c:Lffg;

    iget-object v4, p2, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    invoke-virtual {v1, v4, v2, v3, v5}, Lffg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[Lbmd;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    new-instance v2, Lgkt;

    invoke-direct {v2}, Lgkt;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lgkt;->a:Ljava/lang/String;

    iget-object v1, v2, Lgkt;->c:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v3, v2, Lgkt;->c:Ljava/util/Set;

    iget-object v4, v2, Lgkt;->a:Ljava/lang/String;

    iget-object v2, v2, Lgkt;->b:Ljava/lang/String;

    invoke-direct {v1, v3, v4, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, Lgkq;

    invoke-direct {v12}, Lgkq;-><init>()V

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iput-object v1, v12, Lgkq;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v1, v12, Lgkq;->k:Ljava/util/Set;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    iget-object v2, v12, Lgkq;->k:Ljava/util/Set;

    iget-object v3, v12, Lgkq;->a:Ljava/lang/String;

    iget-object v4, v12, Lgkq;->b:Ljava/util/List;

    iget-object v5, v12, Lgkq;->c:Ljava/lang/String;

    iget-object v6, v12, Lgkq;->d:Ljava/lang/String;

    iget-object v7, v12, Lgkq;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v8, v12, Lgkq;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$NameEntity;

    iget-object v9, v12, Lgkq;->g:Ljava/lang/String;

    iget-object v10, v12, Lgkq;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$StatusForViewerEntity;

    iget-object v11, v12, Lgkq;->i:Ljava/lang/String;

    iget-object v12, v12, Lgkq;->j:Ljava/lang/String;

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$NameEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$StatusForViewerEntity;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    iget-object v1, p0, Lffe;->f:Lgex;

    iget-object v2, p2, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Lgex;->a(Lgfe;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v1, Lgex;->a:Lbmi;

    const/4 v3, 0x2

    const-class v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lgks;

    move-result-object v1

    invoke-interface {v1}, Lgks;->b()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lffa;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lffa;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Pages can not block people."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lffe;->f:Lgex;

    iget-object v2, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4, p2}, Lgex;->a(Lgey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgex;->a:Lbmi;

    invoke-virtual {v0, v2, v1, v3, v4}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lffa;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;)V
    .locals 6

    iget-object v0, p0, Lffe;->i:Lgfi;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x0

    invoke-static {v2, p2}, Lgfi;->a(Lgfk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgfi;->a:Lbmi;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    return-void
.end method

.method public final b()Lffg;
    .locals 1

    iget-object v0, p0, Lffe;->d:Lffg;

    return-object v0
.end method

.method public final b(Lffa;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lffa;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Pages can not unblock people."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lffe;->f:Lgex;

    iget-object v2, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4, p2}, Lgex;->a(Lgfd;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgex;->a:Lbmi;

    invoke-virtual {v0, v2, v1, v3, v4}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lffe;->g:Lgem;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p1, Lffa;->b:Ljava/lang/String;

    invoke-static {p3}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-static {v5, p2, v2, v3, v4}, Lgem;->a(Lgep;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lgem;->a:Lbmi;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v3, v2, v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 6

    iget-object v0, p0, Lffe;->g:Lgem;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lffa;->b:Ljava/lang/String;

    invoke-static {p3}, Lffe;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lgem;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lffa;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lffe;->g:Lgem;

    iget-object v1, p1, Lffa;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lffa;->b:Ljava/lang/String;

    invoke-static {v4, p2, v2}, Lgem;->a(Lgeo;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lgem;->a:Lbmi;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v3, v2, v4}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
