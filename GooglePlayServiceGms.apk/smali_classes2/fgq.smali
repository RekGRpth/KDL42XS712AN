.class abstract Lfgq;
.super Lffb;
.source "SourceFile"


# instance fields
.field private final c:Lfbz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p5}, Lffb;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    iput-object p4, p0, Lfgq;->c:Lfbz;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lfgq;->c()Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lffc;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    iget-object v2, p0, Lfgq;->c:Lfbz;

    iget v3, v0, Lffc;->a:I

    iget-object v0, v0, Lffc;->b:Landroid/os/Bundle;

    invoke-interface {v2, v3, v0, v1}, Lfbz;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PeopleService"

    const-string v2, "Bad operation"

    invoke-static {v1, v2, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lffc;->g:Lffc;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lfgq;->a:Landroid/content/Context;

    iget-object v2, p0, Lffb;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lfjv;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    sget-object v0, Lffc;->f:Lffc;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "PeopleService"

    const-string v2, "Error during operation"

    invoke-static {v1, v2, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v1, p0, Lfgq;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lfgq;->a()Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v0}, Lfjv;->a(Landroid/content/Context;Ljava/lang/Exception;)Lffc;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v1, "PeopleService"

    const-string v2, "Client died?"

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public abstract c()Landroid/util/Pair;
.end method
