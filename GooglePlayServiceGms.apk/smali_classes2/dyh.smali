.class public final Ldyh;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Ldxl;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;

.field private d:Z

.field private e:I

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Ldym;

.field private k:Ldyk;


# direct methods
.method public constructor <init>(Ldxl;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-boolean v4, p0, Ldyh;->d:Z

    new-instance v0, Ldyk;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ldyk;-><init>(I)V

    iput-object v0, p0, Ldyh;->k:Ldyk;

    iput-object p1, p0, Ldyh;->a:Ldxl;

    iget-object v0, p0, Ldyh;->a:Ldxl;

    invoke-virtual {v0}, Ldxl;->T_()Lo;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldyh;->b:Landroid/view/LayoutInflater;

    const/4 v0, 0x2

    iput v0, p0, Ldyh;->e:I

    :try_start_0
    iget-object v0, p0, Ldyh;->a:Ldxl;

    invoke-virtual {v0}, Ldxl;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004    # com.google.android.gms.R.integer.games_select_players_chips_grid_max_columns

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Ldyh;->e:I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-boolean v0, p0, Ldyh;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ldyk;

    invoke-direct {v0, v4}, Ldyk;-><init>(I)V

    iget-object v1, p0, Ldyh;->a:Ldxl;

    const v2, 0x7f0b025c    # com.google.android.gms.R.string.games_player_self

    invoke-virtual {v1, v2}, Ldxl;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldyk;->d:Ljava/lang/String;

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ChipsGridAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to find numColumns resource: 2131296260"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unable to find numColumns resource: 2131296260"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Ldyh;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Ldyh;->f:Landroid/view/View;

    return-object v0
.end method

.method private a(Ldyk;)V
    .locals 3

    iget-boolean v0, p0, Ldyh;->d:Z

    if-eqz v0, :cond_4

    iget v0, p0, Ldyh;->i:I

    :goto_0
    iget-object v1, p0, Ldyh;->k:Ldyk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ldyh;->k:Ldyk;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Ldyh;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    iget-object v0, p0, Ldyh;->j:Ldym;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldyh;->j:Ldym;

    invoke-interface {v0}, Ldym;->a()V

    :cond_3
    return-void

    :cond_4
    iget v0, p0, Ldyh;->i:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method static synthetic b(Ldyh;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Ldyk;)V
    .locals 3

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeChipInternal: chip "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldyh;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Ldyh;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    iget-object v0, p0, Ldyh;->j:Ldym;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldyh;->j:Ldym;

    :cond_2
    return-void
.end method

.method static synthetic c(Ldyh;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldyh;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Ldyh;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    new-instance v0, Ldyk;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldyk;-><init>(I)V

    iget-object v1, p0, Ldyh;->a:Ldxl;

    const v2, 0x7f0b0225    # com.google.android.gms.R.string.games_select_players_auto_pick_chip_name

    invoke-virtual {v1, v2}, Ldxl;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldyk;->d:Ljava/lang/String;

    const v1, 0x7f0200d1    # com.google.android.gms.R.drawable.game_avatar_transparent_anonymous_holo_light

    iput v1, v0, Ldyk;->f:I

    invoke-direct {p0, v0}, Ldyh;->a(Ldyk;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Ldyh;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyk;

    iget v3, v0, Ldyk;->a:I

    if-nez v3, :cond_2

    :goto_1
    invoke-static {v1}, Lbiq;->a(Z)V

    iput-object p1, v0, Ldyk;->e:Landroid/net/Uri;

    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final a(Landroid/view/View;I)V
    .locals 2

    iget-object v0, p0, Ldyh;->f:Landroid/view/View;

    if-nez v0, :cond_1

    iput-object p1, p0, Ldyh;->f:Landroid/view/View;

    iget-object v0, p0, Ldyh;->f:Landroid/view/View;

    const v1, 0x7f0a012e    # com.google.android.gms.R.id.filter_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    iget-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iput p2, p0, Ldyh;->i:I

    iget-object v0, p0, Ldyh;->a:Ldxl;

    const v1, 0x7f0b022a    # com.google.android.gms.R.string.games_select_players_filter_hint

    invoke-virtual {v0, v1}, Ldxl;->b(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyh;->h:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public final a(Ldym;)V
    .locals 0

    iput-object p1, p0, Ldyh;->j:Ldym;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    new-instance v0, Ldyk;

    invoke-direct {v0, p1, v1, v1}, Ldyk;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Ldyh;->b(Ldyk;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    new-instance v0, Ldyk;

    invoke-direct {v0, p1, p2, p3}, Ldyk;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ChipsGridAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addPlayer: chip "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is already present"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Ldyh;->a(Ldyk;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Ldyh;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Ldyh;->g:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 2

    new-instance v0, Ldyk;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldyk;-><init>(I)V

    invoke-direct {p0, v0}, Ldyh;->b(Ldyk;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ldyk;

    invoke-direct {v0, p1, v1, v1}, Ldyk;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Ldyh;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyk;

    iget v3, v0, Ldyk;->a:I

    if-nez v3, :cond_3

    :goto_1
    invoke-static {v1}, Lbiq;->a(Z)V

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v0, p0, Ldyh;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Ldyh;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Ldyh;->k:Ldyk;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    iget-object v0, p0, Ldyh;->j:Ldym;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyh;->j:Ldym;

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2
.end method

.method public final d()V
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyk;

    iput-boolean v2, v0, Ldyk;->b:Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget-boolean v0, p0, Ldyh;->d:Z

    if-eqz v0, :cond_0

    if-lez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    :cond_0
    if-lez v3, :cond_3

    iget-object v0, p0, Ldyh;->c:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyk;

    iget v0, v0, Ldyk;->a:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_3

    add-int/lit8 v0, v3, -0x1

    :goto_1
    iget v3, p0, Ldyh;->e:I

    add-int/2addr v0, v3

    iget v3, p0, Ldyh;->e:I

    div-int/2addr v0, v3

    if-lez v0, :cond_2

    :goto_2
    invoke-static {v1}, Lbiq;->a(Z)V

    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v3, 0x0

    if-nez p2, :cond_2

    invoke-static {p3}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Ldyh;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04005b    # com.google.android.gms.R.layout.games_chips_grid_row

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v5, Ldyo;

    invoke-direct {v5, v3}, Ldyo;-><init>(B)V

    const v0, 0x7f0a0134    # com.google.android.gms.R.id.spanning_search_layer

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v5, Ldyo;->b:Landroid/widget/FrameLayout;

    move v1, v3

    :goto_0
    if-ge v1, v7, :cond_1

    new-instance v6, Ldyl;

    invoke-direct {v6, v3}, Ldyl;-><init>(B)V

    sget-object v0, Ldyo;->c:[I

    aget v0, v0, v1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iget v0, p0, Ldyh;->e:I

    if-le v0, v1, :cond_0

    iput-object v8, v6, Ldyl;->a:Landroid/view/View;

    const v0, 0x7f0a012c    # com.google.android.gms.R.id.search_layer

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v6, Ldyl;->b:Landroid/widget/FrameLayout;

    const v0, 0x7f0a0127    # com.google.android.gms.R.id.normal_layer

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Ldyl;->c:Landroid/view/View;

    const v0, 0x7f0a0129    # com.google.android.gms.R.id.selected_layer

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Ldyl;->d:Landroid/view/View;

    const v0, 0x7f0a0128    # com.google.android.gms.R.id.name_normal

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Ldyl;->e:Landroid/widget/TextView;

    const v0, 0x7f0a012a    # com.google.android.gms.R.id.name_selected

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Ldyl;->f:Landroid/widget/TextView;

    const v0, 0x7f0a005a    # com.google.android.gms.R.id.image

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, v6, Ldyl;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, v6, Ldyl;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c()V

    const v0, 0x7f0a012b    # com.google.android.gms.R.id.x_button

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Ldyl;->h:Landroid/view/View;

    iget-object v0, v6, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v6, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, v6, Ldyl;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v5, Ldyo;->a:[Ldyl;

    aput-object v6, v0, v1

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v5, Ldyo;->a:[Ldyl;

    aput-object v4, v0, v1

    goto :goto_1

    :cond_1
    iget v0, p0, Ldyh;->e:I

    if-gt v0, v7, :cond_3

    move v0, v2

    :goto_2
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyo;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v1, v0, Ldyo;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    move v8, v3

    move v1, v3

    :goto_3
    if-ge v8, v7, :cond_d

    iget v5, p0, Ldyh;->e:I

    if-ge v8, v5, :cond_d

    iget v5, p0, Ldyh;->e:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v8

    iget-object v6, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_4

    iget-object v1, p0, Ldyh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldyk;

    move v5, v2

    :goto_4
    iget-object v6, v0, Ldyo;->a:[Ldyl;

    aget-object v9, v6, v8

    invoke-static {v9}, Lbiq;->a(Ljava/lang/Object;)V

    if-eqz v1, :cond_c

    iget-object v6, v9, Ldyl;->e:Landroid/widget/TextView;

    iget-object v10, v1, Ldyk;->d:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, v9, Ldyl;->f:Landroid/widget/TextView;

    iget-object v10, v1, Ldyk;->d:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v6, v1, Ldyk;->f:I

    if-eqz v6, :cond_5

    iget-object v6, v9, Ldyl;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget v10, v1, Ldyk;->f:I

    invoke-virtual {v6, v4, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_5
    iget-object v6, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v10, v9, Ldyl;->c:Landroid/view/View;

    iget-boolean v6, v1, Ldyk;->b:Z

    if-eqz v6, :cond_6

    move v6, v7

    :goto_6
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v10, v9, Ldyl;->d:Landroid/view/View;

    iget-boolean v6, v1, Ldyk;->b:Z

    if-eqz v6, :cond_7

    move v6, v3

    :goto_7
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, v9, Ldyl;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v6, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget v6, v1, Ldyk;->a:I

    const/4 v10, 0x2

    if-ne v6, v10, :cond_8

    iget-object v6, v9, Ldyl;->h:Landroid/view/View;

    iget-object v10, v1, Ldyk;->c:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v6, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_8
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v1, v5

    goto :goto_3

    :cond_3
    move v0, v3

    goto/16 :goto_2

    :cond_4
    move v5, v1

    move-object v1, v4

    goto :goto_4

    :cond_5
    iget-object v6, v9, Ldyl;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v10, v1, Ldyk;->e:Landroid/net/Uri;

    const v11, 0x7f0200d6    # com.google.android.gms.R.drawable.games_default_profile_img

    invoke-virtual {v6, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    goto :goto_5

    :cond_6
    move v6, v3

    goto :goto_6

    :cond_7
    move v6, v7

    goto :goto_7

    :cond_8
    iget v6, v1, Ldyk;->a:I

    if-nez v6, :cond_9

    iget-object v1, v9, Ldyl;->a:Landroid/view/View;

    const-string v6, "current_player_chip"

    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Ldyl;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_9
    iget v6, v1, Ldyk;->a:I

    const/4 v10, 0x3

    if-ne v6, v10, :cond_a

    iget-object v1, v9, Ldyl;->c:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Ldyl;->d:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Ldyl;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Ldyl;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Ldyl;->b:Landroid/widget/FrameLayout;

    new-instance v6, Ldyj;

    invoke-direct {v6, p0, v9}, Ldyj;-><init>(Ldyh;Ldyl;)V

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    :cond_a
    iget v6, v1, Ldyk;->a:I

    if-ne v6, v2, :cond_b

    move v6, v2

    :goto_9
    invoke-static {v6}, Lbiq;->a(Z)V

    iget-object v6, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Ldyl;->h:Landroid/view/View;

    const-string v6, "auto_match_chip_x"

    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_b
    move v6, v3

    goto :goto_9

    :cond_c
    iget-object v1, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Ldyl;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Ldyl;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_d
    iget-object v2, p0, Ldyh;->f:Landroid/view/View;

    if-eqz v2, :cond_e

    if-nez v1, :cond_e

    iget-object v1, v0, Ldyo;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v0, Ldyo;->b:Landroid/widget/FrameLayout;

    new-instance v2, Ldyi;

    invoke-direct {v2, p0, v0}, Ldyi;-><init>(Ldyh;Ldyo;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    :cond_e
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick(): null tag for view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "current_player_chip"

    if-ne v0, v1, :cond_1

    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick(): unexpected click on current player chip: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "auto_match_chip_x"

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Ldyh;->a:Ldxl;

    instance-of v0, v0, Ldyn;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldyh;->a:Ldxl;

    check-cast v0, Ldyn;

    invoke-interface {v0}, Ldyn;->e()V

    :goto_1
    invoke-virtual {p0}, Ldyh;->d()V

    :goto_2
    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Parent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldyh;->a:Ldxl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isn\'t an OnRemovePlayerListener"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Ldyh;->a:Ldxl;

    instance-of v1, v1, Ldyn;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldyh;->a:Ldxl;

    check-cast v1, Ldyn;

    invoke-interface {v1, v0}, Ldyn;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Parent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldyh;->a:Ldxl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isn\'t an OnRemovePlayerListener"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    goto :goto_2

    :cond_5
    instance-of v1, v0, Ldyk;

    if-eqz v1, :cond_7

    check-cast v0, Ldyk;

    iget-boolean v1, v0, Ldyk;->b:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    iput-boolean v1, v0, Ldyk;->b:Z

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Ldyh;->d()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldyk;->b:Z

    goto :goto_2

    :cond_7
    const-string v1, "ChipsGridAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick(): unexpected tag: view "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "current_player_chip"

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ldyh;->d()V

    invoke-virtual {p0}, Ldyh;->notifyDataSetChanged()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
