.class final Lazo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lazp;

.field private final b:Laye;

.field private final c:Lbac;

.field private final d:Landroid/os/Handler;

.field private e:I

.field private f:I

.field private g:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private h:Ljava/lang/String;

.field private i:Lazt;

.field private j:Z


# direct methods
.method public constructor <init>(Lbac;Lazp;Landroid/os/Handler;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Laye;

    const-string v1, "MediaRouteSession"

    invoke-direct {v0, v1}, Laye;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lazo;->b:Laye;

    iput-object p1, p0, Lazo;->c:Lbac;

    iput-object p2, p0, Lazo;->a:Lazp;

    iput-object p3, p0, Lazo;->d:Landroid/os/Handler;

    const/4 v0, 0x4

    iput v0, p0, Lazo;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    iget-object v0, p0, Lazo;->b:Laye;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laye;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lazo;->b:Laye;

    invoke-virtual {v0, p4}, Laye;->a(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lazo;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lazo;->i:Lazt;

    iget-object v0, p0, Lazo;->c:Lbac;

    invoke-virtual {v0, p1, p2}, Lbac;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private c()V
    .locals 2

    const/4 v0, 0x3

    iput v0, p0, Lazo;->e:I

    iget-boolean v0, p0, Lazo;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lazo;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lazo;->g:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v0, p0, Lazo;->c:Lbac;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lbac;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lazo;->c:Lbac;

    invoke-virtual {v0}, Lbac;->k()V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->g:Lcom/google/android/gms/cast/ApplicationMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(I)V
    .locals 6

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->b:Laye;

    const-string v1, "onApplicationConnectionFailed; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lazo;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lazo;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v5, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lazo;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    iput v0, p0, Lazo;->e:I

    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazr;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lazr;-><init>(Lazo;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lazo;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_2
    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lazo;->f:I

    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazr;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lazr;-><init>(Lazo;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lazo;->i:Lazt;

    iget-object v0, v0, Lazt;->a:Ljava/lang/String;

    iget-object v1, p0, Lazo;->i:Lazt;

    iget-boolean v1, v1, Lazt;->b:Z

    invoke-direct {p0, v0, v1}, Lazo;->b(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    iput v0, p0, Lazo;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazq;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lazq;-><init>(Lazo;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lazo;->h:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final declared-synchronized a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->b:Laye;

    const-string v1, "onApplicationConnected; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lazo;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lazo;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v5, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lazo;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lazo;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_2
    invoke-direct {p0}, Lazo;->c()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    invoke-direct {p0}, Lazo;->c()V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    iput v0, p0, Lazo;->e:I

    iput-object p1, p0, Lazo;->g:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object p2, p0, Lazo;->h:Ljava/lang/String;

    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazs;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lazs;-><init>(Lazo;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lazo;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "session is not currently stopped! state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lazo;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lazo;->e:I

    iget-object v0, p0, Lazo;->c:Lbac;

    invoke-virtual {v0, p1, p2}, Lbac;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->b:Laye;

    const-string v1, "starting session for app %s; mState=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lazo;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lazo;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return-void

    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lazo;->f:I

    invoke-direct {p0, p1, p2}, Lazo;->b(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    const/4 v0, 0x2

    :try_start_2
    iput v0, p0, Lazo;->f:I

    new-instance v0, Lazt;

    invoke-direct {v0, p0, p1, p2}, Lazt;-><init>(Lazo;Ljava/lang/String;Z)V

    iput-object v0, p0, Lazo;->i:Lazt;

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lazo;->f:I

    new-instance v0, Lazt;

    invoke-direct {v0, p0, p1, p2}, Lazt;-><init>(Lazo;Ljava/lang/String;Z)V

    iput-object v0, p0, Lazo;->i:Lazt;

    invoke-direct {p0}, Lazo;->c()V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    iput v0, p0, Lazo;->f:I

    new-instance v0, Lazt;

    invoke-direct {v0, p0, p1, p2}, Lazt;-><init>(Lazo;Ljava/lang/String;Z)V

    iput-object v0, p0, Lazo;->i:Lazt;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->b:Laye;

    const-string v1, "stopping session"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean p1, p0, Lazo;->j:Z

    iget v0, p0, Lazo;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return-void

    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lazo;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    const/4 v0, 0x4

    :try_start_2
    iput v0, p0, Lazo;->f:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    invoke-direct {p0}, Lazo;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(I)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lazo;->b:Laye;

    const-string v1, "onApplicationDisconnected; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lazo;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lazo;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lazo;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lazo;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x4

    iput v0, p0, Lazo;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazq;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lazq;-><init>(Lazo;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    iget-object v0, p0, Lazo;->i:Lazt;

    iget-object v0, v0, Lazt;->a:Ljava/lang/String;

    iget-object v1, p0, Lazo;->i:Lazt;

    iget-boolean v1, v1, Lazt;->b:Z

    invoke-direct {p0, v0, v1}, Lazo;->b(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lazo;->f:I

    const/4 v0, 0x4

    iput v0, p0, Lazo;->e:I

    iget-object v0, p0, Lazo;->d:Landroid/os/Handler;

    new-instance v1, Lazq;

    iget-object v2, p0, Lazo;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lazq;-><init>(Lazo;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lazo;->h:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
