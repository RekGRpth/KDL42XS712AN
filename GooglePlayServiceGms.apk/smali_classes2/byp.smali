.class public final enum Lbyp;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbyp;

.field public static final enum b:Lbyp;

.field private static final g:[Lbyp;

.field private static final synthetic h:[Lbyp;


# instance fields
.field final c:Ljava/lang/String;

.field final d:Lbzk;

.field final e:I

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v10, 0x1

    const/4 v2, 0x0

    new-instance v0, Lbyp;

    const-string v1, "MY_DRIVE"

    const-string v3, "myDrive"

    sget-object v4, Lbzk;->f:Lbzk;

    const v5, 0x7f020106    # com.google.android.gms.R.drawable.ic_drive_my_drive

    const v6, 0x7f0b004b    # com.google.android.gms.R.string.drive_menu_my_drive

    invoke-direct/range {v0 .. v6}, Lbyp;-><init>(Ljava/lang/String;ILjava/lang/String;Lbzk;II)V

    sput-object v0, Lbyp;->a:Lbyp;

    new-instance v3, Lbyp;

    const-string v4, "SHARED_WITH_ME"

    const-string v6, "sharedWithMe"

    sget-object v7, Lbzk;->e:Lbzk;

    const v8, 0x7f020113    # com.google.android.gms.R.drawable.ic_drive_shared_with_me

    const v9, 0x7f0b004d    # com.google.android.gms.R.string.drive_menu_shared_with_me

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lbyp;-><init>(Ljava/lang/String;ILjava/lang/String;Lbzk;II)V

    sput-object v3, Lbyp;->b:Lbyp;

    const/4 v0, 0x2

    new-array v0, v0, [Lbyp;

    sget-object v1, Lbyp;->a:Lbyp;

    aput-object v1, v0, v2

    sget-object v1, Lbyp;->b:Lbyp;

    aput-object v1, v0, v10

    sput-object v0, Lbyp;->h:[Lbyp;

    invoke-static {}, Lbyp;->values()[Lbyp;

    move-result-object v0

    sput-object v0, Lbyp;->g:[Lbyp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lbzk;II)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbyp;->c:Ljava/lang/String;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzk;

    iput-object v0, p0, Lbyp;->d:Lbzk;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lbyp;->e:I

    iput p6, p0, Lbyp;->f:I

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lbyp;
    .locals 5

    invoke-static {}, Lbyp;->values()[Lbyp;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lbyp;->c:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid TopCollection name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic b()[Lbyp;
    .locals 1

    sget-object v0, Lbyp;->g:[Lbyp;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbyp;
    .locals 1

    const-class v0, Lbyp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbyp;

    return-object v0
.end method

.method public static values()[Lbyp;
    .locals 1

    sget-object v0, Lbyp;->h:[Lbyp;

    invoke-virtual {v0}, [Lbyp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbyp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbyp;->f:I

    return v0
.end method
