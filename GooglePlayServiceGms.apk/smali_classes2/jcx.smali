.class public final Ljcx;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljde;Ljava/security/Key;Ljcr;Ljava/security/Key;Ljcq;)Ljdd;
    .locals 6

    const/16 v5, 0x14

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    sget-object v0, Ljcq;->a:Ljcq;

    if-ne p4, v0, :cond_2

    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Not a signcrypted message"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {p0, p1, p2, p4}, Ljcx;->a(Ljde;Ljava/security/Key;Ljcr;Ljcq;)Ljdd;

    move-result-object v1

    :try_start_0
    iget-object v0, v1, Ljdd;->a:Ljdc;

    iget-object v0, v0, Ljdc;->f:Lizf;

    invoke-virtual {v0}, Lizf;->b()[B

    move-result-object v0

    iget-object v2, v1, Ljdd;->b:Lizf;

    invoke-virtual {v2}, Lizf;->b()[B

    move-result-object v2

    invoke-static {p3, p4, v0, v2}, Ljcp;->a(Ljava/security/Key;Ljcq;[B[B)[B
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    invoke-static {p1, p2, p3}, Ljcw;->a(Ljava/security/Key;Ljcr;Ljava/security/Key;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v2}, Lizf;->a([B)Lizf;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljdd;->a(Lizf;)Ljdd;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    :catch_2
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    :cond_3
    const/4 v0, 0x0

    iget-object v3, v1, Ljdd;->a:Ljdc;

    invoke-virtual {v3}, Ljdc;->d()[B

    move-result-object v3

    invoke-static {v3}, Ljcp;->a([B)[B

    move-result-object v3

    array-length v4, v2

    if-lt v4, v5, :cond_4

    invoke-static {v2}, Ljcp;->b([B)[B

    move-result-object v4

    invoke-static {v4, v3}, Ljcp;->a([B[B)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x1

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    :cond_5
    array-length v0, v2

    add-int/lit8 v0, v0, -0x14

    invoke-static {v2, v5, v0}, Lizf;->a([BII)Lizf;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljdd;->a(Lizf;)Ljdd;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljde;Ljava/security/Key;Ljcr;Ljcq;)Ljdd;
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Ljde;->b:Lizf;

    invoke-virtual {v2}, Lizf;->b()[B

    move-result-object v2

    iget-object v3, p0, Ljde;->a:Lizf;

    invoke-virtual {v3}, Lizf;->b()[B

    move-result-object v3

    invoke-static {p1, p2, v2, v3}, Ljcp;->a(Ljava/security/Key;Ljcr;[B[B)Z

    move-result v4

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Ljde;->a:Lizf;

    invoke-virtual {v3}, Lizf;->b()[B

    move-result-object v3

    invoke-static {v3}, Ljdd;->a([B)Ljdd;
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :try_start_1
    iget-object v3, v2, Ljdd;->a:Ljdc;

    iget v3, v3, Ljdc;->a:I

    invoke-virtual {p2}, Ljcr;->a()I

    move-result v5

    if-ne v3, v5, :cond_3

    move v3, v1

    :goto_0
    and-int/2addr v4, v3

    iget-object v3, v2, Ljdd;->a:Ljdc;

    iget v3, v3, Ljdc;->b:I

    invoke-virtual {p3}, Ljcq;->a()I

    move-result v5

    if-ne v3, v5, :cond_4

    move v3, v1

    :goto_1
    and-int/2addr v4, v3

    sget-object v3, Ljcq;->a:Ljcq;

    if-ne p3, v3, :cond_0

    iget-object v3, v2, Ljdd;->a:Ljdc;

    iget-boolean v3, v3, Ljdc;->e:Z

    if-nez v3, :cond_5

    :cond_0
    move v3, v1

    :goto_2
    and-int/2addr v3, v4

    sget-object v4, Ljcq;->a:Ljcq;

    if-eq p3, v4, :cond_1

    invoke-virtual {p2}, Ljcr;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, Ljdd;->a:Ljdc;

    iget-boolean v4, v4, Ljdc;->c:Z
    :try_end_1
    .catch Lizj; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v4, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    and-int/2addr v0, v3

    :goto_3
    if-eqz v0, :cond_6

    return-object v2

    :cond_3
    move v3, v0

    goto :goto_0

    :cond_4
    move v3, v0

    goto :goto_1

    :cond_5
    move v3, v0

    goto :goto_2

    :catch_0
    move-exception v1

    move-object v1, v2

    :goto_4
    move-object v2, v1

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Signatured failed verification"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_4
.end method
