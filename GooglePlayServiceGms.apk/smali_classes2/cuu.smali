.class final Lcuu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcuq;


# instance fields
.field final synthetic a:Lcuo;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Z

.field private e:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)V
    .locals 0

    iput-object p1, p0, Lcuu;->a:Lcuo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcuu;->b:Landroid/content/Context;

    iput-object p3, p0, Lcuu;->c:Lcom/google/android/gms/common/server/ClientContext;

    iput-boolean p4, p0, Lcuu;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ZB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcuu;-><init>(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    iput-object p1, p0, Lcuu;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final run()V
    .locals 11

    const/4 v0, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v1, p0, Lcuu;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcuu;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "muted"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcuu;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v9, v0

    :goto_0
    if-ge v9, v10, :cond_3

    iget-object v0, p0, Lcuu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ldmr;

    invoke-virtual {v6}, Ldmr;->getGamesData()Ldmf;

    move-result-object v3

    invoke-virtual {v6}, Ldmr;->getMarketData()Ldno;

    move-result-object v4

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    const-string v0, "GameAgent"

    const-string v1, "Received application with no app data and no Market data!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    iget-boolean v0, p0, Lcuu;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, v3, Lbni;->a:Landroid/content/ContentValues;

    const-string v1, "muted"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    iget-object v0, p0, Lcuu;->a:Lcuo;

    iget-object v1, p0, Lcuu;->b:Landroid/content/Context;

    iget-object v2, p0, Lcuu;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v6}, Ldmr;->b()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6}, Ldmr;->c()Ljava/lang/Integer;

    move-result-object v6

    iget-boolean v7, p0, Lcuu;->d:Z

    invoke-static/range {v0 .. v8}, Lcuo;->a(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZLjava/util/ArrayList;)J

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcuu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GameAgent"

    invoke-static {v0, v8, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    return-void
.end method
