.class final Lejp;
.super Leku;
.source "SourceFile"


# instance fields
.field final synthetic a:Lejm;


# direct methods
.method constructor <init>(Lejm;)V
    .locals 0

    iput-object p1, p0, Lejp;->a:Lejm;

    invoke-direct {p0, p1}, Leku;-><init>(Lejm;)V

    return-void
.end method

.method private f()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 28

    move-object/from16 v0, p0

    iget-object v2, v0, Lejp;->a:Lejm;

    iget-object v2, v2, Lejm;->g:Leiy;

    invoke-virtual {v2}, Leiy;->j()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lejp;->a:Lejm;

    iget-object v2, v2, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()Leit;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lejp;->a:Lejm;

    iget-object v2, v2, Lejm;->g:Leiy;

    invoke-virtual {v2}, Leiy;->h()Landroid/util/SparseArray;

    move-result-object v18

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lejp;->a:Lejm;

    iget-object v2, v2, Lejm;->f:Lelj;

    invoke-virtual {v2}, Lelj;->b()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/io/File;)J

    move-result-wide v19

    const-wide/16 v10, 0x0

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lejp;->a:Lejm;

    iget-object v3, v3, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v2, v5, v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v8, "cache"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v8, "lib"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/io/File;)J

    move-result-wide v2

    add-long/2addr v2, v10

    :goto_1
    add-int/lit8 v4, v4, 0x1

    move-wide v10, v2

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_0
    const-wide/16 v12, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v3, 0x0

    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    iget-object v0, v7, Leit;->a:[Leiu;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v23, v0

    const/4 v2, 0x0

    move-wide v14, v3

    move-wide/from16 v16, v5

    move v5, v2

    :goto_2
    move/from16 v0, v23

    if-ge v5, v0, :cond_3

    aget-object v4, v22, v5

    iget-wide v0, v4, Leiu;->d:J

    move-wide/from16 v24, v0

    iget-wide v0, v4, Leiu;->e:J

    move-wide/from16 v26, v0

    add-long v8, v16, v24

    add-long v6, v14, v26

    iget v2, v4, Leiu;->a:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_2

    add-long v2, v12, v26

    const-wide/16 v12, 0x0

    cmp-long v12, v24, v12

    if-eqz v12, :cond_1

    const-string v12, "Corpus %d from an unknown package using %d bytes."

    iget v4, v4, Leiu;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-static {v12, v4, v13}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_1
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-wide v14, v6

    move-wide/from16 v16, v8

    move-wide v12, v2

    goto :goto_2

    :cond_2
    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    if-nez v3, :cond_7

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    move-object v4, v3

    :goto_4
    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    add-long v14, v14, v24

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    iget-object v3, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long v3, v3, v26

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v14, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v2, v12

    goto :goto_3

    :cond_3
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Landroid/util/Pair;

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-eqz v2, :cond_4

    sub-long v3, v19, v14

    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v21

    mul-long v2, v3, v21

    div-long v4, v2, v16

    :cond_4
    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lejp;->a:Lejm;

    iget-object v8, v8, Lejm;->i:Lelt;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lels;->c()Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, 0x1

    :goto_6
    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;-><init>(Ljava/lang/String;JZJ)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    goto :goto_6

    :cond_6
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    invoke-interface {v9, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    new-instance v2, Lcom/google/android/gms/appdatasearch/StorageStats;

    move-wide v4, v12

    move-wide/from16 v6, v19

    move-wide v8, v10

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/appdatasearch/StorageStats;-><init>([Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V

    return-object v2

    :cond_7
    move-object v4, v3

    goto/16 :goto_4

    :cond_8
    move-wide v2, v10

    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lejp;->f()Lcom/google/android/gms/appdatasearch/StorageStats;

    move-result-object v0

    return-object v0
.end method
