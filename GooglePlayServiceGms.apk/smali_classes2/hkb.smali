.class public final Lhkb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field private final b:Lidu;

.field private final c:Lhjf;

.field private final d:Z


# direct methods
.method constructor <init>(Ljava/util/List;Lidu;Lhjf;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhkb;-><init>(Ljava/util/List;Lidu;Lhjf;B)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Lidu;Lhjf;B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhkb;->a:Ljava/util/List;

    iput-object p2, p0, Lhkb;->b:Lidu;

    iput-object p3, p0, Lhkb;->c:Lhjf;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhkb;->d:Z

    return-void
.end method

.method private b(Ljava/util/Calendar;J)Lhue;
    .locals 12

    const/4 v6, 0x0

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    iget-object v0, p0, Lhkb;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_0
    iget-object v0, p0, Lhkb;->c:Lhjf;

    invoke-virtual {v0}, Lhjf;->b()Livi;

    move-result-object v0

    if-nez v0, :cond_6

    const-wide/16 v0, 0x0

    :goto_0
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-wide/16 v0, 0x0

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v9, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xc

    const/4 v1, -0x5

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v9, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_2
    invoke-static {p1, v9}, Lilv;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v10

    iget-object v0, p0, Lhkb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lilt;

    invoke-virtual {v1, p1}, Lilt;->b(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v10, :cond_4

    invoke-virtual {v1, v9}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_4
    iget-object v0, p0, Lhkb;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lilt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhkb;->c:Lhjf;

    invoke-virtual {v0}, Lhjf;->m()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_5

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_7

    :cond_5
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_9

    new-instance v0, Lhke;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    sget-object v3, Lhkf;->b:Lhkf;

    iget-boolean v4, p0, Lhkb;->d:Z

    iget-object v5, p0, Lhkb;->b:Lidu;

    invoke-interface {v5}, Lidu;->y()J

    move-result-wide v7

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lhke;-><init>(Lilt;Ljava/util/List;Lhkf;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    :goto_2
    invoke-virtual {v0, p1}, Lhke;->a(Ljava/util/Calendar;)Lilt;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lhke;->c:Lhkf;

    sget-object v2, Lhkf;->a:Lhkf;

    if-ne v0, v2, :cond_a

    sget-object v0, Lhit;->a:Lhit;

    :goto_3
    invoke-static {v1, v0}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    :goto_4
    return-object v0

    :cond_6
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v0

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iget-wide v3, v1, Lilt;->a:J

    invoke-static {v0, v3, v4}, Lilv;->a(Ljava/util/Calendar;J)V

    new-instance v3, Ljava/util/Random;

    iget-object v4, p0, Lhkb;->b:Lidu;

    invoke-interface {v4}, Lidu;->y()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    xor-long/2addr v4, v7

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v3

    const-wide v7, 0x3fe999999999999aL    # 0.8

    cmpg-double v0, v3, v7

    if-gtz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    :cond_9
    new-instance v0, Lhke;

    const/4 v2, 0x0

    sget-object v3, Lhkf;->a:Lhkf;

    iget-boolean v4, p0, Lhkb;->d:Z

    iget-object v5, p0, Lhkb;->b:Lidu;

    invoke-interface {v5}, Lidu;->y()J

    move-result-wide v7

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lhke;-><init>(Lilt;Ljava/util/List;Lhkf;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    goto :goto_2

    :cond_a
    sget-object v0, Lhit;->b:Lhit;

    goto :goto_3

    :cond_b
    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method final a(Ljava/util/Calendar;JZ)J
    .locals 6

    const/4 v5, 0x5

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3}, Lhkb;->b(Ljava/util/Calendar;J)Lhue;

    move-result-object v1

    const/4 v0, 0x0

    if-nez v1, :cond_2

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-virtual {v0, v5, v2}, Ljava/util/Calendar;->add(II)V

    const-wide/16 v3, 0x0

    invoke-static {v0, v3, v4}, Lilv;->a(Ljava/util/Calendar;J)V

    const-wide/16 v3, -0x1

    invoke-direct {p0, v0, v3, v4}, Lhkb;->b(Ljava/util/Calendar;J)Lhue;

    move-result-object v0

    move v1, v2

    move-object v3, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v5, v2}, Ljava/util/Calendar;->add(II)V

    :cond_0
    if-eqz p4, :cond_1

    iget-object v1, v3, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Lilt;

    iget-wide v1, v1, Lilt;->a:J

    :goto_1
    invoke-static {v0, v1, v2}, Lilv;->a(Ljava/util/Calendar;J)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lhkb;->b:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0

    :cond_1
    iget-object v1, v3, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Lilt;

    iget-wide v1, v1, Lilt;->b:J

    goto :goto_1

    :cond_2
    move-object v3, v1

    move v1, v0

    goto :goto_0
.end method

.method final a(Ljava/util/Calendar;J)Lhkc;
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lhkb;->b(Ljava/util/Calendar;J)Lhue;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, v4, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Lilt;

    invoke-virtual {v0, p1}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, v4, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Lhit;

    move-object v1, v0

    :goto_1
    new-instance v5, Lhkc;

    if-nez v4, :cond_2

    :goto_2
    invoke-direct {v5, v3, v1, v2}, Lhkc;-><init>(ZLhit;Lilt;)V

    return-object v5

    :cond_0
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    :cond_1
    move-object v1, v2

    goto :goto_1

    :cond_2
    iget-object v0, v4, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Lilt;

    move-object v2, v0

    goto :goto_2
.end method
