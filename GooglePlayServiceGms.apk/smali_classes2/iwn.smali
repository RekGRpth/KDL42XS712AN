.class final Liwn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liwi;


# instance fields
.field final synthetic a:Liwi;


# direct methods
.method constructor <init>(Liwi;)V
    .locals 0

    iput-object p1, p0, Liwn;->a:Liwi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lixh;
    .locals 1

    iget-object v0, p0, Liwn;->a:Liwi;

    invoke-interface {v0}, Liwi;->a()Lixh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lixh;Lixh;)V
    .locals 1

    iget-object v0, p0, Liwn;->a:Liwi;

    invoke-interface {v0, p1, p2}, Liwi;->a(Lixh;Lixh;)V

    return-void
.end method

.method public final a(Lixh;Lixh;Lixh;)V
    .locals 1

    iget-object v0, p0, Liwn;->a:Liwi;

    invoke-interface {v0, p1, p2, p3}, Liwi;->a(Lixh;Lixh;Lixh;)V

    return-void
.end method

.method public final b()Lixh;
    .locals 1

    iget-object v0, p0, Liwn;->a:Liwi;

    invoke-interface {v0}, Liwi;->b()Lixh;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lixh;Lixh;Lixh;)V
    .locals 2

    invoke-static {}, Liwm;->g()Lixh;

    move-result-object v0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Identity transition matrix must be passed to update in uniform Kalman filter."

    invoke-static {v0, v1}, Lirg;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1, p3}, Liwn;->a(Lixh;Lixh;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
