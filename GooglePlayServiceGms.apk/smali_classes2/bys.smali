.class public final Lbys;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lbzp;


# instance fields
.field private final a:Lbze;

.field private final b:Landroid/view/LayoutInflater;

.field private c:I

.field private final d:Landroid/content/Context;

.field private final e:Lccx;

.field private final f:Lccm;

.field private final g:Lbzr;

.field private final h:Lbzi;

.field private final i:Lbzq;

.field private final j:J

.field private k:Lbzb;

.field private l:Lbyv;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lccx;Lccm;Lbzr;Lbzb;Landroid/view/View$OnClickListener;Lbzi;Lbzq;J)V
    .locals 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p5, p0, Lbys;->k:Lbzb;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lbys;->b:Landroid/view/LayoutInflater;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbys;->d:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccx;

    iput-object v0, p0, Lbys;->e:Lccx;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccm;

    iput-object v0, p0, Lbys;->f:Lccm;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzr;

    iput-object v0, p0, Lbys;->g:Lbzr;

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzi;

    iput-object v0, p0, Lbys;->h:Lbzi;

    invoke-static {p8}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzq;

    iput-object v0, p0, Lbys;->i:Lbzq;

    iput-wide p9, p0, Lbys;->j:J

    new-instance v0, Lbyt;

    invoke-direct {v0, p0}, Lbyt;-><init>(Lbys;)V

    new-instance v1, Lbze;

    iget-object v2, p0, Lbys;->b:Landroid/view/LayoutInflater;

    invoke-direct {v1, v2, p6, v0}, Lbze;-><init>(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;Landroid/database/DataSetObserver;)V

    iput-object v1, p0, Lbys;->a:Lbze;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lccx;Lccm;Lbzr;Lbzb;Landroid/view/View$OnClickListener;Lbzi;Lbzq;JB)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Lbys;-><init>(Landroid/content/Context;Lccx;Lccm;Lbzr;Lbzb;Landroid/view/View$OnClickListener;Lbzi;Lbzq;J)V

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lbys;->k:Lbzb;

    invoke-interface {v0}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lbys;->c:I

    return-void

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)Lcar;
    .locals 2

    iget-object v0, p0, Lbys;->l:Lbyv;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyx;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lbyx;->c:Lcar;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lbzb;)V
    .locals 3

    iget-object v0, p0, Lbys;->k:Lbzb;

    invoke-interface {v0}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object p1, p0, Lbys;->k:Lbzb;

    iget-object v0, p0, Lbys;->l:Lbyv;

    iget-object v1, p0, Lbys;->k:Lbzb;

    iget-object v2, v0, Lbyv;->e:Lcag;

    invoke-virtual {v2, v1}, Lcag;->a(Lbzb;)V

    iput-object v1, v0, Lbyv;->k:Lbzb;

    iget-object v1, v0, Lbyv;->e:Lcag;

    invoke-virtual {v1}, Lcag;->b()Landroid/widget/SectionIndexer;

    move-result-object v1

    iput-object v1, v0, Lbyv;->i:Landroid/widget/SectionIndexer;

    invoke-virtual {p0}, Lbys;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lcag;Lbzk;Ljava/lang/String;)V
    .locals 12

    new-instance v0, Lbyv;

    iget-object v1, p0, Lbys;->d:Landroid/content/Context;

    iget-object v1, p0, Lbys;->k:Lbzb;

    iget-object v5, p0, Lbys;->e:Lccx;

    iget-object v6, p0, Lbys;->f:Lccm;

    iget-object v7, p0, Lbys;->g:Lbzr;

    iget-object v8, p0, Lbys;->h:Lbzi;

    iget-object v9, p0, Lbys;->i:Lbzq;

    iget-wide v10, p0, Lbys;->j:J

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v11}, Lbyv;-><init>(Lbzb;Lcag;Ljava/lang/String;Lbzk;Lccx;Lccm;Lbzr;Lbzi;Lbzq;J)V

    iput-object v0, p0, Lbys;->l:Lbyv;

    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lbys;->a:Lbze;

    iget-boolean v0, v2, Lbze;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v2, Lbze;->f:Z

    if-ne v0, p1, :cond_0

    iget-object v0, v2, Lbze;->g:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v1, v2, Lbze;->e:Z

    iput-boolean p1, v2, Lbze;->f:Z

    iput-object p2, v2, Lbze;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lbze;->a()V

    iget-object v0, v2, Lbze;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    invoke-direct {p0}, Lbys;->c()V

    iget v0, p0, Lbys;->c:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lbys;->a:Lbze;

    iget-boolean v1, v0, Lbze;->e:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbze;->e:Z

    iget-object v0, v0, Lbze;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbys;->a(Landroid/view/View;)Lcar;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lcar;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyx;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lbyx;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lbyx;->d:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyx;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lbyx;->d:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 2

    invoke-direct {p0}, Lbys;->c()V

    iget v1, p0, Lbys;->c:I

    iget-object v0, p0, Lbys;->a:Lbze;

    iget-boolean v0, v0, Lbze;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbys;->k:Lbzb;

    invoke-interface {v0}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    iget v0, p0, Lbys;->c:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    iget-object v0, p0, Lbys;->l:Lbyv;

    iget-object v0, v0, Lbyv;->i:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    iget-object v0, p0, Lbys;->l:Lbyv;

    iget-object v0, v0, Lbyv;->i:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbys;->l:Lbyv;

    iget-object v0, v0, Lbyv;->i:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    invoke-virtual/range {p0 .. p1}, Lbys;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lbys;->a:Lbze;

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    instance-of v1, v0, Landroid/widget/Button;

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v2, Lbze;->a:Landroid/view/LayoutInflater;

    iget v3, v2, Lbze;->d:I

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a00fa    # com.google.android.gms.R.id.sync_more_button

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v2, Lbze;->h:Landroid/widget/Button;

    iget-object v1, v2, Lbze;->h:Landroid/widget/Button;

    iget-object v3, v2, Lbze;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {v2}, Lbze;->a()V

    :goto_0
    return-object p2

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lbys;->k:Lbzb;

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "this should only be called when cursor is valid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move/from16 v0, p1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "couldn\'t move cursor to position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    if-nez p2, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lbys;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f04004a    # com.google.android.gms.R.layout.drive_doc_entry_row

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object/from16 v0, p0

    iget-object v1, v0, Lbys;->l:Lbyv;

    new-instance v1, Lbyx;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lbyx;-><init>(Landroid/view/View;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lbys;->l:Lbyv;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbys;->k:Lbzb;

    invoke-interface {v9}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lbyx;

    const/4 v1, 0x1

    iput-boolean v1, v7, Lbyx;->s:Z

    iget-wide v5, v8, Lbyv;->h:J

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v1

    invoke-virtual {v1}, Lcef;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v10}, Lcef;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/google/android/gms/drive/DriveId;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/DriveId;-><init>(Ljava/lang/String;JJ)V

    iget-object v2, v8, Lbyv;->k:Lbzb;

    invoke-interface {v2}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to bind view with a wrong cursor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_6
    iget-object v2, v8, Lbyv;->e:Lcag;

    invoke-virtual {v2}, Lcag;->a()Lcar;

    move-result-object v2

    iput-object v2, v7, Lbyx;->c:Lcar;

    invoke-interface {v9}, Lbzb;->c()Z

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v8, Lbyv;->d:Lccx;

    invoke-interface {v2}, Lccx;->g()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v5

    iget-object v2, v7, Lbyx;->b:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iget-object v6, v8, Lbyv;->k:Lbzb;

    invoke-interface {v6}, Lbzb;->a()Ljava/lang/String;

    move-result-object v6

    sget-object v9, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v2, v6, v9}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    iget-object v9, v8, Lbyv;->k:Lbzb;

    invoke-interface {v9}, Lbzb;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcfp;->o(Ljava/lang/String;)I

    move-result v9

    iget-object v11, v7, Lbyx;->a:Landroid/content/Context;

    invoke-virtual {v11, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, " "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v8}, Lbyv;->a()Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->setEnabled(Z)V

    invoke-interface {v10}, Landroid/database/Cursor;->isFirst()Z

    move-result v2

    iget-object v6, v7, Lbyx;->c:Lcar;

    invoke-interface {v6}, Lcar;->a()Z

    move-result v6

    if-eqz v6, :cond_f

    iget-object v6, v7, Lbyx;->c:Lcar;

    iget-object v9, v7, Lbyx;->a:Landroid/content/Context;

    invoke-interface {v6, v9}, Lcar;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iget-object v9, v7, Lbyx;->d:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iget-object v11, v7, Lbyx;->f:Landroid/view/View;

    iget-object v12, v7, Lbyx;->e:Landroid/view/View;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    if-eqz v11, :cond_7

    if-eqz v2, :cond_e

    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v11, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    const/4 v2, 0x0

    invoke-virtual {v9, v6, v2}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    :goto_2
    iget-object v2, v8, Lbyv;->k:Lbzb;

    iget-object v6, v8, Lbyv;->e:Lcag;

    invoke-virtual {v6}, Lcag;->c()Lcdp;

    move-result-object v6

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Lbzb;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-boolean v6, v8, Lbyv;->c:Z

    if-nez v6, :cond_8

    iget-object v6, v7, Lbyx;->a:Landroid/content/Context;

    iget v9, v8, Lbyv;->g:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v8, Lbyv;->f:Lccm;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lccm;->a(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-virtual {v6, v9, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-boolean v2, v8, Lbyv;->c:Z

    if-nez v2, :cond_9

    iget-boolean v2, v8, Lbyv;->a:Z

    if-nez v2, :cond_9

    iget-object v2, v7, Lbyx;->o:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v7, Lbyx;->p:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v7, Lbyx;->n:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v7, Lbyx;->r:Landroid/widget/ProgressBar;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v7, Lbyx;->q:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v7, Lbyx;->q:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v2, v7, Lbyx;->r:Landroid/widget/ProgressBar;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setTag(Ljava/lang/Object;)V

    :cond_9
    iget-object v2, v8, Lbyv;->k:Lbzb;

    invoke-interface {v2}, Lbzb;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v6, v7, Lbyx;->i:Landroid/widget/ImageView;

    invoke-static {v2, v3}, Lcfp;->a(Ljava/lang/String;Z)I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xb

    if-lt v2, v9, :cond_10

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_a

    invoke-virtual {v8}, Lbyv;->a()Z

    move-result v2

    if-eqz v2, :cond_11

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_4
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_a
    if-eqz v5, :cond_12

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :goto_5
    iget-boolean v2, v8, Lbyv;->b:Z

    if-nez v2, :cond_13

    if-eqz v3, :cond_13

    const/4 v2, 0x1

    :goto_6
    iget-boolean v3, v7, Lbyx;->m:Z

    if-nez v3, :cond_15

    iget-object v3, v7, Lbyx;->j:Landroid/widget/ImageView;

    if-eqz v2, :cond_14

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_b
    :goto_8
    const-string v2, "  "

    invoke-static {v2, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v7, Lbyx;->g:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    iget-object v2, v7, Lbyx;->k:Landroid/view/View;

    if-eqz v2, :cond_c

    invoke-virtual {v8}, Lbyv;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    :cond_c
    invoke-virtual {v8, v10, v7, v1}, Lbyv;->a(Landroid/database/Cursor;Lbyx;Lcom/google/android/gms/drive/DriveId;)V

    iget-object v1, v7, Lbyx;->h:Landroid/view/View;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz v5, :cond_16

    const v3, 0x7f0c0018    # com.google.android.gms.R.color.drive_list_entry_selected

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_d
    :goto_9
    invoke-virtual {v8}, Lbyv;->a()Z

    move-result v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, v7, Lbyx;->g:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iget-boolean v1, v7, Lbyx;->s:Z

    if-eqz v1, :cond_17

    const/4 v1, 0x0

    :goto_a
    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_f
    iget-object v2, v7, Lbyx;->e:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_10
    const/4 v2, 0x0

    goto :goto_3

    :cond_11
    const v2, 0x3f19999a    # 0.6f

    goto :goto_4

    :cond_12
    const v2, 0x7f0200c6    # com.google.android.gms.R.drawable.drive_doc_icon_state_selector_background

    invoke-static {v6, v2}, Lbyv;->a(Landroid/view/View;I)V

    goto :goto_5

    :cond_13
    const/4 v2, 0x0

    goto :goto_6

    :cond_14
    const/16 v2, 0x8

    goto :goto_7

    :cond_15
    if-eqz v2, :cond_b

    iget-object v2, v7, Lbyx;->a:Landroid/content/Context;

    const v3, 0x7f0b0063    # com.google.android.gms.R.string.drive_shared_status

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_16
    const v2, 0x7f0200c8    # com.google.android.gms.R.drawable.drive_doclist_state_selector_background

    invoke-static {v1, v2}, Lbyv;->a(Landroid/view/View;I)V

    goto :goto_9

    :cond_17
    const/16 v1, 0x8

    goto :goto_a
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    invoke-direct {p0}, Lbys;->c()V

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
