.class public final Lbvx;
.super Lbvv;
.source "SourceFile"


# instance fields
.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public constructor <init>(Lbvs;)V
    .locals 1

    invoke-direct {p0, p1}, Lbvv;-><init>(Lbvs;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbvx;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvx;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lbvx;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Already finished. Did you delegate from more than one FeedProcessor to this one?"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-super {p0, p1}, Lbvv;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lbvx;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lbvx;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "NextFeedMonitorProcessor[delegate=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbvv;->a:Lbvs;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
