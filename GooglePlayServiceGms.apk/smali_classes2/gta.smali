.class public final enum Lgta;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgta;

.field public static final enum b:Lgta;

.field public static final enum c:Lgta;

.field public static final enum d:Lgta;

.field public static final enum e:Lgta;

.field public static final enum f:Lgta;

.field public static final enum g:Lgta;

.field private static final synthetic h:[Lgta;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lgta;

    const-string v1, "CART"

    invoke-direct {v0, v1, v3}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->a:Lgta;

    new-instance v0, Lgta;

    const-string v1, "CARTID"

    invoke-direct {v0, v1, v4}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->b:Lgta;

    new-instance v0, Lgta;

    const-string v1, "ITEM"

    invoke-direct {v0, v1, v5}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->c:Lgta;

    new-instance v0, Lgta;

    const-string v1, "REFUNDABLE"

    invoke-direct {v0, v1, v6}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->d:Lgta;

    new-instance v0, Lgta;

    const-string v1, "SUBSCRIPTION"

    invoke-direct {v0, v1, v7}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->e:Lgta;

    new-instance v0, Lgta;

    const-string v1, "TEMPLATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->f:Lgta;

    new-instance v0, Lgta;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lgta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgta;->g:Lgta;

    const/4 v0, 0x7

    new-array v0, v0, [Lgta;

    sget-object v1, Lgta;->a:Lgta;

    aput-object v1, v0, v3

    sget-object v1, Lgta;->b:Lgta;

    aput-object v1, v0, v4

    sget-object v1, Lgta;->c:Lgta;

    aput-object v1, v0, v5

    sget-object v1, Lgta;->d:Lgta;

    aput-object v1, v0, v6

    sget-object v1, Lgta;->e:Lgta;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgta;->f:Lgta;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgta;->g:Lgta;

    aput-object v2, v0, v1

    sput-object v0, Lgta;->h:[Lgta;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgta;
    .locals 1

    const-class v0, Lgta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgta;

    return-object v0
.end method

.method public static values()[Lgta;
    .locals 1

    sget-object v0, Lgta;->h:[Lgta;

    invoke-virtual {v0}, [Lgta;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgta;

    return-object v0
.end method
