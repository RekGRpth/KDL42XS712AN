.class public final Leyk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final d:Leyk;


# instance fields
.field public a:F

.field public b:F

.field public c:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Leyk;

    invoke-direct {v0, v1, v1, v1}, Leyk;-><init>(FFF)V

    sput-object v0, Leyk;->d:Leyk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Leyk;->a(FFF)V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 3

    iget v0, p0, Leyk;->a:F

    iget v1, p0, Leyk;->a:F

    mul-float/2addr v0, v1

    iget v1, p0, Leyk;->b:F

    iget v2, p0, Leyk;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Leyk;->c:F

    iget v2, p0, Leyk;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    iget v1, p0, Leyk;->a:F

    div-float/2addr v1, v0

    iput v1, p0, Leyk;->a:F

    iget v1, p0, Leyk;->b:F

    div-float/2addr v1, v0

    iput v1, p0, Leyk;->b:F

    iget v1, p0, Leyk;->c:F

    div-float/2addr v1, v0

    iput v1, p0, Leyk;->c:F

    :cond_0
    return v0
.end method

.method public final a(Leyk;)F
    .locals 3

    iget v0, p0, Leyk;->a:F

    iget v1, p1, Leyk;->a:F

    mul-float/2addr v0, v1

    iget v1, p0, Leyk;->b:F

    iget v2, p1, Leyk;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Leyk;->c:F

    iget v2, p1, Leyk;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final a(FFF)V
    .locals 0

    iput p1, p0, Leyk;->a:F

    iput p2, p0, Leyk;->b:F

    iput p3, p0, Leyk;->c:F

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Leyk;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Leyk;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Leyk;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
