.class public abstract Lms;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field protected A:I

.field protected B:I

.field protected C:J

.field D:Z

.field private a:I

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field private e:Lmx;

.field protected k:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field l:I

.field protected m:I

.field protected n:J

.field o:J

.field protected p:Z

.field protected q:I

.field r:Z

.field public s:Lmw;

.field t:Lmu;

.field protected u:Z

.field public v:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public w:J

.field protected x:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field y:J

.field protected z:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, -0x1

    const-wide/high16 v1, -0x8000000000000000L

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lms;->k:I

    iput-wide v1, p0, Lms;->n:J

    iput-boolean v0, p0, Lms;->p:Z

    iput-boolean v0, p0, Lms;->r:Z

    iput v3, p0, Lms;->v:I

    iput-wide v1, p0, Lms;->w:J

    iput v3, p0, Lms;->x:I

    iput-wide v1, p0, Lms;->y:J

    iput v3, p0, Lms;->B:I

    iput-wide v1, p0, Lms;->C:J

    iput-boolean v0, p0, Lms;->D:Z

    return-void
.end method

.method private a(I)J
    .locals 2

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method static synthetic a(Lms;)Landroid/os/Parcelable;
    .locals 1

    invoke-virtual {p0}, Lms;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lms;->s:Lmw;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lms;->v:I

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lms;->b()Landroid/view/View;

    iget-object v1, p0, Lms;->s:Lmw;

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/widget/Adapter;->getItemId(I)J

    invoke-interface {v1, v0}, Lmw;->a(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lms;->s:Lmw;

    goto :goto_0
.end method

.method static synthetic a(Lms;Landroid/os/Parcelable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lms;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method static synthetic b(Lms;)V
    .locals 0

    invoke-direct {p0}, Lms;->a()V

    return-void
.end method


# virtual methods
.method public a(Lmu;)V
    .locals 0

    iput-object p1, p0, Lms;->t:Lmu;

    return-void
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lms;->t:Lmu;

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Lms;->playSoundEffect(I)V

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    :cond_0
    iget-object v1, p0, Lms;->t:Lmu;

    invoke-interface {v1, p1, p2}, Lmu;->a(Landroid/view/View;I)V

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract b()Landroid/view/View;
.end method

.method protected final b(I)V
    .locals 2

    iput p1, p0, Lms;->x:I

    invoke-direct {p0, p1}, Lms;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, Lms;->y:J

    return-void
.end method

.method public abstract c()Landroid/widget/Adapter;
.end method

.method protected final c(I)V
    .locals 2

    iput p1, p0, Lms;->v:I

    invoke-direct {p0, p1}, Lms;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, Lms;->w:J

    iget-boolean v0, p0, Lms;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lms;->q:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    iput p1, p0, Lms;->m:I

    iget-wide v0, p0, Lms;->w:J

    iput-wide v0, p0, Lms;->n:J

    :cond_0
    return-void
.end method

.method protected canAnimate()Z
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lms;->z:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget v0, p0, Lms;->v:I

    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    invoke-virtual {p0}, Lms;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    invoke-virtual {p0, p1}, Lms;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    invoke-virtual {p0, p1}, Lms;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected final e()V
    .locals 6

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_4

    move v3, v2

    :goto_1
    if-eqz v3, :cond_5

    iget-boolean v0, p0, Lms;->d:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    if-eqz v3, :cond_6

    iget-boolean v0, p0, Lms;->c:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v0, p0, Lms;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    if-eqz v4, :cond_1

    invoke-interface {v4}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    :goto_4
    if-eqz v2, :cond_9

    iget-object v0, p0, Lms;->b:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lms;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v5}, Lms;->setVisibility(I)V

    :goto_5
    iget-boolean v0, p0, Lms;->u:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lms;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lms;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lms;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lms;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lms;->onLayout(ZIIII)V

    :cond_2
    :goto_6
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move v2, v1

    goto :goto_4

    :cond_8
    invoke-virtual {p0, v1}, Lms;->setVisibility(I)V

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lms;->b:Landroid/view/View;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lms;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    invoke-virtual {p0, v1}, Lms;->setVisibility(I)V

    goto :goto_6
.end method

.method protected final f()V
    .locals 14

    iget v7, p0, Lms;->z:I

    const/4 v4, 0x0

    if-lez v7, :cond_f

    iget-boolean v0, p0, Lms;->p:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    iput-boolean v0, p0, Lms;->p:Z

    iget v8, p0, Lms;->z:I

    if-eqz v8, :cond_d

    iget-wide v9, p0, Lms;->n:J

    iget v0, p0, Lms;->m:I

    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v1, v9, v1

    if-eqz v1, :cond_d

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v8, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v5, 0x64

    add-long v11, v2, v5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v13

    if-eqz v13, :cond_d

    move v2, v1

    move v3, v1

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    cmp-long v5, v5, v11

    if-gtz v5, :cond_d

    invoke-interface {v13, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v5

    cmp-long v5, v5, v9

    if-nez v5, :cond_6

    :goto_1
    if-ltz v3, :cond_e

    if-ne v3, v3, :cond_e

    invoke-virtual {p0, v3}, Lms;->c(I)V

    const/4 v4, 0x1

    move v0, v4

    :goto_2
    if-nez v0, :cond_4

    iget v1, p0, Lms;->v:I

    if-lt v1, v7, :cond_1

    add-int/lit8 v1, v7, -0x1

    :cond_1
    if-gez v1, :cond_2

    const/4 v1, 0x0

    :cond_2
    if-gez v1, :cond_3

    :cond_3
    if-ltz v1, :cond_4

    invoke-virtual {p0, v1}, Lms;->c(I)V

    invoke-virtual {p0}, Lms;->g()V

    const/4 v0, 0x1

    :cond_4
    :goto_3
    if-nez v0, :cond_5

    const/4 v0, -0x1

    iput v0, p0, Lms;->x:I

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lms;->y:J

    const/4 v0, -0x1

    iput v0, p0, Lms;->v:I

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lms;->w:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lms;->p:Z

    invoke-virtual {p0}, Lms;->g()V

    :cond_5
    return-void

    :cond_6
    add-int/lit8 v5, v8, -0x1

    if-ne v1, v5, :cond_9

    const/4 v5, 0x1

    move v6, v5

    :goto_4
    if-nez v2, :cond_a

    const/4 v5, 0x1

    :goto_5
    if-eqz v6, :cond_7

    if-nez v5, :cond_d

    :cond_7
    if-nez v5, :cond_8

    if-eqz v0, :cond_b

    if-nez v6, :cond_b

    :cond_8
    add-int/lit8 v1, v1, 0x1

    const/4 v0, 0x0

    move v3, v1

    goto :goto_0

    :cond_9
    const/4 v5, 0x0

    move v6, v5

    goto :goto_4

    :cond_a
    const/4 v5, 0x0

    goto :goto_5

    :cond_b
    if-nez v6, :cond_c

    if-nez v0, :cond_0

    if-nez v5, :cond_0

    :cond_c
    add-int/lit8 v2, v2, -0x1

    const/4 v0, 0x1

    move v3, v2

    goto :goto_0

    :cond_d
    const/4 v3, -0x1

    goto :goto_1

    :cond_e
    move v0, v4

    goto :goto_2

    :cond_f
    move v0, v4

    goto :goto_3
.end method

.method protected final g()V
    .locals 4

    iget v0, p0, Lms;->x:I

    iget v1, p0, Lms;->B:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lms;->y:J

    iget-wide v2, p0, Lms;->C:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    :cond_0
    iget-object v0, p0, Lms;->s:Lmw;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lms;->r:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lms;->D:Z

    if-eqz v0, :cond_6

    :cond_1
    iget-object v0, p0, Lms;->e:Lmx;

    if-nez v0, :cond_2

    new-instance v0, Lmx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmx;-><init>(Lms;B)V

    iput-object v0, p0, Lms;->e:Lmx;

    :cond_2
    iget-object v0, p0, Lms;->e:Lmx;

    invoke-virtual {p0, v0}, Lms;->post(Ljava/lang/Runnable;)Z

    :cond_3
    :goto_0
    iget v0, p0, Lms;->x:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    invoke-virtual {p0}, Lms;->isShown()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lms;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lms;->sendAccessibilityEvent(I)V

    :cond_4
    iget v0, p0, Lms;->x:I

    iput v0, p0, Lms;->B:I

    iget-wide v0, p0, Lms;->y:J

    iput-wide v0, p0, Lms;->C:J

    :cond_5
    return-void

    :cond_6
    invoke-direct {p0}, Lms;->a()V

    goto :goto_0
.end method

.method final h()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    iput-boolean v4, p0, Lms;->p:Z

    iget v0, p0, Lms;->a:I

    int-to-long v0, v0

    iput-wide v0, p0, Lms;->o:J

    iget v0, p0, Lms;->x:I

    if-ltz v0, :cond_2

    iget v0, p0, Lms;->x:I

    iget v1, p0, Lms;->k:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-wide v1, p0, Lms;->w:J

    iput-wide v1, p0, Lms;->n:J

    iget v1, p0, Lms;->v:I

    iput v1, p0, Lms;->m:I

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lms;->l:I

    :cond_0
    iput v3, p0, Lms;->q:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, v3}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v1

    iget v2, p0, Lms;->k:I

    if-ltz v2, :cond_4

    iget v2, p0, Lms;->k:I

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    iget v2, p0, Lms;->k:I

    invoke-interface {v1, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v1

    iput-wide v1, p0, Lms;->n:J

    :goto_1
    iget v1, p0, Lms;->k:I

    iput v1, p0, Lms;->m:I

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lms;->l:I

    :cond_3
    iput v4, p0, Lms;->q:I

    goto :goto_0

    :cond_4
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lms;->n:J

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Lms;->e:Lmx;

    invoke-virtual {p0, v0}, Lms;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    invoke-virtual {p0}, Lms;->getHeight()I

    move-result v0

    iput v0, p0, Lms;->a:I

    return-void
.end method

.method public removeAllViews()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeViewAt(I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFocusable(Z)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean p1, p0, Lms;->c:Z

    if-nez p1, :cond_1

    iput-boolean v1, p0, Lms;->d:Z

    :cond_1
    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean p1, p0, Lms;->d:Z

    if-eqz p1, :cond_1

    iput-boolean v2, p0, Lms;->c:Z

    :cond_1
    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
