.class final Lcfh;
.super Lcfl;
.source "SourceFile"


# instance fields
.field final a:Lbqr;

.field private final b:Lcff;


# direct methods
.method public constructor <init>(Lcdu;Lcff;Lbqr;)V
    .locals 2

    invoke-static {}, Lcdi;->a()Lcdi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    iput-object v0, p0, Lcfh;->b:Lcff;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqr;

    iput-object v0, p0, Lcfh;->a:Lbqr;

    invoke-virtual {p2}, Lcff;->m()Z

    move-result v0

    const-string v1, "app must be saved to database before creating scopes"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    return-void
.end method

.method public static a(Lcdu;Lcff;Landroid/database/Cursor;)Lcfh;
    .locals 4

    sget-object v0, Lcdj;->a:Lcdj;

    invoke-virtual {v0}, Lcdj;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p1, Lcfl;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    sget-object v0, Lcdj;->b:Lcdj;

    invoke-virtual {v0}, Lcdj;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Lbqr;->a(I)Lbqr;

    move-result-object v0

    new-instance v1, Lcfh;

    invoke-direct {v1, p0, p1, v0}, Lcfh;-><init>(Lcdu;Lcff;Lbqr;)V

    invoke-static {}, Lcdi;->a()Lcdi;

    move-result-object v0

    invoke-virtual {v0}, Lcdi;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcfh;->d(J)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcdj;->a:Lcdj;

    invoke-virtual {v0}, Lcdj;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfh;->b:Lcff;

    iget-wide v1, v1, Lcfl;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdj;->b:Lcdj;

    invoke-virtual {v0}, Lcdj;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfh;->a:Lbqr;

    invoke-virtual {v1}, Lbqr;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "AppScope [app=%s, scope=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcfh;->b:Lcff;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcfh;->a:Lbqr;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
