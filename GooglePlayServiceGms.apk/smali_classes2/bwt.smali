.class final Lbwt;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private final a:Lbwk;

.field private final b:J

.field private c:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lbwk;JJ)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lbwt;->a:Lbwk;

    iput-wide p3, p0, Lbwt;->b:J

    iput-wide p5, p0, Lbwt;->c:J

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {}, Lbta;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbuf;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - thread interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbuf;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lbwt;->c:J

    long-to-int v1, v1

    iput v1, v0, Lbuf;->bytesTransferred:I

    throw v0

    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 5

    iget-wide v0, p0, Lbwt;->c:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lbwt;->c:J

    iget-object v0, p0, Lbwt;->a:Lbwk;

    iget-wide v1, p0, Lbwt;->c:J

    iget-wide v3, p0, Lbwt;->b:J

    invoke-interface {v0, v1, v2, v3, v4}, Lbwk;->a(JJ)V

    return-void
.end method


# virtual methods
.method public final read()I
    .locals 3

    invoke-direct {p0}, Lbwt;->a()V

    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    invoke-direct {p0}, Lbwt;->a()V

    const-wide/16 v1, 0x1

    invoke-direct {p0, v1, v2}, Lbwt;->a(J)V

    return v0
.end method

.method public final read([B)I
    .locals 3

    invoke-direct {p0}, Lbwt;->a()V

    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->read([B)I

    move-result v0

    invoke-direct {p0}, Lbwt;->a()V

    if-lez v0, :cond_0

    int-to-long v1, v0

    invoke-direct {p0, v1, v2}, Lbwt;->a(J)V

    :cond_0
    return v0
.end method

.method public final read([BII)I
    .locals 3

    invoke-direct {p0}, Lbwt;->a()V

    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    invoke-direct {p0}, Lbwt;->a()V

    if-lez v0, :cond_0

    int-to-long v1, v0

    invoke-direct {p0, v1, v2}, Lbwt;->a(J)V

    :cond_0
    return v0
.end method
