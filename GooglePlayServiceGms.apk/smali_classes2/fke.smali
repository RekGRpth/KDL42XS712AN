.class final Lfke;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfkm;


# instance fields
.field final synthetic a:Lfjx;

.field final synthetic b:Lfkc;


# direct methods
.method constructor <init>(Lfkc;Lfjx;)V
    .locals 0

    iput-object p1, p0, Lfke;->b:Lfkc;

    iput-object p2, p0, Lfke;->a:Lfjx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lfke;->b:Lfkc;

    invoke-static {v0}, Lfkc;->a(Lfkc;)Lfkh;

    move-result-object v0

    invoke-virtual {v0}, Lfkh;->c()Landroid/app/PendingIntent;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lfke;->a:Lfjx;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lfjx;->a(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver success"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    const-string v0, "AutoBackupService"

    const-string v1, "Not bound to internal G+ Service."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lfke;->a:Lfjx;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfjx;->a(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver failure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
