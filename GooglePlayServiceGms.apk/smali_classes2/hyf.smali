.class public final Lhyf;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z

.field public c:D

.field public d:Z

.field public e:D

.field public f:Z

.field public g:F

.field public h:Z

.field public i:J

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:I

.field public n:Z

.field public o:I

.field public p:Z

.field public q:I

.field private r:Z

.field private s:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lhyf;->a:I

    iput-wide v1, p0, Lhyf;->c:D

    iput-wide v1, p0, Lhyf;->e:D

    const/4 v0, 0x0

    iput v0, p0, Lhyf;->g:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhyf;->i:J

    const-string v0, ""

    iput-object v0, p0, Lhyf;->k:Ljava/lang/String;

    iput v4, p0, Lhyf;->m:I

    iput v4, p0, Lhyf;->o:I

    iput v3, p0, Lhyf;->q:I

    iput v3, p0, Lhyf;->s:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lhyf;->s:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lhyf;->b()I

    :cond_0
    iget v0, p0, Lhyf;->s:I

    return v0
.end method

.method public final a(D)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->b:Z

    iput-wide p1, p0, Lhyf;->c:D

    return-object p0
.end method

.method public final a(F)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->f:Z

    iput p1, p0, Lhyf;->g:F

    return-object p0
.end method

.method public final a(I)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->r:Z

    iput p1, p0, Lhyf;->a:I

    return-object p0
.end method

.method public final a(J)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->h:Z

    iput-wide p1, p0, Lhyf;->i:J

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->j:Z

    iput-object p1, p0, Lhyf;->k:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyf;->a(I)Lhyf;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyf;->a(D)Lhyf;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyf;->b(D)Lhyf;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lhyf;->a(F)Lhyf;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyf;->a(J)Lhyf;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhyf;->a(Ljava/lang/String;)Lhyf;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyf;->b(I)Lhyf;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyf;->c(I)Lhyf;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lhyf;->d(I)Lhyf;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Lhyf;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhyf;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Lhyf;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lhyf;->c:D

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(ID)V

    :cond_1
    iget-boolean v0, p0, Lhyf;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lhyf;->e:D

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(ID)V

    :cond_2
    iget-boolean v0, p0, Lhyf;->f:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lhyf;->g:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_3
    iget-boolean v0, p0, Lhyf;->h:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lhyf;->i:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_4
    iget-boolean v0, p0, Lhyf;->j:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhyf;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Lhyf;->l:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lhyf;->m:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_6
    iget-boolean v0, p0, Lhyf;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Lhyf;->o:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_7
    iget-boolean v0, p0, Lhyf;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v1, p0, Lhyf;->q:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_8
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhyf;->r:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhyf;->a:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lhyf;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lhyf;->c:D

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lhyf;->d:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lhyf;->e:D

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lhyf;->f:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lhyf;->g:F

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lhyf;->h:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lhyf;->i:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lhyf;->j:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhyf;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lhyf;->l:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Lhyf;->m:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lhyf;->n:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Lhyf;->o:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhyf;->p:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget v2, p0, Lhyf;->q:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lhyf;->s:I

    return v0
.end method

.method public final b(D)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->d:Z

    iput-wide p1, p0, Lhyf;->e:D

    return-object p0
.end method

.method public final b(I)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->l:Z

    iput p1, p0, Lhyf;->m:I

    return-object p0
.end method

.method public final c(I)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->n:Z

    iput p1, p0, Lhyf;->o:I

    return-object p0
.end method

.method public final d(I)Lhyf;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyf;->p:Z

    iput p1, p0, Lhyf;->q:I

    return-object p0
.end method
