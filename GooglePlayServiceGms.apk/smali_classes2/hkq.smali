.class final Lhkq;
.super Lhsi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhko;


# direct methods
.method private constructor <init>(Lhko;)V
    .locals 0

    iput-object p1, p0, Lhkq;->a:Lhko;

    invoke-direct {p0}, Lhsi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhko;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhkq;-><init>(Lhko;)V

    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0 0"

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v1, v0, Lhng;->a:J

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v3, v0, Lhng;->a:J

    sub-long v0, v3, v1

    long-to-double v2, v0

    const-wide v4, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%.2g"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
    .locals 6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmy;

    new-instance v4, Lcom/google/android/gms/location/DetectedActivity;

    iget-object v1, v0, Lhmy;->a:Lhmz;

    invoke-static {}, Lhko;->b()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v0, v0, Lhmy;->b:I

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a(Livi;)V
    .locals 14

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->a(Lhko;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityDetector"

    const-string v1, "Not processing accel data since activity detection was canceled."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->b(Lhko;)Z

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->c(Lhko;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v1

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->c(Lhko;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v3

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->d(Lhko;)Lhlk;

    sget-object v0, Lhrz;->d:Lhrz;

    invoke-static {p1, v0}, Lhlk;->a(Livi;Lhrz;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x1

    if-gt v0, v5, :cond_4

    move-object v5, v6

    :goto_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "originalAccel: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lhkq;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " trimmedAccel: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Lhkq;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->e(Lhko;)Lhkp;

    move-result-object v9

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/4 v7, 0x1

    if-le v0, v7, :cond_6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v7, v0, Lhng;->a:J

    const/4 v0, 0x0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v10, v0, Lhng;->a:J

    sub-long/2addr v7, v10

    const-wide/16 v10, 0x0

    cmp-long v0, v7, v10

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v10, v0

    long-to-double v7, v7

    const-wide v12, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v7, v12

    div-double v7, v10, v7

    :goto_2
    invoke-interface {v9, v7, v8}, Lhkp;->a(D)V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/4 v7, 0x4

    if-ge v0, v7, :cond_8

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not enough accel samples for activity detection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->e(Lhko;)Lhkp;

    move-result-object v0

    invoke-interface {v0}, Lhkp;->V_()V

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->c(Lhko;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    new-instance v0, Lide;

    sget-object v2, Licn;->S:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lide;-><init>(Licp;Licn;JI)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    goto/16 :goto_0

    :cond_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v7, v0, Lhng;->a:J

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_3
    if-lez v5, :cond_5

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhng;

    iget-wide v9, v0, Lhng;->a:J

    sub-long v9, v7, v9

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->g(Lhko;)J

    move-result-wide v11

    cmp-long v0, v9, v11

    if-gez v0, :cond_5

    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_3

    :cond_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v6, v5, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    move-object v5, v0

    goto/16 :goto_1

    :cond_6
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "ActivityDetector"

    const-string v7, "Not enough samples were found to detect sensor rate."

    invoke-static {v0, v7}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-wide/16 v7, 0x0

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->f(Lhko;)Lhms;

    move-result-object v0

    invoke-interface {v0, v5}, Lhms;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lhkq;->a:Lhko;

    const-wide v7, 0x7fffffffffffffffL

    const/4 v9, 0x0

    invoke-static {v5, v7, v8, v9}, Lhmt;->a(Ljava/util/List;JZ)[D

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lhko;->a(JJ[DLjava/util/List;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz v0, :cond_a

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_9

    const-string v5, "ActivityDetector"

    const-string v7, "Significant tilt detected between activities"

    invoke-static {v5, v7}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v5, p0, Lhkq;->a:Lhko;

    invoke-static {v5}, Lhko;->c(Lhko;)Lidu;

    move-result-object v5

    invoke-interface {v5}, Lidu;->A()Licp;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7}, Licp;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    iget-object v5, p0, Lhkq;->a:Lhko;

    invoke-static {v5}, Lhko;->e(Lhko;)Lhkp;

    move-result-object v5

    invoke-interface {v5, v0}, Lhkp;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    :cond_a
    invoke-static {v6}, Lhkq;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    new-instance v7, Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-wide v9, v1

    move-wide v11, v3

    invoke-direct/range {v7 .. v12}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_b

    const-string v0, "ActivityDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity detection result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->c(Lhko;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Licp;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->e(Lhko;)Lhkp;

    move-result-object v0

    invoke-interface {v0, v7}, Lhkp;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmy;

    iget-object v0, v0, Lhmy;->a:Lhmz;

    invoke-virtual {v0}, Lhmz;->name()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->a(Lhko;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->b(Lhko;)Z

    iget-object v0, p0, Lhkq;->a:Lhko;

    invoke-static {v0}, Lhko;->e(Lhko;)Lhkp;

    move-result-object v0

    invoke-interface {v0, p1}, Lhkp;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
