.class public final enum Lces;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lces;

.field public static final enum b:Lces;

.field public static final enum c:Lces;

.field public static final enum d:Lces;

.field public static final enum e:Lces;

.field public static final enum f:Lces;

.field private static final synthetic h:[Lces;


# instance fields
.field private final g:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x2

    new-instance v0, Lces;

    const-string v1, "CONTENT_HASH"

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "contentHash"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v8, v3, Lcei;->g:Z

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v4

    sget-object v5, Lcem;->a:Lcem;

    invoke-virtual {v5}, Lcem;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->a:Lces;

    new-instance v0, Lces;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "entryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    iput-boolean v8, v3, Lcei;->g:Z

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->b:Lces;

    new-instance v0, Lces;

    const-string v1, "CREATOR_IDENTITY"

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "creatorIdentity"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v8, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->c:Lces;

    new-instance v0, Lces;

    const-string v1, "WRITE_OPEN_TIME"

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "writeOpenTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v8, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->d:Lces;

    new-instance v0, Lces;

    const-string v1, "BYTES_SENT"

    const/4 v2, 0x4

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "bytesSent"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcdq;->a(I)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->e:Lces;

    new-instance v0, Lces;

    const-string v1, "UPLOAD_URI"

    invoke-static {}, Lcer;->d()Lcer;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "uploadUri"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lces;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lces;->f:Lces;

    const/4 v0, 0x6

    new-array v0, v0, [Lces;

    sget-object v1, Lces;->a:Lces;

    aput-object v1, v0, v10

    sget-object v1, Lces;->b:Lces;

    aput-object v1, v0, v8

    sget-object v1, Lces;->c:Lces;

    aput-object v1, v0, v7

    sget-object v1, Lces;->d:Lces;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lces;->e:Lces;

    aput-object v2, v0, v1

    sget-object v1, Lces;->f:Lces;

    aput-object v1, v0, v9

    sput-object v0, Lces;->h:[Lces;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lces;->g:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lces;
    .locals 1

    const-class v0, Lces;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lces;

    return-object v0
.end method

.method public static values()[Lces;
    .locals 1

    sget-object v0, Lces;->h:[Lces;

    invoke-virtual {v0}, [Lces;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lces;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lces;->g:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lces;->g:Lcdp;

    return-object v0
.end method
