.class public final Lhkr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lidz;


# static fields
.field private static volatile w:Z


# instance fields
.field private A:J

.field private B:J

.field private C:J

.field private final a:Lidu;

.field private final b:Z

.field private final c:Lhko;

.field private d:Lhla;

.field private e:J

.field private f:Lcom/google/android/gms/location/ActivityRecognitionResult;

.field private g:Lhlt;

.field private h:J

.field private final i:Lhlb;

.field private final j:Lhli;

.field private final k:Lhln;

.field private l:Ljava/util/List;

.field private final m:Lhms;

.field private final n:Lhms;

.field private final o:Lhms;

.field private p:I

.field private q:Ljava/util/Queue;

.field private r:Ljava/util/Queue;

.field private s:I

.field private t:J

.field private u:J

.field private v:Lilx;

.field private x:I

.field private y:I

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lhkr;->w:Z

    return-void
.end method

.method public constructor <init>(Lidu;)V
    .locals 1

    invoke-interface {p1}, Lidu;->B()Lhln;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhkr;-><init>(Lidu;Lhln;)V

    return-void
.end method

.method private constructor <init>(Lidu;Lhln;)V
    .locals 7

    const-wide v5, 0x7fffffffffffffffL

    const/4 v4, 0x1

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhkw;

    invoke-direct {v0, p0, v1}, Lhkw;-><init>(Lhkr;B)V

    iput-object v0, p0, Lhkr;->d:Lhla;

    iput-wide v2, p0, Lhkr;->e:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhkr;->f:Lcom/google/android/gms/location/ActivityRecognitionResult;

    sget-object v0, Lhlr;->a:Lhlt;

    iput-object v0, p0, Lhkr;->g:Lhlt;

    iput-wide v2, p0, Lhkr;->h:J

    new-instance v0, Lhli;

    invoke-direct {v0}, Lhli;-><init>()V

    iput-object v0, p0, Lhkr;->j:Lhli;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhkr;->l:Ljava/util/List;

    new-instance v0, Lhmr;

    invoke-direct {v0}, Lhmr;-><init>()V

    iput-object v0, p0, Lhkr;->m:Lhms;

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhkr;->n:Lhms;

    new-instance v0, Lhnf;

    invoke-direct {v0}, Lhnf;-><init>()V

    iput-object v0, p0, Lhkr;->o:Lhms;

    iput v1, p0, Lhkr;->p:I

    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    const v0, 0x7fffffff

    iput v0, p0, Lhkr;->s:I

    iput-wide v5, p0, Lhkr;->t:J

    iput-wide v5, p0, Lhkr;->u:J

    iput v4, p0, Lhkr;->x:I

    iput v1, p0, Lhkr;->y:I

    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lhkr;->z:J

    iput-wide v2, p0, Lhkr;->A:J

    iput-wide v2, p0, Lhkr;->B:J

    iput-wide v2, p0, Lhkr;->C:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "ActivityDetectionScheduler started in state off"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Lhkr;->a:Lidu;

    iput-object p2, p0, Lhkr;->k:Lhln;

    new-instance v0, Lhko;

    new-instance v1, Lhlk;

    invoke-direct {v1}, Lhlk;-><init>()V

    invoke-direct {v0, p1, v1}, Lhko;-><init>(Lidu;Lhlk;)V

    iput-object v0, p0, Lhkr;->c:Lhko;

    new-instance v0, Lhlb;

    invoke-direct {v0}, Lhlb;-><init>()V

    iput-object v0, p0, Lhkr;->i:Lhlb;

    invoke-interface {p1}, Lidu;->x()Z

    move-result v0

    iput-boolean v0, p0, Lhkr;->b:Z

    invoke-direct {p0}, Lhkr;->k()Lidx;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Lidx;->a()Livi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->b(I)Z

    move-result v1

    sput-boolean v1, Lhkr;->w:Z

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    iput v0, p0, Lhkr;->x:I

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State loaded: lowPowerMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lhkr;->w:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sensorDelay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lhkr;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-boolean v0, Lhkr;->w:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhkr;->a(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-boolean v0, p0, Lhkr;->b:Z

    if-nez v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityScheduler"

    const-string v1, "No accelerometer detected. Activity detection will be disabled."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0}, Lhkr;->b(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityScheduler"

    const-string v1, "Unable to load data from disk."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lhkr;J)J
    .locals 0

    iput-wide p1, p0, Lhkr;->B:J

    return-wide p1
.end method

.method static synthetic a(Lhms;)J
    .locals 4

    invoke-interface {p0}, Lhms;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private a(J)V
    .locals 5

    iget-wide v0, p0, Lhkr;->e:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    iput-wide p1, p0, Lhkr;->e:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lhkr;->a:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->c()J

    move-result-wide v3

    add-long/2addr v3, p1

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->a:Lidu;

    const/16 v1, 0x8

    iget-object v2, p0, Lhkr;->v:Lilx;

    invoke-interface {v0, v1, p1, p2, v2}, Lidu;->a(IJLilx;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Licp;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    iput-object p1, p0, Lhkr;->f:Lcom/google/android/gms/location/ActivityRecognitionResult;

    iget-object v3, p0, Lhkr;->j:Lhli;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    if-ne v4, v8, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    const/16 v4, 0x3c

    if-lt v0, v4, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, v3, Lhli;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    if-nez v0, :cond_0

    iput-object p1, v3, Lhli;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    :cond_0
    iget v0, v3, Lhli;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lhli;->b:I

    iget-object v0, v3, Lhli;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x75300

    cmp-long v0, v4, v6

    if-ltz v0, :cond_5

    iget v0, v3, Lhli;->b:I

    if-lt v0, v8, :cond_5

    invoke-virtual {v3}, Lhli;->a()V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lhkr;->a(Z)V

    :cond_1
    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0, p1}, Lidu;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-object v1, p0, Lhkr;->i:Lhlb;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v3

    :goto_2
    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v5

    sub-long v5, v3, v5

    const-wide/32 v7, 0x36ee80

    cmp-long v0, v5, v7

    if-lez v0, :cond_7

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityHistory"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Removed "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-ne v0, v8, :cond_6

    move v0, v1

    :goto_3
    if-nez v0, :cond_5

    invoke-virtual {v3}, Lhli;->a()V

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_8

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_9

    :cond_8
    return-void

    :cond_9
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_a

    const-string v0, "ActivityHistory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, v1, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lhlb;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lhlb;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_b

    const-string v0, "ActivityHistory"

    const-string v4, "Merged activities:"

    invoke-static {v0, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-object v0, v1, Lhlb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlc;

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_c

    const-string v4, "ActivityHistory"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MergedActivity [startMillsSinceBoot="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lhlc;->a:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", endMillisSinceBoot="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lhlc;->b:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", start="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v7, v0, Lhlc;->a:J

    add-long/2addr v7, v2

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", end="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v7, v0, Lhlc;->b:J

    add-long/2addr v7, v2

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", activityType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v0, v0, Lhlc;->c:I

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "]"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method static synthetic a(Lhkr;Lhla;)V
    .locals 0

    invoke-direct {p0, p1}, Lhkr;->a(Lhla;)V

    return-void
.end method

.method static synthetic a(Lhkr;Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lhkv;

    invoke-direct {v0, p0, v1}, Lhkv;-><init>(Lhkr;B)V

    invoke-direct {p0, v0}, Lhkr;->a(Lhla;)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lhkr;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhkr;->k:Lhln;

    invoke-interface {v0}, Lhln;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lhkz;

    invoke-direct {v0, p0, v1}, Lhkz;-><init>(Lhkr;B)V

    invoke-direct {p0, v0}, Lhkr;->a(Lhla;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhku;

    invoke-direct {v0, p0, v1}, Lhku;-><init>(Lhkr;B)V

    invoke-direct {p0, v0}, Lhkr;->a(Lhla;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lhkt;

    invoke-direct {v0, p0, v1}, Lhkt;-><init>(Lhkr;B)V

    invoke-direct {p0, v0}, Lhkr;->a(Lhla;)V

    goto :goto_0
.end method

.method private a(Lhla;)V
    .locals 4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Leaving state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkr;->d:Lhla;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhkr;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->e()V

    iput-object p1, p0, Lhkr;->d:Lhla;

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Entering state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkr;->d:Lhla;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhkr;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->d()V

    return-void
.end method

.method private a(Z)V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Switching to low power mode"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    sget-object v1, Licn;->V:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    const/4 v0, 0x1

    sput-boolean v0, Lhkr;->w:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lhkr;->j()V

    :cond_1
    iget-object v0, p0, Lhkr;->a:Lidu;

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_ENABLED"

    invoke-interface {v0, v1}, Lidu;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(ZZ)V
    .locals 8

    const-wide v6, 0x7fffffffffffffffL

    const-wide/16 v4, 0x3e8

    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lhkr;->p:I

    if-nez v0, :cond_2

    iput-wide v6, p0, Lhkr;->t:J

    :goto_0
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lhkr;->t:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lhkr;->t:J

    iget-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lhkr;->u:J

    :goto_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateEnableState minPeriod="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lhkr;->t:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minTiltPeriod="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lhkr;->u:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newClientAdded="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " forceDetectionNow="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lhkr;->p:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->a()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lhkr;->s:I

    int-to-long v0, v0

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lhkr;->t:J

    goto/16 :goto_0

    :cond_3
    iget v1, p0, Lhkr;->s:I

    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lhkr;->t:J

    goto/16 :goto_0

    :cond_4
    iput-wide v6, p0, Lhkr;->u:J

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_1

    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0, p2}, Lhla;->a(Z)V

    goto :goto_2
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lhkr;->w:Z

    return v0
.end method

.method static synthetic a(Lhkr;)Z
    .locals 1

    iget-boolean v0, p0, Lhkr;->b:Z

    return v0
.end method

.method static synthetic b(Lhkr;J)J
    .locals 0

    iput-wide p1, p0, Lhkr;->C:J

    return-wide p1
.end method

.method static synthetic b(Lhkr;)Lidu;
    .locals 1

    iget-object v0, p0, Lhkr;->a:Lidu;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Disabling low power mode"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    sget-object v1, Licn;->U:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lhkr;->j:Lhli;

    invoke-virtual {v0}, Lhli;->a()V

    const/4 v0, 0x0

    sput-boolean v0, Lhkr;->w:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lhkr;->j()V

    :cond_1
    iget-object v0, p0, Lhkr;->a:Lidu;

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_DISABLED"

    invoke-interface {v0, v1}, Lidu;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->B:J

    return-wide v0
.end method

.method static synthetic c(Lhkr;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhkr;->a(J)V

    return-void
.end method

.method static synthetic d(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->C:J

    return-wide v0
.end method

.method static synthetic e(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->A:J

    return-wide v0
.end method

.method static synthetic f(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->t:J

    return-wide v0
.end method

.method static synthetic g(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->z:J

    return-wide v0
.end method

.method static synthetic h(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->u:J

    return-wide v0
.end method

.method static synthetic h()Z
    .locals 1

    sget-boolean v0, Lhkr;->w:Z

    return v0
.end method

.method static synthetic i(Lhkr;)Lhms;
    .locals 1

    iget-object v0, p0, Lhkr;->n:Lhms;

    return-object v0
.end method

.method private i()V
    .locals 4

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lhkr;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Alarm canceled"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->a:Lidu;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lidu;->a(I)V

    iput-wide v2, p0, Lhkr;->e:J

    :cond_1
    return-void
.end method

.method static synthetic j(Lhkr;)Lhms;
    .locals 1

    iget-object v0, p0, Lhkr;->m:Lhms;

    return-object v0
.end method

.method private j()V
    .locals 4

    invoke-direct {p0}, Lhkr;->k()Lidx;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->aY:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    sget-boolean v3, Lhkr;->w:Z

    invoke-virtual {v1, v2, v3}, Livi;->a(IZ)Livi;

    const/4 v2, 0x2

    iget v3, p0, Lhkr;->x:I

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    :try_start_0
    invoke-virtual {v0, v1}, Lidx;->b(Livi;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Unable to save data to disk."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic k(Lhkr;)Lhms;
    .locals 1

    iget-object v0, p0, Lhkr;->o:Lhms;

    return-object v0
.end method

.method private k()Lidx;
    .locals 9

    new-instance v0, Lidx;

    const/4 v1, 0x1

    iget-object v2, p0, Lhkr;->a:Lidu;

    invoke-interface {v2}, Lidu;->h()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lhkr;->a:Lidu;

    invoke-interface {v4}, Lidu;->i()[B

    move-result-object v4

    sget-object v5, Lihj;->aY:Livk;

    iget-object v6, p0, Lhkr;->a:Lidu;

    invoke-interface {v6}, Lidu;->u()Ljava/io/File;

    move-result-object v6

    iget-object v8, p0, Lhkr;->a:Lidu;

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    return-object v0
.end method

.method static synthetic l(Lhkr;)Lhlt;
    .locals 1

    iget-object v0, p0, Lhkr;->g:Lhlt;

    return-object v0
.end method

.method static synthetic m(Lhkr;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 1

    iget-object v0, p0, Lhkr;->f:Lcom/google/android/gms/location/ActivityRecognitionResult;

    return-object v0
.end method

.method static synthetic n(Lhkr;)Lhko;
    .locals 1

    iget-object v0, p0, Lhkr;->c:Lhko;

    return-object v0
.end method

.method static synthetic o(Lhkr;)I
    .locals 1

    iget v0, p0, Lhkr;->x:I

    return v0
.end method

.method static synthetic p(Lhkr;)Lilx;
    .locals 1

    iget-object v0, p0, Lhkr;->v:Lilx;

    return-object v0
.end method

.method static synthetic q(Lhkr;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lhkr;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic r(Lhkr;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhkr;->b(Z)V

    return-void
.end method

.method static synthetic s(Lhkr;)Lhln;
    .locals 1

    iget-object v0, p0, Lhkr;->k:Lhln;

    return-object v0
.end method

.method static synthetic t(Lhkr;)J
    .locals 2

    iget-wide v0, p0, Lhkr;->h:J

    return-wide v0
.end method

.method static synthetic u(Lhkr;)I
    .locals 2

    iget v0, p0, Lhkr;->y:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lhkr;->y:I

    return v0
.end method

.method static synthetic v(Lhkr;)I
    .locals 1

    iget v0, p0, Lhkr;->y:I

    return v0
.end method

.method static synthetic w(Lhkr;)I
    .locals 2

    iget v0, p0, Lhkr;->x:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lhkr;->x:I

    return v0
.end method

.method static synthetic x(Lhkr;)V
    .locals 0

    invoke-direct {p0}, Lhkr;->j()V

    return-void
.end method

.method static synthetic y(Lhkr;)I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lhkr;->y:I

    return v0
.end method

.method static synthetic z(Lhkr;)V
    .locals 0

    invoke-direct {p0}, Lhkr;->i()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    const/16 v6, 0x8

    const-wide/16 v4, -0x1

    iget-wide v0, p0, Lhkr;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iget-wide v2, p0, Lhkr;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    if-ne p1, v6, :cond_0

    iget-wide v0, p0, Lhkr;->e:J

    iput-wide v4, p0, Lhkr;->e:J

    invoke-direct {p0, v0, v1}, Lhkr;->a(J)V

    goto :goto_0

    :cond_2
    if-ne p1, v6, :cond_3

    iput-wide v4, p0, Lhkr;->e:J

    :goto_1
    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->j()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lhkr;->i()V

    goto :goto_1
.end method

.method public final a(IIZLilx;)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setActivityDetectionExternalClientCount: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lhkr;->p:I

    if-le p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lhkr;->p:I

    iput p2, p0, Lhkr;->s:I

    iput-object p4, p0, Lhkr;->v:Lilx;

    invoke-direct {p0, v0, p3}, Lhkr;->a(ZZ)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 1

    if-nez p1, :cond_0

    iput-wide p2, p0, Lhkr;->A:J

    :cond_0
    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0, p1}, Lhla;->a(I)V

    return-void
.end method

.method public final a(IZ)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addActivityDetectionInternalClient "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lhkr;->a(ZZ)V

    return-void
.end method

.method public final a(Lhkx;)V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhkr;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v0, p0, Lhkr;->l:Ljava/util/List;

    return-void
.end method

.method public final a(Lhlt;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0, p1}, Lhla;->a(Lhlt;)V

    iput-object p1, p0, Lhkr;->g:Lhlt;

    iget-object v0, p0, Lhkr;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhkr;->h:J

    goto :goto_0
.end method

.method public final a(Livi;)Z
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lhlb;
    .locals 1

    iget-object v0, p0, Lhkr;->i:Lhlb;

    return-object v0
.end method

.method public final b(I)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeActivityDetectionInternalClient "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->q:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v3, v3}, Lhkr;->a(ZZ)V

    return-void
.end method

.method public final b(Lhkx;)V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhkr;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iput-object v0, p0, Lhkr;->l:Ljava/util/List;

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->f()V

    return-void
.end method

.method public final c(I)V
    .locals 2

    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lhkr;->z:J

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->W_()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lhkr;->d:Lhla;

    invoke-virtual {v0}, Lhla;->X_()V

    return-void
.end method

.method public final f()V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addLowPowerTiltDetectionInternalClient 30"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhkr;->a(ZZ)V

    return-void
.end method

.method public final g()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeLowPowerTiltDetectionInternalClient 30"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkr;->r:Ljava/util/Queue;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v3, v3}, Lhkr;->a(ZZ)V

    return-void
.end method
