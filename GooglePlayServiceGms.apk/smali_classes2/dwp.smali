.class public final Ldwp;
.super Ldxh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ldxh;-><init>()V

    iput-object p1, p0, Ldwp;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldwp;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-object v0, p0, Ldwp;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldwp;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Ldwp;->a:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Ldwp;->f:Z

    iput-object v0, p0, Ldwp;->g:Ljava/lang/String;

    iput-object p1, p0, Ldwp;->h:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Ldwp;->i:Ljava/lang/Object;

    invoke-virtual {p0}, Ldwp;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldwp;->f:Z

    iput-object p1, p0, Ldwp;->h:Landroid/view/View$OnClickListener;

    iput-object p2, p0, Ldwp;->i:Ljava/lang/Object;

    invoke-virtual {p0}, Ldwp;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldwp;->c:Ljava/lang/String;

    invoke-virtual {p0}, Ldwp;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Ldwp;->f:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Ldwp;->f:Z

    invoke-virtual {p0}, Ldwp;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a(II)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lez p1, :cond_1

    invoke-virtual {p0, v0}, Ldwp;->b(Z)V

    sub-int v2, p1, p2

    if-lez v2, :cond_0

    invoke-virtual {p0, v0}, Ldwp;->a(Z)V

    invoke-virtual {p0, v2}, Ldwp;->b(I)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v1}, Ldwp;->a(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Ldwp;->b(Z)V

    move v0, v1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    iget-object v0, p0, Ldwp;->a:Landroid/content/Context;

    sget v1, Lxf;->K:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldwp;->g:Ljava/lang/String;

    invoke-virtual {p0}, Ldwp;->notifyDataSetChanged()V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Ldwp;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->r:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Ldwq;

    invoke-direct {v0, p0, p2}, Ldwq;-><init>(Ldwp;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, v0, Ldwq;->b:Landroid/widget/TextView;

    iget-object v2, v0, Ldwq;->f:Ldwp;

    iget-object v2, v2, Ldwp;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ldwq;->f:Ldwp;

    iget-object v1, v1, Ldwp;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Ldwq;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldwq;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldwq;->f:Ldwp;

    iget-object v2, v2, Ldwp;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, v0, Ldwq;->a:Landroid/view/View;

    iget-object v2, v0, Ldwq;->f:Ldwp;

    iget-object v2, v2, Ldwp;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Ldwq;->f:Ldwp;

    iget-boolean v1, v1, Ldwp;->f:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Ldwq;->a:Landroid/view/View;

    iget-object v2, v0, Ldwq;->f:Ldwp;

    iget-object v2, v2, Ldwp;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Ldwq;->a:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, v0, Ldwq;->d:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Ldwq;->e:Landroid/widget/TextView;

    iget-object v0, v0, Ldwq;->f:Ldwp;

    iget-object v0, v0, Ldwp;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Ldwq;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Ldwq;->a:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, v0, Ldwq;->a:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, v0, Ldwq;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
