.class final Lbss;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbsr;


# direct methods
.method private constructor <init>(Lbsr;)V
    .locals 0

    iput-object p1, p0, Lbss;->a:Lbsr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lbsr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lbss;-><init>(Lbsr;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v1, p0, Lbss;->a:Lbsr;

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->h()V

    const-string v0, "ContentMaintenance"

    const-string v2, "Pruning caches..."

    invoke-static {v0, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->i()J

    move-result-wide v2

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->n()J

    move-result-wide v4

    const-string v0, "ContentMaintenance"

    const-string v6, "Internal cache bytes used: %d; limit: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v0, v6, v7}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    const-string v0, "ContentMaintenance"

    const-string v4, "Evicting LRU items from internal cache..."

    invoke-static {v0, v4}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->m()Lcgs;

    move-result-object v4

    :try_start_0
    invoke-interface {v4}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfx;

    iget-object v6, v1, Lbsr;->a:Lcfz;

    invoke-interface {v6}, Lcfz;->n()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gtz v6, :cond_2

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v4}, Lcgs;->close()V

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    :cond_0
    :goto_1
    invoke-virtual {v1}, Lbsr;->b()V

    invoke-virtual {v1}, Lbsr;->c()V

    const-string v0, "ContentMaintenance"

    const-string v2, "Cache pruning complete."

    invoke-static {v0, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->d()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lbsr;->b:Lcby;

    invoke-interface {v0}, Lcby;->k()J

    move-result-wide v2

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->l()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-lez v0, :cond_1

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->k()Lcgs;

    move-result-object v4

    :try_start_1
    invoke-interface {v4}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfx;

    iget-object v6, v1, Lbsr;->a:Lcfz;

    invoke-interface {v6}, Lcfz;->l()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gtz v6, :cond_5

    invoke-interface {v4}, Lcgs;->close()V

    :cond_1
    :goto_3
    invoke-virtual {v1}, Lbsr;->c()V

    return-void

    :cond_2
    :try_start_2
    iget-object v6, v1, Lbsr;->b:Lcby;

    invoke-interface {v6}, Lcby;->d()Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v1, v0}, Lbsr;->a(Lcfx;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v4}, Lcgs;->close()V

    iget-object v1, v1, Lbsr;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_3
    :try_start_3
    const-string v6, "ContentMaintenance"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Evicting from internal cache: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "ContentMaintenance"

    const-string v7, "%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v0, Lcfx;->g:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcfx;->l()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-interface {v4}, Lcgs;->close()V

    iget-object v0, v1, Lbsr;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    goto/16 :goto_1

    :cond_5
    :try_start_4
    invoke-virtual {v1, v0}, Lbsr;->a(Lcfx;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v4}, Lcgs;->close()V

    throw v0

    :cond_6
    invoke-interface {v4}, Lcgs;->close()V

    goto :goto_3
.end method
