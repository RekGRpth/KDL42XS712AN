.class public final Lfsi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lglz;

.field private static final b:Ljava/util/List;


# instance fields
.field private final c:Lgly;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lglz;

    invoke-direct {v0}, Lglz;-><init>()V

    const-string v1, "genders/value"

    invoke-virtual {v0, v1}, Lglz;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lglz;

    sput-object v0, Lfsi;->a:Lglz;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "disabled"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lfsi;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lbmi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lgly;

    invoke-direct {v0, p1}, Lgly;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsi;->c:Lgly;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lfsi;->c:Lgly;

    const-string v2, "me"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sget-object v4, Lfsi;->b:Ljava/util/List;

    sget-object v5, Lfsi;->a:Lglz;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lgly;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lglz;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "male"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    const-string v1, "female"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "other"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
