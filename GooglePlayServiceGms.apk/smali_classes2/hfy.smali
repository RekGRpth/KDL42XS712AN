.class public final Lhfy;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)Liqs;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->w()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-array v4, v3, [Liqx;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/UriData;

    invoke-static {v0}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Liqx;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Liqs;

    invoke-direct {v0}, Liqs;-><init>()V

    iput-object v4, v0, Liqs;->a:[Liqx;

    :cond_1
    return-object v0
.end method

.method static a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Liqv;
    .locals 4

    new-instance v0, Liqv;

    invoke-direct {v0}, Liqv;-><init>()V

    new-instance v1, Liqc;

    invoke-direct {v1}, Liqc;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/TimeInterval;->b()J

    move-result-wide v2

    iput-wide v2, v1, Liqc;->a:J

    iput-object v1, v0, Liqv;->a:Liqc;

    new-instance v1, Liqc;

    invoke-direct {v1}, Liqc;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/TimeInterval;->c()J

    move-result-wide v2

    iput-wide v2, v1, Liqc;->a:J

    iput-object v1, v0, Liqv;->b:Liqc;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/wallet/wobs/UriData;)Liqx;
    .locals 2

    new-instance v0, Liqx;

    invoke-direct {v0}, Liqx;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liqx;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liqx;->b:Ljava/lang/String;

    :cond_1
    return-object v0
.end method

.method private static b(Lcom/google/android/gms/wallet/wobs/UriData;)Liqg;
    .locals 2

    new-instance v0, Liqg;

    invoke-direct {v0}, Liqg;-><init>()V

    invoke-static {p0}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Liqx;

    move-result-object v1

    iput-object v1, v0, Liqg;->a:Liqx;

    return-object v0
.end method

.method static b(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqt;
    .locals 7

    invoke-static {}, Liqt;->c()[Liqt;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->v()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    new-array v1, v4, [Liqt;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/TextModuleData;

    new-instance v5, Liqt;

    invoke-direct {v5}, Liqt;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Liqt;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Liqt;->b:Ljava/lang/String;

    :cond_1
    aput-object v5, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    :cond_3
    return-object v0
.end method

.method static c(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqo;
    .locals 6

    invoke-static {}, Liqo;->c()[Liqo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->u()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    new-array v1, v4, [Liqo;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    new-instance v0, Liqo;

    invoke-direct {v0}, Liqo;-><init>()V

    aput-object v0, v1, v2

    aget-object v5, v1, v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/UriData;

    invoke-static {v0}, Lhfy;->b(Lcom/google/android/gms/wallet/wobs/UriData;)Liqg;

    move-result-object v0

    iput-object v0, v5, Liqo;->a:Liqg;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :cond_1
    return-object v0
.end method

.method static d(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)Liqp;
    .locals 14

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->s()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->t()Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_0
    new-instance v1, Liqp;

    invoke-direct {v1}, Liqp;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Liqp;->a:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Liqp;->b:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->s()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_a

    new-array v7, v6, [Liqr;

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_9

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;

    new-instance v8, Liqr;

    invoke-direct {v8}, Liqr;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Liqr;->a:Ljava/lang/String;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Liqr;->b:Ljava/lang/String;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->d()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_8

    new-array v11, v10, [Liqq;

    move v2, v3

    :goto_1
    if-ge v2, v10, :cond_7

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/LabelValue;

    new-instance v12, Liqq;

    invoke-direct {v12}, Liqq;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->b()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->b()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Liqq;->a:Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->c()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v12, Liqq;->b:Ljava/lang/String;

    :cond_6
    aput-object v12, v11, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_7
    iput-object v11, v8, Liqr;->c:[Liqq;

    :cond_8
    aput-object v8, v7, v4

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_9
    iput-object v7, v1, Liqp;->c:[Liqr;

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->t()Z

    move-result v0

    iput-boolean v0, v1, Liqp;->d:Z

    move-object v0, v1

    :cond_b
    return-object v0
.end method

.method static e(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Liqe;
    .locals 8

    invoke-static {}, Liqe;->c()[Liqe;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->p()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    new-array v1, v4, [Liqe;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    new-instance v5, Liqe;

    invoke-direct {v5}, Liqe;-><init>()V

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iput-wide v6, v5, Liqe;->a:D

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iput-wide v6, v5, Liqe;->b:D

    aput-object v5, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :cond_1
    return-object v0
.end method

.method static f(Lcom/google/android/gms/wallet/LoyaltyWalletObject;)[Lirb;
    .locals 7

    invoke-static {}, Lirb;->c()[Lirb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->n()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    new-array v1, v4, [Lirb;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;

    new-instance v5, Lirb;

    invoke-direct {v5}, Lirb;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lirb;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lirb;->b:Ljava/lang/String;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->d()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->d()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v6

    invoke-static {v6}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Liqv;

    move-result-object v6

    iput-object v6, v5, Lirb;->c:Liqv;

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->e()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->e()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v6

    invoke-static {v6}, Lhfy;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Liqx;

    move-result-object v6

    iput-object v6, v5, Lirb;->d:Liqx;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->f()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->f()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v0

    invoke-static {v0}, Lhfy;->b(Lcom/google/android/gms/wallet/wobs/UriData;)Liqg;

    move-result-object v0

    iput-object v0, v5, Lirb;->e:Liqg;

    :cond_4
    aput-object v5, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    :cond_6
    return-object v0
.end method
