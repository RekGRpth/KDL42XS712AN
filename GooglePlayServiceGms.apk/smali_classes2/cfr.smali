.class final enum Lcfr;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcfr;

.field public static final enum b:Lcfr;

.field public static final enum c:Lcfr;

.field public static final enum d:Lcfr;

.field public static final enum e:Lcfr;

.field public static final enum f:Lcfr;

.field public static final enum g:Lcfr;

.field public static final enum h:Lcfr;

.field public static final enum i:Lcfr;

.field public static final enum j:Lcfr;

.field private static final p:Ljava/util/Map;

.field private static final synthetic q:[Lcfr;


# instance fields
.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const v11, 0x7f02011e    # com.google.android.gms.R.drawable.ic_drive_type_file_big

    const/4 v2, 0x0

    new-instance v0, Lcfr;

    const-string v1, "COLLECTION"

    const-string v3, "folder"

    const v4, 0x7f02011f    # com.google.android.gms.R.drawable.ic_drive_type_folder_big

    const v5, 0x7f020120    # com.google.android.gms.R.drawable.ic_drive_type_folder_shared_big

    const v6, 0x7f0b0030    # com.google.android.gms.R.string.drive_document_type_folder

    invoke-direct/range {v0 .. v6}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v0, Lcfr;->a:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "DOCUMENT"

    const-string v6, "document"

    const v7, 0x7f02011a    # com.google.android.gms.R.drawable.ic_drive_type_doc_big

    const v8, 0x7f02011a    # com.google.android.gms.R.drawable.ic_drive_type_doc_big

    const v9, 0x7f0b0031    # com.google.android.gms.R.string.drive_document_type_google_document

    move v5, v12

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->b:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "DRAWING"

    const-string v6, "drawing"

    const v7, 0x7f02011b    # com.google.android.gms.R.drawable.ic_drive_type_drawing_big

    const v8, 0x7f02011b    # com.google.android.gms.R.drawable.ic_drive_type_drawing_big

    const v9, 0x7f0b0032    # com.google.android.gms.R.string.drive_document_type_google_drawing

    move v5, v13

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->c:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "FILE"

    const-string v6, "file"

    const v9, 0x7f0b002f    # com.google.android.gms.R.string.drive_document_type_file

    move v5, v14

    move v7, v11

    move v8, v11

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->d:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "FORM"

    const/4 v5, 0x4

    const-string v6, "form"

    const v7, 0x7f020121    # com.google.android.gms.R.drawable.ic_drive_type_form_big

    const v8, 0x7f020121    # com.google.android.gms.R.drawable.ic_drive_type_form_big

    const v9, 0x7f0b0033    # com.google.android.gms.R.string.drive_document_type_google_form

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->e:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "PDF"

    const/4 v5, 0x5

    const-string v6, "pdf"

    const v7, 0x7f020124    # com.google.android.gms.R.drawable.ic_drive_type_pdf_big

    const v8, 0x7f020124    # com.google.android.gms.R.drawable.ic_drive_type_pdf_big

    const v9, 0x7f0b003a    # com.google.android.gms.R.string.drive_document_type_pdf

    const-string v10, "application/pdf"

    invoke-direct/range {v3 .. v10}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcfr;->f:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "PRESENTATION"

    const/4 v5, 0x6

    const-string v6, "presentation"

    const v7, 0x7f020126    # com.google.android.gms.R.drawable.ic_drive_type_presentation_big

    const v8, 0x7f020126    # com.google.android.gms.R.drawable.ic_drive_type_presentation_big

    const v9, 0x7f0b0034    # com.google.android.gms.R.string.drive_document_type_google_presentation

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->g:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "SPREADSHEET"

    const/4 v5, 0x7

    const-string v6, "spreadsheet"

    const v7, 0x7f020127    # com.google.android.gms.R.drawable.ic_drive_type_sheet_big

    const v8, 0x7f020127    # com.google.android.gms.R.drawable.ic_drive_type_sheet_big

    const v9, 0x7f0b0035    # com.google.android.gms.R.string.drive_document_type_google_spreadsheet

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->h:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "TABLE"

    const/16 v5, 0x8

    const-string v6, "table"

    const v7, 0x7f020122    # com.google.android.gms.R.drawable.ic_drive_type_fusion_big

    const v8, 0x7f020122    # com.google.android.gms.R.drawable.ic_drive_type_fusion_big

    const v9, 0x7f0b0036    # com.google.android.gms.R.string.drive_document_type_google_table

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->i:Lcfr;

    new-instance v3, Lcfr;

    const-string v4, "UNKNOWN"

    const/16 v5, 0x9

    const-string v6, "unknown"

    const v7, 0x7f02011d    # com.google.android.gms.R.drawable.ic_drive_type_file

    const v9, 0x7f0b003c    # com.google.android.gms.R.string.drive_document_type_unknown

    move v8, v11

    invoke-direct/range {v3 .. v9}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, Lcfr;->j:Lcfr;

    const/16 v0, 0xa

    new-array v0, v0, [Lcfr;

    sget-object v1, Lcfr;->a:Lcfr;

    aput-object v1, v0, v2

    sget-object v1, Lcfr;->b:Lcfr;

    aput-object v1, v0, v12

    sget-object v1, Lcfr;->c:Lcfr;

    aput-object v1, v0, v13

    sget-object v1, Lcfr;->d:Lcfr;

    aput-object v1, v0, v14

    const/4 v1, 0x4

    sget-object v3, Lcfr;->e:Lcfr;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    sget-object v3, Lcfr;->f:Lcfr;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lcfr;->g:Lcfr;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, Lcfr;->h:Lcfr;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    sget-object v3, Lcfr;->i:Lcfr;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    sget-object v3, Lcfr;->j:Lcfr;

    aput-object v3, v0, v1

    sput-object v0, Lcfr;->q:[Lcfr;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcfr;->values()[Lcfr;

    move-result-object v1

    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v1, v2

    iget-object v5, v4, Lcfr;->k:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcfr;->k:Ljava/lang/String;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcfr;->p:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;III)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcfr;-><init>(Ljava/lang/String;ILjava/lang/String;IIILjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcfr;->k:Ljava/lang/String;

    iput p4, p0, Lcfr;->l:I

    iput p5, p0, Lcfr;->m:I

    iput p6, p0, Lcfr;->n:I

    iput-object p7, p0, Lcfr;->o:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcfr;
    .locals 2

    const-string v0, "application/vnd.google-apps."

    if-nez p0, :cond_1

    sget-object v0, Lcfr;->j:Lcfr;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v0, Lcfr;->d:Lcfr;

    goto :goto_0

    :cond_2
    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcfr;->p:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfr;

    if-nez v0, :cond_0

    sget-object v0, Lcfr;->j:Lcfr;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcfr;
    .locals 1

    const-class v0, Lcfr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcfr;

    return-object v0
.end method

.method public static values()[Lcfr;
    .locals 1

    sget-object v0, Lcfr;->q:[Lcfr;

    invoke-virtual {v0}, [Lcfr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcfr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcfr;->l:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcfr;->m:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcfr;->n:I

    return v0
.end method
