.class public final Ldzj;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Ldvn;)V
    .locals 1

    sget v0, Lxb;->f:I

    invoke-direct {p0, p1, v0}, Ldwx;-><init>(Landroid/content/Context;I)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ldvn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzj;->g:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 11

    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v4, 0x0

    check-cast p4, Lcty;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzk;

    invoke-interface {p4}, Lcty;->f()I

    move-result v5

    invoke-interface {p4}, Lcty;->a()I

    move-result v1

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-ne v1, v2, :cond_0

    if-ne v5, v2, :cond_0

    move v1, v2

    :goto_0
    if-nez v1, :cond_4

    iget-object v1, v0, Ldzk;->h:Ldzj;

    iget-boolean v1, v1, Ldvj;->d:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    :goto_1
    if-nez v5, :cond_2

    iget-object v1, v0, Ldzk;->g:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const/16 v1, 0xff

    :goto_2
    const/16 v2, 0x10

    invoke-static {v2}, Lbpz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageAlpha(I)V

    :goto_3
    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Ldzk;->b:Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    invoke-virtual {v1, v10}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    :goto_4
    invoke-interface {p4}, Lcty;->f()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    iget-object v1, v0, Ldzk;->d:Landroid/database/CharArrayBuffer;

    invoke-interface {p4, v1}, Lcty;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Ldzk;->c:Landroid/widget/TextView;

    iget-object v2, v0, Ldzk;->d:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v3, v0, Ldzk;->d:Landroid/database/CharArrayBuffer;

    iget v3, v3, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/widget/TextView;->setText([CII)V

    iget-object v1, v0, Ldzk;->f:Landroid/database/CharArrayBuffer;

    invoke-interface {p4, v1}, Lcty;->b(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Ldzk;->e:Landroid/widget/TextView;

    iget-object v2, v0, Ldzk;->f:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v3, v0, Ldzk;->f:Landroid/database/CharArrayBuffer;

    iget v3, v3, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/widget/TextView;->setText([CII)V

    :goto_5
    if-nez v5, :cond_7

    sget v1, Lwx;->m:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v1, Lwx;->n:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_6
    iget-object v3, v0, Ldzk;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, v0, Ldzk;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    move v1, v4

    goto :goto_0

    :cond_1
    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown achievement state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p4}, Lcty;->c()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->i:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    goto/16 :goto_1

    :pswitch_1
    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p4}, Lcty;->d()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->h:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    sget v3, Lwz;->g:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, v0, Ldzk;->g:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lwx;->q:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    goto/16 :goto_2

    :cond_3
    iget-object v2, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setAlpha(I)V

    goto/16 :goto_3

    :cond_4
    invoke-interface {p4}, Lcty;->g()I

    move-result v1

    invoke-interface {p4}, Lcty;->e()I

    move-result v3

    if-gtz v3, :cond_8

    const-string v7, "AchievementListAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Inconsistent achievement "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": TYPE_INCREMENTAL, but totalSteps = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_7
    if-lt v1, v2, :cond_5

    const-string v3, "AchievementListAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Inconsistent achievement "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": STATE_REVEALED, but steps = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " / "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    :cond_5
    iget-object v3, v0, Ldzk;->b:Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a(II)V

    iget-object v1, v0, Ldzk;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Ldzk;->b:Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    iget-object v1, v0, Ldzk;->g:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_6
    iget-object v1, v0, Ldzk;->c:Landroid/widget/TextView;

    sget v2, Lxf;->B:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ldzk;->e:Landroid/widget/TextView;

    sget v2, Lxf;->A:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_7
    sget v1, Lwx;->j:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v1, Lwx;->k:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_6

    :cond_8
    move v2, v3

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Landroid/view/View;
    .locals 4

    iget-object v0, p0, Ldzj;->g:Landroid/view/LayoutInflater;

    sget v1, Lxc;->o:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldzk;

    invoke-direct {v1, p0, v0}, Ldzk;-><init>(Ldzj;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
