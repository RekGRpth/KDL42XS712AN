.class public final Lbyz;
.super Lbt;
.source "SourceFile"


# instance fields
.field private final f:Lcfz;

.field private final g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

.field private final h:Ljava/lang/String;

.field private i:Lbzb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfz;Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lbt;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbyz;->f:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    iput-object v0, p0, Lbyz;->g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    iput-object p4, p0, Lbyz;->h:Ljava/lang/String;

    return-void
.end method

.method private a(Lbzb;)V
    .locals 4

    iget-boolean v0, p0, Lcb;->r:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lbyz;->i:Lbzb;

    iput-object p1, p0, Lbyz;->i:Lbzb;

    :try_start_0
    iget-boolean v0, p0, Lcb;->p:Z

    if-eqz v0, :cond_2

    invoke-super {p0, p1}, Lbt;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v2

    if-eq v0, v2, :cond_0

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v3

    if-eq v2, v3, :cond_3

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    invoke-super {p0}, Lbt;->a()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lbzb;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbzb;

    invoke-direct {p0, p1}, Lbyz;->a(Lbzb;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lbyz;->f:Lcfz;

    iget-object v1, p0, Lbyz;->g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    iget-object v2, p0, Lbyz;->g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbyz;->g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcfz;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Lbzs;

    invoke-direct {v1, v0}, Lbzs;-><init>(Landroid/database/Cursor;)V

    return-object v1
.end method

.method protected final f()V
    .locals 0

    invoke-virtual {p0}, Lbyz;->b()Z

    return-void
.end method

.method protected final g()V
    .locals 1

    invoke-super {p0}, Lbt;->g()V

    invoke-virtual {p0}, Lbyz;->b()Z

    iget-object v0, p0, Lbyz;->i:Lbzb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyz;->i:Lbzb;

    invoke-interface {v0}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbyz;->i:Lbzb;

    invoke-interface {v0}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbyz;->i:Lbzb;

    return-void
.end method

.method protected final m_()V
    .locals 1

    iget-object v0, p0, Lbyz;->i:Lbzb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyz;->i:Lbzb;

    invoke-direct {p0, v0}, Lbyz;->a(Lbzb;)V

    :cond_0
    invoke-virtual {p0}, Lbyz;->j()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbyz;->i:Lbzb;

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcb;->a()V

    :cond_2
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocListDataLoader [tagName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbyz;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", docListQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbyz;->g:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
