.class public abstract Laww;
.super Laxx;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Layj;

.field private final e:Layi;

.field private final f:Layj;

.field private final g:Layi;

.field private final h:Layj;

.field private final i:Layi;

.field private final j:Layj;

.field private final k:Layj;

.field private l:D

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const-wide/16 v3, 0xbb8

    const-string v0, "urn:x-cast:com.google.cast.receiver"

    const-string v1, "ReceiverControlChannel"

    invoke-direct {p0, v0, v1}, Laxx;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "receiverDestinationId can\'t be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Laww;->b:Ljava/lang/String;

    new-instance v0, Layj;

    const-wide/16 v1, 0x2710

    invoke-direct {v0, v1, v2}, Layj;-><init>(J)V

    iput-object v0, p0, Laww;->d:Layj;

    new-instance v0, Lawx;

    invoke-direct {v0, p0, v5}, Lawx;-><init>(Laww;B)V

    iput-object v0, p0, Laww;->e:Layi;

    new-instance v0, Layj;

    invoke-direct {v0, v3, v4}, Layj;-><init>(J)V

    iput-object v0, p0, Laww;->f:Layj;

    new-instance v0, Lawz;

    invoke-direct {v0, p0, v5}, Lawz;-><init>(Laww;B)V

    iput-object v0, p0, Laww;->g:Layi;

    new-instance v0, Layj;

    invoke-direct {v0, v3, v4}, Layj;-><init>(J)V

    iput-object v0, p0, Laww;->h:Layj;

    new-instance v0, Lawy;

    invoke-direct {v0, p0, v5}, Lawy;-><init>(Laww;B)V

    iput-object v0, p0, Laww;->i:Layi;

    new-instance v0, Layj;

    invoke-direct {v0, v3, v4}, Layj;-><init>(J)V

    iput-object v0, p0, Laww;->j:Layj;

    new-instance v0, Layj;

    invoke-direct {v0, v3, v4}, Layj;-><init>(J)V

    iput-object v0, p0, Laww;->k:Layj;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    iget-object v0, p0, Laww;->h:Layj;

    invoke-virtual {v0}, Layj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Laww;->f()J

    move-result-wide v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laww;->b:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Laww;->a(Ljava/lang/String;JLjava/lang/String;)V

    iget-object v2, p0, Laww;->h:Layj;

    iget-object v3, p0, Laww;->i:Layi;

    invoke-virtual {v2, v0, v1, v3}, Layj;->a(JLayi;)V

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public final a(DDZ)V
    .locals 5

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v4, p1, v0

    if-gez v4, :cond_1

    move-wide p1, v0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Laww;->f()J

    move-result-wide v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "SET_VOLUME"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "level"

    invoke-virtual {v3, v4, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v4, "volume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "level"

    invoke-virtual {v3, v4, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v4, "muted"

    invoke-virtual {v3, v4, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v4, "expectedVolume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laww;->b:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Laww;->a(Ljava/lang/String;JLjava/lang/String;)V

    iget-object v2, p0, Laww;->j:Layj;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Layj;->a(JLayi;)V

    return-void

    :cond_1
    cmpl-double v0, p1, v2

    if-lez v0, :cond_0

    move-wide p1, v2

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected abstract a(I)V
.end method

.method public final a(J)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0xf

    iget-object v0, p0, Laww;->d:Layj;

    invoke-virtual {v0, p1, p2, v1}, Layj;->b(JI)Z

    iget-object v0, p0, Laww;->f:Layj;

    invoke-virtual {v0, p1, p2, v1}, Layj;->b(JI)Z

    iget-object v0, p0, Laww;->h:Layj;

    invoke-virtual {v0, p1, p2, v1}, Layj;->b(JI)Z

    iget-object v0, p0, Laww;->j:Layj;

    invoke-virtual {v0, p1, p2, v2}, Layj;->b(JI)Z

    iget-object v0, p0, Laww;->k:Layj;

    invoke-virtual {v0, p1, p2, v2}, Layj;->b(JI)Z

    return-void
.end method

.method protected abstract a(Lavw;)V
.end method

.method protected abstract a(Lavw;DZZ)V
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Laww;->f()J

    move-result-wide v1

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "LAUNCH"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "appId"

    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v3, "commandParameters"

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Laww;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Laww;->a(Ljava/lang/String;JLjava/lang/String;)V

    iget-object v0, p0, Laww;->d:Layj;

    iget-object v3, p0, Laww;->e:Layi;

    invoke-virtual {v0, v1, v2, v3}, Layj;->a(JLayi;)V

    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a(ZDZ)V
    .locals 5

    invoke-virtual {p0}, Laww;->f()J

    move-result-wide v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "SET_VOLUME"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "muted"

    invoke-virtual {v3, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v4, "volume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "level"

    invoke-virtual {v3, v4, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v4, "muted"

    invoke-virtual {v3, v4, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v4, "expectedVolume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laww;->b:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Laww;->a(Ljava/lang/String;JLjava/lang/String;)V

    iget-object v2, p0, Laww;->k:Layj;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Layj;->a(JLayi;)V

    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 13

    const/16 v1, 0x7d1

    const/16 v0, 0xd

    const/4 v12, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v2, p0, Laww;->a:Laye;

    const-string v3, "Received: %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "requestId"

    const-wide/16 v8, -0x1

    invoke-virtual {v2, v4, v8, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v4, "RECEIVER_STATUS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x0

    const-string v1, "status"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v1, "applications"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "applications"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    :try_start_1
    new-instance v1, Lavw;

    invoke-direct {v1, v3}, Lavw;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    :try_start_2
    iget-object v0, p0, Laww;->d:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v8, v9, v3}, Layj;->a(JI)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    iget-object v0, p0, Laww;->a:Laye;

    const-string v2, "application launch has completed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Laww;->a(Lavw;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    iget-object v1, p0, Laww;->a:Laye;

    const-string v3, "Error extracting the application info."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Laww;->h:Layj;

    const/4 v3, 0x0

    invoke-virtual {v0, v8, v9, v3}, Layj;->a(JI)Z

    move-result v5

    iget-object v0, p0, Laww;->c:Ljava/lang/String;

    if-eqz v0, :cond_a

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lavw;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Laww;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_3
    move v0, v7

    :goto_2
    iget-object v3, p0, Laww;->f:Layj;

    const/4 v4, 0x0

    invoke-virtual {v3, v8, v9, v4}, Layj;->a(JI)Z

    move-result v3

    if-eqz v0, :cond_4

    iget-object v4, p0, Laww;->a:Laye;

    const-string v10, "application has stopped"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v4, v10, v11}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    if-nez v0, :cond_5

    if-eqz v3, :cond_6

    :cond_5
    invoke-virtual {p0}, Laww;->d()V

    :cond_6
    sget-object v3, Layj;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    iget-object v0, p0, Laww;->j:Layj;

    const/4 v4, 0x0

    invoke-virtual {v0, v8, v9, v4}, Layj;->a(JI)Z

    iget-object v0, p0, Laww;->k:Layj;

    const/4 v4, 0x0

    invoke-virtual {v0, v8, v9, v4}, Layj;->a(JI)Z

    iget-object v0, p0, Laww;->j:Layj;

    invoke-virtual {v0}, Layj;->b()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Laww;->k:Layj;

    invoke-virtual {v0}, Layj;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_7
    move v0, v7

    :goto_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v3, p0, Laww;->a:Laye;

    const-string v4, "requestId = %d, ignoreVolume = %b"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v10, v11

    const/4 v8, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v10, v8

    invoke-virtual {v3, v4, v10}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v3, p0, Laww;->n:Z

    if-nez v3, :cond_8

    iget-object v0, p0, Laww;->a:Laye;

    const-string v3, "first status received, so not ignoring volume change"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Laww;->n:Z

    move v0, v6

    :cond_8
    const-string v3, "volume"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    if-nez v0, :cond_9

    const-string v0, "volume"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "level"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Laww;->l:D

    const-string v2, "muted"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Laww;->m:Z

    :cond_9
    iget-wide v2, p0, Laww;->l:D

    iget-boolean v4, p0, Laww;->m:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Laww;->a(Lavw;DZZ)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Laww;->a:Laye;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object p1, v3, v7

    invoke-virtual {v1, v2, v3}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_a
    move v0, v6

    goto/16 :goto_2

    :cond_b
    move v0, v6

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v3

    throw v0

    :cond_c
    const-string v4, "LAUNCH_ERROR"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v3, "reason"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "BAD_PARAMETER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    :goto_4
    iget-object v0, p0, Laww;->d:Layj;

    invoke-virtual {v0, v8, v9, v1}, Layj;->a(JI)Z

    goto/16 :goto_1

    :cond_d
    const-string v1, "CANCELLED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0x7d2

    goto :goto_4

    :cond_e
    const-string v1, "NOT_ALLOWED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x7d3

    goto :goto_4

    :cond_f
    const-string v1, "NOT_FOUND"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v1, 0x7d4

    goto :goto_4

    :cond_10
    const-string v1, "CAST_INIT_TIMEOUT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const/16 v1, 0xf

    goto :goto_4

    :cond_11
    move v1, v0

    goto :goto_4

    :cond_12
    const-string v4, "INVALID_REQUEST"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "reason"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "INVALID_COMMAND"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "DUPLICATE_REQUEST_ID"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_13
    move v0, v1

    :cond_14
    iget-object v1, p0, Laww;->d:Layj;

    invoke-virtual {v1, v8, v9, v0}, Layj;->a(JI)Z

    iget-object v1, p0, Laww;->f:Layj;

    invoke-virtual {v1, v8, v9, v0}, Layj;->a(JI)Z

    iget-object v1, p0, Laww;->h:Layj;

    invoke-virtual {v1, v8, v9, v0}, Layj;->a(JI)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Laww;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract b(I)V
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Laww;->f()J

    move-result-wide v1

    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "type"

    const-string v4, "STOP"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p1, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "sessionId"

    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Laww;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Laww;->a(Ljava/lang/String;JLjava/lang/String;)V

    iget-object v0, p0, Laww;->f:Layj;

    iget-object v3, p0, Laww;->g:Layi;

    invoke-virtual {v0, v1, v2, v3}, Layj;->a(JLayi;)V

    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Laww;->c:Ljava/lang/String;

    iget-object v0, p0, Laww;->d:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Laww;->f:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Laww;->h:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Laww;->j:Layj;

    invoke-virtual {v0}, Layj;->a()V

    iget-object v0, p0, Laww;->k:Layj;

    invoke-virtual {v0}, Layj;->a()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laww;->l:D

    iput-boolean v2, p0, Laww;->m:Z

    iput-boolean v2, p0, Laww;->n:Z

    return-void
.end method

.method protected abstract c(I)V
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Laww;->a:Laye;

    const-string v1, "current transport id (in control channel) is now: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Laww;->c:Ljava/lang/String;

    return-void
.end method

.method protected abstract d()V
.end method
