.class public final Lilw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field private d:D


# direct methods
.method public constructor <init>([D)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v3, -0x4010000000000000L    # -1.0

    iput-wide v3, p0, Lilw;->d:D

    array-length v0, p1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lilv;->a(Z)V

    aget-wide v2, p1, v2

    iput-wide v2, p0, Lilw;->a:D

    aget-wide v0, p1, v1

    iput-wide v0, p0, Lilw;->b:D

    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lilw;->c:D

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v0, p0, Lilw;->d:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lilw;->a:D

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget-wide v2, p0, Lilw;->b:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    iget-wide v2, p0, Lilw;->c:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lilw;->d:D

    :cond_0
    iget-wide v0, p0, Lilw;->d:D

    return-wide v0
.end method
