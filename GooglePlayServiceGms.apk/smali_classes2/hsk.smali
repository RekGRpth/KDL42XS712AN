.class public Lhsk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[S

.field public static final b:[S


# instance fields
.field final c:Landroid/os/PowerManager$WakeLock;

.field final d:[S

.field private final e:Ljava/lang/String;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x6

    new-array v0, v1, [S

    fill-array-data v0, :array_0

    sput-object v0, Lhsk;->a:[S

    new-array v0, v1, [S

    fill-array-data v0, :array_1

    sput-object v0, Lhsk;->b:[S

    return-void

    :array_0
    .array-data 2
        0xfas
        0x1f4s
        0x3e8s
        0x7d0s
        0xfa0s
        0x1f40s
    .end array-data

    :array_1
    .array-data 2
        0x19s
        0x32s
        0x64s
        0xc8s
        0x190s
        0x320s
    .end array-data
.end method

.method public constructor <init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhsk;->f:J

    invoke-virtual {p4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "collector"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "NlpCollectorWakeLock"

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    if-nez p3, :cond_0

    iget-object v0, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    :cond_0
    iput-object p4, p0, Lhsk;->e:Ljava/lang/String;

    iput-object p5, p0, Lhsk;->d:[S

    return-void

    :cond_1
    const-string v0, "NlpWakeLock"

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/PowerManager;Ljava/lang/String;[S)V
    .locals 6

    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhsk;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)J
    .locals 4

    const-wide/16 v0, -0x1

    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lhsk;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Lhsk;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v0, p1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-wide v0, p0, Lhsk;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lhsk;->f:J

    invoke-static {}, Lhsp;->a()Lhsp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhsp;->a(Lhsk;)V

    :cond_0
    invoke-virtual {p0}, Lhsk;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lilx;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {p1, v0}, Lilx;->a(Landroid/os/PowerManager$WakeLock;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 6

    const-wide/16 v4, -0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lhsk;->f()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    invoke-virtual {p0}, Lhsk;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lhsk;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lhsk;->a(J)J

    move-result-wide v0

    invoke-static {}, Lhsp;->a()Lhsp;

    move-result-object v2

    invoke-virtual {v2, p0, v0, v1}, Lhsp;->a(Lhsk;J)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhsk;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhsk;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhsk;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 0

    return-void
.end method
