.class public final Lbyv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Z

.field final c:Z

.field final d:Lccx;

.field final e:Lcag;

.field final f:Lccm;

.field final g:I

.field final h:J

.field i:Landroid/widget/SectionIndexer;

.field final j:Lbzr;

.field k:Lbzb;

.field private final l:Ljava/lang/String;

.field private final m:Lbzi;

.field private final n:Lbzq;


# direct methods
.method public constructor <init>(Lbzb;Lcag;Ljava/lang/String;Lbzk;Lccx;Lccm;Lbzr;Lbzi;Lbzq;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lbyv;->l:Ljava/lang/String;

    sget-object v0, Lbzk;->d:Lbzk;

    invoke-virtual {p4, v0}, Lbzk;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lbyv;->a:Z

    sget-object v0, Lbzk;->e:Lbzk;

    invoke-virtual {p4, v0}, Lbzk;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lbyv;->b:Z

    sget-object v0, Lbzk;->c:Lbzk;

    invoke-virtual {p4, v0}, Lbzk;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lbyv;->c:Z

    iput-object p5, p0, Lbyv;->d:Lccx;

    iput-object p2, p0, Lbyv;->e:Lcag;

    iput-object p6, p0, Lbyv;->f:Lccm;

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzr;

    iput-object v0, p0, Lbyv;->j:Lbzr;

    iget-object v0, p0, Lbyv;->e:Lcag;

    invoke-virtual {v0}, Lcag;->c()Lcdp;

    move-result-object v0

    sget-object v1, Lceg;->g:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0b0029    # com.google.android.gms.R.string.drive_doclist_date_edited_label

    :goto_0
    iput v0, p0, Lbyv;->g:I

    iput-object p8, p0, Lbyv;->m:Lbzi;

    invoke-static {p9}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzq;

    iput-object v0, p0, Lbyv;->n:Lbzq;

    invoke-virtual {p2}, Lcag;->b()Landroid/widget/SectionIndexer;

    move-result-object v0

    iput-object v0, p0, Lbyv;->i:Landroid/widget/SectionIndexer;

    iput-wide p10, p0, Lbyv;->h:J

    iput-object p1, p0, Lbyv;->k:Lbzb;

    return-void

    :cond_0
    sget-object v1, Lceg;->d:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0b002c    # com.google.android.gms.R.string.drive_doclist_date_opened_label

    goto :goto_0

    :cond_1
    sget-object v1, Lceg;->e:Lceg;

    invoke-virtual {v1}, Lceg;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0b002d    # com.google.android.gms.R.string.drive_doclist_date_shared_label

    goto :goto_0

    :cond_2
    const v0, 0x7f0b002a    # com.google.android.gms.R.string.drive_doclist_date_modified_label

    goto :goto_0
.end method

.method static a(Landroid/view/View;I)V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/database/Cursor;Lbyx;Lcom/google/android/gms/drive/DriveId;)V
    .locals 3

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget-object v1, p2, Lbyx;->l:Ljava/util/List;

    new-instance v2, Lbyw;

    invoke-direct {v2, p0, v0, p3}, Lbyw;-><init>(Lbyv;ILcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method final a()Z
    .locals 2

    iget-object v0, p0, Lbyv;->k:Lbzb;

    invoke-interface {v0}, Lbzb;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbyv;->n:Lbzq;

    invoke-interface {v1, v0}, Lbzq;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
