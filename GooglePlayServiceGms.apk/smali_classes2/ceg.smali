.class public final enum Lceg;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum A:Lceg;

.field public static final enum B:Lceg;

.field public static final enum C:Lceg;

.field public static final enum D:Lceg;

.field public static final enum E:Lceg;

.field public static final enum F:Lceg;

.field public static final enum G:Lceg;

.field public static final enum H:Lceg;

.field public static final enum I:Lceg;

.field public static final enum J:Lceg;

.field public static final enum K:Lceg;

.field public static final enum L:Lceg;

.field public static final enum M:Lceg;

.field public static final enum N:Lceg;

.field public static final enum O:Lceg;

.field public static final enum P:Lceg;

.field private static final synthetic R:[Lceg;

.field public static final enum a:Lceg;

.field public static final enum b:Lceg;

.field public static final enum c:Lceg;

.field public static final enum d:Lceg;

.field public static final enum e:Lceg;

.field public static final enum f:Lceg;

.field public static final enum g:Lceg;

.field public static final enum h:Lceg;

.field public static final enum i:Lceg;

.field public static final enum j:Lceg;

.field public static final enum k:Lceg;

.field public static final enum l:Lceg;

.field public static final enum m:Lceg;

.field public static final enum n:Lceg;

.field public static final enum o:Lceg;

.field public static final enum p:Lceg;

.field public static final enum q:Lceg;

.field public static final enum r:Lceg;

.field public static final enum s:Lceg;

.field public static final enum t:Lceg;

.field public static final enum u:Lceg;

.field public static final enum v:Lceg;

.field public static final enum w:Lceg;

.field public static final enum x:Lceg;

.field public static final enum y:Lceg;

.field public static final enum z:Lceg;


# instance fields
.field private final Q:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v13, 0x4

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x1

    new-instance v0, Lceg;

    const-string v1, "TITLE"

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "title"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v2, v9, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->a:Lceg;

    new-instance v0, Lceg;

    const-string v1, "CREATION_TIME"

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "creationTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v2, v9, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->b:Lceg;

    new-instance v0, Lceg;

    const-string v1, "LAST_MODIFIED_TIME"

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "lastModifiedTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v2, v9, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->c:Lceg;

    new-instance v0, Lceg;

    const-string v1, "LAST_OPENED_TIME"

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "lastOpenedTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v9, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->d:Lceg;

    new-instance v0, Lceg;

    const-string v1, "SHARED_WITH_ME_TIME"

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "sharedWithMeTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v9, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v13, v2}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->e:Lceg;

    new-instance v0, Lceg;

    const-string v1, "SHARED"

    const/4 v2, 0x5

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "shared"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->f:Lceg;

    new-instance v0, Lceg;

    const-string v1, "MODIFIED_BY_ME_TIME"

    const/4 v2, 0x6

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "modifiedByMeTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->g:Lceg;

    new-instance v0, Lceg;

    const-string v1, "LOCAL_INSERT_TIME"

    const/4 v2, 0x7

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "localInsertTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->h:Lceg;

    new-instance v0, Lceg;

    const-string v1, "METADATA_ETAG"

    const/16 v2, 0x8

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "metadataEtag"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->i:Lceg;

    new-instance v0, Lceg;

    const-string v1, "RESOURCE_ID"

    const/16 v2, 0x9

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "resourceId"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->j:Lceg;

    new-instance v0, Lceg;

    const-string v1, "LOCALITY"

    const/16 v2, 0xa

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "locality"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->k:Lceg;

    new-instance v0, Lceg;

    const-string v1, "MIME_TYPE"

    const/16 v2, 0xb

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "mimeType"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->l:Lceg;

    new-instance v0, Lceg;

    const-string v1, "CAN_EDIT"

    const/16 v2, 0xc

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "canEdit"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->m:Lceg;

    new-instance v0, Lceg;

    const-string v1, "STARRED"

    const/16 v2, 0xd

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "starred"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->n:Lceg;

    new-instance v0, Lceg;

    const-string v1, "TRASHED"

    const/16 v2, 0xe

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "trashed"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->o:Lceg;

    new-instance v0, Lceg;

    const-string v1, "PINNED"

    const/16 v2, 0xf

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "pinned"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->p:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_FROM_CHANGE_LOG_FEED"

    const/16 v2, 0x10

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "changeFeed"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->q:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_PLACE_HOLDER"

    const/16 v2, 0x11

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "placeHolder"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->r:Lceg;

    new-instance v0, Lceg;

    const-string v1, "ACCOUNT_ID"

    const/16 v2, 0x12

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "accountId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v4

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/String;

    sget-object v6, Lceg;->j:Lceg;

    iget-object v6, v6, Lceg;->Q:Lcdp;

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-virtual {v4, v5}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->s:Lceg;

    new-instance v0, Lceg;

    const-string v1, "SEQUENCE_NUMBER"

    const/16 v2, 0x13

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "sequenceNumber"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->t:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_DOWNLOADABLE"

    const/16 v2, 0x14

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isDownloadable"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v4, Lcei;->g:Z

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->u:Lceg;

    new-instance v0, Lceg;

    const-string v1, "CONTENT_ID"

    const/16 v2, 0x15

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "contentId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Lcej;->b:Lcej;

    invoke-virtual {v4, v5, v6, v7}, Lcei;->a(Lcdt;Lcdp;Lcej;)Lcei;

    move-result-object v4

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->v:Lceg;

    new-instance v0, Lceg;

    const-string v1, "MD5_CHECKSUM"

    const/16 v2, 0x16

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "md5Checksum"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v9, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->w:Lceg;

    new-instance v0, Lceg;

    const-string v1, "SERVER_CONTENT_ETAG"

    const/16 v2, 0x17

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "contentEtag"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v13, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->x:Lceg;

    new-instance v0, Lceg;

    const-string v1, "CONTENT_ETAG_AT_AT_UPLOAD_OR_DOWNLOAD"

    const/16 v2, 0x18

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "contentEtagAtLastUploadOrDownload"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v13, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->y:Lceg;

    new-instance v0, Lceg;

    const-string v1, "LOCAL_CONTENT_HASH"

    const/16 v2, 0x19

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "localContentHash"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v5

    sget-object v6, Lcem;->a:Lcem;

    invoke-virtual {v6}, Lcem;->a()Lcdp;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v4

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-virtual {v3, v12, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcdq;->a(I)Lcdq;

    move-result-object v3

    const/16 v4, 0xc

    new-instance v5, Lcei;

    const-string v6, "localContentHash"

    sget-object v7, Lcek;->c:Lcek;

    invoke-direct {v5, v6, v7}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v6

    sget-object v7, Lcem;->a:Lcem;

    invoke-virtual {v7}, Lcem;->a()Lcdp;

    move-result-object v7

    sget-object v8, Lcej;->b:Lcej;

    invoke-virtual {v5, v6, v7, v8}, Lcei;->a(Lcdt;Lcdp;Lcej;)Lcei;

    move-result-object v5

    invoke-virtual {v5}, Lcei;->a()Lcei;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->z:Lceg;

    new-instance v0, Lceg;

    const-string v1, "APPDATA_OWNER_PACKAGING_ID"

    const/16 v2, 0x1a

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "getOwnerPackagingId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v12, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->A:Lceg;

    new-instance v0, Lceg;

    const-string v1, "ALTERNATE_LINK"

    const/16 v2, 0x1b

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "alternateLink"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v12, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->B:Lceg;

    new-instance v0, Lceg;

    const-string v1, "OWNER_NAMES"

    const/16 v2, 0x1c

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "ownerNames"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v12, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->C:Lceg;

    new-instance v0, Lceg;

    const-string v1, "DESCRIPTION"

    const/16 v2, 0x1d

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "description"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->D:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_COPYABLE"

    const/16 v2, 0x1e

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isCopyable"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->E:Lceg;

    new-instance v0, Lceg;

    const-string v1, "EMBED_LINK"

    const/16 v2, 0x1f

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "embedLink"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->F:Lceg;

    new-instance v0, Lceg;

    const-string v1, "FILE_EXTENSION"

    const/16 v2, 0x20

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "fileExtension"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->G:Lceg;

    new-instance v0, Lceg;

    const-string v1, "FILE_SIZE"

    const/16 v2, 0x21

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "fileSize"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->H:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_VIEWED"

    const/16 v2, 0x22

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isViewed"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->I:Lceg;

    new-instance v0, Lceg;

    const-string v1, "IS_RESTRICTED"

    const/16 v2, 0x23

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isRestricted"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->J:Lceg;

    new-instance v0, Lceg;

    const-string v1, "ORIGINAL_FILENAME"

    const/16 v2, 0x24

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "originalFilename"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->K:Lceg;

    new-instance v0, Lceg;

    const-string v1, "QUOTA_BYTES_USED"

    const/16 v2, 0x25

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "quotaBytesUsed"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->L:Lceg;

    new-instance v0, Lceg;

    const-string v1, "WEB_CONTENT_LINK"

    const/16 v2, 0x26

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "webContentLink"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->M:Lceg;

    new-instance v0, Lceg;

    const-string v1, "WEB_VIEW_LINK"

    const/16 v2, 0x27

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "webViewLink"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->N:Lceg;

    new-instance v0, Lceg;

    const-string v1, "INDEXABLE_TEXT"

    const/16 v2, 0x28

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "indexableText"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v10, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->O:Lceg;

    new-instance v0, Lceg;

    const-string v1, "HAS_THUMBNAIL"

    const/16 v2, 0x29

    invoke-static {}, Lcef;->d()Lcef;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    const/4 v4, 0x7

    new-instance v5, Lcei;

    const-string v6, "hasThumbnail"

    sget-object v7, Lcek;->a:Lcek;

    invoke-direct {v5, v6, v7}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v4, v5}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lceg;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceg;->P:Lceg;

    const/16 v0, 0x2a

    new-array v0, v0, [Lceg;

    sget-object v1, Lceg;->a:Lceg;

    aput-object v1, v0, v11

    sget-object v1, Lceg;->b:Lceg;

    aput-object v1, v0, v9

    sget-object v1, Lceg;->c:Lceg;

    aput-object v1, v0, v12

    sget-object v1, Lceg;->d:Lceg;

    aput-object v1, v0, v10

    sget-object v1, Lceg;->e:Lceg;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, Lceg;->f:Lceg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lceg;->g:Lceg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lceg;->h:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lceg;->i:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lceg;->j:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lceg;->k:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lceg;->l:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lceg;->m:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lceg;->n:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lceg;->o:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lceg;->p:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lceg;->q:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lceg;->r:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lceg;->s:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lceg;->t:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lceg;->u:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lceg;->v:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lceg;->w:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lceg;->x:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lceg;->y:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lceg;->z:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lceg;->A:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lceg;->B:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lceg;->C:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lceg;->D:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lceg;->E:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lceg;->F:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lceg;->G:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lceg;->H:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lceg;->I:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lceg;->J:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lceg;->K:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lceg;->L:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lceg;->M:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lceg;->N:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lceg;->O:Lceg;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lceg;->P:Lceg;

    aput-object v2, v0, v1

    sput-object v0, Lceg;->R:[Lceg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lceg;->Q:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lceg;
    .locals 1

    const-class v0, Lceg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lceg;

    return-object v0
.end method

.method public static values()[Lceg;
    .locals 1

    sget-object v0, Lceg;->R:[Lceg;

    invoke-virtual {v0}, [Lceg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lceg;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lceg;->Q:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lceg;->Q:Lcdp;

    return-object v0
.end method
