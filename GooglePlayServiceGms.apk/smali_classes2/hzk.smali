.class public final Lhzk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field final b:Lhyt;

.field final c:Ljava/lang/Object;

.field final d:Lhzl;

.field final e:Lhzm;

.field f:I

.field g:Ljava/util/Collection;

.field h:J

.field i:Z

.field private final j:Lbpe;

.field private final k:Landroid/content/Context;

.field private final l:Lhzr;

.field private final m:Landroid/app/AlarmManager;

.field private final n:Lhvb;

.field private final o:I

.field private final p:Ljava/lang/String;

.field private q:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Landroid/content/Context;Lbpe;Lhyt;)V
    .locals 6

    new-instance v4, Lhzr;

    invoke-direct {v4}, Lhzr;-><init>()V

    invoke-static {p1}, Lhvb;->a(Landroid/content/Context;)Lhvb;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lhzk;-><init>(Landroid/content/Context;Lbpe;Lhyt;Lhzr;Lhvb;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lbpe;Lhyt;Lhzr;Lhvb;)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lhzk;->a:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhzk;->c:Ljava/lang/Object;

    new-instance v0, Lhzl;

    invoke-direct {v0, p0}, Lhzl;-><init>(Lhzk;)V

    iput-object v0, p0, Lhzk;->d:Lhzl;

    new-instance v0, Lhzm;

    invoke-direct {v0, p0}, Lhzm;-><init>(Lhzk;)V

    iput-object v0, p0, Lhzk;->e:Lhzm;

    const/4 v0, -0x1

    iput v0, p0, Lhzk;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lhzk;->g:Ljava/util/Collection;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhzk;->h:J

    iput-boolean v2, p0, Lhzk;->i:Z

    iput-object p1, p0, Lhzk;->k:Landroid/content/Context;

    iput-object p2, p0, Lhzk;->j:Lbpe;

    iput-object p3, p0, Lhzk;->b:Lhyt;

    iput-object p4, p0, Lhzk;->l:Lhzr;

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lhzk;->m:Landroid/app/AlarmManager;

    iput-object p5, p0, Lhzk;->n:Lhvb;

    iget-object v0, p0, Lhzk;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lhzk;->e:Lhzm;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.location.intent.action.END_LOCATION_BURST"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lhzk;->o:I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzk;->p:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    const-string v0, "LocationDetector"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LocationDetector"

    const-string v1, "cancelLocationUpdate"

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhzk;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lhzk;->f:I

    iget-boolean v0, p0, Lhzk;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhzk;->a(Z)V

    :cond_1
    iget-object v0, p0, Lhzk;->n:Lhvb;

    iget-object v2, p0, Lhzk;->d:Lhzl;

    invoke-virtual {v0, v2}, Lhvb;->a(Leqc;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(IZLjava/util/Collection;)V
    .locals 9

    if-lez p1, :cond_6

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Location update interval should be positive: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    if-nez p3, :cond_f

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationDetector"

    const-string v1, "Blaming ourself for location updates."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lild;

    iget v1, p0, Lhzk;->o:I

    iget-object v2, p0, Lhzk;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lild;-><init>(ILjava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    :goto_1
    iget-object v6, p0, Lhzk;->c:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lhzk;->j:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v1

    const/16 v0, 0x3c

    if-lt p1, v0, :cond_7

    move v0, p1

    :goto_2
    if-nez p2, :cond_1

    iget v3, p0, Lhzk;->f:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    iget v3, p0, Lhzk;->f:I

    sub-int v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x4

    if-gt v3, v4, :cond_1

    iget-object v3, p0, Lhzk;->g:Ljava/util/Collection;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhzk;->g:Ljava/util/Collection;

    invoke-virtual {v3, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    :cond_1
    const-string v3, "LocationDetector"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "LocationDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "requestLocation: intervalSec="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", trigger="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", acceptedIntervalSec="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " clients="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput v0, p0, Lhzk;->f:I

    const/16 v3, 0x3c

    if-ge v0, v3, :cond_d

    iget-boolean v3, p0, Lhzk;->i:Z

    if-nez v3, :cond_d

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "LocationDetector"

    const-string v3, "Starting location bursts."

    invoke-static {v0, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lhzk;->q:Landroid/app/PendingIntent;

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-boolean v0, p0, Lhzk;->i:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Lbiq;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhzk;->i:Z

    iput-wide v1, p0, Lhzk;->h:J

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.location.intent.action.END_LOCATION_BURST"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhzk;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lhzk;->l:Lhzr;

    iget-object v3, p0, Lhzk;->k:Landroid/content/Context;

    const/4 v4, 0x0

    const/high16 v7, 0x10000000

    invoke-static {v3, v4, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lhzk;->q:Landroid/app/PendingIntent;

    iget-object v0, p0, Lhzk;->m:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    const-wide/32 v7, 0x2bf20

    add-long/2addr v1, v7

    iget-object v4, p0, Lhzk;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :cond_4
    :goto_5
    iput-object v5, p0, Lhzk;->g:Ljava/util/Collection;

    iget v0, p0, Lhzk;->f:I

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    int-to-long v2, v0

    const-wide/16 v7, 0x3e8

    mul-long/2addr v2, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    const/16 v0, 0x66

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, p0, Lhzk;->n:Lhvb;

    iget-object v2, p0, Lhzk;->d:Lhzl;

    const/4 v4, 0x1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZZLjava/util/Collection;)V

    :cond_5
    :goto_6
    monitor-exit v6

    return-void

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    iget-wide v3, p0, Lhzk;->h:J

    const-wide/16 v7, 0x0

    cmp-long v0, v3, v7

    if-ltz v0, :cond_8

    iget-wide v3, p0, Lhzk;->h:J

    sub-long v3, v1, v3

    const-wide/32 v7, 0x116520

    cmp-long v0, v3, v7

    if-gez v0, :cond_8

    iget-boolean v0, p0, Lhzk;->i:Z

    if-eqz v0, :cond_9

    :cond_8
    const/16 v0, 0x1e

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_2

    :cond_9
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_a

    const-string v0, "LocationDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Rejected location bursts. intervalSec="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mLastBurstStartTimeMillis="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v7, p0, Lhzk;->h:J

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", now="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const/16 v0, 0x3c

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_d
    const/16 v1, 0x3c

    if-lt v0, v1, :cond_4

    iget-boolean v0, p0, Lhzk;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhzk;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_e
    :try_start_1
    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_5

    const-string v1, "LocationDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring requestLocation: intervalSec="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", trigger="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", acceptedIntervalSec="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", clients="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_6

    :cond_f
    move-object v5, p3

    goto/16 :goto_1
.end method

.method final a(Z)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lhzk;->q:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-boolean v0, p0, Lhzk;->i:Z

    invoke-static {v0}, Lbiq;->a(Z)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationDetector"

    const-string v2, "Ending location bursts."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-boolean v1, p0, Lhzk;->i:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lhzk;->m:Landroid/app/AlarmManager;

    iget-object v1, p0, Lhzk;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lhzk;->q:Landroid/app/PendingIntent;

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
