.class public final Ligx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final d:Lhuf;

.field private static final e:Lhuf;


# instance fields
.field public final a:Lcom/google/android/gms/maps/model/LatLng;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/android/gms/location/places/PlaceFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lihj;->aj:Livk;

    invoke-static {v0}, Ligv;->a(Livk;)Lhuf;

    move-result-object v0

    sput-object v0, Ligx;->e:Lhuf;

    new-instance v0, Ligy;

    invoke-direct {v0}, Ligy;-><init>()V

    sput-object v0, Ligx;->d:Lhuf;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iput-object p2, p0, Ligx;->b:Ljava/lang/String;

    iput-object p3, p0, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    return-void
.end method

.method static synthetic a()Lhuf;
    .locals 1

    sget-object v0, Ligx;->e:Lhuf;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ligx;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ligx;

    iget-object v2, p0, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, p1, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ligx;->b:Ljava/lang/String;

    iget-object v3, p1, Ligx;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    iget-object v3, p1, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Ligx;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
