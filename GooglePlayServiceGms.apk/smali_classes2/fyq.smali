.class public final Lfyq;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private final c:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/Object;)V
    .locals 1

    const v0, 0x7f0400ed    # com.google.android.gms.R.layout.plus_oob_gender_spinner_item

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    check-cast p2, [Ljava/lang/CharSequence;

    iput-object p2, p0, Lfyq;->c:[Ljava/lang/CharSequence;

    const/4 v0, 0x2

    iput v0, p0, Lfyq;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lfyq;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lfyq;->a:I

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lfyq;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lfyq;->a:I

    return-void
.end method

.method public final b()Z
    .locals 1

    iget v0, p0, Lfyq;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    iget v2, p0, Lfyq;->a:I

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lfyq;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v1

    :pswitch_0
    iget-object v2, p0, Lfyq;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lfyq;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lfyq;->notifyDataSetChanged()V

    const/4 v0, 0x2

    iput v0, p0, Lfyq;->a:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
