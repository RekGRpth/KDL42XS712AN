.class public final Lecs;
.super Leco;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/gms/games/PlayerEntity;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ldax;Lcom/google/android/gms/games/PlayerEntity;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Leco;-><init>(Landroid/content/Context;Ldax;)V

    iput-object p3, p0, Lecs;->f:Lcom/google/android/gms/games/PlayerEntity;

    return-void
.end method

.method public static a(Landroid/content/Context;Ldax;Lcom/google/android/gms/games/PlayerEntity;)V
    .locals 4

    new-instance v0, Lecs;

    invoke-direct {v0, p0, p1, p2}, Lecs;-><init>(Landroid/content/Context;Ldax;Lcom/google/android/gms/games/PlayerEntity;)V

    sget-object v1, Lecs;->a:Lecq;

    sget-object v2, Lecs;->a:Lecq;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lecq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lecq;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    iget-object v0, p0, Lecs;->e:Landroid/view/View;

    const v1, 0x7f0a0155    # com.google.android.gms.R.id.popup_text_data

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lecs;->f:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerEntity;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lecs;->e:Landroid/view/View;

    const v1, 0x7f0a0147    # com.google.android.gms.R.id.popup_text_label

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b025d    # com.google.android.gms.R.string.games_signin_welcome_back

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lecs;->e:Landroid/view/View;

    const v1, 0x7f0a0145    # com.google.android.gms.R.id.popup_icon

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lecs;->f:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerEntity;->c()Landroid/net/Uri;

    move-result-object v1

    const v2, 0x7f0200d6    # com.google.android.gms.R.drawable.games_default_profile_img

    invoke-virtual {p0, v0, v1, v2}, Lecs;->a(Landroid/widget/ImageView;Landroid/net/Uri;I)V

    return-void
.end method
