.class public abstract Laom;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laor;


# instance fields
.field protected final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Laom;->a:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laoh;->b:Ljava/lang/String;

    const v3, 0x7f0b0589    # com.google.android.gms.R.string.auth_authzen_default_request_description

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laoh;->c:Ljava/lang/String;

    const v3, 0x7f0b058e    # com.google.android.gms.R.string.auth_authzen_yes_button_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laoh;->d:Ljava/lang/String;

    const v3, 0x7f0b058f    # com.google.android.gms.R.string.auth_authzen_no_button_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laog;->b:Ljava/lang/String;

    const v3, 0x7f0b0590    # com.google.android.gms.R.string.auth_authzen_confirmation_allow_button_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laog;->c:Ljava/lang/String;

    const v3, 0x7f0b0591    # com.google.android.gms.R.string.auth_authzen_confirmation_cancel_button_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laof;->b:Ljava/lang/String;

    const v3, 0x7f0b058a    # com.google.android.gms.R.string.auth_authzen_pin_validation_default_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laoe;->b:Ljava/lang/String;

    const v3, 0x7f0b058b    # com.google.android.gms.R.string.auth_authzen_notify_google_default_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    sget-object v2, Laoe;->c:Ljava/lang/String;

    const v3, 0x7f0b058d    # com.google.android.gms.R.string.auth_authzen_ok_button_text

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Laom;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Laom;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    return-object v0
.end method
