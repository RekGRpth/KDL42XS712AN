.class public final Lbsw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbsu;


# instance fields
.field private final a:Lbsp;

.field private final b:Lcfz;

.field private final c:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcfz;Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsp;

    iput-object v0, p0, Lbsw;->a:Lbsp;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbsw;->b:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lbsw;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object p4, p0, Lbsw;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lbsw;->b:Lcfz;

    iget-object v1, p0, Lbsw;->a:Lbsp;

    iget-object v2, p0, Lbsw;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1, v2}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "EntryPersistenceStrategy"

    const-string v1, "Unable to persist content to entry %s because it could not be read."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbsw;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lcfp;->s(Ljava/lang/String;)V

    iget-object v1, p0, Lbsw;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcfp;->r(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfp;->k()V

    goto :goto_0
.end method
