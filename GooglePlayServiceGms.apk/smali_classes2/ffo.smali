.class abstract Lffo;
.super Lffb;
.source "SourceFile"


# instance fields
.field private final c:Lfbz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lffb;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    iput-object p4, p0, Lffo;->c:Lfbz;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0}, Lffo;->c()[B

    move-result-object v0

    sget-object v1, Lffl;->a:[B

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lffo;->c:Lfbz;

    sget-object v1, Lffc;->c:Lffc;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lffl;->a(Lfbz;Lffc;Lfju;)V

    :goto_0
    return-void

    :cond_1
    sget-object v1, Lffl;->b:[B

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lffo;->c:Lfbz;

    sget-object v1, Lffc;->e:Lffc;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lffl;->a(Lfbz;Lffc;Lfju;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PeopleService"

    const-string v2, "Error during operation"

    invoke-static {v1, v2, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lffo;->c:Lfbz;

    sget-object v1, Lffc;->d:Lffc;

    invoke-static {v0, v1, v4}, Lffl;->a(Lfbz;Lffc;Lfju;)V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v1, Lfjt;->a:Lfjt;

    invoke-static {}, Lfjt;->a()[Lfju;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v1, v1, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-static {v1, v0}, Lffl;->a(Lfju;[B)V

    iget-object v0, p0, Lffo;->c:Lfbz;

    sget-object v1, Lffc;->c:Lffc;

    invoke-static {v0, v1, v2}, Lffl;->a(Lfbz;Lffc;Lfju;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v2}, Lbpm;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method protected abstract c()[B
.end method
