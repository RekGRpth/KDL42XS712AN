.class public final Lavr;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:J

.field private e:Z

.field private f:J

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lavs;

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput v0, p0, Lavr;->b:I

    iput-wide v1, p0, Lavr;->d:J

    iput-wide v1, p0, Lavr;->f:J

    iput-boolean v0, p0, Lavr;->h:Z

    iput-boolean v0, p0, Lavr;->j:Z

    iput-boolean v0, p0, Lavr;->l:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lavr;->n:Lavs;

    const/4 v0, -0x1

    iput v0, p0, Lavr;->o:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lavr;->o:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lavr;->b()I

    :cond_0
    iget v0, p0, Lavr;->o:I

    return v0
.end method

.method public final a(I)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->a:Z

    iput p1, p0, Lavr;->b:I

    return-object p0
.end method

.method public final a(J)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->c:Z

    iput-wide p1, p0, Lavr;->d:J

    return-object p0
.end method

.method public final a(Lavs;)Lavr;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->m:Z

    iput-object p1, p0, Lavr;->n:Lavs;

    return-object p0
.end method

.method public final a(Z)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->g:Z

    iput-boolean p1, p0, Lavr;->h:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lavr;->a(I)Lavr;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lavr;->a(J)Lavr;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lavr;->b(J)Lavr;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavr;->a(Z)Lavr;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavr;->b(Z)Lavr;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavr;->c(Z)Lavr;

    goto :goto_0

    :sswitch_7
    new-instance v0, Lavs;

    invoke-direct {v0}, Lavs;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lavr;->a(Lavs;)Lavr;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Lavr;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lavr;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Lavr;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lavr;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_1
    iget-boolean v0, p0, Lavr;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lavr;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_2
    iget-boolean v0, p0, Lavr;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lavr;->h:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lavr;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lavr;->j:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_4
    iget-boolean v0, p0, Lavr;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lavr;->l:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Lavr;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lavr;->n:Lavs;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lavr;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lavr;->b:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lavr;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lavr;->d:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lavr;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lavr;->f:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lavr;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lavr;->h:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lavr;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lavr;->j:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lavr;->k:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lavr;->l:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lavr;->m:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lavr;->n:Lavs;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lavr;->o:I

    return v0
.end method

.method public final b(J)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->e:Z

    iput-wide p1, p0, Lavr;->f:J

    return-object p0
.end method

.method public final b(Z)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->i:Z

    iput-boolean p1, p0, Lavr;->j:Z

    return-object p0
.end method

.method public final c(Z)Lavr;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavr;->k:Z

    iput-boolean p1, p0, Lavr;->l:Z

    return-object p0
.end method
