.class public final Liga;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lhuf;

.field private static final d:Lhuf;


# instance fields
.field private final b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

.field private final c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lihj;->ai:Livk;

    invoke-static {v0}, Ligv;->a(Livk;)Lhuf;

    move-result-object v0

    sput-object v0, Liga;->d:Lhuf;

    new-instance v0, Ligb;

    invoke-direct {v0}, Ligb;-><init>()V

    sput-object v0, Liga;->a:Lhuf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/location/places/internal/PlaceImpl;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Liga;->b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

    iput-object p2, p0, Liga;->c:Ljava/util/Map;

    return-void
.end method

.method static synthetic c()Lhuf;
    .locals 1

    sget-object v0, Liga;->d:Lhuf;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/places/internal/PlaceImpl;
    .locals 1

    iget-object v0, p0, Liga;->b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/location/places/internal/PlaceImpl;
    .locals 2

    iget-object v0, p0, Liga;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceLocalization;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Liga;->b:Lcom/google/android/gms/location/places/internal/PlaceImpl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a(Lcom/google/android/gms/location/places/internal/PlaceLocalization;)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v0

    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Liga;->c:Ljava/util/Map;

    return-object v0
.end method
