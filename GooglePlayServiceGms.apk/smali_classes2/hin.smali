.class abstract Lhin;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/lang/String;

.field final b:Lidu;

.field final c:Lhof;

.field final d:Lhkl;

.field final e:Lhiq;

.field protected f:Lhir;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lhin;->b:Lidu;

    iput-object p3, p0, Lhin;->c:Lhof;

    iput-object p4, p0, Lhin;->d:Lhkl;

    iput-object p5, p0, Lhin;->e:Lhiq;

    iput-object p6, p0, Lhin;->f:Lhir;

    iput-object p1, p0, Lhin;->a:Ljava/lang/String;

    return-void
.end method

.method protected static a(JLidr;Lhtf;Lhuv;ZI)Livi;
    .locals 9

    new-instance v1, Livi;

    sget-object v0, Lihj;->F:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    if-eqz p2, :cond_4

    if-nez p3, :cond_0

    if-eqz p4, :cond_8

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    new-instance v3, Livi;

    sget-object v4, Lihj;->i:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    const/4 v4, 0x1

    invoke-interface {p2}, Lidr;->b()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    const/4 v4, 0x2

    invoke-interface {p2}, Lidr;->c()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    new-instance v4, Livi;

    sget-object v5, Lihj;->r:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Livi;->b(ILivi;)Livi;

    const/16 v3, 0x8

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Livi;->e(II)Livi;

    const/4 v3, 0x6

    invoke-interface {p2}, Lidr;->g()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Livi;->a(IJ)Livi;

    const/16 v3, 0x11

    invoke-virtual {v4, v3, p5}, Livi;->a(IZ)Livi;

    const/4 v3, 0x3

    invoke-interface {p2}, Lidr;->a()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v4, v3, v5}, Livi;->e(II)Livi;

    invoke-interface {p2}, Lidr;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x10

    invoke-interface {p2}, Lidr;->e()F

    move-result v5

    invoke-virtual {v4, v3, v5}, Livi;->a(IF)Livi;

    :cond_1
    invoke-interface {p2}, Lidr;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0xd

    invoke-interface {p2}, Lidr;->k()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v4, v3, v5}, Livi;->e(II)Livi;

    :cond_2
    if-eqz v0, :cond_3

    invoke-interface {p2}, Lidr;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0xa

    invoke-interface {p2}, Lidr;->m()D

    move-result-wide v5

    double-to-int v3, v5

    invoke-virtual {v4, v0, v3}, Livi;->e(II)Livi;

    :cond_3
    invoke-virtual {v1, v2, v4}, Livi;->b(ILivi;)Livi;

    invoke-interface {p2}, Lidr;->f()J

    move-result-wide v2

    add-long/2addr v2, p0

    invoke-interface {p2}, Lidr;->g()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const/16 v0, 0xe

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    :cond_4
    if-eqz p3, :cond_5

    const/4 v0, 0x1

    invoke-virtual {p3, p0, p1}, Lhtf;->a(J)Livi;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Livi;->b(ILivi;)Livi;

    :cond_5
    if-eqz p4, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p4, p0, p1, v0}, Lhuv;->a(JZ)Livi;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;

    :cond_6
    new-instance v0, Livi;

    sget-object v2, Lihj;->o:Livk;

    invoke-direct {v0, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p6}, Livi;->e(II)Livi;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    const/4 v2, 0x7

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Livi;->a(IJ)Livi;

    :cond_7
    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V

    return-object v1

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected static a(Lhuv;Lidr;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lhuv;->a:J

    invoke-interface {p1}, Lidr;->f()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    const-wide/32 v3, 0xafc80

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static a(Lidr;Lidr;I)Z
    .locals 8

    invoke-interface {p0}, Lidr;->b()D

    move-result-wide v0

    invoke-interface {p0}, Lidr;->c()D

    move-result-wide v2

    invoke-interface {p1}, Lidr;->b()D

    move-result-wide v4

    invoke-interface {p1}, Lidr;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v0

    int-to-double v2, p2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final g()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fault from instance of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 7

    const/4 v1, 0x0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lhin;->f:Lhir;

    iget-object v3, p0, Lhin;->b:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    sget-object v5, Lhio;->a:[I

    iget-object v6, p0, Lhin;->f:Lhir;

    invoke-virtual {v6}, Lhir;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_0
    iget-object v3, p0, Lhin;->f:Lhir;

    if-eq v2, v3, :cond_1

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhin;->a:Ljava/lang/String;

    const-string v4, "collector state changed from %s to %s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v1

    const/4 v2, 0x1

    iget-object v6, p0, Lhin;->f:Lhir;

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-nez v0, :cond_0

    return-void

    :pswitch_0
    invoke-virtual {p0, v3, v4}, Lhin;->a(J)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v3, v4}, Lhin;->b(J)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v3, v4}, Lhin;->c(J)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v3, v4}, Lhin;->d(J)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v3, v4}, Lhin;->e(J)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, v3, v4}, Lhin;->f(J)Z

    move-result v0

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lhin;->b()Z

    move-result v0

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lhin;->c()Z

    move-result v0

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lhin;->d()Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method a(I)V
    .locals 0

    return-void
.end method

.method a(IIZ)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 0

    return-void
.end method

.method a(Lhtf;)V
    .locals 0

    return-void
.end method

.method a(Lhuv;)V
    .locals 0

    return-void
.end method

.method a(Lidr;)V
    .locals 0

    return-void
.end method

.method a(Lids;)V
    .locals 0

    return-void
.end method

.method a(Lidw;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method a(Livi;)V
    .locals 0

    return-void
.end method

.method a(Z)V
    .locals 0

    return-void
.end method

.method protected a(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Lhuv;)V
    .locals 0

    return-void
.end method

.method b(Z)V
    .locals 0

    return-void
.end method

.method protected b()Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected b(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method c(Z)V
    .locals 0

    return-void
.end method

.method protected c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected c(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected d(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method e()Z
    .locals 2

    iget-object v0, p0, Lhin;->f:Lhir;

    sget-object v1, Lhir;->a:Lhir;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhin;->f:Lhir;

    sget-object v1, Lhir;->b:Lhir;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method protected f(J)Z
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lhin;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
