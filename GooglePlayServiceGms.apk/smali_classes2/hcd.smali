.class public abstract Lhcd;
.super Lhcc;
.source "SourceFile"


# instance fields
.field protected final b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0, p3}, Lhcc;-><init>(Landroid/accounts/Account;)V

    iput-object p1, p0, Lhcd;->b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput p2, p0, Lhcd;->c:I

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lhcd;->b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lhcd;->c:I

    aget-object v0, v0, v1

    return-object v0
.end method
