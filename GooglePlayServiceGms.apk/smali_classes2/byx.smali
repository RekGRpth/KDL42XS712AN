.class public final Lbyx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

.field c:Lcar;

.field final d:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

.field final e:Landroid/view/View;

.field final f:Landroid/view/View;

.field final g:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

.field final h:Landroid/view/View;

.field final i:Landroid/widget/ImageView;

.field final j:Landroid/widget/ImageView;

.field final k:Landroid/view/View;

.field final l:Ljava/util/List;

.field final m:Z

.field final n:Landroid/widget/ImageButton;

.field final o:Landroid/widget/ImageButton;

.field final p:Landroid/widget/ImageButton;

.field final q:Landroid/widget/TextView;

.field final r:Landroid/widget/ProgressBar;

.field s:Z

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbyx;->a:Landroid/content/Context;

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iput-object v0, p0, Lbyx;->b:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const/4 v0, 0x0

    iput-object v0, p0, Lbyx;->c:Lcar;

    const v0, 0x7f0a00e0    # com.google.android.gms.R.id.group_title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iput-object v0, p0, Lbyx;->d:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const v0, 0x7f0a00e1    # com.google.android.gms.R.id.group_title_container

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyx;->e:Landroid/view/View;

    const v0, 0x7f0a00df    # com.google.android.gms.R.id.group_padding

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyx;->f:Landroid/view/View;

    const v0, 0x7f0a00ec    # com.google.android.gms.R.id.labels

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    iput-object v0, p0, Lbyx;->g:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const v0, 0x7f0a00f0    # com.google.android.gms.R.id.doc_entry_container

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyx;->k:Landroid/view/View;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    const v2, 0x7f0a00e3    # com.google.android.gms.R.id.doc_entry_root

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    array-length v1, v0

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbyx;->l:Ljava/util/List;

    const v0, 0x7f0a00e5    # com.google.android.gms.R.id.group_title_horizontal_rule

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyx;->u:Landroid/view/View;

    const v0, 0x7f0a00ef    # com.google.android.gms.R.id.entry_horizontal_rule

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyx;->v:Landroid/view/View;

    const v0, 0x7f0a00e7    # com.google.android.gms.R.id.doc_icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbyx;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lbyx;->i:Landroid/widget/ImageView;

    new-instance v1, Lbyy;

    invoke-direct {v1, p0}, Lbyy;-><init>(Lbyx;)V

    invoke-static {v0, v1}, Lfj;->a(Landroid/view/View;Ldv;)V

    const v0, 0x7f0a00eb    # com.google.android.gms.R.id.shared_icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbyx;->j:Landroid/widget/ImageView;

    const v0, 0x7f0a00ea    # com.google.android.gms.R.id.offline_icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbyx;->t:Landroid/widget/ImageView;

    const v0, 0x7f0a00e4    # com.google.android.gms.R.id.cross_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lbyx;->n:Landroid/widget/ImageButton;

    const v0, 0x7f0a00e8    # com.google.android.gms.R.id.update_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lbyx;->o:Landroid/widget/ImageButton;

    const v0, 0x7f0a00e9    # com.google.android.gms.R.id.stop_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lbyx;->p:Landroid/widget/ImageButton;

    const v0, 0x7f0a00ed    # com.google.android.gms.R.id.offline_status

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbyx;->q:Landroid/widget/TextView;

    const v0, 0x7f0a00ee    # com.google.android.gms.R.id.progress

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbyx;->r:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lbyx;->k:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyx;->k:Landroid/view/View;

    :goto_1
    iput-object v0, p0, Lbyx;->h:Landroid/view/View;

    iget-object v0, p0, Lbyx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcbl;->a(Landroid/content/res/Resources;)Z

    move-result v0

    iput-boolean v0, p0, Lbyx;->m:Z

    return-void

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcbu;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    const v0, 0x7f0a00e6    # com.google.android.gms.R.id.main_body

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method
