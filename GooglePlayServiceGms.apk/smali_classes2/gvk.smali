.class public final Lgvk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field final synthetic b:Lgvy;

.field final synthetic c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgvy;)V
    .locals 0

    iput-object p1, p0, Lgvk;->c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object p2, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object p3, p0, Lgvk;->b:Lgvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtw;

    invoke-virtual {v0}, Lgtw;->b()Lixo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lgvk;->c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v3, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;Lixo;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lgvk;->c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->T_()Lo;

    move-result-object v2

    const-string v3, "address_entry"

    const-string v4, "autocomplete_address"

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getTag()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {v0}, Lgtw;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    iget-object v0, p0, Lgvk;->b:Lgvy;

    invoke-virtual {v0}, Lgvy;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgvk;->b:Lgvy;

    invoke-virtual {v0}, Lgvy;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    int-to-long v0, v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3, v4, v5, v0}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V

    return-void

    :cond_1
    invoke-virtual {v0}, Lgtw;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lgvo;

    iget-object v2, p0, Lgvk;->c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v3, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v2, v3}, Lgvo;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;)V

    new-array v2, v7, [Lgtw;

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lgvo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvk;->c:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v2, p0, Lgvk;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;)V

    goto :goto_0

    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_1
.end method
