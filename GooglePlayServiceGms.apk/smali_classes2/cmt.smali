.class public final Lcmt;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:J

.field private final d:Lbsj;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;JLbsj;Lcms;)V
    .locals 6

    sget-object v1, Lcmr;->b:Lcmr;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-wide p4, p0, Lcmt;->c:J

    invoke-static {p6}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsj;

    iput-object v0, p0, Lcmt;->d:Lbsj;

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->b:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "packagingId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcmt;->c:J

    const-string v0, "isAuthorized"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbsj;->a:Lbsj;

    :goto_0
    iput-object v0, p0, Lcmt;->d:Lbsj;

    return-void

    :cond_0
    sget-object v0, Lbsj;->b:Lbsj;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmt;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 8

    iget-wide v0, p0, Lcmt;->c:J

    iget-object v2, p0, Lcmt;->d:Lbsj;

    invoke-interface {p1, p2, v0, v1, v2}, Lcfz;->a(Lcfp;JLbsj;)Lbsj;

    move-result-object v6

    iget-object v0, p0, Lcmt;->d:Lbsj;

    invoke-virtual {v6, v0}, Lbsj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcmj;

    iget-object v1, p3, Lbsp;->a:Lcfc;

    iget-object v2, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcms;->b:Lcms;

    invoke-direct {v0, v1, v2, v3}, Lcmj;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcmt;

    iget-object v1, p3, Lbsp;->a:Lcfc;

    iget-object v2, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-wide v4, p0, Lcmt;->c:J

    sget-object v7, Lcms;->b:Lcms;

    invoke-direct/range {v0 .. v7}, Lcmt;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;JLbsj;Lcms;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 3

    sget-object v0, Lcmu;->a:[I

    iget-object v1, p0, Lcmt;->d:Lbsj;

    invoke-virtual {v1}, Lbsj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-virtual {p3}, Lcoy;->j()Lcll;

    move-result-object v0

    iget-wide v1, p0, Lcmt;->c:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lclk;

    return-void

    :pswitch_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No server API to deauthorize files."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "packagingId"

    iget-wide v2, p0, Lcmt;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "isAuthorized"

    iget-object v2, p0, Lcmt;->d:Lbsj;

    sget-object v3, Lbsj;->a:Lbsj;

    invoke-virtual {v2, v3}, Lbsj;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcmt;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcmt;

    invoke-virtual {p0, p1}, Lcmt;->a(Lclu;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcmt;->c:J

    iget-wide v4, p1, Lcmt;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcmt;->d:Lbsj;

    iget-object v3, p1, Lcmt;->d:Lbsj;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcmt;->c:J

    iget-wide v3, p0, Lcmt;->c:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcmt;->d:Lbsj;

    invoke-virtual {v1}, Lbsj;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "SetAppAuthStateOp [%s, appPackagingId=%s, authState=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcmt;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcmt;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcmt;->d:Lbsj;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
