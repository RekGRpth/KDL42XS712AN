.class public final enum Lhup;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhup;

.field public static final enum b:Lhup;

.field public static final enum c:Lhup;

.field private static final synthetic d:[Lhup;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhup;

    const-string v1, "STATIONARY"

    invoke-direct {v0, v1, v2}, Lhup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhup;->a:Lhup;

    new-instance v0, Lhup;

    const-string v1, "MOVING"

    invoke-direct {v0, v1, v3}, Lhup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhup;->b:Lhup;

    new-instance v0, Lhup;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lhup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhup;->c:Lhup;

    const/4 v0, 0x3

    new-array v0, v0, [Lhup;

    sget-object v1, Lhup;->a:Lhup;

    aput-object v1, v0, v2

    sget-object v1, Lhup;->b:Lhup;

    aput-object v1, v0, v3

    sget-object v1, Lhup;->c:Lhup;

    aput-object v1, v0, v4

    sput-object v0, Lhup;->d:[Lhup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhup;
    .locals 1

    const-class v0, Lhup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhup;

    return-object v0
.end method

.method public static values()[Lhup;
    .locals 1

    sget-object v0, Lhup;->d:[Lhup;

    invoke-virtual {v0}, [Lhup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhup;

    return-object v0
.end method
