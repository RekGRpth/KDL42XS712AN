.class public final Ljay;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljbg;

.field public b:Ljava/lang/String;

.field public c:[I

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljbf;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v1, p0, Ljay;->a:Ljbg;

    const-string v0, ""

    iput-object v0, p0, Ljay;->b:Ljava/lang/String;

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Ljay;->c:[I

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljay;->d:Z

    const-string v0, ""

    iput-object v0, p0, Ljay;->e:Ljava/lang/String;

    iput-object v1, p0, Ljay;->f:Ljbf;

    const/4 v0, -0x1

    iput v0, p0, Ljay;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Ljay;->a:Ljbg;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljay;->a:Ljbg;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Ljay;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljay;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljay;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljay;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    iget-object v3, p0, Ljay;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Ljay;->c:[I

    aget v3, v3, v1

    invoke-static {v3}, Lizn;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    add-int/2addr v0, v2

    iget-object v1, p0, Ljay;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljay;->d:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x4

    iget-boolean v2, p0, Ljay;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ljay;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Ljay;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Ljay;->f:Ljbf;

    if-eqz v1, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Ljay;->f:Ljbf;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Ljay;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljay;->a:Ljbg;

    if-nez v0, :cond_1

    new-instance v0, Ljbg;

    invoke-direct {v0}, Ljbg;-><init>()V

    iput-object v0, p0, Ljay;->a:Ljbg;

    :cond_1
    iget-object v0, p0, Ljay;->a:Ljbg;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljay;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lizm;->a()I

    :cond_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljay;->c:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Ljay;->c:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ljay;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Ljay;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljay;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lizm;->e(I)V

    iget-object v1, p0, Ljay;->c:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Ljay;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Ljay;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Ljay;->c:[I

    :cond_b
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljay;->d:Z

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljay;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljay;->f:Ljbf;

    if-nez v0, :cond_c

    new-instance v0, Ljbf;

    invoke-direct {v0}, Ljbf;-><init>()V

    iput-object v0, p0, Ljay;->f:Ljbf;

    :cond_c
    iget-object v0, p0, Ljay;->f:Ljbf;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Ljay;->a:Ljbg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljay;->a:Ljbg;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Ljay;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljay;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljay;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljay;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljay;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljay;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Ljay;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Ljay;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_3
    iget-object v0, p0, Ljay;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljay;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Ljay;->f:Ljbf;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljay;->f:Ljbf;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_5
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljay;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljay;

    iget-object v2, p0, Ljay;->a:Ljbg;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljay;->a:Ljbg;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljay;->a:Ljbg;

    iget-object v3, p1, Ljay;->a:Ljbg;

    invoke-virtual {v2, v3}, Ljbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljay;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Ljay;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljay;->b:Ljava/lang/String;

    iget-object v3, p1, Ljay;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljay;->c:[I

    iget-object v3, p1, Ljay;->c:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Ljay;->d:Z

    iget-boolean v3, p1, Ljay;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljay;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Ljay;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljay;->e:Ljava/lang/String;

    iget-object v3, p1, Ljay;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljay;->f:Ljbf;

    if-nez v2, :cond_b

    iget-object v2, p1, Ljay;->f:Ljbf;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ljay;->f:Ljbf;

    iget-object v3, p1, Ljay;->f:Ljbf;

    invoke-virtual {v2, v3}, Ljbf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljay;->a:Ljbg;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljay;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljay;->c:[I

    invoke-static {v2}, Lizq;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Ljay;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljay;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljay;->f:Ljbf;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljay;->a:Ljbg;

    invoke-virtual {v0}, Ljbg;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljay;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljay;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Ljay;->f:Ljbf;

    invoke-virtual {v1}, Ljbf;->hashCode()I

    move-result v1

    goto :goto_4
.end method
