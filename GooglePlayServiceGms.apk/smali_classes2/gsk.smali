.class public final Lgsk;
.super Lsc;
.source "SourceFile"


# static fields
.field static final f:[B


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/util/HashMap;

.field private final j:Luu;

.field private final k:Lut;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Liph;

    invoke-direct {v0}, Liph;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v2, v1, v2

    iput-object v1, v0, Liph;->a:[I

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    sput-object v0, Lgsk;->f:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lhgm;Landroid/os/Handler;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-object p1, p0, Lgsk;->g:Landroid/content/Context;

    iput-object p4, p0, Lgsk;->h:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgsk;->i:Ljava/util/HashMap;

    invoke-virtual {p3}, Lhgm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgsk;->i:Ljava/util/HashMap;

    const-string v1, "Authorization"

    invoke-virtual {p3}, Lhgm;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lgsk;->i:Ljava/util/HashMap;

    const-string v1, "X-Version"

    const v2, 0x41fea6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgsk;->i:Ljava/util/HashMap;

    const-string v1, "X-Modality"

    const-string v2, "ANDROID_NATIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Luu;

    const-string v1, "get_wallet_profile"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgsk;->j:Luu;

    iget-object v0, p0, Lgsk;->j:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lgsk;->k:Lut;

    return-void
.end method

.method private static b(Lrz;)Z
    .locals 2

    iget-object v0, p0, Lrz;->c:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrz;->c:Ljava/util/Map;

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "application/x-protobuf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 5

    iget-object v0, p0, Lgsk;->j:Luu;

    iget-object v1, p0, Lgsk;->k:Lut;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rpc"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgsk;->g:Landroid/content/Context;

    iget-object v1, p0, Lgsk;->j:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lrz;

    invoke-static {p1}, Lgsk;->b(Lrz;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsk;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p1, Lrz;->b:[B

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgsk;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    new-instance v2, Lsp;

    invoke-direct {v2, p1}, Lsp;-><init>(Lrz;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public final b(Lsp;)V
    .locals 3

    iget-object v0, p1, Lsp;->a:Lrz;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lsp;->a:Lrz;

    invoke-static {v0}, Lgsk;->b(Lrz;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsk;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p1, Lsp;->a:Lrz;

    iget-object v2, v2, Lrz;->b:[B

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgsk;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lgsk;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    const-string v0, "application/x-protobuf"

    return-object v0
.end method

.method public final m()[B
    .locals 1

    sget-object v0, Lgsk;->f:[B

    return-object v0
.end method

.method public final o()Lse;
    .locals 1

    sget-object v0, Lse;->a:Lse;

    return-object v0
.end method
