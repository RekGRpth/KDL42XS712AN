.class public final Ldgz;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ldhc;

.field public c:Ldha;

.field public d:Ldhf;

.field public e:Ldhe;

.field public f:Ldhd;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ldgz;->a:I

    iput-object v1, p0, Ldgz;->b:Ldhc;

    iput-object v1, p0, Ldgz;->c:Ldha;

    iput-object v1, p0, Ldgz;->d:Ldhf;

    iput-object v1, p0, Ldgz;->e:Ldhe;

    iput-object v1, p0, Ldgz;->f:Ldhd;

    const/4 v0, -0x1

    iput v0, p0, Ldgz;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Ldgz;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ldgz;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ldgz;->b:Ldhc;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ldgz;->b:Ldhc;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ldgz;->c:Ldha;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ldgz;->c:Ldha;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ldgz;->d:Ldhf;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ldgz;->d:Ldhf;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ldgz;->e:Ldhe;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ldgz;->e:Ldhe;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ldgz;->f:Ldhd;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ldgz;->f:Ldhd;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Ldgz;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ldgz;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgz;->b:Ldhc;

    if-nez v0, :cond_1

    new-instance v0, Ldhc;

    invoke-direct {v0}, Ldhc;-><init>()V

    iput-object v0, p0, Ldgz;->b:Ldhc;

    :cond_1
    iget-object v0, p0, Ldgz;->b:Ldhc;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldgz;->c:Ldha;

    if-nez v0, :cond_2

    new-instance v0, Ldha;

    invoke-direct {v0}, Ldha;-><init>()V

    iput-object v0, p0, Ldgz;->c:Ldha;

    :cond_2
    iget-object v0, p0, Ldgz;->c:Ldha;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldgz;->d:Ldhf;

    if-nez v0, :cond_3

    new-instance v0, Ldhf;

    invoke-direct {v0}, Ldhf;-><init>()V

    iput-object v0, p0, Ldgz;->d:Ldhf;

    :cond_3
    iget-object v0, p0, Ldgz;->d:Ldhf;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldgz;->e:Ldhe;

    if-nez v0, :cond_4

    new-instance v0, Ldhe;

    invoke-direct {v0}, Ldhe;-><init>()V

    iput-object v0, p0, Ldgz;->e:Ldhe;

    :cond_4
    iget-object v0, p0, Ldgz;->e:Ldhe;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldgz;->f:Ldhd;

    if-nez v0, :cond_5

    new-instance v0, Ldhd;

    invoke-direct {v0}, Ldhd;-><init>()V

    iput-object v0, p0, Ldgz;->f:Ldhd;

    :cond_5
    iget-object v0, p0, Ldgz;->f:Ldhd;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Ldgz;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ldgz;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Ldgz;->b:Ldhc;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ldgz;->b:Ldhc;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Ldgz;->c:Ldha;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ldgz;->c:Ldha;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Ldgz;->d:Ldhf;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ldgz;->d:Ldhf;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    iget-object v0, p0, Ldgz;->e:Ldhe;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ldgz;->e:Ldhe;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    iget-object v0, p0, Ldgz;->f:Ldhd;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ldgz;->f:Ldhd;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_5
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ldgz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ldgz;

    iget v2, p0, Ldgz;->a:I

    iget v3, p1, Ldgz;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ldgz;->b:Ldhc;

    if-nez v2, :cond_4

    iget-object v2, p1, Ldgz;->b:Ldhc;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ldgz;->b:Ldhc;

    iget-object v3, p1, Ldgz;->b:Ldhc;

    invoke-virtual {v2, v3}, Ldhc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ldgz;->c:Ldha;

    if-nez v2, :cond_6

    iget-object v2, p1, Ldgz;->c:Ldha;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ldgz;->c:Ldha;

    iget-object v3, p1, Ldgz;->c:Ldha;

    invoke-virtual {v2, v3}, Ldha;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ldgz;->d:Ldhf;

    if-nez v2, :cond_8

    iget-object v2, p1, Ldgz;->d:Ldhf;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ldgz;->d:Ldhf;

    iget-object v3, p1, Ldgz;->d:Ldhf;

    invoke-virtual {v2, v3}, Ldhf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ldgz;->e:Ldhe;

    if-nez v2, :cond_a

    iget-object v2, p1, Ldgz;->e:Ldhe;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ldgz;->e:Ldhe;

    iget-object v3, p1, Ldgz;->e:Ldhe;

    invoke-virtual {v2, v3}, Ldhe;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ldgz;->f:Ldhd;

    if-nez v2, :cond_c

    iget-object v2, p1, Ldgz;->f:Ldhd;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Ldgz;->f:Ldhd;

    iget-object v3, p1, Ldgz;->f:Ldhd;

    invoke-virtual {v2, v3}, Ldhd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Ldgz;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldgz;->b:Ldhc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldgz;->c:Ldha;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldgz;->d:Ldhf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldgz;->e:Ldhe;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldgz;->f:Ldhd;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ldgz;->b:Ldhc;

    invoke-virtual {v0}, Ldhc;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldgz;->c:Ldha;

    invoke-virtual {v0}, Ldha;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ldgz;->d:Ldhf;

    invoke-virtual {v0}, Ldhf;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldgz;->e:Ldhe;

    invoke-virtual {v0}, Ldhe;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Ldgz;->f:Ldhd;

    invoke-virtual {v1}, Ldhd;->hashCode()I

    move-result v1

    goto :goto_4
.end method
