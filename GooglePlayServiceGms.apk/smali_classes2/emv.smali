.class public final Lemv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lemx;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Leiy;Lelt;)V
    .locals 1

    new-instance v0, Lemx;

    invoke-direct {v0, p1, p2, p3}, Lemx;-><init>(Landroid/content/pm/PackageManager;Leiy;Lelt;)V

    invoke-direct {p0, v0}, Lemv;-><init>(Lemx;)V

    return-void
.end method

.method private constructor <init>(Lemx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lemv;->a:Lemx;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lemy;
    .locals 4

    new-instance v0, Lemw;

    invoke-direct {v0, p1}, Lemw;-><init>(Landroid/content/Context;)V

    const-string v1, "getFileDescriptor can not be called on main thread"

    invoke-static {v1}, Lbkm;->c(Ljava/lang/String;)V

    iget-object v1, v0, Lemw;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v1, 0x0

    iput-object v1, v0, Lemw;->d:Landroid/os/ParcelFileDescriptor;

    iget-object v1, v0, Lemw;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    iget-object v1, v0, Lemw;->a:Lbdu;

    invoke-interface {v1}, Lbdu;->a()V

    iget-object v1, v0, Lemw;->b:Landroid/os/ConditionVariable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v1

    iget-object v2, v0, Lemw;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-nez v1, :cond_0

    const-string v1, "Usage reports not received in time."

    invoke-static {v1}, Lehe;->e(Ljava/lang/String;)I

    :cond_0
    iget-object v1, v0, Lemw;->a:Lbdu;

    invoke-interface {v1}, Lbdu;->b()V

    iget-object v0, v0, Lemw;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lemu;->a(Landroid/os/ParcelFileDescriptor;)Ljava/util/Iterator;

    move-result-object v1

    new-instance v2, Lemy;

    iget-object v3, p0, Lemv;->a:Lemx;

    invoke-direct {v2, v1, v0, v3}, Lemy;-><init>(Ljava/util/Iterator;Landroid/os/ParcelFileDescriptor;Lemx;)V

    return-object v2
.end method
