.class public final Lezh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Leyl;

.field private static b:Leyl;

.field private static c:Leyl;

.field private static d:Leyl;

.field private static e:Leyi;

.field private static f:Leyi;

.field private static g:Leyl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyl;

    invoke-direct {v0}, Leyl;-><init>()V

    sput-object v0, Lezh;->a:Leyl;

    new-instance v0, Leyl;

    invoke-direct {v0}, Leyl;-><init>()V

    sput-object v0, Lezh;->b:Leyl;

    new-instance v0, Leyl;

    invoke-direct {v0}, Leyl;-><init>()V

    sput-object v0, Lezh;->c:Leyl;

    new-instance v0, Leyl;

    invoke-direct {v0}, Leyl;-><init>()V

    sput-object v0, Lezh;->d:Leyl;

    new-instance v0, Leyi;

    invoke-direct {v0}, Leyi;-><init>()V

    sput-object v0, Lezh;->e:Leyi;

    new-instance v0, Leyi;

    invoke-direct {v0}, Leyi;-><init>()V

    sput-object v0, Lezh;->f:Leyi;

    new-instance v0, Leyl;

    invoke-direct {v0}, Leyl;-><init>()V

    sput-object v0, Lezh;->g:Leyl;

    return-void
.end method

.method public static a(Leyi;Leyl;)V
    .locals 13

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Leyi;->a(II)D

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Leyi;->a(II)D

    move-result-wide v2

    add-double/2addr v0, v2

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Leyi;->a(II)D

    move-result-wide v2

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double v7, v0, v2

    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Leyi;->a(II)D

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Leyi;->a(II)D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double v1, v0, v2

    const/4 v0, 0x0

    const/4 v3, 0x2

    invoke-virtual {p0, v0, v3}, Leyi;->a(II)D

    move-result-wide v3

    const/4 v0, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5}, Leyi;->a(II)D

    move-result-wide v5

    sub-double/2addr v3, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v5

    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5}, Leyi;->a(II)D

    move-result-wide v5

    const/4 v0, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0, v0, v9}, Leyi;->a(II)D

    move-result-wide v9

    sub-double/2addr v5, v9

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    div-double/2addr v5, v9

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Leyl;->a(DDD)V

    invoke-virtual {p1}, Leyl;->c()D

    move-result-wide v0

    const-wide v2, 0x3fe6a09e667f3bcdL    # 0.7071067811865476

    cmpl-double v2, v7, v2

    if-lez v2, :cond_1

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v2

    div-double v0, v2, v0

    invoke-virtual {p1, v0, v1}, Leyl;->a(D)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v2, -0x40195f619980c433L    # -0.7071067811865476

    cmpl-double v2, v7, v2

    if-lez v2, :cond_2

    invoke-static {v7, v8}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    div-double v0, v2, v0

    invoke-virtual {p1, v0, v1}, Leyl;->a(D)V

    goto :goto_0

    :cond_2
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    sub-double v9, v2, v0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Leyi;->a(II)D

    move-result-wide v0

    sub-double v1, v0, v7

    const/4 v0, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3}, Leyi;->a(II)D

    move-result-wide v3

    sub-double/2addr v3, v7

    const/4 v0, 0x2

    const/4 v5, 0x2

    invoke-virtual {p0, v0, v5}, Leyi;->a(II)D

    move-result-wide v5

    sub-double/2addr v5, v7

    sget-object v0, Lezh;->g:Leyl;

    mul-double v7, v1, v1

    mul-double v11, v3, v3

    cmpl-double v7, v7, v11

    if-lez v7, :cond_4

    mul-double v7, v1, v1

    mul-double v11, v5, v5

    cmpl-double v7, v7, v11

    if-lez v7, :cond_4

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Leyi;->a(II)D

    move-result-wide v3

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Leyi;->a(II)D

    move-result-wide v5

    add-double/2addr v3, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Leyi;->a(II)D

    move-result-wide v5

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Leyi;->a(II)D

    move-result-wide v7

    add-double/2addr v5, v7

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    div-double/2addr v5, v7

    invoke-virtual/range {v0 .. v6}, Leyl;->a(DDD)V

    :goto_1
    invoke-static {v0, p1}, Leyl;->a(Leyl;Leyl;)D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_3

    const-wide/high16 v1, -0x4010000000000000L    # -1.0

    invoke-virtual {v0, v1, v2}, Leyl;->a(D)V

    :cond_3
    invoke-virtual {v0}, Leyl;->b()V

    invoke-virtual {v0, v9, v10}, Leyl;->a(D)V

    invoke-virtual {p1, v0}, Leyl;->a(Leyl;)V

    goto/16 :goto_0

    :cond_4
    mul-double v1, v3, v3

    mul-double v7, v5, v5

    cmpl-double v1, v1, v7

    if-lez v1, :cond_5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Leyi;->a(II)D

    move-result-wide v1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Leyi;->a(II)D

    move-result-wide v5

    add-double/2addr v1, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v5

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Leyi;->a(II)D

    move-result-wide v5

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-virtual {p0, v7, v8}, Leyi;->a(II)D

    move-result-wide v7

    add-double/2addr v5, v7

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    div-double/2addr v5, v7

    invoke-virtual/range {v0 .. v6}, Leyl;->a(DDD)V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Leyi;->a(II)D

    move-result-wide v1

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Leyi;->a(II)D

    move-result-wide v3

    add-double/2addr v1, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Leyi;->a(II)D

    move-result-wide v3

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-virtual {p0, v7, v8}, Leyi;->a(II)D

    move-result-wide v7

    add-double/2addr v3, v7

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v7

    invoke-virtual/range {v0 .. v6}, Leyl;->a(DDD)V

    goto :goto_1
.end method

.method public static a(Leyl;Leyi;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Leyl;->a(Leyl;Leyl;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide v6, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v6, v4, v6

    if-gez v6, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide v6, 0x3fc5555560000000L    # 0.1666666716337204

    mul-double/2addr v4, v6

    sub-double v4, v2, v4

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    :goto_0
    move-object/from16 v0, p0

    iget-wide v6, v0, Leyl;->a:D

    move-object/from16 v0, p0

    iget-wide v8, v0, Leyl;->a:D

    mul-double/2addr v6, v8

    move-object/from16 v0, p0

    iget-wide v8, v0, Leyl;->b:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Leyl;->b:D

    mul-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Leyl;->c:D

    move-object/from16 v0, p0

    iget-wide v12, v0, Leyl;->c:D

    mul-double/2addr v10, v12

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    add-double v16, v8, v10

    mul-double v16, v16, v2

    sub-double v14, v14, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v14, v15}, Leyi;->a(IID)V

    const/4 v12, 0x1

    const/4 v13, 0x1

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v6

    mul-double/2addr v10, v2

    sub-double v10, v14, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v10, v11}, Leyi;->a(IID)V

    const/4 v10, 0x2

    const/4 v11, 0x2

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v6, v8

    mul-double/2addr v6, v2

    sub-double v6, v12, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v6, v7}, Leyi;->a(IID)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Leyl;->c:D

    mul-double/2addr v6, v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Leyl;->a:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Leyl;->b:D

    mul-double/2addr v8, v10

    mul-double/2addr v8, v2

    const/4 v10, 0x0

    const/4 v11, 0x1

    sub-double v12, v8, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v12, v13}, Leyi;->a(IID)V

    const/4 v10, 0x1

    const/4 v11, 0x0

    add-double/2addr v6, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v6, v7}, Leyi;->a(IID)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Leyl;->b:D

    mul-double/2addr v6, v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Leyl;->a:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Leyl;->c:D

    mul-double/2addr v8, v10

    mul-double/2addr v8, v2

    const/4 v10, 0x0

    const/4 v11, 0x2

    add-double v12, v8, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v12, v13}, Leyi;->a(IID)V

    const/4 v10, 0x2

    const/4 v11, 0x0

    sub-double v6, v8, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v6, v7}, Leyi;->a(IID)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Leyl;->a:D

    mul-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Leyl;->b:D

    move-object/from16 v0, p0

    iget-wide v8, v0, Leyl;->c:D

    mul-double/2addr v6, v8

    mul-double/2addr v2, v6

    const/4 v6, 0x1

    const/4 v7, 0x2

    sub-double v8, v2, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8, v9}, Leyi;->a(IID)V

    const/4 v6, 0x2

    const/4 v7, 0x1

    add-double/2addr v2, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v2, v3}, Leyi;->a(IID)V

    return-void

    :cond_0
    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v6, v4, v6

    if-gez v6, :cond_1

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide v6, 0x3fa5555560000000L    # 0.0416666679084301

    mul-double/2addr v6, v4

    sub-double/2addr v2, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x3fc5555560000000L    # 0.1666666716337204

    mul-double/2addr v8, v4

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide v12, 0x3fc5555560000000L    # 0.1666666716337204

    mul-double/2addr v4, v12

    sub-double v4, v10, v4

    mul-double/2addr v4, v8

    sub-double v4, v6, v4

    goto/16 :goto_0

    :cond_1
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v6, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    sub-double v2, v8, v2

    mul-double/2addr v6, v6

    mul-double/2addr v2, v6

    goto/16 :goto_0
.end method

.method public static a(Leyl;Leyl;Leyi;)V
    .locals 11

    const/4 v10, 0x5

    const/4 v9, 0x3

    const/4 v4, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    invoke-virtual {p2}, Leyi;->b()V

    sget-object v0, Lezh;->b:Leyl;

    invoke-static {p0, p1, v0}, Leyl;->a(Leyl;Leyl;Leyl;)V

    sget-object v0, Lezh;->b:Leyl;

    invoke-virtual {v0}, Leyl;->c()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lezh;->c:Leyl;

    invoke-virtual {v0, p0}, Leyl;->a(Leyl;)V

    sget-object v0, Lezh;->d:Leyl;

    invoke-virtual {v0, p1}, Leyl;->a(Leyl;)V

    sget-object v0, Lezh;->b:Leyl;

    invoke-virtual {v0}, Leyl;->b()V

    sget-object v0, Lezh;->c:Leyl;

    invoke-virtual {v0}, Leyl;->b()V

    sget-object v0, Lezh;->d:Leyl;

    invoke-virtual {v0}, Leyl;->b()V

    sget-object v0, Lezh;->e:Leyi;

    sget-object v1, Lezh;->c:Leyl;

    invoke-virtual {v0, v4, v1}, Leyi;->a(ILeyl;)V

    sget-object v1, Lezh;->b:Leyl;

    invoke-virtual {v0, v7, v1}, Leyi;->a(ILeyl;)V

    sget-object v1, Lezh;->b:Leyl;

    sget-object v2, Lezh;->c:Leyl;

    sget-object v3, Lezh;->a:Leyl;

    invoke-static {v1, v2, v3}, Leyl;->a(Leyl;Leyl;Leyl;)V

    sget-object v1, Lezh;->a:Leyl;

    invoke-virtual {v0, v8, v1}, Leyi;->a(ILeyl;)V

    sget-object v1, Lezh;->f:Leyi;

    sget-object v2, Lezh;->d:Leyl;

    invoke-virtual {v1, v4, v2}, Leyi;->a(ILeyl;)V

    sget-object v2, Lezh;->b:Leyl;

    invoke-virtual {v1, v7, v2}, Leyi;->a(ILeyl;)V

    sget-object v2, Lezh;->b:Leyl;

    sget-object v3, Lezh;->d:Leyl;

    sget-object v4, Lezh;->a:Leyl;

    invoke-static {v2, v3, v4}, Leyl;->a(Leyl;Leyl;Leyl;)V

    sget-object v2, Lezh;->a:Leyl;

    invoke-virtual {v1, v8, v2}, Leyi;->a(ILeyl;)V

    iget-object v2, v0, Leyi;->a:[D

    aget-wide v2, v2, v7

    iget-object v4, v0, Leyi;->a:[D

    iget-object v5, v0, Leyi;->a:[D

    aget-wide v5, v5, v9

    aput-wide v5, v4, v7

    iget-object v4, v0, Leyi;->a:[D

    aput-wide v2, v4, v9

    iget-object v2, v0, Leyi;->a:[D

    aget-wide v2, v2, v8

    iget-object v4, v0, Leyi;->a:[D

    iget-object v5, v0, Leyi;->a:[D

    const/4 v6, 0x6

    aget-wide v5, v5, v6

    aput-wide v5, v4, v8

    iget-object v4, v0, Leyi;->a:[D

    const/4 v5, 0x6

    aput-wide v2, v4, v5

    iget-object v2, v0, Leyi;->a:[D

    aget-wide v2, v2, v10

    iget-object v4, v0, Leyi;->a:[D

    iget-object v5, v0, Leyi;->a:[D

    const/4 v6, 0x7

    aget-wide v5, v5, v6

    aput-wide v5, v4, v10

    iget-object v4, v0, Leyi;->a:[D

    const/4 v5, 0x7

    aput-wide v2, v4, v5

    invoke-static {v1, v0, p2}, Leyi;->a(Leyi;Leyi;Leyi;)V

    goto/16 :goto_0
.end method
