.class public final Lige;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Livi;)Liga;
    .locals 15

    const/4 v10, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v13, 0x0

    invoke-virtual {p0, v6}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PlaceCodec"

    const-string v1, "received place lacks id"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v13

    :goto_0
    if-nez v0, :cond_d

    :goto_1
    return-object v13

    :cond_0
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PlaceCodec"

    const-string v1, "received place lacks timestamp"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v13

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v9}, Livi;->k(I)I

    move-result v3

    if-nez v3, :cond_2

    const-string v0, "PlaceCodec"

    const-string v1, "received place with no types"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v13

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "PlaceCodec"

    const-string v1, "received place lacks geometry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v13

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Livi;->f(I)Livi;

    move-result-object v7

    invoke-virtual {v7, v6}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "PlaceCodec"

    const-string v1, "received place lacks latlng"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v13

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v6}, Livi;->g(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {p0, v1}, Livi;->d(I)J

    move-result-wide v11

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v8

    :goto_2
    if-ge v2, v3, :cond_5

    invoke-virtual {p0, v9, v2}, Livi;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-static {p0}, Lige;->b(Livi;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v7, v6}, Livi;->f(I)Livi;

    move-result-object v3

    invoke-static {v3}, Lige;->c(Livi;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v7, v9}, Livi;->i(I)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v7, v9}, Livi;->c(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    :cond_6
    invoke-virtual {v7, v10}, Livi;->i(I)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-virtual {v7, v10}, Livi;->f(I)Livi;

    move-result-object v5

    invoke-virtual {v5, v6}, Livi;->f(I)Livi;

    move-result-object v6

    invoke-static {v6}, Lige;->c(Livi;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    invoke-virtual {v5, v9}, Livi;->f(I)Livi;

    move-result-object v5

    invoke-static {v5}, Lige;->c(Livi;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v9

    new-instance v5, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v5, v6, v9}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    :goto_3
    const-string v6, ""

    const/4 v9, 0x4

    invoke-virtual {v7, v9}, Livi;->i(I)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v6, 0x4

    invoke-virtual {v7, v6}, Livi;->g(I)Ljava/lang/String;

    move-result-object v6

    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p0, v7}, Livi;->h(I)Z

    move-result v7

    if-eqz v7, :cond_b

    const/4 v7, 0x6

    invoke-virtual {p0, v7}, Livi;->g(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    :goto_4
    if-eqz p0, :cond_8

    const/16 v9, 0x9

    invoke-virtual {p0, v9}, Livi;->i(I)Z

    move-result v9

    if-nez v9, :cond_c

    :cond_8
    :goto_5
    const/high16 v9, -0x40800000    # -1.0f

    const/16 v10, 0xb

    invoke-virtual {p0, v10}, Livi;->i(I)Z

    move-result v10

    if-eqz v10, :cond_9

    const/16 v9, 0xb

    invoke-virtual {p0, v9}, Livi;->c(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x41200000    # 10.0f

    div-float/2addr v9, v10

    :cond_9
    const/4 v10, -0x1

    const/16 v14, 0xc

    invoke-virtual {p0, v14}, Livi;->i(I)Z

    move-result v14

    if-eqz v14, :cond_a

    const/16 v10, 0xc

    invoke-virtual {p0, v10}, Livi;->c(I)I

    move-result v10

    :cond_a
    invoke-static/range {v0 .. v12}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a(Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJ)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    move-object v7, v13

    goto :goto_4

    :cond_c
    const/16 v8, 0x9

    invoke-virtual {p0, v8}, Livi;->b(I)Z

    move-result v8

    goto :goto_5

    :cond_d
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, p0}, Lige;->a(Ljava/util/Map;Livi;)V

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v1, "PlaceCodec"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no localizations for place w/id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_e
    new-instance v13, Liga;

    invoke-direct {v13, v0, v1}, Liga;-><init>(Lcom/google/android/gms/location/places/internal/PlaceImpl;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_f
    move-object v5, v13

    goto/16 :goto_3
.end method

.method static a(Lcom/google/android/gms/maps/model/LatLng;)Livi;
    .locals 6

    const-wide v4, 0x416312d000000000L    # 1.0E7

    new-instance v0, Livi;

    sget-object v1, Lihj;->i:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    return-object v0
.end method

.method static a(Ljava/util/Map;Livi;)V
    .locals 12

    const/4 v11, 0x7

    const/4 v1, 0x0

    const/4 v10, 0x3

    invoke-virtual {p1, v10}, Livi;->k(I)I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v10, v2}, Livi;->c(II)Livi;

    move-result-object v4

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Livi;->g(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v4, v11}, Livi;->k(I)I

    move-result v0

    new-array v6, v0, [Ljava/lang/String;

    move v0, v1

    :goto_1
    array-length v7, v6

    if-ge v0, v7, :cond_0

    invoke-virtual {v4, v11, v0}, Livi;->d(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Livi;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v10}, Livi;->g(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v4, v8}, Livi;->g(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x5

    invoke-virtual {v4, v9}, Livi;->g(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {v0, v7, v8, v4, v6}, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/location/places/internal/PlaceLocalization;

    move-result-object v0

    invoke-interface {p0, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method static a(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Livi;)Landroid/os/Bundle;
    .locals 6

    const/4 v5, 0x4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v5}, Livi;->k(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v5, v0}, Livi;->c(II)Livi;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Livi;->g(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Livi;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static c(Livi;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 7

    const-wide v5, 0x416312d000000000L    # 1.0E7

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    const/4 v1, 0x1

    invoke-static {p0, v1}, Ligv;->a(Livi;I)I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v5

    const/4 v3, 0x2

    invoke-static {p0, v3}, Ligv;->a(Livi;I)I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method
