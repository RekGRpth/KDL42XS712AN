.class final Liex;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Liev;

.field private final b:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Liev;)V
    .locals 2

    iput-object p1, p0, Liex;->a:Liev;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iget-object v0, p0, Liex;->a:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Liex;->b:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method synthetic constructor <init>(Liev;B)V
    .locals 0

    invoke-direct {p0, p1}, Liex;-><init>(Liev;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/16 v4, 0x17

    const/16 v6, 0x13

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Liex;->a:Liev;

    invoke-virtual {v0, v4, v1, v1}, Liev;->a(IIZ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Liex;->a:Liev;

    invoke-virtual {v2, v4, v0, v1}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_2
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, p0, Liex;->a:Liev;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_3
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v2, p0, Liex;->a:Liev;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_4
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->c:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0x8

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_5
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_6
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->e:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xa

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_7
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->f:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xb

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto :goto_0

    :cond_8
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->g:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_9
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->h:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xd

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_a
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->i:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xe

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_b
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->j:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0xf

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_c
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->k:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0x10

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_d
    iget-object v3, p0, Liex;->a:Liev;

    iget-object v3, v3, Liev;->l:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0x11

    invoke-virtual {v2, v3, v1, v0}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_e
    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Liex;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    new-array v0, v0, [Lhuv;

    new-instance v5, Lifr;

    invoke-static {v2, v3, v4}, Lifr;->a(JLjava/util/List;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v5, v2, v3, v4}, Lifr;-><init>(JLjava/util/ArrayList;)V

    aput-object v5, v0, v1

    iget-object v2, p0, Liex;->a:Liev;

    invoke-virtual {v2, v6, v0, v1}, Liev;->a(ILjava/lang/Object;Z)Z

    goto/16 :goto_0

    :cond_f
    const-string v3, "android.net.wifi.BATCHED_RESULTS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    iget-object v0, p0, Liex;->b:Landroid/net/wifi/WifiManager;

    invoke-static {v0}, Lifr;->a(Landroid/net/wifi/WifiManager;)[Lhuv;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Liex;->a:Liev;

    invoke-virtual {v2, v6, v0, v1}, Liev;->a(ILjava/lang/Object;Z)Z

    goto/16 :goto_0

    :cond_10
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    const-string v2, "wifi_state"

    const/4 v3, 0x4

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_11

    if-ne v2, v0, :cond_0

    :cond_11
    iget-object v2, p0, Liex;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-nez v2, :cond_12

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v3

    iget-object v4, p0, Liex;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v4, p1}, Lifs;->a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_12
    move v3, v0

    :goto_1
    if-eqz v2, :cond_14

    const/4 v2, 0x2

    :goto_2
    if-eqz v3, :cond_15

    :goto_3
    or-int/2addr v0, v2

    iget-object v2, p0, Liex;->a:Liev;

    const/16 v3, 0x14

    invoke-virtual {v2, v3, v0, v1}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_13
    move v3, v1

    goto :goto_1

    :cond_14
    move v2, v1

    goto :goto_2

    :cond_15
    move v0, v1

    goto :goto_3

    :cond_16
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    iget-object v0, p0, Liex;->a:Liev;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const/16 v3, 0x15

    invoke-virtual {v0, v3, v2, v1}, Liev;->a(ILjava/lang/Object;Z)Z

    goto/16 :goto_0

    :cond_17
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    iget-object v2, p0, Liex;->a:Liev;

    invoke-static {p1}, Liev;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_18

    :goto_4
    const/16 v3, 0x19

    invoke-virtual {v2, v3, v0, v1}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_18
    move v0, v1

    goto :goto_4

    :cond_19
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Liex;->a:Liev;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2, v1, v1}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :cond_1a
    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    invoke-virtual {v0, v2}, Lifs;->a(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocationCallbackRunner"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected action "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, Liex;->a:Liev;

    const/16 v2, 0x27

    invoke-virtual {v0, v2, v1, v1}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Liex;->a:Liev;

    const/16 v2, 0x26

    invoke-virtual {v0, v2, v1, v1}, Liev;->a(IIZ)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
