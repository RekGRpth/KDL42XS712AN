.class final Lbup;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field final b:Lorg/apache/http/HttpResponse;

.field final c:Ljava/io/IOException;

.field d:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbup;->d:Z

    iput-object p1, p0, Lbup;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    iput-object p2, p0, Lbup;->b:Lorg/apache/http/HttpResponse;

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    iput-object v0, p0, Lbup;->c:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method protected final finalize()V
    .locals 3

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    iget-boolean v0, p0, Lbup;->d:Z

    if-nez v0, :cond_0

    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    const-string v2, "HttpIsser request leak."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbup;->c:Ljava/io/IOException;

    invoke-virtual {v1, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "HttpIssuer request NOT closed."

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
