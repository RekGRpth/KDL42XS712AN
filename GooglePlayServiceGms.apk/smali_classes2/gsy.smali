.class public final Lgsy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgta;

.field private final b:Lgsz;

.field private final c:Lgtb;


# direct methods
.method public constructor <init>(Lgsz;Lgta;Lgtb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgsy;->b:Lgsz;

    iput-object p2, p0, Lgsy;->a:Lgta;

    iput-object p3, p0, Lgsy;->c:Lgtb;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lgsy;

    if-eqz v1, :cond_0

    check-cast p1, Lgsy;

    iget-object v1, p0, Lgsy;->b:Lgsz;

    iget-object v2, p1, Lgsy;->b:Lgsz;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lgsy;->a:Lgta;

    iget-object v2, p1, Lgsy;->a:Lgta;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lgsy;->c:Lgtb;

    iget-object v2, p1, Lgsy;->c:Lgtb;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "google/payments/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lgsy;->b:Lgsz;

    invoke-virtual {v1}, Lgsz;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgsy;->a:Lgta;

    invoke-virtual {v1}, Lgta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgsy;->c:Lgtb;

    invoke-virtual {v1}, Lgtb;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
