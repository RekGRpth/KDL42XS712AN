.class public final Lcft;
.super Lcfl;
.source "SourceFile"


# instance fields
.field final a:J

.field private final b:Lcfp;


# direct methods
.method public constructor <init>(Lcdu;Lcfp;J)V
    .locals 2

    invoke-static {}, Lced;->a()Lced;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    iput-object v0, p0, Lcft;->b:Lcfp;

    iput-wide p3, p0, Lcft;->a:J

    invoke-virtual {p2}, Lcfp;->m()Z

    move-result v0

    const-string v1, "entry must be saved to database before creating authorized apps"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    return-void
.end method

.method public static a(Lcdu;Lcfp;Landroid/database/Cursor;)Lcft;
    .locals 4

    sget-object v0, Lcee;->a:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p1, Lcfl;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    sget-object v0, Lcee;->b:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    new-instance v2, Lcft;

    invoke-direct {v2, p0, p1, v0, v1}, Lcft;-><init>(Lcdu;Lcfp;J)V

    invoke-static {}, Lced;->a()Lced;

    move-result-object v0

    invoke-virtual {v0}, Lced;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcft;->d(J)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcee;->a:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcft;->b:Lcfp;

    iget-wide v1, v1, Lcfl;->f:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcee;->b:Lcee;

    invoke-virtual {v0}, Lcee;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcft;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "EntryAuthorizedApp [entry=%s, authorizedPackagingId=%d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcft;->b:Lcfp;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcft;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
