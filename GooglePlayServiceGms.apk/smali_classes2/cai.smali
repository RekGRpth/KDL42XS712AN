.class public Lcai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static h:I

.field private static final i:[I

.field private static final j:[I


# instance fields
.field private A:Z

.field private final B:Landroid/view/View;

.field private final C:Lcaj;

.field a:Landroid/graphics/drawable/Drawable;

.field b:Landroid/graphics/drawable/Drawable;

.field c:I

.field d:Landroid/graphics/RectF;

.field e:I

.field f:Ljava/lang/String;

.field g:I

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:I

.field private m:I

.field private final n:Landroid/widget/AbsListView;

.field private o:Z

.field private p:I

.field private q:Landroid/graphics/Paint;

.field private r:I

.field private s:I

.field private t:Z

.field private u:[Ljava/lang/Object;

.field private v:Z

.field private w:Lcak;

.field private final x:Landroid/os/Handler;

.field private y:Landroid/widget/ListAdapter;

.field private z:Landroid/widget/SectionIndexer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x4

    sput v0, Lcai;->h:I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a7    # android.R.attr.state_pressed

    aput v1, v0, v2

    sput-object v0, Lcai;->i:[I

    new-array v0, v2, [I

    sput-object v0, Lcai;->j:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcaj;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcai;->s:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcai;->x:Landroid/os/Handler;

    iput-object p2, p0, Lcai;->n:Landroid/widget/AbsListView;

    iput-object p3, p0, Lcai;->B:Landroid/view/View;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaj;

    iput-object v0, p0, Lcai;->C:Lcaj;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const v2, 0x7f0200cc    # com.google.android.gms.R.drawable.drive_scrollbar_handle_accelerated_anim2

    invoke-virtual {v1, v5, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0005    # com.google.android.gms.R.dimen.drive_fastscroll_thumb_width

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcai;->l:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0006    # com.google.android.gms.R.dimen.drive_fastscroll_thumb_height

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcai;->c:I

    iput-boolean v4, p0, Lcai;->A:Z

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    const v1, 0x7f0200cb    # com.google.android.gms.R.drawable.drive_menu_submenu_background

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcai;->b:Landroid/graphics/drawable/Drawable;

    iput-boolean v4, p0, Lcai;->o:Z

    invoke-virtual {p0}, Lcai;->d()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004    # com.google.android.gms.R.dimen.drive_fastscroll_overlay_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcai;->e:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcai;->d:Landroid/graphics/RectF;

    new-instance v0, Lcak;

    invoke-direct {v0, p0}, Lcak;-><init>(Lcai;)V

    iput-object v0, p0, Lcai;->w:Lcak;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    iget-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    iget v1, p0, Lcai;->e:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v4, [I

    const v2, 0x1010036    # android.R.attr.textColorPrimary

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget-object v1, p0, Lcai;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iput v5, p0, Lcai;->g:I

    invoke-direct {p0}, Lcai;->h()V

    return-void

    nop

    :array_0
    .array-data 4
        0x1010336    # android.R.attr.fastScrollThumbDrawable
        0x1010339    # android.R.attr.fastScrollTrackDrawable
    .end array-data
.end method

.method static synthetic a(Lcai;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcai;->B:Landroid/view/View;

    return-object v0
.end method

.method private a(FF)Z
    .locals 2

    invoke-direct {p0}, Lcai;->f()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcai;->e()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, Lcai;->m:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcai;->m:I

    iget v1, p0, Lcai;->c:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()I
    .locals 1

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcai;->b(I)I

    move-result v0

    return v0
.end method

.method private f()I
    .locals 1

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcai;->c(I)I

    move-result v0

    return v0
.end method

.method private g()V
    .locals 5

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    iget-object v0, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcai;->f()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcai;->e()I

    move-result v3

    iget v4, p0, Lcai;->c:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lcai;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    sget-object v0, Lcai;->i:[I

    :goto_0
    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    iget-object v1, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcai;->j:[I

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcai;->g:I

    return v0
.end method

.method public a(I)V
    .locals 6

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iput p1, p0, Lcai;->g:I

    invoke-direct {p0}, Lcai;->h()V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcai;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcai;->w:Lcak;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcai;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcai;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcai;->g()V

    :cond_0
    :pswitch_3
    iget-object v0, p0, Lcai;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcai;->w:Lcak;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcai;->B:Landroid/view/View;

    invoke-direct {p0}, Lcai;->f()I

    move-result v1

    iget v2, p0, Lcai;->m:I

    invoke-direct {p0}, Lcai;->e()I

    move-result v3

    iget v4, p0, Lcai;->m:I

    iget v5, p0, Lcai;->c:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v2, 0x0

    iget v0, p0, Lcai;->g:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcai;->m:I

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcai;->w:Lcak;

    const/4 v1, -0x1

    iget v5, p0, Lcai;->g:I

    if-ne v5, v9, :cond_7

    invoke-virtual {v3}, Lcak;->a()I

    move-result v3

    const/16 v1, 0x68

    if-ge v3, v1, :cond_2

    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    mul-int/lit8 v5, v3, 0x2

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_2
    iget-object v1, p0, Lcai;->C:Lcaj;

    sget-object v5, Lcaj;->a:Lcaj;

    invoke-virtual {v1, v5}, Lcaj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v0, p0, Lcai;->l:I

    mul-int/2addr v0, v3

    div-int/lit16 v0, v0, 0xd0

    move v1, v2

    :goto_1
    iget-object v5, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Lcai;->c:I

    invoke-virtual {v5, v1, v2, v0, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcai;->A:Z

    move v0, v3

    :goto_2
    iget-object v1, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v5, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    iget v6, p0, Lcai;->l:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    div-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    iget-object v6, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    add-int/2addr v5, v3

    iget-object v7, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v1

    invoke-virtual {v6, v3, v1, v5, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcai;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    int-to-float v1, v4

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, v4

    int-to-float v1, v1

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v1, p0, Lcai;->g:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lcai;->v:Z

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcai;->f:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcai;->a(Landroid/graphics/Canvas;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget v1, p0, Lcai;->l:I

    mul-int/2addr v1, v3

    div-int/lit16 v1, v1, 0xd0

    sub-int v1, v0, v1

    goto :goto_1

    :cond_5
    iget v1, p0, Lcai;->g:I

    if-ne v1, v9, :cond_0

    if-nez v0, :cond_6

    invoke-virtual {p0, v2}, Lcai;->a(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcai;->B:Landroid/view/View;

    invoke-direct {p0}, Lcai;->f()I

    move-result v1

    invoke-direct {p0}, Lcai;->e()I

    move-result v2

    iget v3, p0, Lcai;->c:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcai;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcai;->q:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v1

    iget-object v2, p0, Lcai;->d:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v4

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    iget v4, p0, Lcai;->e:I

    div-int/lit8 v4, v4, 0x4

    add-int/2addr v2, v4

    int-to-float v2, v2

    sub-float v1, v2, v1

    invoke-virtual {p1, p2, v3, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method a(Landroid/widget/AbsListView;III)V
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcai;->s:I

    if-eq v0, p4, :cond_0

    if-lez p3, :cond_0

    iput p4, p0, Lcai;->s:I

    iget v0, p0, Lcai;->s:I

    div-int/2addr v0, p3

    sget v3, Lcai;->h:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcai;->t:Z

    :cond_0
    iget-boolean v0, p0, Lcai;->t:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcai;->g:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lcai;->a(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    sub-int v0, p4, p3

    if-lez v0, :cond_5

    iget v0, p0, Lcai;->g:I

    if-eq v0, v5, :cond_5

    iget v0, p0, Lcai;->m:I

    iget-object v3, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getHeight()I

    move-result v3

    iget v4, p0, Lcai;->c:I

    sub-int/2addr v3, v4

    mul-int/2addr v3, p2

    sub-int v4, p4, p3

    div-int/2addr v3, v4

    iput v3, p0, Lcai;->m:I

    iget-boolean v3, p0, Lcai;->A:Z

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcai;->g()V

    iput-boolean v2, p0, Lcai;->A:Z

    :cond_4
    iget v2, p0, Lcai;->m:I

    if-eq v2, v0, :cond_5

    iget-object v0, p0, Lcai;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_5
    iput-boolean v1, p0, Lcai;->o:Z

    iget v0, p0, Lcai;->p:I

    if-eq p2, v0, :cond_1

    iput p2, p0, Lcai;->p:I

    iget v0, p0, Lcai;->g:I

    if-eq v0, v5, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcai;->a(I)V

    iget-object v0, p0, Lcai;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcai;->w:Lcak;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method a(Landroid/view/MotionEvent;)Z
    .locals 2

    iget v0, p0, Lcai;->g:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcai;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcai;->a(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(I)I
    .locals 2

    sget-object v0, Lcaj;->a:Lcaj;

    iget-object v1, p0, Lcai;->C:Lcaj;

    invoke-virtual {v0, v1}, Lcaj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget p1, p0, Lcai;->l:I

    :cond_0
    return p1
.end method

.method b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcai;->a(I)V

    return-void
.end method

.method b(Landroid/view/MotionEvent;)Z
    .locals 14

    iget v0, p0, Lcai;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcai;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcai;->a(I)V

    iget-object v0, p0, Lcai;->y:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcai;->d()V

    :cond_1
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcai;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_15

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcai;->a(I)V

    iget-object v0, p0, Lcai;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcai;->w:Lcak;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcai;->w:Lcak;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_15

    iget v0, p0, Lcai;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_15

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v2, p0, Lcai;->c:I

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0xa

    if-gez v0, :cond_5

    const/4 v0, 0x0

    :cond_4
    :goto_1
    iget v2, p0, Lcai;->m:I

    sub-int/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_6

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcai;->c:I

    add-int/2addr v2, v0

    if-le v2, v1, :cond_4

    iget v0, p0, Lcai;->c:I

    sub-int v0, v1, v0

    goto :goto_1

    :cond_6
    iput v0, p0, Lcai;->m:I

    iget-boolean v0, p0, Lcai;->o:Z

    if-eqz v0, :cond_12

    iget v0, p0, Lcai;->m:I

    int-to-float v0, v0

    iget v2, p0, Lcai;->c:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float v8, v0, v1

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcai;->o:Z

    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v7

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000    # 8.0f

    div-float v9, v0, v1

    iget-object v10, p0, Lcai;->u:[Ljava/lang/Object;

    if-eqz v10, :cond_d

    array-length v0, v10

    const/4 v1, 0x1

    if-le v0, v1, :cond_d

    array-length v11, v10

    int-to-float v0, v11

    mul-float/2addr v0, v8

    float-to-int v0, v0

    if-lt v0, v11, :cond_7

    add-int/lit8 v0, v11, -0x1

    :cond_7
    iget-object v1, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v1, v0}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v1

    add-int/lit8 v5, v0, 0x1

    add-int/lit8 v2, v11, -0x1

    if-ge v0, v2, :cond_19

    iget-object v2, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v2, v5}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v2

    move v6, v2

    :goto_2
    if-ne v6, v1, :cond_18

    move v2, v1

    move v3, v0

    :goto_3
    if-lez v3, :cond_17

    add-int/lit8 v2, v3, -0x1

    iget-object v3, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v3, v2}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v3

    if-eq v3, v1, :cond_9

    move v1, v2

    move v13, v3

    move v3, v2

    move v2, v13

    :goto_4
    if-nez v3, :cond_8

    iget-object v3, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v3

    :cond_8
    :goto_5
    add-int/lit8 v4, v5, 0x1

    :goto_6
    if-ge v4, v11, :cond_a

    iget-object v12, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v12, v4}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v12

    if-ne v12, v6, :cond_a

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_9
    if-nez v2, :cond_16

    const/4 v2, 0x0

    move v1, v0

    move v13, v3

    move v3, v2

    move v2, v13

    goto :goto_4

    :cond_a
    int-to-float v4, v1

    int-to-float v12, v11

    div-float/2addr v4, v12

    int-to-float v5, v5

    int-to-float v11, v11

    div-float/2addr v5, v11

    if-ne v1, v0, :cond_c

    sub-float v0, v8, v4

    cmpg-float v0, v0, v9

    if-gez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/lit8 v1, v7, -0x1

    if-le v0, v1, :cond_b

    add-int/lit8 v0, v7, -0x1

    :cond_b
    move v1, v0

    :goto_8
    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ExpandableListView;

    iget v2, p0, Lcai;->r:I

    add-int/2addr v1, v2

    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    :goto_9
    if-ltz v3, :cond_14

    if-nez v10, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sectionIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for null sections. This should be impossible."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_c
    sub-int v0, v6, v2

    int-to-float v0, v0

    sub-float v1, v8, v4

    mul-float/2addr v0, v1

    sub-float v1, v5, v4

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    goto :goto_7

    :cond_d
    int-to-float v0, v7

    mul-float/2addr v0, v8

    float-to-int v0, v0

    const/4 v3, -0x1

    move v1, v0

    goto :goto_8

    :cond_e
    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v2, p0, Lcai;->r:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_9

    :cond_f
    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    iget v2, p0, Lcai;->r:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_9

    :cond_10
    aget-object v0, v10, v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcai;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_11

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_13

    :cond_11
    array-length v0, v10

    if-ge v3, v0, :cond_13

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Lcai;->v:Z

    :cond_12
    :goto_b
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_13
    const/4 v0, 0x0

    goto :goto_a

    :cond_14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcai;->v:Z

    goto :goto_b

    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_16
    move v13, v3

    move v3, v2

    move v2, v13

    goto/16 :goto_3

    :cond_17
    move v1, v0

    move v3, v0

    goto/16 :goto_4

    :cond_18
    move v2, v1

    move v3, v0

    move v1, v0

    goto/16 :goto_5

    :cond_19
    move v6, v7

    goto/16 :goto_2
.end method

.method final c(I)I
    .locals 2

    sget-object v0, Lcaj;->a:Lcaj;

    iget-object v1, p0, Lcai;->C:Lcaj;

    invoke-virtual {v0, v1}, Lcaj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcai;->l:I

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    iget v0, p0, Lcai;->g:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()V
    .locals 3

    iget-object v0, p0, Lcai;->n:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    iput-object v1, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v1

    iput v1, p0, Lcai;->r:I

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcai;->y:Landroid/widget/ListAdapter;

    instance-of v1, v0, Landroid/widget/SectionIndexer;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/widget/SectionIndexer;

    iput-object v0, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    iget-object v0, p0, Lcai;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcai;->u:[Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    iput-object v0, p0, Lcai;->u:[Ljava/lang/Object;

    goto :goto_0
.end method
