.class Ljy;
.super Ljj;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field final b:Landroid/os/Handler;

.field c:Z

.field private d:Landroid/content/Context;

.field private e:Ljp;

.field private f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private g:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private h:Landroid/view/ViewGroup;

.field private i:Landroid/support/v7/internal/widget/ActionBarView;

.field private j:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private k:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

.field private m:Ljava/util/ArrayList;

.field private n:Ljz;

.field private o:I

.field private p:Z

.field private q:Ljava/util/ArrayList;

.field private r:I

.field private s:Z

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Ljk;


# direct methods
.method public constructor <init>(Ljp;Ljk;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljj;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljy;->m:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Ljy;->o:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljy;->q:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ljy;->b:Landroid/os/Handler;

    iput v2, p0, Ljy;->t:I

    iput-boolean v1, p0, Ljy;->w:Z

    iput-object p1, p0, Ljy;->e:Ljp;

    iput-object p1, p0, Ljy;->a:Landroid/content/Context;

    iput-object p2, p0, Ljy;->y:Ljk;

    iget-object v3, p0, Ljy;->e:Ljp;

    sget v0, Lkl;->d:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Ljy;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Ljy;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljy;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Ljj;)V

    :cond_0
    sget v0, Lkl;->a:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarView;

    iput-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    sget v0, Lkl;->h:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Ljy;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    sget v0, Lkl;->c:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    sget v0, Lkl;->z:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ljy;->h:Landroid/view/ViewGroup;

    iget-object v0, p0, Ljy;->h:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    iget-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Ljy;->h:Landroid/view/ViewGroup;

    :cond_1
    sget v0, Lkl;->w:I

    invoke-virtual {v3, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljy;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Ljy;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ActionBarContextView;)V

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    iput v0, p0, Ljy;->r:I

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->p()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v3, v1

    :goto_1
    if-eqz v3, :cond_4

    iput-boolean v1, p0, Ljy;->p:Z

    :cond_4
    iget-object v0, p0, Ljy;->a:Landroid/content/Context;

    invoke-static {v0}, Lkr;->a(Landroid/content/Context;)Lkr;

    move-result-object v4

    iget-object v0, v4, Lkr;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v5, 0xe

    if-ge v0, v5, :cond_9

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    if-eqz v3, :cond_6

    :cond_5
    move v2, v1

    :cond_6
    invoke-virtual {p0, v2}, Ljy;->b(Z)V

    invoke-virtual {v4}, Lkr;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Ljy;->c(Z)V

    iget-object v0, p0, Ljy;->e:Ljp;

    invoke-virtual {v0}, Ljp;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljy;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v3, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->n()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    invoke-virtual {p0}, Ljy;->d()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(II)V
    .locals 4

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->p()I

    move-result v0

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Ljy;->p:Z

    :cond_0
    iget-object v1, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->b(I)V

    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Landroid/view/View;Ljl;)V
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Ljm;)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p2}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ljm;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljn;)V
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljn;->a()I

    move-result v0

    :cond_0
    iput v0, p0, Ljy;->o:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Ljy;->e:Ljp;

    invoke-virtual {v1}, Ljp;->K_()Lu;

    move-result-object v1

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1}, Lag;->b()Lag;

    move-result-object v1

    iget-object v2, p0, Ljy;->n:Ljz;

    if-ne v2, p1, :cond_4

    iget-object v0, p0, Ljy;->n:Ljz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljy;->n:Ljz;

    iget-object v0, v0, Ljz;->a:Ljo;

    iget-object v0, p0, Ljy;->n:Ljz;

    iget-object v0, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Ljn;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->b(I)V

    :cond_3
    :goto_1
    invoke-virtual {v1}, Lag;->f()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lag;->c()I

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljn;->a()I

    move-result v0

    :cond_5
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->a(I)V

    iget-object v0, p0, Ljy;->n:Ljz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljy;->n:Ljz;

    iget-object v0, v0, Ljz;->a:Ljo;

    iget-object v0, p0, Ljy;->n:Ljz;

    :cond_6
    check-cast p1, Ljz;

    iput-object p1, p0, Ljy;->n:Ljz;

    iget-object v0, p0, Ljy;->n:Ljz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljy;->n:Ljz;

    iget-object v0, v0, Ljz;->a:Ljo;

    iget-object v0, p0, Ljy;->n:Ljz;

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    const/4 v1, 0x4

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Ljy;->a(II)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->p()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(I)V

    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(Z)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-boolean v0, p0, Ljy;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljy;->u:Z

    invoke-virtual {p0}, Ljy;->e()V

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Ljy;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljn;

    invoke-virtual {p0, v0}, Ljy;->a(Ljn;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->e(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method final c(Z)V
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-boolean p1, p0, Ljy;->s:Z

    iget-boolean v0, p0, Ljy;->s:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    iget-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    :goto_0
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v3, :cond_0

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    :cond_0
    :goto_2
    iget-object v3, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-boolean v4, p0, Ljy;->s:Z

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarView;->d(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public final d()Landroid/content/Context;
    .locals 4

    iget-object v0, p0, Ljy;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Ljy;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lki;->d:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_1

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Ljy;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Ljy;->d:Landroid/content/Context;

    :cond_0
    :goto_0
    iget-object v0, p0, Ljy;->d:Landroid/content/Context;

    return-object v0

    :cond_1
    iget-object v0, p0, Ljy;->a:Landroid/content/Context;

    iput-object v0, p0, Ljy;->d:Landroid/content/Context;

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Ljy;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljy;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final d(Z)V
    .locals 1

    iput-boolean p1, p0, Ljy;->x:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->clearAnimation()V

    iget-object v0, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->clearAnimation()V

    :cond_0
    return-void
.end method

.method final e()V
    .locals 6

    const/16 v5, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Ljy;->u:Z

    iget-boolean v3, p0, Ljy;->v:Z

    iget-boolean v4, p0, Ljy;->c:Z

    if-nez v4, :cond_4

    if-nez v2, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_6

    iget-boolean v2, p0, Ljy;->w:Z

    if-nez v2, :cond_3

    iput-boolean v0, p0, Ljy;->w:Z

    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->clearAnimation()V

    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Ljy;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    if-eqz v0, :cond_1

    iget-object v2, p0, Ljy;->a:Landroid/content/Context;

    sget v3, Lkh;->b:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iget-object v3, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljy;->a:Landroid/content/Context;

    sget v2, Lkh;->a:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v2, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    iget-object v0, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v2, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    iget-boolean v2, p0, Ljy;->w:Z

    if-eqz v2, :cond_3

    iput-boolean v1, p0, Ljy;->w:Z

    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->clearAnimation()V

    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_3

    invoke-virtual {p0}, Ljy;->f()Z

    move-result v2

    if-eqz v2, :cond_9

    :goto_3
    if-eqz v0, :cond_7

    iget-object v1, p0, Ljy;->a:Landroid/content/Context;

    sget v2, Lkh;->d:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iget-object v2, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_7
    iget-object v1, p0, Ljy;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_3

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljy;->a:Landroid/content/Context;

    sget v1, Lkh;->c:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_8
    iget-object v0, p0, Ljy;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3
.end method

.method public final e(I)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, -0x1

    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->d(I)V

    packed-switch p1, :pswitch_data_1

    :cond_0
    :goto_1
    iget-object v1, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    if-ne p1, v4, :cond_5

    iget-boolean v0, p0, Ljy;->s:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->d(Z)V

    return-void

    :pswitch_0
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    move v0, v1

    :goto_3
    iput v0, p0, Ljy;->o:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljy;->a(Ljn;)V

    iget-object v0, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Ljy;->n:Ljz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljy;->n:Ljz;

    iget v0, v0, Ljz;->b:I

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_3

    :pswitch_2
    iget-object v0, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->m()I

    move-result v0

    goto :goto_3

    :pswitch_3
    iget-object v0, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-nez v0, :cond_2

    new-instance v0, Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    iget-object v3, p0, Ljy;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    iget-boolean v3, p0, Ljy;->s:Z

    if-eqz v3, :cond_3

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    iget-object v3, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    :goto_4
    iput-object v0, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    :cond_2
    iget-object v0, p0, Ljy;->l:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    iget v0, p0, Ljy;->o:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Ljy;->o:I

    invoke-virtual {p0, v0}, Ljy;->c(I)V

    iput v1, p0, Ljy;->o:I

    goto :goto_1

    :cond_3
    iget-object v3, p0, Ljy;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v3

    if-ne v3, v4, :cond_4

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    :goto_5
    iget-object v3, p0, Ljy;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_5

    :cond_5
    move v0, v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method f()Z
    .locals 1

    iget-boolean v0, p0, Ljy;->x:Z

    return v0
.end method
