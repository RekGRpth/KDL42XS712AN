.class public final enum Lhql;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhql;

.field public static final enum b:Lhql;

.field public static final enum c:Lhql;

.field private static final synthetic d:[Lhql;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhql;

    const-string v1, "MEMORY"

    invoke-direct {v0, v1, v2}, Lhql;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhql;->a:Lhql;

    new-instance v0, Lhql;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v3}, Lhql;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhql;->b:Lhql;

    new-instance v0, Lhql;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v4}, Lhql;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhql;->c:Lhql;

    const/4 v0, 0x3

    new-array v0, v0, [Lhql;

    sget-object v1, Lhql;->a:Lhql;

    aput-object v1, v0, v2

    sget-object v1, Lhql;->b:Lhql;

    aput-object v1, v0, v3

    sget-object v1, Lhql;->c:Lhql;

    aput-object v1, v0, v4

    sput-object v0, Lhql;->d:[Lhql;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhql;
    .locals 1

    const-class v0, Lhql;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhql;

    return-object v0
.end method

.method public static values()[Lhql;
    .locals 1

    sget-object v0, Lhql;->d:[Lhql;

    invoke-virtual {v0}, [Lhql;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhql;

    return-object v0
.end method
