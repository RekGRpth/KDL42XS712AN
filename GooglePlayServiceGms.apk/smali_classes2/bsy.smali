.class public final Lbsy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcdu;

.field private final b:Lcfz;

.field private final c:Lcon;

.field private final d:Lbst;

.field private final e:Ljava/lang/String;

.field private final f:Landroid/os/ParcelFileDescriptor;

.field private volatile g:Ljava/lang/String;

.field private volatile h:Z

.field private volatile i:Z

.field private j:Z

.field private k:Lbsx;


# direct methods
.method constructor <init>(Lcdu;Lcfz;Lcon;Lbst;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbsy;->j:Z

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lbsy;->a:Lcdu;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbsy;->b:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcon;

    iput-object v0, p0, Lbsy;->c:Lcon;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbst;

    iput-object v0, p0, Lbsy;->d:Lbst;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsy;->e:Ljava/lang/String;

    invoke-direct {p0}, Lbsy;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p6}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lbsy;->f:Landroid/os/ParcelFileDescriptor;

    return-void
.end method

.method private d()Ljava/io/File;
    .locals 3

    iget-object v0, p0, Lbsy;->d:Lbst;

    iget-object v1, p0, Lbsy;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized e()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbsy;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbsy;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbsy;->k:Lbsx;

    if-nez v0, :cond_2

    :cond_0
    invoke-direct {p0}, Lbsy;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcbt;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsy;->g:Ljava/lang/String;

    :cond_1
    :goto_0
    iget-object v0, p0, Lbsy;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lbsy;->k:Lbsx;

    invoke-virtual {v0}, Lbsx;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsy;->g:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/os/ParcelFileDescriptor;
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lbsy;->h:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lbsy;->i:Z

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "Cannot get the file descriptor for committed or abandoned content."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsy;->j:Z

    iget-object v0, p0, Lbsy;->f:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lbsu;)Ljava/lang/Object;
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbsy;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content has already been committed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lbsy;->i:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot commit content that has already been abandoned."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsy;->h:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbsy;->f:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    invoke-direct {p0}, Lbsy;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lbsy;->b:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    :try_start_2
    iget-object v0, p0, Lbsy;->b:Lcfz;

    invoke-interface {v0, v1}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lbsy;->c:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v3

    iput-wide v3, v2, Lcfx;->g:J

    iget-object v0, v2, Lcfx;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbsy;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcfx;->a(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Lcfx;->k()V

    :goto_1
    invoke-interface {p1, v1}, Lbsu;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lbsy;->b:Lcfz;

    iget-object v2, p0, Lbsy;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcfz;->f(Ljava/lang/String;)V

    iget-object v1, p0, Lbsy;->b:Lcfz;

    invoke-interface {v1}, Lcfz;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v1, p0, Lbsy;->b:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    return-object v0

    :cond_2
    :try_start_3
    iget-object v3, p0, Lbsy;->d:Lbst;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-eqz v3, :cond_3

    :try_start_4
    invoke-direct {p0}, Lbsy;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    const-string v3, "PendingContent"

    const-string v4, "Unable to delete redundant content; will be garbage collected later: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lbsy;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v3, v0, v4, v5}, Lcbv;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lbsy;->b:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_3
    :try_start_6
    const-string v3, "PendingContent"

    const-string v4, "Content file %s was deleted outside of the content manager; using identical new file %s instead."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-object v6, p0, Lbsy;->e:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-static {v3, v4, v5}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lbsy;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcfx;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lbsy;->a:Lcdu;

    iget-object v2, p0, Lbsy;->c:Lcon;

    invoke-interface {v2}, Lcon;->a()J

    move-result-wide v2

    invoke-direct {p0}, Lbsy;->d()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcfx;->a(Lcdu;Ljava/lang/String;JJ)Lcfy;

    move-result-object v0

    iget-object v2, p0, Lbsy;->e:Ljava/lang/String;

    iput-object v2, v0, Lcfy;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcfy;->a()Lcfx;

    move-result-object v0

    invoke-virtual {v0}, Lcfx;->k()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1
.end method

.method public final declared-synchronized b()Lbsx;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbsy;->h:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbsy;->i:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot get an OutputStream for committed or abandoned content."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbsy;->k:Lbsx;

    if-nez v0, :cond_0

    new-instance v0, Lbsx;

    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    iget-object v2, p0, Lbsy;->f:Landroid/os/ParcelFileDescriptor;

    invoke-direct {v1, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {v0, v1}, Lbsx;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lbsy;->k:Lbsx;

    :cond_0
    iget-object v0, p0, Lbsy;->k:Lbsx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbsy;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbsy;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsy;->i:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lbsy;->f:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget-object v0, p0, Lbsy;->b:Lcfz;

    iget-object v1, p0, Lbsy;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcfz;->f(Ljava/lang/String;)V

    :try_start_2
    invoke-direct {p0}, Lbsy;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PendingContent"

    const-string v2, "Unable to delete abandoned content; will be garbage collected later: %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lbsy;->e:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "PendingContent"

    const-string v2, "Ignored IOException while closing abandoned content: %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lbsy;->e:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
