.class public final Libt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Libp;


# instance fields
.field public final a:Lhoc;

.field public b:Z

.field private final c:Lidu;

.field private final d:Licm;

.field private final e:Libq;

.field private f:Z

.field private g:Lhub;


# direct methods
.method public constructor <init>(Lidu;Lhoc;Licm;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Libq;

    invoke-direct {v0}, Libq;-><init>()V

    iput-object v0, p0, Libt;->e:Libq;

    iput-boolean v1, p0, Libt;->f:Z

    const/4 v0, 0x0

    iput-object v0, p0, Libt;->g:Lhub;

    iput-boolean v1, p0, Libt;->b:Z

    iput-object p1, p0, Libt;->c:Lidu;

    iput-object p2, p0, Libt;->a:Lhoc;

    iput-object p3, p0, Libt;->d:Licm;

    return-void
.end method

.method private declared-synchronized a(Lhub;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Libt;->g:Lhub;

    if-eqz v0, :cond_1

    iget-object v0, p0, Libt;->g:Lhub;

    invoke-virtual {v0, p1}, Lhub;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "ModelStateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Queue "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lhub;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Libt;->e:Libq;

    sget-object v1, Libr;->a:[I

    iget-object v2, p1, Lhub;->a:Lhuc;

    invoke-virtual {v2}, Lhuc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "ModelRequestQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Programming error: unknown indoor request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lhub;->a:Lhuc;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_0
    :try_start_2
    iget-object v0, v0, Libq;->a:Libs;

    invoke-virtual {v0, p1}, Libs;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Libq;->b:Libs;

    invoke-virtual {v0, p1}, Libs;->a(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lhtr;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Libt;->a:Lhoc;

    iget-object v0, v0, Lhoc;->b:Lhnt;

    invoke-virtual {v0, p1}, Lhnt;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Libt;->a:Lhoc;

    iget-object v2, p0, Libt;->d:Licm;

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    iget-object v0, v0, Lhoc;->b:Lhnt;

    invoke-virtual {v0, p1, v2, v3}, Lhnt;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtr;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Libt;->b:Z

    if-eqz v0, :cond_2

    new-instance v0, Lhub;

    sget-object v2, Lhuc;->a:Lhuc;

    invoke-direct {v0, v2, p1}, Lhub;-><init>(Lhuc;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Libt;->a(Lhub;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/List;
    .locals 8

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Libt;->a:Lhoc;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Libt;->d:Licm;

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v6

    iget-object v0, v3, Lhoc;->a:Lhnz;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3, v6, v7}, Lhnz;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public final declared-synchronized a()V
    .locals 8

    const/4 v1, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Libt;->f:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Libt;->e:Libq;

    iget-object v4, v3, Libq;->b:Libs;

    invoke-virtual {v4}, Libs;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v3, v3, Libq;->a:Libs;

    invoke-virtual {v3}, Libs;->b()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    if-eqz v0, :cond_6

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "ModelStateManager"

    const-string v2, "Sending GLS model request"

    invoke-static {v0, v2}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Libt;->f:Z

    iget-object v0, p0, Libt;->e:Libq;

    iget-object v2, v0, Libq;->b:Libs;

    invoke-virtual {v2}, Libs;->b()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, v0, Libq;->b:Libs;

    invoke-virtual {v0}, Libs;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhub;

    :goto_0
    iput-object v0, p0, Libt;->g:Lhub;

    iget-object v2, p0, Libt;->c:Lidu;

    iget-object v3, p0, Libt;->g:Lhub;

    new-instance v4, Livi;

    sget-object v0, Lihj;->I:Livk;

    invoke-direct {v4, v0}, Livi;-><init>(Livk;)V

    const/4 v5, 0x1

    iget-object v0, v3, Lhub;->a:Lhuc;

    sget-object v6, Libu;->a:[I

    invoke-virtual {v0}, Lhuc;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Programming error: unsupported model type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_1
    iget-object v2, v0, Libq;->a:Libs;

    invoke-virtual {v2}, Libs;->b()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v0, v0, Libq;->a:Libs;

    invoke-virtual {v0}, Libs;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhub;

    goto :goto_0

    :cond_4
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_5

    const-string v0, "ModelRequestQueue"

    const-string v2, "Programming error: trying to get next request when there\'s none"

    invoke-static {v0, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x3

    :goto_1
    invoke-virtual {v4, v5, v0}, Livi;->e(II)Livi;

    const/4 v0, 0x2

    iget-object v1, v3, Lhub;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Livi;->b(ILjava/lang/String;)Livi;

    new-instance v0, Livi;

    sget-object v1, Lihj;->F:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, Livi;->a(ILivi;)V

    const/16 v1, 0xa

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Livi;->e(II)Livi;

    new-instance v1, Livi;

    sget-object v3, Lihj;->Q:Livk;

    invoke-direct {v1, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v0}, Livi;->a(ILivi;)V

    invoke-interface {v2, v1}, Lidu;->c(Livi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit p0

    return-void

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized a(Livi;J)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_0

    const-string v2, "ModelStateManager"

    const-string v3, "Received GLS model response..."

    invoke-static {v2, v3}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Libt;->a:Lhoc;

    iget-object v4, p0, Libt;->g:Lhub;

    if-eqz v4, :cond_1

    invoke-static {p1}, Lhoc;->a(Livi;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Libt;->f:Z

    const/4 v1, 0x0

    iput-object v1, p0, Libt;->g:Lhub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_2
    const/4 v2, 0x2

    :try_start_1
    invoke-virtual {p1, v2}, Livi;->k(I)I

    move-result v5

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_b

    const/4 v6, 0x2

    invoke-virtual {p1, v6, v2}, Livi;->c(II)Livi;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Livi;->c(I)I

    move-result v7

    const/16 v8, 0x17

    if-ne v7, v8, :cond_5

    iget-object v2, v4, Lhub;->a:Lhuc;

    sget-object v5, Lhuc;->a:Lhuc;

    if-ne v2, v5, :cond_3

    iget-object v0, v3, Lhoc;->b:Lhnt;

    iget-object v2, v4, Lhub;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p2, p3}, Lhnt;->a(Ljava/lang/Object;Livi;J)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, v4, Lhub;->a:Lhuc;

    sget-object v5, Lhuc;->b:Lhuc;

    if-ne v2, v5, :cond_4

    iget-object v0, v3, Lhoc;->c:Lhnt;

    iget-object v2, v4, Lhub;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p2, p3}, Lhnt;->a(Ljava/lang/Object;Livi;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "ModelState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v4, Lhub;->a:Lhuc;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Livi;->f(I)Livi;

    move-result-object v6

    iget-object v7, v4, Lhub;->a:Lhuc;

    sget-object v8, Lhuc;->a:Lhuc;

    if-ne v7, v8, :cond_8

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Livi;->f(I)Livi;

    move-result-object v6

    if-eqz v6, :cond_7

    iget-object v7, v3, Lhoc;->b:Lhnt;

    iget-object v8, v4, Lhub;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v6, p2, p3}, Lhnt;->a(Ljava/lang/Object;Livi;J)V

    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_7
    sget-boolean v6, Licj;->d:Z

    if-eqz v6, :cond_6

    const-string v6, "ModelState"

    const-string v7, "Malformed reply does not have model cluster."

    invoke-static {v6, v7}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    iget-object v7, v4, Lhub;->a:Lhuc;

    sget-object v8, Lhuc;->b:Lhuc;

    if-ne v7, v8, :cond_a

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Livi;->f(I)Livi;

    move-result-object v6

    if-eqz v6, :cond_9

    iget-object v7, v3, Lhoc;->c:Lhnt;

    iget-object v8, v4, Lhub;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v6, p2, p3}, Lhnt;->a(Ljava/lang/Object;Livi;J)V

    goto :goto_3

    :cond_9
    sget-boolean v6, Licj;->d:Z

    if-eqz v6, :cond_6

    const-string v6, "ModelState"

    const-string v7, "Malformed reply does not have model."

    invoke-static {v6, v7}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "ModelState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v4, Lhub;->a:Lhuc;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)Lhto;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Libt;->a:Lhoc;

    iget-object v0, v0, Lhoc;->c:Lhnt;

    invoke-virtual {v0, p1}, Lhnt;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Libt;->a:Lhoc;

    iget-object v2, p0, Libt;->d:Licm;

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    iget-object v0, v0, Lhoc;->c:Lhnt;

    invoke-virtual {v0, p1, v2, v3}, Lhnt;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhto;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Libt;->b:Z

    if-eqz v0, :cond_2

    new-instance v0, Lhub;

    sget-object v2, Lhuc;->b:Lhuc;

    invoke-direct {v0, v2, p1}, Lhub;-><init>(Lhuc;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Libt;->a(Lhub;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
