.class public Lafh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lafw;


# static fields
.field private static h:Lafh;


# instance fields
.field private a:Laeq;

.field private b:Landroid/content/Context;

.field private c:Lafu;

.field private volatile d:Ljava/lang/String;

.field private volatile e:Ljava/lang/Boolean;

.field private final f:Ljava/util/Map;

.field private g:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lafh;->f:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, Lafb;->a(Landroid/content/Context;)Lafb;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lafh;-><init>(Landroid/content/Context;Laeq;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Laeq;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lafh;->f:Ljava/util/Map;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lafh;->b:Landroid/content/Context;

    iput-object p2, p0, Lafh;->a:Laeq;

    iget-object v0, p0, Lafh;->a:Laeq;

    new-instance v1, Lafi;

    invoke-direct {v1, p0}, Lafi;-><init>(Lafh;)V

    invoke-interface {v0, v1}, Laeq;->a(Lafk;)V

    iget-object v0, p0, Lafh;->a:Laeq;

    new-instance v1, Lafj;

    invoke-direct {v1, p0}, Lafj;-><init>(Lafh;)V

    invoke-interface {v0, v1}, Laeq;->a(Laer;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lafh;
    .locals 2

    const-class v1, Lafh;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lafh;->h:Lafh;

    if-nez v0, :cond_0

    new-instance v0, Lafh;

    invoke-direct {v0, p0}, Lafh;-><init>(Landroid/content/Context;)V

    sput-object v0, Lafh;->h:Lafh;

    :cond_0
    sget-object v0, Lafh;->h:Lafh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lafh;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    iput-object p1, p0, Lafh;->e:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lafh;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lafh;->d:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lafu;
    .locals 3

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lafh;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafu;

    if-nez v0, :cond_1

    new-instance v0, Lafu;

    invoke-direct {v0, p1, p0}, Lafu;-><init>(Ljava/lang/String;Lafw;)V

    iget-object v1, p0, Lafh;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lafh;->c:Lafu;

    if-nez v1, :cond_1

    iput-object v0, p0, Lafh;->c:Lafu;

    :cond_1
    invoke-static {}, Laff;->a()Laff;

    move-result-object v1

    sget-object v2, Lafg;->N:Lafg;

    invoke-virtual {v1, v2}, Laff;->a(Lafg;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hit cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    const-string v0, "language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lafx;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "screenResolution"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lafh;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lafh;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "usage"

    invoke-static {}, Laff;->a()Laff;

    move-result-object v1

    invoke-virtual {v1}, Laff;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    invoke-virtual {v0}, Laff;->b()Ljava/lang/String;

    iget-object v0, p0, Lafh;->a:Laeq;

    invoke-interface {v0, p1}, Laeq;->a(Ljava/util/Map;)V

    const-string v0, "trackingId"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lafh;->g:Ljava/lang/String;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
