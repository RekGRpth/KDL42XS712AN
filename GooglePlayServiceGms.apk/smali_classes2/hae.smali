.class final Lhae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgxh;


# instance fields
.field final synthetic a:Lhad;


# direct methods
.method constructor <init>(Lhad;)V
    .locals 0

    iput-object p1, p0, Lhae;->a:Lhad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    const/4 v6, 0x0

    iget-object v0, p0, Lhae;->a:Lhad;

    invoke-static {v0}, Lhad;->a(Lhad;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lhae;->a:Lhad;

    invoke-static {v1}, Lhad;->b(Lhad;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->h(Lhad;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhae;->a:Lhad;

    invoke-static {v3}, Lhad;->i(Lhad;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lhae;->a:Lhad;

    invoke-static {v4}, Lhad;->c(Lhad;)Z

    move-result v4

    iget-object v5, p0, Lhae;->a:Lhad;

    invoke-static {v5}, Lhad;->c(Lhad;)Z

    move-result v5

    const/4 v8, 0x0

    iget-object v7, p0, Lhae;->a:Lhad;

    invoke-static {v7}, Lhad;->d(Lhad;)Ljava/lang/String;

    move-result-object v9

    iget-object v7, p0, Lhae;->a:Lhad;

    invoke-static {v7}, Lhad;->e(Lhad;)Lioq;

    move-result-object v10

    iget-object v7, p0, Lhae;->a:Lhad;

    invoke-static {v7}, Lhad;->f(Lhad;)Ljava/util/ArrayList;

    move-result-object v11

    move-object v7, v6

    invoke-static/range {v0 .. v11}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lioq;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhae;->a:Lhad;

    const/16 v2, 0x1f5

    invoke-virtual {v1, v0, v2}, Lhad;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method public final a(Lioj;)V
    .locals 9

    iget-object v0, p0, Lhae;->a:Lhad;

    invoke-static {p1}, Lhad;->a(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhae;->a:Lhad;

    invoke-static {v0}, Lhad;->a(Lhad;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lhae;->a:Lhad;

    invoke-static {v1}, Lhad;->b(Lhad;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->c(Lhad;)Z

    move-result v3

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->c(Lhad;)Z

    move-result v4

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->d(Lhad;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->e(Lhad;)Lioq;

    move-result-object v6

    iget-object v2, p0, Lhae;->a:Lhad;

    invoke-static {v2}, Lhad;->f(Lhad;)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v8}, Lhgj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;ZZLjava/lang/String;Lioq;Ljava/util/Collection;Lipv;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhae;->a:Lhad;

    const/16 v2, 0x1f7

    invoke-virtual {v1, v0, v2}, Lhad;->a(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhae;->a:Lhad;

    iget-object v0, v0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhae;->a:Lhad;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lhad;->ag:Z

    iget-object v0, p0, Lhae;->a:Lhad;

    invoke-static {v0}, Lhad;->g(Lhad;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lioj;)V
    .locals 0

    return-void
.end method
