.class public final Lgyv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:Landroid/widget/EditText;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyv;->d:Z

    iput-object p1, p0, Lgyv;->a:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    const/16 v4, 0x20

    const/4 v3, 0x1

    iget-boolean v0, p0, Lgyv;->d:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lgyv;->d:Z

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgyv;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lgyv;->b:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lgyv;->b:I

    :cond_1
    iget v1, p0, Lgyv;->c:I

    if-lez v1, :cond_2

    iget v1, p0, Lgyv;->b:I

    if-lez v1, :cond_2

    iget v1, p0, Lgyv;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_2

    iget v1, p0, Lgyv;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lgyv;->b:I

    :cond_2
    iget v1, p0, Lgyv;->c:I

    if-gez v1, :cond_3

    iget v1, p0, Lgyv;->b:I

    if-le v1, v3, :cond_3

    iget v1, p0, Lgyv;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v4, :cond_3

    iget v0, p0, Lgyv;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgyv;->b:I

    :cond_3
    iget-object v0, p0, Lgyv;->a:Landroid/widget/EditText;

    iget v1, p0, Lgyv;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyv;->d:Z

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    iget-boolean v0, p0, Lgyv;->d:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sub-int v0, p4, p3

    iput v0, p0, Lgyv;->c:I

    add-int v0, p2, p4

    iput v0, p0, Lgyv;->b:I

    goto :goto_0
.end method
