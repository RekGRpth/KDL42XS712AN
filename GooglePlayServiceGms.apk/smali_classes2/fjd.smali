.class abstract Lfjd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic b:Lfiu;


# direct methods
.method private constructor <init>(Lfiu;)V
    .locals 0

    iput-object p1, p0, Lfjd;->b:Lfiu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfiu;B)V
    .locals 0

    invoke-direct {p0, p1}, Lfjd;-><init>(Lfiu;)V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Lfjf;ILjava/lang/String;)V
.end method

.method public final a(Lfjh;)V
    .locals 5

    iget-object v0, p1, Lfjh;->b:Ljava/lang/String;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lfjd;->b:Lfiu;

    invoke-static {v1}, Lfiu;->b(Lfiu;)Lfbn;

    move-result-object v1

    invoke-virtual {p0}, Lfjd;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfjd;->b:Lfiu;

    invoke-static {v3}, Lfiu;->a(Lfiu;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v0, "PeopleService"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lfjd;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lfjd;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4, v0}, Lfiu;->a(Lfjh;Ljava/lang/String;Ljava/lang/String;)Lfjf;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lfjd;->a(Lfjf;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract c()Ljava/lang/String;
.end method
