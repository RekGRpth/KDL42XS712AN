.class public final Lbrd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbrc;


# instance fields
.field private final a:Lbsp;

.field private final b:Lcfz;

.field private final c:Lbwo;

.field private final d:Lcmn;

.field private final e:Lcll;

.field private final f:Lbwb;

.field private final g:Lbuc;

.field private final h:Lbso;

.field private final i:Lcom/google/android/gms/common/server/ClientContext;

.field private final j:Lbwd;

.field private final k:Landroid/content/Context;

.field private final l:Lcom/google/android/gms/drive/DriveId;

.field private final m:Lcdu;

.field private final n:I

.field private final o:Lcgw;

.field private final p:Lchd;

.field private final q:Lcnf;

.field private final r:Lbsr;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILcoy;)V
    .locals 5

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lbrd;->n:I

    iput-object p1, p0, Lbrd;->i:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p3}, Lcoy;->k()Lbwd;

    move-result-object v0

    iput-object v0, p0, Lbrd;->j:Lbwd;

    invoke-virtual {p3}, Lcoy;->h()Lcmn;

    move-result-object v0

    iput-object v0, p0, Lbrd;->d:Lcmn;

    invoke-virtual {p3}, Lcoy;->j()Lcll;

    move-result-object v0

    iput-object v0, p0, Lbrd;->e:Lcll;

    invoke-virtual {p3}, Lcoy;->d()Lcdu;

    move-result-object v0

    iput-object v0, p0, Lbrd;->m:Lcdu;

    invoke-virtual {p3}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbrd;->b:Lcfz;

    invoke-virtual {p3}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbrd;->k:Landroid/content/Context;

    new-instance v0, Lbso;

    invoke-direct {v0, p3}, Lbso;-><init>(Lcoy;)V

    iput-object v0, p0, Lbrd;->h:Lbso;

    new-instance v0, Lbwb;

    invoke-direct {v0, p3}, Lbwb;-><init>(Lcoy;)V

    iput-object v0, p0, Lbrd;->f:Lbwb;

    new-instance v0, Lbuc;

    invoke-direct {v0, p3}, Lbuc;-><init>(Lcoy;)V

    iput-object v0, p0, Lbrd;->g:Lbuc;

    invoke-virtual {p3}, Lcoy;->q()Lbwo;

    move-result-object v0

    iput-object v0, p0, Lbrd;->c:Lbwo;

    new-instance v0, Lcgw;

    invoke-direct {v0}, Lcgw;-><init>()V

    iput-object v0, p0, Lbrd;->o:Lcgw;

    invoke-virtual {p3}, Lcoy;->o()Lchd;

    move-result-object v0

    iput-object v0, p0, Lbrd;->p:Lchd;

    invoke-virtual {p3}, Lcoy;->r()Lcnf;

    move-result-object v0

    iput-object v0, p0, Lbrd;->q:Lcnf;

    invoke-virtual {p3}, Lcoy;->s()Lbsr;

    move-result-object v0

    iput-object v0, p0, Lbrd;->r:Lbsr;

    iget-object v0, p0, Lbrd;->h:Lbso;

    invoke-virtual {v0, p1, v2}, Lbso;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lbsm;

    move-result-object v0

    iget-object v1, v0, Lbsm;->a:Lbsn;

    invoke-virtual {v1}, Lbsn;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lbsm;->c:Lbsl;

    throw v0

    :cond_0
    iget-object v0, v0, Lbsm;->b:Lbsp;

    iput-object v0, p0, Lbrd;->a:Lbsp;

    sget-object v0, Lbqr;->c:Lbqr;

    invoke-virtual {v0}, Lbqr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbrd;->d()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lbsl;

    const-string v1, "Scope %s not supported."

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lbqr;->c:Lbqr;

    invoke-virtual {v4}, Lbqr;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lbrd;->b:Lcfz;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    iget-object v1, v1, Lbsp;->a:Lcfc;

    invoke-static {v1}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v1

    iget-object v2, p0, Lbrd;->k:Landroid/content/Context;

    const v3, 0x7f0b004b    # com.google.android.gms.R.string.drive_menu_my_drive

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->b(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    iget-object v1, p0, Lbrd;->m:Lcdu;

    invoke-static {v0, v1}, Lcfu;->a(Lcfp;Lcdu;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    return-void
.end method

.method private e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->c()J

    move-result-wide v0

    iget-object v2, p0, Lbrd;->m:Lcdu;

    invoke-virtual {v2}, Lcdu;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcfp;->W()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "Invalid parent folder."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_3
    invoke-virtual {v0}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lbsp;
    .locals 1

    iget-object v0, p0, Lbrd;->a:Lbsp;

    return-object v0
.end method

.method public final a(Z)Lbsp;
    .locals 6

    iget-object v0, p0, Lbrd;->h:Lbso;

    iget-object v1, p0, Lbrd;->i:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1, p1}, Lbso;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lbsm;

    move-result-object v0

    iget-object v1, v0, Lbsm;->a:Lbsn;

    invoke-virtual {v1}, Lbsn;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lbsm;->c:Lbsl;

    throw v0

    :cond_0
    iget-object v1, v0, Lbsm;->b:Lbsp;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-virtual {v1, v2}, Lbsp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lbsl;

    const-string v2, "Authorized app changed from %s to %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lbrd;->a:Lbsp;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v0, v0, Lbsm;->b:Lbsp;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbsl;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lbrd;->a:Lbsp;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    invoke-static {}, Lcni;->a()Lcni;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcni;->a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lceg;->c:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " DESC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;)Lcom/google/android/gms/drive/DriveId;
    .locals 4

    invoke-direct {p0, p1}, Lbrd;->f(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iget-object v1, p0, Lbrd;->g:Lbuc;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v3

    invoke-virtual {v1, v2, p2, v3, v0}, Lbuc;->a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lbrd;->e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lbrm;

    const-string v1, "Cannot find root folder id"

    invoke-direct {v0, v4, v1, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    iget-object v1, p0, Lbrd;->a:Lbsp;

    iget-object v1, v1, Lbsp;->a:Lcfc;

    invoke-static {v1}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v1

    iget-object v2, p0, Lbrd;->b:Lcfz;

    invoke-interface {v2, v1, v0}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lbrm;

    const-string v1, "Cannot find root folder"

    invoke-direct {v0, v4, v1, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    invoke-virtual {p0, p1}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Lcjc;->a(Lcfp;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iget v1, p0, Lbrd;->n:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(I)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 6

    const/16 v2, 0xa

    const/4 v5, 0x0

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbrm;

    const-string v1, "Cannot edit metadata of the root folder"

    invoke-direct {v0, v2, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    sget-object v0, Lclg;->a:Lcjf;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcje;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lclg;->a:Lcjf;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p0, p1}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->G()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Lbrm;

    const-string v1, "The user cannot edit the resource."

    invoke-direct {v0, v2, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_2
    new-instance v1, Lcne;

    invoke-direct {v1, v0, p2}, Lcne;-><init>(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    new-instance v2, Lcmh;

    iget-object v3, p0, Lbrd;->a:Lbsp;

    iget-object v3, v3, Lbsp;->a:Lcfc;

    iget-object v4, p0, Lbrd;->a:Lbsp;

    iget-object v4, v4, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v2, v3, v4, v0, p2}, Lcmh;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    iget-object v0, p0, Lbrd;->d:Lcmn;

    invoke-virtual {v0, v2}, Lcmn;->a(Lcml;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to process update"

    invoke-direct {v0, v1, v2, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_3
    iget-object v0, p0, Lbrd;->q:Lcnf;

    iget-object v2, p0, Lbrd;->r:Lbsr;

    invoke-virtual {v1, v0, v2}, Lcne;->a(Lcnf;Lbsr;)V

    invoke-virtual {p0, p1}, Lbrd;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLcom/google/android/gms/drive/DriveId;)V
    .locals 9

    const/4 v8, 0x0

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v0, v0, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->c:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "App must have full Drive scope to perform this action."

    invoke-direct {v0, v1, v2, v8}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    invoke-virtual {p0, p3}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v3

    new-instance v0, Lcmt;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    iget-object v1, v1, Lbsp;->a:Lcfc;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    iget-object v2, v2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v3}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    sget-object v6, Lbsj;->a:Lbsj;

    sget-object v7, Lcms;->a:Lcms;

    move-wide v4, p1

    invoke-direct/range {v0 .. v7}, Lcmt;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;JLbsj;Lcms;)V

    iget-object v1, p0, Lbrd;->d:Lcmn;

    invoke-virtual {v1, v0}, Lcmn;->a(Lcml;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to process authorization"

    invoke-direct {v0, v1, v2, v8}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;ILcht;)V
    .locals 4

    iget-object v1, p0, Lbrd;->o:Lcgw;

    iget-object v2, v1, Lcgw;->a:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, v1, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v1, Lcgx;

    const/4 v3, 0x0

    invoke-direct {v1, p2, p3, v3}, Lcgx;-><init>(ILcht;B)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;ILjava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, Lbrd;->p:Lchd;

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v3, v0, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    new-instance v0, Lcgu;

    iget-object v1, v1, Lchd;->c:Lcdu;

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcgu;-><init>(Lcdu;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;ILjava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lcgu;->k()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "SubscriptionStore"

    const-string v3, "Duplicate description detected: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcgu;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v1, v3, v4}, Lcbv;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "SubscriptionStore"

    const-string v3, "Unable to insert subscription: "

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcgu;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2, v1, v3, v4}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Unable to store subscription"

    invoke-direct {v0, v1, v2, v6}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lbwk;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lbrd;->f:Lbwb;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lbwb;->a(Lbsp;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lbrm; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Drive item not found, or you are not authorized to access it."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_0
    move-exception v1

    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Failed to sync metadata"

    invoke-static {v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lbrd;->c:Lbwo;

    iget-object v3, p0, Lbrd;->a:Lbsp;

    invoke-virtual {v2, v3, v1, v0}, Lbwo;->a(Lbsp;Lcfp;Ljava/lang/String;)Lbwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbwm;->a(Lbwk;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Lbwk;->a(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v0, v0, Lbsp;->a:Lcfc;

    iget-object v0, v0, Lcfc;->a:Ljava/lang/String;

    iget-object v1, p0, Lbrd;->j:Lbwd;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lbwd;->a(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Internal error while requesting sync."

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lbrd;->j:Lbwd;

    invoke-virtual {v1, v0, p1}, Lbwd;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void

    :pswitch_1
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "Sync request rate limit exceeded."

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :pswitch_2
    new-instance v0, Lbrm;

    const/4 v1, 0x7

    const-string v2, "Cannot request a sync while the device is offline."

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;)Z
    .locals 1

    iget-object v0, p0, Lbrd;->o:Lcgw;

    invoke-virtual {v0, p1}, Lcgw;->a(Lcom/google/android/gms/drive/events/DriveEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;)Lcfp;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lbrd;->e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-interface {v1, v2, v0}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "Could not find entry, and no valid resource id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbrm;

    const-string v1, "Provided DriveId is not valid."

    invoke-direct {v0, v6, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "appdata"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v0, v0, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->b:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbrd;->b:Lcfz;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    invoke-interface {v0, v1}, Lcfz;->a(Lbsp;)Lcfg;

    move-result-object v0

    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    iget-object v0, v0, Lcfg;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lbrd;->f:Lbwb;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbwb;->a(Lbsp;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lbrd;->b:Lcfz;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lbrm;

    const-string v1, "Drive item not found, or you are not authorized to access it."

    invoke-direct {v0, v6, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_3
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "The current scope does not allow use of the AppData folder"

    invoke-direct {v0, v1, v2, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_4
    iget-object v0, p0, Lbrd;->b:Lcfz;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;
    .locals 3

    invoke-direct {p0, p1}, Lbrd;->f(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iget-object v1, p0, Lbrd;->g:Lbuc;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-virtual {v1, v2, p2, v0}, Lbuc;->a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;ILcht;)V
    .locals 4

    iget-object v0, p0, Lbrd;->o:Lcgw;

    iget-object v1, v0, Lcgw;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_0

    new-instance v2, Lcgx;

    const/4 v3, 0x0

    invoke-direct {v2, p2, p3, v3}, Lcgx;-><init>(ILcht;B)V

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lbrd;->p:Lchd;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    iget-object v1, v1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    :try_start_0
    iget-object v0, v0, Lchd;->b:Lcfz;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2, p3}, Lcfz;->a(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to remove subscription: id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final c()Lcom/google/android/gms/drive/DriveId;
    .locals 3

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v0, v0, Lbsp;->e:Ljava/util/Set;

    sget-object v1, Lbqr;->b:Lbqr;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbrd;->b:Lcfz;

    iget-object v1, p0, Lbrd;->a:Lbsp;

    invoke-interface {v0, v1}, Lcfz;->a(Lbsp;)Lcfg;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcfg;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-interface {v1, v2, v0}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lbrd;->m:Lcdu;

    invoke-static {v0, v1}, Lcfu;->a(Lcfp;Lcdu;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/drive/DriveId;)V
    .locals 7

    const/16 v2, 0xa

    const/4 v6, 0x0

    iget-object v0, p0, Lbrd;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbrm;

    const-string v1, "Cannot trash root folder"

    invoke-direct {v0, v2, v1, v6}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lbrm;

    const-string v1, "Cannot trash folders through this API."

    invoke-direct {v0, v2, v1, v6}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcfp;->y()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lbrm;

    const-string v1, "Cannot trash AppData"

    invoke-direct {v0, v2, v1, v6}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_2
    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v1, v0, Lbsp;->a:Lcfc;

    iget-object v0, p0, Lbrd;->a:Lbsp;

    iget-object v2, v0, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    new-instance v0, Lcmw;

    sget-object v4, Lcfs;->c:Lcfs;

    sget-object v5, Lcms;->a:Lcms;

    invoke-direct/range {v0 .. v5}, Lcmw;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcfs;Lcms;)V

    iget-object v1, p0, Lbrd;->d:Lcmn;

    invoke-virtual {v1, v0}, Lcmn;->a(Lcml;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to trash file."

    invoke-direct {v0, v1, v2, v6}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_3
    return-void
.end method

.method public final d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    invoke-virtual {p0, p1}, Lbrd;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    iget-object v1, p0, Lbrd;->b:Lcfz;

    iget-object v2, p0, Lbrd;->a:Lbsp;

    invoke-interface {v1, v2, v0}, Lcfz;->b(Lbsp;Lcfp;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lbrd;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lbrd;->a:Lbsp;

    iget-object v1, v1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lbrd;->o:Lcgw;

    iget-object v1, v0, Lcgw;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
