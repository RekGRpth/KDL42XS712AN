.class public final enum Lhsv;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhsv;

.field public static final enum b:Lhsv;

.field public static final enum c:Lhsv;

.field public static final enum d:Lhsv;

.field private static final synthetic e:[Lhsv;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhsv;

    const-string v1, "LOCATION_ACTIVE_ON"

    invoke-direct {v0, v1, v2}, Lhsv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhsv;->a:Lhsv;

    new-instance v0, Lhsv;

    const-string v1, "LOCATION_ACTIVE_OFF"

    invoke-direct {v0, v1, v3}, Lhsv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhsv;->b:Lhsv;

    new-instance v0, Lhsv;

    const-string v1, "SATELLITE_STATUS_ON"

    invoke-direct {v0, v1, v4}, Lhsv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhsv;->c:Lhsv;

    new-instance v0, Lhsv;

    const-string v1, "SATELLITE_STATUS_OFF"

    invoke-direct {v0, v1, v5}, Lhsv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhsv;->d:Lhsv;

    const/4 v0, 0x4

    new-array v0, v0, [Lhsv;

    sget-object v1, Lhsv;->a:Lhsv;

    aput-object v1, v0, v2

    sget-object v1, Lhsv;->b:Lhsv;

    aput-object v1, v0, v3

    sget-object v1, Lhsv;->c:Lhsv;

    aput-object v1, v0, v4

    sget-object v1, Lhsv;->d:Lhsv;

    aput-object v1, v0, v5

    sput-object v0, Lhsv;->e:[Lhsv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhsv;
    .locals 1

    const-class v0, Lhsv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhsv;

    return-object v0
.end method

.method public static values()[Lhsv;
    .locals 1

    sget-object v0, Lhsv;->e:[Lhsv;

    invoke-virtual {v0}, [Lhsv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhsv;

    return-object v0
.end method
