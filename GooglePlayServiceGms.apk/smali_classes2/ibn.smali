.class public final Libn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Libw;


# instance fields
.field private final a:Libp;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Libp;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Libn;-><init>(Libp;B)V

    return-void
.end method

.method private constructor <init>(Libp;B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Libn;->a:Libp;

    const/high16 v0, -0x80000000

    iput v0, p0, Libn;->b:I

    const/4 v0, 0x2

    iput v0, p0, Libn;->c:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;Ljava/util/Map;)Libx;
    .locals 10

    const/4 v3, 0x0

    sget-object v2, Libx;->a:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v5, p0, Libn;->b:I

    if-le v1, v5, :cond_f

    sget-object v1, Libx;->a:Ljava/util/Set;

    if-ne v2, v1, :cond_e

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v2, v1

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Libn;->a:Libp;

    invoke-interface {v0, v2}, Libp;->a(Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_4

    :cond_3
    new-instance v8, Libo;

    iget v0, p0, Libn;->c:I

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v8, v0, v1}, Libo;-><init>(ILjava/util/Map;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "ModelLocalizerV2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cluster result is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v8}, Libo;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "ModelLocalizerV2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required cluster confidence "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Libn;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v8, Libo;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v8}, Libo;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    move-object v0, v3

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Libn;->a:Libp;

    invoke-interface {v0, v5}, Libp;->a(Ljava/lang/String;)Lhtr;

    move-result-object v0

    if-nez v0, :cond_8

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_7

    const-string v0, "ModelLocalizerV2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No level selector for cluster: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object v4, v3

    :goto_6
    if-nez v4, :cond_a

    move-object v0, v3

    goto :goto_5

    :cond_8
    invoke-virtual {v0, p2}, Lhtr;->a(Ljava/util/Map;)Lhts;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_9

    const-string v1, "ModelLocalizerV2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Level model result is: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object v4, v0

    goto :goto_6

    :cond_a
    iget-object v6, v4, Lhts;->a:Ljava/lang/String;

    iget-object v0, p0, Libn;->a:Libp;

    invoke-interface {v0, v6}, Libp;->b(Ljava/lang/String;)Lhto;

    move-result-object v7

    if-nez v7, :cond_c

    sget-boolean v0, Licj;->c:Z

    if-eqz v0, :cond_b

    const-string v0, "ModelLocalizerV2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No model for level: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    move-object v0, v3

    goto :goto_5

    :cond_c
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v0}, Lhto;->a(Ljava/util/List;)Lhtp;

    move-result-object v3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_d

    const-string v0, "ModelLocalizerV2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Level Model Result is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    new-instance v0, Lhtl;

    iget v1, v3, Lhtp;->a:I

    iget v2, v3, Lhtp;->b:I

    iget v3, v3, Lhtp;->c:I

    iget v4, v4, Lhts;->b:F

    const/high16 v9, 0x41200000    # 10.0f

    mul-float/2addr v4, v9

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    invoke-interface {v7}, Lhto;->a()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lhtl;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V

    new-instance v1, Libx;

    iget v2, v8, Libo;->a:I

    add-int/lit8 v2, v2, 0x64

    sget-object v3, Libx;->a:Ljava/util/Set;

    invoke-direct {v1, v0, v2, v3}, Libx;-><init>(Lhug;ILjava/util/Set;)V

    move-object v0, v1

    goto/16 :goto_5

    :cond_e
    move-object v1, v2

    goto/16 :goto_1

    :cond_f
    move-object v1, v2

    goto/16 :goto_2
.end method
