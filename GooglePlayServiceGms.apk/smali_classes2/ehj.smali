.class public final Lehj;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lehh;

.field public b:Lehk;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lehj;->a:Lehh;

    iput-object v0, p0, Lehj;->b:Lehk;

    const/4 v0, -0x1

    iput v0, p0, Lehj;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lehj;->a:Lehh;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lehj;->a:Lehh;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lehj;->b:Lehk;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lehj;->b:Lehk;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lehj;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lehj;->a:Lehh;

    if-nez v0, :cond_1

    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    iput-object v0, p0, Lehj;->a:Lehh;

    :cond_1
    iget-object v0, p0, Lehj;->a:Lehh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lehj;->b:Lehk;

    if-nez v0, :cond_2

    new-instance v0, Lehk;

    invoke-direct {v0}, Lehk;-><init>()V

    iput-object v0, p0, Lehj;->b:Lehk;

    :cond_2
    iget-object v0, p0, Lehj;->b:Lehk;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Lehj;->a:Lehh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lehj;->a:Lehh;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Lehj;->b:Lehk;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lehj;->b:Lehk;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehj;

    iget-object v2, p0, Lehj;->a:Lehh;

    if-nez v2, :cond_3

    iget-object v2, p1, Lehj;->a:Lehh;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lehj;->a:Lehh;

    iget-object v3, p1, Lehj;->a:Lehh;

    invoke-virtual {v2, v3}, Lehh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lehj;->b:Lehk;

    if-nez v2, :cond_5

    iget-object v2, p1, Lehj;->b:Lehk;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lehj;->b:Lehk;

    iget-object v3, p1, Lehj;->b:Lehk;

    invoke-virtual {v2, v3}, Lehk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lehj;->a:Lehh;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lehj;->b:Lehk;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lehj;->a:Lehh;

    invoke-virtual {v0}, Lehh;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lehj;->b:Lehk;

    invoke-virtual {v1}, Lehk;->hashCode()I

    move-result v1

    goto :goto_1
.end method
