.class public final Lmh;
.super Llm;
.source "SourceFile"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private e:Llm;

.field private f:Llq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Llm;Llq;)V
    .locals 0

    invoke-direct {p0, p1}, Llm;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lmh;->e:Llm;

    iput-object p3, p0, Lmh;->f:Llq;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lmh;->f:Llq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmh;->f:Llq;

    invoke-virtual {v0}, Llq;->getItemId()I

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Llm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lln;)V
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0, p1}, Llm;->a(Lln;)V

    return-void
.end method

.method public final a(Llm;Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Llm;->a(Llm;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0, p1, p2}, Llm;->a(Llm;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Llq;)Z
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0, p1}, Llm;->a(Llq;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0}, Llm;->b()Z

    move-result v0

    return v0
.end method

.method public final b(Llq;)Z
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0, p1}, Llm;->b(Llq;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0}, Llm;->c()Z

    move-result v0

    return v0
.end method

.method public final clearHeader()V
    .locals 0

    return-void
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmh;->f:Llq;

    return-object v0
.end method

.method public final r()Llm;
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    return-object v0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Llm;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Llm;->a(Landroid/graphics/drawable/Drawable;)Llm;

    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Llm;->a(Landroid/graphics/drawable/Drawable;)Llm;

    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Llm;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Llm;->a(Ljava/lang/CharSequence;)Llm;

    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Llm;->a(Ljava/lang/CharSequence;)Llm;

    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0, v0, p1}, Llm;->a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmh;->f:Llq;

    invoke-virtual {v0, p1}, Llq;->setIcon(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmh;->f:Llq;

    invoke-virtual {v0, p1}, Llq;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    invoke-virtual {v0, p1}, Llm;->setQwertyMode(Z)V

    return-void
.end method

.method public final u()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lmh;->e:Llm;

    return-object v0
.end method
