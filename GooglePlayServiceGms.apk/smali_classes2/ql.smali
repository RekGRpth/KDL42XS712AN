.class public Lql;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lql;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lql;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lqj;Ljava/lang/String;)I
    .locals 5

    const/4 v1, 0x1

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Language item must be used on array"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lqj;->c()I

    move-result v2

    if-gt v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lqj;->a(I)Lqj;

    move-result-object v2

    invoke-virtual {v2}, Lqj;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "xml:lang"

    invoke-virtual {v2, v1}, Lqj;->c(I)Lqj;

    move-result-object v4

    invoke-virtual {v4}, Lqj;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Lqj;->c(I)Lqj;

    move-result-object v2

    invoke-virtual {v2}, Lqj;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(Lqj;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x1

    const-string v1, "xml:lang"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p2}, Lqf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lql;->a(Lqj;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    and-int/lit16 v2, p3, 0x1000

    if-lez v2, :cond_0

    new-instance v1, Lqj;

    const-string v2, "[]"

    invoke-direct {v1, v2, v5}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    new-instance v2, Lqj;

    const-string v3, "xml:lang"

    const-string v4, "x-default"

    invoke-direct {v2, v3, v4, v5}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    invoke-virtual {v1, v2}, Lqj;->d(Lqj;)V

    invoke-virtual {p0, v1}, Lqj;->b(Lqj;)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lqj;->c()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Lqj;->a(I)Lqj;

    move-result-object v0

    invoke-virtual {v0}, Lqj;->i()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lqj;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static a(Lqj;Ljava/lang/String;Ljava/lang/String;Z)Lqj;
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Lql;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lqj;->a()Lqj;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lqj;->a(Ljava/lang/String;)Lqj;

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    new-instance v1, Lqj;

    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v2, v3}, Lqw;->a(IZ)V

    invoke-direct {v1, p1, v0}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    invoke-virtual {v1, v3}, Lqj;->a(Z)V

    invoke-static {}, Lpt;->a()Lpu;

    move-result-object v0

    invoke-interface {v0, p1}, Lpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lpt;->a()Lpu;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lpu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {v1, v0}, Lqj;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lqj;->a(Lqj;)V

    move-object v0, v1

    :cond_2
    return-object v0

    :cond_3
    new-instance v0, Lpr;

    const-string v1, "Unregistered schema namespace URI"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method static a(Lqj;Ljava/lang/String;Z)Lqj;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;Z)Lqj;

    move-result-object v0

    return-object v0
.end method

.method static a(Lqj;Lqq;ZLqw;)Lqj;
    .locals 9

    const/16 v4, 0x66

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lqq;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lpr;

    const-string v1, "Empty XMPPath"

    invoke-direct {v0, v1, v4}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    invoke-virtual {p1, v3}, Lqq;->a(I)Lqs;

    move-result-object v0

    iget-object v0, v0, Lqs;->a:Ljava/lang/String;

    invoke-static {p0, v0, v2, p2}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;Z)Lqj;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_2
    :goto_0
    return-object v2

    :cond_3
    invoke-virtual {v1}, Lqj;->m()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {v1, v3}, Lqj;->a(Z)V

    move-object v0, v1

    :goto_1
    move v4, v5

    move-object v3, v0

    :goto_2
    :try_start_0
    invoke-virtual {p1}, Lqq;->a()I

    move-result v0

    if-ge v4, v0, :cond_11

    invoke-virtual {p1, v4}, Lqq;->a(I)Lqs;

    move-result-object v0

    iget v6, v0, Lqs;->b:I

    if-ne v6, v5, :cond_5

    iget-object v0, v0, Lqs;->a:Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lql;->b(Lqj;Ljava/lang/String;Z)Lqj;

    move-result-object v1

    :goto_3
    if-nez v1, :cond_e

    if-eqz p2, :cond_2

    invoke-static {v3}, Lql;->b(Lqj;)V
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz v3, :cond_4

    invoke-static {v3}, Lql;->b(Lqj;)V

    :cond_4
    throw v0

    :cond_5
    const/4 v7, 0x2

    if-ne v6, v7, :cond_8

    :try_start_1
    iget-object v0, v0, Lqs;->a:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v0, Lql;->a:Z

    if-nez v0, :cond_6

    const-string v0, "?"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6
    invoke-virtual {v1, v6}, Lqj;->b(Ljava/lang/String;)Lqj;

    move-result-object v0

    if-nez v0, :cond_7

    if-eqz p2, :cond_7

    new-instance v0, Lqj;

    const/4 v7, 0x0

    invoke-direct {v0, v6, v7}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lqj;->a(Z)V

    invoke-virtual {v1, v0}, Lqj;->d(Lqj;)V

    :cond_7
    move-object v1, v0

    goto :goto_3

    :cond_8
    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v7

    invoke-virtual {v7}, Lqw;->d()Z

    move-result v7

    if-nez v7, :cond_9

    new-instance v0, Lpr;

    const-string v1, "Indexing applied to non-array"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_9
    const/4 v7, 0x3

    if-ne v6, v7, :cond_a

    iget-object v0, v0, Lqs;->a:Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lql;->c(Lqj;Ljava/lang/String;Z)I

    move-result v0

    :goto_4
    if-lez v0, :cond_14

    invoke-virtual {v1}, Lqj;->c()I

    move-result v6

    if-gt v0, v6, :cond_14

    invoke-virtual {v1, v0}, Lqj;->a(I)Lqj;

    move-result-object v1

    goto :goto_3

    :cond_a
    const/4 v7, 0x4

    if-ne v6, v7, :cond_b

    invoke-virtual {v1}, Lqj;->c()I

    move-result v0

    goto :goto_4

    :cond_b
    const/4 v7, 0x6

    if-ne v6, v7, :cond_c

    iget-object v0, v0, Lqs;->a:Ljava/lang/String;

    invoke-static {v0}, Lqf;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    aget-object v6, v0, v6

    const/4 v7, 0x1

    aget-object v0, v0, v7

    invoke-static {v1, v6, v0}, Lql;->c(Lqj;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_4

    :cond_c
    const/4 v7, 0x5

    if-ne v6, v7, :cond_d

    iget-object v6, v0, Lqs;->a:Ljava/lang/String;

    invoke-static {v6}, Lqf;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x1

    aget-object v6, v6, v8

    iget v0, v0, Lqs;->d:I

    invoke-static {v1, v7, v6, v0}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_4

    :cond_d
    new-instance v0, Lpr;

    const-string v1, "Unknown array indexing step in FollowXPathStep"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_e
    invoke-virtual {v1}, Lqj;->m()Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lqj;->a(Z)V

    if-ne v4, v5, :cond_10

    invoke-virtual {p1, v4}, Lqq;->a(I)Lqs;

    move-result-object v0

    iget-boolean v0, v0, Lqs;->c:Z

    if-eqz v0, :cond_10

    invoke-virtual {p1, v4}, Lqq;->a(I)Lqs;

    move-result-object v0

    iget v0, v0, Lqs;->d:I

    if-eqz v0, :cond_10

    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {p1, v4}, Lqq;->a(I)Lqs;

    move-result-object v6

    iget v6, v6, Lqs;->d:I

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Lqw;->a(IZ)V

    :cond_f
    :goto_5
    if-nez v3, :cond_13

    move-object v0, v1

    :goto_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v0

    goto/16 :goto_2

    :cond_10
    invoke-virtual {p1}, Lqq;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_f

    invoke-virtual {p1, v4}, Lqq;->a(I)Lqs;

    move-result-object v0

    iget v0, v0, Lqs;->b:I

    if-ne v0, v5, :cond_f

    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->n()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v0

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lqw;->d(Z)Lqw;
    :try_end_1
    .catch Lpr; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    :cond_11
    if-eqz v3, :cond_12

    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0, p3}, Lqw;->a(Lqw;)V

    invoke-virtual {v1}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v1, v0}, Lqj;->a(Lqw;)V

    :cond_12
    move-object v2, v1

    goto/16 :goto_0

    :cond_13
    move-object v0, v3

    goto :goto_6

    :cond_14
    move-object v1, v2

    goto/16 :goto_3

    :cond_15
    move-object v0, v2

    goto/16 :goto_1
.end method

.method static a(Lqw;)Lqw;
    .locals 1

    if-nez p0, :cond_0

    new-instance p0, Lqw;

    invoke-direct {p0}, Lqw;-><init>()V

    :cond_0
    invoke-virtual {p0}, Lqw;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lqw;->j()Lqw;

    :cond_1
    invoke-virtual {p0}, Lqw;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqw;->h()Lqw;

    :cond_2
    invoke-virtual {p0}, Lqw;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lqw;->f()Lqw;

    :cond_3
    invoke-virtual {p0}, Lqw;->n()Z

    iget v0, p0, Lqu;->a:I

    invoke-virtual {p0, v0}, Lqw;->c(I)V

    return-object p0
.end method

.method static a(Lqj;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqj;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->l()Lqw;

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lqj;->c()I

    move-result v3

    if-gt v0, v3, :cond_2

    invoke-virtual {p0, v0}, Lqj;->a(I)Lqj;

    move-result-object v3

    invoke-virtual {v3}, Lqj;->h()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "x-default"

    invoke-virtual {v3, v2}, Lqj;->c(I)Lqj;

    move-result-object v5

    invoke-virtual {v5}, Lqj;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :try_start_0
    invoke-virtual {p0, v0}, Lqj;->b(I)V

    invoke-virtual {p0, v3}, Lqj;->b(Lqj;)V
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v1}, Lqj;->a(I)Lqj;

    move-result-object v0

    invoke-virtual {v3}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqj;->d(Ljava/lang/String;)V

    :cond_2
    return-void

    :catch_0
    move-exception v2

    sget-boolean v2, Lql;->a:Z

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method static a(Lqj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lqj;

    const-string v1, "[]"

    invoke-direct {v0, v1, p2, v3}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    new-instance v1, Lqj;

    const-string v2, "xml:lang"

    invoke-direct {v1, v2, p1, v3}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    invoke-virtual {v0, v1}, Lqj;->d(Lqj;)V

    const-string v2, "x-default"

    invoke-virtual {v1}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lqj;->a(Lqj;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lqj;->b(Lqj;)V

    goto :goto_0
.end method

.method static b(Lqj;Ljava/lang/String;Z)Lqj;
    .locals 4

    const/16 v3, 0x66

    const/4 v2, 0x1

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->m()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lqj;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Named children only allowed for schemas and structs"

    invoke-direct {v0, v1, v3}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lpr;

    const-string v1, "Named children not allowed for arrays"

    invoke-direct {v0, v1, v3}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0, v2}, Lqw;->d(Z)Lqw;

    :cond_2
    invoke-virtual {p0, p1}, Lqj;->a(Ljava/lang/String;)Lqj;

    move-result-object v0

    if-nez v0, :cond_3

    if-eqz p2, :cond_3

    new-instance v1, Lqw;

    invoke-direct {v1}, Lqw;-><init>()V

    new-instance v0, Lqj;

    invoke-direct {v0, p1, v1}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    invoke-virtual {v0, v2}, Lqj;->a(Z)V

    invoke-virtual {p0, v0}, Lqj;->a(Lqj;)V

    :cond_3
    sget-boolean v1, Lql;->a:Z

    if-nez v1, :cond_4

    if-nez v0, :cond_4

    if-eqz p2, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    return-object v0
.end method

.method private static b(Lqj;)V
    .locals 2

    invoke-virtual {p0}, Lqj;->a()Lqj;

    move-result-object v0

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v1

    invoke-virtual {v1}, Lqw;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p0}, Lqj;->e(Lqj;)V

    :goto_0
    invoke-virtual {v0}, Lqj;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lqj;->l()Lqw;

    move-result-object v1

    invoke-virtual {v1}, Lqw;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lqj;->a()Lqj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lqj;->c(Lqj;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0, p0}, Lqj;->c(Lqj;)V

    goto :goto_0
.end method

.method static b(Lqj;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 11

    const/4 v1, 0x0

    const/16 v10, 0x66

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->k()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Localized text array is not alt-text"

    invoke-direct {v0, v1, v10}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lqj;->f()Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v9, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v4

    aput-object v1, v0, v8

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    move v3, v4

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->l()Lqw;

    move-result-object v6

    invoke-virtual {v6}, Lqw;->n()Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v0, Lpr;

    const-string v1, "Alt-text array item is not simple"

    invoke-direct {v0, v1, v10}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    invoke-virtual {v0}, Lqj;->h()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "xml:lang"

    invoke-virtual {v0, v8}, Lqj;->c(I)Lqj;

    move-result-object v7

    invoke-virtual {v7}, Lqj;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    new-instance v0, Lpr;

    const-string v1, "Alt-text array item has no language qualifier"

    invoke-direct {v0, v1, v10}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_4
    invoke-virtual {v0, v8}, Lqj;->c(I)Lqj;

    move-result-object v6

    invoke-virtual {v6}, Lqj;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    new-array v1, v9, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v8}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v4

    aput-object v0, v1, v8

    move-object v0, v1

    goto :goto_0

    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {v6, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    if-nez v2, :cond_c

    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_1

    :cond_6
    const-string v7, "x-default"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    :goto_3
    move-object v1, v0

    goto :goto_1

    :cond_7
    if-ne v3, v8, :cond_8

    new-array v0, v9, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v9}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    aput-object v2, v0, v8

    goto/16 :goto_0

    :cond_8
    if-le v3, v8, :cond_9

    new-array v0, v9, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/Integer;

    const/4 v3, 0x3

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    aput-object v2, v0, v8

    goto/16 :goto_0

    :cond_9
    if-eqz v1, :cond_a

    new-array v0, v9, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/Integer;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v4

    aput-object v1, v0, v8

    goto/16 :goto_0

    :cond_a
    new-array v0, v9, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v1, v0, v4

    invoke-virtual {p0, v8}, Lqj;->a(I)Lqj;

    move-result-object v1

    aput-object v1, v0, v8

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    goto :goto_3

    :cond_c
    move-object v0, v2

    goto :goto_2
.end method

.method private static c(Lqj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    const/4 v1, 0x1

    const/4 v0, -0x1

    move v2, v0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lqj;->c()I

    move-result v3

    if-gt v0, v3, :cond_3

    if-gez v2, :cond_3

    invoke-virtual {p0, v0}, Lqj;->a(I)Lqj;

    move-result-object v4

    invoke-virtual {v4}, Lqj;->l()Lqw;

    move-result-object v3

    invoke-virtual {v3}, Lqw;->c()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Field selector must be used on array of struct"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    move v3, v1

    :goto_1
    invoke-virtual {v4}, Lqj;->c()I

    move-result v5

    if-gt v3, v5, :cond_1

    invoke-virtual {v4, v3}, Lqj;->a(I)Lqj;

    move-result-object v5

    invoke-virtual {v5}, Lqj;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lqj;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    return v2
.end method

.method private static c(Lqj;Ljava/lang/String;Z)I
    .locals 5

    const/16 v3, 0x66

    const/4 v4, 0x1

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Array index must be larger than zero"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Lpr;

    const-string v1, "Array index not digits."

    invoke-direct {v0, v1, v3}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lqj;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v1, Lqj;

    const-string v2, "[]"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lqj;-><init>(Ljava/lang/String;Lqw;)V

    invoke-virtual {v1, v4}, Lqj;->a(Z)V

    invoke-virtual {p0, v1}, Lqj;->a(Lqj;)V

    :cond_1
    return v0
.end method
