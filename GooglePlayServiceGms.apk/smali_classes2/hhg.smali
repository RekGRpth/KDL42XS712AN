.class public final Lhhg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x400

    iput v0, p0, Lhhg;->a:I

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lhhg;-><init>()V

    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
    .locals 3

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "HttpDownloader"

    const-string v2, "Exception closing output stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhhi;Ljava/io/OutputStream;I)Z
    .locals 8

    const/4 v3, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Lhhi;->a()Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    invoke-static {v4, v3}, Lhhg;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    :goto_0
    return v0

    :cond_0
    :try_start_2
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iget v2, p0, Lhhg;->a:I

    invoke-direct {v5, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    new-instance v2, Ljava/io/BufferedOutputStream;

    iget v1, p0, Lhhg;->a:I

    invoke-direct {v2, p2, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget v1, p0, Lhhg;->a:I

    div-int/lit8 v1, v1, 0x8

    new-array v6, v1, [B

    if-gtz p3, :cond_4

    const p3, 0x7fffffff

    move v1, v0

    move v3, v0

    :cond_1
    :goto_1
    const/4 v7, -0x1

    if-eq v3, v7, :cond_3

    invoke-virtual {v5, v6}, Ljava/io/BufferedInputStream;->read([B)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v3

    add-int/2addr v1, v3

    if-le v1, p3, :cond_2

    invoke-static {v4, v2}, Lhhg;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_2
    if-lez v3, :cond_1

    const/4 v7, 0x0

    :try_start_4
    invoke-virtual {v2, v6, v7, v3}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v3, v4

    :goto_2
    :try_start_5
    const-string v4, "HttpDownloader"

    const-string v5, "Exception downloading image"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    invoke-static {v3, v2}, Lhhg;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_3
    invoke-static {v4, v2}, Lhhg;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v4, v3

    :goto_3
    invoke-static {v4, v3}, Lhhg;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v3, v2

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v4, v3

    move-object v3, v2

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v2, v3

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v2, v3

    move-object v3, v4

    goto :goto_2

    :cond_4
    move v1, v0

    move v3, v0

    goto :goto_1
.end method
