.class public final Lemm;
.super Lemj;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lemj;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lemm;->c:Ljava/util/Map;

    return-void
.end method

.method private a(Landroid/util/Pair;)I
    .locals 3

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    const-string v1, "length must specify a value"

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v1, v0}, Lemm;->a(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    const-string v1, "Expected an integer"

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v1, v0}, Lemm;->a(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_2

    const-string v2, "length must be greater than zero"

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v2, v0}, Lemm;->a(Ljava/lang/String;I)V

    :cond_2
    return v1
.end method

.method private a(Leml;)I
    .locals 3

    iget-object v0, p1, Leml;->b:Ljava/lang/Object;

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    iget-object v1, p0, Lemm;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Corpus doesn\'t have section with such name."

    iget v2, p1, Leml;->a:I

    invoke-virtual {p0, v1, v2}, Lemm;->a(Ljava/lang/String;I)V

    :cond_0
    iget-object v1, p0, Lemm;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;)Leip;
    .locals 8

    const/4 v7, 0x1

    new-instance v2, Leip;

    invoke-direct {v2}, Leip;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    check-cast v1, Lemn;

    iget-object v1, v1, Lemn;->a:Ljava/lang/String;

    const-string v4, "length"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v0}, Lemm;->a(Landroid/util/Pair;)I

    move-result v0

    iput v0, v2, Leip;->b:I

    goto :goto_0

    :cond_0
    const-string v4, "snippeted"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    invoke-direct {p0, v0}, Lemm;->b(Leml;)Z

    move-result v0

    iput-boolean v0, v2, Leip;->c:Z

    goto :goto_0

    :cond_1
    const-string v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_2

    const-string v4, "default must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Leml;

    iget v1, v1, Leml;->a:I

    invoke-virtual {p0, v4, v1}, Lemm;->a(Ljava/lang/String;I)V

    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v4, "Expected string literal"

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget v1, v1, Leml;->a:I

    invoke-virtual {p0, v4, v1}, Lemm;->a(Ljava/lang/String;I)V

    :cond_3
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Leip;->a:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v4, "except"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_5

    const-string v4, "except must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Leml;

    iget v1, v1, Leml;->a:I

    invoke-virtual {p0, v4, v1}, Lemm;->a(Ljava/lang/String;I)V

    :cond_5
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    instance-of v1, v1, Lemn;

    if-eqz v1, :cond_6

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    invoke-direct {p0, v0}, Lemm;->a(Leml;)I

    move-result v0

    shl-int v0, v7, v0

    iput v0, v2, Leip;->d:I

    goto/16 :goto_0

    :cond_6
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/List;

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leml;

    iget-object v5, v0, Leml;->b:Ljava/lang/Object;

    instance-of v5, v5, Lemn;

    if-nez v5, :cond_7

    const-string v5, "Expected section name"

    iget v6, v0, Leml;->a:I

    invoke-virtual {p0, v5, v6}, Lemm;->a(Ljava/lang/String;I)V

    :cond_7
    invoke-direct {p0, v0}, Lemm;->a(Leml;)I

    move-result v0

    shl-int v0, v7, v0

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_8
    iput v1, v2, Leip;->d:I

    goto/16 :goto_0

    :cond_9
    const-string v1, "Expected name or list of values"

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v1, v0}, Lemm;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_a
    const-string v1, "Unknown key"

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v1, v0}, Lemm;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_b
    return-object v2
.end method

.method private static b(Ljava/lang/String;)Leio;
    .locals 1

    new-instance v0, Leio;

    invoke-direct {v0}, Leio;-><init>()V

    iput-object p0, v0, Leio;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/util/List;)Leiq;
    .locals 5

    new-instance v2, Leiq;

    invoke-direct {v2}, Leiq;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    check-cast v1, Lemn;

    iget-object v1, v1, Lemn;->a:Ljava/lang/String;

    const-string v4, "length"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v0}, Lemm;->a(Landroid/util/Pair;)I

    move-result v0

    iput v0, v2, Leiq;->b:I

    goto :goto_0

    :cond_0
    const-string v4, "snippeted"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    invoke-direct {p0, v0}, Lemm;->b(Leml;)Z

    move-result v0

    iput-boolean v0, v2, Leiq;->c:Z

    goto :goto_0

    :cond_1
    const-string v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_2

    const-string v4, "default must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Leml;

    iget v1, v1, Leml;->a:I

    invoke-virtual {p0, v4, v1}, Lemm;->a(Ljava/lang/String;I)V

    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget-object v1, v1, Leml;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v4, "Expected string literal"

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Leml;

    iget v1, v1, Leml;->a:I

    invoke-virtual {p0, v4, v1}, Lemm;->a(Ljava/lang/String;I)V

    :cond_3
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Leiq;->d:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "Unknown key"

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v1, v0}, Lemm;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_5
    return-object v2
.end method

.method public static b(C)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v2, 0x61

    if-lt p0, v2, :cond_0

    const/16 v2, 0x7a

    if-le p0, v2, :cond_1

    :cond_0
    const/16 v2, 0x41

    if-lt p0, v2, :cond_4

    const/16 v2, 0x5a

    if-gt p0, v2, :cond_4

    :cond_1
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    const/16 v2, 0x5f

    if-ne p0, v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0

    :cond_4
    move v2, v0

    goto :goto_0
.end method

.method private b(Leml;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Leml;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/Integer;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "Expected a boolean"

    iget v1, p1, Leml;->a:I

    invoke-virtual {p0, v0, v1}, Lemm;->a(Ljava/lang/String;I)V

    move v0, v2

    goto :goto_0

    :cond_3
    instance-of v3, v0, Lemn;

    if-eqz v3, :cond_6

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    const-string v0, "Expected a boolean"

    iget v1, p1, Leml;->a:I

    invoke-virtual {p0, v0, v1}, Lemm;->a(Ljava/lang/String;I)V

    move v0, v2

    goto :goto_0

    :cond_6
    const-string v0, "Expected a boolean"

    iget v1, p1, Leml;->a:I

    invoke-virtual {p0, v0, v1}, Lemm;->a(Ljava/lang/String;I)V

    move v0, v2

    goto :goto_0
.end method

.method public static c(C)Z
    .locals 1

    invoke-static {p0}, Lemm;->b(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lemm;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Leml;
    .locals 4

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Expected a name or $"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lemm;->b()V

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lemm;->d()C

    :cond_1
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    invoke-static {v0}, Lemm;->b(C)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "Expected a name"

    iget v1, p0, Lemj;->b:I

    invoke-virtual {p0, v0, v1}, Lemj;->a(Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {p0}, Lemm;->d()C

    :goto_0
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    invoke-static {v0}, Lemm;->c(C)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lemm;->d()C

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lemm;->c()Leml;

    move-result-object v0

    new-instance v1, Leml;

    iget v2, v0, Leml;->a:I

    new-instance v3, Lemn;

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0}, Lemn;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Leml;-><init>(ILjava/lang/Object;)V

    return-object v1
.end method

.method private j()Leml;
    .locals 5

    const/16 v4, 0x5c

    const/16 v3, 0x22

    invoke-virtual {p0}, Lemm;->f()V

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Expected a value"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    invoke-static {v0}, Lemm;->a(C)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lemm;->g()Leml;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    invoke-static {v0}, Lemm;->b(C)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x24

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-direct {p0}, Lemm;->i()Leml;

    move-result-object v0

    goto :goto_0

    :cond_4
    if-ne v0, v3, :cond_a

    invoke-virtual {p0}, Lemm;->d()C

    iget v1, p0, Lemj;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lemj;->b()V

    :goto_1
    invoke-virtual {p0}, Lemj;->a()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lemj;->e()C

    move-result v0

    if-eq v0, v3, :cond_8

    invoke-virtual {p0}, Lemj;->e()C

    move-result v0

    if-ne v0, v4, :cond_7

    invoke-virtual {p0}, Lemj;->c()Leml;

    move-result-object v0

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lemj;->d()C

    invoke-virtual {p0}, Lemj;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Expected an escape sequence"

    invoke-virtual {p0, v0}, Lemj;->a(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lemj;->e()C

    move-result v0

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const-string v0, "Expected \" or \\"

    invoke-virtual {p0, v0}, Lemj;->a(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lemj;->b()V

    :cond_7
    invoke-virtual {p0}, Lemj;->d()C

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lemj;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Expected string literal end"

    invoke-virtual {p0, v0}, Lemj;->a(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lemj;->c()Leml;

    move-result-object v0

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lemj;->d()C

    new-instance v0, Leml;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Leml;-><init>(ILjava/lang/Object;)V

    goto :goto_0

    :cond_a
    const/16 v1, 0x28

    if-ne v0, v1, :cond_b

    invoke-direct {p0}, Lemm;->k()Leml;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    const-string v0, "Expected a value"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private k()Leml;
    .locals 6

    const/16 v5, 0x2c

    const/16 v4, 0x29

    iget v1, p0, Lemj;->a:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    const/16 v3, 0x28

    if-eq v0, v3, :cond_1

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-ne v0, v5, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-virtual {p0}, Lemm;->d()C

    invoke-virtual {p0}, Lemm;->f()V

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-eq v0, v4, :cond_2

    invoke-direct {p0}, Lemm;->j()Leml;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lemm;->f()V

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-eq v0, v5, :cond_0

    :cond_2
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-eq v0, v4, :cond_4

    :cond_3
    const-string v0, "Expected )"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lemm;->d()C

    new-instance v0, Leml;

    invoke-direct {v0, v1, v2}, Leml;-><init>(ILjava/lang/Object;)V

    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final h()Leio;
    .locals 9

    const/16 v5, 0x29

    const/4 v8, 0x0

    const/16 v1, 0x25

    const/16 v7, 0x24

    invoke-virtual {p0}, Lemm;->b()V

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-ne v0, v1, :cond_15

    invoke-virtual {p0}, Lemm;->d()C

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Expected % or a section name"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lemm;->d()C

    const-string v0, "%"

    invoke-static {v0}, Lemm;->b(Ljava/lang/String;)Leio;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-eq v0, v7, :cond_2

    invoke-static {v0}, Lemm;->b(C)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Expected a section name or special section name"

    invoke-virtual {p0, v0}, Lemm;->a(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lemm;->i()Leml;

    move-result-object v3

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    const/16 v1, 0x28

    if-eq v0, v1, :cond_5

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    :goto_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Leml;

    iget-object v1, v0, Leml;->b:Ljava/lang/Object;

    check-cast v1, Lemn;

    iget-object v1, v1, Lemn;->a:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v0, Leml;->b:Ljava/lang/Object;

    check-cast v1, Lemn;

    iget-object v1, v1, Lemn;->a:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " has already been specified"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v6, v0, Leml;->a:I

    invoke-virtual {p0, v1, v6}, Lemm;->a(Ljava/lang/String;I)V

    :cond_4
    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lemm;->d()C

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_6
    :goto_3
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {p0}, Lemm;->e()C

    move-result v1

    if-eq v1, v5, :cond_e

    invoke-virtual {p0}, Lemm;->e()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lemm;->d()C

    goto :goto_3

    :cond_7
    if-eq v1, v7, :cond_8

    invoke-static {v1}, Lemm;->b(C)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_8
    invoke-direct {p0}, Lemm;->i()Leml;

    move-result-object v1

    invoke-virtual {p0}, Lemm;->f()V

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "Expected a ) or :"

    invoke-virtual {p0, v2}, Lemm;->a(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lemm;->e()C

    move-result v2

    const/16 v4, 0x3a

    if-ne v2, v4, :cond_b

    invoke-virtual {p0}, Lemm;->d()C

    invoke-direct {p0}, Lemm;->j()Leml;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-virtual {p0}, Lemm;->f()V

    invoke-virtual {p0}, Lemm;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "Expected a , or )"

    invoke-virtual {p0, v1}, Lemm;->a(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lemm;->e()C

    move-result v1

    const/16 v2, 0x2c

    if-ne v1, v2, :cond_c

    invoke-virtual {p0}, Lemm;->d()C

    goto :goto_3

    :cond_b
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    if-eq v1, v5, :cond_6

    const-string v1, "Expected a , or )"

    invoke-virtual {p0, v1}, Lemm;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_d
    const-string v1, "Expected a name"

    invoke-virtual {p0, v1}, Lemm;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_e
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "Expected a )"

    invoke-virtual {p0, v1}, Lemm;->a(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lemm;->d()C

    move-object v2, v0

    goto/16 :goto_1

    :cond_10
    new-instance v1, Leio;

    invoke-direct {v1}, Leio;-><init>()V

    iget-object v0, v3, Leml;->b:Ljava/lang/Object;

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v7, :cond_14

    const-string v4, "$uri"

    iget-object v0, v3, Leml;->b:Ljava/lang/Object;

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    const-string v3, "Unknown key"

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Leml;

    iget v0, v0, Leml;->a:I

    invoke-virtual {p0, v3, v0}, Lemm;->a(Ljava/lang/String;I)V

    :cond_11
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, v1, Leio;->d:Leir;

    :goto_5
    move-object v0, v1

    goto/16 :goto_0

    :cond_12
    const-string v4, "$bestmatch"

    iget-object v0, v3, Leml;->b:Ljava/lang/Object;

    check-cast v0, Lemn;

    iget-object v0, v0, Lemn;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-direct {p0, v2}, Lemm;->a(Ljava/util/List;)Leip;

    move-result-object v0

    iput-object v0, v1, Leio;->c:Leip;

    goto :goto_5

    :cond_13
    const-string v0, "Not a valid special section name"

    iget v2, v3, Leml;->a:I

    invoke-virtual {p0, v0, v2}, Lemm;->a(Ljava/lang/String;I)V

    goto :goto_5

    :cond_14
    invoke-direct {p0, v3}, Lemm;->a(Leml;)I

    move-result v0

    invoke-direct {p0, v2}, Lemm;->b(Ljava/util/List;)Leiq;

    move-result-object v2

    iput v0, v2, Leiq;->a:I

    iput-object v2, v1, Leio;->b:Leiq;

    goto :goto_5

    :cond_15
    :goto_6
    invoke-virtual {p0}, Lemm;->a()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-virtual {p0}, Lemm;->e()C

    move-result v0

    if-eq v0, v1, :cond_16

    invoke-virtual {p0}, Lemm;->d()C

    goto :goto_6

    :cond_16
    invoke-virtual {p0}, Lemm;->c()Leml;

    move-result-object v0

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lemm;->b(Ljava/lang/String;)Leio;

    move-result-object v0

    goto/16 :goto_0
.end method
