.class final Lhwh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field final c:Z

.field public final d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhwh;->a:Ljava/lang/String;

    iput p2, p0, Lhwh;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwh;->c:Z

    iput-wide p3, p0, Lhwh;->d:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhwh;->a:Ljava/lang/String;

    iput v0, p0, Lhwh;->b:I

    iput-boolean v0, p0, Lhwh;->c:Z

    iput-wide p2, p0, Lhwh;->d:J

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lhwh;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lhwh;

    iget-object v1, p0, Lhwh;->a:Ljava/lang/String;

    iget-object v2, p1, Lhwh;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lirf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lhwh;->b:I

    iget v2, p1, Lhwh;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lhwh;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
