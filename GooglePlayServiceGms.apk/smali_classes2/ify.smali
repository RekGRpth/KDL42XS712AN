.class public final Lify;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lhuf;


# instance fields
.field private final b:Ligx;

.field private final c:[Ljava/lang/String;

.field private final d:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lifz;

    invoke-direct {v0}, Lifz;-><init>()V

    sput-object v0, Lify;->a:Lhuf;

    return-void
.end method

.method public constructor <init>(Ligx;[Ljava/lang/String;[F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lify;->b:Ligx;

    iput-object p2, p0, Lify;->c:[Ljava/lang/String;

    iput-object p3, p0, Lify;->d:[F

    return-void
.end method


# virtual methods
.method public final a(Ligc;Ljava/lang/String;J)Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lify;->c:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Lcom/google/android/gms/location/places/internal/PlaceImpl;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lify;->c:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lify;->c:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p1, v3, p3, p4}, Ligc;->a(Ljava/lang/String;J)Liga;

    move-result-object v3

    if-nez v3, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v3, p2}, Liga;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v0, v1

    goto :goto_1

    :cond_1
    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lify;->d:[F

    invoke-static {v2, v0, p3, p4}, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->a([Lcom/google/android/gms/location/places/internal/PlaceImpl;[FJ)Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;

    move-result-object v0

    goto :goto_1
.end method

.method public final a()Ligx;
    .locals 1

    iget-object v0, p0, Lify;->b:Ligx;

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lify;->c:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b(I)F
    .locals 1

    iget-object v0, p0, Lify;->d:[F

    aget v0, v0, p1

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lify;->c:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
