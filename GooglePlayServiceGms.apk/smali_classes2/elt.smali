.class public final Lelt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lelu;

.field public final b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lelu;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lelt;->a:Lelu;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lelt;->b:Ljava/util/Map;

    invoke-direct {p0}, Lelt;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lelt;->a:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    invoke-virtual {v0}, Lejk;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lelt;->c(Ljava/lang/String;)Lels;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lely;)Lels;
    .locals 2

    iget-object v1, p1, Lely;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-virtual {p0, v1}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lelv;
    .locals 7

    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p0, Lelt;->a:Lelu;

    iget-object v2, v2, Lelu;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v2, v1, :cond_0

    move v2, v0

    :goto_0
    iget-object v3, p0, Lelt;->a:Lelu;

    iget-object v3, v3, Lelu;->b:Landroid/content/Context;

    const-string v4, "com.google.android.googlequicksearchbox"

    invoke-static {v3, v1, v4}, Lbox;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lelt;->a:Lelu;

    invoke-virtual {v3}, Lelu;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v0

    :goto_1
    iget-object v4, p0, Lelt;->a:Lelu;

    iget-object v4, v4, Lelu;->b:Landroid/content/Context;

    const-string v6, "com.google.android.apps.icing.ui"

    invoke-static {v4, v1, v6}, Lbox;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lelt;->a:Lelu;

    invoke-virtual {v4}, Lelu;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v0

    :goto_2
    new-instance v0, Lelv;

    invoke-direct/range {v0 .. v5}, Lelv;-><init>(IZZZZ)V

    return-object v0

    :cond_0
    move v2, v5

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_2
.end method

.method public final a(ILjava/lang/String;)Lely;
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lelt;->a:Lelu;

    iget-object v2, v2, Lelu;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v2, p1, :cond_0

    move v2, v0

    :goto_0
    const-string v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lelt;->a:Lelu;

    invoke-virtual {v3}, Lelu;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v0

    :goto_1
    const-string v4, "com.google.android.apps.icing.ui"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lelt;->a:Lelu;

    invoke-virtual {v4}, Lelu;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v0

    :goto_2
    iget-object v0, p0, Lelt;->a:Lelu;

    invoke-virtual {v0, p2}, Lelu;->a(Ljava/lang/String;)Z

    move-result v5

    new-instance v0, Lely;

    move v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lely;-><init>(IZZZZLjava/lang/String;)V

    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    move v4, v1

    goto :goto_2
.end method

.method public final a(Landroid/content/pm/ApplicationInfo;)Lely;
    .locals 2

    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lelt;->a(ILjava/lang/String;)Lely;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lely;
    .locals 3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v0, p0, Lelt;->a:Lelu;

    iget-object v2, v0, Lelu;->b:Landroid/content/Context;

    const-string v0, "Package name"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v1, p1}, Lelt;->a(ILjava/lang/String;)Lely;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lelv;)Ljava/util/Set;
    .locals 4

    iget-object v0, p0, Lelt;->a:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    iget v1, p1, Lelv;->a:I

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "No packages found for UID %d"

    iget v1, p1, Lelv;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/util/HashSet;

    array-length v0, v2

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-virtual {p0, v3}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 9

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    const-string v0, "\nRegistered client info:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    iget-object v0, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    iget-object v3, v0, Lels;->j:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v4, "\nInfo for package %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lels;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v4, v0, Lels;->b:Lelu;

    iget-object v4, v4, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v5, v0, Lels;->a:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    const-string v5, "\n  version code: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p1, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v5, "\n  version name: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v4, v6, v7

    invoke-virtual {p1, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    const-string v4, "\n  fingerprint: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lels;->f:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v4, "\n  resource fingerprint: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lels;->g:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\n  blocked:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v0, Lels;->e:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "\n  global search info:"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "\n    sourced at %d %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lels;->d:Lema;

    invoke-virtual {v7}, Lema;->c()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lels;->d:Lema;

    invoke-virtual {v7}, Lema;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v4, "\n    %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v4

    :try_start_5
    const-string v4, "\n  failed to get package info for this client."

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_0
    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-void
.end method

.method public final b(Ljava/lang/String;)Lels;
    .locals 2

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Ljava/util/Set;
    .locals 5

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    iget-object v0, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    iget-object v0, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {v0}, Lels;->b()I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final c(Ljava/lang/String;)Lels;
    .locals 3

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lels;

    iget-object v2, p0, Lelt;->a:Lelu;

    invoke-direct {v0, p1, v2}, Lels;-><init>(Ljava/lang/String;Lelu;)V

    iget-object v2, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()Ljava/util/Set;
    .locals 3

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Ljava/util/Set;
    .locals 3

    iget-object v1, p0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lelt;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lema;->d()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    iget-object v2, v2, Lels;->d:Lema;

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v3, p0, Lelt;->a:Lelu;

    iget-object v3, v3, Lelu;->c:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    invoke-virtual {v2}, Lema;->c()J

    move-result-wide v4

    iget-wide v2, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v2, v4, v2

    if-gez v2, :cond_3

    const-string v2, "GSAI from package %s is stale."

    invoke-static {v2, p1}, Lehe;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "Cannot find package %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-static {v2, v3, v1}, Lehe;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method
