.class public final Lhtz;
.super Lhtf;
.source "SourceFile"


# instance fields
.field private final n:I

.field private final o:I

.field private final p:I


# direct methods
.method public constructor <init>(JIIIIIIILjava/util/Collection;)V
    .locals 14

    const/4 v4, 0x4

    if-nez p10, :cond_0

    sget-object v8, Lhtz;->a:Ljava/util/Collection;

    :goto_0
    const/4 v9, -0x1

    const/4 v10, -0x1

    const v11, 0x7fffffff

    const v12, 0x7fffffff

    move-object v1, p0

    move-wide v2, p1

    move/from16 v5, p5

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v13, p9

    invoke-direct/range {v1 .. v13}, Lhtf;-><init>(JIIIILjava/util/Collection;IIIII)V

    move/from16 v0, p6

    iput v0, p0, Lhtz;->n:I

    move/from16 v0, p7

    iput v0, p0, Lhtz;->o:I

    move/from16 v0, p8

    iput v0, p0, Lhtz;->p:I

    return-void

    :cond_0
    move-object/from16 v8, p10

    goto :goto_0
.end method


# virtual methods
.method public final a(JI)Lhtf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lhtz;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "6:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhtz;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtz;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtz;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtz;->m:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhtz;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Livi;)V
    .locals 2

    iget v0, p0, Lhtz;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhtz;->p:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x7

    iget v1, p0, Lhtz;->p:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    :cond_0
    const/16 v0, 0xb

    iget v1, p0, Lhtz;->n:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    const/16 v0, 0xc

    iget v1, p0, Lhtz;->o:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    return-void
.end method

.method public final a(Lhtf;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lhtz;

    if-eqz v1, :cond_0

    check-cast p1, Lhtz;

    iget v1, p0, Lhtz;->n:I

    iget v2, p1, Lhtz;->n:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtz;->o:I

    iget v2, p1, Lhtz;->o:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " pci: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhtz;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tac "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtz;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timingAdvance "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtz;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lhtz;

    if-eqz v1, :cond_0

    check-cast p1, Lhtz;

    invoke-super {p0, p1}, Lhtf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lhtz;->n:I

    iget v2, p1, Lhtz;->n:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhtz;->o:I

    iget v2, p1, Lhtz;->o:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    invoke-super {p0}, Lhtf;->hashCode()I

    move-result v0

    iget v1, p0, Lhtz;->n:I

    mul-int/lit16 v1, v1, 0x1b65

    xor-int/2addr v0, v1

    iget v1, p0, Lhtz;->o:I

    mul-int/lit16 v1, v1, 0xb7b

    xor-int/2addr v0, v1

    return v0
.end method
