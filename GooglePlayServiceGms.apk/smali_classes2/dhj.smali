.class public final Ldhj;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ldhk;

.field public c:Ldhl;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ldhj;->a:I

    iput-object v1, p0, Ldhj;->b:Ldhk;

    iput-object v1, p0, Ldhj;->c:Ldhl;

    const/4 v0, -0x1

    iput v0, p0, Ldhj;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Ldhj;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ldhj;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ldhj;->b:Ldhk;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ldhj;->b:Ldhk;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ldhj;->c:Ldhl;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ldhj;->c:Ldhl;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Ldhj;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ldhj;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldhj;->b:Ldhk;

    if-nez v0, :cond_1

    new-instance v0, Ldhk;

    invoke-direct {v0}, Ldhk;-><init>()V

    iput-object v0, p0, Ldhj;->b:Ldhk;

    :cond_1
    iget-object v0, p0, Ldhj;->b:Ldhk;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldhj;->c:Ldhl;

    if-nez v0, :cond_2

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldhj;->c:Ldhl;

    :cond_2
    iget-object v0, p0, Ldhj;->c:Ldhl;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Ldhj;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ldhj;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Ldhj;->b:Ldhk;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ldhj;->b:Ldhk;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Ldhj;->c:Ldhl;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ldhj;->c:Ldhl;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ldhj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ldhj;

    iget v2, p0, Ldhj;->a:I

    iget v3, p1, Ldhj;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ldhj;->b:Ldhk;

    if-nez v2, :cond_4

    iget-object v2, p1, Ldhj;->b:Ldhk;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ldhj;->b:Ldhk;

    iget-object v3, p1, Ldhj;->b:Ldhk;

    invoke-virtual {v2, v3}, Ldhk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ldhj;->c:Ldhl;

    if-nez v2, :cond_6

    iget-object v2, p1, Ldhj;->c:Ldhl;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ldhj;->c:Ldhl;

    iget-object v3, p1, Ldhj;->c:Ldhl;

    invoke-virtual {v2, v3}, Ldhl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Ldhj;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldhj;->b:Ldhk;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldhj;->c:Ldhl;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ldhj;->b:Ldhk;

    invoke-virtual {v0}, Ldhk;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ldhj;->c:Ldhl;

    invoke-virtual {v1}, Ldhl;->hashCode()I

    move-result v1

    goto :goto_1
.end method
