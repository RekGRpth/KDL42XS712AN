.class public final Lcaf;
.super Lcag;
.source "SourceFile"


# static fields
.field private static d:[Ljava/lang/CharSequence;


# instance fields
.field private e:[Lbzz;

.field private final f:Ljava/util/Calendar;

.field private final g:Lcay;

.field private final h:Landroid/content/Context;

.field private final i:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcaf;->d:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcoy;Lcdp;Lcah;)V
    .locals 3

    invoke-virtual {p2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcag;-><init>(Ljava/lang/String;Lcah;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcaf;->e:[Lbzz;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Lcoy;->a()Lcon;

    move-result-object v1

    invoke-interface {v1}, Lcon;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    new-instance v1, Lcay;

    invoke-direct {v1, v0}, Lcay;-><init>(Ljava/util/Calendar;)V

    iput-object v1, p0, Lcaf;->g:Lcay;

    iput-object v0, p0, Lcaf;->f:Ljava/util/Calendar;

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcaf;->h:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdp;

    iput-object v0, p0, Lcaf;->i:Lcdp;

    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 8

    const/4 v7, 0x5

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    const/high16 v4, 0x7f0f0000    # com.google.android.gms.R.plurals.drive_fast_scroll_time_grouper_n_days_ago

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    const/4 v3, -0x1

    invoke-virtual {v0, v7, v3}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 8

    const/4 v7, -0x1

    const/4 v6, 0x2

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sget-object v4, Lcaf;->d:[Ljava/lang/CharSequence;

    aget-object v3, v4, v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 10

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_0

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    :cond_0
    move v2, v3

    :goto_0
    const/4 v4, 0x3

    if-ge v2, v4, :cond_1

    const-string v4, "yyyy"

    invoke-static {v4, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-static {p2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 3

    new-instance v2, Lbzz;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lbzz;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private static b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 7

    const/4 v6, 0x2

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    :goto_0
    if-gtz v1, :cond_0

    const v3, 0x7f0f0001    # com.google.android.gms.R.plurals.drive_fast_scroll_time_grouper_n_weeks_ago

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p1, v3, v6, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    const/4 v3, 0x5

    const/4 v4, -0x7

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private declared-synchronized g()[Lbzz;
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcaf;->d:[Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcaf;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0e0000    # com.google.android.gms.R.array.drive_fast_scroll_time_grouper_in_month

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    sput-object v1, Lcaf;->d:[Ljava/lang/CharSequence;

    array-length v1, v1

    iget-object v2, p0, Lcaf;->f:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Insufficient number of months in the resources"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcaf;->e:[Lbzz;

    if-nez v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcaf;->f:Ljava/util/Calendar;

    iget-object v2, p0, Lcaf;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0048    # com.google.android.gms.R.string.drive_fast_scroll_time_grouper_today

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/16 v3, 0xb

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xc

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const v3, 0x7f0b0049    # com.google.android.gms.R.string.drive_fast_scroll_time_grouper_yesterday

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/4 v3, 0x5

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    invoke-static {v1, v2, v0}, Lcaf;->a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcaf;->b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v1, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    const v3, 0x7f0b0047    # com.google.android.gms.R.string.drive_fast_scroll_time_grouper_in_year

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lcaf;->a(Ljava/util/ArrayList;Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    const v3, 0x7f0b0046    # com.google.android.gms.R.string.drive_fast_scroll_time_grouper_earlier

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcaf;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbzz;

    iput-object v0, p0, Lcaf;->e:[Lbzz;

    iget-object v0, p0, Lcaf;->e:[Lbzz;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcaf;->e:[Lbzz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcar;
    .locals 12

    const/4 v7, 0x0

    const-wide/16 v2, -0x1

    const/4 v6, 0x1

    iget v8, p0, Lcag;->b:I

    iget-object v0, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v4, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    :goto_0
    iget-object v4, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-object v9, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->moveToPrevious()Z

    :goto_1
    iget-object v9, p0, Lcaf;->g:Lcay;

    iget-object v10, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcay;->a(J)Lcax;

    move-result-object v8

    cmp-long v10, v0, v2

    if-nez v10, :cond_2

    move v1, v6

    :goto_2
    cmp-long v0, v4, v2

    if-nez v0, :cond_4

    move v0, v6

    :goto_3
    invoke-virtual {v8, v1, v0}, Lcax;->a(ZZ)Lcar;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v0, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-wide v0, v2

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcaf;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToLast()Z

    move-wide v4, v2

    goto :goto_1

    :cond_2
    invoke-virtual {v9, v0, v1}, Lcay;->a(J)Lcax;

    move-result-object v0

    if-eq v8, v0, :cond_3

    move v1, v6

    goto :goto_2

    :cond_3
    move v1, v7

    goto :goto_2

    :cond_4
    invoke-virtual {v9, v4, v5}, Lcay;->a(J)Lcax;

    move-result-object v0

    if-eq v8, v0, :cond_5

    move v0, v6

    goto :goto_3

    :cond_5
    move v0, v7

    goto :goto_3
.end method

.method public final b()Landroid/widget/SectionIndexer;
    .locals 4

    new-instance v0, Lbzx;

    iget-object v1, p0, Lcaf;->c:Landroid/database/Cursor;

    iget v2, p0, Lcag;->b:I

    invoke-direct {p0}, Lcaf;->g()[Lbzz;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lbzx;-><init>(Landroid/database/Cursor;I[Lbzz;)V

    return-object v0
.end method

.method public final c()Lcdp;
    .locals 1

    iget-object v0, p0, Lcaf;->i:Lcdp;

    return-object v0
.end method
