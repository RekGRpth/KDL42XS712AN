.class public Lhuv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(JLjava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lhuv;->a:J

    iput-object p3, p0, Lhuv;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Ljava/io/PrintWriter;Lhuv;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lhuv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lhut;
    .locals 1

    iget-object v0, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    return-object v0
.end method

.method public final a(JZ)Livi;
    .locals 12

    iget-object v0, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Livi;

    sget-object v0, Lihj;->h:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    const/4 v0, 0x1

    iget-wide v2, p0, Lhuv;->a:J

    add-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    const/16 v0, 0x19

    iget-object v2, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    const/4 v4, 0x2

    iget-object v0, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    iget-wide v5, p0, Lhuv;->a:J

    new-instance v7, Livi;

    sget-object v8, Lihj;->f:Livk;

    invoke-direct {v7, v8}, Livi;-><init>(Livk;)V

    const/4 v8, 0x1

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Livi;->b(ILjava/lang/String;)Livi;

    const/16 v8, 0x8

    iget-wide v9, v0, Lhut;->b:J

    invoke-virtual {v7, v8, v9, v10}, Livi;->a(IJ)Livi;

    const/4 v8, 0x4

    iget v9, v0, Lhut;->d:I

    invoke-virtual {v7, v8, v9}, Livi;->e(II)Livi;

    iget-short v8, v0, Lhut;->e:S

    invoke-static {v7, v8}, Lhsn;->a(Livi;I)V

    if-eqz p3, :cond_1

    const/4 v8, 0x2

    iget-object v9, v0, Lhut;->c:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_1
    iget-wide v8, v0, Lhti;->a:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    const/16 v8, 0xc

    iget-wide v9, v0, Lhti;->a:J

    sub-long/2addr v5, v9

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v5

    long-to-int v0, v5

    invoke-virtual {v7, v8, v0}, Livi;->e(II)Livi;

    :cond_2
    invoke-virtual {v1, v4, v7}, Livi;->a(ILivi;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhuv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhuv;

    iget-wide v2, p0, Lhuv;->a:J

    iget-wide v4, p1, Lhuv;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lhuv;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lhuv;->b:Ljava/util/ArrayList;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lhuv;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lhuv;->b:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "WifiScan [deliveryTime="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v2, p0, Lhuv;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v0, ", devices=["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    invoke-virtual {v0}, Lhut;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    const-string v0, "]]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
