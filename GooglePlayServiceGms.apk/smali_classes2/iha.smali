.class public final Liha;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lihb;

.field public final b:Lihd;

.field public final c:Lihd;

.field public final d:Lihd;

.field public final e:Lihd;

.field public final f:Lidu;

.field public volatile g:Z


# direct methods
.method public constructor <init>(Lidu;Lids;)V
    .locals 7

    new-instance v0, Lihb;

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->c()J

    move-result-wide v3

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v5

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lihb;-><init>(Lidu;Lids;JJ)V

    invoke-direct {p0, p1, v0}, Liha;-><init>(Lidu;Lihb;)V

    return-void
.end method

.method private constructor <init>(Lidu;Lihb;)V
    .locals 12

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Liha;->g:Z

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    iput-object p1, p0, Liha;->f:Lidu;

    iput-object p2, p0, Liha;->a:Lihb;

    const/4 v0, 0x0

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    iget-object v4, p2, Lihb;->f:Lidu;

    invoke-interface {v4}, Lidu;->b()Ljava/io/File;

    move-result-object v4

    const-string v5, "cp_state"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v8, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v8}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    invoke-virtual {p2, v1, v2}, Lihb;->b(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V

    :goto_1
    invoke-virtual {p2, v1, v2}, Lihb;->a(J)V

    iget-object v0, p2, Lihb;->b:Lihd;

    iput-object v0, p0, Liha;->b:Lihd;

    iget-object v0, p2, Lihb;->c:Lihd;

    iput-object v0, p0, Liha;->c:Lihd;

    iget-object v0, p2, Lihb;->d:Lihd;

    iput-object v0, p0, Liha;->d:Lihd;

    iget-object v0, p2, Lihb;->e:Lihd;

    iput-object v0, p0, Liha;->e:Lihd;

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CollectionPolicy"

    const-string v1, "Bandwidth token bucket created: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Liha;->b:Lihd;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "CollectionPolicy"

    const-string v1, "GPS token bucket for active and smart collection created: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Liha;->c:Lihd;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "CollectionPolicy"

    const-string v1, "GPS token bucket for sensor collection created: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Liha;->d:Lihd;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "CollectionPolicy"

    const-string v1, "GPS token bucket for burst collection created: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Liha;->e:Lihd;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    :try_start_3
    sget-object v3, Lihj;->aH:Livk;

    invoke-static {v0, v3}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Livi;->d(I)J

    move-result-wide v3

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Livi;->d(I)J

    move-result-wide v5

    const-wide/16 v9, 0x0

    cmp-long v7, v5, v9

    if-gtz v7, :cond_5

    invoke-virtual {p2, v1, v2}, Lihb;->b(J)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v0, v8

    :goto_2
    :try_start_5
    invoke-virtual {p2, v1, v2}, Lihb;->b(J)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    invoke-static {v0}, Lihb;->a(Ljava/io/Closeable;)V

    goto/16 :goto_1

    :cond_5
    const/4 v7, 0x3

    :try_start_6
    invoke-virtual {v0, v7}, Livi;->f(I)Livi;

    move-result-object v7

    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Livi;->f(I)Livi;

    move-result-object v9

    const/4 v10, 0x5

    invoke-virtual {v0, v10}, Livi;->f(I)Livi;

    move-result-object v10

    const/4 v11, 0x6

    invoke-virtual {v0, v11}, Livi;->f(I)Livi;

    move-result-object v11

    iget-object v0, p2, Lihb;->b:Lihd;

    invoke-virtual/range {v0 .. v7}, Lihd;->a(JJJLivi;)V

    iget-object v0, p2, Lihb;->c:Lihd;

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Lihd;->a(JJJLivi;)V

    iget-object v0, p2, Lihb;->d:Lihd;

    move-object v7, v10

    invoke-virtual/range {v0 .. v7}, Lihd;->a(JJJLivi;)V

    iget-object v0, p2, Lihb;->e:Lihd;

    move-object v7, v11

    invoke-virtual/range {v0 .. v7}, Lihd;->a(JJJLivi;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_8
    invoke-virtual {p2, v1, v2}, Lihb;->b(J)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {p2, v1, v2}, Lihb;->b(J)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    invoke-static {v8}, Lihb;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_2
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    :catchall_3
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    :catch_3
    move-exception v3

    goto :goto_2
.end method


# virtual methods
.method public final a(Lihd;JZ)Lihe;
    .locals 9

    iget-object v0, p0, Liha;->f:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v3

    move-object v0, p1

    move-wide v1, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lihd;->b(JJZ)Lihe;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "CollectionPolicy"

    const-string v2, "Requesting: %d, tokens left: %d."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-wide v7, p1, Lihd;->e:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Liha;->a:Lihb;

    invoke-virtual {v1, v3, v4}, Lihb;->a(J)V

    :cond_1
    return-object v0
.end method

.method public final a(Lihe;J)V
    .locals 7

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2, p3}, Lihe;->a(J)J

    move-result-wide v0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "CollectionPolicy"

    const-string v3, "amountToReturn: %d, actualReturned: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Liha;->a:Lihb;

    iget-object v1, p0, Liha;->f:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lihb;->a(J)V

    goto :goto_0
.end method

.method public final declared-synchronized a(J)Z
    .locals 6

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Liha;->g:Z

    if-nez v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "CollectionPolicy"

    const-string v2, "BD precondition check result true - request size: %d - not on Cellular network."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Liha;->b:Lihd;

    iget-object v1, p0, Liha;->f:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v3

    const/4 v5, 0x1

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lihd;->a(JJZ)Z

    move-result v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "CollectionPolicy"

    const-string v2, "BD precondition check result %s: request size: %d, curent bd bucket: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Liha;->b:Lihd;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(J)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Liha;->g:Z

    if-nez v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CollectionPolicy"

    const-string v1, "collection reported: %d - not on cellular network"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Liha;->f:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-object v2, p0, Liha;->b:Lihd;

    invoke-virtual {v2, p1, p2, v0, v1}, Lihd;->a(JJ)Z

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "CollectionPolicy"

    const-string v3, "collection reported %d, new bd bucket: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Liha;->b:Lihd;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Liha;->a:Lihb;

    invoke-virtual {v2, v0, v1}, Lihb;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
