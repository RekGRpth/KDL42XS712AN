.class public final Ljca;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljcc;

.field private static final b:Ljcg;


# instance fields
.field private final c:Ljavax/crypto/Mac;

.field private final d:I

.field private final e:Ljcf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljce;

    invoke-direct {v0}, Ljce;-><init>()V

    sput-object v0, Ljca;->b:Ljcg;

    new-instance v0, Ljcb;

    invoke-direct {v0}, Ljcb;-><init>()V

    sput-object v0, Ljca;->a:Ljcc;

    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Mac;Ljcf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ljca;->c:Ljavax/crypto/Mac;

    const/4 v0, 0x6

    iput v0, p0, Ljca;->d:I

    iput-object p2, p0, Ljca;->e:Ljcf;

    return-void
.end method

.method private static b(J)[B
    .locals 1

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [[B

    iget-object v2, p0, Ljca;->e:Ljcf;

    invoke-interface {v2}, Ljcf;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljca;->b(J)[B

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljca;->b(J)[B

    move-result-object v3

    aput-object v3, v1, v2

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    iget-object v4, p0, Ljca;->c:Ljavax/crypto/Mac;

    invoke-virtual {v4, v3}, Ljavax/crypto/Mac;->update([B)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljca;->c:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v0

    sget-object v1, Ljca;->b:Ljcg;

    iget v2, p0, Ljca;->d:I

    invoke-interface {v1, v2, v0}, Ljcg;->a(I[B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
