.class public final Lhvb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhwa;


# static fields
.field private static b:Lhvb;

.field private static final c:Ljava/lang/Object;


# instance fields
.field final a:Lhvx;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/pm/PackageManager;

.field private final f:Lhuz;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/util/Map;

.field private final i:Ljava/util/HashMap;

.field private j:Lhon;

.field private final k:Lhwn;

.field private final l:Lila;

.field private final m:Lhvo;

.field private final n:Lhvj;

.field private final o:Lild;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lhvb;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    new-instance v1, Lhuz;

    invoke-direct {v1}, Lhuz;-><init>()V

    invoke-static {p1}, Lila;->a(Landroid/content/Context;)Lila;

    move-result-object v2

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-direct {p0, p1, v1, v2, v0}, Lhvb;-><init>(Landroid/content/Context;Lhuz;Lila;Landroid/app/AlarmManager;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lhuz;Lila;Landroid/app/AlarmManager;)V
    .locals 6

    const/4 v5, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lhvb;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lhvb;->h:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhvb;->i:Ljava/util/HashMap;

    new-instance v0, Lhvo;

    invoke-direct {v0}, Lhvo;-><init>()V

    iput-object v0, p0, Lhvb;->m:Lhvo;

    invoke-static {p1}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v0

    iput-object v0, p0, Lhvb;->a:Lhvx;

    iput-object p1, p0, Lhvb;->d:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lhvb;->e:Landroid/content/pm/PackageManager;

    iput-object p2, p0, Lhvb;->f:Lhuz;

    iput-object p3, p0, Lhvb;->l:Lila;

    new-instance v0, Lhvj;

    iget-object v1, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v1}, Lhvx;->d()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, p1, p4, v1}, Lhvj;-><init>(Lhvb;Landroid/content/Context;Landroid/app/AlarmManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lhvb;->n:Lhvj;

    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0, v5, p0}, Lhvx;->a(ILhwa;)V

    new-instance v0, Lhon;

    const-class v1, Lcom/google/android/location/internal/GoogleLocationManagerService;

    new-instance v2, Lhvc;

    invoke-direct {v2, p0}, Lhvc;-><init>(Lhvb;)V

    invoke-direct {v0, p1, v1, v2, v5}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;Lhor;I)V

    iput-object v0, p0, Lhvb;->j:Lhon;

    new-instance v0, Lhwn;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lhwn;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lhvb;->k:Lhwn;

    new-instance v0, Lild;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lild;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lhvb;->o:Lild;

    const/16 v0, 0x13

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhvb;->e()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lhvd;

    new-instance v3, Landroid/os/Handler;

    iget-object v4, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v4}, Lhvx;->d()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lhvd;-><init>(Lhvb;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/os/Bundle;)Landroid/app/PendingIntent;
    .locals 1

    const-string v0, "pi"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "lr"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "pi"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lhvb;
    .locals 2

    sget-object v1, Lhvb;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhvb;->b:Lhvb;

    if-nez v0, :cond_0

    new-instance v0, Lhvb;

    invoke-direct {v0, p0}, Lhvb;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhvb;->b:Lhvb;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lhvb;->b:Lhvb;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lhvb;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v2, v3

    if-ne v2, v0, :cond_1

    aget-object p2, v3, v1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    if-eqz p2, :cond_0

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_2
    if-nez v0, :cond_0

    const-string v0, "GCoreFlp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Client passed in package name "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " which isn\'t in list of know packages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p2, 0x0

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lhvb;Lhwl;Landroid/app/PendingIntent;Z)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x3

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const-string v0, "GCoreFlp"

    const-string v1, "Not request location updates because of incomplete request."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lhvn;

    iget-object v2, p0, Lhvb;->d:Landroid/content/Context;

    iget-object v4, p1, Lhwl;->e:Ljava/util/Collection;

    move-object v1, p0

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lhvn;-><init>(Lhvb;Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/Collection;Z)V

    iget-object v1, p0, Lhvb;->h:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhvn;

    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lhvb;->b(Leqc;)V

    const-string v1, "GCoreFlp"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Replaced preexisting location request by PendingIntent"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GCoreFlp"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Adding PendingIntent request for package %s, hasFinePermissions=%s, %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v7

    const/4 v1, 0x1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    aput-object p1, v3, v1

    invoke-static {v2, v3}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-direct {p0, p1, v0, p3, p2}, Lhvb;->a(Lhwl;Leqc;ZLandroid/app/PendingIntent;)V

    iget-boolean v0, p1, Lhwl;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lhvb;->j:Lhon;

    iget-object v1, p1, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-static {v1, p2}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhon;->a(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method static synthetic a(Lhvb;Lhwl;Leqc;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhvb;->a(Lhwl;Leqc;ZLandroid/app/PendingIntent;)V

    return-void
.end method

.method private a(Lhwl;Leqc;ZLandroid/app/PendingIntent;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-wide/32 v2, 0x927c0

    iget-object v0, p0, Lhvb;->m:Lhvo;

    iget-object v1, p1, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lhvo;->a(Ljava/util/Collection;)V

    iget-object v6, p1, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    if-nez p3, :cond_1

    invoke-virtual {v6}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {v6, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/gms/location/LocationRequest;->d()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    invoke-virtual {v6, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    :cond_1
    new-instance v0, Lhvh;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lhvh;-><init>(Lhvb;Lhwl;ZLeqc;Landroid/app/PendingIntent;)V

    invoke-interface {p2}, Leqc;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v1, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhvh;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v7}, Lhvh;->a(Z)V

    :cond_2
    invoke-virtual {v0, v8}, Lhvh;->a(Z)V

    invoke-direct {p0}, Lhvb;->d()Ljava/lang/Iterable;

    move-result-object v1

    iget-object v3, p0, Lhvb;->a:Lhvx;

    iget-boolean v4, p1, Lhwl;->c:Z

    invoke-virtual {v3, v8, v1, v4}, Lhvx;->a(ILjava/lang/Iterable;Z)V

    iget-object v1, p0, Lhvb;->i:Ljava/util/HashMap;

    iget-object v3, p1, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhvm;

    if-nez v1, :cond_3

    new-instance v1, Lhvm;

    invoke-direct {v1}, Lhvm;-><init>()V

    iget-object v3, p0, Lhvb;->i:Ljava/util/HashMap;

    iget-object v4, p1, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {v6}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v3

    invoke-virtual {v6}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v5

    iget v6, v1, Lhvm;->a:I

    if-nez v6, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, v1, Lhvm;->b:J

    iput-wide v3, v1, Lhvm;->c:J

    iput-wide v3, v1, Lhvm;->f:J

    iput v5, v1, Lhvm;->d:I

    :cond_4
    iget-wide v6, v1, Lhvm;->c:J

    cmp-long v6, v3, v6

    if-gez v6, :cond_5

    iput-wide v3, v1, Lhvm;->c:J

    :cond_5
    iget v3, v1, Lhvm;->d:I

    if-ge v3, v5, :cond_6

    iput v5, v1, Lhvm;->d:I

    :cond_6
    iget v3, v1, Lhvm;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lhvm;->a:I

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lhvb;->n:Lhvj;

    invoke-virtual {v0, p2}, Lhvj;->a(Leqc;)V

    goto :goto_0
.end method

.method static synthetic a(Lhvb;)Z
    .locals 1

    iget-boolean v0, p0, Lhvb;->p:Z

    return v0
.end method

.method static synthetic b(Landroid/os/Bundle;)Lcom/google/android/gms/location/LocationRequest;
    .locals 1

    const-string v0, "lr"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/LocationRequest;

    return-object v0
.end method

.method static synthetic b(Lhvb;)V
    .locals 0

    invoke-direct {p0}, Lhvb;->e()V

    return-void
.end method

.method private b(ILjava/lang/String;Z)Z
    .locals 2

    if-eqz p3, :cond_0

    const-string v0, "android:fine_location"

    :goto_0
    iget-object v1, p0, Lhvb;->l:Lila;

    invoke-virtual {v1, v0, p1, p2}, Lila;->c(Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    const-string v0, "android:coarse_location"

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic c(Lhvb;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lhvb;->g:Ljava/util/Map;

    return-object v0
.end method

.method private static c(Landroid/location/Location;)Z
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "GCoreFlp"

    const-string v2, "Location is incomplete because provider not set."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "GCoreFlp"

    const-string v2, "Location is incomplete because accuracy not set."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    const-string v1, "GCoreFlp"

    const-string v2, "Location is incomplete because time not set."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/16 v1, 0x11

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    const-string v1, "GCoreFlp"

    const-string v2, "Location is incomplete because elapsed realtime nanos not set."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic d(Lhvb;)Lhon;
    .locals 1

    iget-object v0, p0, Lhvb;->j:Lhon;

    return-object v0
.end method

.method private d()Ljava/lang/Iterable;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    iget-object v0, v0, Lhvh;->b:Lhwl;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static synthetic e(Lhvb;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lhvb;->e:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lhvb;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhvb;->p:Z

    return-void
.end method

.method static synthetic f(Lhvb;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lhvb;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic g(Lhvb;)Lhvo;
    .locals 1

    iget-object v0, p0, Lhvb;->m:Lhvo;

    return-object v0
.end method

.method static synthetic h(Lhvb;)Lhuz;
    .locals 1

    iget-object v0, p0, Lhvb;->f:Lhuz;

    return-object v0
.end method

.method static synthetic i(Lhvb;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lhvb;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lhvb;)Lila;
    .locals 1

    iget-object v0, p0, Lhvb;->l:Lila;

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;Z)Landroid/location/Location;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lill;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lhvb;->k:Lhwn;

    iget-object v4, p0, Lhvb;->a:Lhvx;

    if-nez p3, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lhvx;->a(Z)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v3, v0, v2, p3}, Lhwn;->a(Landroid/location/Location;ZZ)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2}, Lhvb;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3, p3}, Lhvb;->b(ILjava/lang/String;Z)Z

    :cond_2
    const-string v3, "GCoreFlp"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "getCurrentLocation returned: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v3, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final a(Landroid/app/PendingIntent;)Lcom/google/android/gms/location/LocationRequest;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lhvb;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvn;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v0}, Leqc;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, v0, Lhvh;->b:Lhwl;

    iget-object v0, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v1, v0}, Lcom/google/android/gms/location/LocationRequest;-><init>(Lcom/google/android/gms/location/LocationRequest;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0}, Lhvx;->a()V

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lhvb;->n:Lhvj;

    new-instance v1, Lhve;

    invoke-direct {v1, p0, p1}, Lhve;-><init>(Lhvb;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lhvj;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x0

    invoke-static {}, Lill;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GCoreFlp"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Dropping location generated by background user."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    iget-object v1, p0, Lhvb;->k:Lhwn;

    iget-object v3, v0, Lhvh;->b:Lhwl;

    iget-boolean v3, v3, Lhwl;->d:Z

    invoke-static {v0}, Lhvh;->a(Lhvh;)Z

    move-result v4

    invoke-virtual {v1, p1, v3, v4}, Lhwn;->a(Landroid/location/Location;ZZ)Landroid/location/Location;

    move-result-object v3

    const-string v1, "GCoreFlp"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Sanitized location for clients %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lhvh;->b:Lhwl;

    iget-object v5, v5, Lhwl;->e:Ljava/util/Collection;

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-static {v1, v4}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Lhvh;->a(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lhvb;->m:Lhvo;

    iget-object v4, v0, Lhvh;->b:Lhwl;

    iget-object v4, v4, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v1, v4}, Lhvo;->c(Ljava/util/Collection;)V

    iget-object v1, v0, Lhvh;->b:Lhwl;

    iget-object v1, v1, Lhwl;->e:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lild;

    iget v5, v1, Lild;->a:I

    iget-object v1, v1, Lild;->b:Ljava/lang/String;

    invoke-static {v0}, Lhvh;->a(Lhvh;)Z

    move-result v6

    invoke-direct {p0, v5, v1, v6}, Lhvb;->b(ILjava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    iget-object v0, v0, Lhvh;->a:Leqc;

    invoke-virtual {p0, v0}, Lhvb;->a(Leqc;)V

    const-string v0, "GCoreFlp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "client died while calling listener "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    :try_start_1
    iget-object v1, v0, Lhvh;->a:Leqc;

    invoke-interface {v1, v3}, Leqc;->a(Landroid/location/Location;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 3

    invoke-static {p1}, Lhvb;->c(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCoreFlp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Injected location object missing required fields: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0, p1, p2}, Lhvx;->a(Landroid/location/Location;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V
    .locals 6

    const/4 v5, 0x1

    new-instance v1, Lild;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lild;-><init>(ILjava/lang/String;)V

    new-instance v0, Lhwl;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object v1, p1

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    iget-object v1, p0, Lhvb;->n:Lhvj;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v0, p2, v2, v5}, Lhvj;->a(Lhwl;Landroid/app/PendingIntent;IZ)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqc;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZLjava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZLjava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0, p4}, Lhvb;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lild;

    invoke-direct {v1, v0, v6}, Lild;-><init>(ILjava/lang/String;)V

    new-instance v0, Lhwl;

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object v1, p1

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    iget-object v1, p0, Lhvb;->n:Lhvj;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    move-object v2, v0

    move-object v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lhvj;->a(Lhwl;Leqc;ZILjava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZZLjava/util/Collection;)V
    .locals 7

    if-eqz p5, :cond_0

    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhvb;->o:Lild;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    :goto_0
    new-instance v0, Lhwl;

    const/4 v2, 0x0

    const/4 v5, 0x1

    move-object v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    iget-object v1, p0, Lhvb;->n:Lhvj;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    iget-object v2, p0, Lhvb;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    move-object v2, v0

    move-object v3, p2

    move v4, p4

    invoke-virtual/range {v1 .. v6}, Lhvj;->a(Lhwl;Leqc;ZILjava/lang/String;)V

    return-void

    :cond_1
    move-object v4, p5

    goto :goto_0
.end method

.method public final a(Leqc;)V
    .locals 1

    iget-object v0, p0, Lhvb;->n:Lhvj;

    invoke-virtual {v0, p1}, Lhvj;->a(Leqc;)V

    return-void
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0, p2}, Lhvx;->a(Ljava/io/PrintWriter;)V

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    monitor-enter v1

    :try_start_0
    new-instance v0, Lhvg;

    invoke-direct {v0, p0, v1, p2}, Lhvg;-><init>(Lhvb;Ljava/lang/Object;Ljava/io/PrintWriter;)V

    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v3}, Lhvx;->d()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v3, 0x3e8

    :try_start_1
    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v0, "\nThread interrupted while dumping location requests"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0, p1}, Lhvx;->b(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0}, Lhvx;->b()V

    iget-object v0, p0, Lhvb;->n:Lhvj;

    new-instance v1, Lhvf;

    invoke-direct {v1, p0}, Lhvf;-><init>(Lhvb;)V

    invoke-virtual {v0, v1}, Lhvj;->a(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhvb;->a(Z)V

    return-void
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lhvb;->n:Lhvj;

    iget-object v0, v0, Lhvj;->a:Lilp;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2, p1}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final b(Landroid/location/Location;)V
    .locals 1

    invoke-static {p1}, Lhvb;->c(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v0, p1}, Lhvx;->b(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lild;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lild;-><init>(ILjava/lang/String;)V

    new-instance v0, Lhwl;

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object v1, p1

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lhwl;-><init>(Lcom/google/android/gms/location/LocationRequest;ZZLjava/util/Collection;Z)V

    iget-object v1, p0, Lhvb;->n:Lhvj;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v0, p2, v2, p3}, Lhvj;->a(Lhwl;Landroid/app/PendingIntent;IZ)V

    return-void
.end method

.method public final b(Leqc;)V
    .locals 13

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-interface {p1}, Leqc;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v0, p0, Lhvb;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lhvb;->m:Lhvo;

    iget-object v3, v0, Lhvh;->b:Lhwl;

    iget-object v3, v3, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v2, v3}, Lhvo;->b(Ljava/util/Collection;)V

    invoke-virtual {v0, v10}, Lhvh;->a(Z)V

    invoke-direct {p0}, Lhvb;->d()Ljava/lang/Iterable;

    move-result-object v2

    iget-object v3, p0, Lhvb;->a:Lhvx;

    invoke-virtual {v3, v11, v2, v10}, Lhvx;->a(ILjava/lang/Iterable;Z)V

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lhvb;->i:Ljava/util/HashMap;

    iget-object v2, v0, Lhvh;->b:Lhwl;

    iget-object v2, v2, Lhwl;->e:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhvm;

    if-eqz v1, :cond_4

    iget v2, v1, Lhvm;->a:I

    if-gtz v2, :cond_2

    const-string v1, "GCoreFlp"

    const-string v2, "Reference counting corrupted in usage statistics."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    const-string v1, "GCoreFlp"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Removed a location request %s"

    new-array v2, v11, [Ljava/lang/Object;

    iget-object v0, v0, Lhvh;->b:Lhwl;

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v1

    const-string v1, "GCoreFlp"

    const-string v2, "Tried to remove a death link to a binder that didn\'t exist."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v2, v1, Lhvm;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lhvm;->a:I

    iget v2, v1, Lhvm;->a:I

    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, v1, Lhvm;->b:J

    sub-long/2addr v2, v4

    iget-wide v4, v1, Lhvm;->e:J

    add-long/2addr v4, v2

    iget-wide v6, v1, Lhvm;->e:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_3

    iget-wide v2, v1, Lhvm;->c:J

    iput-wide v2, v1, Lhvm;->f:J

    :goto_3
    iput-wide v4, v1, Lhvm;->e:J

    goto :goto_1

    :cond_3
    iget-wide v6, v1, Lhvm;->e:J

    iget-wide v8, v1, Lhvm;->f:J

    mul-long/2addr v6, v8

    iget-wide v8, v1, Lhvm;->c:J

    mul-long/2addr v2, v8

    add-long/2addr v2, v6

    div-long/2addr v2, v4

    iput-wide v2, v1, Lhvm;->f:J

    goto :goto_3

    :cond_4
    const-string v1, "GCoreFlp"

    const-string v2, "Couldn\'t find package statistics when removing location request."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    const-string v0, "GCoreFlp"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Attempted to remove location listener that wasn\'t found"

    new-array v1, v10, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final c()Lcom/google/android/gms/location/LocationStatus;
    .locals 3

    iget-object v0, p0, Lhvb;->a:Lhvx;

    iget-object v0, v0, Lhvx;->a:Lhvp;

    iget-object v1, v0, Lhvp;->l:Lhwr;

    const-string v2, "network"

    iget-object v0, v0, Lhvp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lhwr;->a(Z)Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/app/PendingIntent;)V
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "GCoreFlp"

    const-string v1, "Not request location updates because of incomplete request."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhvb;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvn;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhvb;->j:Lhon;

    const/4 v2, 0x0

    invoke-static {v2, p1}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhon;->b(Landroid/os/Parcelable;)V

    :cond_1
    if-nez v0, :cond_2

    const-string v0, "GCoreFlp"

    const-string v1, "Unknown pending intent to remove."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lhvb;->b(Leqc;)V

    goto :goto_0
.end method
