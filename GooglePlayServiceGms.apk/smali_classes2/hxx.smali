.class public final Lhxx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhxk;


# instance fields
.field final a:Ljava/util/ArrayList;

.field d:Lhxp;

.field private final e:I

.field private final f:Ljava/util/ArrayList;

.field private final g:Ljava/util/ArrayList;

.field private h:Lhxj;

.field private i:Landroid/location/Location;

.field private j:J

.field private k:D

.field private l:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lhxx;->g:Ljava/util/ArrayList;

    iput-object v2, p0, Lhxx;->d:Lhxp;

    new-instance v0, Lhxj;

    invoke-direct {v0}, Lhxj;-><init>()V

    iput-object v0, p0, Lhxx;->h:Lhxj;

    iput-object v2, p0, Lhxx;->i:Landroid/location/Location;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhxx;->j:J

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lhxx;->k:D

    const/4 v0, 0x0

    iput v0, p0, Lhxx;->l:I

    iput p1, p0, Lhxx;->e:I

    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lhxx;->e:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    iget-object v0, p0, Lhxx;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhxx;->f:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;
    .locals 2

    invoke-direct {p0}, Lhxx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lhxx;->d:Lhxp;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p3}, Lisp;->a(Ljava/util/Comparator;)Lisp;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lisp;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhxx;->h:Lhxj;

    invoke-virtual {v0, p1, p2, p3}, Lhxj;->a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->b()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lhxx;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget-object v2, p0, Lhxx;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "NearbyGeofenceLocationUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Removed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "from mUpdateWindow. New update window size="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Iterable;IJLandroid/location/Location;)V
    .locals 14

    new-instance v4, Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lhxx;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhxp;

    invoke-virtual {v3}, Lhxp;->a()D

    move-result-wide v7

    const-wide v9, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v7, v7, v9

    if-eqz v7, :cond_2

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v3}, Lhxp;->b()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lhxx;->f:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v5, p0, Lhxx;->e:I

    add-int/lit8 v5, v5, 0x1

    if-lt v3, v5, :cond_4

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, p2

    if-ne v3, v0, :cond_5

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Lbiq;->a(Z)V

    iget-object v3, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    sget-object v3, Lhxx;->b:Ljava/util/Comparator;

    invoke-static {v3}, Lisp;->a(Ljava/util/Comparator;)Lisp;

    move-result-object v3

    iget v5, p0, Lhxx;->e:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v4, v5}, Lisp;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v4

    const/4 v3, 0x0

    :goto_3
    iget v5, p0, Lhxx;->e:I

    if-ge v3, v5, :cond_6

    iget-object v5, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lhxx;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhxp;

    iget-object v6, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    :cond_8
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lhxp;

    invoke-virtual {v4}, Lhxp;->a()D

    move-result-wide v8

    new-instance v3, Leqa;

    invoke-direct {v3}, Leqa;-><init>()V

    const-wide/16 v5, -0x1

    const-wide/16 v10, 0x0

    cmp-long v5, v5, v10

    if-gez v5, :cond_a

    const-wide/16 v5, -0x1

    iput-wide v5, v3, Leqa;->c:J

    :goto_5
    iget-object v5, v4, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Sentinel of \'%s (%.6f, %.6f) %.0fm, %ds, %.0fm to boundary\'"

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->f()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->c()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->d()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->i()I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v10, v11

    const/4 v5, 0x5

    invoke-virtual {v4}, Lhxp;->a()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v10, v5

    invoke-static {v6, v7, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x64

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Leqa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    iput v4, v3, Leqa;->b:I

    invoke-virtual/range {p5 .. p5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {p5 .. p5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    double-to-float v8, v8

    invoke-virtual/range {v3 .. v8}, Leqa;->a(DDF)Leqa;

    new-instance v4, Lhxp;

    invoke-virtual {v3}, Leqa;->a()Lepz;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lhxp;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;Landroid/app/PendingIntent;)V

    iput-object v4, p0, Lhxx;->d:Lhxp;

    iget-object v3, p0, Lhxx;->d:Lhxp;

    move-wide/from16 v0, p3

    move-object/from16 v2, p5

    invoke-virtual {v3, v0, v1, v2}, Lhxp;->a(JLandroid/location/Location;)B

    sget-boolean v3, Lhyb;->a:Z

    if-eqz v3, :cond_9

    const-string v3, "NearbyGeofenceLocationUpdater"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Now monitoring ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "NearbyGeofenceLocationUpdater"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sentinel geofence is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lhxx;->d:Lhxp;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const/4 v3, 0x0

    iput v3, p0, Lhxx;->l:I

    iget-object v3, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-static {p1, v3}, Lhxx;->a(Ljava/lang/Iterable;Ljava/util/ArrayList;)V

    return-void

    :cond_a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const-wide/16 v10, -0x1

    add-long/2addr v5, v10

    iput-wide v5, v3, Leqa;->c:J

    goto/16 :goto_5
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/ArrayList;)V
    .locals 4

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lhxp;->e:Lhxv;

    invoke-virtual {v3}, Lhxv;->c()V

    const/4 v3, 0x0

    iput-boolean v3, v0, Lhxp;->d:Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;
    .locals 17

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    move-object/from16 v0, p0

    iget v4, v0, Lhxx;->e:I

    move/from16 v0, p2

    if-gt v0, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lhxx;->d:Lhxp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->h:Lhxj;

    move-object/from16 v5, p1

    move/from16 v6, p2

    move-wide/from16 v7, p3

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-virtual/range {v4 .. v11}, Lhxj;->a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lhxx;->l:I

    :cond_0
    :goto_0
    sget-boolean v5, Lhyb;->a:Z

    if-eqz v5, :cond_1

    const-string v5, "NearbyGeofenceLocationUpdater"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Took "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    sub-long/2addr v7, v13

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms to process location."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lhxx;->i:Landroid/location/Location;

    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhxx;->j:J

    move-wide/from16 v0, p6

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhxx;->k:D

    return-object v4

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lhxx;->e:I

    if-lt v4, v5, :cond_6

    :cond_3
    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Lbiq;->a(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lhxx;->d:Lhxp;

    if-nez v5, :cond_8

    const/4 v5, 0x1

    :goto_3
    if-ne v4, v5, :cond_9

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, Lbiq;->a(Z)V

    const/4 v4, 0x0

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v4

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhxp;

    invoke-virtual {v4}, Lhxp;->a()D

    move-result-wide v8

    const-wide v10, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v8, v8, v10

    if-nez v8, :cond_5

    iget-boolean v4, v4, Lhxp;->d:Z

    if-eqz v4, :cond_5

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    :cond_9
    const/4 v4, 0x0

    goto :goto_4

    :cond_a
    if-nez v5, :cond_d

    const/4 v4, 0x1

    move v12, v4

    :goto_6
    invoke-direct/range {p0 .. p0}, Lhxx;->b()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->d:Lhxp;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->d:Lhxp;

    iget-object v0, v4, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->c()D

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->d()D

    move-result-wide v6

    invoke-virtual/range {p5 .. p5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual/range {p5 .. p5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static/range {v4 .. v11}, Liba;->c(DDDD)D

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v6

    float-to-double v6, v6

    cmpg-double v4, v4, v6

    if-gez v4, :cond_e

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget v5, v0, Lhxx;->l:I

    const/4 v6, 0x5

    if-ge v5, v6, :cond_f

    const/4 v5, 0x1

    :goto_8
    sget-boolean v6, Lhyb;->a:Z

    if-eqz v6, :cond_b

    const-string v6, "NearbyGeofenceLocationUpdater"

    const-string v7, "geofencesAllKnown=%s, isTrackingNearestGeofences=%s, stillInsideSentinelGeofence=%s, fewConsecutiveSentinelGeofence=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    if-eqz v12, :cond_10

    if-eqz v15, :cond_10

    if-eqz v4, :cond_10

    if-eqz v5, :cond_10

    const/4 v4, 0x1

    :goto_9
    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->h:Lhxj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhxx;->g:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-wide/from16 v7, p3

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-virtual/range {v4 .. v11}, Lhxj;->a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lhxx;->d:Lhxp;

    move-wide/from16 v0, p3

    move-object/from16 v2, p5

    invoke-virtual {v5, v0, v1, v2}, Lhxp;->a(JLandroid/location/Location;)B

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p6

    invoke-virtual {v0, v1, v2, v3, v5}, Lhxx;->a(Ljava/lang/Iterable;DI)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_11

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_11

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lhxx;->d:Lhxp;

    if-ne v5, v6, :cond_11

    move-object/from16 v0, p0

    iget v5, v0, Lhxx;->l:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lhxx;->l:I

    sget-boolean v5, Lhyb;->a:Z

    if-eqz v5, :cond_c

    const-string v5, "NearbyGeofenceLocationUpdater"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Nearest is sentinel, mNumNearestIsSentinel="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v7, v0, Lhxx;->l:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_a
    if-eqz v4, :cond_0

    iget-object v5, v4, Lhxn;->c:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lhxn;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    invoke-direct/range {p0 .. p0}, Lhxx;->a()V

    goto/16 :goto_0

    :cond_d
    const/4 v4, 0x0

    move v12, v4

    goto/16 :goto_6

    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_f
    const/4 v5, 0x0

    goto/16 :goto_8

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_9

    :cond_11
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lhxx;->l:I

    goto :goto_a

    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lhxx;->h:Lhxj;

    move-object/from16 v5, p1

    move/from16 v6, p2

    move-wide/from16 v7, p3

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-virtual/range {v4 .. v11}, Lhxj;->a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;

    move-result-object v4

    invoke-direct/range {p0 .. p5}, Lhxx;->a(Ljava/lang/Iterable;IJLandroid/location/Location;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Iterable;DI)Ljava/util/List;
    .locals 1

    new-instance v0, Lhxo;

    invoke-direct {v0, p2, p3}, Lhxo;-><init>(D)V

    invoke-direct {p0, p1, p4, v0}, Lhxx;->a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 4

    const-string v0, "Location updater: NearbyGeofenceLocationUpdater."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "    Monitoring all now."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    Sentinel geofence: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhxx;->d:Lhxp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "    Monitoring:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Iterable;I)V
    .locals 8

    iget-object v0, p0, Lhxx;->h:Lhxj;

    iget-object v0, v0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget v0, p0, Lhxx;->e:I

    if-le p2, v0, :cond_0

    iget-object v0, p0, Lhxx;->i:Landroid/location/Location;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhxx;->d:Lhxp;

    const/4 v0, 0x0

    iput v0, p0, Lhxx;->l:I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhxx;->h:Lhxj;

    iget-wide v3, p0, Lhxx;->j:J

    iget-object v5, p0, Lhxx;->i:Landroid/location/Location;

    iget-wide v6, p0, Lhxx;->k:D

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v7}, Lhxj;->a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;

    iget-wide v3, p0, Lhxx;->j:J

    iget-object v5, p0, Lhxx;->i:Landroid/location/Location;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lhxx;->a(Ljava/lang/Iterable;IJLandroid/location/Location;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 2

    const/16 v0, 0x32

    sget-object v1, Lhxx;->b:Ljava/util/Comparator;

    invoke-direct {p0, p1, v0, v1}, Lhxx;->a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 4

    invoke-direct {p0}, Lhxx;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lhxx;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_2
    sget-object v0, Lhxx;->c:Ljava/util/Comparator;

    invoke-static {v0}, Lisp;->a(Ljava/util/Comparator;)Lisp;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lisp;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhxx;->h:Lhxj;

    invoke-virtual {v0, p1, p2}, Lhxj;->c(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method
