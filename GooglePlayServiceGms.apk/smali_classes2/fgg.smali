.class final Lfgg;
.super Lfgr;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:I

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    iput-object p6, p0, Lfgg;->c:Ljava/lang/String;

    iput-object p7, p0, Lfgg;->d:Ljava/lang/String;

    iput-object p8, p0, Lfgg;->e:Ljava/lang/String;

    iput-object p9, p0, Lfgg;->f:Ljava/lang/String;

    iput p10, p0, Lfgg;->g:I

    iput-object p11, p0, Lfgg;->h:Ljava/lang/String;

    iput-boolean p12, p0, Lfgg;->i:Z

    invoke-direct/range {p0 .. p5}, Lfgr;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    const/4 v13, 0x0

    iget-object v0, p0, Lfgg;->c:Ljava/lang/String;

    iget-object v1, p0, Lfgg;->d:Ljava/lang/String;

    iget-object v2, p0, Lfgg;->e:Ljava/lang/String;

    iget-object v3, p0, Lfgg;->f:Ljava/lang/String;

    iget v4, p0, Lfgg;->g:I

    iget-object v5, p0, Lfgg;->h:Ljava/lang/String;

    iget-boolean v6, p0, Lfgg;->i:Z

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v8, "account"

    invoke-virtual {v7, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "pagegaiaid"

    invoke-virtual {v7, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "localized_group_names"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const-string v11, "1"

    const v12, 0x7f0b045d    # com.google.android.gms.R.string.common_chips_label_public

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "3"

    const v12, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "4"

    const v12, 0x7f0b045f    # com.google.android.gms.R.string.common_chips_label_extended_circles

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v11, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v10}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    if-eqz v6, :cond_0

    invoke-static {p1, v1, v2, v0}, Lffu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v6, "circlevisibility"

    invoke-virtual {v7, v6, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v6

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Lfdl;->f()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v13

    const/4 v0, 0x1

    invoke-static {v3}, Lfdl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v5}, Lfdl;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v1

    const-string v0, "SELECT circle_id,name,sort_key,people_count,type,client_policies,last_modified,sync_to_contacts,for_sharing FROM circles WHERE (owner_id = ?1)AND ((?2 = \'\') OR (circle_id = ?2))AND ((?3=\'-999\') OR (?3=type) OR (?3=\'-998\' AND type != -1))AND ((?4 = \'\') OR (name like ?4 escape \'\\\')) ORDER BY sort_key"

    invoke-virtual {v6, v0, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v1, v0, v13, v7}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x25

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
