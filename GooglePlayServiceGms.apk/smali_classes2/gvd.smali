.class final Lgvd;
.super Lhgn;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final c:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/util/ArrayList;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lhgn;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lgvd;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object p2, p0, Lgvd;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lgvd;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lgvd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguq;

    invoke-interface {v0}, Lguq;->b()Lgur;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgur;->a()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgvd;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgvd;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
