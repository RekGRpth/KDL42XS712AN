.class public final Lhtb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhto;


# instance fields
.field private final a:Lhun;

.field private final b:Lhun;

.field private final c:I


# direct methods
.method public constructor <init>(Lhun;Lhun;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhtb;->a:Lhun;

    iput-object p2, p0, Lhtb;->b:Lhun;

    iput p3, p0, Lhtb;->c:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lhtb;->c:I

    return v0
.end method

.method public final a(Ljava/util/List;)Lhtp;
    .locals 11

    const/4 v10, 0x1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lhtb;->a:Lhun;

    invoke-virtual {v0, v1}, Lhun;->a(Ljava/util/Map;)[F

    move-result-object v0

    iget-object v2, p0, Lhtb;->b:Lhun;

    invoke-virtual {v2, v1}, Lhun;->a(Ljava/util/Map;)[F

    move-result-object v1

    invoke-static {v0}, Liav;->a([F)Liaw;

    move-result-object v8

    invoke-static {v1}, Liav;->a([F)Liaw;

    move-result-object v9

    iget v0, v8, Liaw;->a:F

    float-to-double v0, v0

    iget v2, v9, Liaw;->a:F

    float-to-double v2, v2

    iget v4, v8, Liaw;->a:F

    iget v5, v8, Liaw;->b:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    iget v6, v9, Liaw;->a:F

    iget v7, v9, Liaw;->b:F

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lhtn;->a(DDDD)D

    move-result-wide v0

    iget-object v2, p0, Lhtb;->a:Lhun;

    invoke-virtual {v2}, Lhun;->a()I

    move-result v2

    if-eq v2, v10, :cond_1

    iget-object v2, p0, Lhtb;->b:Lhun;

    invoke-virtual {v2}, Lhun;->a()I

    move-result v2

    if-ne v2, v10, :cond_2

    :cond_1
    const v0, 0x469c4000    # 20000.0f

    :goto_1
    new-instance v1, Lhtp;

    iget v2, v8, Liaw;->a:F

    float-to-int v2, v2

    iget v3, v9, Liaw;->a:F

    float-to-int v3, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v0}, Lhtp;-><init>(III)V

    return-object v1

    :cond_2
    const/high16 v2, 0x40000000    # 2.0f

    double-to-float v0, v0

    mul-float/2addr v0, v2

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhtb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhtb;

    iget v2, p0, Lhtb;->c:I

    iget v3, p1, Lhtb;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhtb;->a:Lhun;

    iget-object v3, p1, Lhtb;->a:Lhun;

    invoke-virtual {v2, v3}, Lhun;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhtb;->b:Lhun;

    iget-object v3, p1, Lhtb;->b:Lhun;

    invoke-virtual {v2, v3}, Lhun;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lhtb;->c:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhtb;->a:Lhun;

    invoke-virtual {v1}, Lhun;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhtb;->b:Lhun;

    invoke-virtual {v1}, Lhun;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
