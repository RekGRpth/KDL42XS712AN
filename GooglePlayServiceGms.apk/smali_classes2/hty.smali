.class public final enum Lhty;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhty;

.field public static final enum b:Lhty;

.field public static final enum c:Lhty;

.field private static final synthetic d:[Lhty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhty;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lhty;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhty;->a:Lhty;

    new-instance v0, Lhty;

    const-string v1, "NO_LOCATION"

    invoke-direct {v0, v1, v3}, Lhty;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhty;->b:Lhty;

    new-instance v0, Lhty;

    const-string v1, "CACHE_MISS"

    invoke-direct {v0, v1, v4}, Lhty;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhty;->c:Lhty;

    const/4 v0, 0x3

    new-array v0, v0, [Lhty;

    sget-object v1, Lhty;->a:Lhty;

    aput-object v1, v0, v2

    sget-object v1, Lhty;->b:Lhty;

    aput-object v1, v0, v3

    sget-object v1, Lhty;->c:Lhty;

    aput-object v1, v0, v4

    sput-object v0, Lhty;->d:[Lhty;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhty;
    .locals 1

    const-class v0, Lhty;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhty;

    return-object v0
.end method

.method public static values()[Lhty;
    .locals 1

    sget-object v0, Lhty;->d:[Lhty;

    invoke-virtual {v0}, [Lhty;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhty;

    return-object v0
.end method
