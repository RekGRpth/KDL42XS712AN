.class final Lenm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:J

.field final synthetic d:Lajk;

.field final synthetic e:Lenl;


# direct methods
.method constructor <init>(Lenl;[Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLajk;)V
    .locals 0

    iput-object p1, p0, Lenm;->e:Lenl;

    iput-object p2, p0, Lenm;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    iput-object p3, p0, Lenm;->b:Ljava/lang/String;

    iput-wide p4, p0, Lenm;->c:J

    iput-object p6, p0, Lenm;->d:Lajk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lenm;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lenm;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lenm;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v2, v2, v0

    invoke-static {v2}, Lekx;->a(Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring usage report, got bad usage info: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lehe;->e(Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lenm;->e:Lenl;

    invoke-static {v0}, Lenl;->a(Lenl;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Recording usage report from "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lenm;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->b(Ljava/lang/String;)I

    iget-object v0, p0, Lenm;->e:Lenl;

    invoke-static {v0}, Lenl;->b(Lenl;)Lemu;

    move-result-object v0

    iget-object v2, p0, Lenm;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    iget-object v3, p0, Lenm;->b:Ljava/lang/String;

    iget-wide v4, p0, Lenm;->c:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lemu;->a([Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;J)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    :try_start_0
    iget-object v2, p0, Lenm;->d:Lajk;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v2, v3}, Lajk;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    return-void

    :cond_2
    const/4 v0, 0x1

    const-string v2, "Ignoring usage report: reporting disabled."

    invoke-static {v2}, Lehe;->b(Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const/16 v0, 0x8

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "Client died during reportUsage"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lehe;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3
.end method
