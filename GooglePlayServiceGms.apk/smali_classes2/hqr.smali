.class final Lhqr;
.super Lhrx;
.source "SourceFile"

# interfaces
.implements Lhry;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhsu;Liea;ZLhqm;ZLhqq;Limb;Lilx;)V
    .locals 11

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p8

    move-object/from16 v4, p10

    move-object/from16 v5, p11

    move-object/from16 v6, p12

    invoke-direct/range {v1 .. v6}, Lhrx;-><init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object v8, p1

    move-object/from16 v9, p8

    move-object/from16 v10, p12

    invoke-direct/range {v1 .. v10}, Lhqr;->a(Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhsu;Liea;ZLandroid/content/Context;Lhqm;Lilx;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lhqr;->a:Ljava/util/List;

    move/from16 v0, p9

    iput-boolean v0, p0, Lhqr;->b:Z

    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhsu;Liea;ZLandroid/content/Context;Lhqm;Lilx;)Ljava/util/List;
    .locals 11

    invoke-static {}, Lhsn;->a()Ljava/util/ArrayList;

    move-result-object v10

    sget-object v1, Lhrz;->b:Lhrz;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "phone"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    new-instance v1, Lhqh;

    iget-object v5, p0, Lhqr;->e:Lhqq;

    iget-object v6, p0, Lhqr;->c:Limb;

    move-object/from16 v2, p7

    move-object/from16 v4, p8

    move-object/from16 v7, p9

    invoke-direct/range {v1 .. v7}, Lhqh;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lhqm;Lhqq;Limb;Lilx;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    sget-object v1, Lhrz;->a:Lhrz;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez p5, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "wifiService can\'t be null when WiFi scan is requested."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhqr;->c:Limb;

    const-string v2, "No sim card, skipping cellular scan."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-interface/range {p5 .. p5}, Liea;->D()Z

    move-result v1

    if-eqz v1, :cond_a

    if-eqz p6, :cond_9

    invoke-static {}, Lhsx;->a()Lhsx;

    move-result-object v4

    :goto_1
    new-instance v1, Lhsr;

    iget-object v5, p0, Lhqr;->e:Lhqq;

    iget-object v6, p0, Lhqr;->c:Limb;

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v7, p9

    invoke-direct/range {v1 .. v7}, Lhsr;-><init>(Landroid/content/Context;Lhqm;Lhta;Lhqq;Limb;Lilx;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    sget-object v1, Lhrz;->g:Lhrz;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    sget-object v1, Lhrz;->h:Lhrz;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v3, :cond_4

    if-eqz v4, :cond_5

    :cond_4
    const-string v1, "location"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    :try_start_0
    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lhqw;

    iget-object v7, p0, Lhqr;->e:Lhqq;

    iget-object v8, p0, Lhqr;->c:Limb;

    move-object/from16 v2, p7

    move-object v5, p4

    move-object/from16 v6, p8

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lhqw;-><init>(Landroid/content/Context;ZZLhsu;Lhqm;Lhqq;Limb;Lilx;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    invoke-static {}, Lhsn;->a()Ljava/util/ArrayList;

    move-result-object v3

    const-string v1, "sensor"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_6

    :cond_7
    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lhqr;->c:Limb;

    const-string v6, "Sensor type %d does not exit."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Limb;->a(Ljava/lang/String;)V

    :cond_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    invoke-static {}, Lhsz;->a()Lhsz;

    move-result-object v4

    goto/16 :goto_1

    :cond_a
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhqr;->c:Limb;

    const-string v2, "Wifi not enabled, skipping WIFI scan."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    :try_start_1
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhqr;->c:Limb;

    const-string v2, "GPS not enabled, skipping GPS scan."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhqr;->c:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GPS scan skipped: can\'t check GPS "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_c
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {p2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_d
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    new-instance v1, Lhsa;

    iget-object v6, p0, Lhqr;->e:Lhqq;

    iget-object v7, p0, Lhqr;->c:Limb;

    move-object/from16 v2, p7

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p8

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lhsa;-><init>(Landroid/content/Context;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhqm;Lhqq;Limb;Lilx;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    return-object v10
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Lhqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrx;

    invoke-virtual {v0, p0}, Lhrx;->a(Lhry;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lhrz;J)V
    .locals 1

    iget-object v0, p0, Lhqr;->f:Lhpr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqr;->f:Lhpr;

    invoke-virtual {v0, p1, p2, p3}, Lhpr;->a(Lhrz;J)V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 2

    iget-object v0, p0, Lhqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrx;

    invoke-virtual {v0}, Lhrx;->d()V

    goto :goto_0

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhqr;->c:Limb;

    const-string v1, "All scanner finished."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhqr;->e:Lhqq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhqr;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->a()V

    :cond_2
    iget-boolean v0, p0, Lhqr;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhrx;->d:Lhqm;

    invoke-virtual {v0}, Lhqm;->a()V

    :cond_3
    return-void
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lhqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
