.class public final Likb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lika;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/LocationReportingService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    invoke-static {p1, p2, p3, p4, p5}, Lijj;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;)V

    return-void
.end method

.method public final c(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Landroid/content/Context;)V

    return-void
.end method

.method public final d(Landroid/content/Context;)V
    .locals 4

    const v3, 0x7f0b0444    # com.google.android.gms.R.string.location_settings_location_reporting_activity_title

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "showing ambiguous notification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.location.settings.LOCATION_HISTORY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Lax;

    invoke-direct {v1, p1}, Lax;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v1

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lax;->b:Ljava/lang/CharSequence;

    const v2, 0x7f0b0429    # com.google.android.gms.R.string.location_ulr_ambiguous_setting_text

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lax;->c:Ljava/lang/CharSequence;

    const v2, 0x108008a    # android.R.drawable.stat_sys_warning

    invoke-virtual {v1, v2}, Lax;->a(I)Lax;

    move-result-object v1

    invoke-virtual {v1}, Lax;->b()Lax;

    move-result-object v1

    iput-object v0, v1, Lax;->d:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Lax;->d()Landroid/app/Notification;

    move-result-object v1

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v2, 0x1b2c42a

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final e(Landroid/content/Context;)V
    .locals 2

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v1, 0x1b2c42a

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method
