.class public final Lhxz;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lihh;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 6

    iget-object v0, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lihh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihi;

    new-instance v4, Lcom/google/android/gms/location/DetectedActivity;

    iget v2, v0, Lihi;->a:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const/4 v2, 0x4

    :goto_2
    iget v0, v0, Lihi;->b:I

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    const/4 v2, 0x0

    goto :goto_2

    :pswitch_2
    const/4 v2, 0x1

    goto :goto_2

    :pswitch_3
    const/4 v2, 0x2

    goto :goto_2

    :pswitch_4
    const/4 v2, 0x3

    goto :goto_2

    :pswitch_5
    const/4 v2, 0x5

    goto :goto_2

    :pswitch_6
    const/4 v2, 0x6

    goto :goto_2

    :cond_1
    iget-wide v4, p0, Lihh;->b:J

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    iget-wide v2, p0, Lihh;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Lihh;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lihh;

    invoke-direct {v2}, Lihh;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lihh;->a(J)Lihh;

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lihh;->b(J)Lihh;

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    new-instance v4, Lihi;

    invoke-direct {v4}, Lihi;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v1, 0x5

    :goto_2
    invoke-virtual {v4, v1}, Lihi;->a(I)Lihi;

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    invoke-virtual {v4, v0}, Lihi;->b(I)Lihi;

    invoke-virtual {v2, v4}, Lihh;->a(Lihi;)Lihh;

    goto :goto_1

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x2

    goto :goto_2

    :pswitch_3
    const/4 v1, 0x3

    goto :goto_2

    :pswitch_4
    const/4 v1, 0x4

    goto :goto_2

    :pswitch_5
    const/4 v1, 0x6

    goto :goto_2

    :pswitch_6
    const/4 v1, 0x7

    goto :goto_2

    :cond_1
    move-object v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
