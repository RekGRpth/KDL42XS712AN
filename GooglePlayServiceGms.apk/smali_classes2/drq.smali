.class public final Ldrq;
.super Ldat;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private b:Ljava/util/concurrent/CountDownLatch;

.field private c:Z

.field private d:Ldap;

.field private e:Ldrr;

.field private final f:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final g:Landroid/content/Context;

.field private h:Ldrq;

.field private i:Lcom/google/android/gms/common/server/ClientContext;

.field private j:Ldjy;

.field private k:Ljava/lang/String;

.field private l:Ldad;

.field private m:Ldad;

.field private final n:Ljava/util/HashMap;

.field private final o:Ljava/util/HashMap;

.field private final p:Lbnl;

.field private final q:Lbnl;

.field private final r:Lbnl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldrq;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldrs;Ldrq;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ldat;-><init>()V

    iput-object v1, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    iput-boolean v2, p0, Ldrq;->c:Z

    iput-object v1, p0, Ldrq;->d:Ldap;

    iput-object v1, p0, Ldrq;->e:Ldrr;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Ldrq;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iput-object v1, p0, Ldrq;->i:Lcom/google/android/gms/common/server/ClientContext;

    iput-object v1, p0, Ldrq;->j:Ldjy;

    iput-object v1, p0, Ldrq;->k:Ljava/lang/String;

    iput-object v1, p0, Ldrq;->l:Ldad;

    iput-object v1, p0, Ldrq;->m:Ldad;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldrq;->o:Ljava/util/HashMap;

    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    iput-object v0, p0, Ldrq;->p:Lbnl;

    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    iput-object v0, p0, Ldrq;->q:Lbnl;

    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    iput-object v0, p0, Ldrq;->r:Lbnl;

    iput-object p1, p0, Ldrq;->g:Landroid/content/Context;

    new-instance v0, Ldrr;

    invoke-direct {v0, p0, v2}, Ldrr;-><init>(Ldrq;B)V

    iput-object v0, p0, Ldrq;->e:Ldrr;

    iput-object p3, p0, Ldrq;->h:Ldrq;

    iget-object v0, p0, Ldrq;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Ldrq;Ldap;)Ldap;
    .locals 0

    iput-object p1, p0, Ldrq;->d:Ldap;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Ldrs;Ldrq;)Ldrq;
    .locals 5

    new-instance v0, Ldrq;

    invoke-direct {v0, p0, p1, p2}, Ldrq;-><init>(Landroid/content/Context;Ldrs;Ldrq;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, Ldrq;->g:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/games/service/RoomAndroidService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, v0, Ldrq;->g:Landroid/content/Context;

    iget-object v3, v0, Ldrq;->e:Ldrr;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "RoomServiceClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bind to RoomAndroidService failed. RoomServiceClient instance: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v0, Ldrq;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method static synthetic a(Ldrq;)Ldrq;
    .locals 1

    iget-object v0, p0, Ldrq;->h:Ldrq;

    return-object v0
.end method

.method private a(Z)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lbmx;

    iget-object v0, p0, Ldrq;->i:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    :try_start_0
    iget-object v0, p0, Ldrq;->g:Landroid/content/Context;

    invoke-virtual {v2, v0}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz p1, :cond_0

    :try_start_1
    iget-object v1, p0, Ldrq;->g:Landroid/content/Context;

    invoke-static {v1, v0}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Ldrq;->g:Landroid/content/Context;

    invoke-virtual {v2, v1}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_1
    const-string v2, "RoomServiceClient"

    const-string v3, "IOException getting authToken"

    invoke-static {v2, v3, v1}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic b(Ldrq;)V
    .locals 4

    iget-boolean v0, p0, Ldrq;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Waiting for previous tearDown to complete."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0xa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    const-string v0, "RoomServiceClient"

    const-string v1, "Previous tearDown is complete."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Gave up waiting on previous tearDown"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Ldrq;)Ldrq;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Ldrq;->h:Ldrq;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/Object;
    .locals 1

    sget-object v0, Ldrq;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Ldrq;)Ldap;
    .locals 1

    iget-object v0, p0, Ldrq;->d:Ldap;

    return-object v0
.end method

.method private d()Z
    .locals 2

    iget-object v0, p0, Ldrq;->d:Ldap;

    if-nez v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Room android service is null."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Ldrq;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, Ldrq;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method public final a(Ldad;[BLjava/lang/String;)I
    .locals 5

    const/4 v0, -0x1

    sget-object v2, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Ldrq;->d:Ldap;

    invoke-interface {v1, p2, p3}, Ldap;->a([BLjava/lang/String;)I

    move-result v1

    iget-object v3, p0, Ldrq;->o:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "RoomServiceClient"

    const-string v3, "Failed to send message."

    invoke-static {v1, v3}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a([BLjava/lang/String;[Ljava/lang/String;)I
    .locals 5

    const/4 v1, 0x0

    const/4 v0, -0x1

    sget-object v3, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Ldrq;->d:Ldap;

    invoke-interface {v2, p2}, Ldap;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    const-string v4, "Room was initiated with enableSockets option. You must use sockets to send messages."

    invoke-static {v2, v4}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v2, p0, Ldrq;->d:Ldap;

    invoke-interface {v2, p1, p3}, Ldap;->a([B[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v3

    move v0, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v1, "RoomServiceClient"

    const-string v2, "Failed to send message."

    invoke-static {v1, v2}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/gms/games/internal/ConnectionInfo;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p2}, Ldrq;->a(Z)Ljava/lang/String;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-nez v1, :cond_0

    const-string v1, "RoomServiceClient"

    const-string v2, "Error getting auth token"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "RoomServiceClient"

    const-string v3, "Google authentication error"

    invoke-static {v2, v3, v1}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    goto :goto_0

    :cond_0
    sget-object v2, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v3

    if-nez v3, :cond_1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_2
    iget-object v3, p0, Ldrq;->d:Ldap;

    invoke-interface {v3, v1, p1, p2}, Ldap;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/games/internal/ConnectionInfo;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    monitor-exit v2

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v1, "RoomServiceClient"

    const-string v3, "Room android service is not connected."

    invoke-static {v1, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldrq;->c:Z

    iget-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Room torn down; counting down latch."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

.method public final a(IILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->o:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;

    if-nez v0, :cond_0

    const-string v0, "RoomServiceClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No listener for reliable message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Ldad;->a(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Reliable message listener is not connected"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldad;Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 3

    iget-object v0, p0, Ldrq;->l:Ldad;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Overwriting room callbacks listener."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Ldrq;->l:Ldad;

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Ldrq;->d:Ldap;

    invoke-interface {v0, p2, p3}, Ldap;->a(Lcom/google/android/gms/common/data/DataHolder;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Ldad;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->m:Ldad;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Overwriting leave room listener."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Ldrq;->m:Ldad;

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Ldrq;->d:Ldap;

    invoke-interface {v0, p2}, Ldap;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1}, Ldad;->t(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1}, Ldad;->t(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Ldrq;->j:Ldjy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Called DataBroker proxy before initialize"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v5, Ldpc;

    invoke-direct {v5}, Ldpc;-><init>()V

    :try_start_0
    iget-object v0, p0, Ldrq;->r:Lbnl;

    invoke-virtual {v0, p3, v5}, Lbnl;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Ldrq;->j:Ldjy;

    iget-object v2, p0, Ldrq;->m:Ldad;

    iget-object v0, v1, Ldjy;->a:Landroid/content/Context;

    iget-object v1, v1, Ldjy;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ldpc;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ldrq;->m:Ldad;

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Unable to parse room leave diagnostics"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;[BI)V
    .locals 2

    new-instance v0, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;-><init>(Ljava/lang/String;[BI)V

    :try_start_0
    iget-object v1, p0, Ldrq;->l:Ldad;

    invoke-interface {v1, v0}, Ldad;->a(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Waiting room is not connected"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->a(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->a(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iput-object p2, p0, Ldrq;->k:Ljava/lang/String;

    iput-object p1, p0, Ldrq;->i:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v2, Ldjy;

    iget-object v3, p0, Ldrq;->g:Landroid/content/Context;

    iget-object v4, p0, Ldrq;->i:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, v4}, Ldjy;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object v2, p0, Ldrq;->j:Ldjy;

    new-instance v2, Landroid/os/Binder;

    invoke-direct {v2}, Landroid/os/Binder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Ldrq;->d:Ldap;

    invoke-interface {v3, p3, v2}, Ldap;->a(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "RoomServiceClient"

    const-string v3, "Room android service is not connected."

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final b(Ldad;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Overwriting waiting room listener."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p2}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "RoomServiceClient"

    const-string v2, "Disconnecting current room."

    invoke-static {v0, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->d:Ldap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v0, p0, Ldrq;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    iget-object v0, p0, Ldrq;->d:Ldap;

    invoke-interface {v0}, Ldap;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldrq;->d:Ldap;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v0, p0, Ldrq;->e:Ldrr;

    if-eqz v0, :cond_2

    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is disconnecting."

    invoke-static {v0, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->g:Landroid/content/Context;

    iget-object v2, p0, Ldrq;->e:Ldrr;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ldrq;->e:Ldrr;

    :goto_1
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service unavailable to disconnect."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service unavailable to unbind."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final b(Ldad;Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 3

    iget-object v0, p0, Ldrq;->l:Ldad;

    if-eqz v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Overwriting room callbacks listener."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Ldrq;->l:Ldad;

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Ldrq;->d:Ldap;

    invoke-interface {v0, p2, p3}, Ldap;->b(Lcom/google/android/gms/common/data/DataHolder;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Room android service is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1}, Ldad;->u(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1}, Ldad;->u(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final b(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->b(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->b(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1}, Ldad;->v(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1}, Ldad;->v(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final c(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->c(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->c(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1}, Ldad;->w(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1}, Ldad;->w(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final d(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->d(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->d(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1}, Ldad;->x(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1}, Ldad;->x(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final e(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->e(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->e(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, p1}, Ldad;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Listener is not connected"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v0, p1}, Ldjy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, v1, p2}, Ldad;->f(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v1, p2}, Ldad;->f(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "RoomServiceClient"

    const-string v2, "Waiting room is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "RoomServiceClient"

    const-string v2, "Listener is not connected"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final g(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Ldrq;->j:Ldjy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Called DataBroker proxy before initialize"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    array-length v0, p2

    new-array v0, v0, [Ldpf;

    :goto_1
    :try_start_0
    array-length v3, p2

    if-ge v1, v3, :cond_1

    new-instance v3, Ldpf;

    invoke-direct {v3}, Ldpf;-><init>()V

    aput-object v3, v0, v1

    iget-object v3, p0, Ldrq;->q:Lbnl;

    aget-object v4, p2, v1

    aget-object v5, v0, v1

    invoke-virtual {v3, v4, v5}, Lbnl;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Unable to parse room p2p status"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_1
    iget-object v1, p0, Ldrq;->j:Ldjy;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ldjy;->a(Ljava/lang/String;Ljava/util/List;)Ldpi;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v2

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ldpi;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Ldrq;->l:Ldad;

    invoke-interface {v0, p1}, Ldad;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v1, "Listener is not connected"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ldrq;->j:Ldjy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Called DataBroker proxy before initialize"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Ldpi;

    invoke-direct {v0}, Ldpi;-><init>()V

    :try_start_0
    iget-object v2, p0, Ldrq;->p:Lbnl;

    invoke-virtual {v2, p1, v0}, Lbnl;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Ldrq;->j:Ldjy;

    invoke-virtual {v1, v0}, Ldjy;->a(Ldpi;)Z

    move-result v1

    :goto_1
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomServiceClient"

    const-string v2, "Unable to parse room status"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final i(Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    sget-object v2, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Ldrq;->d:Ldap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldrq;->d:Ldap;

    invoke-interface {v0}, Ldap;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldrq;->k:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1
.end method

.method public final j(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Ldrq;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldad;

    if-nez v0, :cond_0

    const-string v0, "RoomServiceClient"

    const-string v1, "Unregistered nonexistent waiting room listener."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Ldrq;->d:Ldap;

    invoke-interface {v2, p1}, Ldap;->c(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "RoomServiceClient"

    const-string v3, "Room android service is not connected."

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final l(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Ldrq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Ldrq;->d()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Ldrq;->d:Ldap;

    invoke-interface {v2, p1}, Ldap;->d(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "RoomServiceClient"

    const-string v3, "Room android service is not connected."

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
