.class final Lhrl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lhqu;

.field b:Lhqm;

.field private final c:Landroid/os/Handler;

.field private d:Lhqr;

.field private e:Limb;


# direct methods
.method constructor <init>(Landroid/os/Handler;Limb;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhrl;->c:Landroid/os/Handler;

    invoke-static {p2}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhrl;->e:Limb;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lhrl;->d:Lhqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrl;->b:Lhqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrl;->b:Lhqm;

    iget-object v1, p0, Lhrl;->d:Lhqr;

    invoke-virtual {v0, v1}, Lhqm;->a(Lhrx;)V

    :cond_0
    return-void
.end method

.method final a(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;)Z
    .locals 23

    invoke-interface/range {p2 .. p2}, Lhqk;->a()Lilx;

    move-result-object v8

    sget-object v1, Lhrm;->a:[I

    invoke-interface/range {p2 .. p2}, Lhqk;->d()Lhql;

    move-result-object v2

    invoke-virtual {v2}, Lhql;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v2, Lhrf;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->e:Limb;

    move-object/from16 v0, p7

    invoke-direct {v2, v0, v1}, Lhrf;-><init>(Lhqq;Limb;)V

    :goto_0
    const/16 v22, 0x0

    new-instance v1, Lhqu;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lhqk;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lhrl;->e:Limb;

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v7}, Lhqu;-><init>(Lhrv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Livi;Limb;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lhrl;->a:Lhqu;

    new-instance v1, Lhqm;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhrl;->a:Lhqu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhrl;->c:Landroid/os/Handler;

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lhrl;->e:Limb;

    invoke-direct {v1, v2, v3, v4, v5}, Lhqm;-><init>(Lhqu;Landroid/os/Handler;ILimb;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lhrl;->b:Lhqm;

    new-instance v9, Lhqr;

    invoke-interface/range {p2 .. p2}, Lhqk;->c()Ljava/util/Set;

    move-result-object v11

    invoke-interface/range {p2 .. p2}, Lhqk;->l()Ljava/util/Map;

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Lhqk;->g()Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-result-object v13

    invoke-interface/range {p2 .. p2}, Lhqk;->i()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lhrl;->b:Lhqm;

    move-object/from16 v17, v0

    invoke-interface/range {p2 .. p2}, Lhqk;->j()Z

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lhrl;->e:Limb;

    move-object/from16 v20, v0

    move-object/from16 v10, p1

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v19, p7

    move-object/from16 v21, v8

    invoke-direct/range {v9 .. v21}, Lhqr;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhsu;Liea;ZLhqm;ZLhqq;Limb;Lilx;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lhrl;->d:Lhqr;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->e:Limb;

    const-string v2, "Real collector stared with config: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->d:Lhqr;

    invoke-virtual {v1}, Lhqr;->c()I

    move-result v1

    if-lez v1, :cond_3

    invoke-interface/range {p2 .. p2}, Lhqk;->g()Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->d:Lhqr;

    new-instance v2, Lhqt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhrl;->d:Lhqr;

    invoke-interface/range {p2 .. p2}, Lhqk;->g()Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lhrl;->b:Lhqm;

    invoke-direct {v2, v3, v4, v5}, Lhqt;-><init>(Lhrx;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhqm;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lhqr;->a(Lhpr;Lhry;)V

    :goto_1
    const/4 v1, 0x1

    :goto_2
    return v1

    :pswitch_0
    new-instance v1, Lhra;

    const-string v2, "power"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-interface/range {p2 .. p2}, Lhqk;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lhqk;->h()[B

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lhrl;->e:Limb;

    new-instance v7, Lhsd;

    invoke-interface/range {p2 .. p2}, Lhqk;->m()Z

    move-result v5

    invoke-direct {v7, v5}, Lhsd;-><init>(Z)V

    move-object/from16 v5, p7

    invoke-direct/range {v1 .. v8}, Lhra;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLhqq;Limb;Lhsd;Lilx;)V

    move-object v2, v1

    goto/16 :goto_0

    :pswitch_1
    new-instance v9, Lhrp;

    sget-object v11, Lhrs;->a:Lhrs;

    new-instance v12, Lhsd;

    invoke-interface/range {p2 .. p2}, Lhqk;->m()Z

    move-result v1

    invoke-direct {v12, v1}, Lhsd;-><init>(Z)V

    invoke-interface/range {p2 .. p2}, Lhqk;->e()Ljava/lang/String;

    move-result-object v13

    invoke-interface/range {p2 .. p2}, Lhqk;->h()[B

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lhrl;->e:Limb;

    move-object/from16 v16, v0

    move-object/from16 v10, p1

    move-object/from16 v15, p7

    move-object/from16 v17, v8

    invoke-direct/range {v9 .. v17}, Lhrp;-><init>(Landroid/content/Context;Lhrs;Lhsd;Ljava/lang/String;[BLhqq;Limb;Lilx;)V

    move-object v2, v9

    goto/16 :goto_0

    :cond_1
    invoke-interface/range {p2 .. p2}, Lhqk;->f()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->d:Lhqr;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lhqr;->a(Lhry;)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lhrl;->d:Lhqr;

    new-instance v2, Lhsj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhrl;->d:Lhqr;

    invoke-interface/range {p2 .. p2}, Lhqk;->f()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lhrl;->b:Lhqm;

    invoke-direct {v2, v3, v4, v5, v6}, Lhsj;-><init>(Lhrx;JLhqm;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lhqr;->a(Lhpr;Lhry;)V

    goto :goto_1

    :cond_3
    move/from16 v1, v22

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
