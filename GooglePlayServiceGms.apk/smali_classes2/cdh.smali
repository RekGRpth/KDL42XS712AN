.class public final enum Lcdh;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcdh;

.field public static final enum b:Lcdh;

.field public static final enum c:Lcdh;

.field public static final enum d:Lcdh;

.field public static final enum e:Lcdh;

.field private static final synthetic g:[Lcdh;


# instance fields
.field private final f:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x6

    const/4 v7, 0x1

    new-instance v0, Lcdh;

    const-string v1, "APP_PACKAGING_ID"

    invoke-static {}, Lcdg;->d()Lcdg;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "appPackagingId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcdh;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdh;->a:Lcdh;

    new-instance v0, Lcdh;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcdg;->d()Lcdg;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "accountId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcdq;->a(I)Lcdq;

    move-result-object v2

    const/16 v3, 0x9

    new-instance v4, Lcei;

    const-string v5, "accountId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v5

    invoke-virtual {v4, v5, v10}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/String;

    sget-object v6, Lcdh;->a:Lcdh;

    iget-object v6, v6, Lcdh;->f:Lcdp;

    invoke-virtual {v6}, Lcdp;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v4, v5}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcdh;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdh;->b:Lcdh;

    new-instance v0, Lcdh;

    const-string v1, "HAS_APPDATA_SYNCED"

    invoke-static {}, Lcdg;->d()Lcdg;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "hasAppDataSynced"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcdh;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdh;->c:Lcdh;

    new-instance v0, Lcdh;

    const-string v1, "APPDATA_FOLDER_ENTRY_ID"

    const/4 v2, 0x3

    invoke-static {}, Lcdg;->d()Lcdg;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "appDataFolderEntryId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v5

    invoke-virtual {v4, v5, v10}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcdh;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdh;->d:Lcdh;

    new-instance v0, Lcdh;

    const-string v1, "IS_APPDATA_FOLDER_PLACEHOLDER"

    const/4 v2, 0x4

    invoke-static {}, Lcdg;->d()Lcdg;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isAppDataFolderPlaceholder"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcdh;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdh;->e:Lcdh;

    const/4 v0, 0x5

    new-array v0, v0, [Lcdh;

    sget-object v1, Lcdh;->a:Lcdh;

    aput-object v1, v0, v9

    sget-object v1, Lcdh;->b:Lcdh;

    aput-object v1, v0, v7

    sget-object v1, Lcdh;->c:Lcdh;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, Lcdh;->d:Lcdh;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcdh;->e:Lcdh;

    aput-object v2, v0, v1

    sput-object v0, Lcdh;->g:[Lcdh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcdh;->f:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdh;
    .locals 1

    const-class v0, Lcdh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdh;

    return-object v0
.end method

.method public static values()[Lcdh;
    .locals 1

    sget-object v0, Lcdh;->g:[Lcdh;

    invoke-virtual {v0}, [Lcdh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdh;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcdh;->f:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcdh;->f:Lcdp;

    return-object v0
.end method
