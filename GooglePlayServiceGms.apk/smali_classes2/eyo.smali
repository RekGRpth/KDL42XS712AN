.class public abstract Leyo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leza;


# static fields
.field private static a:I

.field private static final b:[Leyp;

.field private static c:Ljava/util/concurrent/BlockingDeque;


# instance fields
.field private final d:Ljava/util/concurrent/Semaphore;

.field private final e:Lezl;

.field private final f:Leym;

.field private g:Leyr;

.field private h:Landroid/graphics/Bitmap;

.field private i:Z

.field private j:F

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x2

    sput v0, Leyo;->a:I

    new-array v0, v0, [Leyp;

    sput-object v0, Leyo;->b:[Leyp;

    move v0, v1

    :goto_0
    sget-object v2, Leyo;->b:[Leyp;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Leyo;->b:[Leyp;

    new-instance v3, Leyp;

    invoke-direct {v3, v1}, Leyp;-><init>(B)V

    aput-object v3, v2, v0

    sget-object v2, Leyo;->b:[Leyp;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Leyp;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    sput-object v0, Leyo;->c:Ljava/util/concurrent/BlockingDeque;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Semaphore;Lezl;Leym;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leyo;->i:Z

    const/4 v0, 0x0

    iput v0, p0, Leyo;->j:F

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Leyo;->k:J

    iput-object p1, p0, Leyo;->d:Ljava/util/concurrent/Semaphore;

    iput-object p2, p0, Leyo;->e:Lezl;

    iput-object p3, p0, Leyo;->f:Leym;

    return-void
.end method

.method static synthetic a(Leyo;)V
    .locals 2

    iget-object v0, p0, Leyo;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    invoke-virtual {p0}, Leyo;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Leyo;->h:Landroid/graphics/Bitmap;

    iget-object v0, p0, Leyo;->e:Lezl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lezl;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic g()Ljava/util/concurrent/BlockingDeque;
    .locals 1

    sget-object v0, Leyo;->c:Ljava/util/concurrent/BlockingDeque;

    return-object v0
.end method


# virtual methods
.method public final a()Leyr;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Leyo;->g:Leyr;

    if-eqz v1, :cond_1

    iget-object v0, p0, Leyo;->g:Leyr;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-boolean v1, p0, Leyo;->i:Z

    if-nez v1, :cond_0

    :try_start_0
    sget-object v1, Leyo;->c:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v1, p0}, Ljava/util/concurrent/BlockingDeque;->putLast(Ljava/lang/Object;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Leyo;->i:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    sget-object v0, Leyo;->c:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v0, p0}, Ljava/util/concurrent/BlockingDeque;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Leyo;->h:Landroid/graphics/Bitmap;

    return-void
.end method

.method public final c()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Leyo;->g:Leyr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyo;->g:Leyr;

    new-array v1, v4, [I

    iget v2, v0, Leyr;->a:I

    aput v2, v1, v3

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    const/4 v1, -0x1

    iput v1, v0, Leyr;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Leyo;->g:Leyr;

    const/4 v0, 0x0

    iput v0, p0, Leyo;->j:F

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Leyo;->k:J

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0xb

    const/4 v0, 0x0

    iget-object v1, p0, Leyo;->h:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    new-instance v1, Leyr;

    sget-object v2, Leyt;->a:Leyt;

    invoke-direct {v1, v2}, Leyr;-><init>(Leyt;)V

    iput-object v1, p0, Leyo;->g:Leyr;

    :try_start_0
    iget-object v1, p0, Leyo;->g:Leyr;

    iget-object v2, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Leyr;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Leyu; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Leyo;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object v1, p0, Leyo;->f:Leym;

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_1

    iget-object v1, p0, Leyo;->f:Leym;

    iget-object v2, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Leym;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    iput-object v4, p0, Leyo;->h:Landroid/graphics/Bitmap;

    iput-boolean v0, p0, Leyo;->i:Z

    :goto_1
    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    const-string v1, "DelayedTextureLoader"

    const-string v2, "Could not load texture"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Leyo;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object v1, p0, Leyo;->f:Leym;

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_2

    iget-object v1, p0, Leyo;->f:Leym;

    iget-object v2, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Leym;->a(Landroid/graphics/Bitmap;)V

    :goto_2
    iput-object v4, p0, Leyo;->h:Landroid/graphics/Bitmap;

    iput-boolean v0, p0, Leyo;->i:Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    :catchall_0
    move-exception v1

    iget-object v2, p0, Leyo;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object v2, p0, Leyo;->f:Leym;

    if-eqz v2, :cond_3

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v3, :cond_3

    iget-object v2, p0, Leyo;->f:Leym;

    iget-object v3, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Leym;->a(Landroid/graphics/Bitmap;)V

    :goto_3
    iput-object v4, p0, Leyo;->h:Landroid/graphics/Bitmap;

    iput-boolean v0, p0, Leyo;->i:Z

    throw v1

    :cond_3
    iget-object v2, p0, Leyo;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3
.end method

.method public final e()F
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Leyo;->g:Leyr;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Leyo;->j:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    iget-wide v1, p0, Leyo;->k:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Leyo;->k:J

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Leyo;->k:J

    sub-long/2addr v1, v3

    long-to-float v1, v1

    const/high16 v2, 0x43480000    # 200.0f

    div-float/2addr v1, v2

    iput v1, p0, Leyo;->j:F

    iget v1, p0, Leyo;->j:F

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Leyo;->j:F

    iget-object v0, p0, Leyo;->e:Lezl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lezl;->a(Ljava/lang/Object;)V

    iget v0, p0, Leyo;->j:F

    goto :goto_0
.end method

.method protected abstract f()Landroid/graphics/Bitmap;
.end method
