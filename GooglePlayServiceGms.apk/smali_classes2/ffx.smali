.class final Lffx;
.super Lfgu;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/Boolean;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Ljava/lang/String;

.field final synthetic j:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p8, p0, Lffx;->c:Ljava/lang/String;

    iput-object p9, p0, Lffx;->d:Ljava/lang/Boolean;

    iput-object p10, p0, Lffx;->e:Ljava/lang/String;

    iput-object p11, p0, Lffx;->f:Ljava/lang/String;

    iput-object p12, p0, Lffx;->g:Ljava/lang/String;

    iput-object p13, p0, Lffx;->h:Ljava/lang/String;

    iput-object p14, p0, Lffx;->i:Ljava/lang/String;

    iput-object p15, p0, Lffx;->j:Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 10

    const/4 v9, 0x0

    const/4 v2, 0x3

    const/4 v8, 0x1

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleService"

    const-string v1, "updateCircle: name=%s efs=%s desc=%s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lffx;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, p0, Lffx;->d:Ljava/lang/Boolean;

    aput-object v3, v2, v8

    const/4 v3, 0x2

    iget-object v4, p0, Lffx;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lffx;->f:Ljava/lang/String;

    iget-object v3, p0, Lffx;->c:Ljava/lang/String;

    iget-object v4, p0, Lffx;->d:Ljava/lang/Boolean;

    iget-object v5, p0, Lffx;->e:Ljava/lang/String;

    move-object v0, p2

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lffe;->a(Lffa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lgmb;

    iget-object v0, p0, Lffx;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lffx;->h:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lffx;->i:Ljava/lang/String;

    iget-object v2, p0, Lffx;->f:Ljava/lang/String;

    iget-object v3, p0, Lffx;->c:Ljava/lang/String;

    iget-object v4, p0, Lffx;->d:Ljava/lang/Boolean;

    iget-object v4, p0, Lffx;->e:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "p"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_1

    new-instance v2, Lbmt;

    invoke-direct {v2, p1}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v2

    sget-object v3, Lfnx;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v2

    sget-object v3, Lfnp;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v2

    invoke-static {v5}, Lbcl;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v2

    invoke-static {p1, v2}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    :cond_1
    if-eqz v4, :cond_2

    new-instance v2, Lbmt;

    invoke-direct {v2, p1}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v1

    sget-object v2, Lfnx;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    sget-object v2, Lfnp;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    invoke-static {v5}, Lbcl;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-static {p1, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    :cond_2
    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iget-object v1, p0, Lffx;->i:Ljava/lang/String;

    iget-object v2, p0, Lffx;->j:Ljava/lang/String;

    iget-object v3, p0, Lffx;->f:Ljava/lang/String;

    iget-object v4, p0, Lffx;->c:Ljava/lang/String;

    iget-object v5, p0, Lffx;->d:Ljava/lang/Boolean;

    invoke-virtual/range {v0 .. v5}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I

    move-result v0

    if-eq v0, v8, :cond_5

    iget-object v0, p0, Lffx;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v1, p0, Lffx;->i:Ljava/lang/String;

    iget-object v2, p0, Lffx;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v9}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_3
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    invoke-direct {v0, v1, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_4
    iget-object v0, p0, Lffx;->g:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lffx;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lffx;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lffx;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v1, p0, Lffx;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfhz;->c(Ljava/lang/String;)V

    goto :goto_1
.end method
