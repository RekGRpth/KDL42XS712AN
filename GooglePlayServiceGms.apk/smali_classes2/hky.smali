.class final Lhky;
.super Lhla;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhkr;

.field private b:J

.field private c:Z


# direct methods
.method constructor <init>(Lhkr;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lhky;->a:Lhkr;

    invoke-direct {p0, p1, v2}, Lhla;-><init>(Lhkr;B)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhky;->b:J

    iput-boolean v2, p0, Lhky;->c:Z

    return-void
.end method

.method private static a(JJ)J
    .locals 3

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    add-long v0, p0, p2

    goto :goto_0
.end method

.method private a(J)Z
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->g(Lhkr;)J

    move-result-wide v0

    const-wide v2, 0x1f3fffffc18L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->g(Lhkr;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    cmp-long v0, p1, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->g(Lhkr;)J

    move-result-wide v0

    rem-long/2addr v0, p1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->g(Lhkr;)J

    move-result-wide v0

    rem-long v0, p1, v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    const-wide/32 v0, 0x493e0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(JJJJ)Z
    .locals 7

    const-wide v2, 0x7fffffffffffffffL

    const-wide/16 v5, 0x2

    const/4 v0, 0x0

    cmp-long v1, p0, v2

    if-eqz v1, :cond_0

    cmp-long v1, p2, v2

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    div-long v1, p4, v5

    sub-long v3, p0, p2

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    div-long v5, p6, v5

    cmp-long v1, v3, v1

    if-gtz v1, :cond_0

    cmp-long v1, v3, v5

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 12

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->c(Lhkr;)J

    move-result-wide v2

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->f(Lhkr;)J

    move-result-wide v0

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    invoke-static {v2, v3, v0, v1}, Lhky;->a(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lhky;->a:Lhkr;

    invoke-static {v2}, Lhkr;->d(Lhkr;)J

    move-result-wide v4

    iget-object v2, p0, Lhky;->a:Lhkr;

    invoke-static {v2}, Lhkr;->h(Lhkr;)J

    move-result-wide v2

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v2, v2, v6

    if-nez v2, :cond_6

    const-wide v2, 0x7fffffffffffffffL

    :goto_1
    invoke-static {v4, v5, v2, v3}, Lhky;->a(JJ)J

    move-result-wide v10

    iget-object v2, p0, Lhky;->a:Lhkr;

    invoke-static {v2}, Lhkr;->e(Lhkr;)J

    move-result-wide v2

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "ActivityScheduler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "nextFullTrigger: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nextTiltOnlyTrigger: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nextLocatorTrigger: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lhky;->a:Lhkr;

    invoke-static {v4}, Lhkr;->f(Lhkr;)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lhky;->a(J)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lhky;->a:Lhkr;

    invoke-static {v4}, Lhkr;->f(Lhkr;)J

    move-result-wide v4

    iget-object v6, p0, Lhky;->a:Lhkr;

    invoke-static {v6}, Lhkr;->g(Lhkr;)J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lhky;->a(JJJJ)Z

    move-result v4

    if-eqz v4, :cond_9

    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    move-wide v8, v0

    :goto_2
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->h(Lhkr;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhky;->a(J)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->h(Lhkr;)J

    move-result-wide v4

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->g(Lhkr;)J

    move-result-wide v6

    move-wide v0, v10

    invoke-static/range {v0 .. v7}, Lhky;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_8

    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    :goto_3
    iget-wide v2, p0, Lhky;->b:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    iget-wide v2, p0, Lhky;->b:J

    const-wide/32 v4, 0xafc8

    add-long/2addr v2, v4

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    :goto_4
    const/4 v4, 0x0

    iput-boolean v4, p0, Lhky;->c:Z

    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    iget-object v4, p0, Lhky;->a:Lhkr;

    invoke-static {v4}, Lhkr;->h(Lhkr;)J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    invoke-static/range {v0 .. v7}, Lhky;->a(JJJJ)Z

    move-result v4

    if-nez v4, :cond_2

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "ActivityScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Running tilt only detector next: FullTrigger: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " TiltOnlyTrigger: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lhky;->c:Z

    move-wide v2, v0

    :cond_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "nextTriggerTime: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0, v2, v3}, Lhkr;->c(Lhkr;J)V

    return-void

    :cond_4
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {}, Lhkr;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->i(Lhkr;)Lhms;

    move-result-object v0

    :goto_5
    invoke-static {v0}, Lhkr;->a(Lhms;)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lhky;->a:Lhkr;

    invoke-static {v6}, Lhkr;->f(Lhkr;)J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->j(Lhkr;)Lhms;

    move-result-object v0

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lhky;->a:Lhkr;

    iget-object v2, p0, Lhky;->a:Lhkr;

    invoke-static {v2}, Lhkr;->k(Lhkr;)Lhms;

    move-result-object v2

    invoke-static {v2}, Lhkr;->a(Lhms;)J

    move-result-wide v2

    const-wide/16 v6, 0x0

    iget-object v8, p0, Lhky;->a:Lhkr;

    invoke-static {v8}, Lhkr;->h(Lhkr;)J

    move-result-wide v8

    sub-long v2, v8, v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto/16 :goto_1

    :cond_7
    move-wide v2, v8

    goto/16 :goto_4

    :cond_8
    move-wide v0, v10

    goto/16 :goto_3

    :cond_9
    move-wide v8, v0

    goto/16 :goto_2
.end method


# virtual methods
.method protected final a(I)V
    .locals 0

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhky;->g()V

    goto :goto_0
.end method

.method protected final a(Lhlt;)V
    .locals 7

    const/4 v1, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->l(Lhkr;)Lhlt;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v4, v0, Lhlt;->a:Lhup;

    sget-object v5, Lhup;->c:Lhup;

    if-eq v4, v5, :cond_4

    iget-object v4, p1, Lhlt;->a:Lhup;

    sget-object v5, Lhup;->c:Lhup;

    if-eq v4, v5, :cond_4

    iget-object v0, v0, Lhlt;->a:Lhup;

    iget-object v4, p1, Lhlt;->a:Lhup;

    if-eq v0, v4, :cond_4

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->m(Lhkr;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v4, p1, Lhlt;->a:Lhup;

    sget-object v5, Lhup;->c:Lhup;

    if-ne v4, v5, :cond_5

    :cond_0
    move v3, v2

    :cond_1
    :goto_1
    if-eqz v3, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Travel mode change and inconsistent with last activity detection. Running activity detection early: last="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhky;->a:Lhkr;

    invoke-static {v3}, Lhkr;->l(Lhkr;)Lhlt;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " cur="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0, v2}, Lhkr;->a(Lhkr;Z)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    iget-object v4, p1, Lhlt;->a:Lhup;

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    if-ne v0, v6, :cond_9

    :cond_6
    move v1, v3

    :goto_3
    sget-object v5, Lhup;->b:Lhup;

    if-ne v4, v5, :cond_7

    if-nez v1, :cond_1

    :cond_7
    sget-object v1, Lhup;->a:Lhup;

    if-ne v4, v1, :cond_a

    move v1, v3

    :goto_4
    if-ne v0, v6, :cond_b

    move v0, v3

    :goto_5
    if-ne v1, v0, :cond_1

    move v3, v2

    goto :goto_1

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3

    :cond_a
    move v1, v2

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_5
.end method

.method protected final a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhky;->a:Lhkr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhky;->g()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 5

    const-wide/16 v3, -0x1

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->c(Lhkr;)J

    move-result-wide v0

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, Lhky;->a:Lhkr;

    iget-object v1, p0, Lhky;->a:Lhkr;

    invoke-static {v1}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v1

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lhkr;->a(Lhkr;J)J

    :cond_0
    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->d(Lhkr;)J

    move-result-wide v0

    cmp-long v0, v0, v3

    if-nez v0, :cond_1

    iget-object v0, p0, Lhky;->a:Lhkr;

    iget-object v1, p0, Lhky;->a:Lhkr;

    invoke-static {v1}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v1

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lhkr;->b(Lhkr;J)J

    :cond_1
    invoke-direct {p0}, Lhky;->g()V

    return-void
.end method

.method protected final f()V
    .locals 4

    iget-wide v0, p0, Lhky;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lhky;->a:Lhkr;

    invoke-static {v0}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhky;->b:J

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    const-string v1, "Screen state changed. Schedule activity detection soon."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lhky;->g()V

    return-void
.end method

.method protected final j()V
    .locals 2

    iget-object v0, p0, Lhky;->a:Lhkr;

    iget-boolean v1, p0, Lhky;->c:Z

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Z)V

    return-void
.end method
