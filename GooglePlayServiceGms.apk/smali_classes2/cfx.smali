.class public final Lcfx;
.super Lcfl;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljavax/crypto/SecretKey;

.field public g:J

.field public final h:J


# direct methods
.method private constructor <init>(Lcfy;)V
    .locals 3

    iget-object v0, p1, Lcfy;->a:Lcdu;

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    iget-object v0, p1, Lcfy;->b:Ljava/lang/String;

    iput-object v0, p0, Lcfx;->a:Ljava/lang/String;

    iget-wide v0, p1, Lcfy;->f:J

    iput-wide v0, p0, Lcfx;->g:J

    iget-wide v0, p1, Lcfy;->g:J

    iput-wide v0, p0, Lcfx;->h:J

    iget-object v0, p1, Lcfy;->c:Ljava/lang/String;

    iput-object v0, p0, Lcfx;->b:Ljava/lang/String;

    iget-object v0, p1, Lcfy;->d:Ljava/lang/String;

    iput-object v0, p0, Lcfx;->c:Ljava/lang/String;

    iget-object v0, p1, Lcfy;->e:Ljavax/crypto/SecretKey;

    iput-object v0, p0, Lcfx;->d:Ljavax/crypto/SecretKey;

    iget-object v0, p0, Lcfx;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcfx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one of internalFilename or sharedFilename must be set."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcfy;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcfx;-><init>(Lcfy;)V

    return-void
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcfx;
    .locals 12

    const/4 v7, 0x1

    const/4 v8, 0x0

    sget-object v0, Lcem;->a:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcem;->b:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcem;->c:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcem;->d:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    sget-object v0, Lcem;->e:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    const/4 v0, 0x0

    if-eqz v10, :cond_3

    sget-object v0, Lcec;->c:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    sget-object v0, Lcec;->d:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v6, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    move-object v6, v0

    :goto_0
    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcfx;->a(Lcdu;Ljava/lang/String;JJ)Lcfy;

    move-result-object v2

    iput-object v9, v2, Lcfy;->c:Ljava/lang/String;

    if-nez v6, :cond_0

    move v1, v7

    :goto_1
    if-nez v10, :cond_1

    move v0, v7

    :goto_2
    if-ne v1, v0, :cond_2

    move v0, v7

    :goto_3
    const-string v1, "encryptionKey must be set if and only if sharedFilename is set."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iput-object v10, v2, Lcfy;->d:Ljava/lang/String;

    iput-object v6, v2, Lcfy;->e:Ljavax/crypto/SecretKey;

    invoke-virtual {v2}, Lcfy;->a()Lcfx;

    move-result-object v0

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v1

    invoke-virtual {v1}, Lcel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcfx;->d(J)V

    return-object v0

    :cond_0
    move v1, v8

    goto :goto_1

    :cond_1
    move v0, v8

    goto :goto_2

    :cond_2
    move v0, v8

    goto :goto_3

    :cond_3
    move-object v6, v0

    goto :goto_0
.end method

.method public static a(Lcdu;Ljava/lang/String;JJ)Lcfy;
    .locals 8

    new-instance v0, Lcfy;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v7}, Lcfy;-><init>(Lcdu;Ljava/lang/String;JJB)V

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcem;->a:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcem;->b:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfx;->g:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcem;->c:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfx;->h:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcfx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcem;->d:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcfx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcem;->e:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcfx;->d:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_2

    sget-object v0, Lcem;->f:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    sget-object v0, Lcem;->g:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_0
    sget-object v0, Lcem;->d:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcem;->e:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcem;->f:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    sget-object v0, Lcem;->g:Lcem;

    invoke-virtual {v0}, Lcem;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcfx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "internal and shared filenames cannot both be null"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lcfx;->b:Ljava/lang/String;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FileContent [contentHash="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcfx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", internalFilename="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sharedFilename="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", encryptionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastAccessedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcfx;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcfx;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
