.class public final Lhxp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

.field public final b:Landroid/app/PendingIntent;

.field c:B

.field d:Z

.field final e:Lhxv;

.field f:J

.field g:Z

.field private final h:Lhya;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;Landroid/app/PendingIntent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxp;->d:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhxp;->f:J

    iput-boolean v2, p0, Lhxp;->g:Z

    iput-byte v2, p0, Lhxp;->c:B

    iput-object p1, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    iput-object p2, p0, Lhxp;->b:Landroid/app/PendingIntent;

    new-instance v0, Lhxv;

    invoke-direct {v0, p1}, Lhxv;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V

    iput-object v0, p0, Lhxp;->e:Lhxv;

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lhya;

    invoke-direct {v0, p1}, Lhya;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V

    iput-object v0, p0, Lhxp;->h:Lhya;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhxp;->h:Lhya;

    goto :goto_0
.end method

.method static a(I)B
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static a(B)I
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static a(Lhyf;)Lcom/google/android/gms/location/internal/ParcelableGeofence;
    .locals 9

    const/4 v7, 0x0

    iget v0, p0, Lhyf;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v1, "GeofenceState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not supported."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lhyf;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhyf;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhyf;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhyf;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhyf;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhyf;->l:Z

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v7

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iget-boolean v1, p0, Lhyf;->n:Z

    if-eqz v1, :cond_5

    iget v0, p0, Lhyf;->o:I

    move v6, v0

    :goto_1
    const/4 v0, -0x1

    iget-boolean v1, p0, Lhyf;->p:Z

    if-eqz v1, :cond_4

    iget v0, p0, Lhyf;->q:I

    move v8, v0

    :goto_2
    :try_start_0
    new-instance v0, Leqa;

    invoke-direct {v0}, Leqa;-><init>()V

    iget-object v1, p0, Lhyf;->k:Ljava/lang/String;

    iput-object v1, v0, Leqa;->a:Ljava/lang/String;

    iget v1, p0, Lhyf;->m:I

    iput v1, v0, Leqa;->b:I

    iget-wide v1, p0, Lhyf;->c:D

    iget-wide v3, p0, Lhyf;->e:D

    iget v5, p0, Lhyf;->g:F

    invoke-virtual/range {v0 .. v5}, Leqa;->a(DDF)Leqa;

    move-result-object v0

    iget-wide v1, p0, Lhyf;->i:J

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gez v3, :cond_3

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Leqa;->c:J

    :goto_3
    iput v6, v0, Leqa;->d:I

    iput v8, v0, Leqa;->e:I

    invoke-virtual {v0}, Leqa;->a()Lepz;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    goto :goto_0

    :cond_3
    iput-wide v1, v0, Leqa;->c:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v1, "GeofenceState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid geofence from protocol buffer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v7

    goto :goto_0

    :cond_4
    move v8, v0

    goto :goto_2

    :cond_5
    move v6, v0

    goto :goto_1
.end method

.method private b(B)B
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v3, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v3}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :goto_0
    return v0

    :cond_0
    if-ne p1, v1, :cond_1

    iget-object v0, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "?"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "IN"

    goto :goto_0

    :pswitch_1
    const-string v0, "OUT"

    goto :goto_0

    :pswitch_2
    const-string v0, "STATE_INSIDE_LOW_CONFIDENCE"

    goto :goto_0

    :pswitch_3
    const-string v0, "STATE_OUTSIDE_LOW_CONFIDENCE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(JLandroid/location/Location;)B
    .locals 7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxp;->d:Z

    iget-object v0, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v0}, Lhxv;->a()B

    iget-object v0, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v0, p3}, Lhxv;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v0}, Lhxv;->a()B

    move-result v1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhxp;->h:Lhya;

    invoke-virtual {v0, p3}, Lhya;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lhxp;->h:Lhya;

    invoke-virtual {v0}, Lhya;->a()B

    move-result v0

    if-eq v0, v1, :cond_0

    const-string v2, "GeofenceState"

    const-string v3, "Different status in %s. Simple=%s, HighConfidence=%s, Overlap=%.4f"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Lhxp;->b(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    invoke-static {v1}, Lhxp;->b(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x3

    iget-object v5, p0, Lhxp;->e:Lhxv;

    iget-wide v5, v5, Lhxv;->d:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    if-eq v1, v0, :cond_1

    const/4 v0, 0x2

    if-ne v1, v0, :cond_9

    :cond_1
    const/4 v0, 0x0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    iget-boolean v2, p0, Lhxp;->g:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lhxp;->f:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iput-wide p1, p0, Lhxp;->f:J

    :cond_2
    :goto_0
    iget-byte v2, p0, Lhxp;->c:B

    if-nez v2, :cond_6

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    iput-byte v1, p0, Lhxp;->c:B

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhxp;->b(B)B

    move-result v0

    :goto_1
    return v0

    :cond_3
    iget-wide v2, p0, Lhxp;->f:J

    sub-long v2, p1, v2

    iget-object v4, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v4}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->j()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    const/4 v0, 0x1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lhxp;->g:Z

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lhxp;->f:J

    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v2}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-lez v2, :cond_2

    const-string v2, "GeofenceState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "User dwelled at "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v2}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-lez v2, :cond_2

    const-string v2, "GeofenceState"

    const-string v3, "Still confirming dwelling."

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lhxp;->f:J

    const/4 v2, 0x0

    iput-boolean v2, p0, Lhxp;->g:Z

    goto :goto_0

    :cond_6
    iget-byte v2, p0, Lhxp;->c:B

    if-eq v2, v1, :cond_8

    iput-byte v1, p0, Lhxp;->c:B

    const/4 v0, 0x1

    if-ne v1, v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0, v0}, Lhxp;->b(B)B

    move-result v0

    goto :goto_1

    :cond_7
    const/4 v0, 0x2

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_9

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lhxp;->b(B)B

    move-result v0

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()D
    .locals 4

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iget-object v2, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v2}, Lhxv;->b()D

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v2}, Lhxv;->b()D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(D)I
    .locals 4

    invoke-virtual {p0}, Lhxp;->a()D

    move-result-wide v0

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-nez v2, :cond_1

    const-wide p1, 0x3bc79ca10c924223L    # 1.0E-20

    :cond_1
    div-double/2addr v0, p1

    iget-object v2, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v2}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->i()I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    int-to-double v2, v2

    add-double/2addr v0, v2

    const-wide v2, 0x41dfffffff800000L    # 2.147483646E9

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_2

    const v0, 0x7ffffffe

    goto :goto_0

    :cond_2
    double-to-int v0, v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v1}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->h()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lhxp;->f:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lhxp;->g:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    invoke-virtual {p0}, Lhxp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v0}, Lhxv;->a()B

    move-result v0

    invoke-static {v0}, Lhxp;->b(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v0}, Lhxv;->b()D

    move-result-wide v0

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v3

    if-nez v0, :cond_0

    const-string v0, "UNKNOWN"

    :goto_0
    iget-boolean v1, p0, Lhxp;->g:Z

    if-eqz v1, :cond_1

    const-string v1, " dwelled"

    :goto_1
    const-string v3, "%s d=%s %s%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v5}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    const/4 v0, 0x2

    aput-object v2, v4, v0

    const/4 v0, 0x3

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.0f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lhxp;->e:Lhxv;

    invoke-virtual {v4}, Lhxv;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, ""

    goto :goto_1
.end method
