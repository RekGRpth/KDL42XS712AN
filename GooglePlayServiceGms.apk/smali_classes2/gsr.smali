.class public final Lgsr;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgss;

    invoke-direct {v0, p0}, Lgss;-><init>(Landroid/os/Bundle;)V

    invoke-static {v0}, Lgsr;->a(Lgst;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lgst;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 8

    const/4 v1, 0x0

    const-string v0, "sellerName"

    const-string v2, ""

    invoke-interface {p0, v0, v2}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Liob;

    invoke-direct {v4}, Liob;-><init>()V

    const-string v0, "items"

    invoke-interface {p0, v0}, Lgst;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v0, v6, [Liod;

    iput-object v0, v4, Liob;->b:[Liod;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgst;

    invoke-static {v0}, Lgsr;->b(Lgst;)Liod;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "DisplayHintsParser"

    const-string v2, "Invalid cart item"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v7, v4, Liob;->b:[Liod;

    aput-object v0, v7, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, v4, Liob;->b:[Liod;

    array-length v0, v0

    if-nez v0, :cond_2

    const-string v0, "DisplayHintsParser"

    const-string v2, "No valid cart items"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-static {v4}, Lgth;->a(Liob;)V

    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Liob;)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v2, Lgsu;

    invoke-direct {v2, v1}, Lgsu;-><init>(Lorg/json/JSONObject;)V

    invoke-static {v2}, Lgsr;->a(Lgst;)Lcom/google/android/gms/wallet/common/DisplayHints;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "DisplayHintsParser"

    const-string v2, "String is not valid JSON"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static b(Lgst;)Liod;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "name"

    const-string v3, ""

    invoke-interface {p0, v0, v3}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DisplayHintsParser"

    const-string v1, "Empty or missing \'name\' field in cart item"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "description"

    const-string v3, ""

    invoke-interface {p0, v0, v3}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "quantity"

    invoke-interface {p0, v0, v2}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "currencyCode"

    const-string v6, ""

    invoke-interface {p0, v3, v6}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v3, Liod;

    invoke-direct {v3}, Liod;-><init>()V

    iput-object v5, v3, Liod;->b:Ljava/lang/String;

    iput-object v4, v3, Liod;->a:Ljava/lang/String;

    iput v0, v3, Liod;->e:I

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "price"

    const-string v5, ""

    invoke-interface {p0, v4, v5}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v6, v4}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;)Lioh;

    move-result-object v2

    if-eqz v2, :cond_1

    iput-object v2, v3, Liod;->c:Lioh;

    :cond_1
    if-le v0, v1, :cond_2

    const-string v1, "net_cost"

    const-string v4, ""

    invoke-interface {p0, v1, v4}, Lgst;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v6, v1}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;)Lioh;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, v3, Liod;->d:Lioh;

    :cond_2
    :goto_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "DisplayHintsParser"

    const-string v3, "Invalid numeric field in cart item"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v2

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_2

    new-instance v1, Lioh;

    invoke-direct {v1}, Lioh;-><init>()V

    iput-object v1, v3, Liod;->d:Lioh;

    iget-object v1, v3, Liod;->d:Lioh;

    iput-object v6, v1, Lioh;->b:Ljava/lang/String;

    iget-object v1, v3, Liod;->d:Lioh;

    int-to-long v4, v0

    iget-wide v6, v2, Lioh;->a:J

    mul-long/2addr v4, v6

    iput-wide v4, v1, Lioh;->a:J

    goto :goto_2
.end method
