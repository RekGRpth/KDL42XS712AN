.class public final enum Lcda;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcda;

.field public static final enum b:Lcda;

.field private static final synthetic c:[Lcda;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcda;

    const-string v1, "NOT_SCROLLING"

    invoke-direct {v0, v1, v2}, Lcda;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcda;->a:Lcda;

    new-instance v0, Lcda;

    const-string v1, "SCROLLING"

    invoke-direct {v0, v1, v3}, Lcda;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcda;->b:Lcda;

    const/4 v0, 0x2

    new-array v0, v0, [Lcda;

    sget-object v1, Lcda;->a:Lcda;

    aput-object v1, v0, v2

    sget-object v1, Lcda;->b:Lcda;

    aput-object v1, v0, v3

    sput-object v0, Lcda;->c:[Lcda;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcda;
    .locals 1

    const-class v0, Lcda;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcda;

    return-object v0
.end method

.method public static values()[Lcda;
    .locals 1

    sget-object v0, Lcda;->c:[Lcda;

    invoke-virtual {v0}, [Lcda;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcda;

    return-object v0
.end method
