.class public final enum Lcea;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcea;

.field public static final enum b:Lcea;

.field public static final enum c:Lcea;

.field public static final enum d:Lcea;

.field public static final enum e:Lcea;

.field public static final enum f:Lcea;

.field public static final enum g:Lcea;

.field public static final enum h:Lcea;

.field public static final enum i:Lcea;

.field public static final enum j:Lcea;

.field public static final enum k:Lcea;

.field public static final enum l:Lcea;

.field public static final enum m:Lcea;

.field public static final enum n:Lcea;

.field public static final enum o:Lcea;

.field public static final enum p:Lcea;

.field private static final synthetic r:[Lcea;


# instance fields
.field private final q:Lcdt;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcea;

    const-string v1, "ACCOUNT"

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->a:Lcea;

    new-instance v0, Lcea;

    const-string v1, "APP_AUTH_METADATA"

    invoke-static {}, Lcde;->a()Lcde;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->b:Lcea;

    new-instance v0, Lcea;

    const-string v1, "APPDATA_SYNC_STATUS"

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->c:Lcea;

    new-instance v0, Lcea;

    const-string v1, "APP_SCOPE"

    invoke-static {}, Lcdi;->a()Lcdi;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->d:Lcea;

    new-instance v0, Lcea;

    const-string v1, "DOCUMENT_CONTENT"

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->e:Lcea;

    new-instance v0, Lcea;

    const-string v1, "ENTRY"

    const/4 v2, 0x5

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->f:Lcea;

    new-instance v0, Lcea;

    const-string v1, "CONTAINS_ID"

    const/4 v2, 0x6

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->g:Lcea;

    new-instance v0, Lcea;

    const-string v1, "PARTIAL_FEED"

    const/4 v2, 0x7

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->h:Lcea;

    new-instance v0, Lcea;

    const-string v1, "SYNC_REQUEST"

    const/16 v2, 0x8

    invoke-static {}, Lcey;->a()Lcey;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->i:Lcea;

    new-instance v0, Lcea;

    const-string v1, "UNIQUE_ID"

    const/16 v2, 0x9

    invoke-static {}, Lcfa;->a()Lcfa;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->j:Lcea;

    new-instance v0, Lcea;

    const-string v1, "ENTRY_AUTHORIZED_APP"

    const/16 v2, 0xa

    invoke-static {}, Lced;->a()Lced;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->k:Lcea;

    new-instance v0, Lcea;

    const-string v1, "PENDING_OPERATION"

    const/16 v2, 0xb

    invoke-static {}, Lcep;->a()Lcep;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->l:Lcea;

    new-instance v0, Lcea;

    const-string v1, "FILE_CONTENT"

    const/16 v2, 0xc

    invoke-static {}, Lcel;->a()Lcel;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->m:Lcea;

    new-instance v0, Lcea;

    const-string v1, "PENDING_UPLOADS"

    const/16 v2, 0xd

    invoke-static {}, Lcer;->a()Lcer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->n:Lcea;

    new-instance v0, Lcea;

    const-string v1, "DELETION_LOCK"

    const/16 v2, 0xe

    invoke-static {}, Lcdn;->a()Lcdn;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->o:Lcea;

    new-instance v0, Lcea;

    const-string v1, "SUBSCRIPTION"

    const/16 v2, 0xf

    invoke-static {}, Lcew;->a()Lcew;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcea;-><init>(Ljava/lang/String;ILcdt;)V

    sput-object v0, Lcea;->p:Lcea;

    const/16 v0, 0x10

    new-array v0, v0, [Lcea;

    sget-object v1, Lcea;->a:Lcea;

    aput-object v1, v0, v4

    sget-object v1, Lcea;->b:Lcea;

    aput-object v1, v0, v5

    sget-object v1, Lcea;->c:Lcea;

    aput-object v1, v0, v6

    sget-object v1, Lcea;->d:Lcea;

    aput-object v1, v0, v7

    sget-object v1, Lcea;->e:Lcea;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcea;->f:Lcea;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcea;->g:Lcea;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcea;->h:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcea;->i:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcea;->j:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcea;->k:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcea;->l:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcea;->m:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcea;->n:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcea;->o:Lcea;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcea;->p:Lcea;

    aput-object v2, v0, v1

    sput-object v0, Lcea;->r:[Lcea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdt;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcea;->q:Lcdt;

    return-void
.end method

.method public static synthetic a(Lcea;)Lcdt;
    .locals 1

    iget-object v0, p0, Lcea;->q:Lcdt;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcea;
    .locals 1

    const-class v0, Lcea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcea;

    return-object v0
.end method

.method public static values()[Lcea;
    .locals 1

    sget-object v0, Lcea;->r:[Lcea;

    invoke-virtual {v0}, [Lcea;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcea;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcea;->q:Lcdt;

    return-object v0
.end method
