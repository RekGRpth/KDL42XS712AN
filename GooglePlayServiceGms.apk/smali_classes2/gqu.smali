.class public Lgqu;
.super Lsc;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Lsk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lgqu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgqu;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p2, p4}, Lsc;-><init>(ILjava/lang/String;Lsj;)V

    iput-object p1, p0, Lgqu;->g:Landroid/content/Context;

    iput-object p3, p0, Lgqu;->h:Lsk;

    return-void
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 2

    new-instance v0, Lgqn;

    iget-object v1, p0, Lgqu;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lgqn;-><init>(Landroid/content/Context;)V

    iget-object v1, p1, Lrz;->b:[B

    invoke-virtual {v0, v1}, Lgqn;->a([B)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Ltb;->a(Lrz;)Lrp;

    move-result-object v1

    invoke-static {v0, v1}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lgqu;->h:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    return-void
.end method
