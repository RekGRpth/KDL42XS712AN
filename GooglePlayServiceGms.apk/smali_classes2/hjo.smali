.class final Lhjo;
.super Lhsi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhjn;


# direct methods
.method private constructor <init>(Lhjn;)V
    .locals 0

    iput-object p1, p0, Lhjo;->a:Lhjn;

    invoke-direct {p0}, Lhsi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhjn;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhjo;-><init>(Lhjn;)V

    return-void
.end method

.method private j()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lhjo;->a:Lhjn;

    iput-object v4, v0, Lhjn;->i:Lhqy;

    iget-object v0, p0, Lhjo;->a:Lhjn;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lhjn;->j:J

    iget-object v0, p0, Lhjo;->a:Lhjn;

    iget-object v0, v0, Lhjn;->b:Lidu;

    const/16 v1, 0x9

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    return-void
.end method


# virtual methods
.method public final a(Livi;)V
    .locals 6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjo;->a:Lhjn;

    iget-object v0, v0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Finished indoor/outdoor collection."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhjo;->a:Lhjn;

    iget-object v0, v0, Lhjn;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-object v2, p0, Lhjo;->a:Lhjn;

    iget-wide v2, v2, Lhjn;->j:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    add-long/2addr v2, v0

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Livi;->k(I)I

    move-result v2

    if-lez v2, :cond_2

    iget-object v0, p0, Lhjo;->a:Lhjn;

    invoke-static {v0}, Lhjn;->a(Lhjn;)Lhjp;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, v0, Lhjp;->b:Lidu;

    invoke-interface {v2}, Lidu;->o()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v3, Lhjq;

    invoke-direct {v3, v0, v1, p1}, Lhjq;-><init>(Lhjp;Ljava/util/Calendar;Livi;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :cond_1
    :goto_0
    invoke-direct {p0}, Lhjo;->j()V

    return-void

    :cond_2
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhjo;->a:Lhjn;

    iget-object v2, v2, Lhjn;->a:Ljava/lang/String;

    const-string v3, "Collection duration too short: %d or no GLocRequestElement."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjo;->a:Lhjn;

    iget-object v0, v0, Lhjn;->a:Ljava/lang/String;

    const-string v1, "Failed to do indoor/outdoor collection."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhjo;->j()V

    return-void
.end method
