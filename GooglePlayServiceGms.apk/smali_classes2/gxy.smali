.class public final Lgxy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)V
    .locals 0

    iput-object p1, p0, Lgxy;->a:Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lgxy;->a:Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)Lgxx;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgxy;->a:Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)Lgxx;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lgxx;->a(I)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    const-string v0, "RegionCodeSelectorSpinn"

    const-string v1, "Listener fired for onNothingSelected; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
