.class public final Laup;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Laun;

.field public h:Ljava/lang/String;

.field public i:Ljava/util/List;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Laup;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laup;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laup;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laup;->e:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laup;->g:Laun;

    const-string v0, ""

    iput-object v0, p0, Laup;->h:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laup;->i:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Laup;->n:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Laup;->n:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Laup;->b()I

    :cond_0
    iget v0, p0, Laup;->n:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Laup;->j:Z

    iput-object v0, p0, Laup;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Laup;->k:Z

    iput-object v0, p0, Laup;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Laup;->l:Z

    iput-object v0, p0, Laup;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Laup;->d:Z

    iput-object v0, p0, Laup;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    new-instance v0, Laun;

    invoke-direct {v0}, Laun;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Laup;->f:Z

    iput-object v0, p0, Laup;->g:Laun;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Laup;->m:Z

    iput-object v0, p0, Laup;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    new-instance v0, Lauq;

    invoke-direct {v0}, Lauq;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iget-object v1, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Laup;->i:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Laup;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Laup;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Laup;->k:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Laup;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Laup;->l:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Laup;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Laup;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Laup;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Laup;->f:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Laup;->g:Laun;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_4
    iget-boolean v0, p0, Laup;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Laup;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_6
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Laup;->j:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Laup;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Laup;->k:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Laup;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Laup;->l:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Laup;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Laup;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Laup;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Laup;->f:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Laup;->g:Laun;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Laup;->m:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Laup;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_6
    iput v1, p0, Laup;->n:I

    return v1
.end method
