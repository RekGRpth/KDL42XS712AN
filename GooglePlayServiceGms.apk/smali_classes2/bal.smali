.class public final Lbal;
.super Laya;
.source "SourceFile"

# interfaces
.implements Lbai;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/service/CastService;

.field private final b:Lbac;

.field private final c:Layb;

.field private d:Landroid/os/IBinder$DeathRecipient;

.field private e:Landroid/os/IBinder$DeathRecipient;

.field private final f:Lcom/google/android/gms/cast/CastDevice;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Lbjv;

.field private final j:Ljava/lang/String;

.field private final k:J

.field private final l:Laye;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/service/CastService;Lbjv;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;Layb;Ljava/lang/String;JLaye;)V
    .locals 9

    iput-object p1, p0, Lbal;->a:Lcom/google/android/gms/cast/service/CastService;

    invoke-direct {p0}, Laya;-><init>()V

    move-object/from16 v0, p10

    iput-object v0, p0, Lbal;->l:Laye;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbjv;

    iput-object v2, p0, Lbal;->i:Lbjv;

    iput-object p3, p0, Lbal;->f:Lcom/google/android/gms/cast/CastDevice;

    iput-object p4, p0, Lbal;->g:Ljava/lang/String;

    iput-object p5, p0, Lbal;->h:Ljava/lang/String;

    iput-object p6, p0, Lbal;->c:Layb;

    move-object/from16 v0, p7

    iput-object v0, p0, Lbal;->j:Ljava/lang/String;

    move-wide/from16 v0, p8

    iput-wide v0, p0, Lbal;->k:J

    new-instance v2, Lbam;

    invoke-direct {v2, p0, p1}, Lbam;-><init>(Lbal;Lcom/google/android/gms/cast/service/CastService;)V

    iput-object v2, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    new-instance v2, Lban;

    invoke-direct {v2, p0, p1}, Lban;-><init>(Lbal;Lcom/google/android/gms/cast/service/CastService;)V

    iput-object v2, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    :try_start_0
    iget-object v2, p0, Lbal;->c:Layb;

    invoke-interface {v2}, Layb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lbal;->l:Laye;

    const-string v3, "acquireDeviceController by %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lbal;->j:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/cast/service/CastService;->a(Lcom/google/android/gms/cast/service/CastService;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lbal;->j:Ljava/lang/String;

    iget-object v5, p0, Lbal;->f:Lcom/google/android/gms/cast/CastDevice;

    iget-wide v6, p0, Lbal;->k:J

    move-object v2, p1

    move-object v8, p0

    invoke-static/range {v2 .. v8}, Lbac;->a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;JLbai;)Lbac;

    move-result-object v2

    iput-object v2, p0, Lbal;->b:Lbac;

    iget-object v2, p0, Lbal;->b:Lbac;

    invoke-virtual {v2}, Lbac;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lbal;->i:Lbjv;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbal;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v2

    iget-object v2, p0, Lbal;->l:Laye;

    const-string v3, "client disconnected before listener was set"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Laye;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lbal;->e()V

    goto :goto_0

    :catch_1
    move-exception v2

    iget-object v2, p0, Lbal;->l:Laye;

    const-string v3, "client died while brokering service"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lbal;->e()V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lbal;->b:Lbac;

    invoke-virtual {v2}, Lbac;->e()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lbal;->l:Laye;

    const-string v3, "connecting to device with applicationId=%s, sessionId=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lbal;->g:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lbal;->h:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lbal;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbal;->b:Lbac;

    iget-object v3, p0, Lbal;->g:Ljava/lang/String;

    iget-object v4, p0, Lbal;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lbac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    :try_start_2
    iget-object v2, p0, Lbal;->i:Lbjv;

    invoke-interface {v2}, Lbjv;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v2

    iget-object v2, p0, Lbal;->l:Laye;

    const-string v3, "Unable to link listener reaper"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lbal;->e()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lbal;->b:Lbac;

    invoke-virtual {v2}, Lbac;->b()V

    goto :goto_2
.end method

.method static synthetic a(Lbal;)Laye;
    .locals 1

    iget-object v0, p0, Lbal;->l:Laye;

    return-object v0
.end method

.method static synthetic b(Lbal;)V
    .locals 0

    invoke-direct {p0}, Lbal;->e()V

    return-void
.end method

.method private e()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "Disposing ConnectedClient."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbal;->b:Lbac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->i()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0}, Layb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v3, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    :cond_2
    :goto_1
    iget-object v0, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    if-eqz v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lbal;->i:Lbjv;

    invoke-interface {v0}, Lbjv;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iput-object v3, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    :cond_3
    :goto_2
    return-void

    :cond_4
    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->q()V

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v3, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v3, p0, Lbal;->e:Landroid/os/IBinder$DeathRecipient;

    throw v0

    :catch_1
    move-exception v0

    iput-object v3, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    goto :goto_2

    :catchall_1
    move-exception v0

    iput-object v3, p0, Lbal;->d:Landroid/os/IBinder$DeathRecipient;

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lbal;->e()V

    return-void
.end method

.method public final a(DDZ)V
    .locals 6

    iget-object v0, p0, Lbal;->b:Lbac;

    move-wide v1, p1

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lbac;->a(DDZ)V

    return-void
.end method

.method public final a(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "onDisconnected: status=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0}, Layb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1}, Layb;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->q()V

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbal;->l:Laye;

    const-string v2, "client died while brokering service"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "Binder is not alive. Not calling onDisconnected"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1}, Layb;->e(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbal;->g:Ljava/lang/String;

    iput-object p3, p0, Lbal;->h:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2, p3, p4}, Layb;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1}, Lbac;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;DZ)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2, p3, p4}, Layb;->a(Ljava/lang/String;DZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2, p3}, Layb;->a(Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JI)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2, p3, p4}, Layb;->a(Ljava/lang/String;JI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1, p2}, Lbac;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-le v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbac;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1, p2}, Lbac;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2}, Layb;->a(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-le v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbac;->a(Ljava/lang/String;[BJ)V

    goto :goto_0
.end method

.method public final a(ZDZ)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbac;->a(ZDZ)V

    return-void
.end method

.method public final a_(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1, p2}, Layb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->k()V

    return-void
.end method

.method public final b(I)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1}, Layb;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1}, Lbac;->d(Ljava/lang/String;)V

    return-void
.end method

.method public final c()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lbal;->i:Lbjv;

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbal;->l:Laye;

    const-string v2, "client died while brokering service"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    invoke-interface {v0, p1}, Layb;->d(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0, p1}, Lbac;->f(Ljava/lang/String;)V

    return-void
.end method

.method public final d()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbal;->c:Layb;

    const/16 v1, 0x7d1

    invoke-interface {v0, v1}, Layb;->c(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final j_()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lbal;->i:Lbjv;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbal;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "Connected to device."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbal;->l:Laye;

    const-string v2, "client died while brokering service"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final k_()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lbal;->i:Lbjv;

    const/16 v1, 0x3e9

    invoke-virtual {p0}, Lbal;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    iget-object v0, p0, Lbal;->l:Laye;

    const-string v1, "Connected to device without app."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbal;->l:Laye;

    const-string v2, "client died while brokering service"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final l_()V
    .locals 1

    iget-object v0, p0, Lbal;->b:Lbac;

    invoke-virtual {v0}, Lbac;->m()V

    return-void
.end method
