.class public final enum Lcem;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcem;

.field public static final enum b:Lcem;

.field public static final enum c:Lcem;

.field public static final enum d:Lcem;

.field public static final enum e:Lcem;

.field public static final enum f:Lcem;

.field public static final enum g:Lcem;

.field private static final synthetic i:[Lcem;


# instance fields
.field private final h:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    new-instance v0, Lcem;

    const-string v1, "CONTENT_HASH"

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "md5Hash"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcdq;->a(I)Lcdq;

    move-result-object v2

    new-instance v3, Lcei;

    const-string v4, "hash"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v2, v10, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->a:Lcem;

    new-instance v0, Lcem;

    const-string v1, "LAST_ACCESSED"

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "lastAccessed"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v9, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->b:Lcem;

    new-instance v0, Lcem;

    const-string v1, "SIZE_BYTES"

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "size"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->c:Lcem;

    new-instance v0, Lcem;

    const-string v1, "INTERNAL_FILE_NAME"

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "internalFileName"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->d:Lcem;

    new-instance v0, Lcem;

    const-string v1, "SHARED_FILE_NAME"

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "sharedFileName"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->e:Lcem;

    new-instance v0, Lcem;

    const-string v1, "ENCRYPTION_KEY"

    const/4 v2, 0x5

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "encryptionKey"

    sget-object v6, Lcek;->d:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->f:Lcem;

    new-instance v0, Lcem;

    const-string v1, "ENCRYPTION_ALGORITHM"

    const/4 v2, 0x6

    invoke-static {}, Lcel;->d()Lcel;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "encryptionAlgorithm"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcem;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcem;->g:Lcem;

    const/4 v0, 0x7

    new-array v0, v0, [Lcem;

    sget-object v1, Lcem;->a:Lcem;

    aput-object v1, v0, v8

    sget-object v1, Lcem;->b:Lcem;

    aput-object v1, v0, v9

    sget-object v1, Lcem;->c:Lcem;

    aput-object v1, v0, v7

    sget-object v1, Lcem;->d:Lcem;

    aput-object v1, v0, v11

    sget-object v1, Lcem;->e:Lcem;

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, Lcem;->f:Lcem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcem;->g:Lcem;

    aput-object v2, v0, v1

    sput-object v0, Lcem;->i:[Lcem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcem;->h:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcem;
    .locals 1

    const-class v0, Lcem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcem;

    return-object v0
.end method

.method public static values()[Lcem;
    .locals 1

    sget-object v0, Lcem;->i:[Lcem;

    invoke-virtual {v0}, [Lcem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcem;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcem;->h:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcem;->h:Lcdp;

    return-object v0
.end method
