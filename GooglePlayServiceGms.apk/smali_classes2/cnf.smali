.class public final Lcnf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbus;


# instance fields
.field final a:Lcos;

.field final b:Lcfz;

.field final c:Lbwo;

.field d:Ljava/util/concurrent/ExecutorService;

.field final e:Ljava/util/Map;

.field final f:Ljava/util/Map;

.field final g:Lbwb;

.field private final h:Lcby;

.field private final i:Lcng;

.field private j:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcos;Lcfz;Lcby;Lbwo;Lbwb;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcng;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcng;-><init>(Lcnf;B)V

    iput-object v0, p0, Lcnf;->i:Lcng;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcnf;->j:Ljava/util/concurrent/ExecutorService;

    sget-object v0, Lbqs;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcnf;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcnf;->e:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcnf;->f:Ljava/util/Map;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcos;

    iput-object v0, p0, Lcnf;->a:Lcos;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lcnf;->b:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    iput-object v0, p0, Lcnf;->h:Lcby;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwo;

    iput-object v0, p0, Lcnf;->c:Lbwo;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwb;

    iput-object v0, p0, Lcnf;->g:Lbwb;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcnf;->a:Lcos;

    invoke-interface {v0}, Lcos;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnf;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcng;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcng;-><init>(Lcnf;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :cond_0
    iget-object v1, p0, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnh;

    invoke-virtual {v0}, Lcnh;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final a(Lcfp;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcnf;->a:Lcos;

    invoke-interface {v0}, Lcos;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcfp;->u()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcfp;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcnf;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lbqs;->p:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcnf;->b:Lcfz;

    invoke-virtual {p1}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcnf;->h:Lcby;

    invoke-interface {v0}, Lcby;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcnf;->a:Lcos;

    invoke-interface {v0}, Lcos;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized b()Ljava/util/concurrent/Future;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    sget-object v0, Lbqs;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcnf;->j:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcnf;->i:Lcng;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
