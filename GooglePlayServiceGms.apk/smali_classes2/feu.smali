.class public final Lfeu;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:Z

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Lfev;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-wide v1, p0, Lfeu;->a:J

    iput-wide v1, p0, Lfeu;->b:J

    iput-wide v1, p0, Lfeu;->c:J

    iput-boolean v0, p0, Lfeu;->d:Z

    iput v0, p0, Lfeu;->e:I

    iput v0, p0, Lfeu;->f:I

    iput v0, p0, Lfeu;->g:I

    iput v0, p0, Lfeu;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lfeu;->i:Lfev;

    const/4 v0, -0x1

    iput v0, p0, Lfeu;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const-wide/16 v4, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v1, p0, Lfeu;->a:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Lfeu;->a:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-wide v1, p0, Lfeu;->b:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lfeu;->b:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-wide v1, p0, Lfeu;->c:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lfeu;->c:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lfeu;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lfeu;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lfeu;->e:I

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Lfeu;->e:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lfeu;->f:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lfeu;->f:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lfeu;->g:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Lfeu;->g:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lfeu;->h:I

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Lfeu;->h:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lfeu;->i:Lfev;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lfeu;->i:Lfev;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lfeu;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lfeu;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lfeu;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lfeu;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfeu;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfeu;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfeu;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfeu;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfeu;->h:I

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lfeu;->i:Lfev;

    if-nez v0, :cond_1

    new-instance v0, Lfev;

    invoke-direct {v0}, Lfev;-><init>()V

    iput-object v0, p0, Lfeu;->i:Lfev;

    :cond_1
    iget-object v0, p0, Lfeu;->i:Lfev;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const-wide/16 v3, 0x0

    iget-wide v0, p0, Lfeu;->a:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lfeu;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_0
    iget-wide v0, p0, Lfeu;->b:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lfeu;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_1
    iget-wide v0, p0, Lfeu;->c:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lfeu;->c:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_2
    iget-boolean v0, p0, Lfeu;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lfeu;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_3
    iget v0, p0, Lfeu;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lfeu;->e:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_4
    iget v0, p0, Lfeu;->f:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lfeu;->f:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_5
    iget v0, p0, Lfeu;->g:I

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lfeu;->g:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_6
    iget v0, p0, Lfeu;->h:I

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Lfeu;->h:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_7
    iget-object v0, p0, Lfeu;->i:Lfev;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lfeu;->i:Lfev;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_8
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfeu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfeu;

    iget-wide v2, p0, Lfeu;->a:J

    iget-wide v4, p1, Lfeu;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lfeu;->b:J

    iget-wide v4, p1, Lfeu;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lfeu;->c:J

    iget-wide v4, p1, Lfeu;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lfeu;->d:Z

    iget-boolean v3, p1, Lfeu;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lfeu;->e:I

    iget v3, p1, Lfeu;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lfeu;->f:I

    iget v3, p1, Lfeu;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lfeu;->g:I

    iget v3, p1, Lfeu;->g:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lfeu;->h:I

    iget v3, p1, Lfeu;->h:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lfeu;->i:Lfev;

    if-nez v2, :cond_b

    iget-object v2, p1, Lfeu;->i:Lfev;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lfeu;->i:Lfev;

    iget-object v3, p1, Lfeu;->i:Lfev;

    invoke-virtual {v2, v3}, Lfev;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget-wide v0, p0, Lfeu;->a:J

    iget-wide v2, p0, Lfeu;->a:J

    ushr-long/2addr v2, v5

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lfeu;->b:J

    iget-wide v3, p0, Lfeu;->b:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lfeu;->c:J

    iget-wide v3, p0, Lfeu;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lfeu;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeu;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeu;->f:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeu;->g:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeu;->h:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lfeu;->i:Lfev;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfeu;->i:Lfev;

    invoke-virtual {v0}, Lfev;->hashCode()I

    move-result v0

    goto :goto_1
.end method
