.class public final enum Lhtv;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhtv;

.field public static final enum b:Lhtv;

.field public static final enum c:Lhtv;

.field public static final enum d:Lhtv;

.field private static final synthetic f:[Lhtv;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lhtv;

    const-string v1, "Requests"

    invoke-direct {v0, v1, v5, v2}, Lhtv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhtv;->a:Lhtv;

    new-instance v0, Lhtv;

    const-string v1, "Results"

    invoke-direct {v0, v1, v2, v3}, Lhtv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhtv;->b:Lhtv;

    new-instance v0, Lhtv;

    const-string v1, "FromCachedData"

    invoke-direct {v0, v1, v3, v4}, Lhtv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhtv;->c:Lhtv;

    new-instance v0, Lhtv;

    const-string v1, "Jumps"

    invoke-direct {v0, v1, v4, v6}, Lhtv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhtv;->d:Lhtv;

    new-array v0, v6, [Lhtv;

    sget-object v1, Lhtv;->a:Lhtv;

    aput-object v1, v0, v5

    sget-object v1, Lhtv;->b:Lhtv;

    aput-object v1, v0, v2

    sget-object v1, Lhtv;->c:Lhtv;

    aput-object v1, v0, v3

    sget-object v1, Lhtv;->d:Lhtv;

    aput-object v1, v0, v4

    sput-object v0, Lhtv;->f:[Lhtv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lhtv;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhtv;
    .locals 1

    const-class v0, Lhtv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhtv;

    return-object v0
.end method

.method public static values()[Lhtv;
    .locals 1

    sget-object v0, Lhtv;->f:[Lhtv;

    invoke-virtual {v0}, [Lhtv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhtv;

    return-object v0
.end method
