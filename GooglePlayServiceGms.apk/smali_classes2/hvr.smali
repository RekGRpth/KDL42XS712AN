.class final Lhvr;
.super Landroid/database/ContentObserver;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/ContentResolver;

.field final synthetic b:Lhvp;


# direct methods
.method constructor <init>(Lhvp;Landroid/os/Handler;Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lhvr;->b:Lhvp;

    iput-object p3, p0, Lhvr;->a:Landroid/content/ContentResolver;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 6

    const/4 v0, 0x1

    const/4 v5, 0x0

    iget-object v1, p0, Lhvr;->b:Lhvp;

    iget-object v2, p0, Lhvr;->a:Landroid/content/ContentResolver;

    const-string v3, "gps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, Lhvp;->j:Z

    iget-object v1, p0, Lhvr;->b:Lhvp;

    iget-object v2, p0, Lhvr;->a:Landroid/content/ContentResolver;

    const-string v3, "network"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhvr;->a:Landroid/content/ContentResolver;

    const-string v3, "network_location_opt_in"

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Lhhv;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_3

    :goto_0
    iput-boolean v0, v1, Lhvp;->i:Z

    iget-object v0, p0, Lhvr;->b:Lhvp;

    iget-boolean v0, v0, Lhvp;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhvr;->b:Lhvp;

    invoke-static {v0}, Lhvp;->a(Lhvp;)V

    :cond_0
    iget-object v0, p0, Lhvr;->b:Lhvp;

    iget-boolean v0, v0, Lhvp;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lhvr;->b:Lhvp;

    iget-object v1, v0, Lhvp;->b:Landroid/location/Location;

    if-nez v1, :cond_1

    iget-object v1, v0, Lhvp;->f:Landroid/location/Location;

    if-eqz v1, :cond_2

    :cond_1
    iput-object v5, v0, Lhvp;->b:Landroid/location/Location;

    iput-object v5, v0, Lhvp;->f:Landroid/location/Location;

    iget-object v1, v0, Lhvp;->d:Landroid/location/Location;

    invoke-virtual {v0, v1, v5}, Lhvp;->a(Landroid/location/Location;Landroid/location/Location;)V

    :cond_2
    iget-object v0, p0, Lhvr;->b:Lhvp;

    iget-boolean v0, v0, Lhvp;->j:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lhvr;->b:Lhvp;

    iget-boolean v0, v0, Lhvp;->i:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lhvr;->b:Lhvp;

    invoke-virtual {v0}, Lhvp;->b()V

    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lhvr;->b:Lhvp;

    invoke-virtual {v0}, Lhvp;->a()V

    goto :goto_1
.end method
