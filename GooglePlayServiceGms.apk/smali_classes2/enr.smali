.class public final Lenr;
.super Lenc;
.source "SourceFile"


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Landroid/os/PowerManager$WakeLock;

.field private final h:Landroid/content/Intent;

.field private final i:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 3

    invoke-direct {p0}, Lenc;-><init>()V

    iput-object p1, p0, Lenr;->f:Landroid/content/Context;

    iget-object v0, p0, Lenr;->f:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "Icing"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lenr;->g:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lenr;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lenr;->h:Landroid/content/Intent;

    iput-object p2, p0, Lenr;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final a(Lenf;)V
    .locals 2

    iget v0, p1, Lenf;->h:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lenr;->f:Landroid/content/Context;

    iget-object v1, p0, Lenr;->h:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    iget-object v0, p0, Lenr;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method protected final b()V
    .locals 2

    iget-object v0, p0, Lenr;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v0, "%s: On dead called"

    iget-object v1, p0, Lenr;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lenr;->f:Landroid/content/Context;

    iget-object v1, p0, Lenr;->h:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method
