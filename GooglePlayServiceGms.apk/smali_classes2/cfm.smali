.class public final Lcfm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Locale;

.field private static final b:Lcgg;

.field private static final c:Lcgg;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcfm;->a:Ljava/util/Locale;

    new-instance v0, Lcgg;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    sget-object v2, Lcfm;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Lcgg;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcfm;->b:Lcgg;

    new-instance v0, Lcgg;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSz"

    sget-object v2, Lcfm;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Lcgg;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcfm;->c:Lcgg;

    sget-object v0, Lcfm;->b:Lcgg;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgg;->a(Ljava/util/TimeZone;)V

    sget-object v0, Lcfm;->c:Lcgg;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgg;->a(Ljava/util/TimeZone;)V

    return-void
.end method

.method public static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcfm;->b:Lcgg;

    invoke-virtual {v1, p0}, Lcgg;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcfm;->b:Lcgg;

    :goto_1
    invoke-virtual {v0, p0}, Lcgg;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcfm;->c:Lcgg;

    goto :goto_1
.end method

.method public static a(Lclk;Lcfp;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcfp;->p()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lclk;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-interface {p0}, Lclk;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-interface {p0}, Lclk;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->p(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcfp;->b(Ljava/util/Date;)V

    invoke-interface {p0}, Lclk;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lclk;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->c(Ljava/lang/Long;)V

    :cond_0
    :goto_0
    invoke-interface {p0}, Lclk;->q()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcfp;->a(Ljava/util/Date;)V

    invoke-interface {p0}, Lclk;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1}, Lcfp;->i()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_1
    :goto_2
    invoke-virtual {p1, v0}, Lcfp;->c(Ljava/util/Date;)V

    invoke-interface {p0}, Lclk;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    move-object v0, v2

    :goto_3
    invoke-virtual {p1, v0}, Lcfp;->a(Ljava/lang/Long;)V

    invoke-interface {p0}, Lclk;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    :goto_4
    invoke-virtual {p1, v2}, Lcfp;->b(Ljava/lang/Long;)V

    invoke-interface {p0}, Lclk;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->e(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->a()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->b(Z)V

    invoke-interface {p0}, Lclk;->b()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->c(Z)V

    invoke-interface {p0}, Lclk;->s()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcfp;->z()V

    :goto_5
    invoke-interface {p0}, Lclk;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->a(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->t()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->f(Z)V

    invoke-interface {p0}, Lclk;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->c(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcfp;->a(Z)V

    invoke-interface {p0}, Lclk;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->d(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->u()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->a(Ljava/util/Collection;)V

    invoke-interface {p0}, Lclk;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->g(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->x()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->g(Z)V

    invoke-interface {p0}, Lclk;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->h(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->i(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->A()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcfp;->a(J)V

    invoke-interface {p0}, Lclk;->B()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->h(Z)V

    invoke-interface {p0}, Lclk;->C()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcfp;->i(Z)V

    invoke-interface {p0}, Lclk;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->j(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->E()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcfp;->b(J)V

    invoke-interface {p0}, Lclk;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->k(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->G()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->l(Ljava/lang/String;)V

    invoke-interface {p0}, Lclk;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    :goto_7
    invoke-virtual {p1, v3}, Lcfp;->j(Z)V

    invoke-virtual {p1}, Lcfp;->X()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcfp;->h()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lcbk;->a(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcfp;->Z()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1, v0}, Lcfp;->q(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcfp;->Y()V

    :cond_2
    return-void

    :cond_3
    sget-object v0, Lbth;->g:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lclk;->d()Lbsp;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v5, v0, Lbsp;->b:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcfp;->c(Ljava/lang/Long;)V

    goto/16 :goto_0

    :cond_4
    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto/16 :goto_2

    :cond_6
    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_3

    :cond_7
    invoke-static {v0}, Lcfm;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_4

    :cond_8
    invoke-interface {p0}, Lclk;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcfp;->B()V

    goto/16 :goto_5

    :cond_9
    invoke-virtual {p1}, Lcfp;->D()V

    goto/16 :goto_5

    :cond_a
    move v0, v4

    goto/16 :goto_6

    :cond_b
    move v3, v4

    goto :goto_7
.end method
