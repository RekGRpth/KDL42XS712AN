.class public final Ligu;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Liuc;
    .locals 5

    const-wide v3, 0x416312d000000000L    # 1.0E7

    new-instance v0, Liuc;

    invoke-direct {v0}, Liuc;-><init>()V

    iget-wide v1, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Liuc;->a:Ljava/lang/Integer;

    iget-wide v1, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Liuc;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public static a(ILjava/lang/String;I)Liuh;
    .locals 3

    new-instance v0, Liuh;

    invoke-direct {v0}, Liuh;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Liuh;->a:Ljava/lang/Integer;

    new-instance v1, Litx;

    invoke-direct {v1}, Litx;-><init>()V

    iput-object v1, v0, Liuh;->b:Litx;

    iget-object v1, v0, Liuh;->b:Litx;

    iput-object p1, v1, Litx;->a:Ljava/lang/String;

    iget-object v1, v0, Liuh;->b:Litx;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Litx;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)Liuj;
    .locals 5

    new-instance v1, Liuj;

    invoke-direct {v1}, Liuj;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Liuj;->a:Ljava/lang/Integer;

    iput-object p1, v1, Liuj;->b:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p2, v1, Liuj;->c:Ljava/lang/String;

    :cond_0
    new-instance v0, Liug;

    invoke-direct {v0}, Liug;-><init>()V

    iput-object v0, v1, Liuj;->d:Liug;

    iget-object v0, v1, Liuj;->d:Liug;

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, v0, Liug;->b:[Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v3, v1, Liuj;->d:Liug;

    iget-object v3, v3, Liug;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    goto :goto_0

    :cond_1
    iget-object v0, v1, Liuj;->d:Liug;

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Liug;->d:Ljava/lang/String;

    iget-object v0, v1, Liuj;->d:Liug;

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Liug;->e:Ljava/lang/Boolean;

    return-object v1
.end method
