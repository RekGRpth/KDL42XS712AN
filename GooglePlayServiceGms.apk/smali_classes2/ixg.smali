.class public final Lixg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lixi;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lixg;->a:Lixi;

    return-void
.end method


# virtual methods
.method public final a(Lixh;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must call decompose before calling invert"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object/from16 v0, p1

    iget v1, v0, Lixh;->a:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lixg;->a:Lixi;

    iget v2, v2, Lixi;->a:I

    if-ne v1, v2, :cond_1

    move-object/from16 v0, p1

    iget v1, v0, Lixh;->b:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lixg;->a:Lixi;

    iget v2, v2, Lixi;->b:I

    if-eq v1, v2, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "output has the wrong size, should be %d x %d but is %d x %d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lixg;->a:Lixi;

    iget v5, v5, Lixi;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lixg;->a:Lixi;

    iget v5, v5, Lixi;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p1

    iget v5, v0, Lixh;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p1

    iget v5, v0, Lixh;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v10, v1, Lixi;->c:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    move v9, v1

    :goto_0
    if-ge v9, v10, :cond_6

    move v1, v2

    move v3, v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lixg;->a:Lixi;

    iget v5, v5, Lixi;->a:I

    if-ge v1, v5, :cond_5

    if-ne v2, v1, :cond_3

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lixg;->a:Lixi;

    iget v7, v7, Lixi;->a:I

    sub-int v8, v3, v7

    add-int/lit8 v7, v1, -0x1

    move v15, v7

    move/from16 v16, v8

    move-wide v7, v5

    move v5, v15

    move/from16 v6, v16

    :goto_3
    if-lt v5, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lixg;->a:Lixi;

    iget-object v11, v11, Lixi;->d:[D

    aget-wide v11, v11, v6

    move-object/from16 v0, p1

    iget-object v13, v0, Lixh;->d:[D

    add-int v14, v9, v5

    aget-wide v13, v13, v14

    mul-double/2addr v11, v13

    sub-double/2addr v7, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lixg;->a:Lixi;

    iget v11, v11, Lixi;->a:I

    sub-int/2addr v6, v11

    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    :cond_3
    const-wide/16 v5, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p1

    iget-object v5, v0, Lixh;->d:[D

    add-int v6, v9, v1

    move-object/from16 v0, p0

    iget-object v11, v0, Lixg;->a:Lixi;

    iget-object v11, v11, Lixi;->d:[D

    aget-wide v11, v11, v3

    div-double/2addr v7, v11

    aput-wide v7, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lixg;->a:Lixi;

    iget v5, v5, Lixi;->a:I

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    add-int/2addr v1, v9

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lixg;->a:Lixi;

    iget v3, v3, Lixi;->a:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v4, v3

    move v9, v1

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    sub-int v2, v10, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    add-int/lit8 v1, v1, -0x1

    move v7, v1

    move v8, v2

    :goto_4
    if-ltz v7, :cond_9

    add-int/lit8 v2, v10, -0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    move v6, v2

    :goto_5
    if-lt v5, v7, :cond_8

    move-object/from16 v0, p1

    iget-object v1, v0, Lixh;->d:[D

    add-int v2, v8, v5

    aget-wide v3, v1, v2

    add-int/lit8 v2, v6, 0x1

    add-int/lit8 v1, v5, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lixg;->a:Lixi;

    iget v9, v9, Lixi;->a:I

    if-ge v1, v9, :cond_7

    move-object/from16 v0, p0

    iget-object v9, v0, Lixg;->a:Lixi;

    iget-object v9, v9, Lixi;->d:[D

    aget-wide v11, v9, v2

    move-object/from16 v0, p1

    iget-object v9, v0, Lixh;->d:[D

    add-int v13, v8, v1

    aget-wide v13, v9, v13

    mul-double/2addr v11, v13

    sub-double/2addr v3, v11

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    move-object/from16 v0, p1

    iget-object v1, v0, Lixh;->d:[D

    add-int v2, v8, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lixg;->a:Lixi;

    iget-object v9, v9, Lixi;->d:[D

    aget-wide v11, v9, v6

    div-double/2addr v3, v11

    aput-wide v3, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    add-int/lit8 v1, v1, 0x1

    sub-int v2, v6, v1

    add-int/lit8 v1, v5, -0x1

    move v5, v1

    move v6, v2

    goto :goto_5

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    sub-int v2, v8, v1

    add-int/lit8 v1, v7, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lixg;->a:Lixi;

    iget v3, v3, Lixi;->a:I

    move v7, v1

    move v8, v2

    goto :goto_4

    :cond_9
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    move v3, v1

    move v4, v2

    :goto_7
    if-ge v3, v10, :cond_b

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_8
    if-ge v2, v4, :cond_a

    add-int v5, v3, v2

    add-int v6, v1, v4

    move-object/from16 v0, p1

    iget-object v7, v0, Lixh;->d:[D

    move-object/from16 v0, p1

    iget-object v8, v0, Lixh;->d:[D

    aget-wide v8, v8, v6

    aput-wide v8, v7, v5

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lixg;->a:Lixi;

    iget v5, v5, Lixi;->a:I

    add-int/2addr v1, v5

    goto :goto_8

    :cond_a
    add-int/lit8 v2, v4, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lixg;->a:Lixi;

    iget v1, v1, Lixi;->a:I

    add-int/2addr v1, v3

    move v3, v1

    move v4, v2

    goto :goto_7

    :cond_b
    return-void
.end method
