.class public final Leia;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:J

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-wide v2, p0, Leia;->a:J

    const-string v0, ""

    iput-object v0, p0, Leia;->b:Ljava/lang/String;

    iput-wide v2, p0, Leia;->c:J

    iput v1, p0, Leia;->d:I

    iput v1, p0, Leia;->e:I

    const/4 v0, -0x1

    iput v0, p0, Leia;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const-wide/16 v4, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v1, p0, Leia;->a:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Leia;->a:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Leia;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Leia;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-wide v1, p0, Leia;->c:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Leia;->c:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Leia;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Leia;->d:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Leia;->e:I

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Leia;->e:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Leia;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Leia;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leia;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Leia;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leia;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Leia;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const-wide/16 v3, 0x0

    iget-wide v0, p0, Leia;->a:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Leia;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_0
    iget-object v0, p0, Leia;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Leia;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-wide v0, p0, Leia;->c:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Leia;->c:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_2
    iget v0, p0, Leia;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Leia;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_3
    iget v0, p0, Leia;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Leia;->e:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_4
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leia;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leia;

    iget-wide v2, p0, Leia;->a:J

    iget-wide v4, p1, Leia;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Leia;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Leia;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Leia;->b:Ljava/lang/String;

    iget-object v3, p1, Leia;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Leia;->c:J

    iget-wide v4, p1, Leia;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Leia;->d:I

    iget v3, p1, Leia;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Leia;->e:I

    iget v3, p1, Leia;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget-wide v0, p0, Leia;->a:J

    iget-wide v2, p0, Leia;->a:J

    ushr-long/2addr v2, v5

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Leia;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Leia;->c:J

    iget-wide v3, p0, Leia;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leia;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leia;->e:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Leia;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
