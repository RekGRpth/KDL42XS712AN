.class public final Ljck;
.super Lizk;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:J

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:Z

.field private F:I

.field private G:Z

.field private H:I

.field private I:Z

.field private J:Lizf;

.field private K:Z

.field private L:Ljava/lang/String;

.field private M:Z

.field private N:Lizf;

.field private O:Z

.field private P:Ljava/lang/String;

.field private Q:I

.field a:Lizf;

.field b:Lizf;

.field private c:Z

.field private d:J

.field private e:Z

.field private f:Lizf;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Lizf;

.field private o:Z

.field private p:J

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:J

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-wide v1, p0, Ljck;->d:J

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->f:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->a:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->b:Lizf;

    const-string v0, ""

    iput-object v0, p0, Ljck;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljck;->l:Ljava/lang/String;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->n:Lizf;

    iput-wide v1, p0, Ljck;->p:J

    const-string v0, ""

    iput-object v0, p0, Ljck;->r:Ljava/lang/String;

    iput-wide v1, p0, Ljck;->t:J

    const-string v0, ""

    iput-object v0, p0, Ljck;->v:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljck;->x:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljck;->z:Ljava/lang/String;

    iput-wide v1, p0, Ljck;->B:J

    const-string v0, ""

    iput-object v0, p0, Ljck;->D:Ljava/lang/String;

    iput v3, p0, Ljck;->F:I

    iput v3, p0, Ljck;->H:I

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->J:Lizf;

    const-string v0, ""

    iput-object v0, p0, Ljck;->L:Ljava/lang/String;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljck;->N:Lizf;

    const-string v0, ""

    iput-object v0, p0, Ljck;->P:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Ljck;->Q:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljck;->Q:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljck;->b()I

    :cond_0
    iget v0, p0, Ljck;->Q:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljck;->a(J)Ljck;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->c(Lizf;)Ljck;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->a(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->b(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->d(Lizf;)Ljck;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Ljck;->o:Z

    iput-wide v0, p0, Ljck;->p:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->c(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljck;->b(J)Ljck;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->d(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->e(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->f(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljck;->c(J)Ljck;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->g(Ljava/lang/String;)Ljck;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljck;->a(I)Ljck;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljck;->b(I)Ljck;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    iput-boolean v2, p0, Ljck;->I:Z

    iput-object v0, p0, Ljck;->J:Lizf;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->h(Ljava/lang/String;)Ljck;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->a(Lizf;)Ljck;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->b(Lizf;)Ljck;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->e(Lizf;)Ljck;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljck;->i(Ljava/lang/String;)Ljck;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x22 -> :sswitch_2
        0x3a -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
        0x60 -> :sswitch_6
        0x6a -> :sswitch_7
        0x70 -> :sswitch_8
        0x7a -> :sswitch_9
        0x82 -> :sswitch_a
        0x8a -> :sswitch_b
        0x90 -> :sswitch_c
        0x9a -> :sswitch_d
        0xb0 -> :sswitch_e
        0xc0 -> :sswitch_f
        0xea -> :sswitch_10
        0xfa -> :sswitch_11
        0x332 -> :sswitch_12
        0x33a -> :sswitch_13
        0x1f42 -> :sswitch_14
        0x1f4a -> :sswitch_15
    .end sparse-switch
.end method

.method public final a(I)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->E:Z

    iput p1, p0, Ljck;->F:I

    return-object p0
.end method

.method public final a(J)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->c:Z

    iput-wide p1, p0, Ljck;->d:J

    return-object p0
.end method

.method public final a(Lizf;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->e:Z

    iput-object p1, p0, Ljck;->f:Lizf;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->i:Z

    iput-object p1, p0, Ljck;->j:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Ljck;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Ljck;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->b(IJ)V

    :cond_0
    iget-boolean v0, p0, Ljck;->h:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iget-object v1, p0, Ljck;->b:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_1
    iget-boolean v0, p0, Ljck;->i:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    iget-object v1, p0, Ljck;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Ljck;->k:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    iget-object v1, p0, Ljck;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Ljck;->m:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    iget-object v1, p0, Ljck;->n:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_4
    iget-boolean v0, p0, Ljck;->o:Z

    if-eqz v0, :cond_5

    const/16 v0, 0xc

    iget-wide v1, p0, Ljck;->p:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_5
    iget-boolean v0, p0, Ljck;->q:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xd

    iget-object v1, p0, Ljck;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_6
    iget-boolean v0, p0, Ljck;->s:Z

    if-eqz v0, :cond_7

    const/16 v0, 0xe

    iget-wide v1, p0, Ljck;->t:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_7
    iget-boolean v0, p0, Ljck;->u:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xf

    iget-object v1, p0, Ljck;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_8
    iget-boolean v0, p0, Ljck;->w:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x10

    iget-object v1, p0, Ljck;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_9
    iget-boolean v0, p0, Ljck;->y:Z

    if-eqz v0, :cond_a

    const/16 v0, 0x11

    iget-object v1, p0, Ljck;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_a
    iget-boolean v0, p0, Ljck;->A:Z

    if-eqz v0, :cond_b

    const/16 v0, 0x12

    iget-wide v1, p0, Ljck;->B:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_b
    iget-boolean v0, p0, Ljck;->C:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x13

    iget-object v1, p0, Ljck;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_c
    iget-boolean v0, p0, Ljck;->E:Z

    if-eqz v0, :cond_d

    const/16 v0, 0x16

    iget v1, p0, Ljck;->F:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_d
    iget-boolean v0, p0, Ljck;->G:Z

    if-eqz v0, :cond_e

    const/16 v0, 0x18

    iget v1, p0, Ljck;->H:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_e
    iget-boolean v0, p0, Ljck;->I:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x1d

    iget-object v1, p0, Ljck;->J:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_f
    iget-boolean v0, p0, Ljck;->K:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x1f

    iget-object v1, p0, Ljck;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_10
    iget-boolean v0, p0, Ljck;->e:Z

    if-eqz v0, :cond_11

    const/16 v0, 0x66

    iget-object v1, p0, Ljck;->f:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_11
    iget-boolean v0, p0, Ljck;->g:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x67

    iget-object v1, p0, Ljck;->a:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_12
    iget-boolean v0, p0, Ljck;->M:Z

    if-eqz v0, :cond_13

    const/16 v0, 0x3e8

    iget-object v1, p0, Ljck;->N:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_13
    iget-boolean v0, p0, Ljck;->O:Z

    if-eqz v0, :cond_14

    const/16 v0, 0x3e9

    iget-object v1, p0, Ljck;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_14
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljck;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Ljck;->d:J

    invoke-static {v0}, Lizh;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljck;->h:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v2, p0, Ljck;->b:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljck;->i:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x7

    iget-object v2, p0, Ljck;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljck;->k:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    iget-object v2, p0, Ljck;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljck;->m:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x9

    iget-object v2, p0, Ljck;->n:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljck;->o:Z

    if-eqz v1, :cond_5

    const/16 v1, 0xc

    iget-wide v2, p0, Ljck;->p:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljck;->q:Z

    if-eqz v1, :cond_6

    const/16 v1, 0xd

    iget-object v2, p0, Ljck;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Ljck;->s:Z

    if-eqz v1, :cond_7

    const/16 v1, 0xe

    iget-wide v2, p0, Ljck;->t:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Ljck;->u:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xf

    iget-object v2, p0, Ljck;->v:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Ljck;->w:Z

    if-eqz v1, :cond_9

    const/16 v1, 0x10

    iget-object v2, p0, Ljck;->x:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Ljck;->y:Z

    if-eqz v1, :cond_a

    const/16 v1, 0x11

    iget-object v2, p0, Ljck;->z:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Ljck;->A:Z

    if-eqz v1, :cond_b

    const/16 v1, 0x12

    iget-wide v2, p0, Ljck;->B:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Ljck;->C:Z

    if-eqz v1, :cond_c

    const/16 v1, 0x13

    iget-object v2, p0, Ljck;->D:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-boolean v1, p0, Ljck;->E:Z

    if-eqz v1, :cond_d

    const/16 v1, 0x16

    iget v2, p0, Ljck;->F:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-boolean v1, p0, Ljck;->G:Z

    if-eqz v1, :cond_e

    const/16 v1, 0x18

    iget v2, p0, Ljck;->H:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-boolean v1, p0, Ljck;->I:Z

    if-eqz v1, :cond_f

    const/16 v1, 0x1d

    iget-object v2, p0, Ljck;->J:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-boolean v1, p0, Ljck;->K:Z

    if-eqz v1, :cond_10

    const/16 v1, 0x1f

    iget-object v2, p0, Ljck;->L:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-boolean v1, p0, Ljck;->e:Z

    if-eqz v1, :cond_11

    const/16 v1, 0x66

    iget-object v2, p0, Ljck;->f:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-boolean v1, p0, Ljck;->g:Z

    if-eqz v1, :cond_12

    const/16 v1, 0x67

    iget-object v2, p0, Ljck;->a:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-boolean v1, p0, Ljck;->M:Z

    if-eqz v1, :cond_13

    const/16 v1, 0x3e8

    iget-object v2, p0, Ljck;->N:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-boolean v1, p0, Ljck;->O:Z

    if-eqz v1, :cond_14

    const/16 v1, 0x3e9

    iget-object v2, p0, Ljck;->P:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iput v0, p0, Ljck;->Q:I

    return v0
.end method

.method public final b(I)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->G:Z

    iput p1, p0, Ljck;->H:I

    return-object p0
.end method

.method public final b(J)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->s:Z

    iput-wide p1, p0, Ljck;->t:J

    return-object p0
.end method

.method public final b(Lizf;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->g:Z

    iput-object p1, p0, Ljck;->a:Lizf;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->k:Z

    iput-object p1, p0, Ljck;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final c(J)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->A:Z

    iput-wide p1, p0, Ljck;->B:J

    return-object p0
.end method

.method public final c(Lizf;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->h:Z

    iput-object p1, p0, Ljck;->b:Lizf;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->q:Z

    iput-object p1, p0, Ljck;->r:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Lizf;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->m:Z

    iput-object p1, p0, Ljck;->n:Lizf;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->u:Z

    iput-object p1, p0, Ljck;->v:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Lizf;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->M:Z

    iput-object p1, p0, Ljck;->N:Lizf;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->w:Z

    iput-object p1, p0, Ljck;->x:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->y:Z

    iput-object p1, p0, Ljck;->z:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->C:Z

    iput-object p1, p0, Ljck;->D:Ljava/lang/String;

    return-object p0
.end method

.method public final h(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->K:Z

    iput-object p1, p0, Ljck;->L:Ljava/lang/String;

    return-object p0
.end method

.method public final i(Ljava/lang/String;)Ljck;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljck;->O:Z

    iput-object p1, p0, Ljck;->P:Ljava/lang/String;

    return-object p0
.end method
