.class final Lbvn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwz;


# instance fields
.field private final a:Lcfc;

.field private final b:Lbvs;

.field private final c:I

.field private d:Lbvx;

.field private e:Lbwi;

.field private final f:Lcgb;


# direct methods
.method public constructor <init>(Lcfc;Lbvs;Lcgb;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbvn;->d:Lbvx;

    iput-object v0, p0, Lbvn;->e:Lbwi;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-ltz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p1, p0, Lbvn;->a:Lcfc;

    iput-object p2, p0, Lbvn;->b:Lbvs;

    iput p4, p0, Lbvn;->c:I

    iput-object p3, p0, Lbvn;->f:Lcgb;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lbvn;->d:Lbvx;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbvn;->d:Lbvx;

    iget-boolean v2, v1, Lbvx;->c:Z

    const-string v3, "Must not call this method before finish()"

    invoke-static {v2, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v1, v1, Lbvx;->b:Ljava/lang/String;

    iget-object v2, p0, Lbvn;->e:Lbwi;

    invoke-virtual {v2}, Lbwi;->b()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    iget-object v2, p0, Lbvn;->f:Lcgb;

    invoke-virtual {v2, v1, v0}, Lcgb;->a(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lbvn;->f:Lcgb;

    invoke-virtual {v0}, Lcgb;->k()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lbvn;->b:Lbvs;

    invoke-interface {v1}, Lbvs;->a()V

    iget-object v1, p0, Lbvn;->b:Lbvs;

    invoke-interface {v1, v0}, Lbvs;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lbvt;Landroid/content/SyncResult;)V
    .locals 4

    iget-object v0, p0, Lbvn;->f:Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    invoke-virtual {v0}, Lbuw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbvn;->f:Lcgb;

    iget-object v0, v0, Lcgb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    new-instance v2, Lbwi;

    iget-object v3, p0, Lbvn;->b:Lbvs;

    invoke-direct {v2, v3, v0, v1}, Lbwi;-><init>(Lbvs;J)V

    iput-object v2, p0, Lbvn;->e:Lbwi;

    new-instance v0, Lbvx;

    iget-object v1, p0, Lbvn;->e:Lbwi;

    invoke-direct {v0, v1}, Lbvx;-><init>(Lbvs;)V

    iput-object v0, p0, Lbvn;->d:Lbvx;

    iget-object v0, p0, Lbvn;->a:Lcfc;

    iget-object v0, v0, Lcfc;->a:Ljava/lang/String;

    iget-object v1, p0, Lbvn;->f:Lcgb;

    iget-object v1, v1, Lcgb;->a:Lbuw;

    iget v2, p0, Lbvn;->c:I

    iget-object v3, p0, Lbvn;->d:Lbvx;

    invoke-virtual {p1, v1, v0, v2, v3}, Lbvt;->a(Lbuw;Ljava/lang/String;ILbvs;)V

    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "SyncMoreAlgorithm[delegate=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbvn;->b:Lbvs;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
