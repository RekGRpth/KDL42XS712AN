.class final Leef;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Leei;

.field final synthetic b:Lcom/google/android/gms/games/multiplayer/Invitation;

.field final synthetic c:Landroid/app/Activity;


# direct methods
.method constructor <init>(Leei;Lcom/google/android/gms/games/multiplayer/Invitation;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Leef;->a:Leei;

    iput-object p2, p0, Leef;->b:Lcom/google/android/gms/games/multiplayer/Invitation;

    iput-object p3, p0, Leef;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const-string v0, "UiUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Leef;->a:Leei;

    iget-object v1, p0, Leef;->b:Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0, v1}, Leei;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v0, Ldvi;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Leef;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
