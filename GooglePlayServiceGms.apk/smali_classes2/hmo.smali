.class final Lhmo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0x42

    const-wide v7, 0x3fc3333333333333L    # 0.15

    const/16 v6, 0xb

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3feda00c0f01fb83L    # 0.925787

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9272862f5989dfL    # 0.018015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f543bf727136a40L    # 0.001235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023ca4f440af1ccL    # 9.895136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40224e34330d7386L    # 9.152742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1e2584f4c6e6daL    # 1.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402355c1615ebfa9L    # 9.667491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe19ec918e325d5L    # 0.550633

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde09bb6aa4b988L    # 0.469344

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f04f8b588e368f1L    # 4.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fe026c3b927d45aL    # 0.504732

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f51b60ae9680e06L    # 0.001081

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40236cf2cf95d4e9L    # 9.71279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2344806290eed0L    # 1.47E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f223810e8858ff7L    # 1.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f09b0ab2e1693c0L    # 4.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff4b55043e5321eL    # 1.294266

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5719f7f8ca8199L    # 0.00141

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa3f65dbfceb789L    # 0.038989

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa41a6937d1fe65L    # 0.039264

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde1737542a23c0L    # 0.470167

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1083dbc23315d7L    # 6.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f27b95a294141eaL    # 1.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f80129cbab649d4L    # 0.007848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f40adcd2d44dca9L    # 5.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a79fec99f1ae3L    # 1.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdb13b18dac258dL    # 0.423077

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ee711947cfa26a2L    # 1.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2733226c3b927dL    # 1.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fff42ea960b6fa0L    # 1.953837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f442f61ed5ae1ceL    # 6.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0b8fbca1059eaL    # 0.522581

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f79806f262888b5L    # 0.006226

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff9ea66dbd72bcbL    # 1.619727

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f14f8b588e368f1L    # 8.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa27a63736cdf26L    # 0.03609

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f057eed45e9185dL    # 4.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c4fc1df3300deL    # 1.08E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26f0068db8bac7L    # 1.75E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f469c23b7952d23L    # 6.9E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26ce789e774eecL    # 1.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd55553ef6b5d46L    # 0.333333

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f74727dcbddb984L    # 0.004992

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd01b21c475e636L    # 0.251656

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd8a18372e6a769L    # 0.384858

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc097b7414a4d2bL    # 0.12963

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f196d8f4f93bc0aL    # 9.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f47e34b945308bcL    # 7.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2d3aa369fcf3dcL    # 2.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0e68a0d349be90L    # 5.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40063e36d22424a2L    # 2.780378

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffe45ad538ac190L    # 1.892011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffe6bba98eda22fL    # 1.901301

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402184ba732df506L    # 8.759235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f8db48909289eL    # 0.030814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40f4517614595L    # 0.156716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd521d10b1feeb3L    # 0.330189

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd4892ab68cef67L    # 0.320872

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f98c586876e1debL    # 0.024191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400198e757928e0dL    # 2.19966

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb6d3f9e7b80a9eL    # 0.089172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ceaf251c193b4L    # 0.00353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ef284167a95dL    # 0.155736

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020beb5f5f0b285L    # 8.372482

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7be4cd74927914L    # 0.00681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4027274c2b51bd62L    # 11.576753

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc407c4199fe436L    # 0.156487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4932d6ece13f4bL    # 7.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc403c4b09e98ddL    # 0.156365

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f6f4be835deeL    # 0.155974

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddef7c243dcceaL    # 0.467742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd842edbb59ddc2L    # 0.379085

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f352b0a6fc58ab9L    # 3.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f85f45e0b4e11dcL    # 0.01072

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fac3009b30728e9L    # 0.055054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f738bcdfefbf402L    # 0.004772

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4873ffac1d29eL    # 0.320755

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f255d5f56a7ac82L    # 1.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023dc2e557de0d6L    # 9.930041

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f25a07b352a8438L    # 1.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b845564b662feL    # 0.003359

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8f811f4f50a03L    # 0.780282

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd3a9a3d2d87f88L    # 0.307229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e5a78f25a2510L    # 0.014821

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f19b0ab2e1693c0L    # 9.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40232706b37867f1L    # 9.576223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ffef39085f4aL    # 0.156248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd7f97edc7ef178L    # 0.374603

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5ad6451b93037dL    # 0.001638

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eef75104d551d69L    # 1.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40243e6299524bfdL    # 10.121846

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffea96f6512a950L    # 1.916366

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0de26916440f24L    # 5.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3797cc39ffd60fL    # 3.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4027c694ccab3edeL    # 11.887854

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5633482be8bc17L    # 0.001355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2797cc39ffd60fL    # 1.8E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f783b60285ec3dbL    # 0.005916

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f302ff8ec0f8833L    # 2.47E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde06f694467382L    # 0.469175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402250a2a90cd424L    # 9.157491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402163f7f06705c9L    # 8.695251

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6142b302f72b45L    # 0.002107

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022ea339c0ebee0L    # 9.457425

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f93486f049a9974L    # 0.018831

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc611a975afaf86L    # 0.172414

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4028a4f0520d130eL    # 12.322146

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fab8e6d15ad106fL    # 0.053821

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f74e4c942d490e6L    # 0.005101

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff80807357e670eL    # 1.50196

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff206f7a0b5ed8dL    # 1.126701

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc842e9899bf594L    # 0.189542

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f216ebd4cfd08d5L    # 1.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f381e03f705857bL    # 3.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f84b380cb6c7a7dL    # 0.010108

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5b97353b4b2fa9L    # 0.001684

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40099704ff43419eL    # 3.19874

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f454c985f06f694L    # 6.5E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b530ced4e4c94L    # 0.006671

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd5aaef2c732592L    # 0.338558

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c3efae7924af1L    # 0.001724

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40018d26fa3fcc9fL    # 2.193922

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ba7fc32ebe597L    # 4.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c92ddbdb5d895L    # 2.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13a92a30553261L    # 7.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcb4067cf1c3266L    # 0.212903

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f11d3671ac14c66L    # 6.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022b185271bcdbcL    # 9.346719

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f014d2f5dbb9cfaL    # 3.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fef39085f4a1L    # 0.156218

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f972c94b380cb6cL    # 0.022631

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9d66adb402d16cL    # 0.028712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2bc98a222d5172L    # 2.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5cac083126e979L    # 0.00175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f10c6f7a0b5ed8dL    # 6.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3aac53b0813cacL    # 4.07E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcf1fa333764f12L    # 0.243153

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13ec460ed80a18L    # 7.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f12dfd694ccab3fL    # 7.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2322f2734f82f5L    # 1.46E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd512231832fcadL    # 0.329232

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f181e03f705857bL    # 9.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0040bfe3b03e21L    # 3.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f26052502eec7c9L    # 1.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ee92a737110e454L    # 1.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024ad796bfca85dL    # 10.338817

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0f75104d551d69L    # 6.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0f75104d551d69L    # 6.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe884fd2a62aa19L    # 0.766234

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc407357e670e2cL    # 0.15647

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40238a97e132b55fL    # 9.77069

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3f31f46ed245b3L    # 4.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4001726aec0724b7L    # 2.180868

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b64e054690de1L    # 2.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd28ac5c13fd0d0L    # 0.28972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13ec460ed80a18L    # 7.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3acde19fc2a887L    # 4.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40294b586ca89fc7L    # 12.647159

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe999999999999aL    # 0.8

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fec2d14a0a0f4d8L    # 0.880503

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f766277c45cbbc3L    # 0.005465

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf6b0d9513f8dbL    # 0.490909

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa94467381d7dbfL    # 0.04935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff92a65cf67b1c0L    # 1.572851

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7346994185058eL    # 0.004706

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f510a137f38c543L    # 0.00104

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f33122b7baecd08L    # 2.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b0ab2e1693c04L    # 0.006602

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fad87f88765ba6fL    # 0.057678

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4167a95c853c1L    # 0.156936

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdb4be835dedf1eL    # 0.426508

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61f70de8f6ceffL    # 0.002193

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc257b84db9c737L    # 0.143302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8a2595bbbe8790L    # 0.012767

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c2e33eff19503L    # 2.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401486fc58ab92c0L    # 5.131822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffa839bcba30121L    # 1.657131

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc7d805e5f30e80L    # 0.18628

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd3d91eeaa6d267L    # 0.310127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa9fe86833c6003L    # 0.05077

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb3ed634549b62cL    # 0.077841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdcd2391d57ff9bL    # 0.450331

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa0e581cf7878b8L    # 0.033001

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f63ff25e56cd6c3L    # 0.002441

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fd6bb6672fba01fL    # 0.355188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f04f8b588e368f1L    # 4.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4c68ec52a411c3L    # 8.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2626b2f23033a4L    # 1.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdfbdf090f733a9L    # 0.495968

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3b97353b4b2fa9L    # 4.21E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdae24febd09e13L    # 0.420063

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a79fec99f1ae3L    # 1.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f04f8b588e368f1L    # 4.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef1d3671ac14c66L    # 1.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc54434e3369b9dL    # 0.166144

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2733226c3b927dL    # 1.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40186cd466f5019fL    # 6.106279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f693f6c2699c7bdL    # 0.003082

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5c36976bc1effaL    # 0.001722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a5870da5daf08L    # 2.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f35b1422ccb3a26L    # 3.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f62f6e82949a565L    # 0.002315

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddeca686e7e62eL    # 0.467569

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe2a20578e5c4ebL    # 0.582278

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1eeed8904f6dfcL    # 1.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0eeed8904f6dfcL    # 5.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8276fb09203a32L    # 0.009016

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb2bb55ac03ff69L    # 0.073171

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f47d2849cb252ceL    # 7.27E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc424edf6124074L    # 0.157377

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43879c4113c686L    # 5.96E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4c92ddbdb5d895L    # 8.72E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c0ca600b02928L    # 1.07E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe09f969e3c9689L    # 0.519481

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa985058dde7a74L    # 0.049843

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5eb4202d9cf13dL    # 0.001874

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4003aab9b23dd54eL    # 2.458362

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde0578e5c4eb57L    # 0.469084

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4006783f0bae89ecL    # 2.808714

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4bc98a222d5172L    # 8.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7479d4d834091cL    # 0.004999

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f51f4f50a02b841L    # 0.001096

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc406e9ff0cbaf9L    # 0.156461

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44bdfd2630ec31L    # 6.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6782d38476f2a6L    # 0.00287

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f168b5cbff47736L    # 8.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402f5723ee1bd1eeL    # 15.670196

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26aceaaf35e311L    # 1.73E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5fd9ba1b1960faL    # 0.001944

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fafb6bb1290257dL    # 0.061941

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9871a3b14a9047L    # 0.023871

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fb9a57646ae3a3bL    # 0.100181

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2abd1aa821f299L    # 2.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1322f2734f82f5L    # 7.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2450efdc9c4da9L    # 1.55E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44163779e9d0eaL    # 6.13E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc408398a654930L    # 0.156501

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3e4cd74927914L    # 0.15542

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023128cbd1244a6L    # 9.53623

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ba7fc32ebe597L    # 4.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5ac57e23f24d90L    # 0.001634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc465a57646ae3aL    # 0.159352

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d29dc725c3deeL    # 0.00356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8701d9f4d37c13L    # 0.011234

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f96d3f9e7b80a9eL    # 0.022293

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f79620685553ef7L    # 0.006197

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40b88ca3e7d13L    # 0.156602

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc1df090f733a8aL    # 0.139619

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403343076c050bd8L    # 19.261832

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fca1f5be5d9e40dL    # 0.204082

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44cec41dd1a21fL    # 6.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc52fa93af74cd3L    # 0.165517

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5376d549731099L    # 0.001188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb1fe21d96e9bbfL    # 0.070284

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401ffefd438d1d8aL    # 7.999013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f951a437824d4ccL    # 0.020608

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f959146e4c0df59L    # 0.021062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f754b8bef8ceb35L    # 0.005199

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5df761cbccf28cL    # 0.001829

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62fd32c625e99fL    # 0.002318

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4815a07b352a84L    # 7.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f56a055757d5a9fL    # 0.001381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2beb18116ebd4dL    # 2.13E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f921dda059a73b4L    # 0.017692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f68bf7f06705c89L    # 0.003021

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91a47a9e2bcf92L    # 0.017229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f1a5ca29845eL    # 0.155812

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc415f45e0b4e12L    # 0.15692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6142b302f72b45L    # 0.002107

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40498aa8e2e2b8c7L    # 51.083279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f55a4acf312b1b3L    # 0.001321

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f129cbab649d389L    # 7.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4027b526fa3fcc9fL    # 11.853813

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc997353b4b2fa9L    # 0.199927

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd6354c122749f1L    # 0.347003

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7015c209246bf0L    # 0.003927

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0dba908a265f1L    # 0.526814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f27b95a294141eaL    # 1.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402506876e1deacdL    # 10.512752

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2d9f4d37c1376dL    # 2.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403065b8ed1bf7adL    # 16.397353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402274a0c282c6efL    # 9.227789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fde12839042d8c3L    # 0.46988

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6a91105e1c1509L    # 0.003243

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40311b80fdc1615fL    # 17.107437

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f40be9424e59296L    # 5.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3f31f46ed245b3L    # 4.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe6b83cf2cf95d5L    # 0.70999

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5f53825e13b18eL    # 0.001912

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f196d8f4f93bc0aL    # 9.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f129cbab649d389L    # 7.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6958969a0ad8a1L    # 0.003094

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40086d06fef7c244L    # 3.053236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f33122b7baecd08L    # 2.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f58c5c9a34ca0c3L    # 0.001512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2ba7fc32ebe597L    # 2.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f435daad601ffb5L    # 5.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff0569b17481b22L    # 1.021144

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff3f03e20ccff22L    # 1.246153

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd399999999999aL    # 0.30625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f885be1a8262457L    # 0.011894

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a79fec99f1ae3L    # 2.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43b9f127f5e84fL    # 6.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc292f6e82949a5L    # 0.14511

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5cc100e6afcce2L    # 0.001755

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb9d12cadddf43cL    # 0.100848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f995feda6612839L    # 0.02478

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde03254e6e221dL    # 0.468942

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff706af46aa087dL    # 1.439132

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f774d594f26aec0L    # 0.005689

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f61d798d8a979e1L    # 0.002178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4030a28d10f51acaL    # 16.634965

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x400bc10624dd2f1bL    # 3.46925

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f82bdc26dce39b4L    # 0.009151

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4038ff0f27bb2fecL    # 24.996325

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa694467381d7dcL    # 0.0441

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc4cccccccccccdL    # 0.1625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa6fc158fb43d8aL    # 0.044892

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f62ccf6be37de94L    # 0.002295

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4031f7aa9b499d02L    # 17.967447

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff6d752977c88e8L    # 1.427569

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7a9003eea209abL    # 0.006485

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef1d3671ac14c66L    # 1.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faaee84ad794a6fL    # 0.052601

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403023b19e731d2eL    # 16.139429

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8436b8f9b13166L    # 0.00987

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4abd1aa821f299L    # 8.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff9d185cee17a03L    # 1.613653

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40184e95b78cc9a7L    # 6.076743

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40237e8ff327aa69L    # 9.747192

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f54ec204f2ae07eL    # 0.001277

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f98cf398e970718L    # 0.024228

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb25c2d2780778aL    # 0.071719

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40210fb267c6b8b7L    # 8.530658

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc3716d2aa5c5f8L    # 0.151899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5376d549731099L    # 0.001188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8da48b65237048L    # 0.014474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402ebd08d4bad7d8L    # 15.369208

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4039557ba6698bb5L    # 25.333918

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fa0df58c08b76L    # 7.907102

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e56577531db44L    # 7.584318

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f96b8305d95dd4cL    # 0.022187

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f663779e9d0e992L    # 0.002712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402278576cce5f74L    # 9.235042

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f808b32ce89656fL    # 0.008078

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faff2e48e8a71deL    # 0.0624

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc349a5657fb69aL    # 0.150685

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4043c8ef5ec80c74L    # 39.569805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7606317268d328L    # 0.005377

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40054cb3a2595bbcL    # 2.662452

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0c63f141205bcL    # 0.065525

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f47e34b945308bcL    # 7.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40003f6db940feceL    # 2.030971

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc34299d883ba34L    # 0.15047

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033903547e06962L    # 19.563313

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4036634afd545415L    # 22.387863

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff687ae147ae148L    # 1.408125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f92d0e560418937L    # 0.018375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff2e4851a869403L    # 1.180791

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033df6c7a7c9de0L    # 19.872749

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402e7fc6540cc78fL    # 15.24956

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fce975afaf85943L    # 0.238994

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb18a979e16d6dcL    # 0.068521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a437824d4cb9fL    # 0.001603

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f82495e17e34b94L    # 0.008929

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa1d57ff9b56324L    # 0.034832

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40055a9e2bcf91a3L    # 2.669247

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0b2e9ccb7d417L    # 0.06523

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL    # 1.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc476c8b4395810L    # 0.159875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f8b1bbcf4e875L    # 7.885848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402f5953dea465a5L    # 15.674468

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fcb6b228dc981bfL    # 0.214207

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fee09eba6aca793L    # 0.938711

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e405f6ba0620bL    # 7.562864

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6a1986b9c304cdL    # 0.003186

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f45a8deb0fadf2fL    # 6.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6eafee6fb4c3c2L    # 0.003746

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021d8e0c9d9d346L    # 8.92359

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f96a7264a16a487L    # 0.022122

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403a4a3369b9d7feL    # 26.289847

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f763022dd7a99faL    # 0.005417

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4037ab851eb851ecL    # 23.67

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb86fcdee34fc61L    # 0.095456

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd46290eed02cd4L    # 0.318516

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb84dca8e2e2b8cL    # 0.094937

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb12bb23571d1d4L    # 0.067073

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f978e9f6a93f291L    # 0.023005

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb90acc0bdcad15L    # 0.097821

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb61f9f01b866e4L    # 0.08642

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6a1986b9c304cdL    # 0.003186

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4002bc13fd0d0679L    # 2.341835

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff183d577963993L    # 1.094686

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020001adea89763L    # 8.000205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76c7219220ff54L    # 0.005561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f45b9a5a89b951cL    # 6.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff696366d7a56deL    # 1.411673

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbb497b7414a4d3L    # 0.10659

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a79fec99f1ae3L    # 1.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd63e63e8dda48bL    # 0.347558

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fbdfa54c5543287L    # 0.117101

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa1096bb98c7e28L    # 0.033275

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc79439de481f54L    # 0.184211

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f816ebd4cfd08d5L    # 0.008512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8506dd69d30271L    # 0.010267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404a2fd3fe1975f3L    # 52.373657

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb651a005c4651fL    # 0.087183

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc721ee675147f1L    # 0.180723

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb3419e30014f8bL    # 0.07522

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033d759146e4c0eL    # 19.841203

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff92598e10cf5b2L    # 1.571679

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe24aeea63b688cL    # 0.571647

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40347f9d3cbc48f1L    # 20.498493

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa359146e4c0df6L    # 0.037789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7655e28aa43351L    # 0.005453

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41a719b4dcec0L    # 0.157057

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e1369b9d7fd82L    # 7.518958

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034ea7829068987L    # 20.915896

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6fdbd2fa0d77b8L    # 0.003889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3feef38a3b57c4e3L    # 0.967229

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41bfbdf090f73L    # 0.157104

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4c37e6f71a7e3L    # 0.040554

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff910470a808c82L    # 1.566474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbddc50ce4ead0cL    # 0.116643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400b97bd05af6c6aL    # 3.449091

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4016f7cdcca70d20L    # 5.741996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4007c52e72da1230L    # 2.97128

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401543365881a155L    # 5.315637

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3faf7b395c422036L    # 0.061487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400b9bdbe3c10518L    # 3.451103

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f11d3671ac14c66L    # 6.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400cb71c10d7be98L    # 3.589409

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f64f69ca9ef5233L    # 0.002559

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f51ba3ca7503b82L    # 0.001082

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4158750c1b973L    # 0.156907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403f65dfb9389b52L    # 31.397945

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa245d41fa76534L    # 0.035689

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x40122eab367a0f91L    # 4.545575

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cc

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3faf9fa97e132b56L    # 0.061765

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ca

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f110a137f38c543L    # 6.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa8f7db6e503fb3L    # 0.048766

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f407b784662bae0L    # 5.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40059f8769ec2ce4L    # 2.702895

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6157abb8800eaeL    # 0.002117

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdb425f202107b8L    # 0.425926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f865dbfceb78898L    # 0.010921

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4565c2d2780779L    # 6.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2de26916440f24L    # 2.28E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ca
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40353bfdf7e8038aL    # 21.234344

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
