.class abstract Lhyx;
.super Lhyy;
.source "SourceFile"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# instance fields
.field final synthetic b:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhyx;->b:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyy;-><init>(Lhyt;Lhys;)V

    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/HashMap;
    .locals 5

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    iget-object v4, v0, Lhxp;->b:Landroid/app/PendingIntent;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, v0, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private a(Ljava/util/List;I)V
    .locals 9

    const/4 v8, 0x3

    const-string v0, "GeofencerStateMachine"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Firing geofences, transition="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lill;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GeofencerStateMachine"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Ignoring geofence alerts because the service is not owned by foreground user."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :pswitch_1
    const-string v0, "dwell"

    goto :goto_0

    :pswitch_2
    const-string v0, "enter"

    goto :goto_0

    :pswitch_3
    const-string v0, "exit"

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lhyx;->a(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->q(Lhyt;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION removed in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const-string v2, "GeofencerStateMachine"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Firing geofence: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.location.intent.extra.transition"

    invoke-virtual {v3, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v3, v1}, Leqf;->a(Landroid/content/Intent;Ljava/util/ArrayList;)V

    iget-object v1, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    iget-object v4, v1, Lhys;->m:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v5, v1, Lhys;->n:Landroid/content/Context;

    invoke-static {v5}, Lbqf;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v4, v2}, Lbqf;->a(ILjava/lang/String;)Landroid/os/WorkSource;

    move-result-object v2

    iget-object v4, v1, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v2}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_6
    :goto_2
    iget-object v1, v1, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_1
    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->q(Lhyt;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_7

    const-string v1, "GeofencerStateMachine"

    const-string v2, "Removing canceled pending intent."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lhyx;->c:Lhys;

    invoke-virtual {v1, v0}, Lhys;->a(Landroid/app/PendingIntent;)I

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->d()V

    goto/16 :goto_1

    :catch_1
    move-exception v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Z)V
    .locals 3

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhys;->b(D)V

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhyx;->b(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhyx;->b:Lhyt;

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->m(Lhyt;)Lhzc;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->l(Lhyt;Lbqg;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0, p1}, Lhys;->a(Landroid/content/Intent;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 13

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "processMessage, current state="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhyx;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " msg="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lhyx;->b:Lhyt;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Lhyt;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x4

    if-eq v1, v0, :cond_c

    const/4 v0, 0x5

    if-eq v1, v0, :cond_c

    iget-object v0, p0, Lhyx;->c:Lhys;

    iget-object v4, v0, Lhys;->i:Lhxq;

    invoke-virtual {v4}, Lhxq;->c()Z

    iget-object v0, v0, Lhys;->h:Lhxq;

    invoke-virtual {v0}, Lhxq;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v4, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lhys;->b(D)V

    iget-object v4, p0, Lhyx;->c:Lhys;

    invoke-virtual {v4}, Lhys;->c()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v0, p0, Lhyx;->b:Lhyt;

    invoke-static {v0, p1}, Lhyt;->c(Lhyt;Landroid/os/Message;)V

    iget-object v0, p0, Lhyx;->b:Lhyt;

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->m(Lhyt;)Lhzc;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->i(Lhyt;Lbqg;)V

    :cond_1
    :goto_0
    return v3

    :cond_2
    move v4, v0

    :goto_1
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p1}, Lhyx;->b(Landroid/os/Message;)Z

    move-result v3

    move v0, v2

    :cond_3
    :goto_2
    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v1, v1, Lhys;->f:Lhyi;

    iget-object v1, v1, Lhyi;->g:Lhzo;

    invoke-virtual {v1}, Lhzo;->e()V

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_4

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Geofence expires but message handling method does not update requirement, updating now."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0, v2}, Lhyx;->b(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhyx;->b:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->a()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Network location disabled."

    invoke-static {v0, v1}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhyx;->b:Lhyt;

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->n(Lhyt;)Lhyz;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->j(Lhyt;Lbqg;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lhyx;->b:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v1, "GPS disabled."

    invoke-static {v0, v1}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lhyx;->j()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhyk;

    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v1, v1, Lhys;->d:Lhxw;

    iget-object v1, v1, Lhxw;->c:Landroid/util/Pair;

    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v5, v0, Lhyk;->a:Ljava/util/List;

    iget-object v6, v0, Lhyk;->b:Landroid/app/PendingIntent;

    invoke-virtual {v6}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_7

    move v1, v2

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lhyk;->a(Ljava/lang/Object;)V

    iget-object v5, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v6

    iget-object v0, v5, Lhys;->d:Lhxw;

    iget-object v8, v0, Lhxw;->c:Landroid/util/Pair;

    if-eqz v8, :cond_8

    iget-object v0, v5, Lhys;->c:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v9

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    sub-long/2addr v9, v11

    const-wide/16 v11, 0x1388

    cmp-long v0, v9, v11

    if-gez v0, :cond_8

    invoke-virtual {v5, v8, v2, v6, v7}, Lhys;->a(Landroid/util/Pair;ZD)Lhxn;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_6

    iget-object v5, v0, Lhxn;->a:Ljava/util/ArrayList;

    if-eqz v5, :cond_6

    iget-object v0, v0, Lhxn;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v3}, Lhyx;->a(Ljava/util/List;I)V

    :cond_6
    if-nez v1, :cond_9

    move v0, v3

    :goto_5
    invoke-direct {p0, v0}, Lhyx;->c(Z)V

    const-string v1, "GeofencerStateMachine"

    const/4 v5, 0x3

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GeofencerStateMachine"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Number of valid geofences now: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lhyx;->c:Lhys;

    iget-object v6, v6, Lhys;->h:Lhxq;

    invoke-virtual {v6}, Lhxq;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    iget-object v1, v1, Lhys;->h:Lhxq;

    invoke-virtual {v1, v5, v6}, Lhxq;->a(Ljava/util/List;Landroid/app/PendingIntent;)I

    move-result v1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhzv;

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_a

    const-string v1, "GeofencerStateMachine"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "removeFence: request="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget v1, v0, Lhzv;->a:I

    packed-switch v1, :pswitch_data_1

    move v1, v2

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lhzv;->a(Ljava/lang/Object;)V

    if-nez v1, :cond_b

    move v0, v3

    :goto_7
    invoke-direct {p0, v2}, Lhyx;->c(Z)V

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "GeofencerStateMachine"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Number of valid geofences now: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lhyx;->c:Lhys;

    iget-object v6, v6, Lhys;->h:Lhxq;

    invoke-virtual {v6}, Lhxq;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v5, v0, Lhzv;->c:Ljava/lang/String;

    iget-object v6, v1, Lhys;->i:Lhxq;

    invoke-virtual {v6, v5}, Lhxq;->a(Ljava/lang/String;)I

    iget-object v1, v1, Lhys;->h:Lhxq;

    invoke-virtual {v1, v5}, Lhxq;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_6

    :pswitch_5
    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v5, v0, Lhzv;->e:Landroid/app/PendingIntent;

    invoke-virtual {v1, v5}, Lhys;->a(Landroid/app/PendingIntent;)I

    move-result v1

    goto :goto_6

    :pswitch_6
    iget-object v1, p0, Lhyx;->c:Lhys;

    iget-object v5, v0, Lhzv;->f:[Ljava/lang/String;

    iget-object v6, v0, Lhzv;->c:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lhys;->a([Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_6

    :cond_b
    move v0, v2

    goto :goto_7

    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    invoke-virtual {p0, v0}, Lhyx;->a(Landroid/util/Pair;)Z

    move-result v0

    goto/16 :goto_2

    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lhzp;

    invoke-virtual {p0, v0}, Lhyx;->a(Lhzp;)Lbqh;

    move-result-object v0

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1}, Lhyt;->r(Lhyt;)V

    iget-object v1, p0, Lhyx;->b:Lhyt;

    invoke-static {v1, v0}, Lhyt;->m(Lhyt;Lbqg;)V

    move v0, v2

    goto/16 :goto_2

    :pswitch_9
    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lhys;->b(D)V

    invoke-virtual {p0, v3}, Lhyx;->b(Z)V

    move v0, v3

    goto/16 :goto_2

    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lhyx;->a(Landroid/content/Intent;)Z

    move-result v0

    goto/16 :goto_2

    :pswitch_b
    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->e()V

    move v0, v2

    goto/16 :goto_2

    :pswitch_c
    iget-object v0, p0, Lhyx;->b:Lhyt;

    iget-object v1, p0, Lhyx;->c:Lhys;

    invoke-virtual {v1}, Lhys;->f()Lhzp;

    move-result-object v1

    invoke-virtual {p0, v1}, Lhyx;->a(Lhzp;)Lbqh;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->k(Lhyt;Lbqg;)V

    iget-object v0, p0, Lhyx;->b:Lhyt;

    invoke-static {v0}, Lhyt;->p(Lhyt;)V

    move v0, v2

    goto/16 :goto_2

    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lhyx;->a(Z)Z

    move-result v0

    goto/16 :goto_2

    :cond_c
    move v4, v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected a(Landroid/util/Pair;)Z
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v1

    invoke-virtual {v0, p1, v4, v1, v2}, Lhys;->a(Landroid/util/Pair;ZD)Lhxn;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Triggering location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, v0, Lhxn;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lhxn;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v4}, Lhyx;->a(Ljava/util/List;I)V

    :cond_1
    iget-object v1, v0, Lhxn;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lhxn;->b:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lhyx;->a(Ljava/util/List;I)V

    :cond_2
    iget-object v1, v0, Lhxn;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lhxn;->c:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lhyx;->a(Ljava/util/List;I)V

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhyx;->b(Z)V

    return v4
.end method

.method protected a(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected b(Z)V
    .locals 12

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, -0x1

    iget-object v5, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->e()I

    move-result v2

    invoke-virtual {p0}, Lhyx;->f()I

    move-result v0

    if-ne v2, v1, :cond_7

    if-ne v0, v1, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lhyx;->g()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {p0}, Lhyx;->d()I

    move-result v7

    invoke-virtual {p0}, Lhyx;->h()Ljava/util/Collection;

    move-result-object v2

    if-lez v0, :cond_9

    iget-object v8, v5, Lhys;->g:Lhzk;

    invoke-virtual {v8, v0, p1, v6}, Lhzk;->a(IZLjava/util/Collection;)V

    :goto_1
    if-lez v7, :cond_d

    iget-object v5, v5, Lhys;->f:Lhyi;

    if-lez v7, :cond_a

    move v0, v3

    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Activity update interval should be positive: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbiq;->a(ZLjava/lang/Object;)V

    if-nez v2, :cond_e

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityDetector"

    const-string v2, "Blaming ourself for activity updates."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lild;

    iget v2, v5, Lhyi;->d:I

    iget-object v6, v5, Lhyi;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v6}, Lild;-><init>(ILjava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_3
    iget-object v2, v5, Lhyi;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v6, v5, Lhyi;->j:I

    if-ne v7, v6, :cond_2

    iget-object v6, v5, Lhyi;->k:Ljava/util/Collection;

    if-eqz v6, :cond_2

    iget-object v6, v5, Lhyi;->k:Ljava/util/Collection;

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    :cond_2
    iget-object v6, v5, Lhyi;->g:Lhzo;

    invoke-virtual {v6}, Lhzo;->b()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-gez v6, :cond_b

    :cond_3
    :goto_4
    const-string v4, "ActivityDetector"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "ActivityDetector"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "requestActivity: intervalSec="

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", trigger="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", clients="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget v4, v5, Lhyi;->j:I

    if-ne v4, v1, :cond_5

    iget-object v1, v5, Lhyi;->g:Lhzo;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lhzo;->a(Z)V

    :cond_5
    iput v7, v5, Lhyi;->j:I

    iput-object v0, v5, Lhyi;->k:Ljava/util/Collection;

    invoke-virtual {v5, v7, v3, v0}, Lhyi;->a(IZLjava/util/Collection;)V

    :cond_6
    :goto_5
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_6
    return-void

    :cond_7
    if-ne v0, v1, :cond_8

    move v0, v2

    goto/16 :goto_0

    :cond_8
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v2, 0x14

    const/16 v6, 0x708

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_0

    :cond_9
    iget-object v0, v5, Lhys;->g:Lhzk;

    invoke-virtual {v0}, Lhzk;->a()V

    goto/16 :goto_1

    :cond_a
    move v0, v4

    goto/16 :goto_2

    :cond_b
    :try_start_1
    iget-object v6, v5, Lhyi;->c:Lbpe;

    invoke-interface {v6}, Lbpe;->b()J

    move-result-wide v10

    sub-long v8, v10, v8

    const-wide/32 v10, 0x15f90

    cmp-long v6, v8, v10

    if-gtz v6, :cond_3

    move v3, v4

    goto :goto_4

    :cond_c
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_6

    const-string v0, "ActivityDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring requestActivity: intervalSec="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_d
    iget-object v0, v5, Lhys;->f:Lhyi;

    invoke-virtual {v0}, Lhyi;->a()V

    goto :goto_6

    :cond_e
    move-object v0, v2

    goto/16 :goto_3
.end method

.method protected abstract d()I
.end method

.method protected abstract e()I
.end method

.method protected abstract f()I
.end method

.method protected g()Ljava/util/Collection;
    .locals 3

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {p0}, Lhyx;->i()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lhys;->a(D)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method protected h()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected abstract i()D
.end method

.method protected j()V
    .locals 0

    return-void
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhyx;->c:Lhys;

    invoke-virtual {v0}, Lhys;->d()V

    return-void
.end method
