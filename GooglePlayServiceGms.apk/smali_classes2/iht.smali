.class public final Liht;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lijb;

.field private final c:Lihy;

.field private final d:Lihn;

.field private final e:Lbpe;

.field private final f:Lika;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lijb;Lihy;Lihn;)V
    .locals 7

    new-instance v5, Lbpg;

    invoke-direct {v5}, Lbpg;-><init>()V

    new-instance v6, Likb;

    invoke-direct {v6}, Likb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Liht;-><init>(Landroid/content/Context;Lijb;Lihy;Lihn;Lbpe;Lika;)V

    invoke-static {p1}, Likh;->a(Landroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lijb;Lihy;Lihn;Lbpe;Lika;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Liht;->a:Landroid/content/Context;

    iput-object p2, p0, Liht;->b:Lijb;

    iput-object p3, p0, Liht;->c:Lihy;

    iput-object p4, p0, Liht;->d:Lihn;

    iput-object p5, p0, Liht;->e:Lbpe;

    iput-object p6, p0, Liht;->f:Lika;

    return-void
.end method

.method private a()Lihu;
    .locals 1

    iget-object v0, p0, Liht;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lbbv;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lihu;->c:Lihu;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lihu;->b:Lihu;

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/reporting/service/ReportingConfig;JLcom/google/android/ulr/ApiRate;Landroid/location/Location;)V
    .locals 7

    invoke-static {p4}, Lihm;->a(Lcom/google/android/ulr/ApiRate;)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Likf;->a(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v0

    :try_start_0
    iget-object v4, p0, Liht;->d:Lihn;

    invoke-virtual {v4, v3, v1, v0}, Lihn;->saveEntity(Landroid/accounts/Account;Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    if-eqz p5, :cond_2

    iget-object v0, p0, Liht;->b:Lijb;

    invoke-virtual {v0, p5}, Lijb;->b(Landroid/location/Location;)V

    :cond_2
    iget-object v0, p0, Liht;->b:Lijb;

    invoke-virtual {v0, p2, p3}, Lijb;->c(J)V

    iget-object v0, p0, Liht;->f:Lika;

    iget-object v1, p0, Liht;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lika;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Lijb;Lcom/google/android/location/reporting/service/ReportingConfig;Landroid/location/Location;)Z
    .locals 18

    invoke-virtual/range {p1 .. p1}, Lijb;->f()Landroid/location/Location;

    move-result-object v2

    invoke-static {}, Lhkr;->a()Z

    move-result v4

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lihz;->a(Landroid/location/Location;Landroid/location/Location;)F

    move-result v3

    const/4 v5, 0x0

    cmpg-float v5, v3, v5

    if-gtz v5, :cond_2

    new-instance v3, Liia;

    if-nez v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Liia;-><init>(ZI)V

    move-object v10, v3

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Liht;->b:Lijb;

    invoke-virtual {v2}, Lijb;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gtz v2, :cond_7

    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GCoreUlr"

    const-string v3, "Not uploading first location since start (in case we\'re in restart loop)"

    invoke-static {v2, v3}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    :goto_2
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v5

    sget-object v2, Lijs;->o:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v6, v5, v3

    if-lez v6, :cond_3

    cmpl-float v6, v5, v2

    if-lez v6, :cond_3

    new-instance v2, Liia;

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Liia;-><init>(ZI)V

    move-object v10, v2

    goto :goto_1

    :cond_3
    cmpl-float v3, v3, v2

    if-lez v3, :cond_5

    new-instance v3, Liia;

    if-nez v4, :cond_4

    const/4 v2, 0x1

    :goto_3
    const/4 v4, 0x3

    invoke-direct {v3, v2, v4}, Liia;-><init>(ZI)V

    move-object v10, v3

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    new-instance v3, Liia;

    cmpl-float v2, v5, v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_4
    const/4 v4, 0x4

    invoke-direct {v3, v2, v4}, Liia;-><init>(ZI)V

    move-object v10, v3

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    :cond_7
    iget-boolean v13, v10, Liia;->a:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Liht;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->b()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Liht;->b:Lijb;

    invoke-virtual {v2}, Lijb;->g()J

    move-result-wide v2

    sub-long v4, v14, v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-gtz v2, :cond_a

    sget-object v3, Lihu;->a:Lihu;

    move-object v12, v3

    :goto_5
    if-eqz v13, :cond_e

    invoke-interface {v12}, Lihr;->a()J

    move-result-wide v2

    move-wide v8, v2

    :goto_6
    cmp-long v2, v4, v8

    if-ltz v2, :cond_f

    const/4 v11, 0x1

    :goto_7
    if-eqz v11, :cond_11

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Sending an intent to LocationReportingService, hasMoved: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", elapsed millis: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    new-instance v2, Lcom/google/android/ulr/ApiRate;

    invoke-interface {v12}, Lihr;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Liht;->e:Lbpe;

    invoke-interface {v7}, Lbpe;->a()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    if-eqz v13, :cond_10

    const-string v9, "default"

    :goto_8
    invoke-direct/range {v2 .. v9}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Liht;->b:Lijb;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v3, v0, v1, v10, v12}, Likf;->a(Lijb;Lcom/google/android/location/reporting/service/ReportingConfig;Landroid/location/Location;Liia;Lihr;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-wide v5, v14

    move-object v7, v2

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Liht;->a(Lcom/google/android/location/reporting/service/ReportingConfig;JLcom/google/android/ulr/ApiRate;Landroid/location/Location;)V

    :cond_9
    :goto_9
    move v2, v11

    goto/16 :goto_2

    :cond_a
    if-eqz v13, :cond_c

    invoke-direct/range {p0 .. p0}, Liht;->a()Lihu;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Liht;->c:Lihy;

    invoke-virtual {v2}, Lihy;->c()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a()J

    move-result-wide v7

    invoke-interface {v3}, Lihr;->a()J

    move-result-wide v11

    cmp-long v7, v7, v11

    if-gez v7, :cond_13

    :goto_b
    move-object v3, v2

    goto :goto_a

    :cond_b
    move-object v12, v3

    goto/16 :goto_5

    :cond_c
    invoke-direct/range {p0 .. p0}, Liht;->a()Lihu;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Liht;->c:Lihy;

    invoke-virtual {v2}, Lihy;->c()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b()J

    move-result-wide v7

    invoke-interface {v3}, Lihr;->b()J

    move-result-wide v11

    cmp-long v7, v7, v11

    if-gez v7, :cond_12

    :goto_d
    move-object v3, v2

    goto :goto_c

    :cond_d
    move-object v12, v3

    goto/16 :goto_5

    :cond_e
    invoke-interface {v12}, Lihr;->b()J

    move-result-wide v2

    move-wide v8, v2

    goto/16 :goto_6

    :cond_f
    const/4 v11, 0x0

    goto/16 :goto_7

    :cond_10
    const-string v9, "stationary"

    goto/16 :goto_8

    :cond_11
    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Not calling LocationReportingService, hasMoved: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", elapsed millis: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_12
    move-object v2, v3

    goto :goto_d

    :cond_13
    move-object v2, v3

    goto :goto_b
.end method
