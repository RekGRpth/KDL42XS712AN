.class final Lhqh;
.super Lhrx;
.source "SourceFile"


# instance fields
.field final a:Landroid/telephony/TelephonyManager;

.field volatile b:I

.field private final g:Landroid/telephony/PhoneStateListener;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lhqm;Lhqq;Limb;Lilx;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lhrx;-><init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V

    new-instance v0, Lhqi;

    invoke-direct {v0, p0}, Lhqi;-><init>(Lhqh;)V

    iput-object v0, p0, Lhqh;->g:Landroid/telephony/PhoneStateListener;

    const/16 v0, -0x270f

    iput v0, p0, Lhqh;->b:I

    new-instance v0, Lhqj;

    invoke-direct {v0, p0}, Lhqj;-><init>(Lhqh;)V

    iput-object v0, p0, Lhqh;->h:Ljava/lang/Runnable;

    invoke-static {p2}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method static synthetic a(Lhqh;Landroid/telephony/CellLocation;)V
    .locals 8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v0, p0, Lhrx;->d:Lhqm;

    iget-object v1, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    iget-object v2, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lhqh;->b:I

    iget-object v3, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v5

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lhqm;->a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    sget-object v0, Lhrz;->b:Lhrz;

    invoke-virtual {p0, v0, v6, v7}, Lhqh;->b(Lhrz;J)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    iget-object v0, p0, Lhrx;->d:Lhqm;

    iget-object v1, p0, Lhqh;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lhqm;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lhqh;->g:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x111

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lhqh;->e:Lhqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqh;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->c()V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 3

    iget-object v0, p0, Lhqh;->a:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lhqh;->g:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lhrx;->d:Lhqm;

    iget-object v1, p0, Lhqh;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhqm;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lhqh;->e:Lhqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqh;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->b()V

    :cond_0
    return-void
.end method
