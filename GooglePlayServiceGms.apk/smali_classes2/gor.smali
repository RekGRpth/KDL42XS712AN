.class public final Lgor;
.super Lfpu;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lfpv;


# instance fields
.field private A:Lfrk;

.field private B:Lgot;

.field private final q:Landroid/content/Context;

.field private final r:Lak;

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private u:Landroid/widget/Filter;

.field private v:Ljava/lang/String;

.field private w:Lfro;

.field private x:Lgou;

.field private y:Lfrp;

.field private z:Lgov;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfrb;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lfpu;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lgou;

    invoke-direct {v0, p0, v5}, Lgou;-><init>(Lgor;B)V

    iput-object v0, p0, Lgor;->x:Lgou;

    new-instance v0, Lgov;

    invoke-direct {v0, p0, v5}, Lgov;-><init>(Lgor;B)V

    iput-object v0, p0, Lgor;->z:Lgov;

    new-instance v0, Lgot;

    invoke-direct {v0, p0, v5}, Lgot;-><init>(Lgor;B)V

    iput-object v0, p0, Lgor;->B:Lgot;

    iput-object p0, p0, Lfpu;->e:Lfpv;

    iput-object p1, p0, Lgor;->q:Landroid/content/Context;

    iput-object p2, p0, Lgor;->r:Lak;

    iput-object p3, p0, Lgor;->s:Ljava/lang/String;

    iput-object p4, p0, Lgor;->t:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgor;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lgor;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lgor;->q:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgor;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgor;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqe;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqe;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqe;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lgor;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfqe;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final O_()V
    .locals 3

    iget-object v0, p0, Lgor;->y:Lfrp;

    iget-object v1, p0, Lgor;->v:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lfrp;->a(Ljava/lang/String;I)Lfrp;

    return-void
.end method

.method protected final a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 2

    invoke-super/range {p0 .. p12}, Lfpu;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d(Z)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lgor;->w:Lfro;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgor;->y:Lfrp;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lgor;->v:Ljava/lang/String;

    iget-object v0, p0, Lgor;->A:Lfrk;

    iget-object v1, p0, Lgor;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfrk;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lgor;->w:Lfro;

    iget-object v1, p0, Lgor;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfro;->a(Ljava/lang/String;)Lfrn;

    iget-object v0, p0, Lgor;->y:Lfrp;

    iget-object v1, p0, Lgor;->v:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lfrp;->a(Ljava/lang/String;I)Lfrp;

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 2

    iget-object v0, p0, Lgor;->u:Landroid/widget/Filter;

    if-nez v0, :cond_0

    new-instance v0, Lgos;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgos;-><init>(Lgor;B)V

    iput-object v0, p0, Lgor;->u:Landroid/widget/Filter;

    :cond_0
    iget-object v0, p0, Lgor;->u:Landroid/widget/Filter;

    return-object v0
.end method

.method public final j()V
    .locals 3

    invoke-super {p0}, Lfpu;->j()V

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x1

    iget-object v2, p0, Lgor;->x:Lgou;

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfro;

    iput-object v0, p0, Lgor;->w:Lfro;

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x2

    iget-object v2, p0, Lgor;->z:Lgov;

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrp;

    iput-object v0, p0, Lgor;->y:Lfrp;

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x3

    iget-object v2, p0, Lgor;->B:Lgot;

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    move-result-object v0

    check-cast v0, Lfrk;

    iput-object v0, p0, Lgor;->A:Lfrk;

    return-void
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lak;->a(I)V

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lak;->a(I)V

    iget-object v0, p0, Lgor;->r:Lak;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lak;->a(I)V

    invoke-super {p0}, Lfpu;->k()V

    return-void
.end method
