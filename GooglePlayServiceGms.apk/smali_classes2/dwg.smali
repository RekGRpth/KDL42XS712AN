.class final Ldwg;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldvv;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ldvv;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Ldwg;->a:Ldvv;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p2}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Ldwg;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Ldwg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Ldwg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Ldwg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwj;

    iget v0, v0, Ldwj;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Ldwg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwj;

    iget v0, v0, Ldwj;->c:I

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-virtual {p0, p1}, Ldwg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwj;

    if-nez p2, :cond_0

    iget-object v1, v0, Ldwj;->f:Ldvv;

    invoke-virtual {v1}, Ldvv;->T_()Lo;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, v0, Ldwj;->d:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {v0, p2}, Ldwj;->a(Landroid/view/View;)V

    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Ldwg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwj;

    iget-boolean v0, v0, Ldwj;->e:Z

    return v0
.end method
