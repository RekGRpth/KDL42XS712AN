.class final Ligs;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Ligr;


# direct methods
.method constructor <init>(Landroid/os/Looper;Ligr;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Ligs;->a:Ligr;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    const/4 v1, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Ligs;->a:Ligr;

    iget-object v0, v0, Ligr;->a:Lifw;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lifw;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ligj;

    iget-object v2, v0, Ligj;->a:Ligi;

    iget-object v0, v0, Ligj;->b:Landroid/location/Location;

    new-instance v3, Ligx;

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v0, v2, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v0, v0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iget-object v5, v2, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v5, v5, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v5}, Lcom/google/android/gms/location/places/PlaceRequest;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Ligx;-><init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)V

    iget-object v4, v2, Ligi;->e:Ligf;

    iget-object v0, v2, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v5, v0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v2, v2, Ligi;->i:Ligm;

    iget-object v0, v4, Ligf;->e:Lilo;

    invoke-virtual {v0}, Lilo;->a()J

    move-result-wide v6

    iget-object v0, v4, Ligf;->b:Lifu;

    invoke-virtual {v0, v3}, Lifu;->a(Ligx;)Lify;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v8, v4, Ligf;->a:Ligc;

    iget-object v9, v3, Ligx;->b:Ljava/lang/String;

    invoke-virtual {v0, v8, v9, v6, v7}, Lify;->a(Ligc;Ljava/lang/String;J)Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, Lbgr;->d()Lbgt;

    move-result-object v3

    move v0, v1

    :goto_1
    invoke-virtual {v6}, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c()[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->a(I)F

    move-result v4

    invoke-static {v1, v4}, Lcom/google/android/gms/location/places/PlaceLikelihood;->a(Lcom/google/android/gms/location/places/internal/PlaceImpl;F)Lcom/google/android/gms/location/places/PlaceLikelihood;

    move-result-object v1

    invoke-static {v3, v1}, Lbgr;->a(Lbgt;Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v3, v0}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v2, v0}, Lerq;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v0, "PlaceEstimator"

    const-string v1, "places query cache hit"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    const-string v0, "PlaceEstimator"

    const-string v1, "places query cache hit but place cache miss"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, v4, Ligf;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligg;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Ligg;->a(Lerq;)V

    const-string v0, "PlaceEstimator"

    const-string v1, "places query cache miss, attached to pending query"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ligg;

    invoke-direct {v0, v4, v3, v2}, Ligg;-><init>(Ligf;Ligx;Lerq;)V

    iget-object v1, v4, Ligf;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "PlaceEstimator"

    const-string v2, "places query cache miss, querying GLS"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v4, Ligf;->d:Lifw;

    iget-object v2, v3, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, v3, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    invoke-virtual {v1, v2, v3, v5, v0}, Lifw;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
