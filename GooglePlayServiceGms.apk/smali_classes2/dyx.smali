.class public final Ldyx;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldvf;
.implements Ledv;
.implements Lox;


# instance fields
.field ad:Z

.field private ae:Ldzc;

.field private af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private ag:Ldxi;

.field private ah:Ldwp;

.field private ai:Ldyp;

.field private aj:Ldwp;

.field private ak:Ldyp;

.field private al:Ldwp;

.field private am:Ldyp;

.field private an:Ldxi;

.field private ao:Ldyp;

.field private final ap:Ljava/lang/Runnable;

.field private aq:Lbgo;

.field private ar:Lbgx;

.field private as:Lbhf;

.field private at:Z

.field private au:Lfaj;

.field private av:Ledu;

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:Z

.field private az:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldxe;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyx;->ad:Z

    new-instance v0, Ldyy;

    invoke-direct {v0, p0}, Ldyy;-><init>(Ldyx;)V

    iput-object v0, p0, Ldyx;->ap:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Ldyx;Lbgo;)Lbgo;
    .locals 0

    iput-object p1, p0, Ldyx;->aq:Lbgo;

    return-object p1
.end method

.method static synthetic a(Ldyx;Lbgx;)Lbgx;
    .locals 0

    iput-object p1, p0, Ldyx;->ar:Lbgx;

    return-object p1
.end method

.method static synthetic a(Ldyx;Lbhf;)Lbhf;
    .locals 0

    iput-object p1, p0, Ldyx;->as:Lbhf;

    return-object p1
.end method

.method static synthetic a(Ldyx;)Ldyp;
    .locals 1

    iget-object v0, p0, Ldyx;->am:Ldyp;

    return-object v0
.end method

.method static synthetic a(Ldyx;Ldyp;)Ldyp;
    .locals 0

    iput-object p1, p0, Ldyx;->ao:Ldyp;

    return-object p1
.end method

.method private a(Lbdu;Z)V
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Ldyx;->ae:Ldzc;

    invoke-interface {v1}, Ldzc;->A()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move-object v2, v0

    :goto_0
    sget-object v1, Lcte;->m:Lctw;

    invoke-interface {v1, p1, p2}, Lctw;->a(Lbdu;Z)Lbeh;

    move-result-object v3

    sget-object v1, Lcte;->m:Lctw;

    iget-object v4, p0, Ldyx;->Y:Ldvn;

    invoke-static {v4}, Ledx;->a(Landroid/content/Context;)I

    move-result v4

    invoke-interface {v1, p1, v4, p2}, Lctw;->a(Lbdu;IZ)Lbeh;

    move-result-object v4

    iget-object v1, p0, Ldyx;->ae:Ldzc;

    invoke-interface {v1}, Ldzc;->z()Ljava/util/ArrayList;

    move-result-object v1

    if-nez p2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, p1, v1}, Lctw;->a(Lbdu;Ljava/util/ArrayList;)Lbeh;

    move-result-object v5

    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v6, Lbdt;

    invoke-direct {v6, v0}, Lbdt;-><init>(Ljava/util/ArrayList;)V

    new-instance v0, Ldyz;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldyz;-><init>(Ldyx;Lbeh;Lbeh;Lbeh;Lbeh;)V

    invoke-virtual {v6, v0}, Lbdt;->a(Lbel;)V

    return-void

    :pswitch_0
    sget-object v1, Lcte;->m:Lctw;

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Q()Lcom/google/android/gms/games/GameEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Lctw;->a(Lbdu;Ljava/lang/String;Z)Lbeh;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object v5, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Ldyx;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;
    .locals 1

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    return-object v0
.end method

.method static synthetic c(Ldyx;)Z
    .locals 1

    iget-boolean v0, p0, Ldyx;->az:Z

    return v0
.end method

.method static synthetic d(Ldyx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyx;->az:Z

    return v0
.end method

.method static synthetic e(Ldyx;)Ldvn;
    .locals 1

    iget-object v0, p0, Ldyx;->Y:Ldvn;

    return-object v0
.end method

.method static synthetic f(Ldyx;)Ldvn;
    .locals 1

    iget-object v0, p0, Ldyx;->Y:Ldvn;

    return-object v0
.end method

.method static synthetic g(Ldyx;)Ldvn;
    .locals 1

    iget-object v0, p0, Ldyx;->Y:Ldvn;

    return-object v0
.end method

.method static synthetic h(Ldyx;)Ldyp;
    .locals 1

    iget-object v0, p0, Ldyx;->ai:Ldyp;

    return-object v0
.end method

.method static synthetic i(Ldyx;)Lbgo;
    .locals 1

    iget-object v0, p0, Ldyx;->aq:Lbgo;

    return-object v0
.end method

.method static synthetic j(Ldyx;)Z
    .locals 1

    iget-boolean v0, p0, Ldyx;->at:Z

    return v0
.end method

.method static synthetic k(Ldyx;)Lbgx;
    .locals 1

    iget-object v0, p0, Ldyx;->ar:Lbgx;

    return-object v0
.end method

.method static synthetic l(Ldyx;)Ldyp;
    .locals 1

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    return-object v0
.end method

.method static synthetic m(Ldyx;)Lbhf;
    .locals 1

    iget-object v0, p0, Ldyx;->as:Lbhf;

    return-object v0
.end method

.method static synthetic n(Ldyx;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyx;->ay:Z

    return v0
.end method

.method static synthetic o(Ldyx;)V
    .locals 2

    iget-boolean v0, p0, Ldyx;->ay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyx;->au:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyx;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic p(Ldyx;)Leds;
    .locals 1

    iget-object v0, p0, Ldyx;->Z:Leds;

    return-object v0
.end method


# virtual methods
.method public final R()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b()V

    invoke-virtual {p0, v1, v1}, Ldyx;->b(ZZ)V

    return-void
.end method

.method public final V()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Ldyx;->ae:Ldzc;

    invoke-interface {v1}, Ldzc;->w()I

    move-result v1

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    return-void
.end method

.method final W()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ldyx;->Y()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldyx;->an:Ldxi;

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    :goto_0
    iget-object v0, p0, Ldyx;->as:Lbhf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyx;->as:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, p0, Ldyx;->ar:Lbgx;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldyx;->ar:Lbgx;

    invoke-virtual {v3}, Lbgx;->a()I

    move-result v3

    if-lez v3, :cond_2

    move v3, v1

    :goto_2
    iget-object v4, p0, Ldyx;->aq:Lbgo;

    if-eqz v4, :cond_3

    iget-object v4, p0, Ldyx;->aq:Lbgo;

    invoke-virtual {v4}, Lbgo;->a()I

    move-result v4

    if-lez v4, :cond_3

    :goto_3
    iget-object v2, p0, Ldyx;->ah:Ldwp;

    invoke-virtual {v2, v1}, Ldwp;->b(Z)V

    iget-object v1, p0, Ldyx;->aj:Ldwp;

    invoke-virtual {v1, v3}, Ldwp;->b(Z)V

    iget-object v1, p0, Ldyx;->al:Ldwp;

    invoke-virtual {v1, v0}, Ldwp;->b(Z)V

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->J()V

    return-void

    :cond_0
    iget-object v0, p0, Ldyx;->an:Ldxi;

    invoke-virtual {v0, v2}, Ldxi;->b(Z)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public final X()V
    .locals 2

    iget-object v1, p0, Ldyx;->ag:Ldxi;

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ldyx;->ae:Ldzc;

    invoke-interface {v0}, Ldzc;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ldxi;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final Y()I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ldyx;->as:Lbhf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldyx;->as:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Ldyx;->ad:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Ldyx;->ar:Lbgx;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldyx;->ar:Lbgx;

    invoke-virtual {v2}, Lbgx;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    :goto_1
    iget-boolean v3, p0, Ldyx;->ad:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Ldyx;->aq:Lbgo;

    if-eqz v3, :cond_0

    iget-object v1, p0, Ldyx;->aq:Lbgo;

    invoke-virtual {v1}, Lbgo;->a()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    :cond_0
    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final Z()I
    .locals 1

    iget-object v0, p0, Ldyx;->as:Lbhf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyx;->as:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-ne p1, v4, :cond_1

    invoke-static {p3}, Lbex;->a(Landroid/content/Intent;)Lbey;

    move-result-object v0

    invoke-interface {v0}, Lbey;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lbey;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Ldyx;->Y:Ldvn;

    invoke-virtual {v2}, Ldvn;->j()Lbdu;

    move-result-object v2

    iget-object v3, p0, Ldyx;->Y:Ldvn;

    invoke-static {v2, v3, v1}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onActivityResult: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    sget-object v1, Lcte;->f:Lctl;

    invoke-interface {v1, v2}, Lctl;->a(Lbdu;)Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldyx;->Y:Ldvn;

    iget-object v3, p0, Ldyx;->ax:Ljava/lang/String;

    invoke-static {v2, v1, v3, v4, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method public final a(Lbdu;)V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, p1}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersListFrag"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldyx;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldyx;->ax:Ljava/lang/String;

    iget-object v1, p0, Ldyx;->ax:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onGoogleApiClientConnected: no current account! Bailing out..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldyx;->Y:Ldvn;

    invoke-virtual {v0}, Ldvn;->finish()V

    goto :goto_0

    :cond_1
    iput-boolean v2, p0, Ldyx;->ay:Z

    iget-object v1, p0, Ldyx;->au:Lfaj;

    iget-object v1, v1, Lfaj;->a:Lfch;

    invoke-virtual {v1}, Lfch;->d_()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ldyx;->au:Lfaj;

    invoke-virtual {v1}, Lfaj;->a()V

    :cond_2
    iget-object v1, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v1, v0}, Ldyp;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v1, v0}, Ldyp;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v1, v0}, Ldyp;->a(Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Ldyx;->a(Lbdu;Z)V

    goto :goto_0
.end method

.method public final a(Lctx;)V
    .locals 5

    invoke-interface {p1}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctx;->a()Lcts;

    move-result-object v1

    :try_start_0
    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcts;->b()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Ldyx;->Y:Ldvn;

    invoke-virtual {v2, v0}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcts;->b()V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-static {v0}, Leee;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v2}, Ldyp;->h()V

    :cond_3
    new-instance v2, Lbhf;

    const-string v3, "profile_name"

    invoke-direct {v2, v1, v3}, Lbhf;-><init>(Lbgo;Ljava/lang/String;)V

    iput-object v2, p0, Ldyx;->as:Lbhf;

    iget-object v2, p0, Ldyx;->am:Ldyp;

    iget-object v3, p0, Ldyx;->as:Lbhf;

    invoke-virtual {v2, v3}, Ldyp;->a(Lbgo;)V

    invoke-static {v0}, Leee;->a(I)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v0, p0, Ldyx;->Z:Leds;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Leds;->a(I)V

    invoke-virtual {p0}, Ldyx;->W()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcts;->b()V

    throw v0

    :cond_4
    :try_start_3
    iget-object v2, p0, Ldyx;->Z:Leds;

    const v3, 0x7f0b02d8    # com.google.android.gms.R.string.games_player_search_list_generic_error

    const v4, 0x7f0b02d5    # com.google.android.gms.R.string.games_search_players_no_results

    invoke-virtual {v2, v0, v3, v4}, Leds;->a(III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Ldyx;->as:Lbhf;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldyx;->as:Lbhf;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    sget-object v2, Lbhf;->e:Lbhj;

    invoke-virtual {v0, v1, v2, p1}, Lbhf;->a(Landroid/content/Context;Lbhj;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Ldyx;->ad:Z

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    invoke-virtual {v0, v4}, Ldyp;->b(Z)V

    :cond_1
    iget-object v0, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v0, v4}, Ldyp;->a(Z)V

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v0, v4}, Ldyp;->a(Z)V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0, p0}, Ldyp;->a(Ldvf;)V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0}, Ldyp;->e()Lbgo;

    move-result-object v0

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "savedStatePageToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "next_page_token"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Ldyx;->W()V

    :goto_1
    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0}, Ldyp;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Ldyx;->ad:Z

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    invoke-virtual {v0, v3}, Ldyp;->b(Z)V

    :cond_4
    iget-object v0, p0, Ldyx;->ah:Ldwp;

    invoke-virtual {v0, v3}, Ldwp;->b(Z)V

    iget-object v0, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v0, v3}, Ldyp;->a(Z)V

    iget-object v0, p0, Ldyx;->aj:Ldwp;

    invoke-virtual {v0, v3}, Ldwp;->b(Z)V

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v0, v3}, Ldyp;->a(Z)V

    iget-object v0, p0, Ldyx;->al:Ldwp;

    invoke-virtual {v0, v3}, Ldwp;->b(Z)V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldyp;->a(Ldvf;)V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0}, Ldyp;->e()Lbgo;

    move-result-object v0

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "next_page_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "savedStatePageToken"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Ldyx;->ai:Ldyp;

    iget-object v1, p0, Ldyx;->aw:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ldyp;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->M()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-gez v1, :cond_1

    :goto_1
    neg-int v2, v1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->N()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->N()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public final a_(I)V
    .locals 3

    invoke-virtual {p0}, Ldyx;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->m:Lctw;

    iget-object v2, p0, Ldyx;->Y:Ldvn;

    invoke-static {v2}, Ledx;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lctw;->a(Lbdu;I)Lbeh;

    move-result-object v0

    new-instance v1, Ldza;

    invoke-direct {v1, p0}, Ldza;-><init>(Ldyx;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    invoke-virtual {p0}, Ldyx;->L()Z

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v0, p1}, Ldyp;->d(Z)V

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v0, p1}, Ldyp;->d(Z)V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0, p1}, Ldyp;->d(Z)V

    return-void
.end method

.method public final b(ZZ)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ldyx;->J()Lbdu;

    move-result-object v0

    iput-boolean v3, p0, Ldyx;->az:Z

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_0

    iget-object v1, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v1}, Ldyp;->a()V

    iget-object v1, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v1}, Ldyp;->a()V

    iget-object v1, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v1}, Ldyp;->a()V

    iget-object v1, p0, Ldyx;->ah:Ldwp;

    invoke-virtual {v1, v2}, Ldwp;->b(Z)V

    iget-object v1, p0, Ldyx;->aj:Ldwp;

    invoke-virtual {v1, v2}, Ldwp;->b(Z)V

    iget-object v1, p0, Ldyx;->al:Ldwp;

    invoke-virtual {v1, v2}, Ldwp;->b(Z)V

    iget-object v1, p0, Ldyx;->an:Ldxi;

    invoke-virtual {v1, v2}, Ldxi;->b(Z)V

    iget-object v1, p0, Ldyx;->Z:Leds;

    invoke-virtual {v1, v3}, Leds;->a(I)V

    :cond_0
    invoke-direct {p0, v0, v3}, Ldyx;->a(Lbdu;Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v0}, Ldyp;->notifyDataSetChanged()V

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v0}, Ldyp;->notifyDataSetChanged()V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0}, Ldyp;->notifyDataSetChanged()V

    return-void

    :cond_2
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "refresh: googleApiClient not connected, ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(ZZ)V
    .locals 1

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyx;->ao:Ldyp;

    invoke-virtual {v0, p1, p2}, Ldyp;->b(ZZ)V

    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldzc;

    iput-object v0, p0, Ldyx;->ae:Ldzc;

    iget-object v0, p0, Ldyx;->ae:Ldzc;

    invoke-interface {v0}, Ldzc;->v()Z

    move-result v0

    iput-boolean v0, p0, Ldyx;->at:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->E:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iput-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v0, Ldxi;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v2, 0x7f02017c    # com.google.android.gms.R.drawable.illo_info

    const v3, 0x7f0b022e    # com.google.android.gms.R.string.games_select_players_auto_match_warning_card_text

    const v4, 0x7f0b022f    # com.google.android.gms.R.string.games_select_players_auto_match_warning_card_button

    const-string v6, "autoMatchTile"

    const-string v7, "learnMore"

    const/4 v8, 0x3

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Ldyx;->ag:Ldxi;

    iget-object v0, p0, Ldyx;->ag:Ldxi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    new-instance v0, Ldwp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldyx;->ah:Ldwp;

    iget-object v0, p0, Ldyx;->ah:Ldwp;

    const v1, 0x7f0b022b    # com.google.android.gms.R.string.games_select_players_recent_header

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    new-instance v0, Ldyp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iget-object v3, p0, Ldyx;->ae:Ldzc;

    invoke-direct {v0, v1, v2, v3, p0}, Ldyp;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Ldzc;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Ldyx;->ai:Ldyp;

    new-instance v0, Ldwp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldyx;->aj:Ldwp;

    iget-object v0, p0, Ldyx;->aj:Ldwp;

    const v1, 0x7f0b022c    # com.google.android.gms.R.string.games_select_players_connected_header

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    new-instance v0, Ldyp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iget-object v3, p0, Ldyx;->ae:Ldzc;

    invoke-direct {v0, v1, v2, v3, p0}, Ldyp;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Ldzc;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Ldyx;->ak:Ldyp;

    new-instance v0, Ldwp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldyx;->al:Ldwp;

    iget-object v0, p0, Ldyx;->al:Ldwp;

    const v1, 0x7f0b022d    # com.google.android.gms.R.string.games_select_players_my_circles_header

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    new-instance v0, Ldyp;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iget-object v3, p0, Ldyx;->ae:Ldzc;

    invoke-direct {v0, v1, v2, v3, p0}, Ldyp;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Ldzc;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Ldyx;->am:Ldyp;

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0, p0}, Ldyp;->a(Ldvf;)V

    new-instance v0, Ldxi;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v2, 0x7f02017f    # com.google.android.gms.R.drawable.illo_tutorial_you_may_know

    const v3, 0x7f0b0229    # com.google.android.gms.R.string.games_select_players_no_invitable_players

    const v4, 0x7f0b024b    # com.google.android.gms.R.string.games_find_people

    const-string v6, "findPeople"

    const-string v7, "findPeople"

    const/4 v8, 0x2

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Ldyx;->an:Ldxi;

    iget-object v0, p0, Ldyx;->an:Ldxi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    new-instance v0, Ldwu;

    const/16 v1, 0x8

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Ldyx;->ag:Ldxi;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ldyx;->ah:Ldwp;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Ldyx;->ai:Ldyp;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Ldyx;->aj:Ldwp;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Ldyx;->ak:Ldyp;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Ldyx;->al:Ldwp;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Ldyx;->am:Ldyp;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Ldyx;->an:Ldxi;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Ldyx;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {p0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    new-instance v2, Ldzb;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Ldzb;-><init>(Ldyx;B)V

    new-instance v0, Lfaj;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    const/16 v4, 0x76

    const/4 v5, 0x0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Ldyx;->au:Lfaj;

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Ldyx;->ai:Ldyp;

    invoke-virtual {v0}, Ldyp;->a()V

    iget-object v0, p0, Ldyx;->ak:Ldyp;

    invoke-virtual {v0}, Ldyp;->a()V

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0}, Ldyp;->a()V

    invoke-virtual {p0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Ldyx;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-super {p0}, Ldxe;->g_()V

    iget-object v0, p0, Ldyx;->au:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/games/Player;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0181    # com.google.android.gms.R.id.in_circles_indicator

    if-ne v2, v3, :cond_1

    iget-object v1, p0, Ldyx;->av:Ledu;

    if-eqz v1, :cond_0

    const-string v1, "SelectPlayersListFrag"

    const-string v2, "onManageCircles(): canceling the helper that was already running..."

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Ldyx;->av:Ledu;

    invoke-virtual {v1}, Ledu;->b()V

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldyx;->aw:Ljava/lang/String;

    iget-object v1, p0, Ldyx;->Y:Ldvn;

    iget-object v3, p0, Ldyx;->au:Lfaj;

    iget-object v4, p0, Ldyx;->ax:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Ledu;->a(Lcom/google/android/gms/games/Player;Ledv;Landroid/support/v4/app/Fragment;Lfaj;Ljava/lang/String;I)Ledu;

    move-result-object v0

    iput-object v0, p0, Ldyx;->av:Ledu;

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/Player;)V

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldyx;->am:Ldyp;

    invoke-virtual {v0, v1}, Ldyp;->b(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Ldyx;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Ldyx;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_3
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    const-string v2, "auto_pick_item_add_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->M()V

    goto :goto_0

    :cond_4
    const-string v2, "auto_pick_item_remove_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->N()V

    goto :goto_0

    :cond_5
    const-string v2, "learnMore"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v0, Ldvi;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Ldyx;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    const-string v2, "autoMatchTile"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iget-object v1, v1, Ldzg;->b:Ldvn;

    invoke-virtual {v1}, Ldvn;->h()V

    :cond_7
    :goto_1
    const-string v1, "SelectPlayersListFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "findPeople"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d()Z

    goto :goto_1

    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0a00d9    # com.google.android.gms.R.id.overflow_menu

    if-ne v1, v2, :cond_7

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v1, 0x7f120002    # com.google.android.gms.R.menu.games_client_autopick_menu

    invoke-virtual {v0, v1}, Lov;->a(I)V

    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->P()I

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v1

    if-lez v1, :cond_b

    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v1

    if-lez v1, :cond_a

    invoke-virtual {p0}, Ldyx;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0010    # com.google.android.gms.R.plurals.games_select_players_del_autopick_players

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v7, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lov;->a:Llm;

    const/4 v3, -0x1

    invoke-interface {v2, v6, v3, v6, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_a
    iget-object v1, p0, Ldyx;->af:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v1

    if-lt v1, v5, :cond_b

    invoke-virtual {p0}, Ldyx;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0222    # com.google.android.gms.R.string.games_select_players_delall_autopick_players

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lov;->a:Llm;

    invoke-interface {v2, v6, v6, v7, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_b
    iput-object p0, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto/16 :goto_0
.end method
