.class public final Lcdk;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s %s IN (SELECT %s FROM %s WHERE %s)"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v0

    invoke-virtual {v0}, Lcef;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    if-eqz p0, :cond_0

    const-string v0, "NOT"

    :goto_0
    aput-object v0, v3, v5

    sget-object v0, Lcdm;->b:Lcdm;

    invoke-virtual {v0}, Lcdm;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v0

    invoke-virtual {v0}, Lcdl;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcdm;->a:Lcdm;

    invoke-virtual {v4}, Lcdm;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " IN ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v5

    invoke-virtual {v5}, Lcef;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v5

    invoke-virtual {v5}, Lcef;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v5

    invoke-virtual {v5}, Lcef;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lceg;->j:Lceg;

    invoke-virtual {v5}, Lceg;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s %s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v0

    invoke-virtual {v0}, Lcef;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    if-eqz p0, :cond_2

    const-string v0, "NOT"

    :goto_2
    aput-object v0, v3, v5

    sget-object v0, Lcdm;->b:Lcdm;

    invoke-virtual {v0}, Lcdm;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v0

    invoke-virtual {v0}, Lcdl;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    sget-object v0, Lcdm;->a:Lcdm;

    invoke-virtual {v0}, Lcdm;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method
