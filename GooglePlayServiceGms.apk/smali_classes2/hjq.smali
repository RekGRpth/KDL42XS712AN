.class final Lhjq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/Calendar;

.field final synthetic b:Livi;

.field final synthetic c:Lhjp;


# direct methods
.method constructor <init>(Lhjp;Ljava/util/Calendar;Livi;)V
    .locals 0

    iput-object p1, p0, Lhjq;->c:Lhjp;

    iput-object p2, p0, Lhjq;->a:Ljava/util/Calendar;

    iput-object p3, p0, Lhjq;->b:Livi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjq;->c:Lhjp;

    move-object/from16 v0, p0

    iget-object v12, v0, Lhjq;->a:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhjq;->b:Livi;

    if-eqz v1, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Livi;->k(I)I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "IOHistory"

    const-string v2, "No data to classify."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Livi;->c(II)Livi;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_3

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "IOHistory"

    const-string v2, "No data to classify."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Livi;->f(I)Livi;

    move-result-object v9

    const/16 v1, 0xa

    invoke-static {v9, v1}, Lilv;->b(Livi;I)Ljava/util/List;

    move-result-object v7

    const/4 v1, 0x4

    invoke-static {v9, v1}, Lilv;->b(Livi;I)Ljava/util/List;

    move-result-object v10

    iget-object v1, v8, Lhjp;->a:Lhjl;

    new-instance v5, Lhjj;

    invoke-direct {v5}, Lhjj;-><init>()V

    new-instance v1, Lhjk;

    invoke-static {v7}, Lhjj;->a(Ljava/util/List;)F

    move-result v2

    const/4 v3, 0x1

    invoke-static {v7, v3}, Lhjj;->a(Ljava/util/List;Z)F

    move-result v3

    invoke-virtual {v5, v7}, Lhjj;->b(Ljava/util/List;)F

    move-result v4

    invoke-virtual {v5, v7}, Lhjj;->c(Ljava/util/List;)F

    move-result v5

    invoke-static {v7}, Lhjj;->d(Ljava/util/List;)F

    move-result v6

    invoke-static {v7}, Lhjj;->e(Ljava/util/List;)F

    move-result v7

    invoke-direct/range {v1 .. v7}, Lhjk;-><init>(FFFFFF)V

    const-wide v2, 0x3fe6a0902de00d1bL    # 0.7071

    const-wide v4, -0x403f10cb295e9e1bL    # -0.1323

    iget v6, v1, Lhjk;->a:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x4065c91d14e3bcd3L    # -0.0256

    iget v6, v1, Lhjk;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x40aab367a0f9096cL    # -0.0013

    iget v6, v1, Lhjk;->c:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, 0x3fa1de69ad42c3caL    # 0.0349

    iget v6, v1, Lhjk;->d:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x4048c7e28240b780L    # -0.0907

    iget v6, v1, Lhjk;->e:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x407d916872b020c5L    # -0.009

    iget v1, v1, Lhjk;->f:F

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double v1, v2, v4

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    const-wide v13, 0x4005bf0a8b145769L    # Math.E

    neg-double v1, v1

    invoke-static {v13, v14, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    add-double/2addr v1, v5

    div-double v1, v3, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v1, v3

    if-ltz v3, :cond_8

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_9

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v3, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v13

    invoke-static {v10}, Lhjd;->a(Ljava/util/List;)[[D

    move-result-object v1

    if-nez v1, :cond_b

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_4

    const-string v2, "CalibrationCollector"

    const-string v3, "Too few gyro readings: %d"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-nez v10, :cond_a

    const/4 v1, 0x0

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 v1, 0x0

    move-object v11, v1

    :goto_4
    if-eqz v9, :cond_5

    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Livi;->i(I)Z

    move-result v1

    if-nez v1, :cond_e

    :cond_5
    const/4 v1, 0x0

    move-object v10, v1

    :goto_5
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_6

    const-string v1, "IOHistory"

    const-string v2, "Indoor probability: <%s, %.5f>; Is still:%s; Max speed: %s dms/s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v13, Lhue;->a:Ljava/lang/Object;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v13, Lhue;->b:Ljava/lang/Object;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v11, v3, v4

    const/4 v4, 0x3

    aput-object v10, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    new-instance v1, Lidx;

    const/4 v2, 0x1

    iget-object v3, v8, Lhjp;->b:Lidu;

    invoke-interface {v3}, Lidu;->h()Ljavax/crypto/SecretKey;

    move-result-object v3

    const/4 v4, 0x2

    iget-object v5, v8, Lhjp;->b:Lidu;

    invoke-interface {v5}, Lidu;->i()[B

    move-result-object v5

    sget-object v6, Lihj;->aR:Livk;

    iget-object v7, v8, Lhjp;->b:Lidu;

    invoke-interface {v7}, Lidu;->t()Ljava/io/File;

    move-result-object v7

    iget-object v9, v8, Lhjp;->b:Lidu;

    invoke-direct/range {v1 .. v9}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    :try_start_0
    invoke-virtual {v1}, Lidx;->a()Livi;

    move-result-object v2

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_7

    const-string v3, "IOHistory"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Actual file version: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lidx;->b()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    move-object v3, v2

    :goto_6
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2, v3}, Lhjp;->a(Ljava/util/TimeZone;Livi;)I

    move-result v2

    if-gez v2, :cond_12

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "IOHistory"

    const-string v2, "Unable to find/create time zone ID."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_9
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v1, v4, v1

    goto/16 :goto_2

    :cond_a
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    goto/16 :goto_3

    :cond_b
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_c

    const-string v2, "CalibrationCollector"

    const-string v3, "VX: %.8f, VY: %.8f, VZ: %.8f"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    aget-object v6, v1, v6

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v1, v6

    const/4 v7, 0x1

    aget-wide v6, v6, v7

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x1

    aget-object v6, v1, v6

    const/4 v7, 0x2

    aget-wide v6, v6, v7

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const/4 v2, 0x1

    aget-object v2, v1, v2

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide v4, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_d

    const/4 v2, 0x1

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    const-wide v4, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_d

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x2

    aget-wide v1, v1, v2

    const-wide v3, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v1, v1, v3

    if-gez v1, :cond_d

    const/4 v1, 0x1

    :goto_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v11, v1

    goto/16 :goto_4

    :cond_d
    const/4 v1, 0x0

    goto :goto_7

    :cond_e
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    move v5, v1

    move v1, v3

    move v3, v4

    :goto_8
    const/16 v4, 0x8

    invoke-virtual {v9, v4}, Livi;->k(I)I

    move-result v4

    if-ge v5, v4, :cond_f

    const/16 v4, 0x8

    invoke-virtual {v9, v4, v5}, Livi;->c(II)Livi;

    move-result-object v6

    const/16 v4, 0x8

    invoke-virtual {v6, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_18

    const/16 v4, 0x8

    invoke-virtual {v6, v4}, Livi;->c(I)I

    move-result v4

    if-nez v4, :cond_18

    const/16 v4, 0x10

    invoke-virtual {v6, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_18

    add-int/lit8 v4, v3, 0x1

    const/16 v3, 0x10

    invoke-virtual {v6, v3}, Livi;->e(I)F

    move-result v3

    cmpl-float v6, v3, v1

    if-ltz v6, :cond_17

    move v2, v3

    move v3, v4

    :goto_9
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    goto :goto_8

    :cond_f
    const/4 v4, 0x2

    if-lt v3, v4, :cond_10

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v10, v1

    goto/16 :goto_5

    :cond_10
    const/4 v1, 0x0

    move-object v10, v1

    goto/16 :goto_5

    :catch_0
    move-exception v2

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_11

    const-string v2, "IOHistory"

    const-string v3, "Invalid or empty IO history."

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    new-instance v2, Livi;

    sget-object v3, Lihj;->aR:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    move-object v3, v2

    goto/16 :goto_6

    :cond_12
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    new-instance v6, Livi;

    sget-object v7, Lihj;->aQ:Livk;

    invoke-direct {v6, v7}, Livi;-><init>(Livk;)V

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v4, v5}, Livi;->a(IJ)Livi;

    const/4 v4, 0x2

    invoke-virtual {v6, v4, v2}, Livi;->e(II)Livi;

    iget-object v2, v13, Lhue;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_13

    const/4 v4, 0x3

    iget-object v2, v13, Lhue;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v6, v4, v2}, Livi;->a(IZ)Livi;

    :cond_13
    const/4 v4, 0x4

    iget-object v2, v13, Lhue;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    mul-double/2addr v12, v14

    double-to-int v2, v12

    invoke-virtual {v6, v4, v2}, Livi;->e(II)Livi;

    if-eqz v11, :cond_14

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v2, 0x5

    const/4 v4, 0x1

    invoke-virtual {v6, v2, v4}, Livi;->a(IZ)Livi;

    :cond_14
    if-eqz v10, :cond_15

    const/4 v2, 0x6

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v2, v4}, Livi;->e(II)Livi;

    :cond_15
    const/4 v2, 0x7

    const/4 v4, 0x1

    invoke-virtual {v6, v2, v4}, Livi;->e(II)Livi;

    invoke-static {v3}, Lhjp;->b(Livi;)V

    const/4 v2, 0x2

    invoke-virtual {v3, v2, v6}, Livi;->a(ILivi;)V

    :try_start_1
    invoke-virtual {v1, v3}, Lidx;->b(Livi;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_16
    :goto_a
    new-instance v2, Lhjr;

    sget-object v1, Lhkg;->i:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lilt;

    invoke-direct {v2, v1}, Lhjr;-><init>(Lilt;)V

    invoke-virtual {v2, v3}, Lhjr;->a(Livi;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v8, Lhjp;->c:Lhjf;

    iget-object v3, v8, Lhjp;->b:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->b()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4, v1}, Lhjf;->a(JLjava/util/Map;)V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_16

    const-string v1, "IOHistory"

    const-string v2, "Unable to save data to disk."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_17
    move v3, v4

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    goto/16 :goto_9

    :cond_18
    move/from16 v16, v2

    move v2, v1

    move/from16 v1, v16

    goto/16 :goto_9
.end method
