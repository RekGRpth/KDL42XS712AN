.class final Lhtx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhuf;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Ljava/io/DataInput;)Lhtw;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lhty;->values()[Lhty;

    move-result-object v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v2, Lhty;->a:Lhty;

    if-ne v1, v2, :cond_0

    sget-object v0, Lhug;->h:Lhuf;

    invoke-interface {v0, p0}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhug;

    :cond_0
    new-instance v2, Lhtw;

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    invoke-direct {v2, v0, v1, v3, v4}, Lhtw;-><init>(Lhug;Lhty;J)V

    return-object v2

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid status"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lhtx;->b(Ljava/io/DataInput;)Lhtw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2

    check-cast p1, Lhtw;

    iget-object v0, p1, Lhtw;->d:Lhty;

    invoke-virtual {v0}, Lhty;->ordinal()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lhtw;->d:Lhty;

    sget-object v1, Lhty;->a:Lhty;

    if-ne v0, v1, :cond_0

    sget-object v0, Lhug;->h:Lhuf;

    iget-object v1, p1, Lhtw;->c:Lhug;

    invoke-interface {v0, v1, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :cond_0
    iget-wide v0, p1, Lhtw;->e:J

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    return-void
.end method
