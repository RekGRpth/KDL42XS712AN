.class public final Lbug;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/net/HttpURLConnection;Lbty;Landroid/content/Context;)V
    .locals 4

    :try_start_0
    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bearer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbty;->b:Lbsp;

    new-instance v3, Lbmx;

    invoke-virtual {v2}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    invoke-direct {v3, v2}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v3, p2}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lbub;

    const-string v2, "Failed to get a token"

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lbub;

    const-string v2, "Failed to get a token"

    invoke-direct {v1, v2, v0}, Lbub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
