.class final Lhve;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lhvb;


# direct methods
.method constructor <init>(Lhvb;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lhve;->b:Lhvb;

    iput-object p2, p0, Lhve;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhve;->b:Lhvb;

    invoke-static {v0}, Lhvb;->d(Lhvb;)Lhon;

    move-result-object v0

    iget-object v3, p0, Lhve;->a:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lhon;->c(Landroid/content/Intent;)V

    iget-object v0, p0, Lhve;->b:Lhvb;

    invoke-static {v0}, Lhvb;->d(Lhvb;)Lhon;

    move-result-object v0

    invoke-virtual {v0}, Lhon;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iget-object v4, p0, Lhve;->b:Lhvb;

    invoke-static {v0}, Lhvb;->b(Landroid/os/Bundle;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    iget-object v5, p0, Lhve;->b:Lhvb;

    invoke-static {v0}, Lhvb;->a(Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v5

    iget-object v0, p0, Lhve;->b:Lhvb;

    invoke-static {v0}, Lhvb;->e(Lhvb;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v5, v0}, Lilm;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v0

    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    if-eqz v0, :cond_2

    iget-object v6, p0, Lhve;->b:Lhvb;

    const/4 v7, 0x2

    if-ne v0, v7, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v6, v4, v5, v0}, Lhvb;->b(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V

    const-string v0, "GCoreFlp"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Added request from SystemMemoryCache: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-static {v0, v5}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    const-string v0, "GCoreFlp"

    const-string v4, "Incomplete LocationRequest found in SystemMemoryCache."

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    return-void
.end method
