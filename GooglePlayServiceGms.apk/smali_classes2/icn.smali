.class public final enum Licn;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Licn;

.field public static final enum B:Licn;

.field public static final enum C:Licn;

.field public static final enum D:Licn;

.field public static final enum E:Licn;

.field public static final enum F:Licn;

.field public static final enum G:Licn;

.field public static final enum H:Licn;

.field public static final enum I:Licn;

.field public static final enum J:Licn;

.field public static final enum K:Licn;

.field public static final enum L:Licn;

.field public static final enum M:Licn;

.field public static final enum N:Licn;

.field public static final enum O:Licn;

.field public static final enum P:Licn;

.field public static final enum Q:Licn;

.field public static final enum R:Licn;

.field public static final enum S:Licn;

.field public static final enum T:Licn;

.field public static final enum U:Licn;

.field public static final enum V:Licn;

.field public static final enum W:Licn;

.field public static final enum X:Licn;

.field private static final synthetic Z:[Licn;

.field public static final enum a:Licn;

.field public static final enum b:Licn;

.field public static final enum c:Licn;

.field public static final enum d:Licn;

.field public static final enum e:Licn;

.field public static final enum f:Licn;

.field public static final enum g:Licn;

.field public static final enum h:Licn;

.field public static final enum i:Licn;

.field public static final enum j:Licn;

.field public static final enum k:Licn;

.field public static final enum l:Licn;

.field public static final enum m:Licn;

.field public static final enum n:Licn;

.field public static final enum o:Licn;

.field public static final enum p:Licn;

.field public static final enum q:Licn;

.field public static final enum r:Licn;

.field public static final enum s:Licn;

.field public static final enum t:Licn;

.field public static final enum u:Licn;

.field public static final enum v:Licn;

.field public static final enum w:Licn;

.field public static final enum x:Licn;

.field public static final enum y:Licn;

.field public static final enum z:Licn;


# instance fields
.field public final Y:Lico;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Licn;

    const-string v1, "INITIALIZE"

    sget-object v2, Lico;->a:Lico;

    invoke-direct {v0, v1, v4, v2}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->a:Licn;

    new-instance v0, Licn;

    const-string v1, "QUIT"

    sget-object v2, Lico;->a:Lico;

    invoke-direct {v0, v1, v5, v2}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->b:Licn;

    new-instance v0, Licn;

    const-string v1, "SET_PERIOD"

    sget-object v2, Lico;->a:Lico;

    invoke-direct {v0, v1, v6, v2}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->c:Licn;

    new-instance v0, Licn;

    const-string v1, "AIRPLANE_MODE_CHANGED"

    sget-object v2, Lico;->a:Lico;

    invoke-direct {v0, v1, v7, v2}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->d:Licn;

    new-instance v0, Licn;

    const-string v1, "ALARM_RING"

    sget-object v2, Lico;->a:Lico;

    invoke-direct {v0, v1, v8, v2}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->e:Licn;

    new-instance v0, Licn;

    const-string v1, "BATTERY_STATE_CHANGED"

    const/4 v2, 0x5

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->f:Licn;

    new-instance v0, Licn;

    const-string v1, "CELL_SCAN_RESULTS"

    const/4 v2, 0x6

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->g:Licn;

    new-instance v0, Licn;

    const-string v1, "CELL_SIGNAL_STRENGTH"

    const/4 v2, 0x7

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->h:Licn;

    new-instance v0, Licn;

    const-string v1, "FULL_COLLECTION_MODE_CHANGED"

    const/16 v2, 0x8

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->i:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_DEVICE_LOCATION_RESPONSE"

    const/16 v2, 0x9

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->j:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_MODEL_QUERY_RESPONSE"

    const/16 v2, 0xa

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->k:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_QUERY_RESPONSE"

    const/16 v2, 0xb

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->l:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_UPLOAD_RESPONSE"

    const/16 v2, 0xc

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->m:Licn;

    new-instance v0, Licn;

    const-string v1, "GPS_LOCATION"

    const/16 v2, 0xd

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->n:Licn;

    new-instance v0, Licn;

    const-string v1, "NETWORK_CHANGED"

    const/16 v2, 0xe

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->o:Licn;

    new-instance v0, Licn;

    const-string v1, "NLP_PARAMS_CHANGED"

    const/16 v2, 0xf

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->p:Licn;

    new-instance v0, Licn;

    const-string v1, "SCREEN_STATE_CHANGED"

    const/16 v2, 0x10

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->q:Licn;

    new-instance v0, Licn;

    const-string v1, "WIFI_SCAN_RESULTS"

    const/16 v2, 0x11

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->r:Licn;

    new-instance v0, Licn;

    const-string v1, "WIFI_STATE_CHANGED"

    const/16 v2, 0x12

    sget-object v3, Lico;->a:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->s:Licn;

    new-instance v0, Licn;

    const-string v1, "ALARM_RESET"

    const/16 v2, 0x13

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->t:Licn;

    new-instance v0, Licn;

    const-string v1, "ALARM_CANCEL"

    const/16 v2, 0x14

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->u:Licn;

    new-instance v0, Licn;

    const-string v1, "CELL_REQUEST_SCAN"

    const/16 v2, 0x15

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->v:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_DEVICE_LOCATION_QUERY"

    const/16 v2, 0x16

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->w:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_QUERY"

    const/16 v2, 0x17

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->x:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_UPLOAD"

    const/16 v2, 0x18

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->y:Licn;

    new-instance v0, Licn;

    const-string v1, "GLS_MODEL_QUERY"

    const/16 v2, 0x19

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->z:Licn;

    new-instance v0, Licn;

    const-string v1, "PERSISTENT_STATE_DIR"

    const/16 v2, 0x1a

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->A:Licn;

    new-instance v0, Licn;

    const-string v1, "MAKE_FILE_PRIVATE"

    const/16 v2, 0x1b

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->B:Licn;

    new-instance v0, Licn;

    const-string v1, "COLLECTION_POLICY_STATE_DIR"

    const/16 v2, 0x1c

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->C:Licn;

    new-instance v0, Licn;

    const-string v1, "SEEN_DEVICES_DIR"

    const/16 v2, 0x1d

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->D:Licn;

    new-instance v0, Licn;

    const-string v1, "NLP_PARAMS_STATE_DIR"

    const/16 v2, 0x1e

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->E:Licn;

    new-instance v0, Licn;

    const-string v1, "COLLECTOR_STATE_DIR"

    const/16 v2, 0x1f

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->F:Licn;

    new-instance v0, Licn;

    const-string v1, "GET_ENCRYPTION_KEY"

    const/16 v2, 0x20

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->G:Licn;

    new-instance v0, Licn;

    const-string v1, "GPS_ON_OFF"

    const/16 v2, 0x21

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->H:Licn;

    new-instance v0, Licn;

    const-string v1, "IS_GPS_ENABLED"

    const/16 v2, 0x22

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->I:Licn;

    new-instance v0, Licn;

    const-string v1, "LOCATION_REPORT"

    const/16 v2, 0x23

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->J:Licn;

    new-instance v0, Licn;

    const-string v1, "STATUS_REPORT"

    const/16 v2, 0x24

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->K:Licn;

    new-instance v0, Licn;

    const-string v1, "LOG"

    const/16 v2, 0x25

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->L:Licn;

    new-instance v0, Licn;

    const-string v1, "WAKELOCK_ACQUIRE"

    const/16 v2, 0x26

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->M:Licn;

    new-instance v0, Licn;

    const-string v1, "WAKELOCK_RELEASE"

    const/16 v2, 0x27

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->N:Licn;

    new-instance v0, Licn;

    const-string v1, "WIFI_REQUEST_SCAN"

    const/16 v2, 0x28

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->O:Licn;

    new-instance v0, Licn;

    const-string v1, "USER_REPORT_MAPS_ISSUE"

    const/16 v2, 0x29

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->P:Licn;

    new-instance v0, Licn;

    const-string v1, "ACTIVITY_DETECTION_START"

    const/16 v2, 0x2a

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->Q:Licn;

    new-instance v0, Licn;

    const-string v1, "ACTIVITY_DETECTION_RESULT"

    const/16 v2, 0x2b

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->R:Licn;

    new-instance v0, Licn;

    const-string v1, "ACTIVITY_INSUFFICIENT_SAMPLES"

    const/16 v2, 0x2c

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->S:Licn;

    new-instance v0, Licn;

    const-string v1, "SIGNIFICANT_MOTION"

    const/16 v2, 0x2d

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->T:Licn;

    new-instance v0, Licn;

    const-string v1, "LOW_POWER_MODE_OFF"

    const/16 v2, 0x2e

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->U:Licn;

    new-instance v0, Licn;

    const-string v1, "LOW_POWER_MODE_ON"

    const/16 v2, 0x2f

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->V:Licn;

    new-instance v0, Licn;

    const-string v1, "VEHICLE_EXIT_STATE_CHANGE"

    const/16 v2, 0x30

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->W:Licn;

    new-instance v0, Licn;

    const-string v1, "VEHICLE_EXIT_DETECTED"

    const/16 v2, 0x31

    sget-object v3, Lico;->b:Lico;

    invoke-direct {v0, v1, v2, v3}, Licn;-><init>(Ljava/lang/String;ILico;)V

    sput-object v0, Licn;->X:Licn;

    const/16 v0, 0x32

    new-array v0, v0, [Licn;

    sget-object v1, Licn;->a:Licn;

    aput-object v1, v0, v4

    sget-object v1, Licn;->b:Licn;

    aput-object v1, v0, v5

    sget-object v1, Licn;->c:Licn;

    aput-object v1, v0, v6

    sget-object v1, Licn;->d:Licn;

    aput-object v1, v0, v7

    sget-object v1, Licn;->e:Licn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Licn;->f:Licn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Licn;->g:Licn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Licn;->h:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Licn;->i:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Licn;->j:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Licn;->k:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Licn;->l:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Licn;->m:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Licn;->n:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Licn;->o:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Licn;->p:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Licn;->q:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Licn;->r:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Licn;->s:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Licn;->t:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Licn;->u:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Licn;->v:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Licn;->w:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Licn;->x:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Licn;->y:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Licn;->z:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Licn;->A:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Licn;->B:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Licn;->C:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Licn;->D:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Licn;->E:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Licn;->F:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Licn;->G:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Licn;->H:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Licn;->I:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Licn;->J:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Licn;->K:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Licn;->L:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Licn;->M:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Licn;->N:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Licn;->O:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Licn;->P:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Licn;->Q:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Licn;->R:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Licn;->S:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Licn;->T:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Licn;->U:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Licn;->V:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Licn;->W:Licn;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Licn;->X:Licn;

    aput-object v2, v0, v1

    sput-object v0, Licn;->Z:[Licn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILico;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Licn;->Y:Lico;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Licn;
    .locals 1

    const-class v0, Licn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Licn;

    return-object v0
.end method

.method public static values()[Licn;
    .locals 1

    sget-object v0, Licn;->Z:[Licn;

    invoke-virtual {v0}, [Licn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Licn;

    return-object v0
.end method
