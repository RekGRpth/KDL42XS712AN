.class public final Lfer;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile D:[Lfer;


# instance fields
.field public A:I

.field public B:J

.field public a:Lfew;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field public j:I

.field public k:I

.field public l:I

.field public m:Z

.field public n:I

.field public o:I

.field public p:I

.field public q:Z

.field public r:I

.field public s:I

.field public t:I

.field public u:Z

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfer;->a:Lfew;

    iput v1, p0, Lfer;->b:I

    iput v1, p0, Lfer;->c:I

    iput v1, p0, Lfer;->d:I

    iput-boolean v1, p0, Lfer;->e:Z

    iput v1, p0, Lfer;->f:I

    iput v1, p0, Lfer;->g:I

    iput v1, p0, Lfer;->h:I

    iput-boolean v1, p0, Lfer;->i:Z

    iput v1, p0, Lfer;->j:I

    iput v1, p0, Lfer;->k:I

    iput v1, p0, Lfer;->l:I

    iput-boolean v1, p0, Lfer;->m:Z

    iput v1, p0, Lfer;->n:I

    iput v1, p0, Lfer;->o:I

    iput v1, p0, Lfer;->p:I

    iput-boolean v1, p0, Lfer;->q:Z

    iput v1, p0, Lfer;->r:I

    iput v1, p0, Lfer;->s:I

    iput v1, p0, Lfer;->t:I

    iput-boolean v1, p0, Lfer;->u:Z

    iput v1, p0, Lfer;->v:I

    iput v1, p0, Lfer;->w:I

    iput v1, p0, Lfer;->x:I

    iput v1, p0, Lfer;->y:I

    iput v1, p0, Lfer;->z:I

    iput v1, p0, Lfer;->A:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfer;->B:J

    const/4 v0, -0x1

    iput v0, p0, Lfer;->C:I

    return-void
.end method

.method public static c()[Lfer;
    .locals 2

    sget-object v0, Lfer;->D:[Lfer;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lfer;->D:[Lfer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lfer;

    sput-object v0, Lfer;->D:[Lfer;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lfer;->D:[Lfer;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lfer;->a:Lfew;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lfer;->a:Lfew;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Lfer;->b:I

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Lfer;->b:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lfer;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lfer;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lfer;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lfer;->d:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lfer;->e:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lfer;->e:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lfer;->f:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lfer;->f:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lfer;->g:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Lfer;->g:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lfer;->h:I

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Lfer;->h:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lfer;->i:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Lfer;->i:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lfer;->j:I

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget v2, p0, Lfer;->j:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lfer;->k:I

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget v2, p0, Lfer;->k:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lfer;->l:I

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget v2, p0, Lfer;->l:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Lfer;->m:Z

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget-boolean v2, p0, Lfer;->m:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lfer;->n:I

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    iget v2, p0, Lfer;->n:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lfer;->o:I

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    iget v2, p0, Lfer;->o:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lfer;->p:I

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    iget v2, p0, Lfer;->p:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-boolean v1, p0, Lfer;->q:Z

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    iget-boolean v2, p0, Lfer;->q:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Lfer;->r:I

    if-eqz v1, :cond_11

    const/16 v1, 0x12

    iget v2, p0, Lfer;->r:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Lfer;->s:I

    if-eqz v1, :cond_12

    const/16 v1, 0x13

    iget v2, p0, Lfer;->s:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget v1, p0, Lfer;->t:I

    if-eqz v1, :cond_13

    const/16 v1, 0x14

    iget v2, p0, Lfer;->t:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-boolean v1, p0, Lfer;->u:Z

    if-eqz v1, :cond_14

    const/16 v1, 0x15

    iget-boolean v2, p0, Lfer;->u:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_14
    iget v1, p0, Lfer;->v:I

    if-eqz v1, :cond_15

    const/16 v1, 0x16

    iget v2, p0, Lfer;->v:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    iget v1, p0, Lfer;->w:I

    if-eqz v1, :cond_16

    const/16 v1, 0x17

    iget v2, p0, Lfer;->w:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget v1, p0, Lfer;->x:I

    if-eqz v1, :cond_17

    const/16 v1, 0x18

    iget v2, p0, Lfer;->x:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    iget v1, p0, Lfer;->y:I

    if-eqz v1, :cond_18

    const/16 v1, 0x19

    iget v2, p0, Lfer;->y:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    iget v1, p0, Lfer;->z:I

    if-eqz v1, :cond_19

    const/16 v1, 0x1a

    iget v2, p0, Lfer;->z:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    iget v1, p0, Lfer;->A:I

    if-eqz v1, :cond_1a

    const/16 v1, 0x1b

    iget v2, p0, Lfer;->A:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    iget-wide v1, p0, Lfer;->B:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1b

    const/16 v1, 0x1c

    iget-wide v2, p0, Lfer;->B:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    iput v0, p0, Lfer;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lfer;->a:Lfew;

    if-nez v0, :cond_1

    new-instance v0, Lfew;

    invoke-direct {v0}, Lfew;-><init>()V

    iput-object v0, p0, Lfer;->a:Lfew;

    :cond_1
    iget-object v0, p0, Lfer;->a:Lfew;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfer;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfer;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->k:I

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfer;->m:Z

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->n:I

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->o:I

    goto :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->p:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfer;->q:Z

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->r:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->s:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->t:I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfer;->u:Z

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->v:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->w:I

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->x:I

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->y:I

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->z:I

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfer;->A:I

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lfer;->B:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe0 -> :sswitch_1c
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-object v0, p0, Lfer;->a:Lfew;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lfer;->a:Lfew;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget v0, p0, Lfer;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lfer;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1
    iget v0, p0, Lfer;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lfer;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    iget v0, p0, Lfer;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lfer;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_3
    iget-boolean v0, p0, Lfer;->e:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lfer;->e:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_4
    iget v0, p0, Lfer;->f:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lfer;->f:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_5
    iget v0, p0, Lfer;->g:I

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lfer;->g:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_6
    iget v0, p0, Lfer;->h:I

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Lfer;->h:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_7
    iget-boolean v0, p0, Lfer;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lfer;->i:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_8
    iget v0, p0, Lfer;->j:I

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Lfer;->j:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_9
    iget v0, p0, Lfer;->k:I

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lfer;->k:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_a
    iget v0, p0, Lfer;->l:I

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lfer;->l:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_b
    iget-boolean v0, p0, Lfer;->m:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-boolean v1, p0, Lfer;->m:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_c
    iget v0, p0, Lfer;->n:I

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    iget v1, p0, Lfer;->n:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_d
    iget v0, p0, Lfer;->o:I

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget v1, p0, Lfer;->o:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_e
    iget v0, p0, Lfer;->p:I

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    iget v1, p0, Lfer;->p:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_f
    iget-boolean v0, p0, Lfer;->q:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    iget-boolean v1, p0, Lfer;->q:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_10
    iget v0, p0, Lfer;->r:I

    if-eqz v0, :cond_11

    const/16 v0, 0x12

    iget v1, p0, Lfer;->r:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_11
    iget v0, p0, Lfer;->s:I

    if-eqz v0, :cond_12

    const/16 v0, 0x13

    iget v1, p0, Lfer;->s:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_12
    iget v0, p0, Lfer;->t:I

    if-eqz v0, :cond_13

    const/16 v0, 0x14

    iget v1, p0, Lfer;->t:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_13
    iget-boolean v0, p0, Lfer;->u:Z

    if-eqz v0, :cond_14

    const/16 v0, 0x15

    iget-boolean v1, p0, Lfer;->u:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_14
    iget v0, p0, Lfer;->v:I

    if-eqz v0, :cond_15

    const/16 v0, 0x16

    iget v1, p0, Lfer;->v:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_15
    iget v0, p0, Lfer;->w:I

    if-eqz v0, :cond_16

    const/16 v0, 0x17

    iget v1, p0, Lfer;->w:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_16
    iget v0, p0, Lfer;->x:I

    if-eqz v0, :cond_17

    const/16 v0, 0x18

    iget v1, p0, Lfer;->x:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_17
    iget v0, p0, Lfer;->y:I

    if-eqz v0, :cond_18

    const/16 v0, 0x19

    iget v1, p0, Lfer;->y:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_18
    iget v0, p0, Lfer;->z:I

    if-eqz v0, :cond_19

    const/16 v0, 0x1a

    iget v1, p0, Lfer;->z:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_19
    iget v0, p0, Lfer;->A:I

    if-eqz v0, :cond_1a

    const/16 v0, 0x1b

    iget v1, p0, Lfer;->A:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1a
    iget-wide v0, p0, Lfer;->B:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1b

    const/16 v0, 0x1c

    iget-wide v1, p0, Lfer;->B:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_1b
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfer;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfer;

    iget-object v2, p0, Lfer;->a:Lfew;

    if-nez v2, :cond_3

    iget-object v2, p1, Lfer;->a:Lfew;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfer;->a:Lfew;

    iget-object v3, p1, Lfer;->a:Lfew;

    invoke-virtual {v2, v3}, Lfew;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lfer;->b:I

    iget v3, p1, Lfer;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lfer;->c:I

    iget v3, p1, Lfer;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lfer;->d:I

    iget v3, p1, Lfer;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lfer;->e:Z

    iget-boolean v3, p1, Lfer;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lfer;->f:I

    iget v3, p1, Lfer;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lfer;->g:I

    iget v3, p1, Lfer;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Lfer;->h:I

    iget v3, p1, Lfer;->h:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-boolean v2, p0, Lfer;->i:Z

    iget-boolean v3, p1, Lfer;->i:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Lfer;->j:I

    iget v3, p1, Lfer;->j:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget v2, p0, Lfer;->k:I

    iget v3, p1, Lfer;->k:I

    if-eq v2, v3, :cond_e

    move v0, v1

    goto :goto_0

    :cond_e
    iget v2, p0, Lfer;->l:I

    iget v3, p1, Lfer;->l:I

    if-eq v2, v3, :cond_f

    move v0, v1

    goto :goto_0

    :cond_f
    iget-boolean v2, p0, Lfer;->m:Z

    iget-boolean v3, p1, Lfer;->m:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    goto :goto_0

    :cond_10
    iget v2, p0, Lfer;->n:I

    iget v3, p1, Lfer;->n:I

    if-eq v2, v3, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget v2, p0, Lfer;->o:I

    iget v3, p1, Lfer;->o:I

    if-eq v2, v3, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget v2, p0, Lfer;->p:I

    iget v3, p1, Lfer;->p:I

    if-eq v2, v3, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-boolean v2, p0, Lfer;->q:Z

    iget-boolean v3, p1, Lfer;->q:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget v2, p0, Lfer;->r:I

    iget v3, p1, Lfer;->r:I

    if-eq v2, v3, :cond_15

    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget v2, p0, Lfer;->s:I

    iget v3, p1, Lfer;->s:I

    if-eq v2, v3, :cond_16

    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget v2, p0, Lfer;->t:I

    iget v3, p1, Lfer;->t:I

    if-eq v2, v3, :cond_17

    move v0, v1

    goto/16 :goto_0

    :cond_17
    iget-boolean v2, p0, Lfer;->u:Z

    iget-boolean v3, p1, Lfer;->u:Z

    if-eq v2, v3, :cond_18

    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget v2, p0, Lfer;->v:I

    iget v3, p1, Lfer;->v:I

    if-eq v2, v3, :cond_19

    move v0, v1

    goto/16 :goto_0

    :cond_19
    iget v2, p0, Lfer;->w:I

    iget v3, p1, Lfer;->w:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget v2, p0, Lfer;->x:I

    iget v3, p1, Lfer;->x:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    goto/16 :goto_0

    :cond_1b
    iget v2, p0, Lfer;->y:I

    iget v3, p1, Lfer;->y:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    goto/16 :goto_0

    :cond_1c
    iget v2, p0, Lfer;->z:I

    iget v3, p1, Lfer;->z:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    goto/16 :goto_0

    :cond_1d
    iget v2, p0, Lfer;->A:I

    iget v3, p1, Lfer;->A:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    goto/16 :goto_0

    :cond_1e
    iget-wide v2, p0, Lfer;->B:J

    iget-wide v4, p1, Lfer;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    iget-object v0, p0, Lfer;->a:Lfew;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->b:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->c:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->d:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfer;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->f:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->g:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->h:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfer;->i:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->j:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->k:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->l:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfer;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->n:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->o:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->p:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfer;->q:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->r:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->s:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfer;->t:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lfer;->u:Z

    if-eqz v3, :cond_5

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->v:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->w:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->x:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->y:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->z:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfer;->A:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lfer;->B:J

    iget-wide v3, p0, Lfer;->B:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lfer;->a:Lfew;

    invoke-virtual {v0}, Lfew;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
