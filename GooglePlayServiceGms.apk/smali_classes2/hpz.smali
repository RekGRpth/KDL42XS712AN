.class final enum Lhpz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhpz;

.field public static final enum b:Lhpz;

.field public static final enum c:Lhpz;

.field public static final enum d:Lhpz;

.field public static final enum e:Lhpz;

.field private static final synthetic f:[Lhpz;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhpz;

    const-string v1, "NOT_READ_YET"

    invoke-direct {v0, v1, v2}, Lhpz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpz;->a:Lhpz;

    new-instance v0, Lhpz;

    const-string v1, "READ_INTERRUPTED"

    invoke-direct {v0, v1, v3}, Lhpz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpz;->b:Lhpz;

    new-instance v0, Lhpz;

    const-string v1, "READ_SUCCESS"

    invoke-direct {v0, v1, v4}, Lhpz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpz;->c:Lhpz;

    new-instance v0, Lhpz;

    const-string v1, "READ_FAILURE"

    invoke-direct {v0, v1, v5}, Lhpz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpz;->d:Lhpz;

    new-instance v0, Lhpz;

    const-string v1, "INVALID_FORMAT"

    invoke-direct {v0, v1, v6}, Lhpz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpz;->e:Lhpz;

    const/4 v0, 0x5

    new-array v0, v0, [Lhpz;

    sget-object v1, Lhpz;->a:Lhpz;

    aput-object v1, v0, v2

    sget-object v1, Lhpz;->b:Lhpz;

    aput-object v1, v0, v3

    sget-object v1, Lhpz;->c:Lhpz;

    aput-object v1, v0, v4

    sget-object v1, Lhpz;->d:Lhpz;

    aput-object v1, v0, v5

    sget-object v1, Lhpz;->e:Lhpz;

    aput-object v1, v0, v6

    sput-object v0, Lhpz;->f:[Lhpz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhpz;
    .locals 1

    const-class v0, Lhpz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhpz;

    return-object v0
.end method

.method public static values()[Lhpz;
    .locals 1

    sget-object v0, Lhpz;->f:[Lhpz;

    invoke-virtual {v0}, [Lhpz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpz;

    return-object v0
.end method
