.class final Lcnh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcnf;

.field private b:Lcfp;

.field private c:Lbwm;


# direct methods
.method public constructor <init>(Lcnf;Lcfp;)V
    .locals 0

    iput-object p1, p0, Lcnh;->a:Lcnf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcnh;->b:Lcfp;

    return-void
.end method

.method static synthetic a(Lcnh;)Lbwm;
    .locals 1

    iget-object v0, p0, Lcnh;->c:Lbwm;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v1, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v0, v1}, Lcnf;->a(Lcfp;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcnh;->c:Lbwm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnh;->c:Lbwm;

    invoke-virtual {v0}, Lbwm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 7

    const/4 v5, 0x2

    const/4 v2, 0x1

    :try_start_0
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->b:Lcfz;

    iget-object v1, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v1}, Lcfp;->c()J

    move-result-wide v3

    invoke-interface {v0, v3, v4}, Lcfz;->a(J)Lcfc;

    move-result-object v0

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lcnh;->a:Lcnf;

    iget-object v1, v1, Lcnf;->b:Lcfz;

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v1

    iput-object v1, p0, Lcnh;->b:Lcfp;

    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    iget-object v1, p0, Lcnh;->a:Lcnf;

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v1, v3}, Lcnf;->a(Lcfp;)Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v1, v0, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    iget-object v2, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    const-string v1, "PinnedContentDownloader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Requesting download of file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v4}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcnh;->a:Lcnf;

    iget-object v1, v1, Lcnf;->g:Lbwb;

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3}, Lcfp;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lbwb;->a(Lbsp;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcnh;->a:Lcnf;

    iget-object v3, v3, Lcnf;->c:Lbwo;

    iget-object v4, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3, v0, v4, v1}, Lbwo;->a(Lbsp;Lcfp;Ljava/lang/String;)Lbwm;

    move-result-object v0

    iput-object v0, p0, Lcnh;->c:Lbwm;

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    iget-object v0, p0, Lcnh;->c:Lbwm;

    invoke-virtual {v0}, Lbwm;->b()I

    move-result v0

    if-eq v0, v5, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download failed with status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_5
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->f:Ljava/util/Map;

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, "PinnedContentDownloader"

    const-string v3, "Failed to download file (attempt %d): %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v6}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v1, v3, v4}, Lcbv;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcnh;->a:Lcnf;

    iget-object v1, v1, Lcnf;->f:Ljava/util/Map;

    iget-object v2, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v1, v0, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_6
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    iget-object v2, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit p0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :catchall_3
    move-exception v0

    iget-object v1, p0, Lcnh;->a:Lcnf;

    iget-object v1, v1, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_8
    iget-object v2, p0, Lcnh;->a:Lcnf;

    iget-object v2, v2, Lcnf;->e:Ljava/util/Map;

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcnh;->a:Lcnf;

    iget-object v2, v2, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    throw v0

    :cond_1
    :try_start_9
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->f:Ljava/util/Map;

    iget-object v1, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v1}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "PinnedContentDownloader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Finished downloading file: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v3}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v1, v0, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_a
    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    iget-object v2, p0, Lcnh;->b:Lcfp;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcnh;->a:Lcnf;

    iget-object v0, v0, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :catchall_5
    move-exception v0

    monitor-exit v1

    throw v0
.end method
