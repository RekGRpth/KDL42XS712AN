.class public final Lepe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leox;


# instance fields
.field private final a:Lepf;

.field private final b:Lcom/google/android/gms/identity/accounts/api/AccountData;


# direct methods
.method public constructor <init>(Lepf;Lcom/google/android/gms/identity/accounts/api/AccountData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lepe;->b:Lcom/google/android/gms/identity/accounts/api/AccountData;

    iput-object p1, p0, Lepe;->a:Lepf;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lepe;->a:Lepf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lepf;->a([B)V

    return-void
.end method

.method public final a(Leok;)V
    .locals 5

    iget-object v1, p0, Lepe;->a:Lepf;

    iget-object v0, p0, Lepe;->b:Lcom/google/android/gms/identity/accounts/api/AccountData;

    const-string v2, "Account data must not be null."

    invoke-static {v0, v2}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x10

    new-array v2, v2, [B

    iget-object v3, p1, Leok;->a:Ljava/security/SecureRandom;

    invoke-virtual {v3, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-virtual {p1, v0, v2}, Leok;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;[B)[B

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lepf;->a([B)V

    return-void

    :cond_0
    invoke-virtual {p1, v2, v0}, Leok;->a([B[B)[B

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;

    invoke-direct {v4, v0, v2, v3}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;-><init>([B[B[B)V

    invoke-static {v4}, Lbks;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)[B

    move-result-object v0

    goto :goto_0
.end method
