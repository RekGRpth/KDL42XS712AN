.class public final Lfpo;
.super Lcb;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfuh;


# instance fields
.field private final a:Lftz;

.field private b:Lftx;

.field private c:Z

.field private final d:Landroid/accounts/Account;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private i:Lbbo;

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    sget-object v7, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lfpo;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lftz;)V
    .locals 0

    invoke-direct {p0, p1}, Lcb;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lfpo;->d:Landroid/accounts/Account;

    iput-object p3, p0, Lfpo;->e:Ljava/lang/String;

    iput-object p4, p0, Lfpo;->f:Ljava/lang/String;

    iput p5, p0, Lfpo;->g:I

    iput-object p6, p0, Lfpo;->h:Ljava/lang/String;

    iput-object p7, p0, Lfpo;->a:Lftz;

    return-void
.end method

.method private a(Lftx;)V
    .locals 3

    iget-object v0, p0, Lfpo;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfpo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpo;->e:Ljava/lang/String;

    :goto_0
    iget v1, p0, Lfpo;->g:I

    iget-object v2, p0, Lfpo;->h:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lftx;->a(Lfuh;Ljava/lang/String;ILjava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "all"

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfpo;->f:Ljava/lang/String;

    iget v1, p0, Lfpo;->g:I

    iget-object v2, p0, Lfpo;->h:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lftx;->b(Lfuh;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-boolean v0, p0, Lcb;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfpo;->m_()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpo;->c:Z

    return-void
.end method

.method protected final a()V
    .locals 1

    invoke-super {p0}, Lcb;->a()V

    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-direct {p0, v0}, Lfpo;->a(Lftx;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lfpo;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 1

    iput-object p1, p0, Lfpo;->i:Lbbo;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpo;->c:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfpo;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    .locals 0

    iput-object p1, p0, Lfpo;->i:Lbbo;

    iput-object p2, p0, Lfpo;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    invoke-virtual {p0, p2}, Lfpo;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Lbbo;
    .locals 1

    iget-object v0, p0, Lfpo;->i:Lbbo;

    return-object v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpo;->c:Z

    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-direct {p0, v0}, Lfpo;->a(Lftx;)V

    return-void
.end method

.method protected final f()V
    .locals 1

    iget-object v0, p0, Lfpo;->b:Lftx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lfpo;->c:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lfpo;->b:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpo;->c:Z

    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    invoke-super {p0}, Lcb;->g()V

    invoke-virtual {p0}, Lfpo;->f()V

    return-void
.end method

.method protected final m_()V
    .locals 3

    iget-object v0, p0, Lfpo;->b:Lftx;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcb;->o:Landroid/content/Context;

    iget-object v1, p0, Lfpo;->a:Lftz;

    iget-object v2, p0, Lfpo;->d:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v0, p0, p0, v2}, Lfob;->a(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfpo;->b:Lftx;

    :cond_0
    iget-object v0, p0, Lfpo;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpo;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    invoke-virtual {p0, v0}, Lfpo;->b(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p0}, Lfpo;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfpo;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcb;->a()V

    :cond_3
    return-void
.end method
