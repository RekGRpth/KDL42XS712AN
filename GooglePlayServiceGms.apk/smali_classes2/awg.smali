.class final Lawg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[B

.field b:I

.field c:I

.field d:I

.field e:Z

.field f:[Ljava/nio/ByteBuffer;

.field g:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x20000

    new-array v0, v0, [B

    iput-object v0, p0, Lawg;->a:[B

    new-array v0, v1, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lawg;->f:[Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lawg;->g:[Ljava/nio/ByteBuffer;

    const/4 v0, -0x1

    iput v0, p0, Lawg;->d:I

    iput-boolean v1, p0, Lawg;->e:Z

    return-void
.end method


# virtual methods
.method final a(B)V
    .locals 2

    iget-object v0, p0, Lawg;->a:[B

    iget v1, p0, Lawg;->c:I

    aput-byte p1, v0, v1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lawg;->b(I)V

    return-void
.end method

.method final a(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x1

    iget v0, p0, Lawg;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lawg;->b:I

    iget v0, p0, Lawg;->b:I

    iget-object v1, p0, Lawg;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lawg;->b:I

    iget-object v1, p0, Lawg;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lawg;->b:I

    :cond_0
    iget v0, p0, Lawg;->b:I

    iget v1, p0, Lawg;->c:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lawg;->d:I

    if-ne v0, v2, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lawg;->c:I

    iput v0, p0, Lawg;->b:I

    iput v2, p0, Lawg;->d:I

    iput-boolean v3, p0, Lawg;->e:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v3, p0, Lawg;->e:Z

    goto :goto_0
.end method

.method public final a()[Ljava/nio/ByteBuffer;
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lawg;->b:I

    iget v1, p0, Lawg;->c:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lawg;->f:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lawg;->a:[B

    iget v2, p0, Lawg;->b:I

    iget v3, p0, Lawg;->c:I

    iget v4, p0, Lawg;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lawg;->g:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lawg;->a:[B

    iget v2, p0, Lawg;->b:I

    iget-object v3, p0, Lawg;->a:[B

    array-length v3, v3

    iget v4, p0, Lawg;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x1

    iget-object v2, p0, Lawg;->a:[B

    iget v3, p0, Lawg;->c:I

    invoke-static {v2, v5, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method final b(I)V
    .locals 2

    iget v0, p0, Lawg;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lawg;->c:I

    iget v0, p0, Lawg;->c:I

    iget-object v1, p0, Lawg;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lawg;->c:I

    iget-object v1, p0, Lawg;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lawg;->c:I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lawg;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lawg;->d:I

    return-void
.end method

.method public final b()[Ljava/nio/ByteBuffer;
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lawg;->c:I

    iget v1, p0, Lawg;->b:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lawg;->f:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lawg;->a:[B

    iget v2, p0, Lawg;->c:I

    iget v3, p0, Lawg;->b:I

    iget v4, p0, Lawg;->c:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lawg;->g:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lawg;->a:[B

    iget v2, p0, Lawg;->c:I

    iget-object v3, p0, Lawg;->a:[B

    array-length v3, v3

    iget v4, p0, Lawg;->c:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x1

    iget-object v2, p0, Lawg;->a:[B

    iget v3, p0, Lawg;->b:I

    invoke-static {v2, v5, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    iget-boolean v0, p0, Lawg;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawg;->a:[B

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lawg;->c:I

    iget v1, p0, Lawg;->b:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lawg;->b:I

    iget v1, p0, Lawg;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lawg;->a:[B

    array-length v0, v0

    iget v1, p0, Lawg;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, Lawg;->b:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    iget-boolean v0, p0, Lawg;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lawg;->b:I

    iget v1, p0, Lawg;->c:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lawg;->c:I

    iget v1, p0, Lawg;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lawg;->a:[B

    array-length v0, v0

    iget v1, p0, Lawg;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lawg;->c:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget-boolean v0, p0, Lawg;->e:Z

    if-nez v0, :cond_0

    iget v0, p0, Lawg;->b:I

    iget v1, p0, Lawg;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final f()B
    .locals 2

    iget-object v0, p0, Lawg;->a:[B

    iget v1, p0, Lawg;->b:I

    aget-byte v0, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lawg;->a(I)V

    return v0
.end method
