.class public final Lbuh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lbum;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lbuh;->a:Ljava/util/Map;

    const-string v1, "www.googleapis.com"

    const-string v2, "oauth"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbum;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbuh;->b:Landroid/content/Context;

    iput-object p2, p0, Lbuh;->c:Lbum;

    return-void
.end method

.method private b(Lbsp;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 4

    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bearer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Lbmx;

    invoke-virtual {p1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-direct {v2, v3}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iget-object v3, p0, Lbuh;->b:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lbmx;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbuh;->c:Lbum;

    invoke-interface {v0, p2}, Lbum;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lbsp;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 3

    :try_start_0
    invoke-direct {p0, p1, p2}, Lbuh;->b(Lbsp;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0x191

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lbuh;->c:Lbum;

    invoke-interface {v0}, Lbum;->c()V

    iget-object v0, p0, Lbuh;->c:Lbum;

    invoke-interface {v0}, Lbum;->b()V

    invoke-direct {p0, p1, p2}, Lbuh;->b(Lbsp;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lbuh;->c:Lbum;

    invoke-interface {v0}, Lbum;->b()V

    return-void
.end method

.method public final a(Lorg/apache/http/HttpRequest;)V
    .locals 1

    iget-object v0, p0, Lbuh;->c:Lbum;

    invoke-interface {v0, p1}, Lbum;->a(Lorg/apache/http/HttpRequest;)V

    return-void
.end method
