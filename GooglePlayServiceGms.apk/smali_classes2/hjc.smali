.class final Lhjc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/LinkedList;

.field b:Ljava/util/HashMap;

.field c:Ljava/util/HashMap;

.field d:J

.field final e:I

.field final f:I

.field final g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhjc;->a:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhjc;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhjc;->c:Ljava/util/HashMap;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhjc;->d:J

    const/16 v0, 0x64

    iput v0, p0, Lhjc;->e:I

    const/16 v0, 0x2ee0

    iput v0, p0, Lhjc;->f:I

    const v0, 0xea60

    iput v0, p0, Lhjc;->g:I

    return-void
.end method


# virtual methods
.method public final a()Lidr;
    .locals 2

    iget-object v0, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhjc;->a:Ljava/util/LinkedList;

    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    goto :goto_0
.end method

.method public final a(ZJJZLidr;)Livi;
    .locals 11

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lidr;

    invoke-interface {v1}, Lidr;->f()J

    move-result-wide v3

    cmp-long v1, v3, p4

    if-ltz v1, :cond_0

    move v10, v2

    :goto_1
    const/4 v1, -0x1

    if-ne v10, v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    return-object v1

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    move v10, v2

    goto :goto_1

    :cond_2
    new-instance v9, Livi;

    sget-object v1, Lihj;->Q:Livk;

    invoke-direct {v9, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x0

    move v8, v1

    :goto_3
    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v8, v1, :cond_9

    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lidr;

    iget-object v1, p0, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhtf;

    iget-object v1, p0, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhuv;

    if-ge v8, v10, :cond_3

    const/16 v7, 0x11

    :goto_4
    move-wide v1, p2

    move/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lhin;->a(JLidr;Lhtf;Lhuv;ZI)Livi;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v9, v2, v1}, Livi;->a(ILivi;)V

    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_3

    :cond_3
    if-ne v8, v10, :cond_5

    if-eqz p1, :cond_4

    const/16 v7, 0x13

    goto :goto_4

    :cond_4
    const/4 v7, 0x6

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v8, v1, :cond_8

    if-nez p7, :cond_6

    const/16 v7, 0x9

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    iget-object v2, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lidr;

    const/16 v2, 0x19

    move-object/from16 v0, p7

    invoke-static {v1, v0, v2}, Lhin;->a(Lidr;Lidr;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v7, 0xa

    goto :goto_4

    :cond_7
    const/16 v7, 0x9

    goto :goto_4

    :cond_8
    const/16 v7, 0x8

    goto :goto_4

    :cond_9
    move-object v1, v9

    goto :goto_2
.end method

.method final a(I)V
    .locals 7

    if-ltz p1, :cond_0

    iget-object v0, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    iget-object v1, p0, Lhjc;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    iget-object v1, p0, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "BurstCollector"

    const-string v2, "Removed location@ %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Lidr;->f()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(JLidr;Lhiq;Lhtf;Lhuv;)Z
    .locals 7

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz p6, :cond_7

    invoke-static {p6, p3}, Lhin;->a(Lhuv;Lidr;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lhjc;->d:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x2ee0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_7

    iget-object v0, p0, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p6, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_6

    const-wide/32 v4, 0x927c0

    move-object v0, p4

    move-object v1, p6

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lhiq;->a(Lhuv;JJ)I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p6, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    const-wide v2, 0x3fd3333333333333L    # 0.3

    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    if-eqz p5, :cond_8

    iget-object v1, p0, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-wide v1, p0, Lhjc;->d:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x2ee0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_8

    if-nez v0, :cond_1

    invoke-virtual {p4, p5, p3}, Lhiq;->a(Lhtf;Lidr;)Z

    move-result v1

    if-nez v1, :cond_8

    :cond_1
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_9

    :goto_4
    if-eqz v0, :cond_a

    :goto_5
    if-eqz p5, :cond_2

    iget-object v1, p0, Lhjc;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p3, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "BurstCollector"

    const-string v2, "Added cell scan @%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p5}, Lhtf;->f()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz p6, :cond_3

    iget-object v1, p0, Lhjc;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p3, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "BurstCollector"

    const-string v2, "Added wifi scan @%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, p6, Lhuv;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-nez p6, :cond_4

    if-eqz p5, :cond_5

    :cond_4
    iput-wide p1, p0, Lhjc;->d:J

    :cond_5
    invoke-virtual {p4, p3, p5, p6}, Lhiq;->a(Lidr;Lhtf;Lhuv;)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    const/4 p5, 0x0

    goto :goto_4

    :cond_a
    const/4 p6, 0x0

    goto :goto_5
.end method
