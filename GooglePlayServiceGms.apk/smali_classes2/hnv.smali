.class final Lhnv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lhuo;

.field final synthetic b:Livi;

.field final synthetic c:Ljava/lang/Object;

.field final synthetic d:Lhnt;


# direct methods
.method constructor <init>(Lhnt;Lhuo;Livi;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lhnv;->d:Lhnt;

    iput-object p2, p0, Lhnv;->a:Lhuo;

    iput-object p3, p0, Lhnv;->b:Livi;

    iput-object p4, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "DiskTemporalCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lhnv;->d:Lhnt;

    iget-object v2, v2, Lhnt;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deleted, re-creating..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v1, v0, Lhnt;->a:Ljava/io/File;

    iget-object v0, p0, Lhnv;->a:Lhuo;

    iget-object v0, v0, Lhuo;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->c:Lidq;

    invoke-interface {v0, v6}, Lidq;->a(Ljava/io/File;)V

    new-instance v0, Lidx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lhnv;->d:Lhnt;

    iget-object v4, v4, Lhnt;->b:[B

    iget-object v5, p0, Lhnv;->d:Lhnt;

    iget-object v5, v5, Lhnt;->f:Lhuj;

    iget-object v5, v5, Lhuj;->a:Livk;

    sget-object v7, Lidx;->a:Lidz;

    iget-object v8, p0, Lhnv;->d:Lhnt;

    iget-object v8, v8, Lhnt;->c:Lidq;

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    iget-object v1, p0, Lhnv;->b:Livi;

    invoke-virtual {v0, v1}, Lidx;->b(Livi;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "DiskTemporalCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully wrote element "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to disk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_3

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The file that we were trying to write was not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v1, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhox;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->e:Lhnw;

    iget-object v1, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhnw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_4

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException while writing proto to disk: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0

    :try_start_2
    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v1, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhox;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhnv;->d:Lhnt;

    iget-object v0, v0, Lhnt;->e:Lhnw;

    iget-object v1, p0, Lhnv;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhnw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
