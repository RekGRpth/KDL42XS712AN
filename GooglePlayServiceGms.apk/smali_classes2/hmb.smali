.class public final Lhmb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhlu;

.field private final b:Ljava/util/Map;

.field private c:Ljava/lang/String;

.field private d:J


# direct methods
.method constructor <init>(Lhlu;)V
    .locals 2

    iput-object p1, p0, Lhmb;->a:Lhlu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhmb;->b:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lhmb;->c:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhmb;->d:J

    return-void
.end method


# virtual methods
.method final a(Lhlz;J)V
    .locals 5

    iget-object v0, p0, Lhmb;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhmb;->b:Ljava/util/Map;

    iget-object v1, p0, Lhmb;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhma;

    iget-wide v1, p0, Lhmb;->d:J

    sub-long v1, p2, v1

    iget-wide v3, v0, Lhma;->a:J

    add-long/2addr v1, v3

    iput-wide v1, v0, Lhma;->a:J

    iget v1, v0, Lhma;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lhma;->b:I

    :cond_0
    iget-object v0, p0, Lhmb;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lhlz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhmb;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lhlz;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lhma;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lhma;-><init>(B)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {p1}, Lhlz;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhmb;->c:Ljava/lang/String;

    iput-wide p2, p0, Lhmb;->d:J

    return-void
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;J)V
    .locals 12

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhmb;->a:Lhlu;

    iget-object v1, v1, Lhlu;->d:Lhlz;

    invoke-virtual {v1}, Lhlz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lhmb;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhma;

    iget-wide v2, v1, Lhma;->a:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    iget v1, v1, Lhma;->b:I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lhmb;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v6, p0, Lhmb;->d:J

    sub-long v6, p3, v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    add-long/2addr v2, v6

    add-int/lit8 v0, v1, 0x1

    move-wide v1, v2

    :goto_1
    const-string v3, " TimeInState: "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "sec"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " StateEnteredCount: "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " Avg: "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    int-to-long v6, v0

    div-long v0, v1, v6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sec/entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move v0, v1

    move-wide v10, v2

    move-wide v1, v10

    goto :goto_1
.end method
