.class public final Lcjc;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 3

    new-instance v1, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v1}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    invoke-static {v0}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcjd;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/internal/model/File;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a(Lcfp;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    invoke-static {}, Lckw;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    invoke-static {v0}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcjd;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    invoke-static {v0}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcjd;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-virtual {v0, p1, p0}, Lcjd;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcfp;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 6

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lckw;->a(Ljava/lang/String;)Lcje;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcjd;->a(Lorg/json/JSONObject;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    goto :goto_0

    :cond_0
    const-string v3, "MetadataBufferConversion"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Ignored unknown metadata field in JSON: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;
    .locals 3

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    invoke-static {v0}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcjd;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lorg/json/JSONObject;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method
