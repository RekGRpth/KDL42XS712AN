.class public final Lihs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Liht;

.field private final b:Lijb;

.field private final c:Lihn;

.field private final d:Lijx;

.field private final e:Lcom/google/android/location/reporting/StateReporter;

.field private final f:Lcom/google/android/location/reporting/LocationReportingController;

.field private final g:Lbpe;

.field private h:I

.field private final i:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lijb;Lihn;Lijx;Liht;Lcom/google/android/location/reporting/StateReporter;Lcom/google/android/location/reporting/LocationReportingController;ILbpe;Landroid/net/wifi/WifiManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lihs;->h:I

    iput-object p1, p0, Lihs;->b:Lijb;

    iput-object p2, p0, Lihs;->c:Lihn;

    iput-object p3, p0, Lihs;->d:Lijx;

    iput-object p4, p0, Lihs;->a:Liht;

    iput-object p5, p0, Lihs;->e:Lcom/google/android/location/reporting/StateReporter;

    iput-object p6, p0, Lihs;->f:Lcom/google/android/location/reporting/LocationReportingController;

    iput p7, p0, Lihs;->h:I

    iput-object p8, p0, Lihs;->g:Lbpe;

    iput-object p9, p0, Lihs;->i:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/LocationReportingController;Lbpe;)V
    .locals 3

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Location reporting no longer active, stopping; reasons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingConfig;->getInactiveReasonsString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p2}, Lbpe;->a()J

    move-result-wide v0

    invoke-virtual {p1, p0, v0, v1}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/service/ReportingConfig;J)V

    return-void
.end method

.method private a(Liif;Landroid/location/Location;)V
    .locals 12

    :try_start_0
    iget-object v0, p0, Lihs;->b:Lijb;

    invoke-virtual {v0}, Lijb;->e()J

    move-result-wide v2

    iget-object v0, p0, Lihs;->g:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v4

    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    sget-object v0, Lijs;->k:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, v2

    cmp-long v0, v4, v0

    if-ltz v0, :cond_7

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {p2}, Lilk;->g(Landroid/location/Location;)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_10

    if-eqz v0, :cond_10

    sget-object v0, Lijs;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    const-string v1, "Adding AP connectivity to wifi scan"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lihs;->i:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lihs;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_5

    invoke-static {v0}, Lhuw;->a(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v9

    const/4 v1, 0x0

    iget-object v0, p0, Lihs;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lihs;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v11, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v11, v9}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v0, 0x1

    :goto_4
    move v1, v0

    :cond_3
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liih;

    iget-wide v10, v0, Liih;->b:J

    cmp-long v10, v10, v7

    if-nez v10, :cond_4

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Liih;->a(Z)Liih;

    invoke-virtual {v0, v1}, Liih;->b(I)Liih;

    const-string v0, "GCoreUlr"

    const/4 v7, 0x3

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isConnected=true, wifiAuth: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liih;

    invoke-virtual {p1, v0}, Liif;->a(Liih;)Liif;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const-string v2, "Best-effort Wifi scan attachment failed"

    invoke-static {v1, v2, v0}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_6
    :goto_6
    return-void

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_a
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_3

    :cond_b
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x2

    goto :goto_4

    :cond_c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x3

    goto :goto_4

    :cond_d
    const/4 v0, 0x4

    goto :goto_4

    :cond_e
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Attached wifi. Last wifi scan attached timestamp: "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " location reports. Expected every "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lijs;->k:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millis."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    iget-object v0, p0, Lihs;->b:Lijb;

    invoke-virtual {v0, v4, v5}, Lijb;->b(J)V

    goto :goto_6

    :cond_10
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v1, "GCoreUlr"

    const-string v7, "Skipped attaching wifi. wifiScan == null: %s, elapsedRealtime: %d, lastWifiAttachedRealtime: %d, Expected every: %d millis"

    const/4 v0, 0x4

    new-array v8, v0, [Ljava/lang/Object;

    const/4 v9, 0x0

    if-nez v6, :cond_11

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v0

    const/4 v0, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v0

    const/4 v0, 0x3

    sget-object v2, Lijs;->k:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v8, v0

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_6

    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static b(Landroid/content/Intent;)Z
    .locals 5

    const-string v0, "providerEnabled"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Location provider enable status is now "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "providerEnabled"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 12

    const/4 v11, 0x5

    const-wide v9, 0x416312d000000000L    # 1.0E7

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "LocationReceiver received "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; lowPowerMode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lhkr;->a()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; mState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lihs;->b:Lijb;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1}, Lihs;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lihs;->e:Lcom/google/android/location/reporting/StateReporter;

    invoke-interface {v0}, Lcom/google/android/location/reporting/StateReporter;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/reporting/service/ReportingConfig;->isReportingActive()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lihs;->f:Lcom/google/android/location/reporting/LocationReportingController;

    iget-object v1, p0, Lihs;->g:Lbpe;

    invoke-static {v5, v0, v1}, Lihs;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/LocationReportingController;Lbpe;)V

    goto :goto_0

    :cond_3
    const-string v0, "com.google.android.location.LOCATION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    if-nez v0, :cond_4

    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    const-string v1, "Received null location, so returning early."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lihs;->b:Lijb;

    invoke-virtual {v4}, Lijb;->b()V

    iget-object v4, p0, Lihs;->b:Lijb;

    invoke-virtual {v4}, Lijb;->c()Landroid/location/Location;

    move-result-object v4

    invoke-static {v5}, Likf;->a(Lcom/google/android/location/reporting/service/ReportingConfig;)V

    new-instance v6, Liig;

    invoke-direct {v6}, Liig;-><init>()V

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    mul-double/2addr v7, v9

    double-to-int v7, v7

    invoke-virtual {v6, v7}, Liig;->b(I)Liig;

    move-result-object v6

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    mul-double/2addr v7, v9

    double-to-int v7, v7

    invoke-virtual {v6, v7}, Liig;->a(I)Liig;

    move-result-object v6

    invoke-static {v0}, Lilk;->d(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Liif;

    invoke-direct {v8}, Liif;-><init>()V

    invoke-virtual {v8, v6}, Liif;->a(Liig;)Liif;

    move-result-object v6

    const-string v8, "cell"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    move v1, v2

    :cond_5
    :goto_1
    invoke-virtual {v6, v1}, Liif;->a(I)Liif;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Liif;->a(J)Liif;

    move-result-object v6

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v1

    invoke-virtual {v6, v1}, Liif;->a(F)Liif;

    :cond_6
    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v1

    invoke-virtual {v6, v1}, Liif;->b(F)Liif;

    :cond_7
    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Liif;->a(D)Liif;

    :cond_8
    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v6, v1}, Liif;->c(F)Liif;

    :cond_9
    invoke-direct {p0, v6, v0}, Lihs;->a(Liif;Landroid/location/Location;)V

    invoke-static {v0}, Lilk;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v6, v1}, Liif;->a(Ljava/lang/String;)Liif;

    :cond_a
    invoke-static {v0}, Lilk;->b(Landroid/location/Location;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v6, v1}, Liif;->d(I)Liif;

    :cond_b
    iget v1, p0, Lihs;->h:I

    const/4 v7, -0x1

    if-eq v1, v7, :cond_c

    iget v1, p0, Lihs;->h:I

    invoke-virtual {v6, v1}, Liif;->b(I)Liif;

    :cond_c
    iget-object v1, p0, Lihs;->d:Lijx;

    invoke-virtual {v1}, Lijx;->b()I

    move-result v1

    invoke-virtual {v6, v1}, Liif;->c(I)Liif;

    iget-object v1, p0, Lihs;->d:Lijx;

    invoke-virtual {v1}, Lijx;->a()Z

    move-result v1

    invoke-virtual {v6, v1}, Liif;->a(Z)Liif;

    invoke-static {v0, v4}, Lihz;->a(Landroid/location/Location;Landroid/location/Location;)F

    move-result v1

    const/4 v7, 0x0

    cmpg-float v7, v1, v7

    if-gtz v7, :cond_10

    move v1, v2

    :goto_2
    invoke-virtual {v6, v1}, Liif;->b(Z)Liif;

    invoke-virtual {v5}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/reporting/service/AccountConfig;

    :try_start_0
    iget-object v4, p0, Lihs;->c:Lihn;

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v9

    invoke-virtual {v4, v8, v6, v9}, Lihn;->saveEntity(Landroid/accounts/Account;Ljava/lang/Object;Z)Z
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_d

    move v3, v2

    goto :goto_3

    :cond_e
    const-string v8, "wifi"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    move v1, v3

    goto/16 :goto_1

    :cond_f
    const-string v8, "gps"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v1, 0x2

    goto/16 :goto_1

    :cond_10
    invoke-virtual {v4, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v4

    cmpg-float v1, v4, v1

    if-lez v1, :cond_11

    sget-object v1, Lijs;->o:Lbfy;

    invoke-virtual {v1}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v4, v1

    if-gtz v1, :cond_12

    :cond_11
    move v1, v2

    goto :goto_2

    :cond_12
    move v1, v3

    goto :goto_2

    :catch_0
    move-exception v4

    const-string v8, "GCoreUlr"

    invoke-static {v8, v11}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_d

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "GCoreUlr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Error saving location to database for "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1, v4}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_13
    if-eqz v3, :cond_16

    const-string v1, "GCoreUlr"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v1, "GCoreUlr"

    const-string v2, "Successfully inserted location"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    iget-object v1, p0, Lihs;->b:Lijb;

    invoke-virtual {v1, v0}, Lijb;->a(Landroid/location/Location;)V

    :cond_15
    :goto_4
    iget-object v1, p0, Lihs;->a:Liht;

    iget-object v2, p0, Lihs;->b:Lijb;

    invoke-virtual {v1, v2, v5, v0}, Liht;->a(Lijb;Lcom/google/android/location/reporting/service/ReportingConfig;Landroid/location/Location;)Z

    goto/16 :goto_0

    :cond_16
    const-string v1, "GCoreUlr"

    invoke-static {v1, v11}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v1, "GCoreUlr"

    const-string v2, "Location insertion failed for all accounts"

    invoke-static {v1, v2}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method
