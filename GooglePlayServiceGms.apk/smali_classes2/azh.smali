.class public final Lazh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lazg;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V
    .locals 0

    iput-object p1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Laye;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceFound, device="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Layl;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/media/remotedisplay/RemoteDisplay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setDescription(Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    iget-object v1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->addDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;I)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Laye;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceStartedMirroring, device="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    invoke-virtual {v0, p2}, Lcom/android/media/remotedisplay/RemoteDisplay;->setPresentationDisplayId(I)V

    iget-object v1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Laye;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceStoppedMirroring, device="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b030a    # com.google.android.gms.R.string.cast_display_notification_connection_failed_message

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Layl;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setPresentationDisplayId(I)V

    iget-object v1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Laye;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceLost, device="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->removeDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    iget-object v0, p0, Lazh;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    :cond_0
    return-void
.end method
