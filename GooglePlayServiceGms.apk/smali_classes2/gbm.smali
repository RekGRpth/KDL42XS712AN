.class public abstract Lgbm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field final a:Lcom/google/android/gms/common/server/ClientContext;

.field final b:Lfsy;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbm;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgbm;->b:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lgbm;->b(Landroid/content/Context;Lfrx;)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lbbo;

    invoke-virtual {v0}, Lbbo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "pendingIntent"

    invoke-virtual {v0}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_0
    iget-object v4, p0, Lgbm;->b:Lfsy;

    invoke-virtual {v0}, Lbbo;->c()I

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    invoke-interface {v4, v5, v1, v0}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v3, "pendingIntent"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgbm;->b:Lfsy;

    invoke-interface {v0, v7, v1, v2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgbm;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgbm;->b:Lfsy;

    invoke-interface {v1, v7, v0, v2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgbm;->b:Lfsy;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v2, v2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgbm;->b:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbm;->b:Lfsy;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lfsy;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public abstract b(Landroid/content/Context;Lfrx;)Landroid/util/Pair;
.end method
