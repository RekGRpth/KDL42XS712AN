.class public final Lfbh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[S

.field c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    const/16 v9, 0x5200

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    const v6, 0x7f080004    # com.google.android.gms.R.raw.pinyins

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :try_start_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-array v1, v6, [Ljava/lang/String;

    iput-object v1, p0, Lfbh;->a:[Ljava/lang/String;

    move v1, v0

    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    if-eq v1, v6, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Mismatched counts."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PeoplePinyin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed or missing input files for HanziPinyin."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lfdk;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfbh;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lfbh;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lfbh;->b:[S
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_0
    :try_start_2
    iget-object v8, p0, Lfbh;->a:[Ljava/lang/String;

    aput-object v7, v8, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    new-instance v5, Ljava/io/BufferedInputStream;

    const/high16 v1, 0x7f080000    # com.google.android.gms.R.raw.indexes

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const v1, 0xa400

    :try_start_3
    new-array v4, v1, [B

    move v1, v0

    :cond_2
    array-length v6, v4

    sub-int/2addr v6, v1

    invoke-virtual {v5, v4, v1, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    add-int/2addr v1, v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    add-int/2addr v6, v1

    array-length v7, v4

    if-ne v6, v7, :cond_2

    :cond_3
    const/16 v1, 0x5200

    new-array v1, v1, [S

    iput-object v1, p0, Lfbh;->b:[S

    :goto_2
    if-ge v0, v9, :cond_6

    mul-int/lit8 v1, v0, 0x2

    aget-byte v1, v4, v1

    int-to-short v1, v1

    shl-int/lit8 v1, v1, 0x8

    int-to-short v1, v1

    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x1

    aget-byte v6, v4, v6

    int-to-short v6, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v1, v6

    int-to-short v1, v1

    if-ltz v1, :cond_4

    iget-object v6, p0, Lfbh;->a:[Ljava/lang/String;

    array-length v6, v6

    if-lt v1, v6, :cond_5

    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid character to pinyin index: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "PeoplePinyin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed or missing indexes.txt for HanziPinyin."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lfdk;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfbh;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lfbh;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lfbh;->b:[S
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_5
    :try_start_5
    iget-object v6, p0, Lfbh;->b:[S

    aput-short v1, v6, v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "PeoplePinyin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "HanziToPinyin initialization took "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfbh;->c:Z

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    invoke-static {v5}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method static a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfbi;

    invoke-direct {v1, p2, v0, v0}, Lfbi;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    return-void
.end method
