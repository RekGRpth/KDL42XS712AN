.class public final enum Ljcq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ljcq;

.field public static final enum b:Ljcq;

.field private static final synthetic e:[Ljcq;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Ljcq;

    const-string v1, "NONE"

    const-string v2, "InvalidDoNotUseForJCA"

    invoke-direct {v0, v1, v4, v3, v2}, Ljcq;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Ljcq;->a:Ljcq;

    new-instance v0, Ljcq;

    const-string v1, "AES_256_CBC"

    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-direct {v0, v1, v3, v5, v2}, Ljcq;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Ljcq;->b:Ljcq;

    new-array v0, v5, [Ljcq;

    sget-object v1, Ljcq;->a:Ljcq;

    aput-object v1, v0, v4

    sget-object v1, Ljcq;->b:Ljcq;

    aput-object v1, v0, v3

    sput-object v0, Ljcq;->e:[Ljcq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Ljcq;->c:I

    iput-object p4, p0, Ljcq;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ljcq;
    .locals 1

    const-class v0, Ljcq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljcq;

    return-object v0
.end method

.method public static values()[Ljcq;
    .locals 1

    sget-object v0, Ljcq;->e:[Ljcq;

    invoke-virtual {v0}, [Ljcq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljcq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljcq;->c:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcq;->d:Ljava/lang/String;

    return-object v0
.end method
