.class public final enum Lcex;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcex;

.field public static final enum b:Lcex;

.field public static final enum c:Lcex;

.field public static final enum d:Lcex;

.field public static final enum e:Lcex;

.field private static final synthetic g:[Lcex;


# instance fields
.field private final f:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    new-instance v0, Lcex;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcew;->d()Lcew;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "entryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcex;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcex;->a:Lcex;

    new-instance v0, Lcex;

    const-string v1, "EVENT_TYPE"

    invoke-static {}, Lcew;->d()Lcew;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "eventType"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcex;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcex;->b:Lcex;

    new-instance v0, Lcex;

    const-string v1, "PACKAGE_NAME"

    invoke-static {}, Lcew;->d()Lcew;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "packageName"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcex;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcex;->c:Lcex;

    new-instance v0, Lcex;

    const-string v1, "EVENT_SERVICE"

    invoke-static {}, Lcew;->d()Lcew;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "eventService"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcex;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcex;->d:Lcex;

    new-instance v0, Lcex;

    const-string v1, "SIGNING_CERTIFICIATE_HASH"

    invoke-static {}, Lcew;->d()Lcew;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "signingCertificateHash"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    iput-boolean v6, v3, Lcei;->g:Z

    new-array v4, v10, [Ljava/lang/String;

    sget-object v5, Lcex;->a:Lcex;

    iget-object v5, v5, Lcex;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    sget-object v5, Lcex;->b:Lcex;

    iget-object v5, v5, Lcex;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    sget-object v5, Lcex;->c:Lcex;

    iget-object v5, v5, Lcex;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Lcex;->d:Lcex;

    iget-object v5, v5, Lcex;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcex;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcex;->e:Lcex;

    const/4 v0, 0x5

    new-array v0, v0, [Lcex;

    sget-object v1, Lcex;->a:Lcex;

    aput-object v1, v0, v8

    sget-object v1, Lcex;->b:Lcex;

    aput-object v1, v0, v6

    sget-object v1, Lcex;->c:Lcex;

    aput-object v1, v0, v7

    sget-object v1, Lcex;->d:Lcex;

    aput-object v1, v0, v9

    sget-object v1, Lcex;->e:Lcex;

    aput-object v1, v0, v10

    sput-object v0, Lcex;->g:[Lcex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcex;->f:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcex;
    .locals 1

    const-class v0, Lcex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcex;

    return-object v0
.end method

.method public static values()[Lcex;
    .locals 1

    sget-object v0, Lcex;->g:[Lcex;

    invoke-virtual {v0}, [Lcex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcex;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcex;->f:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcex;->f:Lcdp;

    return-object v0
.end method
