.class public final Lbry;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/GetMetadataRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbry;->c:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbry;->c:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    const-string v1, "Invalid get metadata request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbry;->c:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/GetMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid get metadata request: no id"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbry;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnMetadataResponse;

    iget-object v2, p0, Lbry;->b:Lbrc;

    iget-object v3, p0, Lbry;->c:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/GetMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v2, v3}, Lbrc;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/internal/OnMetadataResponse;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-interface {v0, v1}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnMetadataResponse;)V

    return-void
.end method
