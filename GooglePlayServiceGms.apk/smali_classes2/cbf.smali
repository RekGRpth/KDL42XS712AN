.class public final Lcbf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcov;


# instance fields
.field private final a:Lcon;

.field private b:I

.field private c:J

.field private d:J

.field private e:I

.field private final f:Lbfy;

.field private final g:Lbfy;


# direct methods
.method public constructor <init>(Lcon;Lbfy;Lbfy;)V
    .locals 7

    const/4 v4, 0x0

    invoke-interface {p1}, Lcon;->a()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcbf;-><init>(Lcon;Lbfy;Lbfy;IJ)V

    return-void
.end method

.method public constructor <init>(Lcon;Lbfy;Lbfy;IJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcon;

    iput-object v0, p0, Lcbf;->a:Lcon;

    iput-object p2, p0, Lcbf;->f:Lbfy;

    iput-object p3, p0, Lcbf;->g:Lbfy;

    invoke-virtual {p2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcbf;->b:I

    invoke-virtual {p3}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcbf;->c:J

    iput p4, p0, Lcbf;->e:I

    iput-wide p5, p0, Lcbf;->d:J

    return-void
.end method

.method private declared-synchronized a(J)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcbf;->d:J

    sub-long v0, p1, v0

    iget-wide v2, p0, Lcbf;->c:J

    neg-long v2, v2

    iget v4, p0, Lcbf;->b:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    iget-wide v0, p0, Lcbf;->c:J

    iget v2, p0, Lcbf;->b:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcbf;->d:J

    iget-wide v0, p0, Lcbf;->c:J

    neg-long v0, v0

    :cond_0
    iget-wide v2, p0, Lcbf;->c:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    iget v2, p0, Lcbf;->e:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcbf;->c:J

    div-long v4, v0, v4

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lcbf;->e:I

    iget-wide v2, p0, Lcbf;->c:J

    rem-long/2addr v0, v2

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcbf;->d:J

    iget v0, p0, Lcbf;->e:I

    iget v1, p0, Lcbf;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcbf;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()J
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbf;->a:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcbf;->a(J)V

    iget v2, p0, Lcbf;->e:I

    if-lez v2, :cond_0

    iget v0, p0, Lcbf;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcbf;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, 0x0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v2, p0, Lcbf;->d:J

    iget-wide v4, p0, Lcbf;->c:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcbf;->d:J

    iget-wide v2, p0, Lcbf;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v0, v2, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcbf;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcbf;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcbf;->a:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcbf;->a(J)V

    iget v0, p0, Lcbf;->e:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget v1, p0, Lcbf;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcbf;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcbf;->g()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    iget-wide v1, p0, Lcbf;->d:J

    iget-wide v3, p0, Lcbf;->c:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcbf;->d:J

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized f()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcbf;->e:I

    iget-object v0, p0, Lcbf;->a:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcbf;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
