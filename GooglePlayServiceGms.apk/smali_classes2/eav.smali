.class public final Leav;
.super Ldxh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/google/android/gms/games/Player;

.field private final c:I

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:Ledl;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/Player;ILandroid/view/View$OnClickListener;Ledl;)V
    .locals 1

    invoke-direct {p0}, Ldxh;-><init>()V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Leav;->a:Landroid/view/LayoutInflater;

    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Leav;->b:Lcom/google/android/gms/games/Player;

    iput p3, p0, Leav;->c:I

    iput-object p4, p0, Leav;->e:Landroid/view/View$OnClickListener;

    iput-object p5, p0, Leav;->f:Ledl;

    return-void
.end method

.method static synthetic a(Leav;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Leav;->e:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    const-string v1, "PubPlayerAddAdapter"

    const-string v2, "Player to mark as added is null. Let\'s do nothing in this case."

    invoke-static {v1, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lbex;->a(Landroid/content/Intent;)Lbey;

    move-result-object v1

    invoke-interface {v1}, Lbey;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Leav;)Lcom/google/android/gms/games/Player;
    .locals 1

    iget-object v0, p0, Leav;->b:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic c(Leav;)I
    .locals 1

    iget v0, p0, Leav;->c:I

    return v0
.end method

.method static synthetic d(Leav;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leav;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Leav;)Ledl;
    .locals 1

    iget-object v0, p0, Leav;->f:Ledl;

    return-object v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Leav;->a:Landroid/view/LayoutInflater;

    sget v1, Lxc;->u:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Leaw;

    invoke-direct {v0, p0, p2}, Leaw;-><init>(Leav;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaw;

    invoke-virtual {v0}, Leaw;->a()V

    return-object p2
.end method
