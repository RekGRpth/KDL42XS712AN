.class final Lhvz;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Iterable;

.field final synthetic b:Lhvx;


# direct methods
.method constructor <init>(Lhvx;Landroid/os/Looper;Ljava/lang/Iterable;)V
    .locals 0

    iput-object p1, p0, Lhvz;->b:Lhvx;

    iput-object p3, p0, Lhvz;->a:Ljava/lang/Iterable;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->b(Lhvx;)[Lhwa;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhwa;

    aput-object v0, v1, v2

    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->c(Lhvx;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->b(Lhvx;)[Lhwa;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->c(Lhvx;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->d(Lhvx;)[Ljava/lang/Iterable;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Iterable;

    :goto_1
    aput-object v0, v3, v4

    iget v0, p1, Landroid/os/Message;->arg2:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iget-object v1, p0, Lhvz;->b:Lhvx;

    iget-object v3, v1, Lhvx;->a:Lhvp;

    iget-object v1, p0, Lhvz;->b:Lhvx;

    invoke-static {v1}, Lhvx;->e(Lhvx;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhvz;->b:Lhvx;

    invoke-static {v1}, Lhvx;->d(Lhvx;)[Ljava/lang/Iterable;

    move-result-object v1

    aget-object v1, v1, v2

    :goto_3
    invoke-virtual {v3, v1, v0}, Lhvp;->a(Ljava/lang/Iterable;Z)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lhvx;->f()Ljava/lang/Iterable;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lhvz;->a:Ljava/lang/Iterable;

    goto :goto_3

    :pswitch_3
    iget-object v0, p0, Lhvz;->b:Lhvx;

    iget-object v0, v0, Lhvx;->a:Lhvp;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0xf4240

    mul-long/2addr v3, v5

    iget-wide v5, v0, Lhvp;->e:J

    sub-long/2addr v3, v5

    iget-object v5, v0, Lhvp;->c:Landroid/location/Location;

    if-eqz v5, :cond_4

    const-wide v5, 0x22ecb25c00L

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    :cond_4
    move v2, v1

    :cond_5
    iget-object v0, v0, Lhvp;->n:Lhwb;

    invoke-virtual {v0, v1, v2}, Lhwb;->a(ZZ)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lhvz;->b:Lhvx;

    iget-object v0, v0, Lhvx;->a:Lhvp;

    iget-object v0, v0, Lhvp;->n:Lhwb;

    invoke-virtual {v0, v2, v2}, Lhwb;->a(ZZ)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->f(Lhvx;)V

    :goto_4
    iget-object v0, p0, Lhvz;->b:Lhvx;

    iget-object v1, v0, Lhvx;->a:Lhvp;

    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->e(Lhvx;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->d(Lhvx;)[Ljava/lang/Iterable;

    move-result-object v0

    aget-object v0, v0, v2

    :goto_5
    invoke-virtual {v1, v0, v2}, Lhvp;->a(Ljava/lang/Iterable;Z)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->a(Lhvx;)V

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhvz;->a:Ljava/lang/Iterable;

    goto :goto_5

    :pswitch_6
    iget-object v0, p0, Lhvz;->b:Lhvx;

    invoke-static {v0}, Lhvx;->e(Lhvx;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-static {v0}, Lilk;->f(Landroid/location/Location;)V

    iget-object v1, p0, Lhvz;->b:Lhvx;

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {v1, v2}, Lhvx;->a(Lhvx;Landroid/location/Location;)Landroid/location/Location;

    iget-object v1, p0, Lhvz;->b:Lhvx;

    invoke-static {v1, v0}, Lhvx;->b(Lhvx;Landroid/location/Location;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lhvz;->b:Lhvx;

    iget-object v2, v2, Lhvx;->a:Lhvp;

    packed-switch v1, :pswitch_data_1

    const-string v0, "GCoreFlp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown injection type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_8
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    sget-object v1, Liwf;->d:Liwf;

    invoke-virtual {v2, v0, v1}, Lhvp;->a(Landroid/location/Location;Liwf;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method
