.class public final Lhyn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field private final c:Lbpe;

.field private d:J

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(Landroid/os/PowerManager;Lbpe;)V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyn;->a:Z

    iput-wide v1, p0, Lhyn;->d:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhyn;->e:Z

    iput-wide v1, p0, Lhyn;->f:J

    iput-wide v1, p0, Lhyn;->b:J

    iput-object p2, p0, Lhyn;->c:Lbpe;

    iget-object v0, p0, Lhyn;->c:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    iput-boolean v2, p0, Lhyn;->a:Z

    iput-wide v0, p0, Lhyn;->d:J

    iput-wide v0, p0, Lhyn;->f:J

    invoke-direct {p0}, Lhyn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhyn;->b:J

    return-void
.end method

.method private a()J
    .locals 4

    iget-wide v0, p0, Lhyn;->d:J

    iget-wide v2, p0, Lhyn;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method final a(Z)V
    .locals 2

    iput-boolean p1, p0, Lhyn;->a:Z

    iget-object v0, p0, Lhyn;->c:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhyn;->d:J

    invoke-direct {p0}, Lhyn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhyn;->b:J

    return-void
.end method

.method final b(Z)V
    .locals 2

    iput-boolean p1, p0, Lhyn;->e:Z

    iget-object v0, p0, Lhyn;->c:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhyn;->f:J

    invoke-direct {p0}, Lhyn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhyn;->b:J

    return-void
.end method
