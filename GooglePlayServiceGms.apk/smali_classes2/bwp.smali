.class final Lbwp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lbwj;

.field b:Ljava/util/concurrent/Future;

.field final synthetic c:Lbwo;

.field private final d:Lbsp;

.field private final e:Lcfp;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Lbwo;Lbsp;Lcfp;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lbwp;->c:Lbwo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lbwp;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lbwj;

    invoke-direct {v0}, Lbwj;-><init>()V

    iput-object v0, p0, Lbwp;->a:Lbwj;

    iput-object p2, p0, Lbwp;->d:Lbsp;

    iput-object p3, p0, Lbwp;->e:Lcfp;

    iput-object p4, p0, Lbwp;->f:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lbwp;)Lbsp;
    .locals 1

    iget-object v0, p0, Lbwp;->d:Lbsp;

    return-object v0
.end method

.method static synthetic b(Lbwp;)Lcfp;
    .locals 1

    iget-object v0, p0, Lbwp;->e:Lcfp;

    return-object v0
.end method


# virtual methods
.method public final a()Lbwm;
    .locals 1

    iget-object v0, p0, Lbwp;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    new-instance v0, Lbwm;

    invoke-direct {v0, p0}, Lbwm;-><init>(Lbwp;)V

    return-object v0
.end method

.method final b()V
    .locals 2

    iget-object v0, p0, Lbwp;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwp;->a:Lbwj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbwj;->a(I)V

    iget-object v0, p0, Lbwp;->b:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwp;->b:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public final run()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lbwp;->c:Lbwo;

    iget-object v0, p0, Lbwp;->e:Lcfp;

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    iget-object v5, p0, Lbwp;->f:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lbwp;->a:Lbwj;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lbwj;->a(I)V

    iget-object v0, v3, Lbwo;->d:Lcfz;

    iget-object v6, p0, Lbwp;->d:Lbsp;

    invoke-interface {v0, v6, v4}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v0, "FileDownloader"

    const-string v2, "File is no longer available or permission was denied: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-static {v0, v2, v5}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    iget-object v1, v3, Lbwo;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    iget-object v0, v3, Lbwo;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbwp;->a:Lbwj;

    invoke-virtual {v0, v11}, Lbwj;->a(I)V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_2
    invoke-virtual {v3, v6}, Lbwo;->a(Lcfp;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "FileDownloader"

    const-string v2, "Up-to-date file is already available locally: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-static {v0, v2, v6}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    iget-object v1, v3, Lbwo;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_3
    iget-object v0, v3, Lbwo;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lbwp;->a:Lbwj;

    invoke-virtual {v0, v11}, Lbwj;->a(I)V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_4
    iget-object v0, v3, Lbwo;->b:Lbwq;

    iget-object v0, v0, Lbwq;->b:Ldl;

    invoke-virtual {v6}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v7

    invoke-virtual {v0, v7}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbws;

    if-eqz v0, :cond_4

    iget-object v7, v0, Lbws;->b:Ljava/lang/String;

    invoke-virtual {v6}, Lcfp;->Z()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v0, v0, Lbws;->a:Lbsy;

    :goto_1
    iget-object v6, p0, Lbwp;->d:Lbsp;

    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    new-instance v8, Ljava/net/URI;

    invoke-direct {v8, v5}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    new-instance v5, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v5}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const v8, 0x1d4c0

    invoke-static {v5, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/4 v8, 0x0

    invoke-static {v5, v8}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-interface {v7, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->setParams(Lorg/apache/http/params/HttpParams;)V

    iget-object v5, v3, Lbwo;->a:Lbuh;

    invoke-virtual {v5, v7}, Lbuh;->a(Lorg/apache/http/HttpRequest;)V

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lbsy;->b()Lbsx;

    move-result-object v5

    invoke-virtual {v5}, Lbsx;->a()J

    move-result-wide v8

    const-string v5, "Range"

    new-instance v10, Lbtz;

    invoke-direct {v10, v8, v9}, Lbtz;-><init>(J)V

    invoke-virtual {v10}, Lbtz;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v5, v3, Lbwo;->a:Lbuh;

    invoke-virtual {v5, v6, v7}, Lbuh;->a(Lbsp;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v1

    :try_start_5
    invoke-virtual {v3, p0, v1, v0}, Lbwo;->a(Lbwp;Lorg/apache/http/HttpResponse;Lbsy;)V

    const-string v0, "FileDownloader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Download complete for file: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    iget-object v1, v3, Lbwo;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_6
    iget-object v0, v3, Lbwo;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    iget-object v0, p0, Lbwp;->a:Lbwj;

    invoke-virtual {v0, v11}, Lbwj;->a(I)V

    iget-object v0, v3, Lbwo;->a:Lbuh;

    invoke-virtual {v0}, Lbuh;->a()V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :goto_2
    :try_start_7
    const-string v2, "FileDownloader"

    const-string v5, "Error downloading: %"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {v2, v0, v5, v6}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    iget-object v2, v3, Lbwo;->c:Ljava/util/Map;

    monitor-enter v2

    :try_start_8
    iget-object v0, v3, Lbwo;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    iget-object v0, p0, Lbwp;->a:Lbwj;

    invoke-virtual {v0, v12}, Lbwj;->a(I)V

    if-eqz v1, :cond_0

    iget-object v0, v3, Lbwo;->a:Lbuh;

    invoke-virtual {v0}, Lbuh;->a()V

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_4
    move-exception v0

    :goto_3
    iget-object v2, v3, Lbwo;->c:Ljava/util/Map;

    monitor-enter v2

    :try_start_9
    iget-object v5, v3, Lbwo;->c:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    iget-object v2, p0, Lbwp;->a:Lbwj;

    invoke-virtual {v2, v12}, Lbwj;->a(I)V

    if-eqz v1, :cond_5

    iget-object v1, v3, Lbwo;->a:Lbuh;

    invoke-virtual {v1}, Lbuh;->a()V

    :cond_5
    throw v0

    :catchall_5
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_6
    move-exception v0

    move v1, v2

    goto :goto_3

    :catch_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method
