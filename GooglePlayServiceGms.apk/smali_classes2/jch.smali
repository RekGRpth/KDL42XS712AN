.class public final Ljch;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljcr;

.field private static final b:Ljcq;

.field private static final c:Ljcr;

.field private static final d:Ljcr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Ljcr;->a:Ljcr;

    sput-object v0, Ljch;->a:Ljcr;

    sget-object v0, Ljcq;->b:Ljcq;

    sput-object v0, Ljch;->b:Ljcq;

    sget-object v0, Ljcr;->b:Ljcr;

    sput-object v0, Ljch;->c:Ljcr;

    sget-object v0, Ljcr;->c:Ljcr;

    sput-object v0, Ljch;->d:Ljcr;

    return-void
.end method

.method public static a(Z)Ljava/security/KeyPair;
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {}, Ljcs;->c()Ljava/security/KeyPair;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljcs;->a()Ljava/security/KeyPair;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;
    .locals 2

    const-string v0, "ECDH"

    invoke-static {p0}, Ljci;->b(Ljava/security/PrivateKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "DH"

    :cond_0
    :try_start_0
    invoke-static {v0}, Ljavax/crypto/KeyAgreement;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyAgreement;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0, p0}, Ljavax/crypto/KeyAgreement;->init(Ljava/security/Key;)V

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Ljavax/crypto/KeyAgreement;->doPhase(Ljava/security/Key;Z)Ljava/security/Key;

    invoke-virtual {v0}, Ljavax/crypto/KeyAgreement;->generateSecret()[B

    move-result-object v0

    invoke-static {v0}, Ljch;->a([B)[B

    move-result-object v0

    invoke-static {v0}, Ljci;->a([B)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljavax/crypto/SecretKey;)[B
    .locals 1

    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, Ljch;->a([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljck;Ljavax/crypto/SecretKey;Ljava/security/PrivateKey;)[B
    .locals 6

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Ljck;->a:Lizf;

    invoke-virtual {v0}, Lizf;->b()[B

    move-result-object v0

    invoke-static {p1}, Ljch;->a(Ljavax/crypto/SecretKey;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DeviceMasterKeyHash not set correctly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {p2}, Ljci;->b(Ljava/security/PrivateKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Ljch;->d:Ljcr;

    :goto_0
    new-instance v1, Ljcw;

    invoke-direct {v1}, Ljcw;-><init>()V

    iget-object v2, p0, Ljck;->b:Lizf;

    invoke-virtual {v2}, Lizf;->b()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljcw;->b([B)Ljcw;

    move-result-object v1

    invoke-virtual {p0}, Ljck;->d()[B

    move-result-object v2

    if-eqz p2, :cond_3

    if-eqz v0, :cond_3

    if-nez v2, :cond_5

    :cond_3
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    sget-object v0, Ljch;->c:Ljcr;

    goto :goto_0

    :cond_5
    iget-object v3, v1, Ljcw;->a:Lizf;

    if-eqz v3, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set decryptionKeyId for a cleartext message"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    sget-object v3, Ljcq;->a:Ljcq;

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v3, v4}, Ljcw;->a(Ljcr;Ljcq;[B)Ljdc;

    move-result-object v3

    invoke-static {v3, v2}, Ljcw;->a(Ljdc;[B)[B

    move-result-object v2

    invoke-virtual {v1, p2, v0, v2}, Ljcw;->a(Ljava/security/Key;Ljcr;[B)Ljde;

    move-result-object v1

    new-instance v0, Ljcw;

    invoke-direct {v0}, Ljcw;-><init>()V

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-virtual {v0, v2}, Ljcw;->b([B)Ljcw;

    move-result-object v0

    new-instance v2, Ljcl;

    invoke-direct {v2}, Ljcl;-><init>()V

    sget-object v3, Ljco;->a:Ljco;

    invoke-virtual {v3}, Ljco;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljcl;->a(I)Ljcl;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljcl;->b(I)Ljcl;

    move-result-object v2

    invoke-virtual {v2}, Ljcl;->d()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljcw;->a([B)Ljcw;

    move-result-object v0

    sget-object v2, Ljch;->a:Ljcr;

    sget-object v4, Ljch;->b:Ljcq;

    invoke-virtual {v1}, Ljde;->d()[B

    move-result-object v5

    move-object v1, p1

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Ljcw;->a(Ljava/security/Key;Ljcr;Ljava/security/Key;Ljcq;[B)Ljde;

    move-result-object v0

    invoke-virtual {v0}, Ljde;->d()[B

    move-result-object v0

    return-object v0
.end method

.method private static a([B)[B
    .locals 2

    :try_start_0
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
