.class final Lcuf;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Ljava/util/Random;

.field private final c:Ldly;

.field private final d:Ldln;

.field private final e:Ldlz;

.field private final f:Ldlo;

.field private final g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;Lbmi;)V
    .locals 3

    const-string v0, "AchievementAgent"

    sget-object v1, Lcuf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcuf;->b:Ljava/util/Random;

    new-instance v0, Ldly;

    invoke-direct {v0, p2}, Ldly;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuf;->c:Ldly;

    new-instance v0, Ldln;

    invoke-direct {v0, p2}, Ldln;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuf;->d:Ldln;

    new-instance v0, Ldlz;

    invoke-direct {v0, p3}, Ldlz;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuf;->e:Ldlz;

    new-instance v0, Ldlo;

    invoke-direct {v0, p3}, Ldlo;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuf;->f:Ldlo;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcuf;->g:Ljava/util/HashMap;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 19

    invoke-static/range {p2 .. p2}, Ldiv;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v11

    const/4 v9, 0x0

    const/4 v7, 0x0

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    sget-object v3, Lcuj;->a:[Ljava/lang/String;

    const-string v4, "account_name=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v11, v5, v1

    const-string v6, "package_name"

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v13

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    move-object v10, v7

    :goto_0
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v1, 0x8

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v1, 0x6

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x5

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/4 v4, 0x3

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/4 v4, 0x4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-static {v1, v3, v11}, Lcum;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v6

    invoke-virtual {v14, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v3, Lcug;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v3 .. v8}, Lcug;-><init>(Lcuf;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v14, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcug;

    const/4 v5, 0x0

    const/4 v4, 0x0

    if-lez v18, :cond_1

    new-instance v4, Ldmy;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v4, v3}, Ldmy;-><init>(Ljava/lang/Integer;)V

    const-string v3, "SET_STEPS_AT_LEAST"

    :goto_1
    new-instance v7, Ldlw;

    invoke-direct {v7, v15, v5, v4, v3}, Ldlw;-><init>(Ljava/lang/String;Ldmx;Ldmy;Ljava/lang/String;)V

    iget-object v1, v1, Lcug;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v6, v10}, Lcom/google/android/gms/common/server/ClientContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    move v3, v1

    :goto_2
    invoke-virtual {v14, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcug;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    iget-object v1, v1, Lcug;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v10, v6

    goto/16 :goto_0

    :cond_1
    if-lez v17, :cond_2

    new-instance v5, Ldmx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcuf;->b:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v5, v3, v7}, Ldmx;-><init>(Ljava/lang/Long;Ljava/lang/Integer;)V

    const-string v3, "INCREMENT"

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_3

    const-string v3, "REVEAL"

    goto :goto_1

    :cond_3
    const-string v3, "UNLOCK"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    move v3, v1

    goto :goto_2

    :cond_5
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v9

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v14, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcug;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcuf;->a(Lcom/google/android/gms/common/server/ClientContext;Lcug;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v2, Lcug;->f:Ljava/util/ArrayList;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :catchall_0
    move-exception v1

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_6
    const/4 v1, 0x5

    move v3, v1

    goto :goto_3

    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "AchievementAgent"

    invoke-static {v1, v12, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    return v3
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)I
    .locals 14

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {p1, v0, v1}, Lcuf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {p0, p1, v0, v1, v2}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {p1, v0, v1}, Lcuf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t find local achievement to update for achievement ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez p6, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {p1, v0, v1, v2}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0xbb8

    :goto_0
    return v2

    :cond_1
    invoke-direct/range {p0 .. p5}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v8

    move-object/from16 v0, p2

    invoke-static {v0, v8, v9}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v10

    const/4 v2, -0x1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "state"

    aput-object v5, v3, v4

    invoke-static {p1, v10, v3}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v4

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v2, 0x0

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    move v3, v2

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    const/4 v2, -0x1

    if-ne v3, v2, :cond_2

    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No prior achievement state set for instance ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0xbb9

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_2
    sget-object v2, Lcwh;->m:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p6, :cond_3

    move/from16 v0, p6

    if-ne v0, v3, :cond_3

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {p1, v0, v1}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-static {p1, v0, v2}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move/from16 v0, p6

    if-eq v0, v3, :cond_5

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "state"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v11, v10, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {p1, v0, v1}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v13

    if-nez p6, :cond_6

    :try_start_1
    move-object/from16 v0, p7

    invoke-static {p1, v0, v13}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcuf;->c:Ldly;

    invoke-static/range {p5 .. p5}, Ldly;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Ldly;->a:Lbmi;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Ldlt;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    :goto_2
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {p0, v0, v1}, Lcuf;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_4

    :try_start_2
    iget-object v2, p0, Lcuf;->c:Ldly;

    invoke-static/range {p5 .. p5}, Ldly;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Ldly;->a:Lbmi;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Ldlr;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Ldlr;

    invoke-virtual {v2}, Ldlr;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lddy;->a(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    const-string v3, "state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v11, v10, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to update local instance for ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object/from16 v0, p7

    invoke-static {p1, v0, v13}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_0
    move-exception v2

    invoke-static {}, Ldac;->a()Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "AchievementAgent"

    invoke-static {v2, v3}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_8
    invoke-static {v2}, Lbng;->a(Lsp;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "AchievementAgent"

    const-string v3, "Encountered hard error while incrementing achievement."

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x6

    goto/16 :goto_0

    :cond_9
    const-string v2, "AchievementAgent"

    const-string v3, "Unable to update achievement. Update will be deferred."

    invoke-static {v2, v3}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v8, p6

    invoke-static/range {v2 .. v10}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    const/4 v2, 0x5

    goto/16 :goto_0

    :catch_1
    move-exception v2

    move-object v11, v2

    const-string v2, "AchievementAgent"

    const-string v3, "Auth error while updating achievement over network"

    invoke-static {v2, v3, v11}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v8, p6

    invoke-static/range {v2 .. v10}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    throw v11

    :cond_a
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to update local achievement instance for ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_b
    move v3, v2

    goto/16 :goto_1
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I
    .locals 10

    :try_start_0
    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_0

    iget-object v1, p0, Lcuf;->f:Ldlo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p3, v0, v2, v3}, Ldlo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Ldlo;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldlp;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldlp;

    :goto_0
    invoke-virtual {v0}, Ldlp;->getItems()Ljava/util/ArrayList;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v0, -0x1

    cmp-long v0, v3, v0

    if-nez v0, :cond_2

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No game found matching external game ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcuf;->d:Ldln;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Ldln;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Ldln;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldlp;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldlp;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/16 v0, 0x1f4

    goto :goto_2

    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p2, p3}, Ldit;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "external_achievement_id"

    invoke-static {p1, v0, v1}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v1, v0

    :goto_3
    if-ge v1, v7, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldlm;

    iget-object v0, v0, Lbni;->a:Landroid/content/ContentValues;

    const-string v8, "game_id"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "sorting_rank"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "external_achievement_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "is_revealed_icon_default"

    const-string v9, "revealed_icon_image_url"

    invoke-static {p1, v0, v8, v9}, Lcuf;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "is_unlocked_icon_default"

    const-string v9, "unlocked_icon_image_url"

    invoke-static {p1, v0, v8, v9}, Lcuf;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2, v0}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "AchievementAgent"

    invoke-static {v0, v5, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    :cond_5
    const/16 v0, 0x1f4

    goto/16 :goto_2
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 10

    const/4 v9, 0x1

    const/4 v5, 0x0

    const-wide/16 v7, -0x1

    invoke-static {p1, p2, p3, p4, p5}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v2, v0, v7

    if-nez v2, :cond_4

    const-string v0, "AchievementAgent"

    const-string v1, "forceResolveInstanceId did not find instance"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcuf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inserting a local stub for achievement instance for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", achievement "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2, p5}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find definition for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-static {p1, p2, p3, p4, p5}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v2, v0, v7

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find achievement instance to update; Game: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Achievement: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Player: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1, p2, p4}, Lcum;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v2, v7

    if-nez v0, :cond_2

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find record for player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "definition_id"

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "player_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "state"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v9, :cond_3

    const-string v0, "current_steps"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "formatted_current_steps"

    const-string v1, "0"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    :cond_4
    return-wide v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J
    .locals 13

    const-wide/16 v10, -0x1

    const/4 v9, -0x1

    const/4 v8, -0x1

    const/4 v7, -0x1

    invoke-static {p1}, Ldiv;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcuj;->a:[Ljava/lang/String;

    const-string v5, "external_achievement_id=?"

    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v6, v1

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v12

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_e

    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    :goto_0
    const/4 v1, 0x4

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x4

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    const/4 v7, -0x1

    if-eq v2, v7, :cond_0

    const/4 v7, -0x1

    if-ne v1, v7, :cond_3

    :cond_0
    const/4 v7, 0x1

    :goto_2
    const-string v8, "Both increment and set steps may not be positive"

    invoke-static {v7, v8}, Lbiq;->a(ZLjava/lang/Object;)V

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    cmp-long v8, v5, v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    move/from16 v0, p5

    if-eq v0, v8, :cond_4

    const/4 v7, 0x1

    :cond_1
    :goto_3
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "client_context_id"

    invoke-static {p0, p1}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "external_achievement_id"

    move-object/from16 v0, p4

    invoke-virtual {v8, v9, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "achievement_type"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "external_game_id"

    invoke-virtual {v8, v9, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "external_player_id"

    move-object/from16 v0, p3

    invoke-virtual {v8, v9, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v7, :cond_a

    const/4 v7, 0x1

    move/from16 v0, p5

    if-eq v0, v7, :cond_6

    const/4 v1, 0x1

    if-ne v4, v1, :cond_2

    if-nez p6, :cond_2

    const-string v1, "new_state"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    :goto_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    :goto_5
    return-wide v5

    :catchall_0
    move-exception v1

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    :cond_4
    if-lez p8, :cond_5

    const/4 v8, -0x1

    if-ne v2, v8, :cond_5

    const/4 v7, 0x1

    goto :goto_3

    :cond_5
    if-lez p7, :cond_1

    const/4 v8, -0x1

    if-ne v1, v8, :cond_1

    const/4 v7, 0x1

    goto :goto_3

    :cond_6
    if-lez v2, :cond_7

    const-string v1, "steps_to_increment"

    add-int v2, v2, p7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_4

    :cond_7
    if-lez v1, :cond_8

    const-string v2, "min_steps_to_set"

    move/from16 v0, p8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_4

    :cond_8
    const-string v1, "AchievementAgent"

    const-string v2, "Can\'t coalesce an incremental achievement op with no steps!"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v5, -0x1

    goto :goto_5

    :cond_9
    const-string v1, "AchievementAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to update existing pending op with ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v5, -0x1

    goto :goto_5

    :cond_a
    const/4 v1, 0x1

    move/from16 v0, p5

    if-eq v0, v1, :cond_b

    const-string v1, "new_state"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v3, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    goto :goto_5

    :cond_b
    if-lez p8, :cond_c

    const-string v1, "min_steps_to_set"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_6

    :cond_c
    const-string v1, "steps_to_increment"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_6

    :cond_d
    move v1, v7

    goto/16 :goto_1

    :cond_e
    move v2, v8

    goto/16 :goto_0

    :cond_f
    move v1, v7

    move v2, v8

    move v4, v9

    move-wide v5, v10

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lctz;

    invoke-static {p0, v0, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lctz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lctz;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lctz;->b(I)Lcty;

    move-result-object v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.google.android.gms.games.extra.name"

    invoke-interface {v2}, Lcty;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.extra.imageUri"

    invoke-interface {v2}, Lcty;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Lctz;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lctz;->b()V

    throw v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    if-nez p5, :cond_0

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p3, p4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcuf;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v0, v1, v3

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1, p2, p3, p6}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcuf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v5

    :cond_1
    invoke-static {p2, p4, p3}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "state,last_updated_timestamp DESC,sorting_rank"

    move-object v0, p1

    move-object v2, v6

    move-object v3, v6

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v5

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lbkg;

    invoke-direct {v1, v0}, Lbkg;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0d0082    # com.google.android.gms.R.dimen.games_image_download_size_achievement

    invoke-virtual {v1, p0, v0}, Lbkg;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcuh;IZ)V
    .locals 4

    iget v0, p1, Lcuh;->h:I

    if-le p2, v0, :cond_0

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->j:Landroid/net/Uri;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, p3, v3}, Lcuf;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z

    if-eqz p3, :cond_0

    const/16 v0, 0xbbb

    iput v0, p1, Lcuh;->i:I

    iget-boolean v0, p1, Lcuh;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->g:Ldax;

    iget-object v2, p1, Lcuh;->k:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcuf;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    if-eqz p2, :cond_0

    const-string v2, "current_steps"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "formatted_current_steps"

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz p3, :cond_1

    const-string v2, "state"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    if-eqz p4, :cond_2

    const-string v2, "state"

    invoke-static {p4}, Lddy;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, p2}, Lcuf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "AchievementAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find definition for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p3, v0, :cond_2

    if-ne p3, v2, :cond_1

    const-string v0, "AchievementAgent"

    const-string v2, "Attempted to increment a non-incremental achievement"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "AchievementAgent"

    const-string v2, "Attempted to change state of an incremental achievement"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 16

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v5, v3, v5

    if-nez v5, :cond_2

    const-string v3, "AchievementAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No game found matching external game ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcum;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_3

    const-string v3, "AchievementAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Attempted to retrieve achievement instances for unknown player "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "external_achievement_id"

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "external_achievement_id"

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {p1 .. p1}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v11

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v4, v3

    :goto_1
    if-ge v4, v12, :cond_6

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldoa;

    iget-object v13, v3, Lbni;->a:Landroid/content/ContentValues;

    const-string v3, "player_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v13, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "external_achievement_id"

    invoke-virtual {v13, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v3, "external_achievement_id"

    invoke-virtual {v13, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v3, "external_game_id"

    invoke-virtual {v13, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    invoke-virtual {v8, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    if-nez v3, :cond_4

    const-string v3, "AchievementAgent"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v15, "Unable to find local record for external achievement ID "

    invoke-direct {v13, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_4
    const-string v15, "definition_id"

    invoke-virtual {v13, v15, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    if-nez v3, :cond_5

    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    :goto_3
    invoke-static {v4}, Lcum;->a(I)Z

    move-result v13

    invoke-virtual {v3, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v11, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    goto :goto_3

    :cond_6
    const-string v3, "AchievementAgent"

    invoke-static {v5, v10, v3}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Lcug;)Z
    .locals 12

    const/4 v9, 0x0

    const/4 v8, 0x1

    :try_start_0
    const-string v1, "AchievementAgent"

    const-string v2, "Sending achievement batch..."

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcuf;->c:Ldly;

    iget-object v5, p2, Lcug;->g:Ldlu;

    const-string v4, "achievements/updateMultiple"

    iget-object v1, v1, Ldly;->a:Lbmi;

    const/4 v3, 0x1

    const-class v6, Ldlv;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldlv;

    iget-object v2, p0, Lcuf;->g:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    invoke-virtual {v1}, Ldlv;->getUpdatedAchievements()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ldlx;

    move-object v7, v0

    const-string v1, "AchievementAgent"

    const-string v2, "Achievement batch response [ID=%s, state=%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v7}, Ldlx;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v7}, Ldlx;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p2, Lcug;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcug;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    move v1, v8

    :goto_1
    if-eqz v1, :cond_0

    iget-object v11, p2, Lcug;->a:Landroid/content/Context;

    invoke-virtual {v7}, Ldlx;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p2, Lcug;->h:Lcuf;

    iget-object v2, p2, Lcug;->a:Landroid/content/Context;

    iget-object v3, p2, Lcug;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p2, Lcug;->c:Ljava/lang/String;

    iget-object v5, p2, Lcug;->d:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, p2, Lcug;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3, v1, v2}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7}, Ldlx;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7}, Ldlx;->e()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v7}, Ldlx;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v11, v1, v2, v3, v4}, Lcuf;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "AchievementAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    invoke-static {v1}, Lbng;->a(Lsp;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "AchievementAgent"

    const-string v2, "Could not submit pending operations, will try again later"

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v9

    :goto_2
    return v1

    :cond_2
    move v1, v9

    goto :goto_1

    :cond_3
    move v1, v8

    goto :goto_2

    :cond_4
    const-string v1, "AchievementAgent"

    const-string v2, "Encountered hard error while submiting pending operations."

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v8

    goto :goto_2
.end method

.method private a(Lcuh;ZZ)Z
    .locals 9

    const/4 v6, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcuh;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v8}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AchievementAgent"

    const-string v1, "Failed check achievement type."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xbba

    iput v0, p1, Lcuh;->i:I

    :goto_0
    return v7

    :cond_0
    iget-object v1, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v2, p1, Lcuh;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcuh;->c:Ljava/lang/String;

    iget-object v4, p1, Lcuh;->d:Ljava/lang/String;

    iget-object v5, p1, Lcuh;->e:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v3, p1, Lcuh;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p1, Lcuh;->e:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p1, Lcuh;->k:Landroid/os/Bundle;

    iget-object v2, p1, Lcuh;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Lcuh;->j:Landroid/net/Uri;

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->j:Landroid/net/Uri;

    sget-object v2, Lcui;->a:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    if-nez v0, :cond_2

    sget-object v0, Lcwh;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->g:Ldax;

    iget-object v2, p1, Lcuh;->k:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    :cond_1
    iput v7, p1, Lcuh;->i:I

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    if-nez p2, :cond_3

    iget v0, p1, Lcuh;->f:I

    if-lt v1, v0, :cond_3

    iput v7, p1, Lcuh;->i:I

    goto :goto_0

    :cond_3
    if-eq v2, v6, :cond_4

    if-ne v1, v6, :cond_5

    :cond_4
    const-string v0, "AchievementAgent"

    const-string v1, "Unable to find state record for incremental achievement"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xbb9

    iput v0, p1, Lcuh;->i:I

    goto/16 :goto_0

    :cond_5
    iget v3, p1, Lcuh;->f:I

    if-eqz p2, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lcuh;->h:I

    if-eqz p2, :cond_6

    iget v0, p1, Lcuh;->h:I

    if-gez v0, :cond_6

    iput v2, p1, Lcuh;->h:I

    :cond_6
    if-ge v1, v2, :cond_8

    iget v0, p1, Lcuh;->h:I

    if-ne v0, v2, :cond_8

    move v0, v8

    :goto_3
    iget-object v1, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v2, p1, Lcuh;->j:Landroid/net/Uri;

    iget v3, p1, Lcuh;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v0, v4}, Lcuf;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v0, "AchievementAgent"

    const-string v1, "Unable to update incremental achievement record."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iput v8, p1, Lcuh;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto :goto_2

    :cond_8
    move v0, v7

    goto :goto_3

    :cond_9
    if-eqz v0, :cond_a

    iput-boolean v8, p1, Lcuh;->l:Z

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->g:Ldax;

    iget-object v2, p1, Lcuh;->k:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lecn;->a(Landroid/content/Context;Ldax;Landroid/os/Bundle;)V

    :cond_a
    if-eqz p3, :cond_b

    iget-object v0, p1, Lcuh;->a:Landroid/content/Context;

    iget-object v1, p1, Lcuh;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0, v1}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    iput v0, p1, Lcuh;->i:I

    :goto_4
    move v7, v8

    goto/16 :goto_0

    :cond_b
    iput v7, p1, Lcuh;->i:I

    goto :goto_4

    :cond_c
    move v0, v6

    move v1, v6

    move v2, v6

    goto/16 :goto_1
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 7

    invoke-static {p1, p3, p2}, Ldiu;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "external_achievement_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p4, v4, v0

    const-wide/16 v5, -0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9

    const-wide/16 v7, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v6, -0x1

    invoke-static {p1}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcuk;->a:[Ljava/lang/String;

    const-string v3, "external_achievement_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    cmp-long v3, v1, v7

    if-eqz v3, :cond_0

    if-ne v0, v6, :cond_1

    :cond_0
    :goto_1
    return-object v5

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    new-instance v5, Landroid/util/Pair;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v5, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v0, v6

    move-wide v1, v7

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcuf;->g:Ljava/util/HashMap;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 9

    const/16 v8, 0x1f4

    const/4 v7, 0x0

    :try_start_0
    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-eqz p5, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcum;->a(Ljava/util/ArrayList;)Ldmj;

    move-result-object v6

    iget-object v0, p0, Lcuf;->e:Ldlz;

    const-string v3, "ALL"

    const/4 v5, 0x0

    move-object v1, p2

    move-object v2, p4

    invoke-virtual/range {v0 .. v6}, Ldlz;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldmj;)Ldob;

    move-result-object v0

    invoke-direct {p0, p3, p4}, Lcuf;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldob;->getItems()Ljava/util/ArrayList;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    invoke-static {p1, p2, v0, p3, p4}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    :goto_1
    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcuf;->c:Ldly;

    const-string v1, "ALL"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p4, v4, v2, v3, v1}, Ldly;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldly;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldoc;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldoc;

    invoke-direct {p0, p3, p4}, Lcuf;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldoc;->getItems()Ljava/util/ArrayList;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    move v0, v8

    goto :goto_1

    :cond_2
    move v0, v8

    goto :goto_1
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p1, p2}, Ldit;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    .locals 11

    new-instance v1, Lcuh;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcuh;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    const/4 v2, 0x1

    move/from16 v0, p8

    invoke-direct {p0, v1, v2, v0}, Lcuf;->a(Lcuh;ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    iget v1, v1, Lcuh;->i:I

    :goto_0
    return v1

    :cond_0
    if-eqz p8, :cond_1

    iget v2, v1, Lcuh;->i:I

    if-eqz v2, :cond_2

    :cond_1
    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v8, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    invoke-static {p2}, Ldrn;->b(Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x5

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcuf;->c:Ldly;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcuf;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-static {v0, v3, v4}, Ldly;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Ldly;->a:Lbmi;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Ldlq;

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Ldlq;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    invoke-direct {p0, p3, p4}, Lcuf;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ldlq;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2}, Ldlq;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v1, v3, v2}, Lcuf;->a(Lcuh;IZ)V

    :cond_3
    iget v1, v1, Lcuh;->i:I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "AchievementAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_4
    invoke-static {v1}, Lbng;->a(Lsp;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "AchievementAgent"

    const-string v2, "Encountered hard error while incrementing achievement."

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x6

    goto :goto_0

    :cond_5
    const-string v1, "AchievementAgent"

    const-string v2, "Unable to increment achievement. Increment will be deferred."

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v8, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    const/4 v1, 0x5

    goto/16 :goto_0

    :catch_1
    move-exception v1

    move-object v10, v1

    const-string v1, "AchievementAgent"

    const-string v2, "Auth error while incrementing achievement over network"

    invoke-static {v1, v2, v10}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v8, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    throw v10
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 5

    invoke-direct {p0, p1, p2}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 6

    invoke-direct {p0, p1, p2}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcuf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to sync instances for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 7

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to sync definitions for game "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_0
    if-nez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;Z)I
    .locals 11

    new-instance v1, Lcuh;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcuh;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)V

    const/4 v2, 0x0

    move/from16 v0, p8

    invoke-direct {p0, v1, v2, v0}, Lcuf;->a(Lcuh;ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    iget v1, v1, Lcuh;->i:I

    :goto_0
    return v1

    :cond_0
    if-eqz p8, :cond_1

    iget v2, v1, Lcuh;->i:I

    if-eqz v2, :cond_2

    :cond_1
    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    invoke-static {p2}, Ldrn;->b(Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x5

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcuf;->c:Ldly;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-static {v0, v3}, Ldly;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Ldly;->a:Lbmi;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Ldls;

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Ldls;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    invoke-direct {p0, p3, p4}, Lcuf;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ldls;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2}, Ldls;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v1, v3, v2}, Lcuf;->a(Lcuh;IZ)V

    :cond_3
    iget v1, v1, Lcuh;->i:I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "AchievementAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_4
    invoke-static {v1}, Lbng;->a(Lsp;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "AchievementAgent"

    const-string v2, "Encountered hard error while setting achievement steps."

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x6

    goto :goto_0

    :cond_5
    const-string v1, "AchievementAgent"

    const-string v2, "Unable to set achievement steps. Set will be deferred."

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    const/4 v1, 0x5

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v10, v1

    const-string v1, "AchievementAgent"

    const-string v2, "Auth error while incrementing achievement over network"

    invoke-static {v1, v2, v10}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)J

    throw v10
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldax;)I
    .locals 8

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILdax;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcuf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
