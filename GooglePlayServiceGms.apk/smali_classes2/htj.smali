.class public final Lhtj;
.super Lhtw;
.source "SourceFile"


# instance fields
.field public final a:Lhuv;


# direct methods
.method private constructor <init>(Lhug;Lhty;JLhuv;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lhtw;-><init>(Lhug;Lhty;J)V

    iput-object p5, p0, Lhtj;->a:Lhuv;

    return-void
.end method

.method public static a(Livi;Lhuv;J)Lhtj;
    .locals 6

    const/4 v5, 0x3

    const/4 v0, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0, v3}, Livi;->c(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v4}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v4, v0}, Livi;->c(II)Livi;

    move-result-object v1

    invoke-virtual {v1, v3}, Livi;->c(I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v4}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v4}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-virtual {v1, v3}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Livi;->f(I)Livi;

    move-result-object v2

    invoke-virtual {v2, v3}, Livi;->c(I)I

    move-result v3

    invoke-virtual {v2, v4}, Livi;->c(I)I

    move-result v4

    invoke-virtual {v1, v5}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v5}, Livi;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    move v2, v0

    :goto_0
    const v0, 0x4c4b40

    if-gt v2, v0, :cond_0

    new-instance v0, Lhtj;

    new-instance v1, Lhug;

    invoke-direct {v1, v3, v4, v2}, Lhug;-><init>(III)V

    sget-object v2, Lhty;->a:Lhty;

    move-wide v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhtj;-><init>(Lhug;Lhty;JLhuv;)V

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v2, v0

    goto :goto_0
.end method

.method public static a(Ljava/io/PrintWriter;Lhtj;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "GlsLocatorResult [wifiScan="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lhtj;->a:Lhuv;

    invoke-static {p0, v0}, Lhuv;->a(Ljava/io/PrintWriter;Lhuv;)V

    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0, p1}, Lhtw;->a(Ljava/io/PrintWriter;Lhtw;)V

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Lhtj;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    const-string v0, "GlsLocatorResult [wifiScan="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lhtj;->a:Lhuv;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lhtw;->a(Ljava/lang/StringBuilder;Lhtw;)V

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GlsLocatorResult [wifiScan="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhtj;->a:Lhuv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lhtw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
