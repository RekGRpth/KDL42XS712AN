.class public final Lbso;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcfz;

.field private final d:Lcll;

.field private final e:Lcon;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lbso;->a:J

    return-void
.end method

.method public constructor <init>(Lcoy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbso;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbso;->c:Lcfz;

    invoke-virtual {p1}, Lcoy;->a()Lcon;

    move-result-object v0

    iput-object v0, p0, Lbso;->e:Lcon;

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v0

    iput-object v0, p0, Lbso;->d:Lcll;

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/util/Set;Lcfc;Lbsp;Z)Lbsm;
    .locals 8

    :try_start_0
    iget-object v0, p0, Lbso;->d:Lcll;

    invoke-interface {v0, p1}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    iget-object v0, p0, Lbso;->e:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    sget-wide v4, Lbso;->a:J

    add-long v5, v0, v4

    new-instance v0, Lbsp;

    move-object v1, p4

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lbsp;-><init>(Lcfc;JLcom/google/android/gms/drive/auth/AppIdentity;JLjava/util/Set;)V

    iget-object v1, p0, Lbso;->c:Lcfz;

    invoke-interface {v1, v0}, Lcfz;->b(Lbsp;)V

    sget-object v1, Lbsn;->a:Lbsn;

    invoke-static {v1, v0}, Lbsm;->a(Lbsn;Lbsp;)Lbsm;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    if-eqz p6, :cond_0

    sget-object v0, Lbsn;->b:Lbsn;

    invoke-static {v0, p5}, Lbsm;->a(Lbsn;Lbsp;)Lbsm;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v2, Lbsn;->d:Lbsn;

    invoke-virtual {v1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lrn;

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lrn;

    invoke-static {v0}, Lcom;->a(Lrn;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Lbsl;

    const-string v3, "server returned error: %s. See https://developers.google.com/drive/handle-errors for details."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-static {v2, v0}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Lbsl;

    const-string v3, "server returned response code %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Lrn;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbsl;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    new-instance v0, Lbsl;

    invoke-direct {v0, v1}, Lbsl;-><init>(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lbso;->c:Lcfz;

    invoke-interface {v1, p4, p2}, Lcfz;->b(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)V

    sget-object v1, Lbsn;->c:Lbsn;

    new-instance v2, Lbsl;

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lbsl;-><init>(Ljava/lang/Exception;Landroid/content/Intent;)V

    invoke-static {v1, v2}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lbso;->c:Lcfz;

    invoke-interface {v1, p4, p2}, Lcfz;->b(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)V

    sget-object v1, Lbsn;->d:Lbsn;

    new-instance v2, Lbsl;

    const-string v3, "See https://developers.google.com/drive/android/auth for details on authorizing an application."

    invoke-direct {v2, v3, v0}, Lbsl;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v1, v2}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Z)Lbsm;
    .locals 13

    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    const-class v0, Lbqr;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-static {}, Lbqr;->values()[Lbqr;

    move-result-object v2

    array-length v4, v2

    move v0, v7

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    invoke-virtual {v5}, Lbqr;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v3, Lbsl;

    const-string v4, "Auth package name \'%s\' did not match calling package name \'%s\'"

    new-array v5, v12, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v2, Lbsl;

    const-string v3, "Requested account name \'%s\' did not match resolved account name \'%s\'"

    new-array v5, v12, [Ljava/lang/Object;

    aput-object v4, v5, v7

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v1

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lbso;->c:Lcfz;

    invoke-interface {v5, v4}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v4

    iget-object v5, p0, Lbso;->b:Landroid/content/Context;

    invoke-static {v5, v0, v2}, Lbox;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v3, Lbsn;->d:Lbsn;

    new-instance v4, Lbsl;

    const-string v5, "Package %s is not valid for uid %d."

    new-array v6, v12, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto :goto_1

    :cond_4
    :try_start_0
    iget-object v0, p0, Lbso;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lbox;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v3, Lbsl;

    const-string v4, "No certificates for %s from package manager."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v3, Lbsl;

    const-string v4, "Package not found: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v7

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    invoke-static {v2, v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v2

    iget-object v0, p0, Lbso;->c:Lcfz;

    invoke-interface {v0, v4, v2}, Lcfz;->a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v0, v5, Lbsp;->e:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v6, v1

    :goto_2
    if-eqz v5, :cond_8

    iget-object v0, p0, Lbso;->e:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v8

    iget-wide v10, v5, Lbsp;->d:J

    cmp-long v0, v10, v8

    if-gtz v0, :cond_7

    move v0, v1

    :goto_3
    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    if-eqz v6, :cond_9

    if-eqz v0, :cond_9

    sget-object v0, Lbsn;->a:Lbsn;

    invoke-static {v0, v5}, Lbsm;->a(Lbsn;Lbsp;)Lbsm;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move v6, v7

    goto :goto_2

    :cond_7
    move v0, v7

    goto :goto_3

    :cond_8
    move v0, v7

    goto :goto_4

    :cond_9
    if-eqz p2, :cond_a

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lbso;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/util/Set;Lcfc;Lbsp;Z)Lbsm;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    if-eqz v6, :cond_b

    sget-object v0, Lbsn;->b:Lbsn;

    invoke-static {v0, v5}, Lbsm;->a(Lbsn;Lbsp;)Lbsm;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    if-eqz v5, :cond_c

    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v2, Lbsl;

    const-string v4, "Expected scopes %s, but cached authorization only has scopes %s."

    new-array v6, v12, [Ljava/lang/Object;

    aput-object v3, v6, v7

    iget-object v3, v5, Lbsp;->e:Ljava/util/Set;

    aput-object v3, v6, v1

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    sget-object v0, Lbsn;->d:Lbsn;

    new-instance v1, Lbsl;

    const-string v2, "The user has revoked access. Reconnect the Drive API client to reauthorize."

    invoke-direct {v1, v2}, Lbsl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbsm;->a(Lbsn;Lbsl;)Lbsm;

    move-result-object v0

    goto/16 :goto_1
.end method
