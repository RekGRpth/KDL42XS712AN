.class final Lape;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field private final b:Laoy;


# direct methods
.method public constructor <init>(Laoy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lape;->a:Ljava/util/List;

    iput-object p1, p0, Lape;->b:Laoy;

    return-void
.end method


# virtual methods
.method final a(Lapt;I)Lape;
    .locals 2

    invoke-virtual {p1}, Lapt;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method final a(Lapt;Lapt;Ljava/lang/String;)Lape;
    .locals 2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0, p1, p3}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    iget-object v1, p0, Lape;->b:Laoy;

    invoke-virtual {v1, p3}, Laoy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object p0

    goto :goto_0
.end method

.method final a(Lapt;Ljava/lang/String;)Lape;
    .locals 1

    invoke-virtual {p1}, Lapt;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method final a(Lapt;Z)Lape;
    .locals 2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lapt;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lape;
    .locals 1

    sget-object v0, Lapt;->o:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lape;
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lape;->a:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public final a(Z)Lape;
    .locals 1

    sget-object v0, Lapt;->x:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Z)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lape;
    .locals 2

    sget-object v0, Lapt;->n:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    sget-object v1, Lapt;->m:Lapt;

    invoke-virtual {v0, v1, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)Lape;
    .locals 1

    sget-object v0, Lapt;->F:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Z)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lape;
    .locals 1

    sget-object v0, Lapt;->g:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lape;
    .locals 1

    sget-object v0, Lapt;->h:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lape;
    .locals 1

    sget-object v0, Lapt;->a:Lapt;

    invoke-virtual {p0, v0, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v0

    return-object v0
.end method
