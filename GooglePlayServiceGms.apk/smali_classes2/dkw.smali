.class public final Ldkw;
.super Ldfa;
.source "SourceFile"


# instance fields
.field private final d:Ldkk;

.field private final e:Ldkn;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Ldkk;Ldkj;)V
    .locals 1

    invoke-direct {p0, p1}, Ldfa;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Ldkw;->d:Ldkk;

    iput-object p2, p0, Ldfa;->b:Ldez;

    new-instance v0, Ldkn;

    invoke-direct {v0, p0, p3}, Ldkn;-><init>(Landroid/os/Handler;Ldkj;)V

    iput-object v0, p0, Ldkw;->e:Ldkn;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/Message;
    .locals 3

    const/16 v0, 0xfa1

    invoke-virtual {p0, v0, p1}, Ldkw;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1, v2}, Ldkw;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-object v0
.end method

.method final a()Ldkn;
    .locals 1

    iget-object v0, p0, Ldkw;->e:Ldkn;

    return-object v0
.end method

.method public final a(Landroid/os/Message;)V
    .locals 1

    const/16 v0, 0xfa1

    invoke-virtual {p0, v0, p1}, Ldkw;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public final b()V
    .locals 1

    const/16 v0, 0xfa1

    invoke-virtual {p0, v0}, Ldkw;->removeMessages(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    const/16 v0, 0xfa2

    invoke-virtual {p0, v0}, Ldkw;->removeMessages(I)V

    return-void
.end method

.method public final d()V
    .locals 3

    const/16 v0, 0xfa2

    const-wide/32 v1, 0x493e0

    invoke-virtual {p0, v0, v1, v2}, Ldkw;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Ldfa;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-virtual {p0}, Ldkw;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ldfa;->handleMessage(Landroid/os/Message;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Ldkw;->d:Ldkk;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldkk;->g(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Ldkw;->d:Ldkk;

    invoke-virtual {v0}, Ldkk;->e()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xfa1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
