.class public final Lcne;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcfp;

.field private final b:I


# direct methods
.method public constructor <init>(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcne;->a:Lcfp;

    sget-object v0, Lcle;->g:Lcje;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcje;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcle;->g:Lcje;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcne;->a:Lcfp;

    invoke-virtual {v1}, Lcfp;->u()Z

    move-result v1

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcne;->a:Lcfp;

    invoke-virtual {v0}, Lcfp;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v3, p0, Lcne;->b:I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "Pinning is not enabled for this document: %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcne;->a:Lcfp;

    invoke-virtual {v4}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcne;->b:I

    goto :goto_0

    :cond_2
    iput v5, p0, Lcne;->b:I

    goto :goto_0

    :cond_3
    iput v5, p0, Lcne;->b:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcnf;Lbsr;)V
    .locals 3

    iget v0, p0, Lcne;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcnf;->b()Ljava/util/concurrent/Future;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcne;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcne;->a:Lcfp;

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iget-object v1, p1, Lcnf;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p1, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnh;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcnh;->a(Lcnh;)Lbwm;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcnh;->a(Lcnh;)Lbwm;

    move-result-object v0

    invoke-virtual {v0}, Lbwm;->a()V

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Lbsr;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
