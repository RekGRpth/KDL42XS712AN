.class public final Lcpu;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcpy;)Ljava/lang/String;
    .locals 10

    const/4 v9, 0x2

    const/4 v0, 0x0

    const/16 v1, 0x9

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    new-array v2, v9, [I

    fill-array-data v2, :array_1

    const/16 v3, 0x1b

    new-array v3, v3, [I

    fill-array-data v3, :array_2

    const/16 v4, 0xb

    new-array v4, v4, [I

    fill-array-data v4, :array_3

    const/16 v5, 0x10

    new-array v5, v5, [I

    fill-array-data v5, :array_4

    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_5

    const/16 v7, 0xa

    new-array v7, v7, [I

    fill-array-data v7, :array_6

    const/4 v8, 0x3

    new-array v8, v8, [[I

    aput-object v6, v8, v0

    const/4 v6, 0x1

    aput-object v7, v8, v6

    aput-object v1, v8, v9

    array-length v1, v8

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v6, v8, v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0, v6}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0, v4}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :array_0
    .array-data 4
        0xd9
        0xc4
        0xc3
        0xd8
        0xcc
        0xe2
        0xba
        0xc9
        0x50
    .end array-data

    :array_1
    .array-data 4
        0xe5
        0x66
    .end array-data

    :array_2
    .array-data 4
        0xc8
        0xd0
        0x66
        0xc4
        0xdf
        0x9d
        0xe2
        0xc4
        0x66
        0xe0
        0xbc
        0xdb
        0x75
        0xdf
        0xbf
        0xc4
        0xc4
        0x66
        0xce
        0xd3
        0xde
        0xd2
        0x95
        0xb7
        0xba
        0xe0
        0x66
    .end array-data

    :array_3
    .array-data 4
        0x66
        0xdf
        0xbf
        0xe0
        0xc0
        0xc0
        0xd9
        0x9a
        0xb7
        0xc4
        0xc4
    .end array-data

    :array_4
    .array-data 4
        0xdc
        0x95
        0xbe
        0x95
        0xb7
        0xd6
        0x66
        0xe4
        0xbc
        0xdc
        0xe2
        0xd2
        0xbf
        0x6
        0x25
        0xb7
    .end array-data

    :array_5
    .array-data 4
        0xb1
        0xe0
        0xd2
        0x75
        0xbf
        0x95
        0x50
    .end array-data

    :array_6
    .array-data 4
        0xd3
        0xb7
        0xba
        0xde
        0xdf
        0xd2
        0xab
        0xd4
        0xdc
        0x50
    .end array-data
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_0
    :goto_2
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_4
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method
