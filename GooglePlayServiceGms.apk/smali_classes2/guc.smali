.class public final Lguc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsk;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lsf;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lsk;

.field private final g:Lsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lsf;ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lguc;->a:Landroid/content/Context;

    iput-object p2, p0, Lguc;->b:Lsf;

    iput p3, p0, Lguc;->c:I

    iput-object p4, p0, Lguc;->d:Ljava/lang/String;

    iput-object p5, p0, Lguc;->e:Ljava/lang/String;

    iput-object p6, p0, Lguc;->f:Lsk;

    iput-object p7, p0, Lguc;->g:Lsj;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lgud;

    iget-object v1, p0, Lguc;->a:Landroid/content/Context;

    iget v2, p0, Lguc;->c:I

    iget-object v5, p0, Lguc;->g:Lsj;

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lgud;-><init>(Landroid/content/Context;ILjava/lang/String;Lsk;Lsj;)V

    iget-object v1, p0, Lguc;->b:Lsf;

    invoke-virtual {v1, v0}, Lsf;->a(Lsc;)Lsc;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lguc;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lguc;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lorg/json/JSONObject;

    iget-object v0, p0, Lguc;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lgty;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lguc;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lguc;->f:Lsk;

    invoke-interface {v0, p1}, Lsk;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
