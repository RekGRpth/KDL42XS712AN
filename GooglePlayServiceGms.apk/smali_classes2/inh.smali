.class public final Linh;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:Z

.field private c:I

.field private d:Z

.field private e:Z

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput v0, p0, Linh;->c:I

    iput v0, p0, Linh;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Linh;->f:J

    const/4 v0, -0x1

    iput v0, p0, Linh;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Linh;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Linh;->b()I

    :cond_0
    iget v0, p0, Linh;->g:I

    return v0
.end method

.method public final a(I)Linh;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linh;->d:Z

    iput p1, p0, Linh;->a:I

    return-object p0
.end method

.method public final a(J)Linh;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linh;->e:Z

    iput-wide p1, p0, Linh;->f:J

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Linh;->b:Z

    iput v0, p0, Linh;->c:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Linh;->a(I)Linh;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Linh;->a(J)Linh;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Linh;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Linh;->c:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Linh;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Linh;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-boolean v0, p0, Linh;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Linh;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Linh;->b:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Linh;->c:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Linh;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Linh;->a:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Linh;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Linh;->f:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Linh;->g:I

    return v0
.end method
