.class public final Lbzg;
.super Lbyr;
.source "SourceFile"


# instance fields
.field private final e:I

.field private final f:Lbyu;

.field private g:Lbys;


# direct methods
.method public constructor <init>(Lcoy;Lcom/google/android/gms/drive/data/view/DocListView;Landroid/widget/ListView;Lbzq;)V
    .locals 3

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2, p3, v1}, Lbyr;-><init>(Lcfz;Lcom/google/android/gms/drive/data/view/DocListView;Landroid/widget/ListView;Lcom/google/android/gms/drive/data/view/StickyHeaderView;)V

    new-instance v0, Lbyu;

    invoke-direct {v0, p1, p4}, Lbyu;-><init>(Lcoy;Lbzq;)V

    iput-object v0, p0, Lbzg;->f:Lbyu;

    invoke-virtual {p3}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    iput v0, p0, Lbzg;->e:I

    return-void
.end method


# virtual methods
.method public final a(Lcfc;Lbzb;Lcag;)V
    .locals 12

    const/4 v11, 0x0

    iget-object v0, p0, Lbzg;->c:Landroid/widget/ListView;

    iget v1, p0, Lbzg;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v0, p0, Lbzg;->g:Lbys;

    if-nez v0, :cond_2

    new-instance v6, Lbzh;

    invoke-direct {v6, p0}, Lbzh;-><init>(Lbzg;)V

    iget-object v5, p0, Lbzg;->f:Lbyu;

    iget-object v2, p0, Lbzg;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v4, p0, Lbzg;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    new-instance v0, Lbys;

    iget-object v1, v5, Lbyu;->a:Landroid/content/Context;

    iget-object v3, v5, Lbyu;->b:Lccm;

    iget-object v7, v5, Lbyu;->c:Lbzi;

    iget-object v8, v5, Lbyu;->d:Lbzq;

    iget-wide v9, v5, Lbyu;->e:J

    move-object v5, p2

    invoke-direct/range {v0 .. v11}, Lbys;-><init>(Landroid/content/Context;Lccx;Lccm;Lbzr;Lbzb;Landroid/view/View$OnClickListener;Lbzi;Lbzq;JB)V

    iput-object v0, p0, Lbzg;->g:Lbys;

    :goto_0
    iget-object v0, p0, Lbzg;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->f()Lbzk;

    move-result-object v0

    iget-object v1, p0, Lbzg;->g:Lbys;

    iget-object v2, p1, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v1, p3, v0, v2}, Lbys;->a(Lcag;Lbzk;Ljava/lang/String;)V

    iget-object v0, p0, Lbzg;->g:Lbys;

    iget-object v1, p0, Lbzg;->c:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbzg;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lbzg;->g:Lbys;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    iget-object v0, p0, Lbzg;->g:Lbys;

    invoke-virtual {p0, v0}, Lbzg;->a(Lbzp;)V

    if-eqz v11, :cond_1

    iget-object v0, p0, Lbzg;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->i()V

    iget-object v0, p0, Lbzg;->g:Lbys;

    invoke-virtual {v0, p2}, Lbys;->a(Lbzb;)V

    :cond_1
    return-void

    :cond_2
    const/4 v11, 0x1

    goto :goto_0
.end method

.method protected final d()Lbzp;
    .locals 1

    iget-object v0, p0, Lbzg;->g:Lbys;

    return-object v0
.end method
