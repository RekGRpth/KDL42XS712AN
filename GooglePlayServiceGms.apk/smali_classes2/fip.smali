.class public final Lfip;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;

.field static final b:Ljava/util/Map;

.field static final c:Ljava/util/Map;

.field static final d:Ljava/util/Map;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lbpe;

.field private final g:Lfbo;

.field private final h:Lfbn;

.field private final i:Lffh;

.field private final j:J

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Landroid/content/ContentValues;

.field private n:Ljava/util/Set;

.field private o:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfiq;

    invoke-direct {v0}, Lfiq;-><init>()V

    sput-object v0, Lfip;->a:Ljava/util/Map;

    new-instance v0, Lfir;

    invoke-direct {v0}, Lfir;-><init>()V

    sput-object v0, Lfip;->b:Ljava/util/Map;

    new-instance v0, Lfis;

    invoke-direct {v0}, Lfis;-><init>()V

    sput-object v0, Lfip;->c:Ljava/util/Map;

    new-instance v0, Lfit;

    invoke-direct {v0}, Lfit;-><init>()V

    sput-object v0, Lfip;->d:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lfip;->m:Landroid/content/ContentValues;

    iput-object v1, p0, Lfip;->n:Ljava/util/Set;

    iput-object v1, p0, Lfip;->o:Ljava/util/Set;

    iput-object p1, p0, Lfip;->e:Landroid/content/Context;

    iget-object v0, p0, Lfip;->e:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    iput-object v0, p0, Lfip;->f:Lbpe;

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iput-object v0, p0, Lfip;->g:Lfbo;

    iget-object v0, p0, Lfip;->g:Lfbo;

    invoke-virtual {v0}, Lfbo;->d()Lfbn;

    move-result-object v0

    iput-object v0, p0, Lfip;->h:Lfbn;

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lfip;->j:J

    iput-object p2, p0, Lfip;->k:Ljava/lang/String;

    iput-object p3, p0, Lfip;->l:Ljava/lang/String;

    invoke-static {p1}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    iput-object v0, p0, Lfip;->i:Lffh;

    return-void
.end method

.method private a(Lgld;I)Landroid/content/ContentValues;
    .locals 6

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v1

    const-string v0, "last_modified"

    iget-object v2, p0, Lfip;->f:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "owner_id"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "circle_id"

    invoke-interface {p1}, Lgld;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-interface {p1}, Lgld;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "people_count"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "sort_key"

    const-string v2, "s%06d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "type"

    sget-object v0, Lfdl;->a:Ljava/util/Map;

    invoke-interface {p1}, Lgld;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "client_policies"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v1
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;
    .locals 4

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v1

    const-string v0, "owner_id"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "qualified_id"

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "custom_label"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "type"

    sget-object v0, Lfip;->b:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v1
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "e"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lbkm;->b(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "PeopleSync"

    const-string v1, "FK Error, ignoring."

    invoke-static {v0, v1, p1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "PeopleSync"

    const-string v1, "FK Error, ignoring."

    invoke-static {p0, v0, v1, p1}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "circle_members"

    const-string v3, "owner_id = ? AND qualified_id = ?"

    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "PeopleService"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "PeopleSync"

    const-string v6, "    %s    --> %s: (Create)"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object p1, v7, v2

    aput-object v0, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lfip;->h(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "PeopleSync"

    const-string v6, "    Circle %s doesn\'t exist"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "owner_id"

    iget-wide v7, p0, Lfip;->j:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "circle_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "qualified_id"

    invoke-virtual {v5, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v6, "circle_members"

    invoke-virtual {v0, v6, v5}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    :cond_2
    return-void
.end method

.method private b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "emails"

    const-string v3, "owner_id = ? AND qualified_id = ?"

    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lfhr;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {p2}, Lfhr;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v3, :cond_2

    new-instance v0, Lfhy;

    const-string v1, "No email for email only person"

    invoke-direct {v0, v1}, Lfhy;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "emails"

    invoke-direct {p0, p1, v0}, Lfip;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    :cond_3
    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->e()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lfip;->h:Lfbn;

    const-string v5, "emails"

    invoke-direct {p0, p1, v0}, Lfip;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private c(Lgkp;)Landroid/content/ContentValues;
    .locals 4

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v1

    invoke-interface {p1}, Lgkp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfhy;

    const-string v1, "Missing gaia-id for +page"

    invoke-direct {v0, v1}, Lfhy;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "gaia_id"

    iget-object v2, p0, Lfip;->e:Landroid/content/Context;

    invoke-static {v2}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->e()Lfbk;

    move-result-object v2

    iget-object v3, p0, Lfip;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfbk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "account_name"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "display_name"

    invoke-interface {p1}, Lgkp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "page_gaia_id"

    invoke-interface {p1}, Lgkp;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "etag"

    invoke-interface {p1}, Lgkp;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "avatar"

    invoke-interface {p1}, Lgkp;->h()Lgks;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_1
    invoke-interface {p1}, Lgkp;->h()Lgks;

    move-result-object v0

    invoke-interface {v0}, Lgks;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lgmb;I)Landroid/content/ContentValues;
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "last_modified"

    iget-object v2, p0, Lfip;->f:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "owner_id"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "circle_id"

    invoke-interface {p1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "name"

    invoke-interface {p1}, Lgmb;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sort_key"

    const-string v2, "p%06d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "type"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "client_policies"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "for_sharing"

    invoke-interface {p1}, Lgmb;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 10

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "phones"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->f()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lfip;->h:Lfbn;

    const-string v5, "phones"

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lfip;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "qualified_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "phone"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    sget-object v8, Lfip;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v5, v6}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 10

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "postal_address"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->h()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lfip;->h:Lfbn;

    const-string v5, "postal_address"

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lfip;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "qualified_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "postal_address"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    sget-object v8, Lfip;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v5, v6}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v4

    const-string v0, "last_modified"

    iget-object v5, p0, Lfip;->f:Lbpe;

    invoke-interface {v5}, Lbpe;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "owner_id"

    iget-wide v5, p0, Lfip;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "qualified_id"

    invoke-static {p1}, Lfhr;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "v2_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lfhy;

    const-string v1, "This person has no metadata. Each synced person must have a gaia-id or belong to a circle."

    invoke-direct {v0, v1}, Lfhy;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v5, "invisible_3p"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "blocked"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "gaia_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string v5, "in_viewer_domain"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "in_circle"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "in_contacts"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, "sort_key"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sort_key_irank"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    const-string v1, "profile_type"

    sget-object v5, Lfip;->d:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-static {v5, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lfhv;->a:Lfhv;

    invoke-static {v0, v1}, Lfhr;->a(Ljava/util/List;Lfhs;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;

    const-string v1, "tagline"

    if-nez v0, :cond_a

    move-object v0, v3

    :goto_8
    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lfhr;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v1, "name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "family_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "given_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "middle_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "name_verified"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    if-nez v5, :cond_b

    :goto_9
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :goto_a
    invoke-static {p1}, Lfhr;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_1
    const-string v0, "avatar"

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_b
    return-object v4

    :cond_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    const-string v0, "gaia_id"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_3

    :cond_6
    move v0, v2

    goto/16 :goto_4

    :cond_7
    const-string v0, "in_contacts"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    :cond_8
    const-string v0, "sort_key"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "sort_key_irank"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_9
    move-object v0, v3

    goto/16 :goto_7

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    :cond_b
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->i()Z

    move-result v2

    goto :goto_9

    :cond_c
    const-string v0, "name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "family_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "given_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "middle_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "name_verified"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_a

    :cond_d
    const-string v1, "avatar"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b
.end method

.method private e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 7

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "application_packages"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    const-string v3, "android"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "dev_console_id"

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "owner_id"

    iget-wide v5, p0, Lfip;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "package_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "certificate_hash"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v4, "application_packages"

    invoke-virtual {v0, v4, v3}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private q(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lfip;->g:Lfbo;

    invoke-virtual {v0}, Lfbo;->h()Lfbq;

    move-result-object v0

    iget-object v1, p0, Lfip;->k:Ljava/lang/String;

    iget-object v2, p0, Lfip;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lfbq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private u()Landroid/content/ContentValues;
    .locals 1

    iget-object v0, p0, Lfip;->m:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    iget-object v0, p0, Lfip;->m:Landroid/content/ContentValues;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT value FROM sync_tokens WHERE owner_id=? AND name=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 8

    const-string v0, "PeopleSync"

    const-string v1, "New sync Tokens are:"

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT name, value FROM sync_tokens WHERE owner_id = ? ORDER BY name"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v0, "name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const-string v2, "value"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PeopleSync"

    const-string v4, "    %s: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public final a(J)V
    .locals 5

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "last_full_people_sync_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "owners"

    const-string v4, "_id = ?"

    invoke-virtual {v2, v3, v0, v4, v1}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V
    .locals 5

    const/4 v3, 0x2

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleSync"

    const-string v2, "    Application: %s - Person: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "dev_console_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_id"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "qualified_id"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v2, "facl_people"

    invoke-virtual {v0, v2, v1}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V
    .locals 6

    const/4 v3, 0x2

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v0

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleSync"

    const-string v2, "    %s: %s (Create)"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, Lgld;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "circles"

    invoke-direct {p0, v0, p2}, Lfip;->a(Lgld;I)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 11

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    invoke-static {p1}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    const-string v2, "PeopleService"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PeopleSync"

    const-string v4, "    %s: %s (Update)"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v6, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v5

    const-string v0, "gaia_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    const-string v2, "display_name"

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lfhr;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_1
    const-string v0, "avatar"

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;

    :goto_3
    const-string v6, "cover_photo_url"

    if-nez v0, :cond_7

    move-object v2, v1

    :goto_4
    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "cover_photo_height"

    if-nez v0, :cond_8

    move v2, v3

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "cover_photo_width"

    if-nez v0, :cond_9

    move v2, v3

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cover_photo_id"

    if-nez v0, :cond_a

    :goto_7
    invoke-virtual {v5, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "owners"

    const-string v2, "_id = ?"

    invoke-virtual {v0, v1, v5, v2, v4}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "owner_emails"

    const-string v2, "owner_id=?"

    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lfhr;->b(Ljava/util/List;)I

    move-result v4

    move v1, v3

    :goto_8
    if-ge v1, v4, :cond_b

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->e()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    invoke-static {v5}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lfip;->h:Lfbn;

    const-string v6, "owner_emails"

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "owner_id"

    iget-wide v9, p0, Lfip;->j:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "email"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "type"

    sget-object v9, Lfip;->b:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v5, v6, v7}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_3
    const-string v0, "- Unknown -"

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    const-string v2, "avatar"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    move-object v0, v1

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->d()I

    move-result v2

    goto/16 :goto_5

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->h()I

    move-result v2

    goto/16 :goto_6

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->e()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_7

    :cond_b
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "owner_phones"

    const-string v2, "owner_id=?"

    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lfhr;->b(Ljava/util/List;)I

    move-result v4

    move v1, v3

    :goto_9
    if-ge v1, v4, :cond_d

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->f()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    invoke-static {v5}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v5

    if-eqz v5, :cond_c

    iget-object v5, p0, Lfip;->h:Lfbn;

    const-string v6, "owner_phones"

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "owner_id"

    iget-wide v9, p0, Lfip;->j:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "phone"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "type"

    sget-object v9, Lfip;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v5, v6, v7}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_d
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "owner_postal_address"

    const-string v2, "owner_id=?"

    iget-wide v4, p0, Lfip;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lfhr;->b(Ljava/util/List;)I

    move-result v2

    :goto_a
    if-ge v3, v2, :cond_f

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->h()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, p0, Lfip;->h:Lfbn;

    const-string v5, "owner_postal_address"

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lfip;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "postal_address"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    sget-object v8, Lfip;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lfip;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v5, v6}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_f
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lfid;)V
    .locals 11

    const/4 v1, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    const-string v0, "PeopleService"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v2, "    %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lfip;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget v2, p2, Lfid;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lfid;->I:I

    :goto_2
    iget-object v2, p0, Lfip;->n:Ljava/util/Set;

    if-nez v2, :cond_4

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lfip;->n:Ljava/util/Set;

    :cond_4
    iget-object v2, p0, Lfip;->o:Ljava/util/Set;

    if-nez v2, :cond_5

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lfip;->o:Ljava/util/Set;

    :cond_5
    iget-object v2, p0, Lfip;->n:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    iget-object v2, p0, Lfip;->o:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_7

    iget-object v6, p0, Lfip;->n:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    iget v2, p2, Lfid;->J:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lfid;->J:I

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v4, "SELECT circle_id FROM circle_members WHERE owner_id=?  AND qualified_id=?"

    iget-wide v5, p0, Lfip;->j:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :goto_4
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lfip;->o:Ljava/util/Set;

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-static {p1}, Lfhr;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lfip;->n:Ljava/util/Set;

    iget-object v5, p0, Lfip;->o:Ljava/util/Set;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lfip;->g:Lfbo;

    iget-object v5, p0, Lfip;->k:Ljava/lang/String;

    invoke-virtual {v4, v5, v1, v2}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_9
    invoke-static {p1}, Lfip;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget v1, p2, Lfid;->K:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lfid;->K:I

    :goto_5
    const-string v1, "PeopleService"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PeopleSync"

    const-string v2, "Inconsistency found: gaia=%s  expected=%s  actual=%s"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v0, v4, v3

    iget-object v0, p0, Lfip;->n:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    iget-object v0, p0, Lfip;->o:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    iget v1, p2, Lfid;->L:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lfid;->L:I

    goto :goto_5
.end method

.method public final a(Lgkp;)V
    .locals 5

    const/4 v2, 0x2

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Create)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lgkp;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lgkp;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "owners"

    invoke-direct {p0, p1}, Lfip;->c(Lgkp;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public final a(Lgmb;I)V
    .locals 5

    const/4 v2, 0x2

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Create)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lgmb;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "circles"

    invoke-direct {p0, p1, p2}, Lfip;->c(Lgmb;I)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->b()V

    :try_start_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE FROM sync_tokens WHERE owner_id = ? AND name = ?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "INSERT INTO sync_tokens(owner_id,name,value) VALUES (?,?,?)"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->e()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfip;->h:Lfbn;

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=? AND contact_id=?  AND value=? "

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "INSERT INTO gaia_id_map(owner_id,contact_id,value,gaia_id,type) VALUES (?,?,?,?,?)"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p1, p2, p3, v3}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final a(ZJLjava/lang/Long;Z)V
    .locals 6

    const/4 v4, 0x3

    const/4 v5, 0x1

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->g()V

    const-string v0, "PeopleService"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sync stats: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " page="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfip;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  end="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-wide v0, p0, Lfip;->j:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "last_sync_start_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-nez p4, :cond_2

    const-string v2, "last_sync_status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "last_sync_finish_time"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    if-nez p5, :cond_1

    const-string v2, "has_people"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    iget-object v2, p0, Lfip;->i:Lffh;

    iget-object v3, p0, Lfip;->k:Ljava/lang/String;

    iget-object v4, p0, Lfip;->l:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "owners"

    const-string v4, "_id = ?"

    invoke-virtual {v2, v3, v1, v4, v0}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lfip;->i:Lffh;

    invoke-virtual {v0}, Lffh;->b()V

    return-void

    :cond_2
    const-string v2, "last_sync_finish_time"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz p1, :cond_3

    const-string v2, "last_sync_status"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "last_successful_sync_time"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_3
    const-string v2, "last_sync_status"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lfip;->l:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v3, "Owner domain: %s %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v0

    if-eqz p1, :cond_2

    const-string v1, "is_dasher"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "dasher_domain"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "owners"

    const-string v3, "account_name=?"

    iget-object v4, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const-string v2, "is_dasher"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "dasher_domain"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()Ljava/util/Set;
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT qualified_id FROM people WHERE blocked = 0 AND owner_id = ?"

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v2, "qualified_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Create)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lfip;->u()Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "dev_console_id"

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "owner_id"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "applications"

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-direct {p0, p1}, Lfip;->e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V
    .locals 5

    const/4 v3, 0x2

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleSync"

    const-string v2, "    Application: %s - circle: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "INSERT INTO facl_people(dev_console_id, owner_id, qualified_id) SELECT ?, owner_id, qualified_id FROM circle_members WHERE owner_id = ? AND circle_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, p2}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lfip;->e:Landroid/content/Context;

    invoke-static {v1, v0}, Lfip;->a(Landroid/content/Context;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V
    .locals 7

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v0

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleSync"

    const-string v2, "    %s: %s (Update)"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v0}, Lgld;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "circles"

    invoke-direct {p0, v0, p2}, Lfip;->a(Lgld;I)Landroid/content/ContentValues;

    move-result-object v0

    const-string v4, "owner_id = ? AND circle_id = ?"

    invoke-virtual {v2, v3, v0, v4, v1}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 7

    const/4 v6, 0x2

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    invoke-static {p1}, Lfhr;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "PeopleService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lfhr;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    const-string v2, "PeopleSync"

    const-string v3, "    %s: %s [%d Phones, %d Emails, %d Addresses] (Create)"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v2, "people"

    invoke-direct {p0, p1}, Lfip;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-direct {p0, v1, p1}, Lfip;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1}, Lfip;->q(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "- Unknown -"

    goto :goto_0
.end method

.method public final b(Lgkp;)V
    .locals 6

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Update)"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lgkp;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-interface {p1}, Lgkp;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-interface {p1}, Lgkp;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "owners"

    invoke-direct {p0, p1}, Lfip;->c(Lgkp;)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "account_name = ? AND page_gaia_id = ?"

    invoke-virtual {v1, v2, v3, v4, v0}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final b(Lgmb;I)V
    .locals 6

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Update)"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-interface {p1}, Lgmb;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-interface {p1}, Lgmb;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "circles"

    invoke-direct {p0, p1, p2}, Lfip;->c(Lgmb;I)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "owner_id = ? AND circle_id = ?"

    invoke-virtual {v1, v2, v3, v4, v0}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->g()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE FROM sync_tokens WHERE owner_id = ? AND name LIKE ?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final c()Ljava/util/Set;
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT circle_id, type FROM circles WHERE owner_id = ? AND type != -1"

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v2, "circle_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v3, "type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Update)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lfip;->e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    invoke-static {p1}, Lfhr;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "PeopleService"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lfhr;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    const-string v2, "PeopleSync"

    const-string v3, "    %s: %s [%d Phones, %d Emails, %d Addresses] (Update)"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v7

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lfhr;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    aput-object v1, v0, v7

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "people"

    invoke-direct {p0, p1}, Lfip;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "owner_id = ? AND qualified_id = ?"

    invoke-virtual {v2, v3, v4, v5, v0}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0, v1, p1}, Lfip;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1, p1}, Lfip;->d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    invoke-direct {p0, v1}, Lfip;->q(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "- Unknown -"

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 7

    const-wide/16 v5, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->g()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT COUNT(1) FROM sync_tokens WHERE owner_id = ? AND name LIKE ? "

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5, v6}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v5

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Set;
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT circle_id, type FROM circles WHERE owner_id = ? AND type = -1"

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v2, "circle_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v3, "type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Clear fACL)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "facl_people"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "owners"

    const-string v3, "account_name = ? AND page_gaia_id = ?"

    invoke-virtual {v1, v2, v3, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final e()Ljava/util/Set;
    .locals 4

    iget-object v0, p0, Lfip;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL AND account_name = ?"

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v2, "page_gaia_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lfip;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT COUNT(_id) FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v4, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v4, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v5, v6}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v3

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lfip;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT avatar FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 8

    const/4 v1, 0x0

    sget-object v0, Lfbd;->m:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v2, "SELECT last_full_people_sync_time FROM owners WHERE _id=?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v0, p0, Lfip;->f:Lbpe;

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    sget-object v0, Lfbd;->n:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lfip;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT cover_photo_url FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE    FROM owner_sync_requests WHERE account_name=?1 AND page_gaia_id IS NOT NULL  AND page_gaia_id NOT IN  (SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL  AND account_name=?1)"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final h()Ljava/util/List;
    .locals 3

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT page_gaia_id   FROM owner_sync_requests WHERE account_name=?1 AND page_gaia_id IS NOT NULL  AND page_gaia_id NOT IN  (SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL  AND account_name=?1)"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Z
    .locals 6

    const-wide/16 v4, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT COUNT(_id) FROM circles WHERE owner_id=? AND circle_id=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT avatar FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;)Z
    .locals 6

    const-wide/16 v4, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT COUNT(_id) FROM people WHERE owner_id=? AND qualified_id=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT cover_photo_url FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    iget-object v2, p0, Lfip;->k:Ljava/lang/String;

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final j(Ljava/lang/String;)V
    .locals 4

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s - everyone"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "INSERT INTO facl_people(dev_console_id, owner_id, qualified_id) SELECT ?, owner_id, qualified_id FROM people WHERE owner_id =? AND in_circle=1"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lfip;->e:Landroid/content/Context;

    invoke-static {v1, v0}, Lfip;->a(Landroid/content/Context;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final k()Ljava/util/Set;
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfip;->h:Lfbn;

    const-string v3, "SELECT dev_console_id FROM applications WHERE owner_id = ?"

    invoke-virtual {v2, v3, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    const-string v2, "dev_console_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "applications"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-wide v3, p0, Lfip;->j:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final l()V
    .locals 4

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleSync"

    const-string v1, "Clearing gaia-id map."

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object p1, v0, v4

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "people"

    const-string v3, "owner_id = ? AND qualified_id = ?"

    invoke-virtual {v1, v2, v3, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final m()I
    .locals 5

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s(v2): ??? (Delete)"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object p1, v0, v4

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "DELETE FROM people WHERE owner_id = ? AND v2_id = ?"

    invoke-virtual {v1, v2, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final n()I
    .locals 5

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=? AND gaia_id IS NOT NULL AND in_circle=1"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lfip;->j:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object p1, v0, v4

    iget-object v1, p0, Lfip;->h:Lfbn;

    const-string v2, "circles"

    const-string v3, "owner_id = ? AND circle_id = ?"

    invoke-virtual {v1, v2, v3, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final o()I
    .locals 5

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=? AND in_circle=0"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Clear for :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=? AND contact_id=?"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final p(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->f()V

    iget-object v0, p0, Lfip;->h:Lfbn;

    const-string v1, "   SELECT gaia_id FROM gaia_id_map m1  WHERE owner_id=?1 AND contact_id=?2  AND NOT EXISTS(SELECT * FROM gaia_id_map m2  WHERE contact_id<>?2 AND owner_id=?1  AND m1.gaia_id=m2.gaia_id)  AND NOT EXISTS(SELECT * FROM people p  WHERE p.gaia_id=m1.gaia_id AND p.owner_id=?1 AND in_circle=1)"

    iget-wide v2, p0, Lfip;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final p()V
    .locals 3

    iget-object v0, p0, Lfip;->g:Lfbo;

    iget-object v1, p0, Lfip;->k:Ljava/lang/String;

    iget-object v2, p0, Lfip;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lfbo;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final q()V
    .locals 1

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->b()V

    return-void
.end method

.method public final r()V
    .locals 1

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->c()Z

    return-void
.end method

.method public final s()V
    .locals 1

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->e()V

    return-void
.end method

.method public final t()V
    .locals 1

    iget-object v0, p0, Lfip;->h:Lfbn;

    invoke-virtual {v0}, Lfbn;->d()V

    return-void
.end method
