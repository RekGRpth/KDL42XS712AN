.class final Lebb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Landroid/widget/ImageView;

.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/widget/TextView;

.field final f:Landroid/widget/TextView;

.field final g:Landroid/view/View;

.field final h:Landroid/view/View;

.field final synthetic i:Leba;


# direct methods
.method public constructor <init>(Leba;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lebb;->i:Leba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lebb;->a:Landroid/widget/ImageView;

    sget v0, Lxa;->aP:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebb;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebb;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, p0, Lebb;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d()V

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebb;->d:Landroid/widget/TextView;

    sget v0, Lxa;->bf:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebb;->e:Landroid/widget/TextView;

    sget v0, Lxa;->t:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebb;->f:Landroid/widget/TextView;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebb;->g:Landroid/view/View;

    iget-object v0, p0, Lebb;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebb;->h:Landroid/view/View;

    iget-object v0, p0, Lebb;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v3, v0, Lcom/google/android/gms/games/request/GameRequest;

    if-eqz v3, :cond_2

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxa;->ah:I

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iget-object v1, p0, Lebb;->i:Leba;

    invoke-static {v1}, Leba;->a(Leba;)Lebg;

    move-result-object v1

    invoke-interface {v1, v0}, Lebg;->b_(Lcom/google/android/gms/games/Game;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    sget v4, Lxa;->af:I

    if-ne v3, v4, :cond_1

    iget-object v1, p0, Lebb;->i:Leba;

    invoke-virtual {v1}, Leba;->e()Lbgo;

    move-result-object v1

    check-cast v1, Lbhb;

    invoke-virtual {v1, v0}, Lbhb;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lebb;->i:Leba;

    invoke-virtual {v1}, Leba;->notifyDataSetChanged()V

    iget-object v1, p0, Lebb;->i:Leba;

    invoke-static {v1}, Leba;->a(Leba;)Lebg;

    move-result-object v1

    invoke-interface {v1, v0}, Lebg;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/google/android/gms/games/request/GameRequest;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lxa;->aq:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lebb;->i:Leba;

    invoke-static {v1}, Leba;->a(Leba;)Lebg;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/request/GameRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lebg;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lxa;->ap:I

    if-ne v1, v0, :cond_0

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    sget v1, Lxd;->e:I

    invoke-virtual {v0, v1}, Lov;->a(I)V

    new-instance v1, Ledk;

    invoke-direct {v1, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v1, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto :goto_0

    :cond_2
    const-string v1, "PublicRequestListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
