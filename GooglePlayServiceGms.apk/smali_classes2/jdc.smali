.class public final Ljdc;
.super Lizk;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field c:Z

.field public d:Lizf;

.field e:Z

.field f:Lizf;

.field public g:Lizf;

.field private h:Z

.field private i:Z

.field private j:Lizf;

.field private k:Z

.field private l:Z

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lizk;-><init>()V

    iput v0, p0, Ljdc;->a:I

    iput v0, p0, Ljdc;->b:I

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljdc;->d:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljdc;->j:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljdc;->f:Lizf;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Ljdc;->g:Lizf;

    const/4 v0, -0x1

    iput v0, p0, Ljdc;->m:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljdc;->m:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljdc;->b()I

    :cond_0
    iget v0, p0, Ljdc;->m:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljdc;->a(I)Ljdc;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Ljdc;->b(I)Ljdc;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljdc;->a(Lizf;)Ljdc;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljdc;->b(Lizf;)Ljdc;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljdc;->c(Lizf;)Ljdc;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljdc;->d(Lizf;)Ljdc;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(I)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->h:Z

    iput p1, p0, Ljdc;->a:I

    return-object p0
.end method

.method public final a(Lizf;)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->c:Z

    iput-object p1, p0, Ljdc;->d:Lizf;

    return-object p0
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljdc;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdc;->a:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Ljdc;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdc;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-boolean v0, p0, Ljdc;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdc;->d:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_2
    iget-boolean v0, p0, Ljdc;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljdc;->j:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_3
    iget-boolean v0, p0, Ljdc;->k:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljdc;->f:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_4
    iget-boolean v0, p0, Ljdc;->l:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljdc;->g:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_5
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljdc;->h:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdc;->a:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljdc;->i:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdc;->b:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Ljdc;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdc;->d:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljdc;->e:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljdc;->j:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Ljdc;->k:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdc;->f:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Ljdc;->l:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljdc;->g:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Ljdc;->m:I

    return v0
.end method

.method public final b(I)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->i:Z

    iput p1, p0, Ljdc;->b:I

    return-object p0
.end method

.method public final b(Lizf;)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->e:Z

    iput-object p1, p0, Ljdc;->j:Lizf;

    return-object p0
.end method

.method public final c(Lizf;)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->k:Z

    iput-object p1, p0, Ljdc;->f:Lizf;

    return-object p0
.end method

.method public final d(Lizf;)Ljdc;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdc;->l:Z

    iput-object p1, p0, Ljdc;->g:Lizf;

    return-object p0
.end method
