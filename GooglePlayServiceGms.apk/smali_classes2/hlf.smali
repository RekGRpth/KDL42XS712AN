.class public final Lhlf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhln;


# instance fields
.field public final a:Z

.field final b:Lidu;

.field c:Lhlo;

.field private final d:Landroid/hardware/SensorManager;

.field private final e:Landroid/hardware/Sensor;

.field private f:Lhlg;


# direct methods
.method public constructor <init>(Landroid/hardware/SensorManager;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhlf;-><init>(Landroid/hardware/SensorManager;Lidu;)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/SensorManager;Lidu;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhlf;->d:Landroid/hardware/SensorManager;

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lhlf;->e:Landroid/hardware/Sensor;

    iput-object p2, p0, Lhlf;->b:Lidu;

    iget-object v0, p0, Lhlf;->e:Landroid/hardware/Sensor;

    if-eqz v0, :cond_6

    const-string v0, "Nexus 4"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-ne v0, v3, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion not properly supported on Nexus 4 JBMR2"

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lhlf;->a:Z

    return-void

    :cond_1
    const-string v0, "Nexus 7"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion not properly supported on Nexus 7"

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    sget-object v0, Lhjv;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion disabled by Gservices on this device"

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    iget-object v0, p0, Lhlf;->c:Lhlo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhlf;->c:Lhlo;

    iget-object v0, p0, Lhlf;->d:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lhlf;->f:Lhlg;

    iget-object v2, p0, Lhlf;->e:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->cancelTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lhlo;)Z
    .locals 3

    iget-boolean v0, p0, Lhlf;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Significant motion is not supported on this device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhlf;->c:Lhlo;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Significant motion already enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lhlf;->c:Lhlo;

    new-instance v0, Lhlg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhlg;-><init>(Lhlf;B)V

    iput-object v0, p0, Lhlf;->f:Lhlg;

    iget-object v0, p0, Lhlf;->d:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lhlf;->f:Lhlg;

    iget-object v2, p0, Lhlf;->e:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->requestTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lhlf;->a:Z

    return v0
.end method
