.class final enum Laew;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Laew;

.field public static final enum b:Laew;

.field public static final enum c:Laew;

.field public static final enum d:Laew;

.field public static final enum e:Laew;

.field public static final enum f:Laew;

.field private static final synthetic g:[Laew;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Laew;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->a:Laew;

    new-instance v0, Laew;

    const-string v1, "CONNECTED_SERVICE"

    invoke-direct {v0, v1, v4}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->b:Laew;

    new-instance v0, Laew;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v5}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->c:Laew;

    new-instance v0, Laew;

    const-string v1, "PENDING_CONNECTION"

    invoke-direct {v0, v1, v6}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->d:Laew;

    new-instance v0, Laew;

    const-string v1, "PENDING_DISCONNECT"

    invoke-direct {v0, v1, v7}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->e:Laew;

    new-instance v0, Laew;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Laew;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laew;->f:Laew;

    const/4 v0, 0x6

    new-array v0, v0, [Laew;

    sget-object v1, Laew;->a:Laew;

    aput-object v1, v0, v3

    sget-object v1, Laew;->b:Laew;

    aput-object v1, v0, v4

    sget-object v1, Laew;->c:Laew;

    aput-object v1, v0, v5

    sget-object v1, Laew;->d:Laew;

    aput-object v1, v0, v6

    sget-object v1, Laew;->e:Laew;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Laew;->f:Laew;

    aput-object v2, v0, v1

    sput-object v0, Laew;->g:[Laew;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laew;
    .locals 1

    const-class v0, Laew;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laew;

    return-object v0
.end method

.method public static values()[Laew;
    .locals 1

    sget-object v0, Laew;->g:[Laew;

    invoke-virtual {v0}, [Laew;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laew;

    return-object v0
.end method
