.class public final Lheo;
.super Lhgn;
.source "SourceFile"


# instance fields
.field private final a:Lhfb;

.field private final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private final d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

.field private final e:Z

.field private final f:J

.field private g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

.field private h:Ljava/lang/Runnable;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Luu;

.field private l:Lut;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lhfb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Z)V
    .locals 2

    invoke-virtual {p4}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhgn;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lheo;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lheo;->j:Ljava/lang/String;

    iput-object p2, p0, Lheo;->a:Lhfb;

    iput-object p3, p0, Lheo;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p4, p0, Lheo;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lheo;->f:J

    iput-boolean p5, p0, Lheo;->e:Z

    if-eqz p5, :cond_0

    new-instance v0, Luu;

    const-string v1, "load_full_wallet"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lheo;->k:Luu;

    iget-object v0, p0, Lheo;->k:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lheo;->l:Lut;

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lhfb;Landroid/accounts/Account;Ljbg;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lheo;
    .locals 8

    invoke-static/range {p9 .. p9}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lhgg;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgg;->a(Ljava/lang/String;)Lhgg;

    move-result-object v0

    invoke-virtual {v0}, Lhgg;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p3, Ljbg;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p3, Ljbg;->e:Ljava/lang/String;

    invoke-static {v0}, Lhfx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p4, :cond_1

    invoke-static {p4}, Lhfx;->a(Lcom/google/android/gms/wallet/Cart;)Ljaf;

    move-result-object v5

    :cond_0
    :goto_0
    move-object v0, p3

    move-object v1, p5

    move-object v2, p6

    move v3, p7

    move/from16 v4, p8

    invoke-static/range {v0 .. v6}, Lhfx;->a(Ljbg;Ljava/lang/String;Ljava/lang/String;ZZLjaf;Ljava/lang/String;)Ljau;

    move-result-object v1

    new-instance v0, Lheo;

    new-instance v4, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    iget-object v2, p3, Ljbg;->a:Ljan;

    iget-object v2, v2, Ljan;->a:Ljava/lang/String;

    invoke-direct {v4, p2, v1, v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Ljau;Ljava/lang/String;)V

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, v7

    invoke-direct/range {v0 .. v5}, Lheo;-><init>(Landroid/content/Context;Lhfb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Z)V

    return-object v0

    :cond_1
    new-instance v5, Ljaf;

    invoke-direct {v5}, Ljaf;-><init>()V

    iget-object v1, p3, Ljbg;->f:Ljava/lang/String;

    iput-object v1, v5, Ljaf;->b:Ljava/lang/String;

    iput-object v0, v5, Ljaf;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lhfb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lheo;
    .locals 6

    new-instance v0, Lheo;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lheo;-><init>(Landroid/content/Context;Lhfb;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Z)V

    return-object v0
.end method

.method private declared-synchronized i()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lheo;->i:Z

    iget-object v0, p0, Lheo;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lheo;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lheo;->h:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lheo;->j:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v1, Lgsm;

    iget-object v0, p0, Lheo;->b:Landroid/content/Context;

    iget-object v2, p0, Lheo;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lgsm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lheo;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "full_wallet_success"

    :goto_1
    const-string v2, "get_full_wallet"

    iget-object v3, p0, Lheo;->j:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "full_wallet_failure"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private varargs k()Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lheo;->b:Landroid/content/Context;

    invoke-static {v0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PreFetchFullWalletTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FetchFullWalletTask started,isPrefetch="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lheo;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",googTxnId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lheo;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Ljau;

    move-result-object v2

    iget-object v2, v2, Ljau;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v0, p0, Lheo;->a:Lhfb;

    iget-object v1, p0, Lheo;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lheo;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v0, v1, v2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v0

    iput-object v0, p0, Lheo;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lheo;->i()V

    invoke-direct {p0}, Lheo;->j()V

    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "FetchFullWalletTask completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "Exception during fetching of full wallet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lheo;->i()V

    invoke-direct {p0}, Lheo;->j()V

    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "FetchFullWalletTask completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lheo;->i()V

    invoke-direct {p0}, Lheo;->j()V

    const-string v1, "PreFetchFullWalletTask"

    const-string v2, "FetchFullWalletTask completed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lheo;->f:J

    return-wide v0
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lheo;->k()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lheo;->h:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lheo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lheo;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lheo;->j:Ljava/lang/String;

    invoke-virtual {p0}, Lheo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lheo;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lheo;->e:Z

    return v0
.end method

.method public final c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;
    .locals 1

    iget-object v0, p0, Lheo;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lheo;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 1

    iget-object v0, p0, Lheo;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    return-object v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lheo;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lheo;->k:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lheo;->l:Lut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lheo;->k:Luu;

    iget-object v1, p0, Lheo;->l:Lut;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "prefetch_to_actual_service_call"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lheo;->b:Landroid/content/Context;

    iget-object v1, p0, Lheo;->k:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lheo;->k:Luu;

    iput-object v5, p0, Lheo;->l:Lut;

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lheo;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v2

    const/16 v3, 0x10

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljav;

    iget-object v0, v0, Ljav;->i:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
