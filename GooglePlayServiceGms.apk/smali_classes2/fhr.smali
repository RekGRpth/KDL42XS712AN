.class public final Lfhr;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lfhu;->a:Lfhu;

    invoke-static {v0, v1}, Lfhr;->a(Ljava/util/List;Lfhs;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    return-object v0
.end method

.method static a(Ljava/util/List;Lfhs;)Ljava/lang/Object;
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lfhs;->a(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lfhr;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "profile"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Z
    .locals 1

    invoke-static {p0}, Lfhr;->b(Ljava/util/List;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)I
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    goto :goto_0
.end method

.method public static c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lfht;->a:Lfht;

    invoke-static {v0, v1}, Lfhr;->a(Ljava/util/List;Lfhs;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    return-object v0
.end method

.method public static d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lfhr;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lfhy;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to get qualifed ID.  v2id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lfhy;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public static f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-static {p0}, Lfhr;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lfdl;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {v0}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
