.class public final Lfpg;
.super Lbod;
.source "SourceFile"

# interfaces
.implements Lfuc;


# instance fields
.field private final b:Lftz;

.field private final c:Landroid/accounts/Account;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V
    .locals 6

    sget-object v5, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfpg;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lftz;)V
    .locals 0

    invoke-direct {p0, p1}, Lbod;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lfpg;->c:Landroid/accounts/Account;

    iput p3, p0, Lfpg;->d:I

    iput-object p4, p0, Lfpg;->e:Ljava/lang/String;

    iput-object p5, p0, Lfpg;->b:Lftz;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;Lbbr;Lbbs;)Lbbq;
    .locals 2

    iget-object v0, p0, Lfpg;->b:Lftz;

    iget-object v1, p0, Lfpg;->c:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2, p3, v1}, Lfob;->b(Lftz;Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)Lftx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbbo;Lfxi;Ljava/lang/String;)V
    .locals 0

    iput-object p3, p0, Lfpg;->f:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lfpg;->a(Lbbo;Lbgo;)V

    return-void
.end method

.method protected final bridge synthetic a(Lbbq;)V
    .locals 2

    check-cast p1, Lftx;

    iget v0, p0, Lfpg;->d:I

    iget-object v1, p0, Lfpg;->e:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1}, Lftx;->a(Lfuc;ILjava/lang/String;)V

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfpg;->f:Ljava/lang/String;

    return-object v0
.end method
