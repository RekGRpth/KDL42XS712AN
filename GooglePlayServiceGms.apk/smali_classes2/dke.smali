.class final Ldke;
.super Lbqh;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldkb;


# direct methods
.method private constructor <init>(Ldkb;)V
    .locals 0

    iput-object p1, p0, Ldke;->a:Ldkb;

    invoke-direct {p0}, Lbqh;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldkb;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldke;-><init>(Ldkb;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 10

    const/4 v3, 0x0

    const/4 v2, 0x1

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    move v0, v3

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Ldke;->a:Ldkb;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldpi;

    invoke-static {v1, v0}, Ldkb;->a(Ldkb;Ldpi;)V

    move v0, v2

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1, v0}, Ldkb;->a(Ldkb;Ljava/util/ArrayList;)V

    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v1

    iget-object v4, p0, Ldke;->a:Ldkb;

    iget-object v4, v4, Ldkb;->f:Ljava/lang/String;

    iget-object v5, p0, Ldke;->a:Ldkb;

    invoke-static {v0}, Ldkb;->a(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ldrt;->b(Ljava/lang/String;[Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldjx;

    iget-object v1, v0, Ldjx;->a:Ljava/lang/String;

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v4

    invoke-virtual {v4, v1}, Ldkh;->b(Ljava/lang/String;)Ldki;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v4

    iget-object v5, v1, Ldki;->a:Ljava/lang/String;

    invoke-interface {v4, v5}, Ldrt;->k(Ljava/lang/String;)V

    iget-object v4, p0, Ldke;->a:Ldkb;

    const-string v5, "CONNECTION_FAILED"

    invoke-static {v4, v1, v5, v0}, Ldkb;->a(Ldkb;Ldki;Ljava/lang/String;Ldjx;)V

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldjx;

    iget-object v1, v0, Ldjx;->a:Ljava/lang/String;

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v4

    invoke-virtual {v4, v1}, Ldkh;->b(Ljava/lang/String;)Ldki;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v4

    iget-object v5, v1, Ldki;->a:Ljava/lang/String;

    invoke-interface {v4, v5}, Ldrt;->j(Ljava/lang/String;)V

    iget-object v4, p0, Ldke;->a:Ldkb;

    const-string v5, "CONNECTION_ESTABLISHED"

    invoke-static {v4, v1, v5, v0}, Ldkb;->a(Ldkb;Ldki;Ljava/lang/String;Ldjx;)V

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldjz;

    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v1

    iget-object v4, v0, Ldjz;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ldkh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Reliable Message Received from : %s, who is not a participant in the room"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, Ldjz;->b:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Reliable Message Received from : %s, "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Ldjz;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v4

    iget-object v5, v0, Ldjz;->a:[B

    iget v0, v0, Ldjz;->c:I

    invoke-interface {v4, v1, v5, v0}, Ldrt;->a(Ljava/lang/String;[BI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Ldke;->a:Ldkb;

    const/4 v1, 0x3

    new-instance v4, Ldjw;

    iget-object v5, p0, Ldke;->a:Ldkb;

    iget-object v5, v5, Ldkb;->f:Ljava/lang/String;

    invoke-direct {v4, v5, v3, v3}, Ldjw;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1, v4}, Ldkb;->a(ILjava/lang/Object;)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_5
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldka;

    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v1

    iget-object v4, v0, Ldka;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ldkh;->c(Ljava/lang/String;)Ldki;

    move-result-object v1

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v4

    iget-boolean v4, v4, Ldkh;->c:Z

    if-eqz v4, :cond_3

    if-eqz v1, :cond_3

    iget-object v4, v1, Ldki;->b:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-boolean v4, v1, Ldki;->d:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->a(Ldkb;)Ldkk;

    move-result-object v4

    iget-object v5, v0, Ldka;->b:[B

    iget-object v1, v1, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Ldkk;->a([BLjava/lang/String;)I

    move-result v1

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->g(Ldkb;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v0, v0, Ldka;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v1

    const/16 v4, 0x1b5b

    iget v5, v0, Ldka;->a:I

    iget-object v0, v0, Ldka;->c:Ljava/lang/String;

    invoke-interface {v1, v4, v5, v0}, Ldrt;->a(IILjava/lang/String;)V

    goto :goto_2

    :pswitch_6
    iget-object v0, p0, Ldke;->a:Ldkb;

    invoke-static {v0}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v0

    iget-boolean v0, v0, Ldkh;->c:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldkf;

    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1, v0}, Ldkb;->a(Ldkb;Ldkf;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v1, v0, Ldkf;->b:[Ljava/lang/String;

    if-eqz v1, :cond_5

    move v5, v2

    :goto_3
    new-instance v7, Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/HashSet;-><init>(I)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v3

    :goto_4
    if-ge v4, v8, :cond_7

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldki;

    if-eqz v1, :cond_6

    iget-object v9, v1, Ldki;->b:Ljava/lang/String;

    if-eqz v9, :cond_6

    iget-boolean v9, v1, Ldki;->d:Z

    if-eqz v9, :cond_6

    iget-object v1, v1, Ldki;->b:Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    :cond_5
    move v5, v3

    goto :goto_3

    :cond_6
    if-eqz v5, :cond_4

    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v1

    const-string v9, "Attempting to send an unreliable message to participant who is not in connected set."

    invoke-static {v1, v9}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    iget-object v1, p0, Ldke;->a:Ldkb;

    invoke-static {v1}, Ldkb;->a(Ldkb;)Ldkk;

    move-result-object v1

    iget-object v4, v0, Ldkf;->a:[B

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Ldkk;->a([B[Ljava/lang/String;)I

    :goto_6
    move v0, v2

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempting to send an unreliable message to participants when not in connected set."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :pswitch_7
    iget-object v0, p0, Ldke;->a:Ldkb;

    invoke-static {v0}, Ldkb;->g(Ldkb;)Ljava/util/Map;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_a

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->f(Ldkb;)Ldkh;

    move-result-object v4

    invoke-virtual {v4, v1}, Ldkh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v4, p0, Ldke;->a:Ldkb;

    invoke-static {v4}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v4, v5, v0, v1}, Ldrt;->a(IILjava/lang/String;)V

    :cond_9
    move v0, v2

    goto/16 :goto_0

    :cond_a
    invoke-static {}, Ldkb;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Got null token for messageId "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v3

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ldke;->a:Ldkb;

    iget-object v0, v0, Ldkb;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Ldke;->a:Ldkb;

    iget-object v0, v0, Ldkb;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Ldke;->a:Ldkb;

    invoke-static {v0}, Ldkb;->c(Ldkb;)Ldrt;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Ldke;->a:Ldkb;

    iget-object v0, v0, Ldkb;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    :goto_3
    invoke-static {v1}, Lbkm;->a(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method
