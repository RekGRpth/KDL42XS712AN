.class public final Leai;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/content/Context;[Lcom/google/android/gms/games/multiplayer/Participant;)V
    .locals 1

    iput-object p1, p0, Leai;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Leai;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Leai;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->k:I

    invoke-virtual {v0, v1, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Leak;

    iget-object v1, p0, Leai;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-direct {v0, v1, p2}, Leak;-><init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leak;

    iget-object v1, p0, Leai;->a:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)[Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    aget-object v2, v1, p1

    if-nez v2, :cond_1

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    sget v2, Lxf;->aI:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->f:I

    invoke-virtual {v1, v7, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v1, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->c(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Leal;

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    iget-object v0, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lwx;->e:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-object p2

    :cond_1
    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lwz;->f:I

    invoke-virtual {v1, v7, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v5, v0, Leak;->a:Landroid/widget/TextView;

    sget v6, Lxf;->aN:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v5, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v1

    sget v6, Lwz;->f:I

    invoke-virtual {v5, v1, v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    iget-object v5, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v5, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Leak;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v4, :cond_3

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_5

    :cond_3
    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_4
    iget-object v5, v0, Leak;->a:Landroid/widget/TextView;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    iget-object v3, v0, Leak;->d:Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Leak;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
