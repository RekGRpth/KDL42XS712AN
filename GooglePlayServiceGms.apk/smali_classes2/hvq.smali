.class final Lhvq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lixd;


# instance fields
.field final synthetic a:Lhux;

.field final synthetic b:Lhvp;


# direct methods
.method constructor <init>(Lhvp;Lhux;)V
    .locals 0

    iput-object p1, p0, Lhvq;->b:Lhvp;

    iput-object p2, p0, Lhvq;->a:Lhux;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 13

    const/4 v2, 0x1

    const-wide v11, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide v9, 0x3fe3333333333333L    # 0.6

    const/4 v1, 0x0

    iget-object v0, p0, Lhvq;->a:Lhux;

    invoke-virtual {v0}, Lhux;->c()D

    move-result-wide v3

    cmpl-double v0, v3, v11

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvq;->a:Lhux;

    invoke-virtual {v0}, Lhux;->e()D

    move-result-wide v5

    const-wide v7, 0x3ff226c3bcdbe7aeL    # 1.1344640137963142

    cmpl-double v0, v5, v7

    if-lez v0, :cond_4

    :cond_0
    iget-object v0, p0, Lhvq;->a:Lhux;

    invoke-virtual {v0}, Lhux;->d()D

    move-result-wide v3

    move v0, v2

    :goto_0
    cmpl-double v5, v3, v11

    if-eqz v5, :cond_2

    iget-object v5, p0, Lhvq;->b:Lhvp;

    iget-object v5, v5, Lhvp;->k:Liwp;

    double-to-float v3, v3

    iget-object v4, v5, Liwp;->a:Liwm;

    invoke-virtual {v4}, Liwm;->d()V

    iget-object v4, v5, Liwp;->b:Lixa;

    if-nez v0, :cond_1

    iget-object v0, v4, Lixa;->h:Liwz;

    sget-object v6, Liwz;->c:Liwz;

    if-eq v0, v6, :cond_3

    :cond_1
    iget-object v0, v4, Lixa;->b:Liwm;

    invoke-virtual {v0}, Liwm;->d()V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, v4, Lixa;->g:F

    :goto_1
    iget-object v0, v5, Liwp;->c:Liwg;

    :cond_2
    return-void

    :cond_3
    iget-object v0, v4, Lixa;->c:Lixh;

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v6, v9

    invoke-virtual {v0, v1, v1, v6, v7}, Lixh;->a(IID)V

    iget-object v0, v4, Lixa;->c:Lixh;

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v6, v9

    invoke-virtual {v0, v2, v1, v6, v7}, Lixh;->a(IID)V

    iget-object v0, v4, Lixa;->a:Liwi;

    iget-object v1, v4, Lixa;->e:Lixh;

    iget-object v2, v4, Lixa;->d:Lixh;

    iget-object v6, v4, Lixa;->c:Lixh;

    invoke-interface {v0, v1, v2, v6}, Liwi;->a(Lixh;Lixh;Lixh;)V

    iput-wide p1, v4, Lixa;->f:J

    iput v3, v4, Lixa;->g:F

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method
