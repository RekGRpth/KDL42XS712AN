.class public final Lgtd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Lgtf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lgtd;->a:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Lgtf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgtd;->b:Lgtf;

    return-void
.end method

.method synthetic constructor <init>(Lgtf;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgtd;-><init>(Lgtf;)V

    return-void
.end method

.method public static a()Lgte;
    .locals 2

    new-instance v0, Lgte;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lgte;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    iget-object v4, p0, Lgtd;->b:Lgtf;

    new-instance v0, Lgtf;

    iget-boolean v1, v4, Lgtf;->a:Z

    iget-boolean v2, v4, Lgtf;->b:Z

    iget v3, v4, Lgtf;->c:I

    iget-object v4, v4, Lgtf;->d:Ljava/util/Locale;

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lgtf;-><init>(ZZILjava/util/Locale;Ljava/lang/String;)V

    sget-object v1, Lgtd;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/NumberFormat;

    if-nez v1, :cond_4

    invoke-static {p2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    iget-object v1, p0, Lgtd;->b:Lgtf;

    iget-object v4, v1, Lgtf;->d:Ljava/util/Locale;

    iget-object v1, p0, Lgtd;->b:Lgtf;

    iget-boolean v1, v1, Lgtf;->b:Z

    if-eqz v1, :cond_3

    invoke-static {v4}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    :goto_0
    iget-object v1, p0, Lgtd;->b:Lgtf;

    iget-boolean v1, v1, Lgtf;->a:Z

    invoke-virtual {v2, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    invoke-virtual {v3}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    invoke-virtual {v2, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    invoke-virtual {v2, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    :cond_0
    instance-of v1, v2, Ljava/text/DecimalFormat;

    if-eqz v1, :cond_2

    move-object v1, v2

    check-cast v1, Ljava/text/DecimalFormat;

    iget-object v5, p0, Lgtd;->b:Lgtf;

    iget-boolean v5, v5, Lgtf;->b:Z

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v5

    invoke-virtual {v3, v4}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    :cond_1
    iget-object v3, p0, Lgtd;->b:Lgtf;

    iget v3, v3, Lgtf;->c:I

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "en"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Ljava/text/DecimalFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    const-string v4, ";"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    :cond_2
    sget-object v1, Lgtd;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-virtual {v2, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-static {v4}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    goto :goto_0

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method
