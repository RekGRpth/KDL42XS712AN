.class final Lhsr;
.super Lhrx;
.source "SourceFile"


# instance fields
.field final a:Landroid/net/wifi/WifiManager;

.field final b:Lhta;

.field private final g:Landroid/content/Context;

.field private h:Landroid/net/wifi/WifiManager$WifiLock;

.field private final i:Landroid/content/BroadcastReceiver;

.field private volatile j:I

.field private k:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Lhqm;Lhta;Lhqq;Limb;Lilx;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lhrx;-><init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V

    new-instance v0, Lhss;

    invoke-direct {v0, p0}, Lhss;-><init>(Lhsr;)V

    iput-object v0, p0, Lhsr;->i:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput v0, p0, Lhsr;->j:I

    new-instance v0, Lhst;

    invoke-direct {v0, p0}, Lhst;-><init>(Lhsr;)V

    iput-object v0, p0, Lhsr;->k:Ljava/lang/Runnable;

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lhsr;->g:Landroid/content/Context;

    iput-object p3, p0, Lhsr;->b:Lhta;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lhsr;->a:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic a(Lhsr;)V
    .locals 0

    invoke-direct {p0}, Lhsr;->c()V

    return-void
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lhrx;->d:Lhqm;

    iget-object v1, p0, Lhsr;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lhqm;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lhsr;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    iget-object v0, p0, Lhsr;->e:Lhqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsr;->e:Lhqq;

    iget v1, p0, Lhsr;->j:I

    invoke-interface {v0, v1}, Lhqq;->e_(I)V

    :cond_0
    iget v0, p0, Lhsr;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhsr;->j:I

    iget-object v0, p0, Lhrx;->d:Lhqm;

    iget-object v1, p0, Lhsr;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lhqm;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    iget-object v0, p0, Lhsr;->a:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x2

    const-string v2, "WifiScanner"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    iget-object v0, p0, Lhsr;->g:Landroid/content/Context;

    iget-object v1, p0, Lhsr;->i:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lhsr;->c()V

    return-void
.end method

.method protected final b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsr;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_0
    iget-object v0, p0, Lhsr;->g:Landroid/content/Context;

    iget-object v1, p0, Lhsr;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lhsr;->e:Lhqq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhsr;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->f()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
