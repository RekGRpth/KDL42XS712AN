.class final Lhkz;
.super Lhla;
.source "SourceFile"

# interfaces
.implements Lhlo;


# instance fields
.field final synthetic a:Lhkr;

.field private b:Z


# direct methods
.method private constructor <init>(Lhkr;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lhkz;->a:Lhkr;

    invoke-direct {p0, p1, v0}, Lhla;-><init>(Lhkr;B)V

    iput-boolean v0, p0, Lhkz;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lhkr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhkz;-><init>(Lhkr;)V

    return-void
.end method


# virtual methods
.method protected final a(Lhlt;)V
    .locals 4

    iget-object v0, p1, Lhlt;->a:Lhup;

    sget-object v1, Lhup;->b:Lhup;

    if-ne v0, v1, :cond_1

    iget-wide v0, p1, Lhlt;->b:D

    const-wide v2, 0x3fe6666666666666L    # 0.7

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Location Changed. Leaving low power mode."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkz;->a:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    iget-object v0, p0, Lhkz;->a:Lhkr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Z)V

    :cond_1
    return-void
.end method

.method protected final a(Z)V
    .locals 6

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v2, 0x3

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    iget-object v2, p0, Lhkz;->a:Lhkr;

    invoke-static {v2}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v2

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    iget-object v4, p0, Lhkz;->a:Lhkr;

    invoke-static {v4}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v4

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    iget-object v1, p0, Lhkz;->a:Lhkr;

    invoke-static {v1, v0}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    return-void
.end method

.method protected final d()V
    .locals 1

    invoke-super {p0}, Lhla;->d()V

    iget-object v0, p0, Lhkz;->a:Lhkr;

    invoke-static {v0}, Lhkr;->s(Lhkr;)Lhln;

    move-result-object v0

    invoke-interface {v0, p0}, Lhln;->a(Lhlo;)Z

    return-void
.end method

.method protected final e()V
    .locals 1

    invoke-super {p0}, Lhla;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhkz;->b:Z

    iget-object v0, p0, Lhkz;->a:Lhkr;

    invoke-static {v0}, Lhkr;->s(Lhkr;)Lhln;

    move-result-object v0

    invoke-interface {v0}, Lhln;->a()Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-boolean v0, p0, Lhkz;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    const-string v1, "Significant motion detected"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhkz;->a:Lhkr;

    invoke-static {v0}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v0

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    sget-object v1, Licn;->T:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lhkz;->a:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    iget-object v0, p0, Lhkz;->a:Lhkr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Z)V

    goto :goto_0
.end method
