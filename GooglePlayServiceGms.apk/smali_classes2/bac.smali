.class public final Lbac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawd;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final i:Ljava/util/concurrent/atomic/AtomicLong;

.field private static s:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final A:J

.field private final B:Ljava/lang/Runnable;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:I

.field private G:J

.field private final H:Lbap;

.field private final I:Ljava/lang/Runnable;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private final L:Landroid/os/PowerManager$WakeLock;

.field private M:Ljava/lang/String;

.field private final b:Laye;

.field private c:Ljava/lang/Integer;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/google/android/gms/cast/CastDevice;

.field private final g:Lbai;

.field private final h:Lawc;

.field private final j:Lbah;

.field private final k:Laww;

.field private final l:Laws;

.field private m:Lawt;

.field private n:Lawu;

.field private final o:Ljava/util/Set;

.field private final p:Ljava/util/Map;

.field private final q:Ljava/lang/String;

.field private r:D

.field private t:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Z

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lbac;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lbac;->i:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lbac;->s:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;JLbai;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lbac;->A:J

    new-instance v0, Lbbj;

    const-string v1, "CastDeviceController"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbac;->b:Laye;

    sget-object v0, Lbac;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    const-string v0, "instance-%d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->M:Ljava/lang/String;

    iget-object v0, p0, Lbac;->b:Laye;

    iget-object v2, p0, Lbac;->M:Ljava/lang/String;

    invoke-virtual {v0, v2}, Laye;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, v5}, Ljava/lang/Integer;-><init>(I)V

    iput-object v0, p0, Lbac;->c:Ljava/lang/Integer;

    iput-boolean v5, p0, Lbac;->E:Z

    iput-object p1, p0, Lbac;->d:Landroid/content/Context;

    iput-object p2, p0, Lbac;->e:Landroid/os/Handler;

    iput-object p3, p0, Lbac;->f:Lcom/google/android/gms/cast/CastDevice;

    iput-object p7, p0, Lbac;->g:Lbai;

    invoke-virtual {p0, p5, p6}, Lbac;->a(J)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbac;->p:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbac;->o:Ljava/util/Set;

    iput v5, p0, Lbac;->F:I

    new-instance v0, Lbap;

    invoke-direct {v0}, Lbap;-><init>()V

    iput-object v0, p0, Lbac;->H:Lbap;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v2, "%s-%d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lawc;

    invoke-direct {v0, p1, p0}, Lawc;-><init>(Landroid/content/Context;Lawd;)V

    iput-object v0, p0, Lbac;->h:Lawc;

    new-instance v0, Lbah;

    invoke-direct {v0, p0, v5}, Lbah;-><init>(Lbac;B)V

    iput-object v0, p0, Lbac;->j:Lbah;

    new-instance v0, Lbad;

    const-string v1, "receiver-0"

    invoke-direct {v0, p0, v1}, Lbad;-><init>(Lbac;Ljava/lang/String;)V

    iput-object v0, p0, Lbac;->k:Laww;

    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {p0, v0}, Lbac;->a(Laxx;)V

    new-instance v0, Laws;

    invoke-direct {v0, p4}, Laws;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbac;->l:Laws;

    iget-object v0, p0, Lbac;->l:Laws;

    invoke-virtual {p0, v0}, Lbac;->a(Laxx;)V

    const-string v0, "%s-%d"

    new-array v1, v7, [Ljava/lang/Object;

    aput-object p4, v1, v5

    sget-object v2, Lbac;->s:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->q:Ljava/lang/String;

    new-instance v0, Lbae;

    invoke-direct {v0, p0}, Lbae;-><init>(Lbac;)V

    iput-object v0, p0, Lbac;->B:Ljava/lang/Runnable;

    new-instance v0, Lbaf;

    invoke-direct {v0, p0}, Lbaf;-><init>(Lbac;)V

    iput-object v0, p0, Lbac;->I:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lbac;D)D
    .locals 0

    iput-wide p1, p0, Lbac;->r:D

    return-wide p1
.end method

.method public static a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;JLbai;)Lbac;
    .locals 8

    new-instance v0, Lbac;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-wide v5, p4

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lbac;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;JLbai;)V

    invoke-direct {v0}, Lbac;->s()V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;Lbai;)Lbac;
    .locals 7

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lbac;->a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;JLbai;)Lbac;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbac;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbac;->u:Ljava/lang/String;

    return-object v0
.end method

.method private a(IZ)V
    .locals 12

    const-wide/16 v0, -0x1

    const/4 v11, 0x0

    const-wide/16 v2, 0x0

    iput-boolean v11, p0, Lbac;->C:Z

    iget-object v4, p0, Lbac;->b:Laye;

    const-string v5, "handleConnectionFailure; statusCode=%d, mDisconnectStatusCode=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    const/4 v7, 0x1

    iget v8, p0, Lbac;->F:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p2, :cond_4

    iget-object v4, p0, Lbac;->H:Lbap;

    iget-wide v5, v4, Lbap;->d:J

    cmp-long v5, v5, v2

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    iget-object v2, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v3, p0, Lbac;->I:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_1
    return-void

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, v4, Lbap;->c:J

    cmp-long v7, v7, v2

    if-eqz v7, :cond_3

    iget-wide v7, v4, Lbap;->d:J

    sub-long v7, v5, v7

    iget-wide v9, v4, Lbap;->b:J

    cmp-long v7, v7, v9

    if-ltz v7, :cond_2

    iput-wide v2, v4, Lbap;->d:J

    goto :goto_0

    :cond_2
    iget-wide v0, v4, Lbap;->a:J

    iget-wide v7, v4, Lbap;->c:J

    sub-long v4, v5, v7

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    :cond_3
    move-wide v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    :cond_5
    iget v0, p0, Lbac;->F:I

    if-eqz v0, :cond_6

    iget v0, p0, Lbac;->F:I

    iput v11, p0, Lbac;->F:I

    :cond_6
    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->c()V

    goto :goto_1
.end method

.method private a(Lavw;Z)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "connectToApplicationAndNotify"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lavw;->c()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lbac;->l:Laws;

    invoke-virtual {v1, v0}, Laws;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "setting current transport ID to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lbac;->k:Laww;

    invoke-virtual {v1, v0}, Laww;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lavw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Ljava/lang/String;)Lavx;

    move-result-object v1

    invoke-virtual {p1}, Lavw;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V

    invoke-virtual {p1}, Lavw;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/WebImage;

    iget-object v3, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/common/images/WebImage;)V

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lbac;->k:Laww;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laww;->c(Ljava/lang/String;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Lavw;->f()Ljava/util/List;

    move-result-object v0

    iget-object v2, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/util/List;)V

    invoke-virtual {p1}, Lavw;->d()Lawr;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lawr;->b:Ljava/lang/String;

    iget-object v3, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v3, v2}, Lcom/google/android/gms/cast/ApplicationMetadata;->b(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V

    iget-object v0, v0, Lawr;->c:Landroid/net/Uri;

    iget-object v2, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Landroid/net/Uri;)V

    :cond_1
    invoke-virtual {p1}, Lavw;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->v:Ljava/lang/String;

    iget-object v0, v1, Lavx;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object v0, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {p1}, Lavw;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->u:Ljava/lang/String;

    invoke-virtual {p1}, Lavw;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->J:Ljava/lang/String;

    iget-object v0, p0, Lbac;->u:Ljava/lang/String;

    iput-object v0, p0, Lbac;->K:Ljava/lang/String;

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbac;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->j_()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbac;->g:Lbai;

    iget-object v1, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v2, p0, Lbac;->v:Ljava/lang/String;

    iget-object v3, p0, Lbac;->u:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p2}, Lbai;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method static synthetic a(Lbac;I)V
    .locals 0

    invoke-direct {p0, p1}, Lbac;->e(I)V

    return-void
.end method

.method static synthetic a(Lbac;Lavw;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbac;->a(Lavw;Z)V

    return-void
.end method

.method static synthetic a(Lbac;Lavw;DZZ)V
    .locals 9

    const/16 v2, 0x7d5

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v3, "processReceiverStatus: applicationInfo=%s, volume=%f, muteState=%b"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x2

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lavw;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbac;->v:Ljava/lang/String;

    iget-boolean v0, p0, Lbac;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->g:Lbai;

    iget-object v3, p0, Lbac;->v:Ljava/lang/String;

    invoke-interface {v0, v3, p2, p3, p4}, Lbai;->a(Ljava/lang/String;DZ)V

    :cond_0
    if-eqz p5, :cond_1

    iget-object v0, p0, Lbac;->y:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lavw;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v3, p0, Lbac;->y:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lbac;->y:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v3, "application to join (%s) is NOT available!"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lbac;->y:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->y:Ljava/lang/String;

    iput-object v1, p0, Lbac;->y:Ljava/lang/String;

    iput-object v1, p0, Lbac;->z:Ljava/lang/String;

    iput-object v1, p0, Lbac;->J:Ljava/lang/String;

    iput-object v1, p0, Lbac;->K:Ljava/lang/String;

    iget-boolean v3, p0, Lbac;->x:Z

    if-eqz v3, :cond_4

    iput-boolean v7, p0, Lbac;->x:Z

    invoke-virtual {p0, v0, v1, v8}, Lbac;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->k_()V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "calling mListener.onApplicationConnectionFailed"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0, v2}, Lbai;->b(I)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lbac;->z:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbac;->z:Ljava/lang/String;

    invoke-virtual {p1}, Lavw;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lbac;->y:Ljava/lang/String;

    iput-object v1, p0, Lbac;->y:Ljava/lang/String;

    iput-object v1, p0, Lbac;->z:Ljava/lang/String;

    iget-boolean v3, p0, Lbac;->x:Z

    if-eqz v3, :cond_7

    iput-boolean v7, p0, Lbac;->x:Z

    invoke-virtual {p0, v0, v1, v8}, Lbac;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0, v2}, Lbai;->b(I)V

    goto :goto_2

    :cond_8
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lavw;->e()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lbac;->v:Ljava/lang/String;

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lavw;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-direct {p0, p1, v7}, Lbac;->a(Lavw;Z)V

    :goto_4
    iput-object v1, p0, Lbac;->y:Ljava/lang/String;

    iput-object v1, p0, Lbac;->z:Ljava/lang/String;

    iput-boolean v7, p0, Lbac;->x:Z

    goto :goto_2

    :cond_9
    move-object v0, v1

    goto :goto_3

    :cond_a
    iget-object v3, p0, Lbac;->g:Lbai;

    const-string v0, ""

    iget-object v4, p0, Lbac;->y:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_5
    invoke-interface {v3, v0}, Lbai;->b(I)V

    goto :goto_4

    :cond_b
    const/16 v0, 0x7d4

    goto :goto_5
.end method

.method private a(Ljava/nio/ByteBuffer;Ljava/lang/String;J)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0, p1}, Lawc;->a(Ljava/nio/ByteBuffer;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0, p2, p3, p4}, Lbai;->a(Ljava/lang/String;J)V
    :try_end_0
    .catch Lavz; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lawn; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d7

    invoke-interface {v0, p2, p3, p4, v1}, Lbai;->a(Ljava/lang/String;JI)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d6

    invoke-interface {v0, p2, p3, p4, v1}, Lbai;->a(Ljava/lang/String;JI)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lbac;->g:Lbai;

    const/4 v1, 0x7

    invoke-interface {v0, p2, p3, p4, v1}, Lbai;->a(Ljava/lang/String;JI)V

    goto :goto_0
.end method

.method static synthetic b(Lbac;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    iget-object v0, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object v0
.end method

.method static synthetic c(Lbac;)Lbai;
    .locals 1

    iget-object v0, p0, Lbac;->g:Lbai;

    return-object v0
.end method

.method static synthetic d(Lbac;)Z
    .locals 1

    iget-boolean v0, p0, Lbac;->w:Z

    return v0
.end method

.method private e(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lbac;->l:Laws;

    iget-object v1, p0, Lbac;->k:Laww;

    invoke-virtual {v1}, Laww;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laws;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0, v4}, Laww;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lbac;->u:Ljava/lang/String;

    iget-object v1, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-eqz v1, :cond_1

    iput-object v4, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object v4, p0, Lbac;->u:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lbac;->g:Lbai;

    invoke-interface {v1, p1, v0}, Lbai;->a(ILjava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while leaving application"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0
.end method

.method static synthetic e(Lbac;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbac;->w:Z

    return v0
.end method

.method static synthetic f(Lbac;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->J:Ljava/lang/String;

    return-object v0
.end method

.method private f(I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "finishDisconnecting; socketError=%d, mDisconnectStatusCode=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Lbac;->F:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v5, p0, Lbac;->C:Z

    iput-boolean v5, p0, Lbac;->D:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbac;->r:D

    iput-object v6, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object v6, p0, Lbac;->u:Ljava/lang/String;

    iput-object v6, p0, Lbac;->v:Ljava/lang/String;

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbac;->B:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget v0, p0, Lbac;->F:I

    if-eqz v0, :cond_0

    iget v0, p0, Lbac;->F:I

    iput v5, p0, Lbac;->F:I

    :goto_0
    iget-object v1, p0, Lbac;->H:Lbap;

    invoke-virtual {v1}, Lbap;->a()Z

    iget-object v1, p0, Lbac;->g:Lbai;

    invoke-interface {v1, v0}, Lbai;->a(I)V

    return-void

    :cond_0
    invoke-static {p1}, Lbac;->g(I)I

    move-result v0

    goto :goto_0
.end method

.method private static g(I)I
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    const/16 v0, 0xf

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    if-ne p0, v0, :cond_2

    const/16 v0, 0x7d0

    goto :goto_0

    :cond_2
    const/4 v0, 0x7

    goto :goto_0
.end method

.method static synthetic g(Lbac;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lbac;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbac;->y:Ljava/lang/String;

    return-object v0
.end method

.method private h(I)V
    .locals 1

    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0}, Lawc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lbac;->F:I

    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0}, Lawc;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lbac;->f(I)V

    goto :goto_0
.end method

.method static synthetic i(Lbac;)Lbap;
    .locals 1

    iget-object v0, p0, Lbac;->H:Lbap;

    return-object v0
.end method

.method static synthetic j(Lbac;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lbac;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lbac;)Lawu;
    .locals 1

    iget-object v0, p0, Lbac;->n:Lawu;

    return-object v0
.end method

.method static synthetic m(Lbac;)Laye;
    .locals 1

    iget-object v0, p0, Lbac;->b:Laye;

    return-object v0
.end method

.method static synthetic n(Lbac;)V
    .locals 1

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lbac;->h(I)V

    return-void
.end method

.method static synthetic o(Lbac;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lbac;->B:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic p(Lbac;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic q(Lbac;)Laww;
    .locals 1

    iget-object v0, p0, Lbac;->k:Laww;

    return-object v0
.end method

.method static synthetic r(Lbac;)Lawt;
    .locals 1

    iget-object v0, p0, Lbac;->m:Lawt;

    return-object v0
.end method

.method static synthetic r()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    sget-object v0, Lbac;->i:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic s(Lbac;)Lawt;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->m:Lawt;

    return-object v0
.end method

.method private declared-synchronized s()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lbac;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized t()V
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-lez v0, :cond_1

    :try_start_1
    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Unbalanced call in releasing the wake lock. Released %d locks."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method static synthetic t(Lbac;)V
    .locals 6

    const/4 v5, 0x7

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "finishConnecting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lbac;->l:Laws;

    const-string v1, "receiver-0"

    invoke-virtual {v0, v1}, Laws;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lawu;

    invoke-direct {v0}, Lawu;-><init>()V

    iput-object v0, p0, Lbac;->n:Lawu;

    iget-object v0, p0, Lbac;->n:Lawu;

    invoke-virtual {p0, v0}, Lbac;->a(Laxx;)V

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbac;->B:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbac;->D:Z

    iput-boolean v4, p0, Lbac;->C:Z

    iget-object v0, p0, Lbac;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lbac;->x:Z

    iget-object v0, p0, Lbac;->J:Ljava/lang/String;

    iget-object v1, p0, Lbac;->K:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lbac;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v5}, Lbac;->h(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->j_()V

    :try_start_1
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, v5}, Lbac;->h(I)V

    goto :goto_0
.end method

.method static synthetic u(Lbac;)V
    .locals 2

    const/16 v0, 0x7d0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbac;->a(IZ)V

    return-void
.end method

.method static synthetic v(Lbac;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastIntentService;->e(Landroid/content/Context;Lbac;)V

    return-void
.end method

.method public final a(DDZ)V
    .locals 7

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;DDZ)V

    return-void
.end method

.method public final a(I)V
    .locals 5

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "onConnectionFailed; socketError=%d, disposed:%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lbac;->E:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;I)V

    return-void
.end method

.method public final a(J)V
    .locals 4

    iget-wide v0, p0, Lbac;->G:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-wide p1, p0, Lbac;->G:J

    iget-wide v0, p0, Lbac;->G:J

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lbac;->b:Laye;

    invoke-virtual {v1, v0}, Laye;->a(Z)V

    invoke-static {v0}, Laye;->b(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Laxx;)V
    .locals 2

    iget-object v0, p0, Lbac;->j:Lbah;

    invoke-virtual {p1, v0}, Laxx;->a(Layh;)V

    iget-object v0, p0, Lbac;->p:Ljava/util/Map;

    invoke-virtual {p1}, Laxx;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    const-string v0, "%s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbac;->M:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbac;->M:Ljava/lang/String;

    iget-object v0, p0, Lbac;->b:Laye;

    iget-object v1, p0, Lbac;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laye;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "reconnectToDevice: lastApplicationId=%s, lastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Lbac;->J:Ljava/lang/String;

    iput-object p2, p0, Lbac;->K:Ljava/lang/String;

    invoke-virtual {p0}, Lbac;->b()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    iget-object v1, p0, Lbac;->k:Laww;

    invoke-virtual {v1}, Laww;->b()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "ignoring attempt to send a text message with no destination ID"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d1

    invoke-interface {v0, p1, p3, p4, v1}, Lbai;->a(Ljava/lang/String;JI)V

    :goto_0
    return-void

    :cond_0
    if-nez p5, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The application has not launched yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while sending message"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Lazw;

    invoke-direct {v0}, Lazw;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazw;->a(I)Lazw;

    move-result-object v0

    iget-object v1, p0, Lbac;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lazw;->a(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, p5}, Lazw;->b(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lazw;->c(Ljava/lang/String;)Lazw;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazw;->b(I)Lazw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lazw;->d(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0}, Lazw;->d()[B

    move-result-object v0

    array-length v1, v0

    const/high16 v2, 0x10000

    if-le v1, v2, :cond_2

    new-instance v0, Lawn;

    invoke-direct {v0}, Lawn;-><init>()V

    throw v0

    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-direct {p0, v0, p1, p3, p4}, Lbac;->a(Ljava/nio/ByteBuffer;Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "launchApplicationInternal() id=%s, relaunch=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d4

    invoke-interface {v0, v1}, Lbai;->b(I)V

    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    :try_start_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0, p1, p2}, Laww;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while launching application"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0

    :cond_2
    iput-boolean v5, p0, Lbac;->x:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbac;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "launchApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1, p2}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 7

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    iget-object v1, p0, Lbac;->k:Laww;

    invoke-virtual {v1}, Laww;->b()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/lang/String;[BJLjava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;[BJLjava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "ignoring attempt to send a binary message with no destination ID"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d1

    invoke-interface {v0, p1, p3, p4, v1}, Lbai;->a(Ljava/lang/String;JI)V

    :goto_0
    return-void

    :cond_0
    if-nez p5, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The application has not launched yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while sending message"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Lazw;

    invoke-direct {v0}, Lazw;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazw;->a(I)Lazw;

    move-result-object v0

    iget-object v1, p0, Lbac;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lazw;->a(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, p5}, Lazw;->b(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lazw;->c(Ljava/lang/String;)Lazw;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazw;->b(I)Lazw;

    move-result-object v0

    invoke-static {p2}, Lizf;->a([B)Lizf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazw;->a(Lizf;)Lazw;

    move-result-object v0

    invoke-virtual {v0}, Lazw;->d()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-direct {p0, v0, p1, p3, p4}, Lbac;->a(Ljava/nio/ByteBuffer;Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbac;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbac;->c:Ljava/lang/Integer;

    sget-object v0, Laxa;->d:Lbfy;

    invoke-static {v0}, Lbhv;->a(Lbfy;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(ZDZ)V
    .locals 6

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;ZDZ)V

    return-void
.end method

.method public final b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "already reconnecting; ignoring"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "calling CastIntentService.connectToDevice"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;)V

    goto :goto_0
.end method

.method public final b(DDZ)V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lbac;->k:Laww;

    move-wide v1, p1

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Laww;->a(DDZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while setting volume"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "onDisconnected; socketError=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->n:Lawu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->n:Lawu;

    invoke-virtual {p0, v0}, Lbac;->b(Laxx;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->n:Lawu;

    :cond_0
    iget-boolean v0, p0, Lbac;->E:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lbac;->f(I)V

    :goto_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->c()V

    return-void

    :cond_1
    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->b(Landroid/content/Context;Lbac;I)V

    goto :goto_0
.end method

.method public final b(Laxx;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Laxx;->a(Layh;)V

    iget-object v0, p0, Lbac;->p:Ljava/util/Map;

    invoke-virtual {p1}, Laxx;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "stopApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "joinApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lbac;->n:Lawu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbac;->n:Lawu;

    invoke-virtual {v0}, Lawu;->a()V

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0}, Lazw;->a([B)Lazw;
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v2, v1, Lazw;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Received a message with an empty or missing namespace"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Received an unparseable protobuf: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lizj;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget v3, v1, Lazw;->b:I

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v4, "onMessageReceived. namespace: %s payloadType: %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v0, v4, v5}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->p:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    if-eqz v0, :cond_2

    packed-switch v3, :pswitch_data_0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Unknown payload type %s; discarding message"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lbac;->b:Laye;

    const-string v3, "onMessageReceived, Received message: %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v1, Lazw;->c:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v1, Lazw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laxx;->a_(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, v1, Lazw;->d:Lizf;

    invoke-virtual {v1}, Lizf;->a()I

    move-result v2

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Lizf;->b([B)V

    invoke-virtual {v0, v2}, Laxx;->a([B)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lbac;->o:Ljava/util/Set;

    monitor-enter v4

    :try_start_1
    iget-object v0, p0, Lbac;->o:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_3

    packed-switch v3, :pswitch_data_1

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Unknown payload type %s; discarding message"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :pswitch_2
    iget-object v0, p0, Lbac;->g:Lbai;

    iget-object v1, v1, Lazw;->c:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lbai;->a_(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, v1, Lazw;->d:Lizf;

    invoke-virtual {v0}, Lizf;->a()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, Lizf;->b([B)V

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0, v2, v1}, Lbai;->a(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Ignoring message. Namespace has not been registered."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized b(Z)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbac;->L:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    :goto_0
    iget-object v2, p0, Lbac;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_4

    iget-object v2, p0, Lbac;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lbac;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbac;->E:Z

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "[%s] *** disposing ***"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbac;->f:Lcom/google/android/gms/cast/CastDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lbac;->t()V

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbac;->B:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbac;->e:Landroid/os/Handler;

    iget-object v1, p0, Lbac;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lbac;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Lbac;->b:Laye;

    const-string v3, "Unbalanced call in releasing the wake lock."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Laye;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    :try_start_2
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v2, "Unbalanced call to releaseReference(); mDisposed=%b"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lbac;->E:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Laye;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method public final b(ZDZ)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0, p1, p2, p3, p4}, Laww;->a(ZDZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while setting mute state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0}, Lawc;->f()I

    move-result v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "connectToDeviceInternal; socket state = %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eq v0, v6, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Redundant call to connect to device"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    iput-boolean v6, p0, Lbac;->C:Z

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lbap;->c:J

    iput-wide v1, v0, Lbap;->d:J

    :cond_2
    iget-object v0, p0, Lbac;->H:Lbap;

    iget-wide v1, v0, Lbap;->d:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lbap;->c:J

    :cond_3
    :try_start_0
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "connecting socket now"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->h:Lawc;

    iget-object v1, p0, Lbac;->f:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v1

    iget-object v2, p0, Lbac;->f:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lawc;->a(Ljava/net/Inet4Address;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "connection exception"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0, v6}, Lbac;->a(IZ)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "onSocketConnectionFailedInternal: socketError=%d"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lbac;->g(I)I

    move-result v0

    invoke-direct {p0, v0, v5}, Lbac;->a(IZ)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "stopApplicationInternal() sessionId=%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbac;->w:Z

    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0, p1}, Laww;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while stopping application"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "joinApplicationInternal(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lbac;->u:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "already connected to requested app, so skipping join logic"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->g:Lbai;

    iget-object v1, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v2, p0, Lbac;->v:Ljava/lang/String;

    iget-object v3, p0, Lbac;->u:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lbai;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "clearing mLastConnected* variables"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v5, p0, Lbac;->J:Ljava/lang/String;

    iput-object v5, p0, Lbac;->K:Ljava/lang/String;

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->k_()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbac;->g:Lbai;

    const/16 v1, 0x7d5

    invoke-interface {v0, v1}, Lbai;->b(I)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    :goto_1
    iput-object p1, p0, Lbac;->y:Ljava/lang/String;

    iput-object p2, p0, Lbac;->z:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while requesting device status for join"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0

    :cond_4
    const-string p1, ""

    goto :goto_1
.end method

.method public final d(I)V
    .locals 5

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "onSocketDisconnectedInternal: socketError=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lbac;->f(I)V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->b(Landroid/content/Context;Lbac;Ljava/lang/String;)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lbac;->D:Z

    return v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbac;->o:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbac;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lbac;->C:Z

    return v0
.end method

.method public final f()D
    .locals 2

    iget-wide v0, p0, Lbac;->r:D

    return-wide v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastIntentService;->c(Landroid/content/Context;Lbac;Ljava/lang/String;)V

    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbac;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbac;->o:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbac;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()J
    .locals 2

    iget-wide v0, p0, Lbac;->G:J

    return-wide v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastIntentService;->b(Landroid/content/Context;Lbac;)V

    return-void
.end method

.method public final j()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "disconnectFromDeviceInternal: disposed: %b isSocketConnected(): %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lbac;->E:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lbac;->h:Lawc;

    invoke-virtual {v3}, Lawc;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->H:Lbap;

    invoke-virtual {v0}, Lbap;->a()Z

    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0}, Lawc;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "disconnecting from app %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lbac;->l:Laws;

    invoke-virtual {v1, v0}, Laws;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lbac;->h:Lawc;

    invoke-virtual {v0}, Lawc;->b()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "Socket is NOT connected."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastIntentService;->c(Landroid/content/Context;Lbac;)V

    return-void
.end method

.method public final l()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "leaveApplicationInternal()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->t:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbac;->g:Lbai;

    invoke-interface {v0}, Lbai;->d()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v3}, Lbac;->e(I)V

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lbac;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastIntentService;->d(Landroid/content/Context;Lbac;)V

    return-void
.end method

.method public final n()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Error while stopping application"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbac;->h(I)V

    goto :goto_0
.end method

.method public final o()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbac;->b:Laye;

    const-string v1, "onSocketConnectedInternal"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lbag;

    const-string v1, "receiver-0"

    iget-object v2, p0, Lbac;->h:Lawc;

    invoke-virtual {v2}, Lawc;->g()[B

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbag;-><init>(Lbac;Ljava/lang/String;[B)V

    iput-object v0, p0, Lbac;->m:Lawt;

    iget-object v0, p0, Lbac;->m:Lawt;

    invoke-virtual {p0, v0}, Lbac;->a(Laxx;)V

    :try_start_0
    iget-object v0, p0, Lbac;->m:Lawt;

    invoke-virtual {v0}, Lawt;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbac;->b:Laye;

    const-string v2, "Not able to authenticated Cast Device"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbac;->m:Lawt;

    invoke-virtual {p0, v0}, Lbac;->b(Laxx;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbac;->m:Lawt;

    const/16 v0, 0x7d0

    invoke-direct {p0, v0, v4}, Lbac;->a(IZ)V

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbac;->k:Laww;

    invoke-virtual {v0}, Laww;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized q()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lbac;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
