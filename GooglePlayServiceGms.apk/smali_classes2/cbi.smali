.class public final Lcbi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcos;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcbi;->a:Landroid/content/Context;

    return-void
.end method

.method private static a(Landroid/net/NetworkInfo;)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/net/NetworkInfo;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Landroid/net/NetworkInfo;
    .locals 2

    iget-object v0, p0, Lcbi;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    invoke-direct {p0}, Lcbi;->d()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lcbi;->b(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    invoke-direct {p0}, Lcbi;->d()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lcbi;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public final c()Lcot;
    .locals 2

    invoke-direct {p0}, Lcbi;->d()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lcbi;->b(Landroid/net/NetworkInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lcot;->a:Lcot;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcbi;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcot;->b:Lcot;

    goto :goto_0

    :cond_1
    sget-object v0, Lcot;->c:Lcot;

    goto :goto_0
.end method
