.class public final Lgvg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsk;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object p2, p0, Lgvg;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lorg/json/JSONObject;

    iget-object v0, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    invoke-static {p1}, Lgty;->b(Lorg/json/JSONObject;)I

    move-result v0

    invoke-static {v0}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgvg;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixo;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lixo;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lixo;->k:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lixo;->k:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p1, v3}, Lgty;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iput-object v3, v0, Lixo;->d:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lgty;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v2

    iget-object v2, v2, Lixo;->k:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v2

    iget-object v2, v2, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v1

    iget-object v1, v1, Lixo;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v1

    iput-object v0, v1, Lixo;->k:Ljava/lang/String;

    :cond_3
    :goto_1
    iget-object v0, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lorg/json/JSONObject;)V

    return-void

    :cond_4
    iget-object v2, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    new-instance v3, Lixo;

    invoke-direct {v3}, Lixo;-><init>()V

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lixo;)Lixo;

    iget-object v2, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v2

    iput-object v0, v2, Lixo;->k:Ljava/lang/String;

    iget-object v0, p0, Lgvg;->b:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;

    move-result-object v0

    iput-object v1, v0, Lixo;->a:Ljava/lang/String;

    goto :goto_1
.end method
