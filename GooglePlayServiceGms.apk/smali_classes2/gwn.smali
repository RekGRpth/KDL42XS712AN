.class public final Lgwn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lgwi;
.implements Lgyo;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgwn;->a:Landroid/content/Context;

    iput-object p2, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput p3, p0, Lgwn;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lgwn;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgwn;->a:Landroid/content/Context;

    iput-object p2, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object p3, p0, Lgwn;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    const/4 v0, -0x1

    iput v0, p0, Lgwn;->d:I

    return-void
.end method

.method private a(Z)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v0

    :goto_0
    :pswitch_0
    return p1

    :cond_0
    iget v1, p0, Lgwn;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lgwn;->d:I

    :goto_1
    packed-switch v1, :pswitch_data_0

    move p1, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lgwn;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgwn;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgth;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-static {v1}, Lgth;->d(I)I

    move-result v1

    if-ne v2, v1, :cond_3

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final R_()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v1}, Lgwn;->a(Z)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgwn;->a:Landroid/content/Context;

    const v3, 0x7f0b015d    # com.google.android.gms.R.string.wallet_error_cvc_invalid

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final S_()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lgwn;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgwn;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lgwn;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->selectAll()V

    :cond_0
    return-void
.end method
