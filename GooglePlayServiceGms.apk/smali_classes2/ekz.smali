.class public final Lekz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public c:[I

.field public d:[I

.field public e:[B

.field public f:[Lela;

.field public g:[B


# direct methods
.method public constructor <init>(Leid;J)V
    .locals 9

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(J)I

    move-result v0

    iput v0, p0, Lekz;->a:I

    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(J)I

    move-result v0

    iput v0, p0, Lekz;->b:I

    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lekz;->b(Ljava/nio/ByteBuffer;)[I

    move-result-object v0

    iput-object v0, p0, Lekz;->c:[I

    iget-object v0, p0, Lekz;->c:[I

    array-length v0, v0

    const-string v2, "mCorpusIds"

    invoke-direct {p0, v0, v2}, Lekz;->a(ILjava/lang/String;)V

    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->d(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lekz;->b(Ljava/nio/ByteBuffer;)[I

    move-result-object v0

    iput-object v0, p0, Lekz;->d:[I

    iget-boolean v0, p1, Leid;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lekz;->d:[I

    array-length v0, v0

    const-string v2, "mUriLengths"

    invoke-direct {p0, v0, v2}, Lekz;->a(ILjava/lang/String;)V

    :cond_0
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lekz;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    iput-object v0, p0, Lekz;->e:[B

    iget-object v0, p1, Leid;->a:[Leie;

    array-length v0, v0

    new-array v0, v0, [Lela;

    iput-object v0, p0, Lekz;->f:[Lela;

    move v3, v1

    :goto_0
    iget-object v0, p0, Lekz;->f:[Lela;

    array-length v0, v0

    if-ge v3, v0, :cond_4

    new-instance v4, Lela;

    invoke-direct {v4}, Lela;-><init>()V

    iget-object v0, p1, Leid;->a:[Leie;

    aget-object v5, v0, v3

    iget-boolean v0, p1, Leid;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, v5, Leie;->d:[Leim;

    array-length v0, v0

    move v2, v0

    :goto_1
    new-array v0, v2, [Lelb;

    iput-object v0, v4, Lela;->a:[Lelb;

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_2

    new-instance v6, Lelb;

    invoke-direct {v6}, Lelb;-><init>()V

    invoke-static {p2, p3, v3, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JII)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lekz;->b(Ljava/nio/ByteBuffer;)[I

    move-result-object v7

    iput-object v7, v6, Lelb;->a:[I

    iget-object v7, v6, Lelb;->a:[I

    array-length v7, v7

    const-string v8, "mContentLengths"

    invoke-direct {p0, v7, v8}, Lekz;->a(ILjava/lang/String;)V

    invoke-static {p2, p3, v3, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(JII)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v7}, Lekz;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v7

    iput-object v7, v6, Lelb;->b:[B

    iget-object v7, v4, Lela;->a:[Lelb;

    aput-object v6, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    iget-object v0, v5, Leie;->b:[Leig;

    array-length v0, v0

    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v0, v5, Leie;->c:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Lelc;

    iput-object v0, v4, Lela;->b:[Lelc;

    move v0, v1

    :goto_3
    iget-object v2, v4, Lela;->b:[Lelc;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    new-instance v2, Lelc;

    invoke-direct {v2}, Lelc;-><init>()V

    invoke-static {p2, p3, v3, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(JII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-static {v5}, Lekz;->c(Ljava/nio/ByteBuffer;)[Z

    move-result-object v5

    iput-object v5, v2, Lelc;->a:[Z

    iget-object v5, v2, Lelc;->a:[Z

    array-length v5, v5

    const-string v6, "mDocHasTag"

    invoke-direct {p0, v5, v6}, Lekz;->a(ILjava/lang/String;)V

    iget-object v5, v4, Lela;->b:[Lelc;

    aput-object v2, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lekz;->f:[Lela;

    aput-object v4, v0, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_4
    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->f(J)[B

    move-result-object v0

    iput-object v0, p0, Lekz;->g:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->g(J)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->g(J)V

    throw v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    iget v0, p0, Lekz;->a:I

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " len "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lekz;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;)[B
    .locals 1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method private static b(Ljava/nio/ByteBuffer;)[I
    .locals 2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    return-object v1
.end method

.method private static c(Ljava/nio/ByteBuffer;)[Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v3, v0, [Z

    move v0, v1

    :goto_0
    array-length v2, v3

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    aput-boolean v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    return-object v3
.end method
