.class public final Licw;
.super Lido;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhud;

.field final synthetic b:Licp;


# direct methods
.method public constructor <init>(Licp;Licn;JLhud;)V
    .locals 0

    iput-object p1, p0, Licw;->b:Licp;

    iput-object p5, p0, Licw;->a:Lhud;

    invoke-direct {p0, p2, p3, p4}, Lido;-><init>(Licn;J)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v0, p0, Licw;->a:Lhud;

    const-string v1, "NetworkLocation [\n bestResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lhud;->a:Lhtw;

    if-nez v1, :cond_1

    const-string v1, "null"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v1, "\n wifiResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lhud;->b:Lhuu;

    invoke-static {p1, v1}, Lhuu;->a(Ljava/io/PrintWriter;Lhuu;)V

    const-string v1, "\n cellResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lhud;->c:Lhtd;

    invoke-static {p1, v1}, Lhtd;->a(Ljava/io/PrintWriter;Lhtd;)V

    const-string v1, "\n glsResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lhud;->d:Lhtj;

    invoke-static {p1, v1}, Lhtj;->a(Ljava/io/PrintWriter;Lhtj;)V

    const-string v1, "\n isLowPower="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, v0, Lhud;->e:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "\n]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v1, v0, Lhud;->a:Lhtw;

    iget-object v2, v0, Lhud;->b:Lhuu;

    if-ne v1, v2, :cond_2

    const-string v1, "WIFI"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lhud;->a:Lhtw;

    iget-object v2, v0, Lhud;->c:Lhtd;

    if-ne v1, v2, :cond_3

    const-string v1, "CELL"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lhud;->a:Lhtw;

    iget-object v2, v0, Lhud;->d:Lhtj;

    if-ne v1, v2, :cond_0

    const-string v1, "GLS"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method
