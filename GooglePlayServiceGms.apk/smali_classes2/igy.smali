.class final Ligy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhuf;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 12

    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v3

    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v5

    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    invoke-static {}, Ligx;->a()Lhuf;

    move-result-object v0

    invoke-interface {v0, p1}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->a()Lerj;

    move-result-object v8

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v9

    if-eqz v9, :cond_3

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v9, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Livi;->d(II)Ljava/lang/String;

    move-result-object v11

    const-string v1, "coarse"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lerp;->b:Ljava/util/Set;

    invoke-interface {v10, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    invoke-static {v11}, Lerp;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {v11}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v1

    :cond_1
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iput-object v10, v8, Lerj;->a:Ljava/util/Collection;

    :cond_3
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->g(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lerj;->b:Ljava/lang/String;

    :cond_4
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Livi;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, v8, Lerj;->c:Z

    :cond_5
    iget-object v0, v8, Lerj;->a:Ljava/util/Collection;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, v8, Lerj;->a:Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_2
    new-instance v1, Lcom/google/android/gms/location/places/PlaceFilter;

    iget-object v2, v8, Lerj;->b:Ljava/lang/String;

    iget-boolean v8, v8, Lerj;->c:Z

    const/4 v9, 0x0

    invoke-direct {v1, v0, v2, v8, v9}, Lcom/google/android/gms/location/places/PlaceFilter;-><init>(Ljava/util/List;Ljava/lang/String;ZB)V

    move-object v0, v1

    :goto_3
    new-instance v1, Ligx;

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v1, v2, v7, v0}, Ligx;-><init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)V

    return-object v1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2

    check-cast p1, Ligx;

    iget-object v0, p1, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Ligx;->b:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p1, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    invoke-static {v0}, Ligh;->a(Lcom/google/android/gms/location/places/PlaceFilter;)Livi;

    move-result-object v0

    invoke-static {}, Ligx;->a()Lhuf;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    return-void
.end method
