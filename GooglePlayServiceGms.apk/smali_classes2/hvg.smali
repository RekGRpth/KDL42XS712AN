.class final Lhvg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Ljava/io/PrintWriter;

.field final synthetic c:Lhvb;


# direct methods
.method constructor <init>(Lhvb;Ljava/lang/Object;Ljava/io/PrintWriter;)V
    .locals 0

    iput-object p1, p0, Lhvg;->c:Lhvb;

    iput-object p2, p0, Lhvg;->a:Ljava/lang/Object;

    iput-object p3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v2, p0, Lhvg;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v1, "\nActive Location Requests (GCore Only):"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhvg;->c:Lhvb;

    invoke-static {v0}, Lhvb;->c(Lhvb;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v4, "    Receiver: "

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    iget-object v4, v0, Lhvh;->b:Lhwl;

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v4, " numUpdatesDelivered="

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    invoke-static {v0}, Lhvh;->b(Lhvh;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->print(I)V

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v4, " lastDeliveryElapsedRealtime="

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lhvh;->c(Lhvh;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "[ms]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v1, "\nLocation Request History By Package (GCore Only):"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhvg;->c:Lhvb;

    invoke-static {v0}, Lhvb;->f(Lhvb;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvm;

    iget-object v4, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v5, "    Receiver: "

    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Lhvg;->b:Ljava/io/PrintWriter;

    invoke-virtual {v4, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    iget-object v1, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lhvg;->b:Ljava/io/PrintWriter;

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lhvg;->b:Ljava/io/PrintWriter;

    const-string v1, "\nEvent Log:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lilf;

    const/4 v1, 0x0

    iget-object v3, p0, Lhvg;->c:Lhvb;

    invoke-static {v3}, Lhvb;->g(Lhvb;)Lhvo;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lhvg;->c:Lhvb;

    iget-object v3, v3, Lhvb;->a:Lhvx;

    invoke-virtual {v3}, Lhvx;->e()Lilf;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0}, Lilf;->a([Lilf;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lhvg;->b:Ljava/io/PrintWriter;

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lhvg;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
