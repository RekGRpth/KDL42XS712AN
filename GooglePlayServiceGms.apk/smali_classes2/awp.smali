.class final Lawp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Laye;

.field private static b:Ljavax/net/ssl/SSLContext;


# instance fields
.field private final c:Ljava/nio/channels/SocketChannel;

.field private final d:Lawg;

.field private final e:Lawg;

.field private final f:Ljava/nio/ByteBuffer;

.field private final g:Ljava/nio/ByteBuffer;

.field private final h:Ljavax/net/ssl/SSLEngine;

.field private i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lbbj;

    const-string v1, "SSLSocketEngine"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lawp;->a:Laye;

    :try_start_0
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    sput-object v0, Lawp;->b:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    sget-object v0, Lawp;->b:Ljavax/net/ssl/SSLContext;

    const/4 v1, 0x0

    sget-object v2, Lavv;->a:[Lavv;

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lawp;->a:Laye;

    const-string v2, "Failed SSLContext.init."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/nio/channels/SocketChannel;Lawg;Lawg;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lawp;->c:Ljava/nio/channels/SocketChannel;

    iput-object p2, p0, Lawp;->d:Lawg;

    iput-object p3, p0, Lawp;->e:Lawg;

    sget-object v0, Lawp;->b:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->createSSLEngine()Ljavax/net/ssl/SSLEngine;

    move-result-object v0

    iput-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPacketBufferSize()I

    move-result v0

    sget-object v1, Lawp;->a:Laye;

    const-string v2, "channelBufferSize = %d"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    iput-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    iput-boolean v5, p0, Lawp;->j:Z

    iput-boolean v5, p0, Lawp;->k:Z

    return-void
.end method

.method private a(Ljavax/net/ssl/SSLEngineResult;)V
    .locals 2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v0, Lawq;->b:[I

    iget-object v1, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lawp;->j:Z

    goto :goto_0

    :goto_1
    :pswitch_1
    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getDelegatedTask()Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private h()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lawp;->c:Ljava/nio/channels/SocketChannel;

    iget-object v1, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v0

    if-gez v0, :cond_0

    sget-object v0, Lawp;->a:Laye;

    const-string v1, "writeToChannel: throwing ClosedChannelException"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    :cond_0
    sget-object v1, Lawp;->a:Laye;

    const-string v2, "writeToChannel: count %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lawp;->j:Z

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    return-void
.end method

.method public final c()V
    .locals 3

    sget-object v0, Lawp;->a:Laye;

    const-string v1, "disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lawp;->k:Z

    return-void
.end method

.method public final d()I
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lawp;->k:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iget-object v3, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v3

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lawp;->k:Z

    if-eqz v3, :cond_4

    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    return v0

    :cond_2
    iget-object v0, p0, Lawp;->d:Lawg;

    invoke-virtual {v0}, Lawg;->e()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v4, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    if-gtz v3, :cond_0

    iget-object v3, p0, Lawp;->e:Lawg;

    iget-boolean v3, v3, Lawg;->e:Z

    if-eqz v3, :cond_0

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public final e()V
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lawp;->c:Ljava/nio/channels/SocketChannel;

    iget-object v3, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    if-gez v0, :cond_3

    sget-object v0, Lawp;->a:Laye;

    const-string v3, "readFromChannel: throwing ClosedChannelException"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    move v0, v2

    :goto_0
    iget-object v3, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    :goto_1
    if-nez v0, :cond_7

    iget-object v3, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-lez v3, :cond_7

    iget-object v3, p0, Lawp;->d:Lawg;

    invoke-virtual {v3}, Lawg;->b()[Ljava/nio/ByteBuffer;

    move-result-object v3

    iget-object v4, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    iget-object v5, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v5, v3}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v4

    sget-object v5, Lawp;->a:Laye;

    const-string v6, "handleRead called SSLEngine.unwrap: status=%s handshakeStatus=%s bytesConsumed=%s bytesProduced=%s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x2

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lawp;->a(Ljavax/net/ssl/SSLEngineResult;)V

    iget-object v5, p0, Lawp;->d:Lawg;

    iget-object v6, v5, Lawg;->f:[Ljava/nio/ByteBuffer;

    if-ne v3, v6, :cond_5

    iget-object v3, v5, Lawg;->f:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    iget v6, v5, Lawg;->c:I

    sub-int/2addr v3, v6

    :goto_2
    if-lez v3, :cond_1

    invoke-virtual {v5, v3}, Lawg;->b(I)V

    :cond_1
    sget-object v3, Lawq;->a:[I

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v4

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    :cond_2
    :goto_3
    return-void

    :cond_3
    :try_start_1
    sget-object v3, Lawp;->a:Laye;

    const-string v4, "readFromChannel: count %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v0, :cond_4

    move v0, v2

    :goto_4
    if-eqz v0, :cond_2

    move v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_4

    :cond_5
    iget-object v6, v5, Lawg;->g:[Ljava/nio/ByteBuffer;

    if-ne v3, v6, :cond_6

    iget-object v3, v5, Lawg;->g:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    iget v6, v5, Lawg;->c:I

    sub-int/2addr v3, v6

    iget-object v6, v5, Lawg;->g:[Ljava/nio/ByteBuffer;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/2addr v3, v6

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected buffer overflow condition."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    :cond_7
    iget-object v1, p0, Lawp;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeInbound()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final f()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_1

    invoke-direct {p0}, Lawp;->h()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lawp;->e:Lawg;

    invoke-virtual {v0}, Lawg;->a()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iget-object v1, p0, Lawp;->e:Lawg;

    iget-boolean v1, v1, Lawg;->e:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lawp;->k:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V

    :cond_2
    iget-object v1, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    iget-object v2, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0, v2}, Ljavax/net/ssl/SSLEngine;->wrap([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v1

    sget-object v2, Lawp;->a:Laye;

    const-string v3, "handleWrite called SSLEngine.wrap: status=%s handshakeStatus=%s bytesConsumed=%s bytesProduced=%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x2

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lawp;->a(Ljavax/net/ssl/SSLEngineResult;)V

    iget-object v2, p0, Lawp;->e:Lawg;

    iget-object v3, v2, Lawg;->f:[Ljava/nio/ByteBuffer;

    if-ne v0, v3, :cond_5

    iget-object v0, v2, Lawg;->f:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v3, v2, Lawg;->b:I

    sub-int/2addr v0, v3

    :goto_1
    if-lez v0, :cond_3

    invoke-virtual {v2, v0}, Lawg;->a(I)V

    :cond_3
    sget-object v0, Lawq;->a:[I

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lawp;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_4

    invoke-direct {p0}, Lawp;->h()V

    :cond_4
    iget-object v0, p0, Lawp;->e:Lawg;

    iget-boolean v0, v0, Lawg;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lawp;->i:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v0, v1, :cond_1

    goto/16 :goto_0

    :cond_5
    iget-object v3, v2, Lawg;->g:[Ljava/nio/ByteBuffer;

    if-ne v0, v3, :cond_6

    iget-object v0, v2, Lawg;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v3, v2, Lawg;->b:I

    sub-int/2addr v0, v3

    iget-object v3, v2, Lawg;->g:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v8

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected buffer underflow condition."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected buffer overflow condition."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final g()[B
    .locals 2

    iget-boolean v0, p0, Lawp;->j:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lawp;->h:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getEncoded()[B
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
