.class final Lhvf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lhvb;


# direct methods
.method constructor <init>(Lhvb;)V
    .locals 0

    iput-object p1, p0, Lhvf;->a:Lhvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lhvf;->a:Lhvb;

    invoke-static {v0}, Lhvb;->c(Lhvb;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    iget-object v2, v0, Lhvh;->c:Landroid/app/PendingIntent;

    if-nez v2, :cond_0

    iget-object v2, v0, Lhvh;->b:Lhwl;

    iget-boolean v2, v2, Lhwl;->d:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lhvf;->a:Lhvb;

    iget-object v3, v0, Lhvh;->a:Leqc;

    invoke-virtual {v2, v3}, Lhvb;->a(Leqc;)V

    const-string v2, "GCoreFlp"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Removed a listener stuck after disconnect in package %s, %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Lhvh;->b:Lhwl;

    iget-object v0, v0, Lhwl;->e:Ljava/util/Collection;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method
