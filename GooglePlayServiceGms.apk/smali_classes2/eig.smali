.class public final Leig;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile e:[Leig;


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v1, p0, Leig;->a:I

    iput-boolean v1, p0, Leig;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Leig;->c:Z

    iput v1, p0, Leig;->d:I

    const/4 v0, -0x1

    iput v0, p0, Leig;->C:I

    return-void
.end method

.method public static c()[Leig;
    .locals 2

    sget-object v0, Leig;->e:[Leig;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Leig;->e:[Leig;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Leig;

    sput-object v0, Leig;->e:[Leig;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Leig;->e:[Leig;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Leig;->a:I

    if-eqz v1, :cond_0

    iget v1, p0, Leig;->a:I

    invoke-static {v3, v1}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-boolean v1, p0, Leig;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Leig;->b:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Leig;->c:Z

    if-eq v1, v3, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Leig;->c:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Leig;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Leig;->d:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Leig;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leig;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leig;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leig;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leig;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Leig;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Leig;->a:I

    invoke-virtual {p1, v2, v0}, Lizn;->c(II)V

    :cond_0
    iget-boolean v0, p0, Leig;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Leig;->b:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Leig;->c:Z

    if-eq v0, v2, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Leig;->c:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_2
    iget v0, p0, Leig;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Leig;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leig;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leig;

    iget v2, p0, Leig;->a:I

    iget v3, p1, Leig;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Leig;->b:Z

    iget-boolean v3, p1, Leig;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Leig;->c:Z

    iget-boolean v3, p1, Leig;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Leig;->d:I

    iget v3, p1, Leig;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    iget v0, p0, Leig;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Leig;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Leig;->c:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leig;->d:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
