.class final Lhiq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lidr;

.field b:J

.field c:J

.field final d:Lhoj;

.field private e:Lhip;


# direct methods
.method public constructor <init>(Lhoj;)V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhiq;->a:Lidr;

    iput-wide v1, p0, Lhiq;->b:J

    iput-wide v1, p0, Lhiq;->c:J

    new-instance v0, Lhip;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lhip;-><init>(B)V

    iput-object v0, p0, Lhiq;->e:Lhip;

    iput-object p1, p0, Lhiq;->d:Lhoj;

    return-void
.end method


# virtual methods
.method final a(Lhuv;JJ)I
    .locals 9

    const-wide/16 v5, -0x1

    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p1, v1}, Lhuv;->a(I)Lhut;

    move-result-object v0

    iget-wide v3, v0, Lhut;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, p0, Lhiq;->d:Lhoj;

    iget-object v3, v3, Lhoj;->b:Lhok;

    invoke-virtual {v3, v0}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-nez v0, :cond_2

    move-wide v3, v5

    :goto_1
    cmp-long v0, v3, v5

    if-eqz v0, :cond_0

    sub-long v3, p2, v3

    const-wide/32 v7, 0x927c0

    cmp-long v0, v3, v7

    if-lez v0, :cond_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-wide v3, v0, Lhol;->b:J

    goto :goto_1

    :cond_3
    return v2
.end method

.method final a(Lidr;Lhtf;Lhuv;)V
    .locals 9

    const/4 v8, 0x6

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lhiq;->a:Lidr;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lhtf;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lhiq;->c:J

    if-eqz p1, :cond_1

    iget-object v0, p0, Lhiq;->e:Lhip;

    invoke-virtual {v0, p2, p1}, Lhip;->a(Lhtf;Lidr;)V

    :cond_1
    if-eqz p3, :cond_3

    iget-wide v0, p3, Lhuv;->a:J

    iput-wide v0, p0, Lhiq;->b:J

    iget-object v0, p0, Lhiq;->d:Lhoj;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lhiq;->d:Lhoj;

    if-eqz p3, :cond_3

    iget-object v0, v1, Lhoj;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v2

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p3, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {p3, v0}, Lhuv;->a(I)Lhut;

    move-result-object v4

    iget-wide v4, v4, Lhut;->b:J

    invoke-virtual {v1, v4, v5, v2, v3}, Lhoj;->a(JJ)Lhol;

    move-result-object v4

    iget-object v5, v4, Lhol;->a:Livi;

    invoke-virtual {v5, v8}, Livi;->c(I)I

    move-result v5

    iget-object v6, v4, Lhol;->a:Livi;

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v6, v8, v5}, Livi;->e(II)Livi;

    invoke-virtual {v4}, Lhol;->a()F

    move-result v5

    cmpl-float v6, v5, v7

    if-lez v6, :cond_2

    const v6, 0x3dcccccd    # 0.1f

    sub-float/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-virtual {v4, v5}, Lhol;->a(F)V

    :cond_2
    iput-wide v2, v4, Lhol;->b:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method final a(Lhtf;Lidr;)Z
    .locals 12

    const/4 v8, 0x0

    iget-object v10, p0, Lhiq;->e:Lhip;

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v8

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lhtf;->a()Ljava/lang/String;

    move-result-object v11

    invoke-interface {p2}, Lidr;->b()D

    move-result-wide v4

    invoke-interface {p2}, Lidr;->c()D

    move-result-wide v6

    move v9, v8

    :goto_1
    const/4 v0, 0x5

    if-ge v9, v0, :cond_3

    iget-object v0, v10, Lhip;->b:[Z

    aget-boolean v0, v0, v9

    if-eqz v0, :cond_2

    iget-object v0, v10, Lhip;->a:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v10, Lhip;->c:[D

    aget-wide v0, v0, v9

    iget-object v2, v10, Lhip;->d:[D

    aget-wide v2, v2, v9

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v10, p1, p2}, Lhip;->a(Lhtf;Lidr;)V

    move v0, v8

    goto :goto_0
.end method

.method final a(Lhuv;J)Z
    .locals 6

    const-wide/32 v4, 0x927c0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lhiq;->a(Lhuv;JJ)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
