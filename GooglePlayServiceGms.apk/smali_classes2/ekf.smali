.class final Lekf;
.super Leku;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Z

.field final synthetic d:Leix;

.field final synthetic e:J

.field final synthetic f:Lejm;


# direct methods
.method constructor <init>(Lejm;Ljava/lang/String;IZLeix;J)V
    .locals 0

    iput-object p1, p0, Lekf;->f:Lejm;

    iput-object p2, p0, Lekf;->a:Ljava/lang/String;

    iput p3, p0, Lekf;->b:I

    iput-boolean p4, p0, Lekf;->c:Z

    iput-object p5, p0, Lekf;->d:Leix;

    iput-wide p6, p0, Lekf;->e:J

    invoke-direct {p0, p1}, Leku;-><init>(Lejm;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 8

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lekf;->f:Lejm;

    iget-object v1, p0, Lekf;->a:Ljava/lang/String;

    iget v2, p0, Lekf;->b:I

    iget-boolean v3, p0, Lekf;->c:Z

    iget-object v4, p0, Lekf;->d:Leix;

    iget-wide v5, p0, Lekf;->e:J

    invoke-virtual/range {v0 .. v6}, Lejm;->b(Ljava/lang/String;IZLeix;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    move v0, v7

    :goto_1
    if-nez v0, :cond_0

    const-string v0, "Indexing done %s"

    iget-object v1, p0, Lekf;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lekf;->f:Lejm;

    iget-object v0, v0, Lejm;->k:Ljava/util/Set;

    iget v1, p0, Lekf;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :try_start_1
    iget-object v0, p0, Lekf;->d:Leix;

    invoke-virtual {v0}, Leix;->d()V
    :try_end_1
    .catch Lelf; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_2
    return-void

    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lekf;->f:Lejm;

    iget-object v1, p0, Lekf;->a:Ljava/lang/String;

    iget v2, p0, Lekf;->b:I

    iget-boolean v3, p0, Lekf;->c:Z

    iget-object v4, p0, Lekf;->d:Leix;

    iget-wide v5, p0, Lekf;->e:J

    invoke-virtual/range {v0 .. v6}, Lejm;->a(Ljava/lang/String;IZLeix;J)V

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_1
    const-string v0, "Aborting indexing of corpus %s"

    iget-object v1, p0, Lekf;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "Indexing done %s"

    iget-object v2, p0, Lekf;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v1, p0, Lekf;->f:Lejm;

    iget-object v1, v1, Lejm;->k:Ljava/util/Set;

    iget v2, p0, Lekf;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :try_start_3
    iget-object v1, p0, Lekf;->d:Leix;

    invoke-virtual {v1}, Leix;->d()V
    :try_end_3
    .catch Lelf; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    throw v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lelf;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    const-string v1, "Cursor close call threw an exception"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lekf;->f:Lejm;

    iget-object v0, v0, Lejm;->m:Leji;

    const-string v1, "cursor_close_exception"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lelf;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "Cursor close call threw an exception"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lekf;->f:Lejm;

    iget-object v1, v1, Lejm;->m:Leji;

    const-string v2, "cursor_close_exception"

    invoke-interface {v1, v2}, Leji;->a(Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
