.class public final Lehu;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile g:[Lehu;


# instance fields
.field public a:I

.field public b:[B

.field public c:[B

.field public d:Z

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v1, p0, Lehu;->a:I

    sget-object v0, Lizv;->h:[B

    iput-object v0, p0, Lehu;->b:[B

    sget-object v0, Lizv;->h:[B

    iput-object v0, p0, Lehu;->c:[B

    iput-boolean v1, p0, Lehu;->d:Z

    iput-wide v2, p0, Lehu;->e:J

    iput-wide v2, p0, Lehu;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lehu;->C:I

    return-void
.end method

.method public static c()[Lehu;
    .locals 2

    sget-object v0, Lehu;->g:[Lehu;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lehu;->g:[Lehu;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lehu;

    sput-object v0, Lehu;->g:[Lehu;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lehu;->g:[Lehu;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 6

    const-wide/16 v4, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Lehu;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lehu;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lehu;->b:[B

    sget-object v2, Lizv;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lehu;->b:[B

    invoke-static {v1, v2}, Lizn;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lehu;->c:[B

    sget-object v2, Lizv;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lehu;->c:[B

    invoke-static {v1, v2}, Lizn;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lehu;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lehu;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-wide v1, p0, Lehu;->e:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lehu;->e:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-wide v1, p0, Lehu;->f:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-wide v2, p0, Lehu;->f:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lehu;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehu;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v0

    iput-object v0, p0, Lehu;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v0

    iput-object v0, p0, Lehu;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lehu;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehu;->e:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehu;->f:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const-wide/16 v3, 0x0

    iget v0, p0, Lehu;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lehu;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Lehu;->b:[B

    sget-object v1, Lizv;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lehu;->b:[B

    invoke-virtual {p1, v0, v1}, Lizn;->a(I[B)V

    :cond_1
    iget-object v0, p0, Lehu;->c:[B

    sget-object v1, Lizv;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lehu;->c:[B

    invoke-virtual {p1, v0, v1}, Lizn;->a(I[B)V

    :cond_2
    iget-boolean v0, p0, Lehu;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lehu;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_3
    iget-wide v0, p0, Lehu;->e:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lehu;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_4
    iget-wide v0, p0, Lehu;->f:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lehu;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_5
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehu;

    iget v2, p0, Lehu;->a:I

    iget v3, p1, Lehu;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lehu;->b:[B

    iget-object v3, p1, Lehu;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lehu;->c:[B

    iget-object v3, p1, Lehu;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lehu;->d:Z

    iget-boolean v3, p1, Lehu;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lehu;->e:J

    iget-wide v4, p1, Lehu;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-wide v2, p0, Lehu;->f:J

    iget-wide v4, p1, Lehu;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget v0, p0, Lehu;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lehu;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lehu;->c:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lehu;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehu;->e:J

    iget-wide v3, p0, Lehu;->e:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehu;->f:J

    iget-wide v3, p0, Lehu;->f:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method
