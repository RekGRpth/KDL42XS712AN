.class public final Laun;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Laun;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laun;->b:Ljava/lang/String;

    iput-boolean v1, p0, Laun;->c:Z

    const-string v0, ""

    iput-object v0, p0, Laun;->d:Ljava/lang/String;

    iput-boolean v1, p0, Laun;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Laun;->k:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Laun;->k:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Laun;->b()I

    :cond_0
    iget v0, p0, Laun;->k:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Laun;->f:Z

    iput-object v0, p0, Laun;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Laun;->g:Z

    iput-object v0, p0, Laun;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v1, p0, Laun;->h:Z

    iput-boolean v0, p0, Laun;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Laun;->i:Z

    iput-object v0, p0, Laun;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v1, p0, Laun;->j:Z

    iput-boolean v0, p0, Laun;->e:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Laun;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Laun;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Laun;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Laun;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Laun;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Laun;->c:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Laun;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Laun;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Laun;->j:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Laun;->e:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_4
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Laun;->f:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Laun;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Laun;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Laun;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Laun;->h:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Laun;->c:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Laun;->i:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Laun;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Laun;->j:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Laun;->e:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Laun;->k:I

    return v0
.end method
