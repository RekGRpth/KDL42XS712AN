.class public final Ldxi;
.super Ldxh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 10

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v9}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;IB)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;IB)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;IB)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ldxh;-><init>()V

    iput-object p1, p0, Ldxi;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldxi;->b:Landroid/view/LayoutInflater;

    iput p2, p0, Ldxi;->c:I

    iput p3, p0, Ldxi;->f:I

    iput p4, p0, Ldxi;->g:I

    iput-object p5, p0, Ldxi;->i:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Ldxi;->j:Ljava/lang/String;

    iput-object p7, p0, Ldxi;->k:Ljava/lang/String;

    iput p8, p0, Ldxi;->l:I

    iput v1, p0, Ldxi;->h:I

    iput v1, p0, Ldxi;->e:I

    return-void
.end method

.method static synthetic a(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->h:I

    return v0
.end method

.method static synthetic b(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->l:I

    return v0
.end method

.method static synthetic c(Ldxi;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Ldxi;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Ldxi;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldxi;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Ldxi;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Ldxi;->i:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic f(Ldxi;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldxi;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->c:I

    return v0
.end method

.method static synthetic h(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->f:I

    return v0
.end method

.method static synthetic i(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->e:I

    return v0
.end method

.method static synthetic j(Ldxi;)I
    .locals 1

    iget v0, p0, Ldxi;->g:I

    return v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Ldxi;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->q:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Ldxj;

    invoke-direct {v0, p0, p2}, Ldxj;-><init>(Ldxi;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxj;

    invoke-virtual {v0}, Ldxj;->a()V

    return-object p2
.end method
