.class public final Lhix;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lidu;

.field b:Lhiz;

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:I

.field j:J

.field k:J

.field l:Z

.field m:I

.field n:Z

.field o:J

.field p:Lihe;

.field private final q:Ljava/util/List;

.field private final r:Lhkr;

.field private final s:Liha;

.field private final t:Lids;

.field private final u:Ljava/util/Random;

.field private final v:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Lidu;Liha;Lids;Lhkr;)V
    .locals 6

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lhix;-><init>(Lidu;Liha;Lids;Lhkr;Ljava/util/Random;)V

    return-void
.end method

.method private constructor <init>(Lidu;Liha;Lids;Lhkr;Ljava/util/Random;)V
    .locals 5

    const/4 v4, -0x1

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    sget-object v0, Lhiz;->a:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    iput-boolean v1, p0, Lhix;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhix;->d:Z

    iput-boolean v1, p0, Lhix;->e:Z

    iput-boolean v1, p0, Lhix;->f:Z

    iput-boolean v1, p0, Lhix;->g:Z

    iput-boolean v1, p0, Lhix;->h:Z

    iput v4, p0, Lhix;->i:I

    iput-wide v2, p0, Lhix;->j:J

    iput-wide v2, p0, Lhix;->k:J

    iput-boolean v1, p0, Lhix;->l:Z

    iput v4, p0, Lhix;->m:I

    iput-boolean v1, p0, Lhix;->n:Z

    iput-wide v2, p0, Lhix;->o:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhix;->p:Lihe;

    iput-object p4, p0, Lhix;->r:Lhkr;

    iput-object p1, p0, Lhix;->a:Lidu;

    iput-object p3, p0, Lhix;->t:Lids;

    iput-object p2, p0, Lhix;->s:Liha;

    iput-object p5, p0, Lhix;->u:Ljava/util/Random;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lilt;

    const/4 v2, 0x6

    const/16 v3, 0x2d

    const/16 v4, 0xa

    invoke-direct {v1, v2, v3, v4}, Lilt;-><init>(III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lilt;

    const/16 v2, 0xf

    const/16 v3, 0x1e

    const/16 v4, 0x13

    invoke-direct {v1, v2, v3, v4}, Lilt;-><init>(III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhix;->q:Ljava/util/List;

    return-void
.end method

.method private a(JJ)V
    .locals 5

    iput-wide p1, p0, Lhix;->j:J

    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    iget-wide v2, p0, Lhix;->j:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "BurstCollectionTrigger"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm scheduled at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lhix;->j:J

    add-long/2addr v3, p3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/util/Calendar;J)V
    .locals 3

    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    iget-wide v0, v0, Lilt;->a:J

    invoke-static {p1, v0, v1}, Lilv;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    invoke-direct {p0, v0, p2, p3}, Lhix;->b(Ljava/util/Calendar;J)V

    return-void
.end method

.method private a(Ljava/util/Calendar;Lilt;J)V
    .locals 7

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {p2, p1}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p2, Lilt;->a:J

    invoke-static {p1, v0, v1}, Lilv;->a(Ljava/util/Calendar;J)V

    :cond_0
    iget-wide v0, p0, Lhix;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p3, p4}, Lhix;->b(Ljava/util/Calendar;J)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    iget-wide v2, p0, Lhix;->k:J

    const-wide/32 v4, 0x927c0

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    add-long v0, p3, v2

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_2
    invoke-virtual {p2, p1}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p3, p4}, Lhix;->b(Ljava/util/Calendar;J)V

    goto :goto_1

    :cond_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "Skipping to next rush hours."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_5

    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    iget-wide v0, v0, Lilt;->a:J

    invoke-static {p1, v0, v1}, Lilv;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    move-object p2, v0

    goto :goto_0

    :cond_5
    invoke-direct {p0, p1, p3, p4}, Lhix;->a(Ljava/util/Calendar;J)V

    goto :goto_1
.end method

.method private a(J)Z
    .locals 4

    iget-wide v0, p0, Lhix;->j:J

    const-wide/16 v2, 0x7d0

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JZ)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhix;->a:Lidu;

    invoke-interface {v0}, Lidu;->k()Z

    move-result v3

    iget-object v0, p0, Lhix;->t:Lids;

    invoke-virtual {v0}, Lids;->i()Z

    move-result v4

    iget-boolean v0, p0, Lhix;->c:Z

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    iget-boolean v0, p0, Lhix;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sget-object v5, Lhiy;->a:[I

    iget-object v6, p0, Lhix;->b:Lhiz;

    invoke-virtual {v6}, Lhiz;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_0

    const-string v5, "BurstCollectionTrigger"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[fullCollectionEnabled="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lhix;->c:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", screenOn="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lhix;->d:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isBatteryHealthy="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lhix;->e:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", collectionCanceled="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lhix;->f:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", clientEnabled=true, serverEnabled="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", gpsEnabled="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_a

    invoke-direct {p0, p1, p2, p3}, Lhix;->b(JZ)V

    :goto_2
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :pswitch_1
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lhix;->f:Z

    if-eqz v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :pswitch_2
    if-eqz v0, :cond_6

    iget v0, p0, Lhix;->i:I

    if-eqz v0, :cond_5

    iget v0, p0, Lhix;->i:I

    const/4 v5, 0x3

    if-ne v0, v5, :cond_6

    :cond_5
    iget-boolean v0, p0, Lhix;->f:Z

    if-eqz v0, :cond_7

    :cond_6
    move v0, v1

    goto/16 :goto_1

    :cond_7
    move v0, v2

    goto/16 :goto_1

    :pswitch_3
    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lhix;->f:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lhix;->d:Z

    if-nez v0, :cond_8

    invoke-direct {p0, p1, p2}, Lhix;->a(J)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_1

    :cond_a
    move v1, v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(JZ)V
    .locals 5

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lidu;->a(I)V

    iput-wide v3, p0, Lhix;->j:J

    invoke-direct {p0}, Lhix;->f()V

    iput-boolean v2, p0, Lhix;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lhix;->i:I

    iput-boolean v2, p0, Lhix;->f:Z

    iput-boolean v2, p0, Lhix;->g:Z

    if-eqz p3, :cond_0

    iput-wide v3, p0, Lhix;->o:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhix;->p:Lihe;

    :cond_0
    iget-object v0, p0, Lhix;->b:Lhiz;

    sget-object v1, Lhiz;->a:Lhiz;

    if-eq v0, v1, :cond_1

    sget-object v0, Lhiz;->a:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    iput-wide p1, p0, Lhix;->k:J

    :cond_1
    invoke-direct {p0}, Lhix;->e()V

    return-void
.end method

.method private b(Ljava/util/Calendar;J)V
    .locals 2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-direct {p0, v0, v1, p2, p3}, Lhix;->a(JJ)V

    return-void
.end method

.method private d()V
    .locals 3

    iget-boolean v0, p0, Lhix;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lidu;->a(ILilx;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhix;->n:Z

    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    iget-boolean v0, p0, Lhix;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lidu;->b(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhix;->n:Z

    :cond_0
    return-void
.end method

.method private f()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, -0x1

    iget-boolean v0, p0, Lhix;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhix;->m:I

    if-eq v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lilv;->b(Z)V

    iget-object v0, p0, Lhix;->r:Lhkr;

    iget v2, p0, Lhix;->m:I

    invoke-virtual {v0, v2}, Lhkr;->b(I)V

    iput-boolean v1, p0, Lhix;->l:Z

    iput v3, p0, Lhix;->m:I

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 10

    const/4 v0, 0x0

    :cond_0
    iget-object v7, p0, Lhix;->b:Lhiz;

    iget-object v1, p0, Lhix;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v2

    sget-object v1, Lhiy;->a:[I

    iget-object v4, p0, Lhix;->b:Lhiz;

    invoke-virtual {v4}, Lhiz;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lhix;->b:Lhiz;

    if-eq v7, v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "BurstCollectionTrigger"

    const-string v2, "collector state changed from %s to %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lhix;->b:Lhiz;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-nez v0, :cond_0

    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lhix;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v8

    iget-wide v0, p0, Lhix;->j:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    invoke-direct {p0, v2, v3}, Lhix;->a(J)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    iget-wide v4, p0, Lhix;->j:J

    add-long/2addr v4, v8

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lhix;->v:Ljava/util/Calendar;

    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0, v1}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v1, v0

    :goto_1
    if-nez v1, :cond_4

    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->b(JZ)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lhix;->d()V

    iget-boolean v0, p0, Lhix;->l:Z

    if-nez v0, :cond_5

    iget v0, p0, Lhix;->m:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lilv;->b(Z)V

    iget-object v0, p0, Lhix;->t:Lids;

    invoke-virtual {v0}, Lids;->k()I

    move-result v0

    iput v0, p0, Lhix;->m:I

    iget-object v0, p0, Lhix;->r:Lhkr;

    iget v2, p0, Lhix;->m:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lhkr;->a(IZ)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhix;->l:Z

    :cond_5
    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    iget-wide v1, v1, Lilt;->b:J

    invoke-static {v0, v1, v2}, Lilv;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, v8

    invoke-direct {p0, v0, v1, v8, v9}, Lhix;->a(JJ)V

    sget-object v0, Lhiz;->b:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    invoke-direct {p0}, Lhix;->e()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    iget-wide v2, p0, Lhix;->j:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    :cond_8
    :goto_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    add-long v1, v8, v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x0

    iget-object v0, p0, Lhix;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    iget-object v3, p0, Lhix;->v:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Lilt;->a(Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_a

    move-object v6, v0

    :goto_4
    iget-object v3, p0, Lhix;->s:Liha;

    iget-object v0, v3, Liha;->a:Lihb;

    iget-object v0, v0, Lihb;->e:Lihd;

    const-wide/32 v1, 0xe290

    iget-object v3, v3, Liha;->f:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lihd;->a(JJZ)Z

    move-result v0

    if-eqz v6, :cond_b

    if-nez v0, :cond_e

    :cond_b
    if-nez v0, :cond_c

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_c

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "Insufficient GPS budget."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    if-nez v6, :cond_d

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_d

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "After the last rush hours."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    invoke-direct {p0, v0, v8, v9}, Lhix;->a(Ljava/util/Calendar;J)V

    goto :goto_3

    :cond_e
    iget-object v0, p0, Lhix;->v:Ljava/util/Calendar;

    invoke-direct {p0, v0, v6, v8, v9}, Lhix;->a(Ljava/util/Calendar;Lilt;J)V

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_f
    iget-boolean v0, p0, Lhix;->h:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lhix;->u:Ljava/util/Random;

    iget-object v1, p0, Lhix;->t:Lids;

    invoke-virtual {v1}, Lids;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_10

    const-string v1, "BurstCollectionTrigger"

    const-string v4, "Will wait for %d ms before turning on GPS."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    int-to-long v0, v0

    add-long/2addr v0, v2

    iget-object v2, p0, Lhix;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lhix;->a(JJ)V

    sget-object v0, Lhiz;->c:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_11
    invoke-direct {p0, v2, v3}, Lhix;->a(J)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->b(JZ)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_12
    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    iget-wide v2, p0, Lhix;->j:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_13
    invoke-direct {p0, v2, v3}, Lhix;->a(J)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lhix;->a:Lidu;

    sget-object v1, Lidw;->a:Lidw;

    invoke-interface {v0, v1}, Lidu;->a(Lidw;)Z

    invoke-direct {p0}, Lhix;->f()V

    sget-object v0, Lhiz;->d:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_14
    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    iget-wide v2, p0, Lhix;->j:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_3
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_15
    iget-boolean v0, p0, Lhix;->g:Z

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Lhix;->d:Z

    if-eqz v0, :cond_17

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_16

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "Screen is on when GPS is asked to be turned on."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->b(JZ)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_17
    iget-boolean v0, p0, Lhix;->g:Z

    if-eqz v0, :cond_1d

    iget-boolean v0, p0, Lhix;->d:Z

    if-nez v0, :cond_1d

    invoke-direct {p0}, Lhix;->d()V

    iget-object v0, p0, Lhix;->s:Liha;

    iget-object v1, v0, Liha;->a:Lihb;

    iget-object v1, v1, Lihb;->e:Lihd;

    iget-object v0, v0, Liha;->f:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lihd;->b(J)J

    move-result-wide v0

    const-wide/32 v4, 0x1d4c0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    const-wide/32 v4, 0xe290

    cmp-long v4, v0, v4

    if-gez v4, :cond_19

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "Not enough GPS budgets, going off."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->b(JZ)V

    invoke-direct {p0}, Lhix;->e()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_19
    iget-object v4, p0, Lhix;->s:Liha;

    iget-object v5, v4, Liha;->e:Lihd;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v0, v1, v6}, Liha;->a(Lihd;JZ)Lihe;

    move-result-object v4

    iput-object v4, p0, Lhix;->p:Lihe;

    iget-object v4, p0, Lhix;->p:Lihe;

    if-nez v4, :cond_1b

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "No Gps budgets, going off."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lhix;->b(JZ)V

    invoke-direct {p0}, Lhix;->e()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_1b
    iget-object v2, p0, Lhix;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhix;->o:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1c

    const-string v0, "BurstCollectionTrigger"

    const-string v1, "Turning GPS on."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    iget-object v0, p0, Lhix;->a:Lidu;

    const-string v1, "BurstCollectionTrigger"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lidu;->a(Ljava/lang/String;Z)V

    iget-wide v0, p0, Lhix;->o:J

    iget-object v2, p0, Lhix;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lhix;->a(JJ)V

    sget-object v0, Lhiz;->e:Lhiz;

    iput-object v0, p0, Lhix;->b:Lhiz;

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_4
    const/4 v0, 0x0

    invoke-direct {p0, v2, v3, v0}, Lhix;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-wide v0, p0, Lhix;->o:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1e

    iget-object v2, p0, Lhix;->s:Liha;

    iget-object v3, p0, Lhix;->p:Lihe;

    invoke-virtual {v2, v3, v0, v1}, Liha;->a(Lihe;J)V

    :cond_1e
    iget-object v0, p0, Lhix;->a:Lidu;

    const-string v1, "BurstCollectionTrigger"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lidu;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhix;->o:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhix;->p:Lihe;

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_1f
    iget-object v0, p0, Lhix;->a:Lidu;

    const/16 v1, 0xa

    iget-wide v2, p0, Lhix;->j:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_20
    move-object v6, v1

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, Lhix;->a()V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lhix;->b:Lhiz;

    sget-object v1, Lhiz;->d:Lhiz;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhix;->b:Lhiz;

    sget-object v1, Lhiz;->e:Lhiz;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhix;->f:Z

    invoke-virtual {p0}, Lhix;->a()V

    :cond_1
    return-void
.end method
