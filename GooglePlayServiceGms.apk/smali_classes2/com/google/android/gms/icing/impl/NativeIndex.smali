.class public Lcom/google/android/gms/icing/impl/NativeIndex;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z


# instance fields
.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v0, "AppDataSearch"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetVersionCode()I

    move-result v0

    const v1, 0x41fea6

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/lang/UnsatisfiedLinkError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " vs 4325030"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "Native load error: %s"

    invoke-virtual {v0}, Ljava/lang/LinkageError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/io/File;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCreate([B[B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    return-void
.end method

.method public static a(D)I
    .locals 2

    const-wide v0, 0x406fe00000000000L    # 255.0

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(J)I
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumResults(J)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/io/File;)Lcom/google/android/gms/icing/impl/NativeIndex;
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/NativeIndex;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Error creating native index: %s"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static a([B)Lehr;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Lehr;->a([B)Lehr;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed parsing flush status"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static synthetic a(JII)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentLengths(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 1

    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeUnloadExtension()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v0

    const v1, 0x41fea6

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeLoadExtension([BI)Z

    move-result v0

    goto :goto_0
.end method

.method public static synthetic b(J)I
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumScored(J)I

    move-result v0

    return v0
.end method

.method public static b(Ljava/io/File;)J
    .locals 6

    const-wide/16 v0, -0x1

    sget-boolean v2, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    :try_start_0
    const-string v2, "getDiskUsage: %s"

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDiskUsage([B)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Bad path: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lehd;
    .locals 4

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetExtensionInfo([B)[B

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v1}, Lehd;->a([B)Lehd;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed parsing extension info"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static b([B)Leik;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Leik;

    invoke-direct {v0}, Leik;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Leik;->a([B)Leik;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed parsing suggestions"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Leik;

    invoke-direct {v0}, Leik;-><init>()V

    goto :goto_0
.end method

.method public static synthetic b(JII)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentBuffer(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)V
    .locals 1

    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSetLogPriority(I)V

    goto :goto_0
.end method

.method private static c([B)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Can\'t convert byte array to String"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, ""

    goto :goto_0
.end method

.method public static synthetic c(J)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseCorpusIds(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(JII)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDocHasTag(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;)[B
    .locals 2

    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic d(J)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriLengths(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static e(I)Ljava/lang/String;
    .locals 2

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error internal "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "ok"

    goto :goto_0

    :sswitch_1
    const-string v0, "ok trimmed"

    goto :goto_0

    :sswitch_2
    const-string v0, "ok duplicate uri replaced"

    goto :goto_0

    :sswitch_3
    const-string v0, "error uri not found"

    goto :goto_0

    :sswitch_4
    const-string v0, "error i/o"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_4
    .end sparse-switch
.end method

.method public static synthetic e(J)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriBuffer(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic f(J)[B
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDebugInfo(J)[B

    move-result-object v0

    return-object v0
.end method

.method public static synthetic g(J)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDestroy(J)V

    return-void
.end method

.method private native nativeAddCorpus(JJI)Z
.end method

.method private native nativeAddUsageReport(J[B)V
.end method

.method private native nativeClear(J)Z
.end method

.method private native nativeClearUsageReportData(J)Z
.end method

.method private native nativeCompact(JD[J[I)[B
.end method

.method private native nativeCreate([B[B)J
.end method

.method private native nativeDeleteCorpus(JI)[B
.end method

.method private native nativeDeleteDocument(JJI[B)I
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeExecuteQuery(J[B[BIII)J
.end method

.method private native nativeFlush(J)[B
.end method

.method private native nativeGetCompactStatus(J)[B
.end method

.method private native nativeGetDebugInfo(JI)[B
.end method

.method private static native nativeGetDiskUsage([B)J
.end method

.method private native nativeGetDocuments(J[[B[B)J
.end method

.method static native nativeGetExtensionInfo([B)[B
.end method

.method private native nativeGetIMEUpdates(JJII[[B)[B
.end method

.method private native nativeGetPhraseAffinityScores(J[B)[I
.end method

.method private native nativeGetUsageStats(J)[B
.end method

.method private static native nativeGetVersionCode()I
.end method

.method private native nativeIndexDocument(JJ[B[I)[B
.end method

.method private native nativeInit(J[B)[B
.end method

.method private native nativeIsIndexEmpty(J)Z
.end method

.method static native nativeLoadExtension([BI)Z
.end method

.method private native nativeMinFreeFraction(J)D
.end method

.method private native nativeNumDocuments(J)I
.end method

.method private native nativeNumPostingLists(J)I
.end method

.method private native nativeOnMaintenance(JZ)V
.end method

.method private native nativeOnSleep(J)V
.end method

.method private native nativePostFlush(J)Z
.end method

.method private static native nativeQueryResponseCorpusIds(J)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseDebugInfo(J)[B
.end method

.method private static native nativeQueryResponseDestroy(J)V
.end method

.method private static native nativeQueryResponseDocHasTag(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseNumResults(J)I
.end method

.method private static native nativeQueryResponseNumScored(J)I
.end method

.method private static native nativeQueryResponseSectionContentBuffer(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseSectionContentLengths(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseUriBuffer(J)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseUriLengths(J)Ljava/nio/ByteBuffer;
.end method

.method private native nativeRebuildIndex(J)[B
.end method

.method private native nativeRestoreIndex(J)[B
.end method

.method private static native nativeSetLogPriority(I)V
.end method

.method private native nativeSuggest(J[B[II[B)[B
.end method

.method private native nativeTagDocument(JJI[B[BIZ)I
.end method

.method static native nativeUnloadExtension()V
.end method

.method private native nativeUpgrade(JII)Z
.end method


# virtual methods
.method public final a(JILjava/lang/String;)I
    .locals 7

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v6

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteDocument(JJI[B)I

    move-result v0

    return v0
.end method

.method public final a(JILjava/lang/String;Ljava/lang/String;Z)I
    .locals 10

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {p5}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v7

    const v8, 0xfffe

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeTagDocument(JJI[B[BIZ)I

    move-result v0

    return v0
.end method

.method public final a(JLehm;)Landroid/util/Pair;
    .locals 7

    const/4 v0, 0x1

    new-array v6, v0, [I

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p3}, Lizs;->a(Lizs;)[B

    move-result-object v5

    move-object v0, p0

    move-wide v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeIndexDocument(JJ[B[I)[B

    move-result-object v0

    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    aget v2, v6, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final a(D[J[I)Lehr;
    .locals 7

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v0, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JD[J[I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final a(JII[Lehv;)Lehx;
    .locals 9

    const/4 v8, 0x0

    array-length v0, p5

    new-array v7, v0, [[B

    array-length v3, p5

    move v0, v8

    move v1, v8

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p5, v0

    add-int/lit8 v2, v1, 0x1

    invoke-static {v4}, Lizs;->a(Lizs;)[B

    move-result-object v4

    aput-object v4, v7, v1

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetIMEUpdates(JJII[[B)[B

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lehx;->a([B)Lehx;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed parsing ime update response"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lehr;)Lehy;
    .locals 4

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lizs;->a(Lizs;)[B

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeInit(J[B)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lehy;->a([B)Lehy;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed parsing init status"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[II)Leik;
    .locals 7

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v6, 0x0

    move-object v0, p0

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSuggest(J[B[II[B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b([B)Leik;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Leid;II)Lekz;
    .locals 8

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {p2}, Lizs;->a(Lizs;)[B

    move-result-object v4

    const v5, 0x186a0

    move-object v0, p0

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeExecuteQuery(J[B[BIII)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lekz;

    invoke-direct {v0, p2, v1, v2}, Lekz;-><init>(Leid;J)V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;Leie;)Lekz;
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    array-length v0, p1

    new-array v4, v0, [[B

    array-length v5, p1

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, p1, v0

    add-int/lit8 v3, v2, 0x1

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v6

    aput-object v6, v4, v2

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p2}, Lizs;->a(Lizs;)[B

    move-result-object v0

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDocuments(J[[B[B)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    new-instance v4, Leid;

    invoke-direct {v4}, Leid;-><init>()V

    new-array v0, v7, [Leie;

    aput-object p2, v0, v1

    iput-object v0, v4, Leid;->a:[Leie;

    iput-boolean v7, v4, Leid;->d:Z

    new-instance v0, Lekz;

    invoke-direct {v0, v4, v2, v3}, Lekz;-><init>(Leid;J)V

    goto :goto_1
.end method

.method public final a(Leia;)V
    .locals 3

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lizs;->a(Lizs;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAddUsageReport(J[B)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeOnMaintenance(JZ)V

    return-void
.end method

.method public final a(I)Z
    .locals 3

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const/16 v2, 0x26

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeUpgrade(JII)Z

    move-result v0

    return v0
.end method

.method public final a(JI)Z
    .locals 6

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAddCorpus(JJI)Z

    move-result v0

    return v0
.end method

.method public final a(Leib;)[I
    .locals 3

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lizs;->a(Lizs;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetPhraseAffinityScores(J[B)[I

    move-result-object v0

    return-object v0
.end method

.method public final b()Lehr;
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeRestoreIndex(J)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lehr;
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeRebuildIndex(J)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDebugInfo(JI)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lehr;
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteCorpus(JI)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDestroy(J)V

    :cond_0
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    return-void
.end method

.method public final e()Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClear(J)Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeOnSleep(J)V

    return-void
.end method

.method public finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->d()V

    return-void
.end method

.method public final g()Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeIsIndexEmpty(J)Z

    move-result v0

    return v0
.end method

.method public final h()Lehr;
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeFlush(J)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativePostFlush(J)Z

    move-result v0

    return v0
.end method

.method public final j()Lehr;
    .locals 7

    const/4 v5, 0x0

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const-wide/16 v3, 0x0

    move-object v0, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JD[J[I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lehr;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lehg;
    .locals 3

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetCompactStatus(J)[B

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lehg;->a([B)Lehg;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed parsing compact status"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumDocuments(J)I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumPostingLists(J)I

    move-result v0

    return v0
.end method

.method public final n()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeMinFreeFraction(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final o()Leit;
    .locals 4

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetUsageStats(J)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Leit;->a([B)Leit;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed parsing usage stats"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClearUsageReportData(J)Z

    move-result v0

    return v0
.end method
