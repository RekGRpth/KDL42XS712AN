.class public Lcom/google/android/gms/mdm/LockscreenActivity$LockMessageTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity$LockMessageTextView;->getLineCount()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity$LockMessageTextView;->setGravity(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity$LockMessageTextView;->setGravity(I)V

    goto :goto_0
.end method
