.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lgmt;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private A:Ljava/util/List;

.field private B:Ljava/util/List;

.field private C:Ljava/util/List;

.field private D:Ljava/util/List;

.field private E:Ljava/lang/String;

.field private F:Ljava/util/List;

.field private G:Ljava/util/List;

.field private H:Ljava/util/List;

.field private I:Ljava/util/List;

.field private J:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

.field private K:Ljava/util/List;

.field private L:Ljava/util/List;

.field private final e:Ljava/util/Set;

.field private final f:I

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Ljava/util/List;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/List;

.field private q:Ljava/util/List;

.field private r:Ljava/lang/String;

.field private s:Ljava/util/List;

.field private t:Ljava/util/List;

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

.field private w:Ljava/util/List;

.field private x:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

.field private y:Ljava/util/List;

.field private z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lgmt;

    invoke-direct {v0}, Lgmt;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lgmt;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "abouts"

    const-string v2, "abouts"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Abouts;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "addresses"

    const-string v2, "addresses"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "ageRange"

    const-string v2, "ageRange"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "birthdays"

    const-string v2, "birthdays"

    const/4 v3, 0x5

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "braggingRights"

    const-string v2, "braggingRights"

    const/4 v3, 0x6

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$BraggingRights;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "coverPhotos"

    const-string v2, "coverPhotos"

    const/4 v3, 0x7

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "customFields"

    const-string v2, "customFields"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CustomFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "emails"

    const-string v2, "emails"

    const/16 v3, 0x9

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "events"

    const-string v2, "events"

    const/16 v3, 0xb

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Events;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "genders"

    const-string v2, "genders"

    const/16 v3, 0xc

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/16 v3, 0xd

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "images"

    const-string v2, "images"

    const/16 v3, 0xe

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "instantMessaging"

    const-string v2, "instantMessaging"

    const/16 v3, 0xf

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "language"

    const-string v2, "language"

    const/16 v3, 0x11

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "legacyFields"

    const-string v2, "legacyFields"

    const/16 v3, 0x12

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "linkedPeople"

    const-string v2, "linkedPeople"

    const/16 v3, 0x13

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/16 v3, 0x14

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "names"

    const-string v2, "names"

    const/16 v3, 0x15

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "nicknames"

    const-string v2, "nicknames"

    const/16 v3, 0x16

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Nicknames;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "occupations"

    const-string v2, "occupations"

    const/16 v3, 0x17

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Occupations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "organizations"

    const-string v2, "organizations"

    const/16 v3, 0x18

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Organizations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "phoneNumbers"

    const-string v2, "phoneNumbers"

    const/16 v3, 0x19

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "placesLived"

    const-string v2, "placesLived"

    const/16 v3, 0x1a

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PlacesLived;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "profileUrl"

    const-string v2, "profileUrl"

    const/16 v3, 0x1b

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "relations"

    const-string v2, "relations"

    const/16 v3, 0x1c

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Relations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "relationshipInterests"

    const-string v2, "relationshipInterests"

    const/16 v3, 0x1d

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipInterests;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "relationshipStatuses"

    const-string v2, "relationshipStatuses"

    const/16 v3, 0x1e

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipStatuses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "skills"

    const-string v2, "skills"

    const/16 v3, 0x1f

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Skills;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "sortKeys"

    const-string v2, "sortKeys"

    const/16 v3, 0x20

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "taglines"

    const-string v2, "taglines"

    const/16 v3, 0x21

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    const-string v1, "urls"

    const-string v2, "urls"

    const/16 v3, 0x22

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Urls;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:I

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    iput-object p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    iput-object p13, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    iput-object p14, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Ljava/util/List;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/util/List;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/lang/String;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Ljava/util/List;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final A()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    return-object v0
.end method

.method public final B()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/util/List;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    return-object v0
.end method

.method public final E()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    return-object v0
.end method

.method public final F()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    return-object v0
.end method

.method public final G()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Ljava/util/List;

    return-object v0
.end method

.method public final H()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    return-object v0
.end method

.method public final I()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    return-object v0
.end method

.method public final J()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/List;

    return-object v0
.end method

.method public final a()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    :sswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    goto :goto_0

    :sswitch_2
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x14 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xa -> :sswitch_1
        0xd -> :sswitch_2
        0x11 -> :sswitch_3
        0x1b -> :sswitch_4
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    :goto_0
    return-object v0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/util/List;

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    goto :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    goto :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Ljava/util/List;

    goto :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    goto :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/lang/String;

    goto :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    goto :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    goto :goto_0

    :pswitch_12
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    goto :goto_0

    :pswitch_13
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    goto :goto_0

    :pswitch_14
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    goto :goto_0

    :pswitch_15
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    goto :goto_0

    :pswitch_16
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    goto :goto_0

    :pswitch_17
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    goto :goto_0

    :pswitch_18
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/util/List;

    goto :goto_0

    :pswitch_19
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/lang/String;

    goto :goto_0

    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    goto :goto_0

    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    goto :goto_0

    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    goto :goto_0

    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Ljava/util/List;

    goto :goto_0

    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    goto :goto_0

    :pswitch_1f
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    goto :goto_0

    :pswitch_20
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/List;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    goto :goto_0

    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    goto :goto_0

    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/util/List;

    goto :goto_0

    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    goto :goto_0

    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    goto :goto_0

    :pswitch_7
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    goto :goto_0

    :pswitch_8
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    goto :goto_0

    :pswitch_9
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    goto :goto_0

    :pswitch_a
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Ljava/util/List;

    goto :goto_0

    :pswitch_b
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    goto :goto_0

    :pswitch_c
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    goto :goto_0

    :pswitch_d
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    goto :goto_0

    :pswitch_e
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    goto :goto_0

    :pswitch_f
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    goto :goto_0

    :pswitch_10
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    goto :goto_0

    :pswitch_11
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    goto :goto_0

    :pswitch_12
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/util/List;

    goto :goto_0

    :pswitch_13
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    goto :goto_0

    :pswitch_14
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    goto :goto_0

    :pswitch_15
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    goto :goto_0

    :pswitch_16
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Ljava/util/List;

    goto :goto_0

    :pswitch_17
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    goto :goto_0

    :pswitch_18
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/List;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:I

    return v0
.end method

.method protected final c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    return-object v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lgmt;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/util/List;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final i()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/util/Set;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Ljava/util/List;

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    return-object v0
.end method

.method public final v()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    return-object v0
.end method

.method public final w()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lgmt;

    invoke-static {p0, p1, p2}, Lgmt;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/os/Parcel;I)V

    return-void
.end method

.method public final x()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    return-object v0
.end method

.method public final y()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    return-object v0
.end method

.method public final z()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    return-object v0
.end method
