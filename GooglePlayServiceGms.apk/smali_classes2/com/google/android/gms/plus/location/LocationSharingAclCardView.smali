.class public Lcom/google/android/gms/plus/location/LocationSharingAclCardView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Lcom/google/android/gms/plus/location/MarkerIconView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/view/View;

.field private j:Lfxe;

.field private k:Lfxd;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->k:Lfxd;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->g:Landroid/widget/Button;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->k:Lfxd;

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->j:Lfxe;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->k:Lfxd;

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->j:Lfxe;

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->a:Landroid/view/View;

    if-nez v0, :cond_0

    const v0, 0x7f0a0214    # com.google.android.gms.R.id.map

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->a:Landroid/view/View;

    const v0, 0x7f0a0215    # com.google.android.gms.R.id.marker_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/location/MarkerIconView;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->b:Lcom/google/android/gms/plus/location/MarkerIconView;

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    const v0, 0x7f0a0216    # com.google.android.gms.R.id.example

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    const v0, 0x7f0a0218    # com.google.android.gms.R.id.acl

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->f:Landroid/widget/TextView;

    const v0, 0x7f0a021a    # com.google.android.gms.R.id.choose_acl_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->g:Landroid/widget/Button;

    const v0, 0x7f0a0219    # com.google.android.gms.R.id.edit_acl_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->h:Landroid/widget/Button;

    const v0, 0x7f0a021b    # com.google.android.gms.R.id.disabled_screen

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->i:Landroid/view/View;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->h:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/plus/location/LocationSharingAclCardView;->l:Z

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
