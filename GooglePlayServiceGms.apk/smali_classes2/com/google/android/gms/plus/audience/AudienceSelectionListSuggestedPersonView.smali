.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;
.super Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->a:Landroid/view/View;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->onFinishInflate()V

    const v0, 0x7f0a026e    # com.google.android.gms.R.id.audience_selection_suggested_check

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->a:Landroid/view/View;

    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->a:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
