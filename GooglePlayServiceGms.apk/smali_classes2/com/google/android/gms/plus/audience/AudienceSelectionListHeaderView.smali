.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a0262    # com.google.android.gms.R.id.top_border

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->a:Landroid/view/View;

    const v0, 0x7f0a0268    # com.google.android.gms.R.id.audience_selection_header_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->b:Landroid/widget/TextView;

    return-void
.end method
