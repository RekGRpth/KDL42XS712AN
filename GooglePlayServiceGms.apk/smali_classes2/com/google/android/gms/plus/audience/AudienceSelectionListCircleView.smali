.class public final Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
.super Lfqz;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfqz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const v0, 0x7f02020a    # com.google.android.gms.R.drawable.plus_iconic_ic_circles_black_16

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d()V

    invoke-super {p0}, Lfqz;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0363    # com.google.android.gms.R.string.plus_circle_size_pattern

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final bridge synthetic a(Lfra;)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->a(Lfra;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final bridge synthetic a(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public final bridge synthetic b(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->b(Z)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/TextView;

    const v1, 0x7f0b0361    # com.google.android.gms.R.string.plus_audience_selection_hidden_circle

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final bridge synthetic c(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->c(Z)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final bridge synthetic d(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->d(Z)V

    return-void
.end method

.method public final bridge synthetic e()Z
    .locals 1

    invoke-super {p0}, Lfqz;->e()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lfqz;->f()V

    return-void
.end method

.method public final bridge synthetic g()V
    .locals 0

    invoke-super {p0}, Lfqz;->g()V

    return-void
.end method

.method public final bridge synthetic isChecked()Z
    .locals 1

    invoke-super {p0}, Lfqz;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfqz;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public final bridge synthetic onClick(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected final onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lfqz;->onFinishInflate()V

    const v0, 0x7f0a0265    # com.google.android.gms.R.id.audience_selection_circle_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a:Landroid/widget/TextView;

    const v0, 0x7f0a0266    # com.google.android.gms.R.id.audience_selection_circle_count

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    const v0, 0x7f0a0263    # com.google.android.gms.R.id.audience_selection_circle_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/ImageView;

    const v0, 0x7f0a0267    # com.google.android.gms.R.id.audience_selection_circle_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/TextView;

    return-void
.end method

.method public final bridge synthetic setChecked(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->setChecked(Z)V

    return-void
.end method

.method public final bridge synthetic toggle()V
    .locals 0

    invoke-super {p0}, Lfqz;->toggle()V

    return-void
.end method
