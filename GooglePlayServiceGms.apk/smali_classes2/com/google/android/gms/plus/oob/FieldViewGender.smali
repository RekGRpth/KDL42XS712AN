.class public final Lcom/google/android/gms/plus/oob/FieldViewGender;
.super Lfye;
.source "SourceFile"

# interfaces
.implements Lfyr;


# instance fields
.field private d:Lfyq;

.field private e:Lcom/google/android/gms/plus/oob/GenderSpinner;

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400e4    # com.google.android.gms.R.layout.plus_oob_field_gender_setup_wizard

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400e3    # com.google.android.gms.R.layout.plus_oob_field_gender

    goto :goto_0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 11

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->x()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v1, v2

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->r()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v3, v4

    :goto_1
    if-ge v3, v7, :cond_2

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfs;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lgfs;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iput v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    iput-boolean v10, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Z

    iget-boolean v8, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lfyf;

    sget-object v9, Lbcq;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v8, v9}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iput-boolean v10, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    :cond_0
    invoke-interface {v0}, Lgfs;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lfyq;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v3, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lfyq;-><init>(Landroid/content/Context;[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    const v1, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v0, v1}, Lfyq;->setDropDownViewResource(I)V

    const v0, 0x7f0a02a3    # com.google.android.gms.R.id.oob_field_gender_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/GenderSpinner;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->a(Lfyr;)V

    invoke-interface {p1}, Lgfm;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lgfm;->m()Lgfq;

    move-result-object v0

    invoke-interface {v0}, Lgfq;->h()Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03e7    # com.google.android.gms.R.string.plus_gender_prompt

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    :cond_5
    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lfyf;

    sget-object v1, Lbcq;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    invoke-virtual {v0}, Lfyq;->a()V

    iput p1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lfyf;

    invoke-interface {v0}, Lfyf;->a()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lfyf;

    sget-object v1, Lbcq;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lgfm;
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->i()Lgfn;

    move-result-object v1

    new-instance v2, Lgfu;

    invoke-direct {v2}, Lgfu;-><init>()V

    iget v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v4}, Lgfm;->t()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    iput-object v0, v2, Lgfu;->d:Ljava/lang/String;

    iget-object v0, v2, Lgfu;->f:Ljava/util/Set;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lgfu;->a()Lgft;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v4}, Lgfm;->r()Ljava/util/List;

    move-result-object v4

    if-ltz v3, :cond_1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_2

    :cond_1
    const-string v3, "FieldView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid position for options field: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lgfm;

    invoke-interface {v5}, Lgfm;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfs;

    invoke-interface {v0}, Lgfs;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfyq;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Z

    iget v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->c:I

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Lfye;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->a:Z

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->b:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lfyq;

    invoke-virtual {v0}, Lfyq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->c:I

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v0

    goto :goto_0
.end method
