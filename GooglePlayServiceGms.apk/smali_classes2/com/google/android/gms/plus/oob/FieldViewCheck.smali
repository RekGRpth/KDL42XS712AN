.class public final Lcom/google/android/gms/plus/oob/FieldViewCheck;
.super Lfye;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private d:Landroid/widget/CheckBox;

.field private e:Lgqc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400e2    # com.google.android.gms.R.layout.plus_oob_field_check_setup_wizard

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400e1    # com.google.android.gms.R.layout.plus_oob_field_check

    goto :goto_0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0b0330    # com.google.android.gms.R.string.plus_oob_field_view_tag_check

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v0, Lgqc;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-direct {v0, v1}, Lgqc;-><init>(Landroid/widget/CheckBox;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->e:Lgqc;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->e:Lgqc;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lgfm;
    .locals 4

    const-string v0, "customAds"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->c:Lfyf;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbcq;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-interface {v1, v0}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->i()Lgfn;

    move-result-object v0

    new-instance v1, Lgfu;

    invoke-direct {v1}, Lgfu;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iput-boolean v2, v1, Lgfu;->a:Z

    iget-object v2, v1, Lgfu;->f:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lgfu;->a()Lgft;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lbcq;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method protected final g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    const-string v0, "picasa"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbcq;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "customAds"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbcq;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->c:Lfyf;

    invoke-interface {v0}, Lfyf;->a()V

    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-boolean v1, p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Lfye;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->a:Z

    return-object v1
.end method
