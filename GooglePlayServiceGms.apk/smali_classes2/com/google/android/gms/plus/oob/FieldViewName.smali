.class public final Lcom/google/android/gms/plus/oob/FieldViewName;
.super Lfye;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Z

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lbpo;->a(Landroid/content/Context;Landroid/view/View;)Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/oob/FieldViewName;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    new-instance v0, Lfyl;

    invoke-direct {v0, p0, p1}, Lfyl;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;Landroid/view/View;)V

    const-wide/16 v1, 0xfa

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->b(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->b(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lfzd;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->i()Lgfv;

    move-result-object v0

    invoke-interface {v0}, Lgfv;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->i()Lgfv;

    move-result-object v0

    invoke-interface {v0}, Lgfv;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400ea    # com.google.android.gms.R.layout.plus_oob_field_name_setup_wizard

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400e9    # com.google.android.gms.R.layout.plus_oob_field_name

    goto :goto_0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 8

    const/16 v7, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0a02a6    # com.google.android.gms.R.id.name_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {}, Lfzd;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0400f4    # com.google.android.gms.R.layout.plus_oob_name_fields_last_first_setup_wizard

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_1
    const v1, 0x7f0a02a9    # com.google.android.gms.R.id.first_name_edit

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a02aa    # com.google.android.gms.R.id.last_name_edit

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    new-instance v3, Lfym;

    invoke-direct {v3, p0, v2}, Lfym;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;B)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    new-instance v3, Lfym;

    invoke-direct {v3, p0, v2}, Lfym;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;B)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setVisibility(I)V

    invoke-static {}, Lfzd;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_6

    const v1, 0x7f0400f0    # com.google.android.gms.R.layout.plus_oob_name_fields_combined_setup_wizard

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0a02a8    # com.google.android.gms.R.id.combined_name_edit

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-interface {p1}, Lgfm;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b03ed    # com.google.android.gms.R.string.plus_oob_first_and_last_name

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->o()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a02a5    # com.google.android.gms.R.id.edit_name_warning

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0a02a7    # com.google.android.gms.R.id.name_error

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Lgfm;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_5
    if-ge v1, v4, :cond_8

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfo;

    invoke-interface {v0}, Lgfo;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Lgfo;->l()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Lgfo;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->c:Lfyf;

    invoke-interface {v0}, Lgfo;->k()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lgfo;->d()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lgfo;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v5, v6, v7, v0}, Lfzd;->a(Lfyf;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_2
    const v1, 0x7f0400f3    # com.google.android.gms.R.layout.plus_oob_name_fields_last_first

    goto/16 :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_4

    const v1, 0x7f0400f2    # com.google.android.gms.R.layout.plus_oob_name_fields_first_last_setup_wizard

    :goto_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_1

    :cond_4
    const v1, 0x7f0400f1    # com.google.android.gms.R.layout.plus_oob_name_fields_first_last

    goto :goto_6

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    goto/16 :goto_2

    :cond_6
    const v1, 0x7f0400ef    # com.google.android.gms.R.layout.plus_oob_name_fields_combined

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_4

    :cond_8
    return-void
.end method

.method public final b()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lgfm;
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->i()Lgfn;

    move-result-object v1

    new-instance v2, Lgfu;

    invoke-direct {v2}, Lgfu;-><init>()V

    new-instance v3, Lgfw;

    invoke-direct {v3}, Lgfw;-><init>()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lgfw;->b:Ljava/lang/String;

    iget-object v0, v3, Lgfw;->c:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lgfw;->a:Ljava/lang/String;

    iget-object v0, v3, Lgfw;->c:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iget-object v4, v3, Lgfw;->c:Ljava/util/Set;

    iget-object v5, v3, Lgfw;->a:Ljava/lang/String;

    iget-object v3, v3, Lgfw;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v5, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iput-object v0, v2, Lgfu;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iget-object v0, v2, Lgfu;->f:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lgfu;->a()Lgft;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a02a8    # com.google.android.gms.R.id.combined_name_edit

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->m()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    :cond_0
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x5

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->performClick()Z

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a02a8    # com.google.android.gms.R.id.combined_name_edit

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/oob/FieldViewName;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    iget v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->b:I

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->m()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Lfye;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    invoke-direct {v2, v1}, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    iput-boolean v1, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->a:Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbpo;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_0
    :goto_1
    iput v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->b:I

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->d:Ljava/lang/String;

    return-object v2

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
