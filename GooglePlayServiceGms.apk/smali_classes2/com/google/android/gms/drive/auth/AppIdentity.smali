.class public final Lcom/google/android/gms/drive/auth/AppIdentity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Lcom/google/android/gms/drive/auth/AppIdentity;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x1

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/auth/AppIdentity;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    new-instance v0, Lbsk;

    invoke-direct {v0}, Lbsk;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-eqz p2, :cond_0

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-static {p3}, Lcbt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    sget-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 2

    new-instance v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/drive/auth/AppIdentity;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 2

    const-string v0, "isSuperuser"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "packageName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "hash"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lorg/json/JSONObject;
    .locals 3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "isSuperuser"

    iget-boolean v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget-boolean v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-nez v1, :cond_0

    const-string v1, "packageName"

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "hash"

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/auth/AppIdentity;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppIdentity [packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
