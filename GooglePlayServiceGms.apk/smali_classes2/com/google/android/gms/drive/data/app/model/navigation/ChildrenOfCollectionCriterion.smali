.class public Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/gms/drive/DriveId;

.field private c:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbtk;

    invoke-direct {v0}, Lbtk;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    const-class v0, Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->c:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->c:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final a(Lcfz;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lbtl;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No collection with driveId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbtl;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v3

    invoke-virtual {v3}, Lcef;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in (select "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcdm;->b:Lcdm;

    invoke-virtual {v3}, Lcdm;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcdl;->a()Lcdl;

    move-result-object v3

    invoke-virtual {v3}, Lcdl;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " where "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcdm;->a:Lcdm;

    invoke-virtual {v3}, Lcdm;->a()Lcdp;

    move-result-object v3

    invoke-virtual {v3}, Lcdp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->c:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-void
.end method

.method public final b(Lcfz;)Lbvi;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    sget-object v0, Lbvi;->b:Lbvi;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbvi;->a(Ljava/lang/String;)Lbvi;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ChildrenOfCollectionCriterion {DriveId=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
