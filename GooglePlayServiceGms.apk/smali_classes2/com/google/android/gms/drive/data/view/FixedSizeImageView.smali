.class public Lcom/google/android/gms/drive/data/view/FixedSizeImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    return-void
.end method


# virtual methods
.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iput-boolean v1, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    throw v0
.end method

.method public setImageResource(I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iput-boolean v1, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/FixedSizeImageView;->a:Z

    throw v0
.end method
