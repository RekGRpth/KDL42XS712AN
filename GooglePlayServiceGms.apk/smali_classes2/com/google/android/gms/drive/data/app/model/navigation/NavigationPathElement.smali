.class public Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

.field private b:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbts;

    invoke-direct {v0}, Lbts;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    return-object v0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    return-void
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    iget-object v0, p1, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "NavigationPathElement { CriterionSet: %s, savedState=%s }"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
