.class public Lcom/google/android/gms/drive/data/view/DocListView;
.super Lccy;
.source "SourceFile"

# interfaces
.implements Lbzr;
.implements Lcam;
.implements Lccx;


# instance fields
.field private final A:Ljava/util/Set;

.field private B:Lbzg;

.field private C:Lbzq;

.field private d:Landroid/support/v4/app/Fragment;

.field private e:Lbzr;

.field private f:Z

.field private g:Ljava/lang/String;

.field private final h:Lcoy;

.field private final i:Lcfz;

.field private final j:Lbvo;

.field private k:Lbvk;

.field private final l:Lcbj;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/ListView;

.field private o:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

.field private p:Lcfc;

.field private q:Lcom/google/android/gms/drive/DriveId;

.field private r:Lccw;

.field private s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

.field private t:Lccs;

.field private u:Z

.field private v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

.field private final w:Ljava/lang/Thread;

.field private x:Lcat;

.field private y:Lcag;

.field private final z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lccy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->q:Lcom/google/android/gms/drive/DriveId;

    sget-object v0, Lccw;->c:Lccw;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->r:Lccw;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->A:Ljava/util/Set;

    invoke-static {p1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    invoke-virtual {v0}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->i:Lcfz;

    new-instance v0, Lcbj;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    invoke-direct {v0, v1}, Lcbj;-><init>(Lcoy;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->l:Lcbj;

    new-instance v0, Lbvo;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    invoke-direct {v0, v1}, Lbvo;-><init>(Lcoy;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->j:Lbvo;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->w:Ljava/lang/Thread;

    const/4 v1, 0x0

    sget-object v0, Lbqs;->y:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->z:J

    iput-object p0, p0, Lccy;->c:Lcam;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/view/DocListView;Lbvk;)Lbvk;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/view/DocListView;)Lcfz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->i:Lcfz;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/view/DocListView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/view/DocListView;->b(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/drive/data/view/DocListView;)Lbvo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->j:Lbvo;

    return-object v0
.end method

.method private b(Z)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->e()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->a()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lbqs;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    sget-object v0, Lbqs;->H:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    new-instance v4, Lccu;

    invoke-direct {v4, p0, v2}, Lccu;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;Z)V

    invoke-interface {v3, v4, v0}, Lbvk;->a(Lbvl;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->k()Lbzf;

    move-result-object v0

    invoke-interface {v0}, Lbzf;->b()V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->l()V

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->j()V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->a()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v3, :cond_4

    const v4, 0x7f0b0079    # com.google.android.gms.R.string.drive_sync_more_in_progress

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->k()Lbzf;

    move-result-object v4

    if-nez v3, :cond_6

    :goto_3
    invoke-interface {v4, v1, v0}, Lbzf;->a(ZLjava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v4}, Lbvk;->c()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, v4, v6

    if-nez v6, :cond_5

    const v4, 0x7f0b0076    # com.google.android.gms.R.string.drive_sync_more

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0b0077    # com.google.android.gms.R.string.drive_sync_more_before_template

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3
.end method

.method public static synthetic c(Lcom/google/android/gms/drive/data/view/DocListView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->u:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/gms/drive/data/view/DocListView;)Lcfc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/drive/data/view/DocListView;)Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/drive/data/view/DocListView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/drive/data/view/DocListView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->w:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    return-void
.end method

.method public static synthetic h(Lcom/google/android/gms/drive/data/view/DocListView;)Lbzf;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->k()Lbzf;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/drive/data/view/DocListView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->j()V

    return-void
.end method

.method private j()V
    .locals 5

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->k()Lbzf;

    move-result-object v0

    invoke-interface {v0}, Lbzf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->m:Landroid/view/View;

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    if-ne v0, v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->o:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    if-ne v0, v1, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->m:Landroid/view/View;

    if-ne v0, v4, :cond_4

    :goto_4
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_4
.end method

.method private k()Lbzf;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->B:Lbzg;

    if-nez v0, :cond_0

    new-instance v0, Lbzg;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/DocListView;->C:Lbzq;

    invoke-direct {v0, v1, p0, v2, v3}, Lbzg;-><init>(Lcoy;Lcom/google/android/gms/drive/data/view/DocListView;Landroid/widget/ListView;Lbzq;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->B:Lbzg;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->B:Lbzg;

    return-object v0
.end method

.method private l()V
    .locals 7

    const v6, 0x7f0a00f9    # com.google.android.gms.R.id.empty_list_syncing

    const v5, 0x7f0a00f8    # com.google.android.gms.R.id.empty_list_message

    const/16 v4, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->f()Lbzk;

    move-result-object v0

    invoke-virtual {v0}, Lbzk;->a()Lbvi;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->r:Lccw;

    sget-object v3, Lccw;->c:Lccw;

    invoke-virtual {v0, v3}, Lccw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lbvi;->b:Lbvi;

    invoke-virtual {v2, v0}, Lbvi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->r:Lccw;

    sget-object v2, Lccw;->a:Lccw;

    invoke-virtual {v1, v2}, Lccw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0b007a    # com.google.android.gms.R.string.drive_sync_waiting

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    const v0, 0x7f0a00f4    # com.google.android.gms.R.id.doc_list_syncing_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const v1, 0x7f0b007b    # com.google.android.gms.R.string.drive_sync_waiting_subtitle

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->d:Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->d:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->e:Lbzr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->e:Lbzr;

    invoke-interface {v0, p1, p2, p3}, Lbzr;->a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->A:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lbzq;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->C:Lbzq;

    return-void
.end method

.method public final a(Lbzr;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->e:Lbzr;

    return-void
.end method

.method public final a(Lcat;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcat;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->x:Lcat;

    return-void
.end method

.method public final a(Lccw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->r:Lccw;

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->l()V

    return-void
.end method

.method public final a(Lcfc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->q:Lcom/google/android/gms/drive/DriveId;

    iget-object v0, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccy;->b:Lcom/google/android/gms/drive/data/view/CustomListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/CustomListView;->invalidateViews()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->x:Lcat;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->x:Lcat;

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->h:Lcoy;

    invoke-virtual {v0, v4}, Lcat;->a(Lcoy;)Lcag;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->l()V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    invoke-virtual {v4}, Lcag;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    const/16 v6, 0x12

    new-array v6, v6, [Ljava/lang/String;

    const-string v7, "_id"

    aput-object v7, v6, v1

    sget-object v7, Lceg;->a:Lceg;

    invoke-virtual {v7}, Lceg;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    sget-object v7, Lceg;->n:Lceg;

    invoke-virtual {v7}, Lceg;->a()Lcdp;

    move-result-object v7

    invoke-virtual {v7}, Lcdp;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x3

    sget-object v8, Lceg;->p:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, Lceg;->f:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, Lceg;->o:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lceg;->c:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lceg;->d:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lceg;->j:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lceg;->l:Lceg;

    invoke-virtual {v8}, Lceg;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xa

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v8

    invoke-virtual {v8}, Lcef;->f()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xb

    invoke-static {}, Lcey;->a()Lcey;

    move-result-object v8

    invoke-virtual {v8}, Lcey;->f()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, Lcez;->d:Lcez;

    invoke-virtual {v8}, Lcez;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xd

    sget-object v8, Lcez;->c:Lcez;

    invoke-virtual {v8}, Lcez;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xe

    sget-object v8, Lcez;->j:Lcez;

    invoke-virtual {v8}, Lcez;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xf

    sget-object v8, Lcez;->f:Lcez;

    invoke-virtual {v8}, Lcez;->a()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x10

    invoke-virtual {v5}, Lcag;->c()Lcdp;

    move-result-object v8

    invoke-virtual {v8}, Lcdp;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x11

    invoke-virtual {v5}, Lcag;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v7

    invoke-static {v6}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    new-instance v6, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-direct {v6, v3, v4, v5}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Ljava/util/Set;)V

    iput-object v6, p0, Lcom/google/android/gms/drive/data/view/DocListView;->s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    iget-object v5, p0, Lcom/google/android/gms/drive/data/view/DocListView;->i:Lcfz;

    iget-object v6, p0, Lcom/google/android/gms/drive/data/view/DocListView;->s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v4, v6}, Lcfz;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    new-instance v4, Lbzs;

    invoke-direct {v4, v3}, Lbzs;-><init>(Landroid/database/Cursor;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    invoke-virtual {v3, v4}, Lcag;->a(Lbzb;)V

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->k()Lbzf;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    iget-object v6, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    invoke-interface {v3, v5, v4, v6}, Lbzf;->a(Lcfc;Lbzb;Lcag;)V

    if-eqz v0, :cond_5

    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Z)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->t:Lccs;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->t:Lccs;

    iget-object v4, v0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v4}, Lcom/google/android/gms/drive/data/view/DocListView;->g(Lcom/google/android/gms/drive/data/view/DocListView;)V

    iput-boolean v2, v0, Lccs;->a:Z

    :cond_2
    new-instance v0, Lccs;

    invoke-direct {v0, p0, v1}, Lccs;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->t:Lccs;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->b()Landroid/os/Parcelable;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->f:Z

    if-nez v1, :cond_6

    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/DocListView;->f:Z

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    invoke-virtual {p0}, Lccy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lccy;->a:Lcal;

    const/high16 v2, 0x41d00000    # 26.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v9, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcal;->d(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    invoke-virtual {p0}, Lccy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lccy;->a:Lcal;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2, v0}, Lcal;->a(ILandroid/content/res/Resources;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->i()V

    invoke-direct {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->j()V

    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->f()Lbzk;

    move-result-object v0

    invoke-virtual {v0}, Lbzk;->b()Lcat;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_1

    :cond_5
    invoke-interface {v3}, Lbzf;->b()V

    goto :goto_2

    :cond_6
    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_3

    :cond_7
    invoke-interface {v3}, Lbzf;->a()V

    goto :goto_3
.end method

.method public final a(Z)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    sget-object v0, Lbth;->d:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->i:Lcfz;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    if-eqz v0, :cond_4

    new-instance v0, Lcct;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-direct {v0, p0, p1, v2, v3}, Lcct;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;ZLcfc;Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcct;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/view/DocListView;->b(Z)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->p:Lcfc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->e()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lbqs;->G:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    new-instance v2, Lccu;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lccu;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;Z)V

    invoke-interface {v1, v2, v0}, Lbvk;->a(Lbvl;I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Z)V

    :cond_0
    return-void
.end method

.method public final c()Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->s:Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    return-object v0
.end method

.method public final d()Lal;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->t:Lccs;

    return-object v0
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lccy;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    invoke-interface {v0}, Lbvk;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->k:Lbvk;

    :cond_0
    return-void
.end method

.method public final f()Lbzk;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->v:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->c()Lbzk;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    sget-object v0, Lbzk;->a:Lbzk;

    :cond_1
    return-object v0
.end method

.method public final g()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->q:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->y:Lcag;

    iget-object v0, v0, Lcag;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lccy;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->u:Z

    invoke-super {p0}, Lccy;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Lccy;->onFinishInflate()V

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    const v0, 0x7f0a00f7    # com.google.android.gms.R.id.empty_list

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->m:Landroid/view/View;

    const v0, 0x7f0a00f6    # com.google.android.gms.R.id.sticky_header

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->o:Lcom/google/android/gms/drive/data/view/StickyHeaderView;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->n:Landroid/widget/ListView;

    new-instance v1, Lccr;

    invoke-direct {v1, p0}, Lccr;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    :cond_0
    invoke-super {p0, p1, p2}, Lccy;->onMeasure(II)V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lccy;->onScroll(Landroid/widget/AbsListView;III)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/DocListView;->l:Lcbj;

    iget-wide v4, p0, Lcom/google/android/gms/drive/data/view/DocListView;->z:J

    cmp-long v0, v4, v8

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->b(Z)V

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcbj;->a:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-ltz v0, :cond_4

    :goto_2
    invoke-static {v1}, Lbkm;->a(Z)V

    :cond_1
    iget-object v0, v3, Lcbj;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-gez v2, :cond_2

    iget-object v2, v3, Lcbj;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lccy;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/DocListView;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 0

    invoke-super {p0, p1}, Lccy;->setBackgroundResource(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "%s[mainFilter=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/DocListView;->f()Lbzk;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
