.class public Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/util/List;

.field private b:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbtn;

    invoke-direct {v0}, Lbtn;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final a(Lcfz;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;->a(Lcfz;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->d()Lceu;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_1
    sget-object v3, Lcev;->a:Lcev;

    invoke-interface {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lceu;->a(Lcev;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lceu;

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lceu;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-void

    :cond_3
    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcfz;)Lbvi;
    .locals 3

    sget-object v0, Lbvi;->a:Lbvi;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;->b(Lcfz;)Lbvi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbvi;->a(Lbvi;)Lbvi;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    instance-of v2, v0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lbzk;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;

    instance-of v3, v0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "More than one main filter : %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c()Lbzk;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c()Lbzk;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a(Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p1, p0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a(Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "CriterionSet %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    return-void
.end method
