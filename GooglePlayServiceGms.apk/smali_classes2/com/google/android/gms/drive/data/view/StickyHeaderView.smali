.class public Lcom/google/android/gms/drive/data/view/StickyHeaderView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:Lcdb;

.field private b:Landroid/view/View;

.field private c:Lcar;

.field private d:I

.field private e:I

.field private f:Z

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    return-void
.end method


# virtual methods
.method public final a(Lcdb;)V
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {p1}, Lcdb;->a()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->removeView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0}, Lcdb;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    iput-boolean v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->h:Z

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    sget-object v3, Lcda;->b:Lcda;

    invoke-interface {p1, v2, v3}, Lcdb;->a(Landroid/view/View;Lcda;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->g:I

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->c:Lcar;

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    const v1, 0x3dcccccd    # 0.1f

    const v2, 0x3f666666    # 0.9f

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    iget v4, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-interface {v2, v3, v1}, Lcdb;->a(Landroid/view/View;F)V

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    const/4 v4, 0x0

    add-int/2addr v2, v0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v5, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v9}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v4, v0}, Lcdb;->c(Landroid/view/View;)Lcar;

    move-result-object v6

    if-nez v6, :cond_3

    invoke-virtual {p0, v9}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v4, v0}, Lcdb;->d(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_10

    move-object v4, v0

    :goto_1
    add-int/lit8 v0, p2, 0x1

    if-ge v0, p4, :cond_f

    invoke-virtual {p1, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v7, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v7, v0}, Lcdb;->c(Landroid/view/View;)Lcar;

    move-result-object v7

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v7, v0}, Lcdb;->d(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_f

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->c:Lcar;

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iput-object v6, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->c:Lcar;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    iget-object v7, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v1, v7, v6, v8}, Lcdb;->a(Landroid/view/View;Lcar;Landroid/content/Context;)V

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_3
    if-nez p2, :cond_e

    iget v4, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    add-int/2addr v1, v4

    move v4, v1

    :goto_4
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    move v1, v0

    :goto_5
    if-nez p2, :cond_7

    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    if-ne v4, v0, :cond_7

    invoke-virtual {p0, v9}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    :goto_6
    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->h:Z

    if-eqz v0, :cond_a

    if-nez p2, :cond_8

    move v0, v2

    :goto_7
    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v7

    if-ge v7, v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v0, v6}, Lcdb;->b(Landroid/view/View;)V

    :goto_8
    move v0, v3

    :goto_9
    if-ge v0, p3, :cond_a

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v7, v6}, Lcdb;->a(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_5
    const v1, -0x7fffffff

    goto :goto_3

    :cond_6
    const v0, 0x7fffffff

    move v1, v0

    goto :goto_5

    :cond_7
    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    goto :goto_6

    :cond_8
    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    invoke-interface {v0, v6}, Lcdb;->a(Landroid/view/View;)V

    goto :goto_8

    :cond_a
    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    iget v6, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->g:I

    sub-int/2addr v1, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    iget v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->d:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_c

    move v0, v3

    :goto_a
    if-eqz v0, :cond_d

    sget-object v0, Lcda;->b:Lcda;

    :goto_b
    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->a:Lcdb;

    iget-object v3, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->b:Landroid/view/View;

    invoke-interface {v1, v3, v0}, Lcdb;->a(Landroid/view/View;Lcda;)V

    iget v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->e:I

    if-ne v5, v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    if-eqz v0, :cond_0

    :cond_b
    iput-boolean v2, p0, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->f:Z

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/StickyHeaderView;->requestLayout()V

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_a

    :cond_d
    sget-object v0, Lcda;->a:Lcda;

    goto :goto_b

    :cond_e
    move v4, v1

    goto/16 :goto_4

    :cond_f
    move-object v0, v1

    goto/16 :goto_2

    :cond_10
    move-object v4, v1

    goto/16 :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
