.class public interface abstract Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Iterable;


# virtual methods
.method public abstract a()Lcom/google/android/gms/drive/database/SqlWhereClause;
.end method

.method public abstract a(Lcfz;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Z
.end method

.method public abstract b(Lcfz;)Lbvi;
.end method

.method public abstract b()Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract c()Lbzk;
.end method
