.class public final Lcom/google/android/gms/games/ui/widget/MosaicView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lbic;


# instance fields
.field private a:[Leek;

.field private b:I

.field private c:I

.field private d:Landroid/graphics/Matrix;

.field private e:I

.field private f:Lcom/google/android/gms/common/images/ImageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/MosaicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/widget/MosaicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->c:I

    const/16 v0, 0x9

    new-array v0, v0, [Leek;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->d:Landroid/graphics/Matrix;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->e:I

    return-void
.end method

.method private c()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v3, v2, Leek;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    iget-object v3, v2, Leek;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Leek;->a(Landroid/graphics/drawable/Drawable;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->c:I

    if-eqz v3, :cond_1

    iget-object v2, v2, Leek;->a:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lbij;->a:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, v2, Leek;->a:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    return-void
.end method

.method public final a(I)V
    .locals 2

    const/4 v0, 0x0

    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_0
    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->c:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->c()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->invalidate()V

    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 3

    const/4 v0, 0x0

    if-gtz p2, :cond_2

    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_3

    iget-object v2, v1, Leek;->b:Landroid/net/Uri;

    if-eqz v2, :cond_3

    iget-object v2, v1, Leek;->b:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v1, v1, Leek;->c:Z

    if-eqz v1, :cond_3

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-nez v0, :cond_4

    if-eqz p1, :cond_7

    :cond_4
    new-instance v1, Leek;

    invoke-direct {v1, v0, p1}, Leek;-><init>(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    iget v0, v1, Leek;->d:I

    if-eqz v0, :cond_5

    iget v0, v1, Leek;->e:I

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, v1, Leek;->b:Landroid/net/Uri;

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    aput-object v1, v0, v2

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->c()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->invalidate()V

    :cond_7
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->f:Lcom/google/android/gms/common/images/ImageManager;

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->f:Lcom/google/android/gms/common/images/ImageManager;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->f:Lcom/google/android/gms/common/images/ImageManager;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/images/ImageManager;->a(Lbic;Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Z)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    iget-object v3, v2, Leek;->b:Landroid/net/Uri;

    invoke-virtual {p1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, p2, p3}, Leek;->a(Landroid/graphics/drawable/Drawable;Z)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->c()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->b:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->invalidate()V

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    array-length v3, v3

    :goto_0
    if-ge v1, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    aget-object v4, v4, v1

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_22

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->e:I

    const/4 v1, 0x2

    if-ne v9, v1, :cond_1

    const/16 v1, 0x9

    :goto_1
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v10

    const/4 v1, 0x0

    move v8, v1

    :goto_2
    if-ge v8, v10, :cond_22

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->a:[Leek;

    aget-object v11, v1, v8

    iget-object v1, v11, Leek;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1d

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget v13, v11, Leek;->d:I

    iget v14, v11, Leek;->e:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->getWidth()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->getHeight()I

    move-result v4

    const/4 v3, 0x2

    if-ne v9, v3, :cond_7

    packed-switch v10, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 9 drawables to draw on SPLIT_MODE_WIDE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x4

    goto :goto_1

    :pswitch_0
    move v7, v5

    :goto_3
    const/4 v3, 0x2

    if-ne v9, v3, :cond_13

    packed-switch v10, :pswitch_data_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 9 drawables to draw on SPLIT_MODE_WIDE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    if-nez v8, :cond_2

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_2
    int-to-float v3, v5

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :pswitch_2
    if-nez v8, :cond_3

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_3
    int-to-float v3, v5

    const/high16 v6, 0x40c00000    # 6.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :pswitch_3
    if-nez v8, :cond_4

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_4
    const/4 v3, 0x1

    if-ne v8, v3, :cond_5

    int-to-float v3, v5

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_5
    int-to-float v3, v5

    const/high16 v6, 0x40c00000    # 6.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :pswitch_4
    if-nez v8, :cond_6

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_6
    int-to-float v3, v5

    const/high16 v6, 0x40c00000    # 6.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v7, v3

    goto :goto_3

    :cond_7
    packed-switch v10, :pswitch_data_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 4 drawables to draw on SPLIT_MODE_SQUARE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_5
    move v7, v5

    goto :goto_3

    :pswitch_6
    shr-int/lit8 v3, v5, 0x1

    move v7, v3

    goto :goto_3

    :pswitch_7
    move v6, v4

    :goto_4
    const/4 v3, 0x2

    if-ne v9, v3, :cond_1b

    packed-switch v10, :pswitch_data_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 9 drawables to draw on SPLIT_MODE_WIDE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_8
    if-nez v8, :cond_8

    move v6, v4

    goto :goto_4

    :cond_8
    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :pswitch_9
    if-nez v8, :cond_9

    move v6, v4

    goto :goto_4

    :cond_9
    int-to-float v3, v4

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v3, v6

    float-to-int v3, v3

    move v6, v3

    goto :goto_4

    :pswitch_a
    if-nez v8, :cond_a

    move v6, v4

    goto :goto_4

    :cond_a
    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :pswitch_b
    if-nez v8, :cond_b

    move v6, v4

    goto :goto_4

    :cond_b
    const/4 v3, 0x1

    if-ne v8, v3, :cond_c

    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :cond_c
    shr-int/lit8 v3, v4, 0x2

    move v6, v3

    goto :goto_4

    :pswitch_c
    if-nez v8, :cond_d

    move v6, v4

    goto :goto_4

    :cond_d
    const/4 v3, 0x1

    if-eq v8, v3, :cond_e

    const/4 v3, 0x2

    if-ne v8, v3, :cond_f

    :cond_e
    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :cond_f
    shr-int/lit8 v3, v4, 0x2

    move v6, v3

    goto :goto_4

    :pswitch_d
    if-nez v8, :cond_10

    move v6, v4

    goto :goto_4

    :cond_10
    const/4 v3, 0x1

    if-ne v8, v3, :cond_11

    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :cond_11
    shr-int/lit8 v3, v4, 0x2

    move v6, v3

    goto :goto_4

    :pswitch_e
    if-nez v8, :cond_12

    move v6, v4

    goto :goto_4

    :cond_12
    shr-int/lit8 v3, v4, 0x2

    move v6, v3

    goto :goto_4

    :cond_13
    packed-switch v10, :pswitch_data_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 4 drawables to draw on SPLIT_MODE_SQUARE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_f
    move v6, v4

    goto :goto_4

    :pswitch_10
    if-nez v8, :cond_14

    move v6, v4

    goto :goto_4

    :cond_14
    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :pswitch_11
    shr-int/lit8 v3, v4, 0x1

    move v6, v3

    goto :goto_4

    :pswitch_12
    const/4 v3, 0x0

    move v5, v3

    :goto_5
    const/4 v3, 0x2

    if-ne v9, v3, :cond_1e

    packed-switch v10, :pswitch_data_5

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 9 drawables to draw on SPLIT_MODE_WIDE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_13
    if-nez v8, :cond_15

    const/4 v3, 0x0

    move v5, v3

    goto :goto_5

    :cond_15
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_14
    packed-switch v8, :pswitch_data_6

    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_16

    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_15
    const/4 v3, 0x0

    move v5, v3

    goto :goto_5

    :cond_16
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_16
    packed-switch v8, :pswitch_data_7

    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_17

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_17
    const/4 v3, 0x0

    move v5, v3

    goto :goto_5

    :pswitch_18
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :cond_17
    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_19
    packed-switch v8, :pswitch_data_8

    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_18

    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_1a
    const/4 v3, 0x0

    move v5, v3

    goto :goto_5

    :cond_18
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto :goto_5

    :pswitch_1b
    packed-switch v8, :pswitch_data_9

    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_19

    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :pswitch_1c
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_5

    :pswitch_1d
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :pswitch_1e
    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :cond_19
    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :pswitch_1f
    packed-switch v8, :pswitch_data_a

    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_1a

    mul-int/lit8 v3, v5, 0x5

    int-to-float v3, v3

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :pswitch_20
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_5

    :cond_1a
    mul-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    move v5, v3

    goto/16 :goto_5

    :cond_1b
    packed-switch v10, :pswitch_data_b

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 4 drawables to draw on SPLIT_MODE_SQUARE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_21
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_5

    :pswitch_22
    packed-switch v8, :pswitch_data_c

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "position cannot be greater than numDrawables - 1"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_23
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_5

    :pswitch_24
    shr-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_5

    :pswitch_25
    rem-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_1c

    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_5

    :cond_1c
    shr-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_5

    :pswitch_26
    const/4 v3, 0x0

    move v4, v3

    :goto_6
    mul-int v3, v13, v6

    mul-int v15, v7, v14

    if-le v3, v15, :cond_21

    int-to-float v2, v6

    int-to-float v3, v14

    div-float v3, v2, v3

    int-to-float v2, v7

    int-to-float v13, v13

    mul-float/2addr v13, v3

    sub-float/2addr v2, v13

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v2, v13

    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->d:Landroid/graphics/Matrix;

    invoke-virtual {v13, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->d:Landroid/graphics/Matrix;

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v2, v13

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v1, v13

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    int-to-float v1, v5

    int-to-float v2, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v7, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->d:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v1, v11, Leek;->a:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1d
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_2

    :pswitch_27
    packed-switch v8, :pswitch_data_d

    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    :pswitch_28
    const/4 v3, 0x0

    move v4, v3

    goto :goto_6

    :pswitch_29
    packed-switch v8, :pswitch_data_e

    mul-int/lit8 v3, v4, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v3

    goto :goto_6

    :pswitch_2a
    const/4 v3, 0x0

    move v4, v3

    goto :goto_6

    :pswitch_2b
    int-to-float v3, v4

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v3

    goto :goto_6

    :pswitch_2c
    packed-switch v8, :pswitch_data_f

    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    :pswitch_2d
    const/4 v3, 0x0

    move v4, v3

    goto :goto_6

    :pswitch_2e
    packed-switch v8, :pswitch_data_10

    shr-int/lit8 v3, v4, 0x2

    mul-int/lit8 v3, v3, 0x3

    move v4, v3

    goto/16 :goto_6

    :pswitch_2f
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :pswitch_30
    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_6

    :pswitch_31
    packed-switch v8, :pswitch_data_11

    shr-int/lit8 v3, v4, 0x2

    mul-int/lit8 v3, v3, 0x3

    move v4, v3

    goto/16 :goto_6

    :pswitch_32
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :pswitch_33
    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_6

    :pswitch_34
    packed-switch v8, :pswitch_data_12

    shr-int/lit8 v3, v4, 0x2

    mul-int/lit8 v3, v3, 0x3

    move v4, v3

    goto/16 :goto_6

    :pswitch_35
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :pswitch_36
    shr-int/lit8 v3, v4, 0x2

    move v4, v3

    goto/16 :goto_6

    :pswitch_37
    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_6

    :pswitch_38
    packed-switch v8, :pswitch_data_13

    shr-int/lit8 v3, v4, 0x2

    mul-int/lit8 v3, v3, 0x3

    move v4, v3

    goto/16 :goto_6

    :pswitch_39
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :pswitch_3a
    shr-int/lit8 v3, v4, 0x2

    move v4, v3

    goto/16 :goto_6

    :pswitch_3b
    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_6

    :cond_1e
    packed-switch v10, :pswitch_data_14

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have more than 4 drawables to draw on SPLIT_MODE_SQUARE."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_3c
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :pswitch_3d
    if-eqz v8, :cond_1f

    const/4 v3, 0x1

    if-ne v8, v3, :cond_20

    :cond_1f
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_6

    :cond_20
    shr-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_6

    :cond_21
    int-to-float v1, v7

    int-to-float v3, v13

    div-float v3, v1, v3

    int-to-float v1, v6

    int-to-float v13, v14

    mul-float/2addr v13, v3

    sub-float/2addr v1, v13

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v1, v13

    goto/16 :goto_7

    :cond_22
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->c:I

    if-eqz v1, :cond_23

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/gms/games/ui/widget/MosaicView;->c:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_23
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_14
        :pswitch_16
        :pswitch_19
        :pswitch_1b
        :pswitch_1f
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_26
        :pswitch_26
        :pswitch_27
        :pswitch_29
        :pswitch_2c
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_38
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_15
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_17
        :pswitch_18
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_1a
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_20
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x1
        :pswitch_21
        :pswitch_22
        :pswitch_22
        :pswitch_25
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_23
        :pswitch_24
        :pswitch_24
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x0
        :pswitch_28
        :pswitch_28
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_2a
        :pswitch_2b
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x0
        :pswitch_2d
        :pswitch_2d
        :pswitch_2d
    .end packed-switch

    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_2f
        :pswitch_2f
        :pswitch_30
        :pswitch_30
    .end packed-switch

    :pswitch_data_11
    .packed-switch 0x0
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_33
        :pswitch_33
    .end packed-switch

    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_35
        :pswitch_35
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_37
    .end packed-switch

    :pswitch_data_13
    .packed-switch 0x0
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_3a
        :pswitch_3a
        :pswitch_3b
        :pswitch_3b
    .end packed-switch

    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_3c
        :pswitch_3d
        :pswitch_3d
    .end packed-switch
.end method

.method protected final onMeasure(II)V
    .locals 5

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-nez v3, :cond_0

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "width and height cannot be both WRAP_CONTENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v3, :cond_1

    move v1, v0

    :goto_0
    invoke-static {v0, p1}, Lcom/google/android/gms/games/ui/widget/MosaicView;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/gms/games/ui/widget/MosaicView;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/widget/MosaicView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    if-nez v4, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    move v1, v2

    goto :goto_0
.end method
