.class public final Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ListView;

.field private f:Ldwt;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v0, "matchesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    add-int/2addr v0, v1

    sget v2, Lxe;->b:I

    sget v1, Lxf;->S:I

    :goto_0
    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "invitationsButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->i:I

    sget v2, Lxe;->a:I

    sget v1, Lxf;->R:I

    goto :goto_0

    :cond_1
    const-string v0, "myTurnButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    sget v2, Lxe;->c:I

    sget v1, Lxf;->T:I

    goto :goto_0

    :cond_2
    const-string v0, "theirTurnButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    sget v2, Lxe;->d:I

    sget v1, Lxf;->V:I

    goto :goto_0

    :cond_3
    const-string v0, "MatchQuickAccess"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getToastMessage: unexpected tag \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Landroid/widget/TextView;III)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    sget v2, Lxa;->ab:I

    if-eq v0, v2, :cond_0

    if-lez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p1, v1, v1, p3, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    if-eqz v0, :cond_3

    sget v0, Lwx;->u:I

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move p3, p4

    goto :goto_1

    :cond_3
    sget v0, Lwx;->d:I

    goto :goto_2
.end method

.method private e()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lwy;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v4

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->h:I

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/widget/ListView;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    sget v0, Lxa;->ac:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const-string v1, "matchesButton"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lxa;->ab:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a:Landroid/widget/TextView;

    sget v0, Lxa;->al:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const-string v1, "myTurnButton"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lxa;->ak:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->b:Landroid/widget/TextView;

    sget v0, Lxa;->aZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const-string v1, "theirTurnButton"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lxa;->aY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->c:Landroid/widget/TextView;

    sget v0, Lxa;->U:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const-string v1, "invitationsButton"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lxa;->T:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->d:Landroid/widget/TextView;

    return-void
.end method

.method public final a(III)V
    .locals 4

    iput p1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->i:I

    iput p2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    iput p3, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    add-int/2addr v1, v2

    sget v2, Lwz;->u:I

    sget v3, Lwz;->v:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/TextView;III)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    sget v2, Lwz;->w:I

    sget v3, Lwz;->x:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/TextView;III)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->c:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    sget v2, Lwz;->y:I

    sget v3, Lwz;->z:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/TextView;III)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->i:I

    sget v2, Lwz;->s:I

    sget v3, Lwz;->t:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/TextView;III)V

    return-void
.end method

.method public final a(Landroid/widget/ListView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->l:I

    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0
.end method

.method public final a(Ldwt;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->f:Ldwt;

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->i:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    return v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_7

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    const-string v2, "matchesButton"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    add-int/2addr v2, v3

    :goto_0
    if-gtz v2, :cond_0

    const-string v2, "matchesButton"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->f:Ldwt;

    invoke-interface {v1, v0}, Ldwt;->d_(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v2, "myTurnButton"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->j:I

    goto :goto_0

    :cond_3
    const-string v2, "theirTurnButton"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->k:I

    goto :goto_0

    :cond_4
    const-string v2, "invitationsButton"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->i:I

    goto :goto_0

    :cond_5
    const-string v0, "MatchQuickAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_7
    const-string v0, "MatchQuickAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->g:I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->h:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->e()V

    :cond_0
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 5

    const/4 v2, 0x0

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "MatchQuickAccess"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onLongClick: unexpected tag \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'; View: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", id "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->l:I

    if-eq v1, p2, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->m:I

    iput p2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->l:I

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->o:I

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->o:I

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->m:I

    sub-int v0, v2, v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->g:I

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->h:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->setTranslationY(F)V

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->h:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->h:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->g:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->n:I

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->p:I

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
