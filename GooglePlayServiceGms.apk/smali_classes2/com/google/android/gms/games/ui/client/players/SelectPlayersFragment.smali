.class public final Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;
.super Ldxl;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Ldxc;
.implements Ldxd;
.implements Ldyn;
.implements Lje;


# instance fields
.field private Y:Landroid/os/Handler;

.field private Z:Ldyx;

.field private aa:Landroid/widget/FrameLayout;

.field private ab:Landroid/widget/LinearLayout;

.field private ac:Landroid/widget/ListView;

.field private ad:Ldyh;

.field private ae:Landroid/view/View;

.field private af:Landroid/widget/TextView;

.field private ag:Landroid/view/MenuItem;

.field private ah:Landroid/widget/ProgressBar;

.field private ai:Z

.field private aj:Landroid/view/LayoutInflater;

.field private c:Ldzc;

.field private d:Ldyw;

.field private e:Lcom/google/android/gms/games/Player;

.field private f:Leed;

.field private g:I

.field private h:Ljava/util/HashMap;

.field private i:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    const v0, 0x7f04007f    # com.google.android.gms.R.layout.games_select_players_fragment

    invoke-direct {p0, v0}, Ldxl;-><init>(I)V

    return-void
.end method

.method private T()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->d()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->notifyDataSetChanged()V

    return-void
.end method

.method private U()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0}, Leed;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v1}, Ldzc;->u()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->U()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersFrag"

    const-string v1, "doSearch: no room to add more players!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->S()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "SelectPlayersFrag"

    const-string v1, "GoogleApiClient not connected (yet); ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcte;->m:Lctw;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    invoke-virtual {v2}, Ldxm;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lctw;->a(Lbdu;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldyx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/Player;Z)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v2

    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v3, v0, v1, v2}, Ldyh;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->T()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v1, v0}, Ldyh;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v1, v0}, Ldyh;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->aa:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ab:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldxm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldyh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ac:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldxm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    return-object v0
.end method


# virtual methods
.method public final H_()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ae:Landroid/view/View;

    const v1, 0x7f0a012e    # com.google.android.gms.R.id.filter_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    const-string v2, "savedStateSelectedPlayers"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    const-string v1, "savedStateNumOfAutoMatchPlayers"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldyx;->b(ZZ)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    return-void
.end method

.method public final J()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    iget-object v0, v0, Leed;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->c()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    const-string v2, "savedStateSelectedPlayers"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    const-string v3, "savedStateNumOfAutoMatchPlayers"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;)V

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->M()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    return-void
.end method

.method public final K()Leed;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    return-object v0
.end method

.method public final L()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    return v0
.end method

.method public final M()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->U()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersFrag"

    const-string v1, "addAutoMatchPlayer: no room to add more players!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->T()V

    goto :goto_0
.end method

.method public final N()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    if-nez v0, :cond_1

    const-string v0, "SelectPlayersFrag"

    const-string v1, "removeAutoMatchPlayer: nobody to remove!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->T()V

    goto :goto_1
.end method

.method public final O()V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0}, Leed;->a()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v0}, Ldzc;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->P()I

    move-result v5

    if-ltz v5, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v0}, Ldzc;->u()I

    move-result v0

    if-ne v5, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v0}, Ldzc;->n()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v5}, Ldzc;->u()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    if-ne v5, v0, :cond_4

    const v5, 0x7f0b0227    # com.google.android.gms.R.string.games_select_players_null_state_fixed_format

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    invoke-virtual {v5, v0}, Ldxm;->a(Ljava/lang/CharSequence;)V

    :cond_0
    add-int/lit8 v0, v3, 0x1

    const v3, 0x7f0f000f    # com.google.android.gms.R.plurals.games_select_players_current_count_format

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v3, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->af:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v0}, Ldzc;->u()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    sub-int v3, v0, v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    iget-object v0, v4, Leed;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lt v3, v0, :cond_7

    move v0, v1

    :goto_2
    invoke-static {v0}, Lbiq;->a(Z)V

    iput v3, v4, Leed;->b:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->U()Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v3, v0}, Ldyx;->b(Z)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ag:Landroid/view/MenuItem;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ag:Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ag:Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    if-nez v0, :cond_8

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    if-lez v3, :cond_8

    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v2, v0, v1}, Ldyx;->c(ZZ)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Ldyw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Ldyw;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    invoke-interface {v0, v1, v2}, Ldyw;->a(Ljava/util/HashMap;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0}, Ldyx;->V()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0}, Ldyx;->X()V

    return-void

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    const v6, 0x7f0b0226    # com.google.android.gms.R.string.games_select_players_null_state_range_format

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    if-lez v5, :cond_6

    const v0, 0x7f0f0011    # com.google.android.gms.R.plurals.games_select_players_slots_remaining_format

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    const v0, 0x7f0b0228    # com.google.android.gms.R.string.games_select_players_no_slots_remaining

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v1, v2

    goto :goto_3
.end method

.method public final P()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0}, Leed;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v1}, Ldzc;->u()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method public final Q()Lcom/google/android/gms/games/GameEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->o()Lcom/google/android/gms/games/GameEntity;

    move-result-object v0

    return-object v0
.end method

.method public final R()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0}, Ldyx;->Z()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ldyh;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Ldxl;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Y:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->aj:Landroid/view/LayoutInflater;

    const v0, 0x7f0a019c    # com.google.android.gms.R.id.chips_list

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ac:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ac:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ac:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0a019e    # com.google.android.gms.R.id.select_players_list_container

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->aa:Landroid/widget/FrameLayout;

    new-instance v0, Ldyx;

    invoke-direct {v0}, Ldyx;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0}, Ldyx;->P()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0, p0}, Ldyx;->a(Lje;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a019f    # com.google.android.gms.R.id.select_players_list_fragment

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0, v1, v3}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    const v0, 0x7f0a01a0    # com.google.android.gms.R.id.games_select_players_none_found

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ab:Landroid/widget/LinearLayout;

    const v0, 0x7f0a019b    # com.google.android.gms.R.id.select_players_current_count_label

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->af:Landroid/widget/TextView;

    const v0, 0x7f0a01a1    # com.google.android.gms.R.id.games_select_players_start_over

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110008    # com.google.android.gms.R.bool.games_select_players_show_current_count_label

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->af:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    const v1, 0x7f04005a    # com.google.android.gms.R.layout.games_chips_grid_filter

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ae:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ae:Landroid/view/View;

    const v3, 0x7f0a012e    # com.google.android.gms.R.id.filter_text

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ldyr;

    invoke-direct {v3, p0}, Ldyr;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v3, Ldys;

    invoke-direct {v3, p0}, Ldys;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v3, Ldyt;

    invoke-direct {v3, p0, v1}, Ldyt;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/widget/TextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r()V

    return-object v2
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: unhandled request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Ldxl;->a(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    const-string v2, "player_search_results"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    const-string v0, "SelectPlayersFrag"

    const-string v1, "REQUEST_PLAYER_SEARCH: RESULT_OK, but empty result"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    if-ne p2, v2, :cond_0

    invoke-static {p3}, Lbex;->a(Landroid/content/Intent;)Lbey;

    move-result-object v2

    invoke-interface {v2}, Lbey;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v1, v0}, Ldyx;->a(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    const/4 v1, 0x0

    const v0, 0x7f120005    # com.google.android.gms.R.menu.games_client_select_players_menu

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0a0381    # com.google.android.gms.R.id.menu_search

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ag:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ag:Landroid/view/MenuItem;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    const v0, 0x7f0a037e    # com.google.android.gms.R.id.menu_progress_bar

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-static {v2}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->aj:Landroid/view/LayoutInflater;

    const v3, 0x7f04001a    # com.google.android.gms.R.layout.actionbar_indeterminate_progress

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0a007a    # com.google.android.gms.R.id.progress_bar

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ai:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-static {v2, v3}, Leu;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a(Lbdu;)V
    .locals 2

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, p1}, Lctw;->b(Lbdu;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersFrag"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyh;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_1

    const-string v0, "SelectPlayersFrag"

    const-string v1, "togglePlayer: don\'t have mCurrentPlayer yet"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "togglePlayer: ignoring current player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0, v1}, Leed;->b(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/Player;Z)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    iget-object v0, v0, Leed;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Z:Ldyx;

    invoke-virtual {v0, v1, v1}, Ldyx;->b(ZZ)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0, p1}, Ldyh;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    return-void
.end method

.method public final a_(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Ldxl;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->H_()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->V()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a0032 -> :sswitch_0    # com.google.android.gms.R.id.menu_refresh
        0x7f0a0381 -> :sswitch_1    # com.google.android.gms.R.id.menu_search
    .end sparse-switch
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ai:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Player;)V
    .locals 5

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->U()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersFrag"

    const-string v1, "addPlayer: no room to add more players!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0, v2}, Leed;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addPlayer: ignoring already-selected player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_3

    const-string v0, "SelectPlayersFrag"

    const-string v1, "addPlayer: don\'t have mCurrentPlayer yet"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addPlayer: ignoring current player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0, v2}, Leed;->b(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/Player;Z)V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ldxl;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    instance-of v0, v0, Ldzc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    check-cast v0, Ldzc;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    instance-of v0, v0, Ldyw;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a:Ldxm;

    check-cast v0, Ldyw;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Ldyw;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c:Ldzc;

    invoke-interface {v0}, Ldzc;->u()I

    move-result v0

    new-instance v1, Ldyh;

    invoke-direct {v1, p0}, Ldyh;-><init>(Ldxl;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ac:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    new-instance v2, Ldyu;

    invoke-direct {v2, p0}, Ldyu;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v2}, Ldyh;->a(Ldym;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ae:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Ldyh;->a(Landroid/view/View;I)V

    new-instance v1, Leed;

    invoke-direct {v1, v0}, Leed;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:Landroid/os/Bundle;

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->V()V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    if-nez v0, :cond_0

    const-string v0, "SelectPlayersFrag"

    const-string v1, "onRemoveAutoMatchPlayer: no auto-match players to remove!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ad:Ldyh;

    invoke-virtual {v0}, Ldyh;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ldxl;->e(Landroid/os/Bundle;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "savedStateSelectedPlayers"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "savedStateNumOfAutoMatchPlayers"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a019c    # com.google.android.gms.R.id.chips_list

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->T()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Leed;

    invoke-virtual {v0}, Leed;->a()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ae:Landroid/view/View;

    const v1, 0x7f0a012e    # com.google.android.gms.R.id.filter_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->Y:Landroid/os/Handler;

    new-instance v2, Ldyv;

    invoke-direct {v2, p0, v0}, Ldyv;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/widget/TextView;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onTouch: unexpected view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final w()V
    .locals 0

    invoke-super {p0}, Ldxl;->w()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    return-void
.end method

.method public final x_()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ai:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->ah:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method
