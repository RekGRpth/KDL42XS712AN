.class public final Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;
.super Ldvg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Ledt;


# instance fields
.field private Z:Ljava/lang/String;

.field private aa:Ldxm;

.field private ab:Leds;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldvg;-><init>()V

    return-void
.end method


# virtual methods
.method public final R()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->h:Ldfo;

    invoke-interface {v1, v0}, Ldfo;->a(Lbdu;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->ab:Leds;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientLeaderboardList"

    const-string v1, "onRetry(): client not connected yet..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f040088    # com.google.android.gms.R.layout.games_simple_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Leds;

    invoke-direct {v1, v0, p0}, Leds;-><init>(Landroid/view/View;Ledt;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->ab:Leds;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->ab:Leds;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Leds;->a(I)V

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 1

    sget-object v0, Lcte;->h:Ldfo;

    invoke-interface {v0, p1}, Ldfo;->a(Lbdu;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final synthetic a(Lbek;)V
    .locals 5

    check-cast p1, Ldfp;

    invoke-interface {p1}, Ldfp;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Ldfp;->g()Ldfc;

    move-result-object v1

    if-eqz v0, :cond_0

    const-string v2, "ClientLeaderboardList"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onLeaderboardsLoaded: got non-SUCCESS statusCode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", data = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {v1}, Ldfc;->b()V

    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->aa:Ldxm;

    invoke-virtual {v2, v0}, Ldxm;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ldfc;->b()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->i:Ldve;

    invoke-virtual {v2, v1}, Ldve;->a(Lbgo;)V

    invoke-virtual {v1}, Ldfc;->a()I

    move-result v2

    if-lez v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->ab:Leds;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Leds;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldfc;->b()V

    throw v0

    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->ab:Leds;

    const v3, 0x7f0b0250    # com.google.android.gms.R.string.games_leaderboards_generic_error

    const v4, 0x7f0b024e    # com.google.android.gms.R.string.games_leaderboards_empty

    invoke-virtual {v2, v0, v3, v4}, Leds;->a(III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected final b(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->Z:Ljava/lang/String;

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldvg;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldxm;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->aa:Ldxm;

    new-instance v0, Ldzm;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->aa:Ldxm;

    invoke-direct {v0, v1, p0}, Ldzm;-><init>(Ldvn;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->a(Ldve;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Ldfb;

    if-eqz v1, :cond_0

    check-cast v0, Ldfb;

    invoke-interface {v0}, Ldfb;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->J()Lbdu;

    move-result-object v1

    sget-object v2, Lcte;->h:Ldfo;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->Z:Ljava/lang/String;

    invoke-interface {v2, v1, v0, v3}, Ldfo;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardListFragment;->aa:Ldxm;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Ldxm;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "ClientLeaderboardList"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
