.class public Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Paint;

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:I

.field private g:I

.field private h:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->setWillNotDraw(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lwy;->q:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->a:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->b:Landroid/graphics/Paint;

    sget v2, Lwx;->r:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v1, Lwy;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->c:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->d:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->e:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->e:Landroid/graphics/Paint;

    sget v2, Lwx;->s:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->e:Landroid/graphics/Paint;

    sget v2, Lwy;->p:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget v1, Lwy;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->f:I

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->invalidate()V

    return-void
.end method

.method final a(IF)V
    .locals 0

    iput p1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->g:I

    iput p2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->invalidate()V

    return-void
.end method

.method final b(I)V
    .locals 1

    iput p1, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->g:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildCount()I

    move-result v7

    if-lez v7, :cond_1

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    cmpl-float v2, v2, v8

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->g:I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->g:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    int-to-float v3, v3

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    sub-float v4, v5, v4

    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    int-to-float v2, v2

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->h:F

    sub-float v3, v5, v3

    int-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    :cond_0
    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->c:I

    sub-int v2, v6, v2

    int-to-float v2, v2

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->a:I

    sub-int v0, v6, v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v0, 0x1

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_2

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v3, v1

    sub-int v2, v3, v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->f:I

    div-int/lit8 v2, v2, 0x2

    sub-int v4, v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v4

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->f:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_2
    return-void
.end method
