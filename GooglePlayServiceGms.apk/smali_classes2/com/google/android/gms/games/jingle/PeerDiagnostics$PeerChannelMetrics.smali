.class public Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final mConnectionEstablishmentLatencyMs:I

.field private final mConnectionStartTimestampMs:I

.field private final mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

.field private final mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

.field private final mNumMessagesLost:I

.field private final mNumMessagesReceived:I

.field private final mNumMessagesSent:I

.field private final mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesSent:I

    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesReceived:I

    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesLost:I

    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionStartTimestampMs:I

    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionEstablishmentLatencyMs:I

    return-void
.end method


# virtual methods
.method public getConnectionEstablishmentLatencyMs()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionEstablishmentLatencyMs:I

    return v0
.end method

.method public getConnectionStartTimestampMs()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionStartTimestampMs:I

    return v0
.end method

.method public getNumBytesReceived()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method

.method public getNumBytesSent()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method

.method public getNumMessagesLost()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesLost:I

    return v0
.end method

.method public getNumMessagesReceived()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesReceived:I

    return v0
.end method

.method public getNumMessagesSent()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesSent:I

    return v0
.end method

.method public getRoundTripLatencyMs()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method
