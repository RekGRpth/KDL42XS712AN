.class public Lcom/google/android/gms/games/jingle/PeerDiagnostics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

.field private final mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    return-void
.end method


# virtual methods
.method public getReliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    return-object v0
.end method

.method public getUnreliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    return-object v0
.end method
