.class public Lcom/google/android/gms/games/jingle/Libjingle;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

.field public static final AVAILABLE:I = 0x1

.field public static final CALL_FLAG_AUDIO_ONLY:I = 0x1

.field public static final CALL_FLAG_MULTI_USER:I = 0x2

.field public static FAILURE_OPS_ID:I = 0x0

.field public static final HAS_CAMERA_V1:I = 0x8

.field public static final HAS_VIDEO_V1:I = 0x4

.field public static final HAS_VOICE_V1:I = 0x2

.field public static final STR1_KEY:Ljava/lang/String; = "str1"

.field public static final STR2_KEY:Ljava/lang/String; = "str2"

.field public static final STR3_KEY:Ljava/lang/String; = "str3"

.field public static final STR4_KEY:Ljava/lang/String; = "str4"

.field private static final TAG:Ljava/lang/String; = "games_rtmp:Libjingle"

.field public static final UNAVAILABLE:I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialized:Z

.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "games_rtmp_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeInit()V

    invoke-static {v7}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetLoggingLevel(I)V

    const/4 v0, 0x6

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "gtalk_vc_xmpp_hostname"

    aput-object v2, v1, v4

    const-string v2, "XMPP_HOSTNAME"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "gtalk_vc_xmpp_port"

    aput-object v2, v1, v4

    const-string v2, "XMPP_PORT"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "games.notification_channel"

    aput-object v2, v1, v4

    const-string v2, "GAMES_NOTIFICATION_CHANNEL"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.notification_jid"

    aput-object v3, v2, v4

    const-string v3, "GAMES_NOTIFICATION_JID"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.buzzbot_channel"

    aput-object v3, v2, v4

    const-string v3, "GAMES_BUZZBOT_CHANNEL"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "games.buzzbot_jid"

    aput-object v2, v1, v4

    const-string v2, "GAMES_BUZZBOT_JID"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/gms/games/jingle/Libjingle;->ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/games/jingle/Libjingle;->FAILURE_OPS_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private static dispatchNativeEvent(Ljava/lang/Object;IIILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3, p8}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "str1"

    check-cast p4, Ljava/lang/String;

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "str2"

    check-cast p5, Ljava/lang/String;

    invoke-virtual {v2, v3, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "str3"

    check-cast p6, Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "str4"

    check-cast p7, Ljava/lang/String;

    invoke-virtual {v2, v3, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    const-string v0, "games_rtmp:Libjingle"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static logTagToInt(Ljava/lang/String;)I
    .locals 5

    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-static {p0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    goto :goto_0
.end method

.method private final native nativeAcceptCall(Ljava/lang/String;)V
.end method

.method private final native nativeCall(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method private final native nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method private final native nativeCreateSocketConnection(Ljava/lang/String;)I
.end method

.method private final native nativeDeclineCall(Ljava/lang/String;)V
.end method

.method private final native nativeDisconnectAndSignout(Ljava/lang/String;)V
.end method

.method private final native nativeEndCall(Ljava/lang/String;)V
.end method

.method private final native nativeFinalize()V
.end method

.method private final native nativeGetDebugString()Ljava/lang/String;
.end method

.method private final native nativeGetPeerDiagnosticMetrics(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method private static native nativeInit()V
.end method

.method private final native nativeListenForBuzzNotifications()V
.end method

.method private final native nativePrepareEngine(Ljava/lang/String;)V
.end method

.method private final native nativeProcessSessionStanza(Ljava/lang/String;)V
.end method

.method private final native nativeProcessSessionStanzaResponse(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeRegisterWithBuzzbot(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeRelease()V
.end method

.method private final native nativeReleaseEngine()V
.end method

.method private final native nativeSendData(Ljava/lang/String;[B)V
.end method

.method private final native nativeSendDirectedPresence(Ljava/lang/String;)I
.end method

.method private final native nativeSendIbbData(Ljava/lang/String;[B)I
.end method

.method private final native nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeSetJingleInfoStanza(Ljava/lang/String;)V
.end method

.method private static final native nativeSetLoggingLevel(I)V
.end method

.method private final native nativeSetPeerCapabilities(Ljava/lang/String;I)V
.end method

.method private final native nativeSetup(Ljava/lang/Object;ZI)V
.end method

.method private final native nativeSubscribeToBuzzChannels(Z)V
.end method

.method private final native nativeTerminateAllCalls()V
.end method

.method private final native nativeUnregisterWithBuzzbot()V
.end method

.method private final native nativeUpdateRemoteJidForSession(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static onDataChannelConnectionResult(Ljava/lang/Object;Ljava/lang/String;ZI)V
    .locals 4

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x12d

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v3, v1, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static onDirectedPresenceReceipt(Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 5

    const/4 v2, 0x0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x191

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static onNotifSubscriptionResult(Ljava/lang/Object;Z)V
    .locals 6

    const/4 v2, 0x0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc9

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v1, v2, v5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private prepareEngine(Ljava/lang/String;)V
    .locals 1

    const-string v0, "prepare engine"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativePrepareEngine(Ljava/lang/String;)V

    return-void
.end method

.method private static receiveBuzzNotif(Ljava/lang/Object;[B[B)V
    .locals 7

    const/4 v6, 0x0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p2}, Ljava/lang/String;-><init>([B)V

    const-string v3, "games_rtmp:Libjingle"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received buzz notification:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget v3, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xca

    invoke-virtual {v3, v4, v6, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v3, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(I)V

    const-string v4, "str1"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static receiveDataChannelData(Ljava/lang/Object;Ljava/lang/String;[B)V
    .locals 4

    const/4 v3, 0x0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x12e

    invoke-virtual {v1, v2, v3, v3, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "str1"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static receiveIbbData(Ljava/lang/Object;Ljava/lang/String;[B)V
    .locals 6

    const/4 v5, 0x0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p2}, Ljava/lang/String;-><init>([B)V

    const-string v2, "games_rtmp:Libjingle"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received from:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " data:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2, v5, v5, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "str1"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static receiveIbbSendResult(Ljava/lang/Object;ILjava/lang/String;Z)V
    .locals 4

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    const-string v1, "games_rtmp:Libjingle"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received result, peerJid:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " success:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x66

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v3, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received null Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendIbbData(Ljava/lang/String;[B)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    if-nez v0, :cond_0

    const-string v0, "release: already released"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    const-string v0, "Release: call nativeRelease"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeRelease()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeAcceptCall(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    if-eqz v0, :cond_0

    const-string v0, "init: already initialized"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v7, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetup(Ljava/lang/Object;ZI)V

    iget-object v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/jingle/Libjingle;->ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    aget-object v6, v5, v1

    invoke-static {v2, v6}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    aget-object v5, v5, v7

    invoke-direct {p0, v5, v6}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/games/jingle/Libjingle;->logTagToInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetLoggingLevel(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeCall(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSubscribeToBuzzChannels(Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeCreateSocketConnection(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeUnregisterWithBuzzbot()V

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetPeerCapabilities(Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;[B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendData(Ljava/lang/String;[B)V

    return-void
.end method

.method public final c(Ljava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendDirectedPresence(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeListenForBuzzNotifications()V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeRegisterWithBuzzbot(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeGetDebugString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeDisconnectAndSignout(Ljava/lang/String;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeEndCall(Ljava/lang/String;)V

    return-void
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;
    .locals 1

    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeGetPeerDiagnosticMetrics(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeFinalize()V

    return-void
.end method
