.class public final Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:[B

.field private final d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leom;

    invoke-direct {v0}, Leom;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I[B[B[B)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p2

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Encrypted bytes must not be empty."

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    array-length v0, p3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "IV bytes must not be empty."

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    array-length v0, p4

    if-lez v0, :cond_2

    :goto_2
    const-string v0, "MAC bytes must not be empty."

    invoke-static {v1, v0}, Lbkm;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->a:I

    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->b:[B

    iput-object p3, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->c:[B

    iput-object p4, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->d:[B

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public constructor <init>([B[B[B)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;-><init>(I[B[B[B)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->a:I

    return v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->b:[B

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->c:[B

    return-object v0
.end method

.method public final d()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->d:[B

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Leom;->a(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;Landroid/os/Parcel;)V

    return-void
.end method
