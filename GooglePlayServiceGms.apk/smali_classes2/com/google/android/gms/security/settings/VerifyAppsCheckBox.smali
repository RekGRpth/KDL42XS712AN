.class public Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Lbfy;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/CheckBox;

.field private e:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "verify_apps_learn_more_url"

    const-string v1, "https://support.google.com/accounts/bin/answer.py?answer=2812853"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->a:Lbfy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v0, p1, Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    :try_start_0
    const-string v0, "android.provider.Settings$Global"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v5, "putInt"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/ContentResolver;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v0, 0x3

    new-array v7, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    aput-object v8, v7, v0

    const/4 v0, 0x1

    const-string v8, "package_verifier_enable"

    aput-object v8, v7, v0

    const/4 v8, 0x2

    if-eqz v4, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "package_verifier_enable"

    if-eqz v4, :cond_3

    :goto_3
    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->c:Landroid/widget/TextView;

    const v0, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    const v0, 0x7f0a02de    # com.google.android.gms.R.id.caption

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->e:Landroid/widget/TextView;

    invoke-virtual {p0, p0}, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    const v1, 0x7f0b01a8    # com.google.android.gms.R.string.verify_applications_summary

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->a:Lbfy;

    invoke-virtual {v3}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->e:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->e:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    invoke-static {v1}, Lgqm;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->d:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/security/settings/VerifyAppsCheckBox;->b:Landroid/content/Context;

    invoke-static {v1}, Lgqm;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method
