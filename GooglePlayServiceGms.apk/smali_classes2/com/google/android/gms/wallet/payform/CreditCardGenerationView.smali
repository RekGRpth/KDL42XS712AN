.class public Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;


# instance fields
.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ViewFlipper;

.field private h:Ljava/util/Random;

.field private i:Z

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "instanceState"

    sput-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a:Ljava/lang/String;

    const-string v0, "shownInitialBlankCard"

    sput-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b:Ljava/lang/String;

    const-string v0, "finalLastFourDigits"

    sput-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->c:Ljava/lang/String;

    const-string v0, "currentCardNumber"

    sput-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    const-wide/16 v3, 0x7d0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->h:Ljava/util/Random;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040154    # com.google.android.gms.R.layout.wallet_view_credit_card_generation

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0331    # com.google.android.gms.R.id.card_holder_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->e:Landroid/widget/TextView;

    const v0, 0x7f0a036a    # com.google.android.gms.R.id.credit_card_number

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->f:Landroid/widget/TextView;

    const v0, 0x7f0a0369    # com.google.android.gms.R.id.view_flipper

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->g:Landroid/widget/ViewFlipper;

    const/high16 v0, 0x10a0000    # android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const v1, 0x10a0001    # android.R.anim.fade_out

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v0}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XXXX XXXX XXXX "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->i:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->i:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b()V

    const-string v0, "XXXX"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b()V

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_3

    check-cast p1, Landroid/os/Bundle;

    sget-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    sget-object v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->i:Z

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x58

    if-ne v1, v2, :cond_2

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, 0x4b

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "XXXX"

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a:Ljava/lang/String;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public run()V
    .locals 8

    const/4 v7, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->f:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x13

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_1

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->h:Ljava/util/Random;

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-wide/16 v0, 0x4b

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method
