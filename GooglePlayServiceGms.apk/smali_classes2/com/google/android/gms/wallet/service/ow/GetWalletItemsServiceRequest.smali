.class public Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Ljba;

.field private c:[B

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhez;

    invoke-direct {v0}, Lhez;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Ljba;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Ljba;

    iput-boolean p3, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[BZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    iput-boolean p3, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;[BZB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;-><init>(Landroid/accounts/Account;[BZ)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Ljba;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Ljba;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    const-class v1, Ljba;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljba;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Ljba;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Ljba;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Ljba;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
