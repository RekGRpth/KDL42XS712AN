.class public Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lipf;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhcr;

    invoke-direct {v0}, Lhcr;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lipf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->b:Lipf;

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->c:[B

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;[BB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;-><init>(Landroid/accounts/Account;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lipf;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->b:Lipf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->c:[B

    const-class v1, Lipf;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipf;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->b:Lipf;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->b:Lipf;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->b:Lipf;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
