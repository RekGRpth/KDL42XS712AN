.class public Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lipk;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhdw;

    invoke-direct {v0}, Lhdw;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lipk;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b:Lipk;

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c:[B

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;[BB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;-><init>(Landroid/accounts/Account;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lipk;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b:Lipk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c:[B

    const-class v1, Lipk;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipk;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b:Lipk;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b:Lipk;

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b:Lipk;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c:[B

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->c()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
