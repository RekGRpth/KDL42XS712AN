.class public Lcom/google/android/gms/wallet/common/ui/AccountSelector;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field a:Landroid/widget/Spinner;

.field b:Landroid/widget/TextView;

.field c:Lguw;

.field private d:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040150    # com.google.android.gms.R.layout.wallet_view_account_selector

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0357    # com.google.android.gms.R.id.email_address

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    const v0, 0x7f0a00c7    # com.google.android.gms.R.id.account_spinner

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method private b()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lguv;

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Lguv;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_4

    invoke-virtual {v0, v2}, Lguv;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lguu;

    iget-object v1, v1, Lguu;->a:Landroid/accounts/Account;

    invoke-static {v1, v4}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    const/4 v2, -0x1

    goto :goto_2
.end method

.method private c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "account_selection"

    const-string v2, "account_selected"

    const-string v3, "account_spinner"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lgsl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Lguw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Lguw;

    invoke-interface {v0, p1}, Lguw;->a(Landroid/accounts/Account;)V

    :cond_1
    return-void
.end method

.method public final a(Lguw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Lguw;

    return-void
.end method

.method public final a([Lguu;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    array-length v0, p1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    new-instance v3, Lguv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lguv;-><init>(Landroid/content/Context;[Lguu;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b()V

    array-length v0, p1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Lguw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Lguw;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lguw;->a(Landroid/accounts/Account;)V

    :cond_0
    return-void

    :cond_1
    array-length v0, p1

    if-ne v0, v1, :cond_2

    aget-object v0, p1, v2

    iget-object v0, v0, Lguu;->a:Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguu;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lguu;->a:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/os/Bundle;

    const-string v0, "instanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "currentAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "instanceState"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "currentAccount"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
