.class public final Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgwv;
.implements Lgyq;
.implements Lsk;


# static fields
.field static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final ad:Landroid/util/SparseBooleanArray;

.field private static final ae:Landroid/util/SparseBooleanArray;

.field private static final af:Landroid/util/SparseBooleanArray;

.field private static final ag:Landroid/util/SparseBooleanArray;

.field private static final ah:Ljava/util/Comparator;


# instance fields
.field Y:Lorg/json/JSONObject;

.field Z:Ljava/lang/String;

.field public aa:Ljava/util/ArrayList;

.field ab:Lsf;

.field ac:Ljava/lang/String;

.field private ai:Landroid/view/View;

.field private aj:I

.field private ak:Z

.field private al:Z

.field private am:Z

.field private final an:Ljava/util/HashSet;

.field private ao:Ljava/util/ArrayList;

.field private ap:I

.field private aq:Z

.field private ar:Lixo;

.field private as:Lgxt;

.field private at:Lgza;

.field b:Lgxw;

.field c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

.field d:Landroid/view/ViewGroup;

.field e:Landroid/widget/ProgressBar;

.field f:Lgwr;

.field g:[I

.field h:I

.field i:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x43

    const/16 v5, 0x32

    const/16 v4, 0x31

    const/4 v1, 0x0

    const/4 v3, 0x1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Landroid/util/SparseBooleanArray;

    const/16 v2, 0x9

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x53

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x52

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v6, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4e

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v4, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v5, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x44

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x58

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    new-instance v0, Landroid/util/SparseBooleanArray;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ae:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4f

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ae:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x50

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ae:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4e

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    new-instance v0, Landroid/util/SparseBooleanArray;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v4, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v5, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x33

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v6, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x53

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x58

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    new-instance v0, Landroid/util/SparseBooleanArray;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ag:Landroid/util/SparseBooleanArray;

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    sget-object v4, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ag:Landroid/util/SparseBooleanArray;

    sget-object v5, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    sget-object v4, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ag:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v3, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lgve;

    invoke-direct {v0}, Lgve;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ah:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ak:Z

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->am:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->an:Ljava/util/HashSet;

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:I

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    return-void
.end method

.method private J()V
    .locals 4

    new-instance v0, Lgtu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-string v2, "data"

    new-instance v3, Lgvf;

    invoke-direct {v3, p0}, Lgvf;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-direct {v0, v1, v2, p0, v3}, Lgtu;-><init>(Landroid/content/Context;Ljava/lang/String;Lsk;Lsj;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->U()Lsf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lsf;->a(Lsc;)Lsc;

    return-void
.end method

.method private K()Landroid/util/SparseArray;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v3}, Landroid/util/SparseArray;-><init>(I)V

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {v1, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eqz v0, :cond_2

    const/16 v0, 0x52

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-static {v2}, Lhgq;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private L()Z
    .locals 5

    const/16 v4, 0x2b3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    array-length v2, v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->M()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    invoke-static {v2, v3}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v2}, Lbpr;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhgq;->b(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    invoke-static {v3, v2}, Lboz;->a([II)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    invoke-static {v2, v4}, Lboz;->a([II)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    aget v1, v2, v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eqz v2, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->M()V

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    if-eqz v2, :cond_8

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method private M()V
    .locals 4

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-nez v0, :cond_0

    const-string v0, "AddressEntryFragment"

    const-string v1, "Resetting selected country"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->L()Z

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-interface {v0, v1}, Lgxw;->d_(I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v0}, Lgty;->b(Lorg/json/JSONObject;)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lorg/json/JSONObject;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->a(IZ)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->T()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private N()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v2, v3}, Lgty;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v2, "lfmt"

    invoke-static {v1, v2}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v2, "fmt"

    invoke-static {v1, v2}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v1}, Lgty;->b(Ljava/lang/String;)[C

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ak:Z

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-static {v1}, Lgty;->b(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    array-length v2, v5

    new-array v7, v2, [C

    const/4 v3, 0x0

    array-length v6, v5

    const/4 v2, 0x0

    move v4, v2

    move v2, v3

    :goto_1
    if-ge v4, v6, :cond_6

    aget-char v8, v5, v4

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ad:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->am:Z

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ae:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    if-eqz v1, :cond_3

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    add-int/lit8 v3, v2, 0x1

    aput-char v8, v7, v2

    move v2, v3

    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    new-array v6, v2, [C

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_7

    aget-char v3, v7, v1

    aput-char v3, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v2, "require"

    invoke-static {v1, v2}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    move v10, v1

    :goto_3
    array-length v1, v6

    if-ge v10, v1, :cond_1a

    aget-char v5, v6, v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->an:Ljava/util/HashSet;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040163    # com.google.android.gms.R.layout.wallet_view_text

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_4
    invoke-static {v5}, Lgty;->a(C)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v1, 0x0

    aput-char v1, v6, v10

    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v5, v1}, Lgty;->a(CLorg/json/JSONObject;)Z

    move-result v12

    const/16 v1, 0x53

    if-ne v5, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v1, v2}, Lgty;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v13

    const/4 v2, 0x0

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v2, "sub_lnames"

    invoke-static {v1, v2}, Lgty;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :cond_9
    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v2, "sub_keys"

    invoke-static {v1, v2}, Lgty;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :cond_a
    if-eqz v2, :cond_b

    array-length v1, v2

    if-nez v1, :cond_c

    :cond_b
    const/4 v1, 0x0

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_15

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040155    # com.google.android.gms.R.layout.wallet_view_default_spinner

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormSpinner;

    invoke-virtual {v1, v12}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->a(Z)V

    new-instance v3, Lgvm;

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v4, v2, v8}, Lgvm;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    const v2, 0x7f040140    # com.google.android.gms.R.layout.wallet_row_admin_spinner_dropdown

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v2, Lgvj;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lgvj;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto/16 :goto_4

    :cond_c
    if-eqz v13, :cond_11

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_d

    array-length v3, v1

    array-length v4, v2

    if-eq v3, v4, :cond_e

    :cond_d
    move-object v1, v2

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v4, "sub_zips"

    invoke-static {v3, v4}, Lgty;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    array-length v4, v3

    array-length v8, v2

    if-eq v4, v8, :cond_10

    :cond_f
    const/4 v3, 0x0

    :cond_10
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_7
    array-length v8, v2

    if-ge v4, v8, :cond_13

    new-instance v14, Lgvn;

    aget-object v15, v2, v4

    aget-object v16, v1, v4

    if-eqz v3, :cond_12

    aget-object v8, v3, v4

    :goto_8
    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v8}, Lgvn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v3, "sub_names"

    invoke-static {v1, v3}, Lgty;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_12
    const/4 v8, 0x0

    goto :goto_8

    :cond_13
    if-eqz v13, :cond_14

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ah:Ljava/util/Comparator;

    invoke-static {v9, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_14
    move-object v2, v9

    goto/16 :goto_5

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_16
    const/4 v1, 0x0

    :goto_9
    if-eqz v1, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040159    # com.google.android.gms.R.layout.wallet_view_form_edit_text

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lgvy;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Q()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    invoke-direct/range {v1 .. v8}, Lgvy;-><init>(Landroid/content/Context;ILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v9, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    new-instance v2, Lgvk;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v9, v1}, Lgvk;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgvy;)V

    invoke-virtual {v9, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_a
    invoke-virtual {v9, v12}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v9}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setSingleLine()V

    const/4 v1, 0x1

    sparse-switch v5, :sswitch_data_0

    :goto_b
    invoke-virtual {v9, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v1, v9

    goto/16 :goto_4

    :cond_17
    sparse-switch v5, :sswitch_data_1

    const/4 v1, 0x1

    goto :goto_9

    :sswitch_0
    const/4 v1, 0x0

    goto :goto_9

    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040159    # com.google.android.gms.R.layout.wallet_view_form_edit_text

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    move-object v9, v1

    goto :goto_a

    :sswitch_1
    const/16 v1, 0x2061

    const v2, 0x7f0b018c    # com.google.android.gms.R.string.wallet_address_field_recipient_error_message

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgzm;

    const-string v4, ".*\\S+\\s+\\S+.*"

    invoke-direct {v3, v2, v4}, Lgzm;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    goto :goto_b

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v1}, Lgty;->d(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x3

    goto :goto_b

    :cond_19
    const/16 v1, 0x1001

    goto :goto_b

    :sswitch_3
    const/16 v1, 0x2071

    goto :goto_b

    :sswitch_4
    const/16 v1, 0x2001

    goto :goto_b

    :sswitch_5
    const/16 v1, 0x2001

    goto :goto_b

    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->P()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_3
        0x32 -> :sswitch_3
        0x33 -> :sswitch_3
        0x41 -> :sswitch_3
        0x43 -> :sswitch_5
        0x4e -> :sswitch_1
        0x53 -> :sswitch_4
        0x5a -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x58 -> :sswitch_0
    .end sparse-switch
.end method

.method private O()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1, v0}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x5a

    const/4 v2, 0x0

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lgyo;

    if-nez v1, :cond_2

    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    :cond_1
    :goto_0
    return-void

    :cond_2
    check-cast v0, Lgyp;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    invoke-interface {v0, v1}, Lgyp;->c(Lgzo;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    :cond_3
    new-instance v1, Lgyx;

    invoke-direct {v1}, Lgyx;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    const v1, 0x7f0b0196    # com.google.android.gms.R.string.wallet_error_address_field_invalid

    new-array v3, v8, [Ljava/lang/Object;

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v1}, Lgty;->c(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    new-instance v5, Lgzi;

    invoke-direct {v5, v3, v1}, Lgzi;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v4, v5}, Lgza;->a(Lgzo;)V

    :cond_4
    const/16 v1, 0x53

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_6

    instance-of v4, v1, Landroid/widget/Spinner;

    if-eqz v4, :cond_6

    check-cast v1, Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    instance-of v4, v1, Lgvn;

    if-eqz v4, :cond_6

    check-cast v1, Lgvn;

    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, v1, Lgvn;->b:Ljava/lang/String;

    invoke-static {v1}, Lgty;->c(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    new-instance v5, Lgzi;

    invoke-direct {v5, v3, v1}, Lgzi;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v4, v5}, Lgza;->a(Lgzo;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    invoke-virtual {v1}, Lgza;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    goto :goto_0

    :cond_6
    move-object v1, v2

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v6, v1}, Lgty;->a(CLorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Lgzh;

    const/4 v2, 0x2

    new-array v2, v2, [Lgzo;

    new-instance v4, Lgzf;

    invoke-direct {v4}, Lgzf;-><init>()V

    aput-object v4, v2, v7

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    aput-object v4, v2, v8

    invoke-direct {v1, v3, v2}, Lgzh;-><init>(Ljava/lang/CharSequence;[Lgzo;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->at:Lgza;

    invoke-interface {v0, v1}, Lgyp;->a(Lgzo;)V

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_a

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_9
    invoke-interface {v0}, Lgyp;->R_()Z

    goto/16 :goto_0

    :cond_a
    invoke-interface {v0}, Lgyp;->R_()Z

    goto/16 :goto_0
.end method

.method private Q()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v0, v1}, Lgty;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v1, "lang"

    invoke-static {v0, v1}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private R()Z
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private S()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private T()Ljava/util/ArrayList;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-object v0
.end method

.method private U()Lsf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ab:Lsf;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ab:Lsf;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ab:Lsf;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "params"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lixo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lixo;)Lixo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    return-object p1
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8

    new-instance v0, Lguc;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->U()Lsf;

    move-result-object v2

    new-instance v6, Lgvg;

    invoke-direct {v6, p0, p4}, Lgvg;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Ljava/util/ArrayList;)V

    new-instance v7, Lgvh;

    invoke-direct {v7, p0}, Lgvh;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v7}, Lguc;-><init>(Landroid/content/Context;Lsf;ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    invoke-virtual {v0}, Lguc;->a()V

    return-void
.end method

.method private a(Landroid/util/SparseArray;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x1

    const/4 v6, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    instance-of v0, p0, Landroid/widget/Spinner;

    if-eqz v0, :cond_7

    check-cast p0, Landroid/widget/Spinner;

    if-nez p1, :cond_4

    invoke-static {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v7

    move v5, v6

    move v3, v6

    :goto_1
    if-ge v5, v7, :cond_9

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v1, v2, Lgvr;

    if-eqz v1, :cond_5

    move-object v1, v2

    check-cast v1, Lgvr;

    invoke-interface {v1}, Lgvr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v4

    :goto_2
    if-eqz v1, :cond_6

    invoke-static {p0, v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    move v0, v1

    :goto_3
    if-nez v0, :cond_1

    invoke-static {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v4

    goto :goto_2

    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    goto :goto_1

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown input type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move v1, v3

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3
.end method

.method private static a(Landroid/widget/Spinner;I)V
    .locals 1

    instance-of v0, p0, Lgwb;

    if-eqz v0, :cond_0

    check-cast p0, Lgwb;

    invoke-virtual {p0, p1}, Lgwb;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {p1}, Lgwr;->d(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AddressEntryFragment.NetworkErrorDialog."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k()Lu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_1

    const/16 v0, 0x82

    invoke-virtual {p1, v0}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->O()Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;Lixo;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    move v5, v3

    move v0, v3

    :goto_0
    if-ge v5, v7, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v0, :cond_b

    if-ne v1, p1, :cond_b

    move v4, v2

    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {p2, v0}, Lgvs;->a(Lixo;C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v0, v9}, Lgty;->a(CLorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {v1, v8}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    instance-of v0, v1, Lgyo;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    if-nez v8, :cond_3

    :cond_2
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v4

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->P()V

    const/16 v0, 0x5a

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lgyo;

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->O()Z

    :cond_6
    :goto_4
    return-void

    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eq v0, v1, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ge v4, v0, :cond_5

    invoke-virtual {v6, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_6

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_4

    :cond_b
    move v4, v0

    goto/16 :goto_1
.end method

.method private a([I)V
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received null country list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lgty;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    array-length v1, v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No countries available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->an:Ljava/util/HashSet;

    const/16 v3, 0x52

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    if-ne v1, v0, :cond_4

    :cond_3
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    invoke-interface {v0, v1}, Lgxw;->a([I)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->L()Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    new-instance v1, Lgvi;

    invoke-direct {v1, p0}, Lgvi;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-interface {v0, v1}, Lgxw;->a(Lgxx;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->L()Z

    goto :goto_0
.end method

.method private b(C)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x4e

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v0, p1, v1}, Lgty;->a(Landroid/content/Context;CLorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/view/View;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    instance-of v0, p0, Landroid/widget/Spinner;

    if-eqz v0, :cond_4

    check-cast p0, Landroid/widget/Spinner;

    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lgvr;

    if-eqz v1, :cond_2

    check-cast v0, Lgvr;

    invoke-interface {v0}, Lgvr;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown input type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lixo;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {p1, v0}, Lgvs;->a(Lixo;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private c(C)Landroid/view/View;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v0, p1, :cond_1

    move-object v0, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private f(Z)V
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    const v1, 0x7f0a0152    # com.google.android.gms.R.id.container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040162    # com.google.android.gms.R.layout.wallet_view_region_code_text

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    :goto_0
    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_1

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040160    # com.google.android.gms.R.layout.wallet_view_region_code_spinner

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    check-cast v1, Landroid/view/View;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h(Z)V

    return-void
.end method

.method private g(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->I:Z

    if-eqz v0, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v4, v2

    move v3, v1

    :goto_1
    if-ge v4, v5, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v6, v0, Lgyo;

    if-eqz v6, :cond_4

    if-eqz p1, :cond_3

    check-cast v0, Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    move v0, v1

    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    check-cast v0, Lgyo;

    invoke-interface {v0}, Lgyo;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_0
.end method

.method private h(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:Lgxw;

    invoke-interface {v0, p1}, Lgxw;->setEnabled(Z)V

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final M_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->as:Lgxt;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->U()Lsf;

    move-result-object v0

    new-instance v1, Lgvl;

    invoke-direct {v1, p0}, Lgvl;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-virtual {v0, v1}, Lsf;->a(Lsh;)V

    return-void
.end method

.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f04012c    # com.google.android.gms.R.layout.wallet_fragment_address_entry

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    const v1, 0x7f0a0319    # com.google.android.gms.R.id.address_fields_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    const v1, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    return-object v0
.end method

.method public final a()Lixo;
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->K()Landroid/util/SparseArray;

    move-result-object v3

    new-instance v1, Lixo;

    invoke-direct {v1}, Lixo;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-char v5, v0

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sparse-switch v5, :sswitch_data_0

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :sswitch_0
    iput-object v0, v1, Lixo;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    iget-object v5, v1, Lixo;->q:[Ljava/lang/String;

    invoke-static {v5, v0}, Lboz;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lixo;->q:[Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    iput-object v0, v1, Lixo;->d:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    iput-object v0, v1, Lixo;->f:Ljava/lang/String;

    goto :goto_2

    :sswitch_4
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iput-object v0, v1, Lixo;->k:Ljava/lang/String;

    goto :goto_2

    :sswitch_5
    iput-object v0, v1, Lixo;->m:Ljava/lang/String;

    goto :goto_2

    :sswitch_6
    iput-object v0, v1, Lixo;->s:Ljava/lang/String;

    goto :goto_2

    :sswitch_7
    iput-object v0, v1, Lixo;->r:Ljava/lang/String;

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iput-object v0, v1, Lixo;->c:Ljava/lang/String;

    :cond_3
    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_1
        0x32 -> :sswitch_1
        0x43 -> :sswitch_3
        0x4e -> :sswitch_6
        0x4f -> :sswitch_7
        0x52 -> :sswitch_0
        0x53 -> :sswitch_2
        0x58 -> :sswitch_5
        0x5a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(C)V
    .locals 7

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0196    # com.google.android.gms.R.string.wallet_error_address_field_invalid

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "AddressEntryFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field to mark invalid not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 4

    const/4 v1, 0x2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    if-eq p2, v1, :cond_1

    const-string v0, "AddressEntryFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    if-ne p1, v0, :cond_0

    if-ne p2, v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->J()V

    goto :goto_0

    :cond_3
    if-ne p2, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v0, v1}, Lgty;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->T()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final a(Lgxt;)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-interface {p1, v0}, Lgxt;->c(I)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->as:Lgxt;

    return-void
.end method

.method public final a(Lixo;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v0, :cond_1

    iput-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance p1, Lixo;

    invoke-direct {p1}, Lixo;-><init>()V

    :cond_2
    iget-object v0, p1, Lixo;->a:Ljava/lang/String;

    invoke-static {v0}, Lhgq;->b(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    const/16 v0, 0x35a

    if-eq v3, v0, :cond_4

    move v0, v1

    :goto_1
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    iget-object v4, v4, Lixo;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    iget-object v4, v4, Lixo;->c:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    if-nez v4, :cond_5

    if-eqz v0, :cond_0

    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-ne v3, v0, :cond_9

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v0, v3}, Lgty;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->T()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v2, v3, v0, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    :goto_2
    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Lixo;)V

    iput-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v3, "id"

    invoke-static {v0, v3}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "--"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    const-string v3, "lang"

    invoke-static {v0, v3}, Lgty;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-static {v0, v3}, Lgty;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->T()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v0, v2, v5, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_8
    move v1, v2

    goto :goto_2

    :cond_9
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-nez v0, :cond_a

    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(I)V

    goto/16 :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lorg/json/JSONObject;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    const-string v0, "countries"

    invoke-static {p1, v0}, Lgty;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgty;->a([Ljava/lang/String;)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p1}, Lgty;->b(Lorg/json/JSONObject;)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    if-nez v0, :cond_b

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->K()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    int-to-char v3, v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v5

    instance-of v5, v5, Landroid/widget/Spinner;

    if-eqz v5, :cond_1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    move v3, v0

    :goto_3
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    new-instance v5, Lguo;

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Lguo;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    instance-of v0, v0, Lgug;

    if-eqz v0, :cond_8

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lgug;

    invoke-interface {v0}, Lgug;->a()Lgue;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-static {v0}, Lgul;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    new-instance v5, Lgul;

    iget-object v6, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v5, v6}, Lgul;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->N()V

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/util/SparseArray;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Lixo;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    const v1, 0x7f0a0318    # com.google.android.gms.R.id.focusable_placeholder

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_9

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_6
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    goto/16 :goto_0

    :cond_7
    move v3, v4

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aa:Ljava/util/ArrayList;

    new-instance v5, Lgue;

    iget-object v6, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v5, v6}, Lgue;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_5

    :cond_a
    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ai:Landroid/view/View;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/View;->requestFocus(I)Z

    goto :goto_5

    :cond_b
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->R()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h(Z)V

    :cond_0
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    if-eqz v0, :cond_2

    iget v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    const/16 v2, 0x35a

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    if-ne v2, v3, :cond_0

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aj:I

    :cond_0
    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ak:Z

    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->am:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    if-eqz v2, :cond_1

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-char v4, v2, v1

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->an:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:I

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ao:Ljava/util/ArrayList;

    :cond_2
    return-void
.end method

.method public final b()V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->P()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    if-eqz p1, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    if-nez v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->S()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->M()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->as:Lgxt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->as:Lgxt;

    invoke-interface {v0, p1}, Lgxt;->c(I)V

    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    move-object v1, v0

    :cond_0
    if-eqz p1, :cond_5

    const-string v0, "enabled"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    const-string v0, "regionCodes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    const-string v0, "pendingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "pendingAddress"

    const-class v2, Lixo;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lixo;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lixo;)V

    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-nez v0, :cond_2

    const-string v0, "selectedCountry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    :cond_2
    const-string v0, "countryData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    const-string v2, "countryData"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-static {v0}, Lgty;->b(Lorg/json/JSONObject;)I

    move-result v0

    if-eqz v0, :cond_3

    const/16 v2, 0x35a

    if-eq v0, v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    if-eq v0, v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lorg/json/JSONObject;)V

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    const-string v0, "languageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "languageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    :cond_4
    const-string v0, "networkErrorDialogTag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "networkErrorDialogTag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->al:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    array-length v0, v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddressEntryFragment"

    const-string v2, "Could not construct JSONObject from KEY_COUNTRY_DATA json string"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    if-nez v1, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->J()V

    goto :goto_1

    :cond_7
    array-length v0, v1

    if-nez v0, :cond_8

    const-string v0, "AddressEntryFragment"

    const-string v1, "No countries available"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "List of countries must either be non-empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    goto :goto_1

    :cond_9
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->L()Z

    goto :goto_1
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "selectedCountry"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->aq:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "regionCodes"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    if-eqz v0, :cond_0

    const-string v0, "pendingAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ar:Lixo;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    const-string v0, "countryData"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Y:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "languageCode"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "networkErrorDialogTag"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ap:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->S()V

    return-void
.end method

.method public final i()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lgyo;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lgyo;

    invoke-interface {v0}, Lgyo;->S_()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final w()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k()Lu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    return-void
.end method
