.class public Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;
.super Lgwb;
.source "SourceFile"

# interfaces
.implements Lgxw;


# instance fields
.field private a:Lgxx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lgwb;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lgwb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lgwb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private a()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)Lgxx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a:Lgxx;

    return-object v0
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->S_()Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lgxx;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a:Lgxx;

    return-void
.end method

.method public final a([I)V
    .locals 3

    new-instance v0, Lgxz;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Lboz;->a([I)[Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lgxz;-><init>(Landroid/content/Context;[Ljava/lang/Integer;)V

    const v1, 0x7f040161    # com.google.android.gms.R.layout.wallet_view_region_code_spinner_dropdown

    invoke-virtual {v0, v1}, Lgxz;->setDropDownViewResource(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v0, Lgxy;

    invoke-direct {v0, p0}, Lgxy;-><init>(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final d_(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    const-string v1, "Populate selector with region codes before setting the selected Region Code"

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgxz;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgxz;->getPosition(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a(I)V

    :cond_0
    return-void
.end method
