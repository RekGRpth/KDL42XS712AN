.class public Lcom/google/android/gms/wallet/common/ui/FormSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lgyo;


# static fields
.field public static final a:Lgxd;


# instance fields
.field private b:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private c:Z

.field private d:Lgxd;

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgxc;

    invoke-direct {v0}, Lgxc;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->a:Lgxd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->c:Z

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->a:Lgxd;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->d:Lgxd;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->f:Z

    invoke-super {p0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->c:Z

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->a:Lgxd;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->d:Lgxd;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->f:Z

    invoke-super {p0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->c:Z

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->a:Lgxd;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->d:Lgxd;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->f:Z

    invoke-super {p0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must set non-empty adapter before validating"

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->e:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->S_()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->d:Lgxd;

    invoke-interface {v2, v0}, Lgxd;->a(Landroid/view/View;)V

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->d:Lgxd;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0165    # com.google.android.gms.R.string.wallet_error_field_must_not_be_empty

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lgxd;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    move v1, v2

    goto :goto_1
.end method

.method public final S_()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getSelectedItemPosition()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    instance-of v2, v0, Landroid/widget/ListAdapter;

    if-eqz v2, :cond_3

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->c:Z

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->R_()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    invoke-super/range {p0 .. p5}, Landroid/widget/Spinner;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->f:Z

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->R_()Z

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->R_()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p1}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "superSavedInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "potentialErrorOnConfigChange"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->e:Z

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "superSavedInstanceState"

    invoke-super {p0}, Landroid/widget/Spinner;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "potentialErrorOnConfigChange"

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method
