.class public Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lgyq;


# instance fields
.field a:Landroid/widget/TextView;

.field public b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private c:Ljava/lang/String;

.field private d:Lgxq;

.field private e:Lgxp;

.field private f:Lgxr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015c    # com.google.android.gms.R.layout.wallet_view_money_amount_input

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0371    # com.google.android.gms.R.id.payment_amount_edit_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lgxo;

    invoke-direct {v1, p0}, Lgxo;-><init>(Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    :cond_0
    return v0
.end method

.method public final S_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    return v0
.end method

.method public final a()Lioh;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lgth;->a(Ljava/lang/String;Ljava/lang/String;)Lioh;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lioh;Lioh;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p2, :cond_0

    move-object p2, p1

    :cond_0
    invoke-static {p2}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p2, Lioh;->a:J

    iget-wide v3, p1, Lioh;->a:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b019e    # com.google.android.gms.R.string.wallet_minimum_amount

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lgxq;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lgxq;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lgzo;)V

    :cond_2
    new-instance v1, Lgxq;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b019f    # com.google.android.gms.R.string.wallet_error_amount_invalid

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lgxq;-><init>(Ljava/lang/CharSequence;Lioh;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lgxq;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lgxq;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lgxp;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lgxp;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lgzo;)V

    :cond_3
    new-instance v1, Lgxp;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b01a0    # com.google.android.gms.R.string.wallet_error_amount_less_than_minimum

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Lgxp;-><init>(Ljava/lang/CharSequence;Lioh;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lgxp;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lgxp;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgzo;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lgxr;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lgxr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_4
    new-instance v0, Lgxr;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {p1}, Lgth;->b(Lioh;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lgth;->c(Lioh;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lgxr;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lgxr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lgxr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->c:Ljava/lang/String;

    invoke-static {p1}, Lgth;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0370    # com.google.android.gms.R.id.payment_amount_currency_symbol_left

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0a0372    # com.google.android.gms.R.id.payment_amount_currency_symbol_right

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    return-void
.end method
