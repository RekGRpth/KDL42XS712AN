.class public Lcom/google/android/gms/wallet/common/ui/CvcImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a()V

    return-void
.end method

.method private a()V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f020260    # com.google.android.gms.R.drawable.wallet_cvc_hint_default

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->setImageResource(I)V

    :goto_0
    return-void

    :pswitch_0
    const v0, 0x7f02025f    # com.google.android.gms.R.drawable.wallet_cvc_hint_amex

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->setImageResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a()V

    :cond_0
    return-void
.end method
