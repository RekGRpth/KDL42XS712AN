.class public Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lgvt;


# static fields
.field private static final b:[C


# instance fields
.field final a:Lgvv;

.field private final c:Lgvv;

.field private d:Lgvu;

.field private e:Lipv;

.field private f:Z

.field private g:Ljava/util/ArrayList;

.field private h:I

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x4e

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b:[C

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016f    # com.google.android.gms.R.string.wallet_add_new_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c:Lgvv;

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0170    # com.google.android.gms.R.string.wallet_select_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f:Z

    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->h:I

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->i:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016f    # com.google.android.gms.R.string.wallet_add_new_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c:Lgvv;

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0170    # com.google.android.gms.R.string.wallet_select_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f:Z

    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->h:I

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->i:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016f    # com.google.android.gms.R.string.wallet_add_new_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c:Lgvv;

    new-instance v0, Lgvv;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0170    # com.google.android.gms.R.string.wallet_select_address

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lgvv;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f:Z

    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->h:I

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->i:Z

    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static synthetic a(Lgvx;)Ljava/lang/Object;
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Lgvx;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lxg;->E:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->h:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->i:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Lipv;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b(Lipv;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->h:I

    return v0
.end method

.method private b(Lipv;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->i:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lgth;->b(Lipv;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lipv;->i:[I

    array-length v0, v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c()[C
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b:[C

    return-object v0
.end method

.method private d()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    invoke-virtual {v0, v1}, Lgvw;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private e()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    new-instance v1, Lgvx;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    invoke-direct {v1, v2}, Lgvx;-><init>(Lgvv;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgvw;->insert(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgvw;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgvx;

    invoke-virtual {v0, v1}, Lgvw;->remove(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgvu;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    return-void
.end method

.method public final a(Lipv;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    const-string v1, "Set addresses before setting the selected address"

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    invoke-virtual {v0, p1}, Lgvw;->a(Lipv;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->setSelection(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgvw;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a([Lipv;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lgvx;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lgvv;

    invoke-direct {v0, v2}, Lgvx;-><init>(Lgvv;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_0

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    new-instance v4, Lgvx;

    invoke-direct {v4, v3}, Lgvx;-><init>(Lipv;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    if-eqz v0, :cond_1

    new-instance v0, Lgvx;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c:Lgvv;

    invoke-direct {v0, v2}, Lgvx;-><init>(Lgvv;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    invoke-virtual {v0}, Lgvw;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_2
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->g:Ljava/util/ArrayList;

    new-instance v0, Lgvw;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lgvw;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p0, p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->j:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgvw;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v0}, Lgvw;->a(Lgvw;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lipv;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a([Lipv;)V

    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v0, v1, Lipv;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lipv;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->b(Lipv;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e:Lipv;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a(Lipv;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e:Lipv;

    if-eq v1, v0, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    move-object v0, v1

    check-cast v0, Lipv;

    invoke-interface {v2, v0}, Lgvu;->a(Lipv;)V

    check-cast v1, Lipv;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e:Lipv;

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->f()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->c:Lgvv;

    if-ne v1, v0, :cond_5

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e:Lipv;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    invoke-interface {v0}, Lgvu;->a()V

    goto :goto_0

    :cond_5
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e:Lipv;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    invoke-interface {v0, v2}, Lgvu;->a(Lipv;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->d:Lgvu;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgvu;->a(Lipv;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->isEnabled()Z

    move-result v0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lgvw;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgvw;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e()V

    const/4 p1, 0x0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public setSelection(IZ)V
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->e()V

    const/4 p1, 0x0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/Spinner;->setSelection(IZ)V

    return-void
.end method
