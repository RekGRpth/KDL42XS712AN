.class public Lcom/google/android/gms/wallet/common/DisplayHints;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Liob;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgsq;

    invoke-direct {v0}, Lgsq;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/DisplayHints;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Liob;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Liob;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Liob;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Liob;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Liob;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Liob;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
