.class public Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;
.super Lcom/google/android/gms/wallet/common/ui/FormEditText;
.source "SourceFile"


# instance fields
.field private c:I

.field private d:Landroid/text/TextWatcher;

.field private e:Lgzd;

.field private f:Lgzd;

.field private g:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

.field private h:Ljava/lang/String;

.field private i:[I

.field private final j:Landroid/text/TextWatcher;

.field private final k:Landroid/text/TextWatcher;

.field private l:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    new-instance v0, Lgwe;

    invoke-direct {v0, p0}, Lgwe;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:Landroid/text/TextWatcher;

    new-instance v0, Lgwf;

    invoke-direct {v0, p0}, Lgwf;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/text/TextWatcher;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    new-instance v0, Lgwe;

    invoke-direct {v0, p0}, Lgwe;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:Landroid/text/TextWatcher;

    new-instance v0, Lgwf;

    invoke-direct {v0, p0}, Lgwf;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/text/TextWatcher;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    new-instance v0, Lgwe;

    invoke-direct {v0, p0}, Lgwe;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:Landroid/text/TextWatcher;

    new-instance v0, Lgwf;

    invoke-direct {v0, p0}, Lgwf;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/text/TextWatcher;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e()V

    return-void
.end method

.method private static a(Landroid/util/DisplayMetrics;I)I
    .locals 2

    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-static {v0, v1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private a(I)Landroid/graphics/drawable/Drawable;
    .locals 5

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/16 v3, 0x27

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    const/16 v4, 0x19

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v4, v0, v3, v2}, Lhgz;->a(IIII)I

    move-result v0

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Z)V

    return-void
.end method

.method private varargs b([I)Landroid/graphics/drawable/Drawable;
    .locals 14

    const/4 v13, 0x1

    const/4 v0, 0x0

    array-length v1, p1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v2, v13}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    aget v6, p1, v0

    invoke-static {v5, v6, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v7, v6

    int-to-float v8, v5

    div-float/2addr v7, v8

    const/16 v8, 0x6e

    invoke-static {v2, v8}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    add-int/lit8 v8, v1, -0x1

    mul-int/2addr v8, v3

    sub-int v8, v2, v8

    div-int/2addr v8, v1

    int-to-float v9, v8

    mul-float/2addr v7, v9

    float-to-int v7, v7

    invoke-static {v5, v6, v8, v6}, Lhgz;->a(IIII)I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v7, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    new-instance v5, Landroid/graphics/Paint;

    const/4 v9, 0x2

    invoke-direct {v5, v9}, Landroid/graphics/Paint;-><init>(I)V

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    aget v10, p1, v0

    invoke-static {v9, v10, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {v9, v8, v7, v13}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    mul-int/2addr v11, v0

    mul-int v12, v3, v0

    add-int/2addr v11, v12

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v6, v10, v11, v12, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Z)V
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    packed-switch v1, :pswitch_data_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setMinHeight(I)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    invoke-virtual {v0, v4}, Lgzd;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b([I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b([I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    invoke-static {v1}, Lgth;->a(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    invoke-virtual {v2, v1}, Lgzd;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-static {v1}, Lgth;->a(I)I

    move-result v0

    :cond_5
    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x7f020251    # com.google.android.gms.R.drawable.wallet_card_full_visa
        0x7f02024f    # com.google.android.gms.R.drawable.wallet_card_full_mastercard
        0x7f02024d    # com.google.android.gms.R.drawable.wallet_card_full_discover
    .end array-data

    :array_1
    .array-data 4
        0x7f020251    # com.google.android.gms.R.drawable.wallet_card_full_visa
        0x7f02024f    # com.google.android.gms.R.drawable.wallet_card_full_mastercard
        0x7f02024c    # com.google.android.gms.R.drawable.wallet_card_full_amex
        0x7f02024d    # com.google.android.gms.R.drawable.wallet_card_full_discover
    .end array-data
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private f()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const v0, 0x7f020251    # com.google.android.gms.R.drawable.wallet_card_full_visa

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lgwq;->a(Landroid/graphics/drawable/Drawable;)Lgwq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lgzo;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Landroid/text/TextWatcher;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v0, v3}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    goto :goto_0
.end method

.method public final a([I)V
    .locals 7

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c(Lgzo;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lgzd;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lgzd;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c(Lgzo;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lgzd;

    :cond_1
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    if-ne v1, v6, :cond_2

    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:[I

    if-eqz p1, :cond_4

    array-length v4, p1

    move v1, v3

    :goto_0
    if-ge v1, v4, :cond_4

    aget v5, p1, v1

    if-nez v5, :cond_3

    :goto_1
    if-eq v1, v2, :cond_6

    new-instance v2, Lgzd;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v4

    new-array v5, v6, [I

    aput v3, v5, v3

    invoke-direct {v2, v4, v5}, Lgzd;-><init>(Landroid/content/Context;[I)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lgzd;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lgzd;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lgzo;)V

    if-nez p1, :cond_5

    :goto_2
    new-instance v1, Lgzd;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lgzd;-><init>(Landroid/content/Context;[I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Lgzd;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lgzo;)V

    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Z)V

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    array-length v2, p1

    add-int/lit8 v0, v2, -0x1

    new-array v0, v0, [I

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v1, 0x1

    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_6
    move-object v0, p1

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:Landroid/text/TextWatcher;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:Landroid/text/TextWatcher;

    :cond_1
    new-instance v0, Lgyv;

    invoke-direct {v0, p0}, Lgyv;-><init>(Landroid/widget/EditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:Landroid/text/TextWatcher;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "instrumentType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    const-string v0, "cardNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    const-string v0, "disallowedCardTypes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "superInstanceState"

    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "instrumentType"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "cardNumber"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "disallowedCardTypes"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-object v0
.end method
