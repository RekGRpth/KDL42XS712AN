.class public Lcom/google/android/gms/wallet/ia/CartDetailsView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/view/ViewGroup;

.field c:Landroid/view/ViewGroup;

.field d:Z

.field private e:Landroid/view/LayoutInflater;

.field private f:I

.field private g:Ljava/lang/CharSequence;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Ljava/lang/CharSequence;

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f040152    # com.google.android.gms.R.layout.wallet_view_cart_details

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0358    # com.google.android.gms.R.id.total_price

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    const v0, 0x7f0a035a    # com.google.android.gms.R.id.short_line_items_table

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    const v0, 0x7f0a035b    # com.google.android.gms.R.id.long_line_items_table

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e()V

    return-void
.end method

.method private static a(Landroid/view/ViewGroup;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "line_item"

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    const/4 v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->invalidate()V

    :cond_2
    return-void
.end method

.method private b(I)I
    .locals 3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    return v0
.end method

.method private d()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    if-nez v0, :cond_0

    const v0, 0x7f01004b    # com.google.android.gms.R.attr.drawableWalletCollapse

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:I

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    if-nez v0, :cond_1

    const v0, 0x7f01004a    # com.google.android.gms.R.attr.drawableWalletExpand

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:I

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "line_item"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    return-object v0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0125    # com.google.android.gms.R.string.wallet_details_toggle_accessibility_description

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    const v0, 0x7f0a035c    # com.google.android.gms.R.id.shipping_row

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a035d    # com.google.android.gms.R.id.shipping_label

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a035e    # com.google.android.gms.R.id.shipping_price

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f040146    # com.google.android.gms.R.layout.wallet_row_line_item_default_short

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "line_item"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    const v0, 0x7f0a0365    # com.google.android.gms.R.id.total_price_row

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const v0, 0x7f0a0366    # com.google.android.gms.R.id.item_total_price_long

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    const v0, 0x7f0a035f    # com.google.android.gms.R.id.discount_row

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0360    # com.google.android.gms.R.id.discount_label

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0361    # com.google.android.gms.R.id.discount_amount

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/view/ViewGroup;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:I

    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 2

    const v0, 0x7f0a0362    # com.google.android.gms.R.id.tax_exclusive_row

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0367    # com.google.android.gms.R.id.tax_inclusive

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    const v0, 0x7f0a0367    # com.google.android.gms.R.id.tax_inclusive

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a0362    # com.google.android.gms.R.id.tax_exclusive_row

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a0363    # com.google.android.gms.R.id.exclusive_tax_label

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0364    # com.google.android.gms.R.id.exclusive_tax_amount

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "isShowingLongView"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e()V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "superInstanceState"

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "isShowingLongView"

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method
