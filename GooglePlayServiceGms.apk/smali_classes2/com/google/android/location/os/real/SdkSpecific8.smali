.class public Lcom/google/android/location/os/real/SdkSpecific8;
.super Lifs;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lifs;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/telephony/gsm/GsmCellLocation;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public a(Ljava/lang/String;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/location/Location;)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Landroid/net/wifi/ScanResult;)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Landroid/hardware/SensorManager;Lidu;)Lhln;
    .locals 1

    new-instance v0, Lhlj;

    invoke-direct {v0}, Lhlj;-><init>()V

    return-object v0
.end method

.method public a(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;Lilx;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 6

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v1, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    return-void
.end method

.method public a(Landroid/net/wifi/WifiManager;Lilx;)V
    .locals 0

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->startScan()Z

    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/telephony/TelephonyManager;IJ)[Lhtf;
    .locals 3

    invoke-static {p1, p2, p3, p4}, Lifl;->a(Landroid/telephony/TelephonyManager;IJ)Lhtf;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lhtf;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method public b(Landroid/location/Location;)V
    .locals 0

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
