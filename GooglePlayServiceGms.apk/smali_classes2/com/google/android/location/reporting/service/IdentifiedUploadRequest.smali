.class public Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lihr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:J

.field private final b:J

.field private final c:Lcom/google/android/gms/location/reporting/UploadRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Liiz;

    invoke-direct {v0}, Liiz;-><init>()V

    sput-object v0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJLcom/google/android/gms/location/reporting/UploadRequest;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    iput-wide p3, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    const-string v0, "request"

    invoke-static {p5, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/UploadRequest;

    iput-object v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;
    .locals 6

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    const-class v0, Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/location/reporting/UploadRequest;

    new-instance v0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;-><init>(JJLcom/google/android/gms/location/reporting/UploadRequest;)V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequest;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequest;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/UploadRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/UploadRequest;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    iget-wide v3, p1, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    iget-wide v3, p1, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    iget-object v2, p1, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/location/reporting/UploadRequest;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Lcom/google/android/gms/location/reporting/UploadRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IdentifiedUploadRequest{mId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mElapsedRealtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/location/reporting/service/IdentifiedUploadRequest;->c:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
