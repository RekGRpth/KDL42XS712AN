.class public Lcom/google/android/location/reporting/LocationReportingController;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lihn;

.field private final c:Lihn;

.field private final d:Lihn;

.field private final e:Lihy;

.field private final f:Lijb;

.field private final g:Lhvb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lihn;Lihn;Lihn;Lihy;Lijb;Lhvb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lihn;

    iput-object p3, p0, Lcom/google/android/location/reporting/LocationReportingController;->c:Lihn;

    iput-object p4, p0, Lcom/google/android/location/reporting/LocationReportingController;->d:Lihn;

    iput-object p5, p0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lihy;

    iput-object p6, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    iput-object p7, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lhvb;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/ulr/ApiRate;)V
    .locals 8

    const/4 v7, 0x5

    invoke-static {p2}, Lihm;->a(Lcom/google/android/ulr/ApiRate;)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Likf;->a(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/reporting/LocationReportingController;->d:Lihn;

    invoke-virtual {v4, v3, v1, v0}, Lihn;->saveEntity(Landroid/accounts/Account;Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCoreUlr"

    const/4 v4, 0x5

    invoke-static {v0, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error saving ApiRate to DB for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "GCoreUlr"

    invoke-static {v4, v7}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error saving ApiRate to DB for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v0}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/reporting/service/ReportingConfig;J)V
    .locals 11

    invoke-static {}, Lijv;->a()V

    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    invoke-virtual {v0}, Lijb;->h()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    invoke-static {v0, p1}, Likf;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/location/reporting/service/ReportingConfig;)V

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lihn;

    invoke-static {v1, v0}, Likh;->a(Lihn;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->c:Lihn;

    invoke-static {v1, v0}, Likh;->a(Lihn;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->d:Lihn;

    invoke-static {v1, v0}, Likh;->a(Lihn;Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "GCoreUlr"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ensuring that reporting is active for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lesq;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lhkr;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v4, "internal"

    new-instance v0, Lcom/google/android/ulr/ApiRate;

    const-string v1, "stationary"

    sget-object v2, Lijs;->f:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    const-string v3, "stationary"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V

    move-object v8, v0

    :goto_0
    sget-object v0, Lijs;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sget-object v0, Lijs;->j:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v0, Lijs;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    new-instance v0, Liie;

    invoke-virtual {v8}, Lcom/google/android/ulr/ApiRate;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct/range {v0 .. v7}, Liie;-><init>(JJZJ)V

    const-string v1, "com.google.android.location.reporting.ACTION_LOCATION"

    invoke-direct {p0, v1}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    invoke-virtual {v1}, Lijb;->h()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v1

    if-nez v1, :cond_7

    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Skipping FLP amnesia test: no oldConfig in "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lhvb;

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v2

    iget-wide v4, v0, Liie;->a:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v2

    const/16 v4, 0x66

    invoke-virtual {v2, v4}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v2

    iget-wide v4, v0, Liie;->c:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v2

    iget-boolean v4, v0, Liie;->b:Z

    invoke-virtual {v1, v2, v3, v4}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V

    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GMS FLP location updates requested: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Liie;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; triggerImmediate=false"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v1, "com.google.android.location.reporting.ACTION_ACTIVITY"

    invoke-direct {p0, v1}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    new-instance v3, Liac;

    invoke-direct {v3}, Liac;-><init>()V

    iget-wide v4, v0, Liie;->d:J

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v1}, Liac;->a(JZLandroid/app/PendingIntent;)Liac;

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Liac;->c(Z)Liac;

    invoke-virtual {v3, v2}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_d

    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GCoreUlr"

    const-string v2, "Unable to bind to GMS NLP"

    invoke-static {v1, v2}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    invoke-virtual {v1, p1, v8, v0}, Lijb;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/ulr/ApiRate;Liie;)V

    invoke-direct {p0, p1, v8}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/service/ReportingConfig;Lcom/google/android/ulr/ApiRate;)V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GMS FLP location and AR updates requested: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v0}, Liju;->a(Landroid/content/Context;)V

    :cond_5
    :goto_3
    return-void

    :cond_6
    const-string v4, "internal"

    new-instance v0, Lcom/google/android/ulr/ApiRate;

    const-string v1, "default"

    sget-object v2, Lijs;->e:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    const-string v3, "default"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V

    move-object v8, v0

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/ReportingConfig;->isReportingActive()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lhvb;

    invoke-virtual {v2, v3}, Lhvb;->a(Landroid/app/PendingIntent;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    if-nez v4, :cond_8

    const-string v2, "flp_request_dropped"

    invoke-static {v2}, Likf;->a(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "FLP dropped ULR location request: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v4}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_8
    iget-object v2, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    iget-object v1, v2, Lijb;->a:Liie;

    invoke-virtual {v2}, Lijb;->h()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/ReportingConfig;->isReportingActive()Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    if-eqz v4, :cond_b

    if-eqz v1, :cond_b

    if-eqz v2, :cond_b

    iget-wide v5, v1, Liie;->a:J

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v9

    cmp-long v2, v5, v9

    if-nez v2, :cond_a

    :goto_5
    invoke-virtual {v0, v1}, Liie;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "current request "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " matches "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", no need to request a new one"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    :cond_a
    const-string v2, "GCoreUlr"

    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FLP request "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " incompatible with "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5}, Lijy;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "flp_request_inconsistent"

    invoke-static {v1}, Likf;->a(Ljava/lang/String;)V

    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    :cond_c
    const-string v2, "GCoreUlr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FLP request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " -> "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GMS NLP is bound with activity detection polling interval "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Liie;->d:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_e
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ensuring that reporting is stopped because of reasons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->getInactiveReasonsString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const-string v0, "GCoreUlr"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "GCoreUlr"

    const-string v1, "Stopping Location Reporting."

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    const-string v0, "com.google.android.location.reporting.ACTION_LOCATION"

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lhvb;

    invoke-virtual {v1, v0}, Lhvb;->b(Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    const-string v0, "com.google.android.location.reporting.ACTION_ACTIVITY"

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "GCoreUlr"

    const-string v1, "Unbound from all location providers"

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lihy;

    invoke-virtual {v0}, Lihy;->a()V

    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lijb;

    invoke-virtual {v0, p1}, Lijb;->a(Lcom/google/android/location/reporting/service/ReportingConfig;)V

    goto/16 :goto_3
.end method
