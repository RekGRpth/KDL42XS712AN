.class final Lhvh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final a:Leqc;

.field public final b:Lhwl;

.field public final c:Landroid/app/PendingIntent;

.field final synthetic d:Lhvb;

.field private e:J

.field private f:I

.field private g:Landroid/location/Location;

.field private final h:Z

.field private final i:Ljava/util/HashSet;

.field private final j:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lhvb;Lhwl;ZLeqc;Landroid/app/PendingIntent;)V
    .locals 2

    iput-object p1, p0, Lhvh;->d:Lhvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lhvh;->a:Leqc;

    iput-object p2, p0, Lhvh;->b:Lhwl;

    const/4 v0, 0x0

    iput v0, p0, Lhvh;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lhvh;->g:Landroid/location/Location;

    iput-boolean p3, p0, Lhvh;->h:Z

    iput-object p5, p0, Lhvh;->c:Landroid/app/PendingIntent;

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p2, Lhwl;->e:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lhvh;->i:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p2, Lhwl;->e:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lhvh;->j:Ljava/util/HashSet;

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lhvh;->c:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvh;->d:Lhvb;

    iget-object v1, p0, Lhvh;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lhvb;->b(Landroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhvh;->d:Lhvb;

    iget-object v1, p0, Lhvh;->a:Leqc;

    invoke-virtual {v0, v1}, Lhvb;->a(Leqc;)V

    goto :goto_0
.end method

.method static synthetic a(Lhvh;)Z
    .locals 1

    iget-boolean v0, p0, Lhvh;->h:Z

    return v0
.end method

.method private a(ZLjava/util/HashSet;Ljava/lang/String;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lhvh;->b:Lhwl;

    iget-object v1, v1, Lhwl;->e:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lild;

    invoke-virtual {p2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lhvh;->d:Lhvb;

    invoke-static {v4}, Lhvb;->j(Lhvb;)Lila;

    move-result-object v4

    iget v5, v0, Lild;->a:I

    iget-object v6, v0, Lild;->b:Ljava/lang/String;

    invoke-virtual {v4, p3, v5, v6}, Lila;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v0, v2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lild;

    iget-object v3, p0, Lhvh;->d:Lhvb;

    invoke-static {v3}, Lhvb;->j(Lhvb;)Lila;

    move-result-object v3

    iget v4, v0, Lild;->a:I

    iget-object v0, v0, Lild;->b:Ljava/lang/String;

    invoke-virtual {v3, p3, v4, v0}, Lila;->b(Ljava/lang/String;ILjava/lang/String;)V

    move v0, v2

    goto :goto_2

    :cond_1
    invoke-virtual {p2}, Ljava/util/HashSet;->clear()V

    move v1, v0

    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lhvh;)I
    .locals 1

    iget v0, p0, Lhvh;->f:I

    return v0
.end method

.method static synthetic c(Lhvh;)J
    .locals 2

    iget-wide v0, p0, Lhvh;->e:J

    return-wide v0
.end method

.method static synthetic d(Lhvh;)V
    .locals 0

    invoke-direct {p0}, Lhvh;->a()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    iget-object v0, p0, Lhvh;->i:Ljava/util/HashSet;

    const-string v1, "android:monitor_location"

    invoke-direct {p0, p1, v0, v1}, Lhvh;->a(ZLjava/util/HashSet;Ljava/lang/String;)Z

    iget-object v0, p0, Lhvh;->b:Lhwl;

    iget-object v0, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    if-eqz p1, :cond_1

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhvh;->d:Lhvb;

    invoke-static {v0}, Lhvb;->a(Lhvb;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lhvh;->j:Ljava/util/HashSet;

    const-string v2, "android:monitor_location_high_power"

    invoke-direct {p0, v0, v1, v2}, Lhvh;->a(ZLjava/util/HashSet;Ljava/lang/String;)Z

    move-result v0

    const/16 v1, 0x13

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.location.HIGH_POWER_REQUEST_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhvh;->d:Lhvb;

    invoke-static {v1}, Lhvb;->i(Lhvb;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)Z
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lhvh;->d:Lhvb;

    invoke-static {v2}, Lhvb;->h(Lhvb;)Lhuz;

    invoke-static {p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    iget-object v4, p0, Lhvh;->b:Lhwl;

    iget-object v4, v4, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->e()J

    move-result-wide v5

    cmp-long v5, v2, v5

    if-ltz v5, :cond_2

    invoke-direct {p0}, Lhvh;->a()V

    const-string v5, "GCoreFlp"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Removing expired location, current time=%s, expire at=%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v5, v6}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->f()I

    move-result v5

    iget v6, p0, Lhvh;->f:I

    if-lt v6, v5, :cond_3

    const-string v2, "GCoreFlp"

    invoke-static {v2, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Removing location because it has received maxUpdates=%s locations"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-static {v2, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget v6, p0, Lhvh;->f:I

    if-lez v6, :cond_4

    iget-wide v6, p0, Lhvh;->e:J

    sub-long v6, v2, v6

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->d()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_4

    const-string v5, "GCoreFlp"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Not reporting location that came in too fast, last interval=%s, fastest interval=%s"

    new-array v6, v11, [Ljava/lang/Object;

    iget-wide v7, p0, Lhvh;->e:J

    sub-long/2addr v2, v7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v5, v6}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationRequest;->g()F

    move-result v4

    iget-object v6, p0, Lhvh;->g:Landroid/location/Location;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lhvh;->g:Landroid/location/Location;

    invoke-virtual {v6, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v6

    cmpg-float v6, v6, v4

    if-gez v6, :cond_5

    const-string v2, "GCoreFlp"

    invoke-static {v2, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Not reporting because displacement too small, last displacement=%s, smallest displacement=%s"

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v5, p0, Lhvh;->g:Landroid/location/Location;

    invoke-virtual {v5, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_6

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lhvh;->g:Landroid/location/Location;

    :cond_6
    iput-wide v2, p0, Lhvh;->e:J

    iget v0, p0, Lhvh;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhvh;->f:I

    iget v0, p0, Lhvh;->f:I

    if-lt v0, v5, :cond_7

    invoke-direct {p0}, Lhvh;->a()V

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final binderDied()V
    .locals 0

    invoke-direct {p0}, Lhvh;->a()V

    return-void
.end method
