.class public final Lgww;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgyo;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final c:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgww;->a:Landroid/content/Context;

    iput-object p2, p0, Lgww;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object p3, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    return-void
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v2, 0x64

    if-ge v0, v2, :cond_0

    add-int/lit16 v0, v0, 0x7d0

    :cond_0
    const/16 v2, 0xa

    invoke-static {v1, v0, v2}, Lgth;->a(III)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final R_()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lgww;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lgww;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v1, v2}, Lgww;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgww;->a:Landroid/content/Context;

    const v3, 0x7f0b015a    # com.google.android.gms.R.string.wallet_error_expired_credit_card

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgww;->a:Landroid/content/Context;

    const v3, 0x7f0b015c    # com.google.android.gms.R.string.wallet_error_year_invalid

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final S_()Z
    .locals 2

    iget-object v0, p0, Lgww;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lgww;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Lgww;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
