.class final Ldqz;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldqv;

.field private b:Ldrs;

.field private final c:Ldrq;


# direct methods
.method constructor <init>(Ldqv;Ldrq;)V
    .locals 3

    iput-object p1, p0, Ldqz;->a:Ldqv;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Ldqz;->c:Ldrq;

    const-string v0, "GamesService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RoomConnectAsyncTask initialized with RoomServiceClient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p1, [Ldrs;

    array-length v0, p1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    aget-object v0, p1, v2

    iput-object v0, p0, Ldqz;->b:Ldrs;

    iget-object v0, p0, Ldqz;->a:Ldqv;

    invoke-static {v0}, Ldqv;->e(Ldqv;)Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Ldqz;->a:Ldqv;

    invoke-static {v4}, Ldqv;->f(Ldqv;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Ldqz;->c:Ldrq;

    iget-object v5, p0, Ldqz;->a:Ldqv;

    invoke-static {v5}, Ldqv;->f(Ldqv;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v5

    iget-object v6, p0, Ldqz;->a:Ldqv;

    invoke-static {v6}, Ldqv;->g(Ldqv;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Ldqz;->a:Ldqv;

    invoke-virtual {v7}, Ldqv;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v5, v6, v7}, Ldrq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldqz;->c:Ldrq;

    invoke-virtual {v0, v4, v2}, Ldrq;->a(Ljava/lang/String;Z)Lcom/google/android/gms/games/internal/ConnectionInfo;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldqz;->c:Ldrq;

    invoke-virtual {v0, v4, v1}, Ldrq;->a(Ljava/lang/String;Z)Lcom/google/android/gms/games/internal/ConnectionInfo;

    move-result-object v0

    :cond_2
    if-nez v0, :cond_3

    const-string v0, "GamesService"

    const-string v1, "Failed to retrieve a clientAddress when connecting to network"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/ConnectionInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : Client is connected."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldac;->b()V

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v0, p0, Ldqz;->b:Ldrs;

    invoke-interface {v0, p1}, Ldrs;->a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    return-void
.end method
