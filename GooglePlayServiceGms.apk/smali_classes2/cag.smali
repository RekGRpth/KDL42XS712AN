.class public abstract Lcag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/lang/String;

.field b:I

.field public c:Landroid/database/Cursor;

.field private final d:Lcah;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcah;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcag;->a:Ljava/lang/String;

    iput-object p2, p0, Lcag;->d:Lcah;

    return-void
.end method


# virtual methods
.method public abstract a()Lcar;
.end method

.method public final a(Lbzb;)V
    .locals 2

    invoke-interface {p1}, Lbzb;->d()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcag;->c:Landroid/database/Cursor;

    iget-object v0, p0, Lcag;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcag;->c:Landroid/database/Cursor;

    iget-object v1, p0, Lcag;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcag;->b:I

    :cond_0
    return-void
.end method

.method public abstract b()Landroid/widget/SectionIndexer;
.end method

.method public c()Lcdp;
    .locals 1

    sget-object v0, Lceg;->c:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcag;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcag;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcag;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcag;->d:Lcah;

    invoke-virtual {v1}, Lcah;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
