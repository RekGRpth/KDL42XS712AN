.class public final Lfyj;
.super Lfye;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    const v0, 0x7f0400e6    # com.google.android.gms.R.layout.plus_oob_field_hidden_button

    return v0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0b0332    # com.google.android.gms.R.string.plus_oob_field_view_tag_hidden_button

    invoke-virtual {p0, v0}, Lfyj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfyj;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfyj;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lgfm;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lgfm;->m()Lgfq;

    move-result-object v0

    invoke-interface {v0}, Lgfq;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyj;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lgfm;->m()Lgfq;

    move-result-object v1

    invoke-interface {v1}, Lgfq;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lgfm;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    const-string v0, "appealButton"

    iget-object v1, p0, Lfyj;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 2

    const-string v0, "changeButton"

    iget-object v1, p0, Lfyj;->b:Lgfm;

    invoke-interface {v1}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    invoke-virtual {p0}, Lfyj;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfyj;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfyj;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
