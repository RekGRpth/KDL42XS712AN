.class final Lhzi;
.super Lhyx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhzi;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyx;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "WalkingActivityState"

    return-object v0
.end method

.method protected final d()I
    .locals 4

    const/16 v0, 0x12c

    iget-object v1, p0, Lhzi;->c:Lhys;

    invoke-virtual {v1}, Lhys;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhzi;->c:Lhys;

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhzi;->c:Lhys;

    const-wide v2, 0x40aa0aaaaaaaaaabL    # 3333.3333333333335

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v0, 0xb4

    goto :goto_0
.end method

.method protected final e()I
    .locals 4

    const/16 v0, 0x708

    iget-object v1, p0, Lhzi;->c:Lhys;

    invoke-virtual {v1}, Lhys;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3c

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhzi;->c:Lhys;

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x14

    iget-object v2, p0, Lhzi;->c:Lhys;

    iget v2, v2, Lhys;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method protected final f()I
    .locals 2

    iget-object v0, p0, Lhzi;->c:Lhys;

    iget v0, v0, Lhys;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method protected final i()D
    .locals 2

    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    return-wide v0
.end method
