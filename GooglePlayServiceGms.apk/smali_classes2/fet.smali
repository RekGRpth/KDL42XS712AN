.class public final Lfet;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lfew;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:J

.field public l:[Lfer;

.field public m:J

.field public n:Lfeu;

.field public o:[I


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v2, p0, Lfet;->a:Lfew;

    iput v1, p0, Lfet;->b:I

    iput v1, p0, Lfet;->c:I

    iput v1, p0, Lfet;->d:I

    iput v1, p0, Lfet;->e:I

    const/4 v0, 0x1

    iput v0, p0, Lfet;->f:I

    iput v1, p0, Lfet;->g:I

    const-string v0, ""

    iput-object v0, p0, Lfet;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lfet;->i:Ljava/lang/String;

    iput v1, p0, Lfet;->j:I

    iput-wide v3, p0, Lfet;->k:J

    invoke-static {}, Lfer;->c()[Lfer;

    move-result-object v0

    iput-object v0, p0, Lfet;->l:[Lfer;

    iput-wide v3, p0, Lfet;->m:J

    iput-object v2, p0, Lfet;->n:Lfeu;

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Lfet;->o:[I

    const/4 v0, -0x1

    iput v0, p0, Lfet;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Lfet;->a:Lfew;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfet;->a:Lfew;

    invoke-static {v4, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lfet;->b:I

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget v3, p0, Lfet;->b:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget v2, p0, Lfet;->c:I

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget v3, p0, Lfet;->c:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lfet;->e:I

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget v3, p0, Lfet;->e:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lfet;->f:I

    if-eq v2, v4, :cond_4

    const/4 v2, 0x5

    iget v3, p0, Lfet;->f:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lfet;->g:I

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    iget v3, p0, Lfet;->g:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lfet;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x7

    iget-object v3, p0, Lfet;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lfet;->i:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const/16 v2, 0x8

    iget-object v3, p0, Lfet;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lfet;->j:I

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    iget v3, p0, Lfet;->j:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-wide v2, p0, Lfet;->k:J

    cmp-long v2, v2, v5

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    iget-wide v3, p0, Lfet;->k:J

    invoke-static {v2, v3, v4}, Lizn;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lfet;->l:[Lfer;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lfet;->l:[Lfer;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lfet;->l:[Lfer;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    iget-object v3, p0, Lfet;->l:[Lfer;

    aget-object v3, v3, v0

    if-eqz v3, :cond_a

    const/16 v4, 0xb

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_b
    move v0, v2

    :cond_c
    iget-wide v2, p0, Lfet;->m:J

    cmp-long v2, v2, v5

    if-eqz v2, :cond_d

    const/16 v2, 0xc

    iget-wide v3, p0, Lfet;->m:J

    invoke-static {v2, v3, v4}, Lizn;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lfet;->n:Lfeu;

    if-eqz v2, :cond_e

    const/16 v2, 0xd

    iget-object v3, p0, Lfet;->n:Lfeu;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget v2, p0, Lfet;->d:I

    if-eqz v2, :cond_f

    const/16 v2, 0xe

    iget v3, p0, Lfet;->d:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget-object v2, p0, Lfet;->o:[I

    if-eqz v2, :cond_11

    iget-object v2, p0, Lfet;->o:[I

    array-length v2, v2

    if-lez v2, :cond_11

    move v2, v1

    :goto_1
    iget-object v3, p0, Lfet;->o:[I

    array-length v3, v3

    if-ge v1, v3, :cond_10

    iget-object v3, p0, Lfet;->o:[I

    aget v3, v3, v1

    invoke-static {v3}, Lizn;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_10
    add-int/2addr v0, v2

    iget-object v1, p0, Lfet;->o:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_11
    iput v0, p0, Lfet;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 5

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lfet;->a:Lfew;

    if-nez v0, :cond_1

    new-instance v0, Lfew;

    invoke-direct {v0}, Lfew;-><init>()V

    iput-object v0, p0, Lfet;->a:Lfew;

    :cond_1
    iget-object v0, p0, Lfet;->a:Lfew;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfet;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lfet;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfet;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_6
    iput v0, p0, Lfet;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfet;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfet;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfet;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfet;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lfet;->k:J

    goto :goto_0

    :sswitch_c
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lfet;->l:[Lfer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lfer;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lfet;->l:[Lfer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lfer;

    invoke-direct {v3}, Lfer;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lfet;->l:[Lfer;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lfer;

    invoke-direct {v3}, Lfer;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lfet;->l:[Lfer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lfet;->m:J

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lfet;->n:Lfeu;

    if-nez v0, :cond_5

    new-instance v0, Lfeu;

    invoke-direct {v0}, Lfeu;-><init>()V

    iput-object v0, p0, Lfet;->n:Lfeu;

    :cond_5
    iget-object v0, p0, Lfet;->n:Lfeu;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfet;->d:I

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x78

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lfet;->o:[I

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_6

    iget-object v3, p0, Lfet;->o:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    invoke-virtual {p1}, Lizm;->g()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lfet;->o:[I

    array-length v0, v0

    goto :goto_3

    :cond_8
    invoke-virtual {p1}, Lizm;->g()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lfet;->o:[I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Lizm;->g()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    invoke-virtual {p1, v2}, Lizm;->e(I)V

    iget-object v2, p0, Lfet;->o:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_a

    iget-object v4, p0, Lfet;->o:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_c

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lfet;->o:[I

    array-length v2, v2

    goto :goto_6

    :cond_c
    iput-object v0, p0, Lfet;->o:[I

    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
        0x6a -> :sswitch_e
        0x70 -> :sswitch_f
        0x78 -> :sswitch_10
        0x7a -> :sswitch_11
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_6
        0x3 -> :sswitch_6
        0x4 -> :sswitch_6
        0x5 -> :sswitch_6
        0x6 -> :sswitch_6
        0x64 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lfet;->a:Lfew;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfet;->a:Lfew;

    invoke-virtual {p1, v3, v0}, Lizn;->a(ILizs;)V

    :cond_0
    iget v0, p0, Lfet;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v2, p0, Lfet;->b:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_1
    iget v0, p0, Lfet;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v2, p0, Lfet;->c:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_2
    iget v0, p0, Lfet;->e:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v2, p0, Lfet;->e:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_3
    iget v0, p0, Lfet;->f:I

    if-eq v0, v3, :cond_4

    const/4 v0, 0x5

    iget v2, p0, Lfet;->f:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_4
    iget v0, p0, Lfet;->g:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v2, p0, Lfet;->g:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_5
    iget-object v0, p0, Lfet;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x7

    iget-object v2, p0, Lfet;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lfet;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x8

    iget-object v2, p0, Lfet;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_7
    iget v0, p0, Lfet;->j:I

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v2, p0, Lfet;->j:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_8
    iget-wide v2, p0, Lfet;->k:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-wide v2, p0, Lfet;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    :cond_9
    iget-object v0, p0, Lfet;->l:[Lfer;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lfet;->l:[Lfer;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    :goto_0
    iget-object v2, p0, Lfet;->l:[Lfer;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lfet;->l:[Lfer;

    aget-object v2, v2, v0

    if-eqz v2, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_b
    iget-wide v2, p0, Lfet;->m:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_c

    const/16 v0, 0xc

    iget-wide v2, p0, Lfet;->m:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    :cond_c
    iget-object v0, p0, Lfet;->n:Lfeu;

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget-object v2, p0, Lfet;->n:Lfeu;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_d
    iget v0, p0, Lfet;->d:I

    if-eqz v0, :cond_e

    const/16 v0, 0xe

    iget v2, p0, Lfet;->d:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_e
    iget-object v0, p0, Lfet;->o:[I

    if-eqz v0, :cond_f

    iget-object v0, p0, Lfet;->o:[I

    array-length v0, v0

    if-lez v0, :cond_f

    :goto_1
    iget-object v0, p0, Lfet;->o:[I

    array-length v0, v0

    if-ge v1, v0, :cond_f

    const/16 v0, 0xf

    iget-object v2, p0, Lfet;->o:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_f
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfet;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfet;

    iget-object v2, p0, Lfet;->a:Lfew;

    if-nez v2, :cond_3

    iget-object v2, p1, Lfet;->a:Lfew;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfet;->a:Lfew;

    iget-object v3, p1, Lfet;->a:Lfew;

    invoke-virtual {v2, v3}, Lfew;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lfet;->b:I

    iget v3, p1, Lfet;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lfet;->c:I

    iget v3, p1, Lfet;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lfet;->d:I

    iget v3, p1, Lfet;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lfet;->e:I

    iget v3, p1, Lfet;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lfet;->f:I

    iget v3, p1, Lfet;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lfet;->g:I

    iget v3, p1, Lfet;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lfet;->h:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lfet;->h:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lfet;->h:Ljava/lang/String;

    iget-object v3, p1, Lfet;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lfet;->i:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lfet;->i:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lfet;->i:Ljava/lang/String;

    iget-object v3, p1, Lfet;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_e
    iget v2, p0, Lfet;->j:I

    iget v3, p1, Lfet;->j:I

    if-eq v2, v3, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-wide v2, p0, Lfet;->k:J

    iget-wide v4, p1, Lfet;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lfet;->l:[Lfer;

    iget-object v3, p1, Lfet;->l:[Lfer;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-wide v2, p0, Lfet;->m:J

    iget-wide v4, p1, Lfet;->m:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Lfet;->n:Lfeu;

    if-nez v2, :cond_13

    iget-object v2, p1, Lfet;->n:Lfeu;

    if-eqz v2, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lfet;->n:Lfeu;

    iget-object v3, p1, Lfet;->n:Lfeu;

    invoke-virtual {v2, v3}, Lfeu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p0, Lfet;->o:[I

    iget-object v3, p1, Lfet;->o:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    iget-object v0, p0, Lfet;->a:Lfew;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfet;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfet;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfet;->j:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lfet;->k:J

    iget-wide v4, p0, Lfet;->k:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfet;->l:[Lfer;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lfet;->m:J

    iget-wide v4, p0, Lfet;->m:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfet;->n:Lfeu;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfet;->o:[I

    invoke-static {v1}, Lizq;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lfet;->a:Lfew;

    invoke-virtual {v0}, Lfew;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfet;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lfet;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lfet;->n:Lfeu;

    invoke-virtual {v1}, Lfeu;->hashCode()I

    move-result v1

    goto :goto_3
.end method
