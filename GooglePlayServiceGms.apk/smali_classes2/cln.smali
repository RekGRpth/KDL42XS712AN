.class public final Lcln;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lclj;


# instance fields
.field private final a:Lcom/google/android/gms/drive/internal/model/About;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/internal/model/About;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/About;

    iput-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->g()Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    iget-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->g()Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()J
    .locals 2

    iget-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcln;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->d()J

    move-result-wide v0

    goto :goto_0
.end method
