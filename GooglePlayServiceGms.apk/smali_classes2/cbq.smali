.class public final Lcbq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcov;


# instance fields
.field private final a:J

.field private final b:D

.field private final c:J

.field private final d:Ljava/util/Random;

.field private e:I


# direct methods
.method public constructor <init>(JD)V
    .locals 7

    const-wide v5, 0x7fffffffffffffffL

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcbq;-><init>(JDJ)V

    return-void
.end method

.method public constructor <init>(JDJ)V
    .locals 8

    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v7}, Lcbq;-><init>(JDJLjava/util/Random;)V

    return-void
.end method

.method private constructor <init>(JDJLjava/util/Random;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcbq;->e:I

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p3, v3

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbkm;->b(Z)V

    cmp-long v0, p1, p5

    if-gtz v0, :cond_2

    :goto_2
    invoke-static {v1}, Lbkm;->b(Z)V

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-wide p1, p0, Lcbq;->a:J

    iput-wide p3, p0, Lcbq;->b:D

    iput-wide p5, p0, Lcbq;->c:J

    iput-object p7, p0, Lcbq;->d:Ljava/util/Random;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private declared-synchronized a()J
    .locals 6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcbq;->b:D

    sub-double/2addr v0, v4

    iget-object v2, p0, Lcbq;->d:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    iget-wide v2, p0, Lcbq;->b:D

    iget v4, p0, Lcbq;->e:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcbq;->a:J

    long-to-double v2, v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcbq;->c:J

    long-to-double v2, v2

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    iget-wide v0, p0, Lcbq;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    double-to-long v0, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()J
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcbq;->a()J

    move-result-wide v0

    iget v2, p0, Lcbq;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcbq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcbq;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget v1, p0, Lcbq;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcbq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcbq;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized f()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcbq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ExponentialBackoffRateLimiter[%d tokens, initialMs=%d, factor=%.3f]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcbq;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcbq;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcbq;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
