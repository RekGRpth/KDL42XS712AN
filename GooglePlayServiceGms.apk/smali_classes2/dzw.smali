.class final Ldzw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/database/CharArrayBuffer;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/database/CharArrayBuffer;

.field final f:Landroid/widget/ImageView;

.field final g:Landroid/widget/TextView;

.field final h:Landroid/view/View;

.field final synthetic i:Ldzv;


# direct methods
.method public constructor <init>(Ldzv;Landroid/view/View;)V
    .locals 2

    const/16 v1, 0x40

    iput-object p1, p0, Ldzw;->i:Ldzv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Ldzw;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzw;->b:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldzw;->c:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aF:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzw;->d:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldzw;->e:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aI:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldzw;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Ldzw;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    sget v0, Lxa;->aL:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzw;->g:Landroid/widget/TextView;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzw;->h:Landroid/view/View;

    invoke-static {p1}, Ldzv;->a(Ldzv;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzw;->h:Landroid/view/View;

    invoke-static {p1}, Ldzv;->b(Ldzv;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
