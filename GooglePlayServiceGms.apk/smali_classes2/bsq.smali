.class public final Lbsq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbsu;


# instance fields
.field private final a:Lcfc;

.field private final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final c:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final e:J

.field private final f:Lcmn;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcmn;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbsq;->a:Lcfc;

    iput-object p2, p0, Lbsq;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object p3, p0, Lbsq;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object p4, p0, Lbsq;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-wide p5, p0, Lbsq;->e:J

    iput-object p7, p0, Lbsq;->f:Lcmn;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 8

    new-instance v0, Lclz;

    iget-object v1, p0, Lbsq;->a:Lcfc;

    iget-object v2, p0, Lbsq;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lbsq;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v5, p0, Lbsq;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-wide v6, p0, Lbsq;->e:J

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lclz;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V

    iget-object v1, p0, Lbsq;->f:Lcmn;

    invoke-virtual {v1, v0}, Lcmn;->a(Lcml;)Z

    return-object v0
.end method
