.class public final Lihl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lijb;

.field b:Liht;

.field public final c:Lcom/google/android/location/reporting/StateReporter;

.field public final d:Lcom/google/android/location/reporting/LocationReportingController;

.field public final e:Lbpe;

.field private final f:Lihn;


# direct methods
.method public constructor <init>(Lijb;Lihn;Liht;Lcom/google/android/location/reporting/StateReporter;Lcom/google/android/location/reporting/LocationReportingController;Lbpe;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lihl;->a:Lijb;

    iput-object p2, p0, Lihl;->f:Lihn;

    iput-object p3, p0, Lihl;->b:Liht;

    iput-object p4, p0, Lihl;->c:Lcom/google/android/location/reporting/StateReporter;

    iput-object p5, p0, Lihl;->d:Lcom/google/android/location/reporting/LocationReportingController;

    iput-object p6, p0, Lihl;->e:Lbpe;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;Lcom/google/android/location/reporting/service/ReportingConfig;)V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x3

    const-string v0, "GCoreUlr"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received NLP activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p2}, Likf;->b(Lcom/google/android/location/reporting/service/ReportingConfig;)V

    invoke-virtual {p2}, Lcom/google/android/location/reporting/service/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "GCoreUlr"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Storing activity for account "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :try_start_0
    iget-object v3, p0, Lihl;->f:Lihn;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/AccountConfig;->g()Z

    move-result v0

    invoke-virtual {v3, v2, p1, v0}, Lihn;->saveEntity(Landroid/accounts/Account;Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GCoreUlr"

    const/4 v3, 0x5

    invoke-static {v0, v3}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to store activities for account "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lijy;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "GCoreUlr"

    invoke-static {v3, v7}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Problem saving activity for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Lijy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    const-string v0, "GCoreUlr"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not storing activity for non-history account "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    return-void
.end method
