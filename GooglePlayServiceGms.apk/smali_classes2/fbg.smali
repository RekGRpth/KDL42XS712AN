.class public final Lfbg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfbg;->a:[I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lfbg;->b:Ljava/lang/StringBuilder;

    return-void

    nop

    :array_0
    .array-data 4
        0x1100
        0x1101
        0x0
        0x1102
        0x0
        0x0
        0x1103
        0x1104
        0x1105
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x1106
        0x1107
        0x1108
        0x0
        0x1109
        0x110a
        0x110b
        0x110c
        0x110d
        0x110e
        0x110f
        0x1110
        0x1111
        0x1112
    .end array-data
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/16 v6, 0x3131

    const/4 v0, 0x0

    const v5, 0xac00

    sget-object v1, Lfbg;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    :goto_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    const/16 v3, 0x20

    if-eq v0, v3, :cond_3

    const/16 v3, 0x2c

    if-eq v0, v3, :cond_3

    const/16 v3, 0x2e

    if-eq v0, v3, :cond_3

    const/16 v3, 0x1100

    if-lt v0, v3, :cond_4

    const/16 v3, 0x1112

    if-le v0, v3, :cond_0

    if-lt v0, v6, :cond_4

    :cond_0
    const/16 v3, 0x314e

    if-le v0, v3, :cond_1

    if-lt v0, v5, :cond_4

    :cond_1
    const v3, 0xd7a3

    if-gt v0, v3, :cond_4

    if-lt v0, v5, :cond_5

    sub-int/2addr v0, v5

    div-int/lit16 v0, v0, 0x24c

    add-int/lit16 v0, v0, 0x1100

    :cond_2
    :goto_1
    sget-object v3, Lfbg;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    :cond_3
    if-lt v1, v2, :cond_6

    :cond_4
    sget-object v0, Lfbg;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    if-lt v0, v6, :cond_2

    add-int/lit16 v3, v0, -0x3131

    sget-object v4, Lfbg;->a:[I

    array-length v4, v4

    if-ge v3, v4, :cond_4

    sget-object v3, Lfbg;->a:[I

    add-int/lit16 v0, v0, -0x3131

    aget v0, v3, v0

    if-eqz v0, :cond_4

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0
.end method
