.class final Liwv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liws;


# instance fields
.field a:I

.field b:J

.field final synthetic c:Liwp;


# direct methods
.method constructor <init>(Liwp;)V
    .locals 2

    iput-object p1, p0, Liwv;->c:Liwp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Liwv;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liwv;->b:J

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final a(J)Liws;
    .locals 12

    const-wide/high16 v10, 0x4008000000000000L    # 3.0

    iget-wide v0, p0, Liwv;->b:J

    const-wide v2, 0x37e11d600L

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Liwr;

    iget-object v1, p0, Liwv;->c:Liwp;

    invoke-direct {v0, v1}, Liwr;-><init>(Liwp;)V

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Liwv;->c:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    invoke-virtual {v0}, Liwg;->c()D

    move-result-wide v0

    iget-object v2, p0, Liwv;->c:Liwp;

    iget-object v2, v2, Liwp;->b:Lixa;

    iget-object v2, v2, Lixa;->b:Liwm;

    invoke-virtual {v2}, Liwm;->e()D

    move-result-wide v2

    iget v4, p0, Liwv;->a:I

    const/4 v5, 0x5

    if-lt v4, v5, :cond_2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    cmpg-double v4, v0, v10

    if-ltz v4, :cond_4

    :cond_2
    iget-object v4, p0, Liwv;->c:Liwp;

    iget-wide v4, v4, Liwp;->e:J

    sub-long v4, p1, v4

    long-to-double v4, v4

    const-wide v6, 0x3ffb333333333333L    # 1.7

    iget-object v8, p0, Liwv;->c:Liwp;

    iget-wide v8, v8, Liwp;->d:J

    long-to-double v8, v8

    mul-double/2addr v6, v8

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_4

    :cond_3
    mul-double/2addr v0, v10

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Liwv;->c:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    iget-object v2, p0, Liwv;->c:Liwp;

    iget-wide v2, v2, Liwp;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    :cond_4
    new-instance v0, Liwt;

    iget-object v1, p0, Liwv;->c:Liwp;

    invoke-direct {v0, v1}, Liwt;-><init>(Liwp;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(JLiwe;)V
    .locals 8

    const-wide/16 v2, 0x0

    iget-object v0, p0, Liwv;->c:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Liwv;->c:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    sub-long v0, p1, v0

    :goto_0
    long-to-double v0, v0

    const-wide v4, 0x3ffb333333333333L    # 1.7

    iget-object v6, p0, Liwv;->c:Liwp;

    iget-wide v6, v6, Liwp;->d:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    cmpl-double v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Liwv;->a:I

    :cond_0
    sget-object v0, Liwq;->a:[I

    iget-object v1, p3, Liwe;->b:Liwf;

    invoke-virtual {v1}, Liwf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Liwv;->c:Liwp;

    iget-wide v0, v0, Liwp;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Liwv;->c:Liwp;

    iget-wide v0, v0, Liwp;->g:J

    sub-long v2, p1, v0

    :cond_1
    iput-wide v2, p0, Liwv;->b:J

    return-void

    :cond_2
    move-wide v0, v2

    goto :goto_0

    :pswitch_0
    iget v0, p0, Liwv;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwv;->a:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Liwz;)V
    .locals 0

    return-void
.end method

.method public final b()Liwy;
    .locals 1

    iget-object v0, p0, Liwv;->c:Liwp;

    iget-object v0, v0, Liwp;->b:Lixa;

    return-object v0
.end method
