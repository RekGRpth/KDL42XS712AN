.class public final enum Lse;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lse;

.field public static final enum b:Lse;

.field public static final enum c:Lse;

.field public static final enum d:Lse;

.field private static final synthetic e:[Lse;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lse;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, Lse;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse;->a:Lse;

    new-instance v0, Lse;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lse;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse;->b:Lse;

    new-instance v0, Lse;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, Lse;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse;->c:Lse;

    new-instance v0, Lse;

    const-string v1, "IMMEDIATE"

    invoke-direct {v0, v1, v5}, Lse;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse;->d:Lse;

    const/4 v0, 0x4

    new-array v0, v0, [Lse;

    sget-object v1, Lse;->a:Lse;

    aput-object v1, v0, v2

    sget-object v1, Lse;->b:Lse;

    aput-object v1, v0, v3

    sget-object v1, Lse;->c:Lse;

    aput-object v1, v0, v4

    sget-object v1, Lse;->d:Lse;

    aput-object v1, v0, v5

    sput-object v0, Lse;->e:[Lse;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse;
    .locals 1

    const-class v0, Lse;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lse;

    return-object v0
.end method

.method public static values()[Lse;
    .locals 1

    sget-object v0, Lse;->e:[Lse;

    invoke-virtual {v0}, [Lse;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse;

    return-object v0
.end method
