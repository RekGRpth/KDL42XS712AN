.class public final Lelj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Ljava/io/File;

.field private final f:J

.field private final g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "storage_threshold_bytes"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lelj;->a:Ljava/lang/String;

    const-string v0, "storage_threshold_percent"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lelj;->b:Ljava/lang/String;

    const-string v0, "storage_compact_threshold"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lelj;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v2, p0, Lelj;->h:J

    iput-wide v2, p0, Lelj;->i:J

    iput-wide v2, p0, Lelj;->j:J

    iput-boolean v4, p0, Lelj;->k:Z

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Index file directory must be set"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "AppDataSearch"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path could not be created"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path is not a directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lelj;->d:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lelj;->e:Ljava/io/File;

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create directory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    iput-wide v0, p0, Lelj;->f:J

    iget-wide v0, p0, Lelj;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const-string v0, "There is no storage capacity, icing will not index"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_4
    iput-wide p4, p0, Lelj;->g:J

    const-string v0, "Storage manager: low %s usage %s avail %s capacity %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lelj;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lelj;->j()J

    move-result-wide v3

    invoke-static {v3, v4}, Lell;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v3

    invoke-static {v3, v4}, Lell;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v3, p0, Lelj;->f:J

    invoke-static {v3, v4}, Lell;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    return-void
.end method

.method private static a(Ljava/io/File;)J
    .locals 7

    const-wide/16 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_0

    array-length v6, v5

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v3, v5, v2

    invoke-static {v3}, Lelj;->a(Ljava/io/File;)J

    move-result-wide v3

    add-long/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v3

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method private l()V
    .locals 10

    const-wide/16 v8, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lelj;->h:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    iput-wide v0, p0, Lelj;->h:J

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-static {v0}, Lelj;->a(Ljava/io/File;)J

    move-result-wide v0

    iput-wide v0, p0, Lelj;->i:J

    invoke-virtual {p0}, Lelj;->c()Z

    move-result v0

    iput-boolean v0, p0, Lelj;->k:Z

    iget-boolean v0, p0, Lelj;->k:Z

    if-eqz v0, :cond_1

    iput-wide v8, p0, Lelj;->j:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lelj;->d:Landroid/content/Context;

    sget-object v1, Lelj;->a:Ljava/lang/String;

    const-wide/32 v2, 0x40000000

    invoke-static {v0, v1, v2, v3}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lelj;->d:Landroid/content/Context;

    sget-object v3, Lelj;->b:Ljava/lang/String;

    const-wide/16 v4, 0x1e

    invoke-static {v2, v3, v4, v5}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v4

    iget-wide v6, p0, Lelj;->i:J

    add-long/2addr v4, v6

    sub-long v0, v4, v0

    iget-wide v6, p0, Lelj;->f:J

    mul-long/2addr v2, v6

    const-wide/16 v6, 0x64

    div-long/2addr v2, v6

    sub-long v2, v4, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-wide/32 v2, 0x1400000

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iget-wide v2, p0, Lelj;->g:J

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lelj;->j:J

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-static {v0}, Lell;->a(Ljava/io/File;)V

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    return v0
.end method

.method public final a(Leit;)Z
    .locals 11

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    move v0, v1

    move-wide v2, v4

    move-wide v6, v4

    :goto_0
    iget-object v8, p1, Leit;->a:[Leiu;

    array-length v8, v8

    if-ge v0, v8, :cond_0

    iget-object v8, p1, Leit;->a:[Leiu;

    aget-object v8, v8, v0

    iget-wide v9, v8, Leiu;->d:J

    add-long/2addr v6, v9

    iget-wide v8, v8, Leiu;->e:J

    add-long/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-long/2addr v6, v2

    cmp-long v0, v6, v4

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    return v1

    :cond_2
    long-to-double v2, v2

    long-to-double v4, v6

    div-double/2addr v2, v4

    iget-object v0, p0, Lelj;->d:Landroid/content/Context;

    sget-object v4, Lelj;->c:Ljava/lang/String;

    const-wide/16 v5, 0xa

    invoke-static {v0, v4, v5, v6}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final b()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    return-object v0
.end method

.method final c()Z
    .locals 4

    iget-object v0, p0, Lelj;->d:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    invoke-direct {p0}, Lelj;->l()V

    iget-boolean v0, p0, Lelj;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    invoke-direct {p0}, Lelj;->l()V

    iget-wide v0, p0, Lelj;->i:J

    iget-wide v2, p0, Lelj;->j:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()D
    .locals 4

    invoke-direct {p0}, Lelj;->l()V

    iget-wide v0, p0, Lelj;->i:J

    iget-wide v2, p0, Lelj;->j:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lelj;->j:J

    iget-wide v2, p0, Lelj;->i:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    iget-wide v2, p0, Lelj;->j:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public final g()J
    .locals 2

    invoke-direct {p0}, Lelj;->l()V

    iget-wide v0, p0, Lelj;->i:J

    return-wide v0
.end method

.method public final h()J
    .locals 2

    invoke-direct {p0}, Lelj;->l()V

    iget-wide v0, p0, Lelj;->j:J

    return-wide v0
.end method

.method final i()J
    .locals 2

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v0

    return-wide v0
.end method

.method public final j()J
    .locals 2

    iget-object v0, p0, Lelj;->e:Ljava/io/File;

    invoke-static {v0}, Lelj;->a(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method final k()J
    .locals 2

    iget-wide v0, p0, Lelj;->f:J

    return-wide v0
.end method
