.class abstract Lfgs;
.super Lffb;
.source "SourceFile"


# instance fields
.field private final c:Lfbz;

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILfbz;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p6}, Lffb;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    iput-object p4, p0, Lfgs;->c:Lfbz;

    iput-boolean p5, p0, Lfgs;->d:Z

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)[Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public final b()V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lfgs;->a:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lfgs;->a(Landroid/content/Context;)[Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    sget-object v0, Lffc;->c:Lffc;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-boolean v4, p0, Lfgs;->d:Z

    if-eqz v4, :cond_1

    iget-object v2, p0, Lfgs;->c:Lfbz;

    iget v4, v0, Lffc;->a:I

    iget-object v0, v0, Lffc;->b:Landroid/os/Bundle;

    invoke-interface {v2, v4, v0, v3}, Lfbz;->a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v3, :cond_6

    iget-object v0, p0, Lfgs;->c:Lfbz;

    instance-of v0, v0, Lfca;

    if-nez v0, :cond_6

    array-length v0, v3

    :goto_2
    if-ge v1, v0, :cond_6

    aget-object v2, v3, v1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v3, "PeopleService"

    const-string v4, "Error during operation"

    invoke-static {v3, v4, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v3, p0, Lfgs;->a:Landroid/content/Context;

    iget-object v4, p0, Lffb;->b:Ljava/lang/String;

    invoke-static {v3, v0, v4}, Lfjv;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    sget-object v0, Lffc;->f:Lffc;

    move-object v3, v2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "PeopleService"

    const-string v4, "Error during operation"

    invoke-static {v3, v4, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v3, p0, Lfgs;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lfgs;->a()Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3, v0}, Lfjv;->a(Landroid/content/Context;Ljava/lang/Exception;)Lffc;

    move-result-object v0

    move-object v3, v2

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v4, p0, Lfgs;->c:Lfbz;

    iget v5, v0, Lffc;->a:I

    iget-object v6, v0, Lffc;->b:Landroid/os/Bundle;

    if-eqz v3, :cond_3

    const/4 v0, 0x0

    aget-object v0, v3, v0

    :goto_3
    invoke-interface {v4, v5, v6, v0}, Lfbz;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_3
    const-string v2, "PeopleService"

    const-string v4, "Client died?"

    invoke-static {v2, v4, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v3, :cond_6

    iget-object v0, p0, Lfgs;->c:Lfbz;

    instance-of v0, v0, Lfca;

    if-nez v0, :cond_6

    array-length v0, v3

    :goto_4
    if-ge v1, v0, :cond_6

    aget-object v2, v3, v1

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    move-object v0, v2

    goto :goto_3

    :catchall_0
    move-exception v0

    if-eqz v3, :cond_5

    iget-object v2, p0, Lfgs;->c:Lfbz;

    instance-of v2, v2, Lfca;

    if-nez v2, :cond_5

    array-length v2, v3

    :goto_5
    if-ge v1, v2, :cond_5

    aget-object v4, v3, v1

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    throw v0

    :cond_6
    return-void
.end method
