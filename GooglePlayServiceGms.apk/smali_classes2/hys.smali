.class public final Lhys;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field final c:Lbpe;

.field final d:Lhxw;

.field final e:Lhyr;

.field final f:Lhyi;

.field final g:Lhzk;

.field final h:Lhxq;

.field final i:Lhxq;

.field final j:Landroid/os/PowerManager$WakeLock;

.field final k:Lhyn;

.field final l:Lhyl;

.field final m:Landroid/content/pm/PackageManager;

.field final n:Landroid/content/Context;

.field private final o:I

.field private final p:Ljava/lang/String;

.field private final q:Lhzj;


# direct methods
.method public constructor <init>(ILbpe;Landroid/content/Context;Lhyt;Ljava/lang/Class;Lhyl;Lhzj;)V
    .locals 9

    const/16 v1, 0x64

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lhys;-><init>(ILbpe;Landroid/content/Context;Lhyt;Ljava/lang/Class;Lhyl;Lhzj;B)V

    return-void
.end method

.method private constructor <init>(ILbpe;Landroid/content/Context;Lhyt;Ljava/lang/Class;Lhyl;Lhzj;B)V
    .locals 6

    const v0, 0x7fffffff

    const/16 v1, 0x12

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lhys;->a:I

    iput v0, p0, Lhys;->b:I

    new-instance v0, Lhxw;

    invoke-direct {v0}, Lhxw;-><init>()V

    iput-object v0, p0, Lhys;->d:Lhxw;

    if-nez p7, :cond_1

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lhzj;->a()Lhzj;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lhys;->q:Lhzj;

    :goto_1
    if-nez p6, :cond_3

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lhzs;

    invoke-direct {v0, p4}, Lhzs;-><init>(Lhyt;)V

    :goto_2
    iput-object v0, p0, Lhys;->l:Lhyl;

    :goto_3
    iput-object p2, p0, Lhys;->c:Lbpe;

    new-instance v1, Lhyn;

    const-string v0, "power"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iget-object v2, p0, Lhys;->c:Lbpe;

    invoke-direct {v1, v0, v2}, Lhyn;-><init>(Landroid/os/PowerManager;Lbpe;)V

    iput-object v1, p0, Lhys;->k:Lhyn;

    const-string v0, "power"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "GeofencePendingIntentWakeLock"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    new-instance v0, Lhyi;

    iget-object v2, p0, Lhys;->k:Lhyn;

    new-instance v5, Lhzr;

    invoke-direct {v5}, Lhzr;-><init>()V

    move-object v1, p3

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lhyi;-><init>(Landroid/content/Context;Lhyn;Lbpe;Lhyt;Lhzr;)V

    iput-object v0, p0, Lhys;->f:Lhyi;

    new-instance v0, Lhzk;

    invoke-direct {v0, p3, p2, p4}, Lhzk;-><init>(Landroid/content/Context;Lbpe;Lhyt;)V

    iput-object v0, p0, Lhys;->g:Lhzk;

    new-instance v0, Lhyr;

    invoke-direct {v0, p3}, Lhyr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhys;->e:Lhyr;

    new-instance v0, Lhxq;

    iget-object v4, p0, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lhxq;-><init>(ILbpe;Landroid/content/Context;Landroid/os/PowerManager$WakeLock;Ljava/lang/Class;)V

    iput-object v0, p0, Lhys;->h:Lhxq;

    new-instance v0, Lhxq;

    invoke-direct {v0, p2, p3}, Lhxq;-><init>(Lbpe;Landroid/content/Context;)V

    iput-object v0, p0, Lhys;->i:Lhxq;

    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lhys;->m:Landroid/content/pm/PackageManager;

    invoke-virtual {p3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lhys;->o:I

    invoke-virtual {p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhys;->p:Ljava/lang/String;

    iput-object p3, p0, Lhys;->n:Landroid/content/Context;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_1
    iput-object p7, p0, Lhys;->q:Lhzj;

    goto/16 :goto_1

    :cond_2
    new-instance v0, Lhzq;

    invoke-direct {v0}, Lhzq;-><init>()V

    goto :goto_2

    :cond_3
    iput-object p6, p0, Lhys;->l:Lhyl;

    goto/16 :goto_3
.end method

.method private a(Ljava/util/Collection;Landroid/app/PendingIntent;)V
    .locals 4

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhys;->m:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    new-instance v2, Lild;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v2, v1, v0}, Lild;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GeofencerStateInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package not found: \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lild;

    iget v1, p0, Lhys;->o:I

    iget-object v2, p0, Lhys;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lild;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/app/PendingIntent;)I
    .locals 1

    iget-object v0, p0, Lhys;->i:Lhxq;

    invoke-virtual {v0, p1}, Lhxq;->a(Landroid/app/PendingIntent;)I

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, p1}, Lhxq;->a(Landroid/app/PendingIntent;)I

    move-result v0

    return v0
.end method

.method final a([Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lhys;->i:Lhxq;

    invoke-virtual {v0, v1, p2}, Lhxq;->a(Ljava/util/List;Ljava/lang/String;)I

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, v1, p2}, Lhxq;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method final a(Landroid/util/Pair;ZD)Lhxn;
    .locals 7

    if-eqz p2, :cond_0

    iget-object v4, p0, Lhys;->d:Lhxw;

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lhys;->h:Lhxq;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/location/Location;

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lhxq;->a(JLandroid/location/Location;D)Lhxn;

    move-result-object v0

    invoke-virtual {p0, p3, p4}, Lhys;->b(D)V

    return-object v0

    :cond_1
    iput-object p1, v4, Lhxw;->c:Landroid/util/Pair;

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v3, v0

    :goto_1
    if-nez v3, :cond_3

    const-wide/16 v0, -0x1

    move-wide v1, v0

    :goto_2
    if-eqz v3, :cond_6

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v0, v5, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v5, 0x2710

    cmp-long v0, v0, v5

    if-gez v0, :cond_6

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_4

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationHistory"

    const-string v1, "Location comming too fast, dropping the new one."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    iget-object v1, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v3, v0

    goto :goto_1

    :cond_3
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-wide v1, v0

    goto :goto_2

    :cond_4
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "LocationHistory"

    const-string v1, "Location comming too fast, dropping the last one."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    :cond_6
    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, v4, Lhxw;->a:I

    if-lt v0, v1, :cond_7

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    :cond_7
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_8

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v2, v5

    if-gez v0, :cond_8

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    :cond_8
    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(D)Ljava/util/Collection;
    .locals 9

    const/16 v1, 0x64

    const/4 v5, 0x0

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, p1, p2, v1}, Lhxq;->a(DI)Ljava/util/List;

    move-result-object v3

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, v1}, Lhxq;->a(I)Ljava/util/List;

    move-result-object v4

    invoke-static {v3}, Lhys;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4}, Lhys;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v4}, Lhys;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0, p1, p2}, Lhxp;->a(D)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lhxp;

    iget-object v0, v0, Lhxp;->b:Landroid/app/PendingIntent;

    invoke-direct {p0, v2, v0}, Lhys;->a(Ljava/util/Collection;Landroid/app/PendingIntent;)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x3c

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v5, v0

    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0, p1, p2}, Lhxp;->a(D)I

    move-result v3

    int-to-long v7, v3

    cmp-long v3, v7, v5

    if-gtz v3, :cond_1

    iget-object v0, v0, Lhxp;->b:Landroid/app/PendingIntent;

    invoke-direct {p0, v2, v0}, Lhys;->a(Ljava/util/Collection;Landroid/app/PendingIntent;)V

    goto :goto_2

    :cond_2
    invoke-static {v3}, Lhys;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_3
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhxp;

    invoke-virtual {v0, p1, p2}, Lhxp;->a(D)I

    move-result v5

    invoke-virtual {v1}, Lhxp;->c()I

    move-result v6

    if-ge v5, v6, :cond_4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :cond_5
    if-eqz v4, :cond_7

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->c()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v3, v5

    if-gtz v3, :cond_6

    iget-object v0, v0, Lhxp;->b:Landroid/app/PendingIntent;

    invoke-direct {p0, v2, v0}, Lhys;->a(Ljava/util/Collection;Landroid/app/PendingIntent;)V

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto/16 :goto_0
.end method

.method final a(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lhys;->k:Lhyn;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Lhyn;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v4}, Lhyn;->a(Z)V

    goto :goto_0

    :cond_2
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v4}, Lhyn;->b(Z)V

    goto :goto_0

    :cond_3
    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lhyn;->b(Z)V

    goto :goto_0
.end method

.method final a()Z
    .locals 2

    iget v0, p0, Lhys;->a:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Collection;
    .locals 5

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0}, Lhxq;->e()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    iget-object v3, p0, Lhys;->m:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    new-instance v4, Lild;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v4, v3, v0}, Lild;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method final b(D)V
    .locals 7

    const/4 v5, 0x0

    const v1, 0x7fffffff

    const/4 v4, 0x1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateMinLocationUpdateRate: velocityMetersPerSec="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, p1, p2, v4}, Lhxq;->a(DI)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_4

    :cond_1
    move v0, v1

    :goto_0
    iput v0, p0, Lhys;->a:I

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0, v4}, Lhxq;->a(I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_5

    :cond_2
    move v0, v1

    :goto_1
    iput v0, p0, Lhys;->b:I

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lhys;->a:I

    if-ne v0, v1, :cond_6

    const-string v0, "UNKNOWN"

    :goto_2
    iget v4, p0, Lhys;->b:I

    if-ne v4, v1, :cond_7

    const-string v1, "UNKNOWN"

    :goto_3
    const-string v4, "GeofencerStateInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Geofence with min crossing rate: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; Rate="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "GeofencerStateInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Geofence with min dwelling rate: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; Rate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0, p1, p2}, Lhxp;->a(D)I

    move-result v0

    goto :goto_0

    :cond_5
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->c()I

    move-result v0

    goto :goto_1

    :cond_6
    iget v0, p0, Lhys;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    iget v1, p0, Lhys;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method final c()Z
    .locals 1

    iget-object v0, p0, Lhys;->h:Lhxq;

    invoke-virtual {v0}, Lhxq;->d()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c(D)Z
    .locals 2

    iget v0, p0, Lhys;->a:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhys;->a:I

    int-to-double v0, v0

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    const/16 v0, 0x9

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    :cond_0
    iget-object v0, p0, Lhys;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method final e()V
    .locals 4

    iget-object v0, p0, Lhys;->f:Lhyi;

    iget-object v1, v0, Lhyi;->g:Lhzo;

    invoke-virtual {v1}, Lhzo;->a()Lhye;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v0, v0, Lhyi;->i:Liln;

    invoke-virtual {v0, v1}, Liln;->b(Lizk;)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityDetector"

    const-string v1, "saveState: Activity history written."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "ActivityDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "saveState: Unable to save activity history - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final f()Lhzp;
    .locals 1

    iget-object v0, p0, Lhys;->f:Lhyi;

    iget-object v0, v0, Lhyi;->g:Lhzo;

    invoke-virtual {v0}, Lhzo;->c()Lhzp;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 2

    iget-object v1, p0, Lhys;->l:Lhyl;

    iget-object v0, p0, Lhys;->q:Lhzj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lhyl;->a(Lhyo;)V

    return-void

    :cond_0
    iget-object v0, p0, Lhys;->q:Lhzj;

    invoke-virtual {v0}, Lhzj;->b()Lhyo;

    move-result-object v0

    goto :goto_0
.end method
