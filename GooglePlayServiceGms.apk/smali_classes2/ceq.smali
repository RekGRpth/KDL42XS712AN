.class public final enum Lceq;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lceq;

.field public static final enum b:Lceq;

.field public static final enum c:Lceq;

.field public static final enum d:Lceq;

.field private static final synthetic f:[Lceq;


# instance fields
.field private final e:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    new-instance v0, Lceq;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcep;->d()Lcep;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "accountId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lceq;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceq;->a:Lceq;

    new-instance v0, Lceq;

    const-string v1, "PAYLOAD"

    invoke-static {}, Lcep;->d()Lcep;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "payload"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lceq;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceq;->b:Lceq;

    new-instance v0, Lceq;

    const-string v1, "RETRY_COUNT"

    invoke-static {}, Lcep;->d()Lcep;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "retryCount"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lceq;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceq;->c:Lceq;

    new-instance v0, Lceq;

    const-string v1, "REQUIRED_ENTRY_ID"

    invoke-static {}, Lcep;->d()Lcep;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "requiredEntryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lceq;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lceq;->d:Lceq;

    const/4 v0, 0x4

    new-array v0, v0, [Lceq;

    sget-object v1, Lceq;->a:Lceq;

    aput-object v1, v0, v8

    sget-object v1, Lceq;->b:Lceq;

    aput-object v1, v0, v6

    sget-object v1, Lceq;->c:Lceq;

    aput-object v1, v0, v7

    sget-object v1, Lceq;->d:Lceq;

    aput-object v1, v0, v9

    sput-object v0, Lceq;->f:[Lceq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lceq;->e:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lceq;
    .locals 1

    const-class v0, Lceq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lceq;

    return-object v0
.end method

.method public static values()[Lceq;
    .locals 1

    sget-object v0, Lceq;->f:[Lceq;

    invoke-virtual {v0}, [Lceq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lceq;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lceq;->e:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lceq;->e:Lcdp;

    return-object v0
.end method
