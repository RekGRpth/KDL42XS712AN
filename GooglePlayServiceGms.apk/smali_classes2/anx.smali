.class public final Lanx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Lanx;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lano;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lanx;->a:Landroid/content/Context;

    new-instance v0, Lano;

    invoke-direct {v0, p1}, Lano;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lanx;->b:Lano;

    return-void
.end method

.method public static declared-synchronized a()Lanx;
    .locals 3

    const-class v1, Lanx;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lanx;->c:Lanx;

    if-nez v0, :cond_0

    new-instance v0, Lanx;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lanx;-><init>(Landroid/content/Context;)V

    sput-object v0, Lanx;->c:Lanx;

    :cond_0
    sget-object v0, Lanx;->c:Lanx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/security/KeyPair;)Z
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v1

    instance-of v2, v1, Ljava/security/interfaces/ECPublicKey;

    if-eqz v2, :cond_0

    sget-object v1, Ljcr;->b:Ljcr;

    :goto_0
    invoke-static {}, Ljci;->a()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Ljcr;->c:Ljcr;

    if-ne v1, v2, :cond_2

    :goto_1
    return v0

    :cond_0
    instance-of v1, v1, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v1, :cond_1

    sget-object v1, Ljcr;->c:Ljcr;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/security/InvalidKeyException;

    const-string v2, "Unsupported key type"

    invoke-direct {v1, v2}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "AuthZen"

    const-string v3, "Signing Key found to be invalid"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lany;)V
    .locals 6

    iget-object v0, p0, Lanx;->a:Landroid/content/Context;

    invoke-static {v0}, Lanh;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "AuthZen"

    const-string v1, "Authzen is disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lanx;->a:Landroid/content/Context;

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    const-string v0, "AuthZen"

    const-string v1, "Fetching signing key..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lanx;->b:Lano;

    const-string v1, "device_key"

    invoke-virtual {v0, v1}, Lano;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v1

    invoke-static {v1}, Lanx;->a(Ljava/security/KeyPair;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "AuthZen"

    const-string v2, "SigningKey found depricated. Wiping all keys..."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lanx;->b:Lano;

    iget-object v2, v0, Lano;->b:Lanr;

    const-string v3, "AuthZen"

    const-string v4, "Deleting all SigningKeys"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "signingkeys"

    invoke-virtual {v2, v3}, Lanr;->c(Ljava/lang/String;)I

    move-result v2

    const-string v3, "AuthZen"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "AuthZen"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Count of SigningKeys deleted: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, v0, Lano;->b:Lanr;

    const-string v2, "AuthZen"

    const-string v3, "Deleting all Encryption Keys"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "encryptionkeys"

    invoke-virtual {v0, v2}, Lanr;->c(Ljava/lang/String;)I

    move-result v0

    const-string v2, "AuthZen"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Count of Encryption keys deleted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v0, "AuthZen"

    const-string v2, "Signing key fetched successfuly!"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lanx;->a:Landroid/content/Context;

    iget-object v2, p0, Lanx;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, p1, v0, v1}, Lanx;->a(Lany;Landroid/accounts/Account;Ljava/security/KeyPair;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lanp; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    const-string v2, "Error while fetching key."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "AuthZen"

    const-string v2, "Error while creating key."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method public final a(Lany;Landroid/accounts/Account;Ljava/security/KeyPair;)V
    .locals 4

    sget-object v0, Lany;->a:Lany;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lanx;->b:Lano;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, v0, Lano;->b:Lanr;

    invoke-virtual {v0, v1}, Lanr;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Too many encryption keys for the same account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanj;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v0, Lanj;->a:Lank;

    iget-wide v0, v0, Lank;->d:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No encryption key found or existing keys are expired for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Lanu;

    iget-object v1, p0, Lanx;->a:Landroid/content/Context;

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lanx;->b:Lano;

    invoke-direct {v0, v1, v2, p3, v3}, Lanu;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/security/KeyPair;Lano;)V

    :try_start_0
    const-string v1, "AuthZen"

    const-string v2, "Starting Enrollment..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lanu;->a()Lanj;

    move-result-object v0

    const-string v1, "AuthZen"

    const-string v2, "Enrollment successful!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lanj;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lanv; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to do enrollment for account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
