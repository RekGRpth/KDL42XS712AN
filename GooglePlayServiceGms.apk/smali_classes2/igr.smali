.class public final Ligr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lifw;

.field public final b:Ligp;

.field public final c:Ljava/util/List;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/HandlerThread;

.field private final f:Ligs;

.field private final g:Ligc;

.field private final h:Lifu;

.field private final i:Lilo;

.field private final j:Ligf;

.field private final k:Lhvb;

.field private final l:Lfkq;

.field private final m:Ljava/lang/Object;

.field private n:J

.field private final o:Landroid/location/LocationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhvb;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PlacesHelper"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ligr;->e:Landroid/os/HandlerThread;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ligr;->m:Ljava/lang/Object;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Ligr;->n:J

    iput-object p1, p0, Ligr;->d:Landroid/content/Context;

    iget-object v0, p0, Ligr;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Ligs;

    iget-object v1, p0, Ligr;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Ligs;-><init>(Landroid/os/Looper;Ligr;)V

    iput-object v0, p0, Ligr;->f:Ligs;

    new-instance v0, Ligc;

    invoke-direct {v0}, Ligc;-><init>()V

    iput-object v0, p0, Ligr;->g:Ligc;

    new-instance v0, Lifu;

    invoke-direct {v0}, Lifu;-><init>()V

    iput-object v0, p0, Ligr;->h:Lifu;

    new-instance v0, Lilo;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-direct {v0, v1}, Lilo;-><init>(Lbpe;)V

    iput-object v0, p0, Ligr;->i:Lilo;

    iget-object v0, p0, Ligr;->d:Landroid/content/Context;

    iget-object v1, p0, Ligr;->f:Ligs;

    iget-object v2, p0, Ligr;->g:Ligc;

    iget-object v3, p0, Ligr;->i:Lilo;

    invoke-static {v0, v1, v2, v3}, Lifw;->a(Landroid/content/Context;Landroid/os/Handler;Ligc;Lilo;)Lifw;

    move-result-object v0

    iput-object v0, p0, Ligr;->a:Lifw;

    new-instance v0, Ligf;

    iget-object v1, p0, Ligr;->g:Ligc;

    iget-object v2, p0, Ligr;->h:Lifu;

    iget-object v3, p0, Ligr;->a:Lifw;

    iget-object v4, p0, Ligr;->i:Lilo;

    invoke-direct {v0, v1, v2, v3, v4}, Ligf;-><init>(Ligc;Lifu;Lifw;Lilo;)V

    iput-object v0, p0, Ligr;->j:Ligf;

    new-instance v0, Ligp;

    iget-object v1, p0, Ligr;->j:Ligf;

    iget-object v2, p0, Ligr;->f:Ligs;

    invoke-direct {v0, p1, p2, v1, v2}, Ligp;-><init>(Landroid/content/Context;Lhvb;Ligf;Landroid/os/Handler;)V

    iput-object v0, p0, Ligr;->b:Ligp;

    invoke-static {p1}, Lhvb;->a(Landroid/content/Context;)Lhvb;

    move-result-object v0

    iput-object v0, p0, Ligr;->k:Lhvb;

    const/4 v0, 0x0

    iput-object v0, p0, Ligr;->l:Lfkq;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Ligr;->o:Landroid/location/LocationManager;

    sget-object v0, Lhjh;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ligr;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    iget-object v1, p0, Ligr;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-object v0, Lhjh;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    iget-wide v6, p0, Ligr;->n:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iput-wide v2, p0, Ligr;->n:J

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
