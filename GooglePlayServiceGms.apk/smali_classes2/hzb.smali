.class final Lhzb;
.super Lhyy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhzb;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyy;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "HasGeofenceState"

    return-object v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1}, Lhzb;->b(Landroid/os/Message;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lhzb;->a:Lhyt;

    invoke-static {v1}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v1

    invoke-virtual {v1}, Lhzn;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GeofencerStateMachine"

    const-string v2, "Network location disabled."

    invoke-static {v1, v2}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lhzb;->a:Lhyt;

    iget-object v2, p0, Lhzb;->a:Lhyt;

    invoke-static {v2}, Lhyt;->n(Lhyt;)Lhyz;

    move-result-object v2

    invoke-static {v1, v2}, Lhyt;->g(Lhyt;Lbqg;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lhzb;->a:Lhyt;

    invoke-static {v1, p1}, Lhyt;->b(Lhyt;Landroid/os/Message;)V

    iget-object v1, p0, Lhzb;->a:Lhyt;

    iget-object v2, p0, Lhzb;->c:Lhys;

    invoke-virtual {v2}, Lhys;->f()Lhzp;

    move-result-object v2

    invoke-virtual {p0, v2}, Lhzb;->a(Lhzp;)Lbqh;

    move-result-object v2

    invoke-static {v1, v2}, Lhyt;->h(Lhyt;Lbqg;)V

    iget-object v1, p0, Lhzb;->a:Lhyt;

    invoke-static {v1}, Lhyt;->o(Lhyt;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
    .end sparse-switch
.end method
