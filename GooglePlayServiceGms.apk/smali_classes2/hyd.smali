.class public final Lhyd;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:J

.field public c:Z

.field public d:J

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-wide v0, p0, Lhyd;->b:J

    iput-wide v0, p0, Lhyd;->d:J

    const/4 v0, -0x1

    iput v0, p0, Lhyd;->e:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lhyd;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lhyd;->b()I

    :cond_0
    iget v0, p0, Lhyd;->e:I

    return v0
.end method

.method public final a(J)Lhyd;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyd;->a:Z

    iput-wide p1, p0, Lhyd;->b:J

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyd;->a(J)Lhyd;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lhyd;->b(J)Lhyd;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Lhyd;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lhyd;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_0
    iget-boolean v0, p0, Lhyd;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lhyd;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhyd;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lhyd;->b:J

    invoke-static {v0, v1, v2}, Lizh;->c(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lhyd;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lhyd;->d:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lhyd;->e:I

    return v0
.end method

.method public final b(J)Lhyd;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyd;->c:Z

    iput-wide p1, p0, Lhyd;->d:J

    return-object p0
.end method
