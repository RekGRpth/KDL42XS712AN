.class public abstract Ldwz;
.super Ldwx;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f09000f    # com.google.android.gms.R.integer.games_mixed_tile_num_columns

    invoke-direct {p0, p1, v0}, Ldwx;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Ldwx;-><init>(Landroid/content/Context;II)V

    return-void
.end method


# virtual methods
.method public varargs a([Lbgo;)V
    .locals 4

    new-instance v1, Lbgn;

    invoke-direct {v1}, Lbgn;-><init>()V

    const/4 v0, 0x0

    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lbgn;->a(Lbgo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, v1}, Ldwx;->a(Lbgo;)V

    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 2

    invoke-virtual {p0}, Ldwz;->e()Lbgo;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lbgn;

    if-eqz v1, :cond_0

    check-cast v0, Lbgn;

    invoke-virtual {v0}, Lbgn;->d()V

    :cond_0
    invoke-super {p0}, Ldwx;->notifyDataSetChanged()V

    return-void
.end method
