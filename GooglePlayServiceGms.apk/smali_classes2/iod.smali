.class public final Liod;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile k:[Liod;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lioh;

.field public d:Lioh;

.field public e:I

.field public f:Liop;

.field public g:Lioh;

.field public h:I

.field public i:Z

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Liod;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liod;->b:Ljava/lang/String;

    iput-object v1, p0, Liod;->c:Lioh;

    iput-object v1, p0, Liod;->d:Lioh;

    iput v2, p0, Liod;->e:I

    iput-object v1, p0, Liod;->f:Liop;

    iput-object v1, p0, Liod;->g:Lioh;

    iput v2, p0, Liod;->h:I

    iput-boolean v2, p0, Liod;->i:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liod;->j:J

    const/4 v0, -0x1

    iput v0, p0, Liod;->C:I

    return-void
.end method

.method public static c()[Liod;
    .locals 2

    sget-object v0, Liod;->k:[Liod;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Liod;->k:[Liod;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Liod;

    sput-object v0, Liod;->k:[Liod;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Liod;->k:[Liod;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Liod;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Liod;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Liod;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Liod;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Liod;->c:Lioh;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Liod;->c:Lioh;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Liod;->f:Liop;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Liod;->f:Liop;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Liod;->d:Lioh;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Liod;->d:Lioh;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Liod;->e:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Liod;->e:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Liod;->g:Lioh;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Liod;->g:Lioh;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Liod;->h:I

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Liod;->h:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Liod;->i:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Liod;->i:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-wide v1, p0, Liod;->j:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-wide v2, p0, Liod;->j:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Liod;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liod;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liod;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liod;->c:Lioh;

    if-nez v0, :cond_1

    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    iput-object v0, p0, Liod;->c:Lioh;

    :cond_1
    iget-object v0, p0, Liod;->c:Lioh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Liod;->f:Liop;

    if-nez v0, :cond_2

    new-instance v0, Liop;

    invoke-direct {v0}, Liop;-><init>()V

    iput-object v0, p0, Liod;->f:Liop;

    :cond_2
    iget-object v0, p0, Liod;->f:Liop;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Liod;->d:Lioh;

    if-nez v0, :cond_3

    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    iput-object v0, p0, Liod;->d:Lioh;

    :cond_3
    iget-object v0, p0, Liod;->d:Lioh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Liod;->e:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Liod;->g:Lioh;

    if-nez v0, :cond_4

    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    iput-object v0, p0, Liod;->g:Lioh;

    :cond_4
    iget-object v0, p0, Liod;->g:Lioh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Liod;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Liod;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Liod;->j:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-object v0, p0, Liod;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liod;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Liod;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liod;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Liod;->c:Lioh;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liod;->c:Lioh;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Liod;->f:Liop;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Liod;->f:Liop;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    iget-object v0, p0, Liod;->d:Lioh;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Liod;->d:Lioh;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    iget v0, p0, Liod;->e:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Liod;->e:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_5
    iget-object v0, p0, Liod;->g:Lioh;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Liod;->g:Lioh;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_6
    iget v0, p0, Liod;->h:I

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Liod;->h:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_7
    iget-boolean v0, p0, Liod;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Liod;->i:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_8
    iget-wide v0, p0, Liod;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-wide v1, p0, Liod;->j:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_9
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liod;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liod;

    iget-object v2, p0, Liod;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Liod;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Liod;->a:Ljava/lang/String;

    iget-object v3, p1, Liod;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liod;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Liod;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Liod;->b:Ljava/lang/String;

    iget-object v3, p1, Liod;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Liod;->c:Lioh;

    if-nez v2, :cond_7

    iget-object v2, p1, Liod;->c:Lioh;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Liod;->c:Lioh;

    iget-object v3, p1, Liod;->c:Lioh;

    invoke-virtual {v2, v3}, Lioh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Liod;->d:Lioh;

    if-nez v2, :cond_9

    iget-object v2, p1, Liod;->d:Lioh;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Liod;->d:Lioh;

    iget-object v3, p1, Liod;->d:Lioh;

    invoke-virtual {v2, v3}, Lioh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Liod;->e:I

    iget v3, p1, Liod;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Liod;->f:Liop;

    if-nez v2, :cond_c

    iget-object v2, p1, Liod;->f:Liop;

    if-eqz v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Liod;->f:Liop;

    iget-object v3, p1, Liod;->f:Liop;

    invoke-virtual {v2, v3}, Liop;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Liod;->g:Lioh;

    if-nez v2, :cond_e

    iget-object v2, p1, Liod;->g:Lioh;

    if-eqz v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Liod;->g:Lioh;

    iget-object v3, p1, Liod;->g:Lioh;

    invoke-virtual {v2, v3}, Lioh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget v2, p0, Liod;->h:I

    iget v3, p1, Liod;->h:I

    if-eq v2, v3, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-boolean v2, p0, Liod;->i:Z

    iget-boolean v3, p1, Liod;->i:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-wide v2, p0, Liod;->j:J

    iget-wide v4, p1, Liod;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Liod;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liod;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liod;->c:Lioh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liod;->d:Lioh;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Liod;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liod;->f:Liop;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liod;->g:Lioh;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Liod;->h:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Liod;->i:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x4cf

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Liod;->j:J

    iget-wide v3, p0, Liod;->j:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Liod;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Liod;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Liod;->c:Lioh;

    invoke-virtual {v0}, Lioh;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Liod;->d:Lioh;

    invoke-virtual {v0}, Lioh;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Liod;->f:Liop;

    invoke-virtual {v0}, Liop;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Liod;->g:Lioh;

    invoke-virtual {v1}, Lioh;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    const/16 v0, 0x4d5

    goto :goto_6
.end method
