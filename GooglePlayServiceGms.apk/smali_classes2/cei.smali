.class final Lcei;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcek;

.field c:Lcdt;

.field d:Lcdp;

.field e:Z

.field final f:Ljava/util/Set;

.field g:Z

.field h:Ljava/lang/Object;

.field i:Lcej;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcek;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcei;->c:Lcdt;

    iput-object v1, p0, Lcei;->d:Lcdp;

    iput-boolean v2, p0, Lcei;->e:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcei;->f:Ljava/util/Set;

    iput-boolean v2, p0, Lcei;->g:Z

    iput-object v1, p0, Lcei;->h:Ljava/lang/Object;

    sget-object v0, Lcej;->a:Lcej;

    iput-object v0, p0, Lcei;->i:Lcej;

    iput-object p1, p0, Lcei;->a:Ljava/lang/String;

    iput-object p2, p0, Lcei;->b:Lcek;

    return-void
.end method

.method private b()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcei;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcei;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    const-string v0, "FieldDefinition"

    const-string v1, "Ignoring isIndexed constraint as field also has uniqueness constraint (on just this field, and therefore SQLite will have to create an index on that. For field: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iput-boolean v3, p0, Lcei;->e:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcei;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcei;->e:Z

    invoke-direct {p0}, Lcei;->b()V

    return-object p0
.end method

.method public final a(Lcdt;Lcdp;)Lcei;
    .locals 1

    sget-object v0, Lcej;->a:Lcej;

    invoke-virtual {p0, p1, p2, v0}, Lcei;->a(Lcdt;Lcdp;Lcej;)Lcei;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcdt;Lcdp;Lcej;)Lcei;
    .locals 0

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcei;->c:Lcdt;

    iput-object p2, p0, Lcei;->d:Lcdp;

    iput-object p3, p0, Lcei;->i:Lcej;

    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lcei;
    .locals 2

    iget-object v0, p0, Lcei;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "defaultValue already set"

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    const-string v0, "null defaultValue"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcei;->h:Ljava/lang/Object;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([Ljava/lang/String;)Lcei;
    .locals 2

    iget-object v0, p0, Lcei;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lcei;->f:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcei;->f:Ljava/util/Set;

    iget-object v1, p0, Lcei;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcei;->b()V

    return-object p0
.end method
