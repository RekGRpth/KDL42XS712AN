.class final Lfbp;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfbo;


# direct methods
.method public constructor <init>(Lfbo;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lfbp;->a:Lfbo;

    const/4 v0, 0x0

    const/16 v1, 0xc9

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    const-string v0, "DELETE FROM sqlite_stat1 WHERE tbl=? AND idx IS NULL"

    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const-string v0, "INSERT INTO sqlite_stat1 (tbl,idx,stat) VALUES (?,?,?)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    aput-object p3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "DELETE FROM sqlite_stat1 WHERE tbl=? AND idx=?"

    new-array v1, v4, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v0, "DELETE FROM sqlite_stat1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "people"

    const/4 v1, 0x0

    const-string v2, "500"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owners"

    const/4 v1, 0x0

    const-string v2, "3"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_sync_requests"

    const/4 v1, 0x0

    const-string v2, "3"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sync_tokens"

    const/4 v1, 0x0

    const-string v2, "15"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_members"

    const/4 v1, 0x0

    const-string v2, "1000"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circles"

    const/4 v1, 0x0

    const-string v2, "20"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_index"

    const-string v1, "search_value"

    const-string v2, "1500 3"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_index"

    const-string v1, "search_person_id_index"

    const-string v2, "1500 3"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "emails"

    const-string v1, "email_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "phones"

    const-string v1, "phone_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "postal_address"

    const-string v1, "postal_address_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_emails"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_phones"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_postal_address"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "properties"

    const/4 v1, 0x0

    const-string v2, "10"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "gaia_id_map"

    const/4 v1, 0x0

    const-string v2, "900"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "applications"

    const/4 v1, 0x0

    const-string v2, "30"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "application_packages"

    const/4 v1, 0x0

    const-string v2, "45"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "facl_people"

    const/4 v1, 0x0

    const-string v2, "4000"

    invoke-static {p0, v0, v1, v2}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ANALYZE sqlite_master;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Could not update index stats"

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "Wiping the database..."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS owner_emails;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS owner_phones;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS owner_postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS owner_sync_requests;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_application;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS owners;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS search_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS emails;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS phones;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS circle_members;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS sync_tokens;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS properties;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS email_gaia_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE search_index (person_id INTEGER NOT NULL,kind INTEGER NOT NULL,value TEXT NOT NULL,FOREIGN KEY (person_id) REFERENCES people(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS search_value ON search_index (value);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS search_person_id_index ON search_index (person_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE owners (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,gaia_id TEXT,page_gaia_id TEXT,display_name TEXT,avatar TEXT,cover_photo_url TEXT,cover_photo_height INTEGER NOT NULL DEFAULT 0,cover_photo_width INTEGER NOT NULL DEFAULT 0,cover_photo_id TEXT,last_sync_start_time INTEGER NOT NULL DEFAULT 0,last_sync_finish_time INTEGER NOT NULL DEFAULT 0,last_sync_status INTEGER NOT NULL DEFAULT 0,last_successful_sync_time INTEGER NOT NULL DEFAULT 0,sync_to_contacts INTEGER NOT NULL DEFAULT 0,has_people INTEGER NOT NULL DEFAULT 0,is_dasher INTEGER NOT NULL DEFAULT 0 ,dasher_domain TEXT,etag TEXT,sync_circles_to_contacts INTEGER NOT NULL DEFAULT 0,sync_evergreen_to_contacts INTEGER NOT NULL DEFAULT 0,last_full_people_sync_time INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_sync_requests (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,page_gaia_id TEXT,sync_requested_time INTEGER NOT NULL DEFAULT 0,UNIQUE (account_name,page_gaia_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,gaia_id TEXT,v2_id TEXT NOT NULL,name TEXT,given_name TEXT,family_name TEXT,middle_name TEXT,name_verified INTEGER NOT NULL DEFAULT 0,profile_type INTEGER NOT NULL,sort_key TEXT,sort_key_last_name TEXT,sort_key_irank TEXT,avatar TEXT,tagline TEXT,blocked INTEGER NOT NULL DEFAULT 0,etag TEXT,last_modified INTEGER NOT NULL DEFAULT 0,invisible_3p INTEGER NOT NULL DEFAULT 0,in_viewer_domain INTEGER NOT NULL DEFAULT 0 ,in_circle INTEGER NOT NULL DEFAULT 0,in_contacts INTEGER NOT NULL DEFAULT 0,UNIQUE (owner_id,qualified_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE sync_tokens (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,name TEXT NOT NULL,value TEXT NOT NULL,UNIQUE (owner_id,name),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE circles (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,circle_id TEXT NOT NULL,name TEXT,sort_key TEXT,type INTEGER NOT NULL,for_sharing INTEGER NOT NULL DEFAULT 0,people_count INTEGER NOT NULL DEFAULT -1,client_policies INTEGER NOT NULL DEFAULT 0,etag TEXT,last_modified INTEGER NOT NULL DEFAULT 0,sync_to_contacts INTEGER NOT NULL DEFAULT 0,UNIQUE (owner_id,circle_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS email_person ON emails (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS phone_person ON phones (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS postal_address_person ON postal_address (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE circle_members (owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,circle_id TEXT NOT NULL,PRIMARY KEY (owner_id,qualified_id,circle_id),FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id,circle_id) REFERENCES circles(owner_id,circle_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE gaia_id_map (owner_id INTEGER NOT NULL,contact_id TEXT NOT NULL,value TEXT NOT NULL,gaia_id TEXT NOT NULL,type INTEGER NOT NULL,UNIQUE (owner_id,contact_id,value), FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,UNIQUE (owner_id,dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,FOREIGN KEY (owner_id, dev_console_id)REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id,dev_console_id,qualified_id) ON CONFLICT IGNORE,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Lfbp;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v0, "CREATE TABLE properties (name TEXT NOT NULL PRIMARY KEY,value TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ANALYZE;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Lfbp;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    invoke-virtual {p0, p1}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p1, v0}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/database/sqlite/SQLiteOpenHelper;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 9

    const/16 v3, 0x22

    const/16 v0, 0x21

    const/16 v4, 0x20

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v5, "PeopleDatabaseHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Upgrading from version "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-ge p2, v4, :cond_1c

    :try_start_0
    invoke-static {p1}, Lfbp;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_0
    if-ge v4, v0, :cond_1b

    :goto_1
    if-ge v0, v3, :cond_1a

    invoke-static {p1}, Lfbo;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_2
    const/16 v0, 0x23

    if-ge v3, v0, :cond_19

    invoke-static {p1}, Lfbo;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x23

    move v0, v1

    :goto_3
    const/16 v4, 0x24

    if-ge v3, v4, :cond_0

    invoke-static {p1}, Lfbo;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x24

    :cond_0
    const/16 v4, 0x25

    if-ge v3, v4, :cond_1

    invoke-static {p1}, Lfbo;->g(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x25

    move v2, v1

    :cond_1
    const/16 v4, 0x26

    if-ge v3, v4, :cond_2

    invoke-static {p1}, Lfbo;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x26

    :cond_2
    const/16 v4, 0x27

    if-ge v3, v4, :cond_3

    invoke-static {p1}, Lfbo;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x27

    :cond_3
    const/16 v4, 0x28

    if-ge v3, v4, :cond_4

    invoke-static {p1}, Lfbo;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x28

    :cond_4
    const/16 v4, 0x29

    if-ge v3, v4, :cond_5

    invoke-static {p1}, Lfbo;->k(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x29

    :cond_5
    const/16 v4, 0x2a

    if-ge v3, v4, :cond_6

    invoke-static {p1}, Lfbo;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2a

    :cond_6
    const/16 v4, 0x2c

    if-ge v3, v4, :cond_7

    iget-object v3, p0, Lfbp;->a:Lfbo;

    invoke-static {v3}, Lfbo;->a(Lfbo;)Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v3}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    const/16 v3, 0x2c

    :cond_7
    const/16 v4, 0x2d

    if-ge v3, v4, :cond_8

    invoke-static {p1}, Lfbo;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2d

    :cond_8
    const/16 v4, 0x2e

    if-ge v3, v4, :cond_9

    invoke-static {p1}, Lfbo;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2e

    :cond_9
    const/16 v4, 0x2f

    if-ge v3, v4, :cond_a

    invoke-static {p1}, Lfbo;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2f

    :cond_a
    const/16 v4, 0x64

    if-ge v3, v4, :cond_b

    invoke-static {p1}, Lfbo;->n(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x64

    move v2, v1

    :cond_b
    const/16 v4, 0x65

    if-ge v3, v4, :cond_c

    invoke-static {p1}, Lfbo;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x65

    move v2, v1

    :cond_c
    const/16 v4, 0x66

    if-ge v3, v4, :cond_d

    invoke-static {p1}, Lfbo;->p(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x66

    :cond_d
    const/16 v4, 0x67

    if-ge v3, v4, :cond_e

    invoke-static {p1}, Lfbo;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x67

    :cond_e
    const/16 v4, 0x68

    if-ge v3, v4, :cond_f

    const/16 v3, 0x68

    :cond_f
    const/16 v4, 0x69

    if-ge v3, v4, :cond_18

    invoke-static {p1}, Lfbo;->r(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v2, 0x69

    move v3, v1

    :goto_4
    const/16 v4, 0x6a

    if-ge v2, v4, :cond_10

    invoke-static {p1}, Lfbo;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v2, 0x6a

    :cond_10
    const/16 v4, 0x6b

    if-ge v2, v4, :cond_11

    invoke-static {p1}, Lfbo;->s(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v2, 0x6b

    :cond_11
    const/16 v4, 0x6c

    if-ge v2, v4, :cond_12

    invoke-static {p1}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v2, 0x6c

    :cond_12
    const/16 v4, 0xc8

    if-ge v2, v4, :cond_13

    invoke-static {p1}, Lfbo;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v2, 0xc8

    :cond_13
    const/16 v4, 0xc9

    if-ge v2, v4, :cond_14

    invoke-static {p1}, Lfbo;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_14
    if-eqz v3, :cond_15

    invoke-static {p1}, Lfbp;->b(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_15
    :goto_5
    if-eqz v0, :cond_16

    iget-object v0, p0, Lfbp;->a:Lfbo;

    invoke-static {v0}, Lfbo;->a(Lfbo;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lfbo;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_16
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lbbv;->a()Z

    move-result v2

    if-nez v2, :cond_17

    throw v0

    :cond_17
    const-string v2, "PeopleDatabaseHelper"

    const-string v3, "Upgrade failed.  Re-creating the database."

    invoke-static {v2, v3, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v2, p0, Lfbp;->a:Lfbo;

    invoke-static {v2}, Lfbo;->a(Lfbo;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleDatabaseHelper"

    const-string v4, "Upgrade failed.  Re-creating the database."

    invoke-static {v2, v3, v4, v0}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lfbp;->a:Lfbo;

    invoke-static {v0, p1}, Lfbo;->a(Lfbo;Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v1

    goto :goto_5

    :cond_18
    move v8, v2

    move v2, v3

    move v3, v8

    goto :goto_4

    :cond_19
    move v0, v2

    goto/16 :goto_3

    :cond_1a
    move v3, v0

    goto/16 :goto_2

    :cond_1b
    move v0, v4

    goto/16 :goto_1

    :cond_1c
    move v4, p2

    goto/16 :goto_0
.end method
