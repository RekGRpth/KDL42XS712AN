.class final Lfqf;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

.field final synthetic d:Lfqe;


# direct methods
.method constructor <init>(Lfqe;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;)V
    .locals 0

    iput-object p1, p0, Lfqf;->d:Lfqe;

    iput-object p2, p0, Lfqf;->a:Ljava/lang/String;

    iput-object p3, p0, Lfqf;->b:Ljava/lang/Object;

    iput-object p4, p0, Lfqf;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lfqf;->d:Lfqe;

    invoke-static {v0}, Lfqe;->a(Lfqe;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqf;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AudienceSelectionAdapter"

    const-string v2, "Exception opening ContactsDB avatar"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lfqf;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfqf;->d:Lfqe;

    invoke-static {v0}, Lfqe;->b(Lfqe;)Lbkt;

    move-result-object v0

    iget-object v1, p0, Lfqf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lbkt;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfqf;->b:Ljava/lang/Object;

    iget-object v1, p0, Lfqf;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->h()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lfqf;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
