.class public final Lhjv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lbfy;

.field public static final b:Lbfy;

.field public static final c:Lbfy;

.field public static final d:Lbfy;

.field public static final e:Lbfy;

.field public static final f:Lbfy;

.field public static final g:Lbfy;

.field public static final h:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "location:significant_motion_disabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->a:Lbfy;

    const-string v0, "location:collection_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->b:Lbfy;

    const-string v0, "location:stats_collect_nlp_api"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->c:Lbfy;

    const-string v0, "location:stats_collect_nlp_loctype"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->d:Lbfy;

    const-string v0, "location:stats_collect_nlp_settings"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->e:Lbfy;

    const-string v0, "url:google_location_server"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->f:Lbfy;

    const-string v0, "network_location_provider_debug"

    invoke-static {v0, v3}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->g:Lbfy;

    const-string v0, "android_id"

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lhjv;->h:Lbfy;

    return-void
.end method

.method public static a(Ljava/util/Random;Lbfy;)Z
    .locals 2

    invoke-virtual {p1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
