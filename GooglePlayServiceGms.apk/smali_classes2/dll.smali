.class public final Ldll;
.super Lbmi;
.source "SourceFile"


# static fields
.field private static final g:Ljava/util/HashMap;

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldll;->g:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldll;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZZ)V
    .locals 8

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p3

    move v5, p4

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Ldll;->i:Z

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;I)V
    .locals 3

    sget-object v1, Ldll;->h:Ljava/lang/Object;

    monitor-enter v1

    const/16 v0, 0x1110

    if-ne p1, v0, :cond_0

    :try_start_0
    sget-object v0, Ldll;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    sget-object v0, Ldll;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 2

    sget-object v1, Ldll;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldll;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/16 v0, 0x1110

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Lsk;Lsj;Ljava/util/HashMap;)Lbme;
    .locals 11

    new-instance v0, Ldlk;

    iget-boolean v9, p0, Ldll;->c:Z

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p7

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Ldlk;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 8

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-super/range {p0 .. p5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Null response received for path "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    move-object v3, v0

    iget-object v0, v3, Lsp;->a:Lrz;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_c

    iget-object v1, p0, Ldll;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    throw v0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {v3, v4}, Lbng;->b(Lsp;Ljava/lang/String;)Lbne;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lbne;->c()Ljava/lang/String;

    move-result-object v6

    const-string v0, "NotLicensed"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Ldqq;->d()Ldqr;

    move-result-object v0

    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Ldqr;->a(I)Ldqr;

    move-result-object v0

    move-object v5, v0

    :goto_2
    if-nez v5, :cond_a

    move-object v0, v4

    goto :goto_1

    :cond_4
    const-string v0, "UserRegistrationIncomplete"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Ldqq;->d()Ldqr;

    move-result-object v0

    const/16 v5, 0x5dc

    iput v5, v0, Ldqr;->a:I

    iput v7, v0, Ldqr;->b:I

    move-object v5, v0

    goto :goto_2

    :cond_5
    const-string v0, "UnregisteredClientId"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "accessNotConfigured"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "userRateLimitExceeded"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    invoke-static {}, Ldqq;->d()Ldqr;

    move-result-object v0

    const/16 v5, 0x3ea

    iput v5, v0, Ldqr;->a:I

    iput v7, v0, Ldqr;->b:I

    move-object v5, v0

    goto :goto_2

    :cond_7
    const-string v0, "ApiAccessDenied"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "UserNotAllowed"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    invoke-static {}, Ldqq;->d()Ldqr;

    move-result-object v0

    const/16 v5, 0x3eb

    invoke-virtual {v0, v5}, Ldqr;->a(I)Ldqr;

    move-result-object v0

    move-object v5, v0

    goto :goto_2

    :cond_9
    const-string v0, "ApplicationRequestNotAllowed"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "GamesServer"

    const-string v5, "Attempting to access a resource for another application. Check your resource IDs."

    invoke-static {v0, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldqq;->d()Ldqr;

    move-result-object v0

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Ldqr;->a(I)Ldqr;

    move-result-object v0

    move-object v5, v0

    goto :goto_2

    :cond_a
    iput-object v6, v5, Ldqr;->c:Ljava/lang/String;

    iget v0, v5, Ldqr;->b:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_b

    move v0, v1

    :goto_3
    invoke-static {v0}, Lbkm;->a(Z)V

    new-instance v0, Ldqq;

    invoke-direct {v0, v5, v2}, Ldqq;-><init>(Ldqr;B)V

    goto/16 :goto_1

    :cond_b
    move v0, v2

    goto :goto_3

    :cond_c
    throw v3

    :cond_d
    move-object v5, v4

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcwi;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Ldll;->c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Ldll;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lbmz;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "GamesServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "GamesServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lamq;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Ldll;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "X-Device-ID"

    invoke-static {}, Lbox;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "User-Agent"

    const-string v2, "Games Android SDK/1.0-%s"

    invoke-static {p1, v2}, Lbmw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ldll;->d(Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method protected final a(Lsc;Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lbml;

    iget-object v1, p0, Ldll;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lsc;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/rooms/create"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "[\\S+]*/rooms/[\\S]*/join[\\S+]*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/16 v3, 0x2710

    :goto_0
    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lbml;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lsc;->a(Lsm;)Lsc;

    return-void

    :cond_1
    const/16 v3, 0x1388

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Ldll;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldll;->a:Landroid/content/Context;

    invoke-static {v0}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "serverApiVersion1p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v0, Lcwh;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    sget-object v0, Lcwh;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-static {}, Lcwi;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcwi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Ldll;->a:Landroid/content/Context;

    invoke-static {v0}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "serverApiVersion3p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v0, Lcwh;->d:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcwh;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_2
    invoke-static {}, Lcwi;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcwi;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-super {p0, p1}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Ldll;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    throw v0
.end method

.method protected final c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;
    .locals 2

    new-instance v0, Lbmx;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-object v0
.end method
