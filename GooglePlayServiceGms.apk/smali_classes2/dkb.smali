.class public final Ldkb;
.super Lbqi;
.source "SourceFile"


# static fields
.field private static h:Ljava/lang/String;


# instance fields
.field protected final d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field protected g:Ljava/lang/String;

.field private final i:Ldkd;

.field private final j:Ldkc;

.field private final k:Ldke;

.field private final l:Ldkk;

.field private final m:Ldrt;

.field private n:Ldkh;

.field private o:Z

.field private final p:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "RealTimeStateMachine"

    sput-object v0, Ldkb;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ldkk;Ldrt;)V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Ldkb;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbqi;-><init>(Ljava/lang/String;)V

    new-instance v0, Ldkd;

    invoke-direct {v0, p0, v2}, Ldkd;-><init>(Ldkb;B)V

    iput-object v0, p0, Ldkb;->i:Ldkd;

    new-instance v0, Ldkc;

    invoke-direct {v0, p0, v2}, Ldkc;-><init>(Ldkb;B)V

    iput-object v0, p0, Ldkb;->j:Ldkc;

    new-instance v0, Ldke;

    invoke-direct {v0, p0, v2}, Ldke;-><init>(Ldkb;B)V

    iput-object v0, p0, Ldkb;->k:Ldke;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldkb;->p:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/gms/games/service/RoomAndroidService;->a()V

    iput-object p1, p0, Ldkb;->d:Ljava/lang/String;

    iput-object p2, p0, Ldkb;->l:Ldkk;

    iput-object p3, p0, Ldkb;->m:Ldrt;

    iget-object v0, p0, Ldkb;->i:Ldkd;

    invoke-virtual {p0, v0}, Ldkb;->a(Lbqh;)V

    iget-object v0, p0, Ldkb;->j:Ldkc;

    iget-object v1, p0, Ldkb;->i:Ldkd;

    invoke-virtual {p0, v0, v1}, Ldkb;->a(Lbqh;Lbqh;)V

    iget-object v0, p0, Ldkb;->k:Ldke;

    iget-object v1, p0, Ldkb;->j:Ldkc;

    invoke-virtual {p0, v0, v1}, Ldkb;->a(Lbqh;Lbqh;)V

    iget-object v0, p0, Ldkb;->i:Ldkd;

    invoke-virtual {p0, v0}, Ldkb;->b(Lbqh;)V

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Ldkb;->a(I)V

    invoke-virtual {p0, v2}, Ldkb;->a(Z)V

    return-void
.end method

.method static synthetic a(Ldkb;Ldkh;)Ldkh;
    .locals 0

    iput-object p1, p0, Ldkb;->n:Ldkh;

    return-object p1
.end method

.method static synthetic a(Ldkb;)Ldkk;
    .locals 1

    iget-object v0, p0, Ldkb;->l:Ldkk;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Ldmd;
    .locals 6

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    new-instance v0, Ldmd;

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getCount()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getMax()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getMin()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getSum()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Ldmd;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    :cond_0
    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Ldnw;
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumBytesReceived()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Ldkb;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Ldmd;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumBytesSent()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Ldkb;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Ldmd;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getRoundTripLatencyMs()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Ldkb;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Ldmd;

    move-result-object v7

    new-instance v0, Ldnw;

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesLost()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesReceived()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesSent()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Ldnw;-><init>(Ldmd;Ldmd;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ldmd;)V

    return-object v0
.end method

.method static synthetic a(Ldkb;Ldkf;)Ljava/util/ArrayList;
    .locals 6

    iget-object v2, p1, Ldkf;->b:[Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v0, p0, Ldkb;->n:Ldkh;

    invoke-virtual {v0}, Ldkh;->a()Ljava/util/ArrayList;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    array-length v3, v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Ldkb;->n:Ldkh;

    aget-object v5, v2, v1

    invoke-virtual {v4, v5}, Ldkh;->c(Ljava/lang/String;)Ldki;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(ILjava/util/ArrayList;)V
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v2, "Checking participantStatus %d for %d participants."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Ldkb;->b(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v2

    packed-switch p1, :pswitch_data_0

    sget-object v0, Ldkb;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Peer Status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldrt;->a(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p2}, Ldkb;->c(Ljava/util/ArrayList;)V

    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldrt;->b(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Ldkb;->l:Ldkk;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldki;

    iget-object v0, v0, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ldkk;->f(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldrt;->c(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldrt;->d(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Ldkb;Landroid/os/Message;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Ldkb;Landroid/os/Message;Ljava/lang/String;)V
    .locals 13

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldjw;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Ldkb;->n:Ldkh;

    invoke-virtual {v1}, Ldkh;->a()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v1, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldki;

    iget-object v6, p0, Ldkb;->l:Ldkk;

    iget-object v7, v1, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ldkk;->d(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->getReliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    move-result-object v7

    invoke-static {v7}, Ldkb;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Ldnw;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->getUnreliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    move-result-object v6

    invoke-static {v6}, Ldkb;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Ldnw;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getConnectionStartTimestampMs()I

    move-result v6

    int-to-long v9, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v9, v11, v9

    new-instance v6, Ldnx;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v1, v1, Ldki;->a:Ljava/lang/String;

    invoke-direct {v6, v9, v1, v7, v8}, Ldnx;-><init>(Ljava/lang/Long;Ljava/lang/String;Ldnw;Ldnw;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    sget-object v6, Ldkb;->h:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Received null PeerDiagnostics from DataConnectionManager  for peer: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Ldki;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v1, v1, Ldki;->c:I

    invoke-static {v1}, Ldeb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ldac;->b()V

    goto :goto_1

    :cond_1
    new-instance v1, Ldpc;

    iget v2, v0, Ldjw;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v0, v0, Ldjw;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-boolean v4, p0, Ldkb;->o:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v2, v0, v3, v4}, Ldpc;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    iget-object v0, p0, Ldkb;->l:Ldkk;

    invoke-virtual {v0}, Ldkk;->c()V

    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v2, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v2, p2, v1}, Ldrt;->a(Ljava/lang/String;Ljava/lang/String;Ldpc;)V

    return-void
.end method

.method static synthetic a(Ldkb;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Lbqg;)V

    return-void
.end method

.method static synthetic a(Ldkb;Ldki;Ljava/lang/String;Ldjx;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v0, "CONNECTION_FAILED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v6, p1, Ldki;->e:Z

    :cond_0
    iget-object v3, p1, Ldki;->a:Ljava/lang/String;

    iget-object v0, p0, Ldkb;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v6

    :goto_0
    const-string v1, "CONNECTION_FAILED"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ldpf;

    iget-object v1, p3, Ldjx;->b:Ljava/lang/Integer;

    iget-object v2, p3, Ldjx;->c:Ljava/lang/String;

    iget-object v5, p3, Ldjx;->d:Ljava/lang/Integer;

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Ldpf;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Ldkb;->m:Ldrt;

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v8}, Ldrt;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ldpi;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v2, "RoomStatus returned by p2pStatus : %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ldpi;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ldkb;->a(Ldpi;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v7

    goto :goto_0

    :cond_4
    sget-object v0, Ldkb;->h:Ljava/lang/String;

    const-string v1, "Failed to report p2pStatus"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Ldkb;Ldpi;)V
    .locals 0

    invoke-direct {p0, p1}, Ldkb;->a(Ldpi;)V

    return-void
.end method

.method static synthetic a(Ldkb;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Ldkb;->c(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ldpi;)V
    .locals 19

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iget-object v1, v1, Ldkh;->a:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v2, "Current room %s does not match room %s from notification. Ignoring the notification."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Ldkb;->n:Ldkh;

    iget-object v5, v5, Ldkh;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p1 .. p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Ldpi;->d()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iget v1, v1, Ldkh;->e:I

    invoke-virtual/range {p1 .. p1}, Ldpi;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v1, v2, :cond_2

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v2, "Current status version %d which is the sameor later than the version received %d. Ignoring the notification."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Ldkb;->n:Ldkh;

    iget v5, v5, Ldkh;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p1 .. p1}, Ldpi;->d()Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    invoke-virtual/range {p1 .. p1}, Ldpi;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Ldkh;->e:I

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ldrt;->a(Ldpi;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ldpi;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v18

    move v10, v1

    :goto_1
    move/from16 v0, v18

    if-ge v10, v0, :cond_13

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldph;

    invoke-virtual {v1}, Ldph;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ldph;->b()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_7

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v1}, Ldph;->d()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v3, 0x0

    invoke-virtual {v1}, Ldph;->getClientAddress()Ldoz;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v1}, Ldph;->getClientAddress()Ldoz;

    move-result-object v3

    invoke-virtual {v3}, Ldoz;->b()Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    const-string v6, "capabilities"

    invoke-virtual {v1, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iget-object v1, v1, Ldkh;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iget-boolean v1, v1, Ldkh;->c:Z

    if-eq v5, v1, :cond_5

    if-nez v5, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Ldrt;->i(Ljava/lang/String;)V

    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iput-boolean v5, v1, Ldkh;->c:Z

    :cond_6
    :goto_4
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_1

    :cond_7
    invoke-virtual {v1}, Ldph;->b()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldkb;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Ldrt;->h(Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    invoke-virtual {v1, v2}, Ldkh;->c(Ljava/lang/String;)Ldki;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    if-eqz v7, :cond_d

    iget v6, v7, Ldki;->c:I

    iget-boolean v2, v7, Ldki;->d:Z

    iput-object v3, v7, Ldki;->b:Ljava/lang/String;

    iput v4, v7, Ldki;->c:I

    iput-boolean v5, v7, Ldki;->d:Z

    move v3, v6

    move-object v6, v7

    :goto_5
    if-nez v1, :cond_a

    if-eq v3, v4, :cond_b

    :cond_a
    packed-switch v4, :pswitch_data_0

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v3, "Received participant with invalid status %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    :goto_6
    if-eq v2, v5, :cond_10

    if-eqz v5, :cond_f

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    :goto_7
    iget v1, v6, Ldki;->c:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_11

    const/4 v1, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Ldkb;->n:Ldkh;

    iget-object v3, v2, Ldkh;->f:Ljava/util/HashMap;

    iget-object v4, v6, Ldki;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_12

    iget-object v1, v6, Ldki;->b:Ljava/lang/String;

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v2, Ldkh;->g:Ljava/util/HashMap;

    iget-object v2, v6, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_d
    const/4 v7, 0x1

    new-instance v1, Ldki;

    if-eqz v6, :cond_e

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    :goto_9
    invoke-direct/range {v1 .. v6}, Ldki;-><init>(Ljava/lang/String;Ljava/lang/String;IZI)V

    move v2, v8

    move v3, v9

    move-object v6, v1

    move v1, v7

    goto :goto_5

    :cond_e
    const/4 v6, 0x0

    goto :goto_9

    :pswitch_1
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :pswitch_2
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :pswitch_3
    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :pswitch_4
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_f
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_10
    iget-boolean v1, v6, Ldki;->e:Z

    if-eqz v1, :cond_c

    if-nez v2, :cond_c

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding participant: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to disconnected set."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_11
    const/4 v1, 0x0

    goto :goto_8

    :cond_12
    iget-object v1, v6, Ldki;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, v2, Ldkh;->g:Ljava/util/HashMap;

    iget-object v2, v6, Ldki;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_13
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v13}, Ldkb;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v14}, Ldkb;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v15}, Ldkb;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v12}, Ldkb;->a(ILjava/util/ArrayList;)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldkb;->f:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Ldkb;->b(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ldrt;->e(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_14
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldkb;->f:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ldkb;->b(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ldrt;->f(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    iget v1, v1, Ldkh;->d:I

    invoke-virtual/range {p1 .. p1}, Ldpi;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->n:Ldkh;

    invoke-virtual/range {p1 .. p1}, Ldpi;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Ldkh;->d:I

    invoke-virtual/range {p1 .. p1}, Ldpi;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    sget-object v1, Ldkb;->h:Ljava/lang/String;

    const-string v2, "Received room with invalid status %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Ldpi;->c()Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    invoke-virtual/range {p1 .. p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ldrt;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    invoke-virtual/range {p1 .. p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ldrt;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Ldkb;->m:Ldrt;

    invoke-virtual/range {p1 .. p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ldrt;->g(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(Ldkb;Z)Z
    .locals 0

    iput-boolean p1, p0, Ldkb;->o:Z

    return p1
.end method

.method static synthetic a(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Ldkb;->b(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ldkb;)Ldkc;
    .locals 1

    iget-object v0, p0, Ldkb;->j:Ldkc;

    return-object v0
.end method

.method static synthetic b(Ldkb;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Lbqg;)V

    return-void
.end method

.method private static b(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldki;

    iget-object v0, v0, Ldki;->a:Ljava/lang/String;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method static synthetic c(Ldkb;)Ldrt;
    .locals 1

    iget-object v0, p0, Ldkb;->m:Ldrt;

    return-object v0
.end method

.method static synthetic c(Ldkb;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Lbqg;)V

    return-void
.end method

.method private c(Ljava/util/ArrayList;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v3

    :goto_0
    if-ge v5, v6, :cond_2

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldki;

    iget-object v7, v0, Ldki;->a:Ljava/lang/String;

    iget-object v8, v0, Ldki;->b:Ljava/lang/String;

    iget-object v1, p0, Ldkb;->g:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    move v1, v2

    :goto_1
    sget-object v9, Ldkb;->h:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v4, "Initiating connection with %s"

    :goto_2
    new-array v10, v2, [Ljava/lang/Object;

    aput-object v7, v10, v3

    invoke-static {v4, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v9, v4}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Ldkb;->l:Ldkk;

    iget v0, v0, Ldki;->f:I

    invoke-virtual {v4, v8, v1, v0}, Ldkk;->c(Ljava/lang/String;ZI)I

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    const-string v4, "Waiting for connection from %s"

    goto :goto_2

    :cond_2
    return-void
.end method

.method static synthetic d(Ldkb;)Ldkd;
    .locals 1

    iget-object v0, p0, Ldkb;->i:Ldkd;

    return-object v0
.end method

.method static synthetic d(Ldkb;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Lbqg;)V

    return-void
.end method

.method static synthetic e(Ldkb;)Ldke;
    .locals 1

    iget-object v0, p0, Ldkb;->k:Ldke;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Ldkb;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Ldkb;Lbqg;)V
    .locals 0

    invoke-virtual {p0, p1}, Ldkb;->a(Lbqg;)V

    return-void
.end method

.method static synthetic f(Ldkb;)Ldkh;
    .locals 1

    iget-object v0, p0, Ldkb;->n:Ldkh;

    return-object v0
.end method

.method static synthetic g(Ldkb;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Ldkb;->p:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mRoomId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldkb;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mCurrentParticipantId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldkb;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEnableSockets "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldkb;->o:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mSentMessageTokens size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldkb;->p:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Ldkb;->n:Ldkh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkb;->n:Ldkh;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mRoomId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Ldkh;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mRoomStatus "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Ldkh;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mCurrentParticipantInConnectedSet "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v0, Ldkh;->c:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mStatusVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Ldkh;->e:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Ldkb;->n:Ldkh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkb;->n:Ldkh;

    iget-object v0, v0, Ldkh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "NETWORK_CONNECTED"

    goto :goto_0

    :pswitch_1
    const-string v0, "NETWORK_DISCONNECTED"

    goto :goto_0

    :pswitch_2
    const-string v0, "CLIENT_DISCONNECTED"

    goto :goto_0

    :pswitch_3
    const-string v0, "CREATED_A_ROOM"

    goto :goto_0

    :pswitch_4
    const-string v0, "JOINED_A_ROOM"

    goto :goto_0

    :pswitch_5
    const-string v0, "LEFT_A_ROOM"

    goto :goto_0

    :pswitch_6
    const-string v0, "STATUS_NOTIFICATION"

    goto :goto_0

    :pswitch_7
    const-string v0, "PEER_JOINED"

    goto :goto_0

    :pswitch_8
    const-string v0, "P2P_CONNECTION_SUCCEEDED"

    goto :goto_0

    :pswitch_9
    const-string v0, "P2P_CONNECTION_FAILED"

    goto :goto_0

    :pswitch_a
    const-string v0, "MESSAGE_RECEIVED"

    goto :goto_0

    :pswitch_b
    const-string v0, "MESSAGE_SEND_RESULT"

    goto :goto_0

    :pswitch_c
    const-string v0, "SEND_RELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_d
    const-string v0, "SEND_UNRELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Ldkb;->n:Ldkh;

    iget-object v0, v0, Ldkh;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldki;

    if-nez v0, :cond_0

    const-string v0, "RoomData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received bad participant ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Ldki;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Ldkb;->o:Z

    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Ldkb;->n:Ldkh;

    iget-object v0, v0, Ldkh;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lbqi;->b:Lbql;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqi;->b:Lbql;

    invoke-static {v0}, Lbql;->e(Lbql;)V

    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Ldkb;->n:Ldkh;

    iget-object v0, v0, Ldkh;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
