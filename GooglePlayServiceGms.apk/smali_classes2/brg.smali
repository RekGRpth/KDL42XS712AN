.class public final Lbrg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbrl;


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final b:Lcon;

.field private final c:Landroid/util/SparseArray;

.field private final d:Lcmn;

.field private final e:Lbst;

.field private final f:Lcdu;

.field private final g:Lbsr;


# direct methods
.method public constructor <init>(Lcmn;Lbst;Lcdu;Lcon;Lbsr;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lbrg;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbrg;->c:Landroid/util/SparseArray;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmn;

    iput-object v0, p0, Lbrg;->d:Lcmn;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbst;

    iput-object v0, p0, Lbrg;->e:Lbst;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lbrg;->f:Lcdu;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcon;

    iput-object v0, p0, Lbrg;->b:Lcon;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsr;

    iput-object v0, p0, Lbrg;->g:Lbsr;

    return-void
.end method

.method private declared-synchronized a(I)Lbrh;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbrg;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lbsp;Lbrh;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 9

    monitor-enter p0

    :try_start_0
    sget-object v0, Lclg;->a:Lcjf;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    iget-object v8, p2, Lbrh;->c:Lbsy;

    new-instance v0, Lbsv;

    iget-object v1, p1, Lbsp;->a:Lcfc;

    iget-object v2, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v4, p2, Lbrh;->g:J

    iget-object v7, p0, Lbrg;->d:Lcmn;

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lbsv;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcmn;)V

    invoke-virtual {v8, v0}, Lbsy;->a(Lbsu;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmb;

    iget-object v1, p0, Lbrg;->g:Lbsr;

    invoke-virtual {v1}, Lbsr;->a()V

    iget-object v1, v0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcmb;->a:Lcom/google/android/gms/drive/DriveId;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    invoke-virtual {p0, p2}, Lbrg;->a(Lbrh;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to commit file because of an I/O error."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {p0, p2}, Lbrg;->a(Lbrh;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lbsp;Lbrh;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    sget-object v0, Lclg;->a:Lcjf;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcje;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lclg;->a:Lcjf;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_0
    iget-object v8, p2, Lbrh;->c:Lbsy;

    new-instance v0, Lbsq;

    iget-object v1, p1, Lbsp;->a:Lcfc;

    iget-object v2, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v5, p2, Lbrh;->g:J

    iget-object v7, p0, Lbrg;->d:Lcmn;

    move-object v3, p4

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lbsq;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcmn;)V

    invoke-virtual {v8, v0}, Lbsy;->a(Lbsu;)Ljava/lang/Object;

    iget-object v0, p0, Lbrg;->g:Lbsr;

    invoke-virtual {v0}, Lbsr;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0, p2}, Lbrg;->a(Lbrh;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Failed to commit file because of an I/O error."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {p0, p2}, Lbrg;->a(Lbrh;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lbsp;Lcfp;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 8

    const/16 v5, 0x8

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    move-object v7, v0

    :goto_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    :goto_1
    if-nez v7, :cond_0

    const/high16 v0, 0x20000000

    if-ne p3, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const-string v2, "New files must be created with MODE_WRITE_ONLY."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    :cond_0
    sparse-switch p3, :sswitch_data_0

    :try_start_0
    new-instance v0, Lbrm;

    const/16 v2, 0xa

    const-string v3, "Unrecognized mode."

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v2, "HashBasedOpenContentsStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    new-instance v0, Lbrm;

    const-string v2, "Unable to open file."

    invoke-direct {v0, v5, v2, v1}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    move-object v7, v3

    goto :goto_0

    :cond_2
    move-object v4, v3

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lbrg;->e:Lbst;

    invoke-virtual {p2}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbst;->b(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    :goto_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lbsy;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    :cond_4
    if-nez v2, :cond_5

    new-instance v0, Lbrm;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Content is not available locally: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v5, v2, v1}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lbrg;->e:Lbst;

    invoke-virtual {v0}, Lbst;->a()Lbsy;

    move-result-object v0

    move-object v2, v3

    move-object v3, v0

    goto :goto_3

    :sswitch_2
    iget-object v0, p0, Lbrg;->e:Lbst;

    invoke-virtual {p2}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbst;->a(Ljava/lang/String;)Lbsy;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    move-object v2, v3

    move-object v3, v0

    goto :goto_3

    :cond_5
    new-instance v0, Lbrh;

    iget-object v5, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move-object v1, p0

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lbrh;-><init>(Lbrg;Landroid/os/ParcelFileDescriptor;Lbsy;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;Landroid/os/IBinder;)V

    invoke-direct {p0, v0}, Lbrg;->b(Lbrh;)V

    new-instance v1, Lcom/google/android/gms/drive/Contents;

    iget v0, v0, Lbrh;->a:I

    invoke-direct {v1, v2, v0, p3, v7}, Lcom/google/android/gms/drive/Contents;-><init>(Landroid/os/ParcelFileDescriptor;IILcom/google/android/gms/drive/DriveId;)V

    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x20000000 -> :sswitch_1
        0x30000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private declared-synchronized b(Lbrh;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lbrh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrg;->c:Landroid/util/SparseArray;

    iget v1, p1, Lbrh;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Unable to link client"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lbsp;Landroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x20000000

    invoke-direct {p0, p1, v0, v1, p2}, Lbrg;->b(Lbsp;Lcfp;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbsp;Lcfp;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lbrg;->b(Lbsp;Lcfp;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lbsp;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0, v0, p2}, Lbrg;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lbrg;->a(I)Lbrh;

    move-result-object v0

    iget-object v1, v0, Lbrh;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v1, :cond_0

    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "Cannot create a new file using contents opened from an existing file.Use DriveApi.newContents() to create the contents instead."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0, p3, p4}, Lbrg;->a(Lbsp;Lbrh;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method final declared-synchronized a(Lbrh;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lbrh;->b()V

    iget-object v0, p0, Lbrg;->c:Landroid/util/SparseArray;

    iget v1, p1, Lbrh;->a:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lbsp;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Z)V
    .locals 2

    iget-object v0, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbrg;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    invoke-virtual {p2}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lbrg;->a(I)Lbrh;

    move-result-object v0

    if-eqz p4, :cond_0

    iget-object v1, v0, Lbrh;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbrh;->c:Lbsy;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbrh;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, p1, v0, p3, v1}, Lbrg;->a(Lbsp;Lbrh;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lbrg;->a(Lbrh;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p2}, Lbrg;->a(I)Lbrh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lbrh;->e:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "App %s cannot close this file because it was opened by different app."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    return-void
.end method
