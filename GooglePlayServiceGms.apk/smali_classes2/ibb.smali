.class public final Libb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:D

.field b:D

.field c:I

.field public d:I

.field final e:[D

.field final f:[D

.field final g:[I

.field private h:D

.field private i:D


# direct methods
.method public constructor <init>(I)V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Libb;->a:D

    iput-wide v1, p0, Libb;->b:D

    iput v3, p0, Libb;->c:I

    iput v3, p0, Libb;->d:I

    iput-wide v1, p0, Libb;->h:D

    iput-wide v1, p0, Libb;->i:D

    new-array v0, p1, [D

    iput-object v0, p0, Libb;->e:[D

    new-array v0, p1, [D

    iput-object v0, p0, Libb;->f:[D

    new-array v0, p1, [I

    iput-object v0, p0, Libb;->g:[I

    iput-wide v1, p0, Libb;->a:D

    iput-wide v1, p0, Libb;->b:D

    iput v3, p0, Libb;->c:I

    iput v3, p0, Libb;->d:I

    iput-wide v1, p0, Libb;->h:D

    iput-wide v1, p0, Libb;->i:D

    return-void
.end method


# virtual methods
.method public final a()D
    .locals 4

    iget-wide v0, p0, Libb;->h:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Libb;->c:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Libb;->a:D

    iget v2, p0, Libb;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Libb;->h:D

    :cond_0
    iget-wide v0, p0, Libb;->h:D

    return-wide v0
.end method

.method public final b()D
    .locals 4

    iget-wide v0, p0, Libb;->i:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Libb;->c:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Libb;->b:D

    iget v2, p0, Libb;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Libb;->i:D

    :cond_0
    iget-wide v0, p0, Libb;->i:D

    return-wide v0
.end method

.method public final c()I
    .locals 14

    const/16 v5, 0x1388

    const/4 v13, 0x1

    const/4 v4, 0x0

    iget v0, p0, Libb;->c:I

    if-nez v0, :cond_0

    :goto_0
    return v4

    :cond_0
    iget v0, p0, Libb;->c:I

    if-ne v0, v13, :cond_1

    iget-object v0, p0, Libb;->g:[I

    aget v4, v0, v4

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Libb;->a()D

    move-result-wide v0

    invoke-virtual {p0}, Libb;->b()D

    move-result-wide v2

    move v8, v4

    move v9, v4

    move v10, v5

    move v11, v5

    move v12, v4

    :goto_1
    iget v4, p0, Libb;->c:I

    if-ge v8, v4, :cond_2

    iget-object v4, p0, Libb;->e:[D

    aget-wide v4, v4, v8

    iget-object v6, p0, Libb;->f:[D

    aget-wide v6, v6, v8

    invoke-static/range {v0 .. v7}, Liba;->a(DDDD)D

    move-result-wide v4

    double-to-int v6, v4

    add-int/2addr v12, v6

    iget-object v4, p0, Libb;->g:[I

    aget v4, v4, v8

    if-le v6, v4, :cond_5

    move v5, v13

    :goto_2
    iget-object v4, p0, Libb;->g:[I

    aget v4, v4, v8

    if-ge v4, v11, :cond_4

    iget-object v4, p0, Libb;->g:[I

    aget v7, v4, v8

    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v9, v5

    move v10, v6

    move v11, v7

    goto :goto_1

    :cond_2
    if-eqz v9, :cond_3

    iget v0, p0, Libb;->c:I

    div-int v4, v12, v0

    goto :goto_0

    :cond_3
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_0

    :cond_4
    move v6, v10

    move v7, v11

    goto :goto_3

    :cond_5
    move v5, v9

    goto :goto_2
.end method
