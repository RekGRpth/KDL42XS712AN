.class final Lhdr;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

.field final synthetic f:Lhdj;


# direct methods
.method constructor <init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)V
    .locals 1

    iput-object p1, p0, Lhdr;->f:Lhdj;

    iput-object p4, p0, Lhdr;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lhdr;->e:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    iget-object v0, p0, Lhdr;->f:Lhdj;

    invoke-static {v0}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v0, p0, Lhdr;->f:Lhdj;

    iget-object v0, p0, Lhdr;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhdr;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lhdr;->e:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    iget-object v0, p0, Lhdr;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdr;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhde;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lhde;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "get_purchase_options"

    invoke-static {v8, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lhcd;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Lipl;

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget v0, v0, Lipl;->i:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method
