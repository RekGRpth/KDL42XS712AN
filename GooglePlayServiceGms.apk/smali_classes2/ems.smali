.class public final Lems;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/io/File;

.field private final c:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/Class;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Must specify non-null message type class"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lems;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lems;->a:Ljava/lang/String;

    iput-object p2, p0, Lems;->c:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 10

    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v1, "Requested read of scratch file %s"

    iget-object v2, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    :try_start_0
    iget-object v1, p0, Lems;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lems;->b:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    const-string v0, "Reading scratch file %s"

    iget-object v1, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lems;->c:Ljava/lang/Class;

    invoke-static {v2, v0}, Lemq;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Failed to read valid data from scratch file %s"

    iget-object v2, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Error while reading from %s: file too large: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    iget-object v4, p0, Lems;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Lell;->a(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Error while closing stream to %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v0, v1, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :catch_2
    move-exception v1

    move-object v1, v0

    :goto_2
    :try_start_4
    const-string v2, "Scratch file %s not found, ignoring"

    iget-object v3, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    :catch_3
    move-exception v1

    const-string v2, "Error while closing stream to %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :catch_4
    move-exception v1

    move-object v2, v0

    :goto_3
    :try_start_6
    const-string v3, "Error while reading from %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lems;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v2, :cond_0

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_1

    :catch_5
    move-exception v1

    const-string v2, "Error while closing stream to %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_4
    if-eqz v1, :cond_1

    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    :cond_1
    :goto_5
    throw v0

    :catch_6
    move-exception v1

    const-string v2, "Error while closing stream to %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catch_7
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    goto :goto_3

    :catch_8
    move-exception v2

    goto :goto_2
.end method

.method public final a(Ljava/lang/Iterable;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {p1, v1}, Lemq;->a(Ljava/lang/Iterable;Z)Ljava/nio/ByteBuffer;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    iget-object v0, p0, Lems;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Asked to write no data, deleting scratch file %s"

    iget-object v1, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lems;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Failed to delete scratch file %s"

    iget-object v2, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_3
    const-string v0, "Skipping write out of would be empty scratch file %s"

    iget-object v2, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v3, "Icing"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "Writing out (up to) %s for scratch file %s"

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lell;->a(J)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :goto_1
    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lems;->b:Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "No bytes actually written out for scratch file %s"

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lehe;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Error while closing stream to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_5
    const-string v3, "Writing out scratch file %s"

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_1

    :cond_6
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :catch_1
    move-exception v2

    const-string v3, "Error while closing stream to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lems;->a:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :catch_2
    move-exception v2

    move-object v3, v4

    :goto_3
    :try_start_4
    const-string v4, "Error while writing to %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lems;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v3, :cond_0

    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v2

    const-string v3, "Error while closing stream to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lems;->a:Ljava/lang/String;

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    move-object v3, v4

    :goto_4
    if-eqz v3, :cond_7

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_7
    :goto_5
    throw v2

    :catch_4
    move-exception v3

    const-string v4, "Error while closing stream to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lems;->a:Ljava/lang/String;

    aput-object v5, v1, v0

    invoke-static {v3, v4, v1}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_5

    :catchall_1
    move-exception v2

    goto :goto_4

    :catch_5
    move-exception v2

    goto :goto_3
.end method
