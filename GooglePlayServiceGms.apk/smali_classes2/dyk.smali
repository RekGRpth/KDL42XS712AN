.class final Ldyk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:Z

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Landroid/net/Uri;

.field f:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Ldyk;->a:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyk;->b:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldyk;-><init>(I)V

    iput-object p1, p0, Ldyk;->c:Ljava/lang/String;

    iput-object p2, p0, Ldyk;->d:Ljava/lang/String;

    iput-object p3, p0, Ldyk;->e:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    instance-of v2, p1, Ldyk;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ldyk;

    iget v2, p0, Ldyk;->a:I

    if-nez v2, :cond_2

    iget v2, p1, Ldyk;->a:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Ldyk;->a:I

    if-ne v2, v1, :cond_3

    iget v2, p1, Ldyk;->a:I

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Ldyk;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p1, Ldyk;->a:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ldyk;->c:Ljava/lang/String;

    iget-object v1, p1, Ldyk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lbkj;->a(Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "type"

    iget v2, p0, Ldyk;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "playerId"

    iget-object v2, p0, Ldyk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "displayName"

    iget-object v2, p0, Ldyk;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "iconImageUri"

    iget-object v2, p0, Ldyk;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "iconImageResId"

    iget v2, p0, Ldyk;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "selected"

    iget-boolean v2, p0, Ldyk;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    invoke-virtual {v0}, Lbkk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
