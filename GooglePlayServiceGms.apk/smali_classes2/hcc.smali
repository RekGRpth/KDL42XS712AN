.class public abstract Lhcc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhcc;->a:Landroid/accounts/Account;

    return-void
.end method

.method private static a([I)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    sparse-switch v3, :sswitch_data_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public abstract a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
.end method

.method public final a(Landroid/content/Context;)Lhgm;
    .locals 3

    invoke-virtual {p0}, Lhcc;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhcc;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v1, v0}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lhgm;

    invoke-direct {v2, v0, v1}, Lhgm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljaz;

    iget-object v0, v0, Ljaz;->a:[I

    invoke-static {v0}, Lhcc;->a([I)Z

    move-result v0

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljbb;

    iget-object v0, v0, Ljbb;->a:[I

    invoke-static {v0}, Lhcc;->a([I)Z

    move-result v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljax;

    iget-object v0, v0, Ljax;->a:[I

    invoke-static {v0}, Lhcc;->a([I)Z

    move-result v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljav;

    iget-object v0, v0, Ljav;->i:[I

    invoke-static {v0}, Lhcc;->a([I)Z

    move-result v0

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljat;

    iget-object v0, v0, Ljat;->a:[I

    invoke-static {v0}, Lhcc;->a([I)Z

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_3
        0xf -> :sswitch_2
        0x10 -> :sswitch_4
        0x1e -> :sswitch_5
    .end sparse-switch
.end method
