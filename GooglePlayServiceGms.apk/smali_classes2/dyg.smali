.class public final Ldyg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbel;


# instance fields
.field private final a:Ldxm;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Ldxm;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldyg;->b:Ljava/util/HashMap;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Ldyg;->a:Ldxm;

    iget-object v0, p0, Ldyg;->a:Ldxm;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v0, v1}, Ledd;->a(Lo;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lbek;)V
    .locals 7

    const/4 v6, 0x0

    check-cast p1, Lctr;

    invoke-interface {p1}, Lctr;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Lctr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lctr;->s_()Z

    move-result v3

    iget-object v0, p0, Ldyg;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Ldyg;->a:Ldxm;

    const-string v5, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v4, v5}, Ledd;->a(Lo;Ljava/lang/String;)V

    iget-object v4, p0, Ldyg;->a:Ldxm;

    invoke-virtual {v4, v1}, Ldxm;->d(I)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v0, "ClientMuteGameHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Status code was not SUCCESS! (status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", externalGameId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-nez v3, :cond_1

    const-string v0, "ClientMuteGameHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Application was not muted as it should have been. (status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", externalGameId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Ldyg;->a:Ldxm;

    const v2, 0x7f0b02a4    # com.google.android.gms.R.string.games_toast_dialog_app_muted

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Ldxm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldyg;->a:Ldxm;

    invoke-virtual {v1}, Ldxm;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-string v0, "ClientMuteGameHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Display name of muted game with externalGameId: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 4

    iget-object v0, p0, Ldyg;->a:Ldxm;

    invoke-virtual {v0}, Ldxm;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Ldyg;->a:Ldxm;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ClientMuteGameHelper"

    const-string v1, "muteGame: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldyg;->b:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Ldyg;->a:Ldxm;

    const v2, 0x7f0b02a3    # com.google.android.gms.R.string.games_progress_dialog_muting_game

    invoke-virtual {v1, v2}, Ldxm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    iget-object v2, p0, Ldyg;->a:Ldxm;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogMutingApp"

    invoke-static {v2, v1, v3}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    sget-object v1, Lcte;->n:Lctp;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lctp;->a(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method
