.class public final Ldqy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldad;

.field public final b:Z

.field c:Z

.field public final synthetic d:Ldqv;


# direct methods
.method public constructor <init>(Ldqv;Ldad;Z)V
    .locals 1

    iput-object p1, p0, Ldqy;->d:Ldqv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ldqy;->a:Ldad;

    iput-boolean p3, p0, Ldqy;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldqy;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    invoke-static {}, Ldqv;->z()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldqy;->d:Ldqv;

    invoke-static {v0}, Ldqv;->a(Ldqv;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Ldqy;->c:Z

    if-nez v0, :cond_0

    const-string v0, "GamesService"

    const-string v1, "RoomAndroidService is no longer valid."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
