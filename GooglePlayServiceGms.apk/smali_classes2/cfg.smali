.class public final Lcfg;
.super Lcfl;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Z

.field public c:Z

.field final d:J

.field private final g:J


# direct methods
.method protected constructor <init>(Lcdu;JJLjava/lang/Long;ZZ)V
    .locals 2

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    iput-wide p2, p0, Lcfg;->g:J

    iput-wide p4, p0, Lcfg;->d:J

    iput-object p6, p0, Lcfg;->a:Ljava/lang/Long;

    iput-boolean p7, p0, Lcfg;->b:Z

    iput-boolean p8, p0, Lcfg;->c:Z

    return-void
.end method

.method public static a(Lcdu;Lcfc;Landroid/database/Cursor;)Lcfg;
    .locals 10

    const/4 v1, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcdh;->b:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v5, p1, Lcfc;->b:J

    cmp-long v0, v5, v2

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Account ID from cursor (%d) does not match ID of account passed to cursor (%d)."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v4

    iget-wide v8, p1, Lcfc;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v1

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Lcdh;->a:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcdh;->d:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    sget-object v0, Lcdh;->e:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->e(Landroid/database/Cursor;)Z

    move-result v7

    sget-object v0, Lcdh;->c:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcdp;->e(Landroid/database/Cursor;)Z

    move-result v8

    new-instance v0, Lcfg;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcfg;-><init>(Lcdu;JJLjava/lang/Long;ZZ)V

    invoke-static {}, Lcdg;->a()Lcdg;

    move-result-object v1

    invoke-virtual {v1}, Lcdg;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcfg;->d(J)V

    return-object v0

    :cond_0
    move v0, v4

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lcdh;->b:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfg;->g:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdh;->a:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcfg;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdh;->d:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfg;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcdh;->e:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcfg;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    sget-object v0, Lcdh;->c:Lcdh;

    invoke-virtual {v0}, Lcdh;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcfg;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method
