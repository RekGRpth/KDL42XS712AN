.class final Lhpu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lhpt;


# direct methods
.method constructor <init>(Lhpt;)V
    .locals 0

    iput-object p1, p0, Lhpu;->a:Lhpt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->b(Lhpt;)Limb;

    move-result-object v0

    const-string v1, "Nothing to upload"

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->d(Lhpt;)Lhqf;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->e(Lhpt;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lhpv;

    invoke-direct {v1, p0}, Lhpv;-><init>(Lhpu;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->c(Lhpt;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->f(Lhpt;)Z

    :cond_2
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/io/File;

    iget-object v2, p0, Lhpu;->a:Lhpt;

    invoke-static {v2}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v0, p0, Lhpu;->a:Lhpt;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to list files in directory:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhpu;->a:Lhpt;

    invoke-static {v2}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhpt;->a(Lhpt;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    array-length v2, v4

    if-nez v2, :cond_1

    invoke-direct {p0}, Lhpu;->a()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v2

    invoke-virtual {v2, v3}, Lhru;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lhpu;->a:Lhpt;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lhpu;->a:Lhpt;

    invoke-static {v2}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is locked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhpt;->a(Lhpt;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhsd;->a(Ljava/lang/String;)Lhsd;

    move-result-object v5

    invoke-virtual {v5}, Lhsd;->a()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v5}, Lhsd;->b()I

    move-result v2

    if-eqz v2, :cond_6

    move v2, v0

    :goto_1
    sget-boolean v6, Licj;->b:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lhpu;->a:Lhpt;

    invoke-static {v6}, Lhpt;->b(Lhpt;)Limb;

    move-result-object v6

    const-string v7, "forceUpload: %s, hasUsefulData: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lhsd;->c()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Limb;->a(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v5}, Lhsd;->c()Z

    move-result v6

    if-nez v6, :cond_7

    if-nez v2, :cond_7

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->b(Lhpt;)Limb;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not uploading session since missing required data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0}, Lhpt;->c(Lhpt;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0, v3}, Lhpt;->a(Lhpt;Ljava/io/File;)Z

    :cond_5
    invoke-direct {p0}, Lhpu;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lhpu;->a:Lhpt;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load SessionSummary: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhpt;->a(Lhpt;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lhpu;->a:Lhpt;

    invoke-static {v2, v4}, Lhpt;->a(Lhpt;[Ljava/lang/String;)Lhpy;

    move-result-object v2

    iget-object v3, v2, Lhpy;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_8

    invoke-direct {p0}, Lhpu;->a()V

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lhpu;->a:Lhpt;

    iget-object v4, v2, Lhpy;->b:Ljava/lang/String;

    if-eqz v4, :cond_9

    :goto_2
    invoke-static {v3, v0}, Lhpt;->a(Lhpt;Z)Z

    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v0

    iget-object v1, p0, Lhpu;->a:Lhpt;

    invoke-static {v1}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhru;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lhpu;->a:Lhpt;

    const-string v1, "Failed to lock working directory."

    invoke-static {v0, v1}, Lhpt;->a(Lhpt;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_2

    :cond_a
    :try_start_1
    iget-object v0, p0, Lhpu;->a:Lhpt;

    invoke-static {v0, v2}, Lhpt;->a(Lhpt;Lhpy;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v0

    iget-object v1, p0, Lhpu;->a:Lhpt;

    invoke-static {v1}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhru;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v1

    iget-object v2, p0, Lhpu;->a:Lhpt;

    invoke-static {v2}, Lhpt;->a(Lhpt;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhru;->b(Ljava/lang/String;)V

    throw v0
.end method
