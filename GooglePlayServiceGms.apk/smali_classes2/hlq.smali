.class final enum Lhlq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhlq;

.field public static final enum b:Lhlq;

.field public static final enum c:Lhlq;

.field public static final enum d:Lhlq;

.field public static final enum e:Lhlq;

.field public static final enum f:Lhlq;

.field public static final enum g:Lhlq;

.field public static final enum h:Lhlq;

.field private static final synthetic i:[Lhlq;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lhlq;

    const-string v1, "SCAN_OVERLAP_RATIO_0_2"

    invoke-direct {v0, v1, v3}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->a:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "SCAN_OVERLAP_COUNT_0_2"

    invoke-direct {v0, v1, v4}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->b:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "SCAN_OVERLAP_MIN_RADIUS_0_2"

    invoke-direct {v0, v1, v5}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->c:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "SCAN_OVERLAP_RATIO_1_2"

    invoke-direct {v0, v1, v6}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->d:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "WIFI_LOC_DISTANCE_0_1"

    invoke-direct {v0, v1, v7}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->e:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "WIFI_LOC_DISTANCE_0_2"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->f:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "WIFI_LOC_DISTANCE_1_2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->g:Lhlq;

    new-instance v0, Lhlq;

    const-string v1, "CELL_LOC_DISTANCE_0_2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lhlq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlq;->h:Lhlq;

    const/16 v0, 0x8

    new-array v0, v0, [Lhlq;

    sget-object v1, Lhlq;->a:Lhlq;

    aput-object v1, v0, v3

    sget-object v1, Lhlq;->b:Lhlq;

    aput-object v1, v0, v4

    sget-object v1, Lhlq;->c:Lhlq;

    aput-object v1, v0, v5

    sget-object v1, Lhlq;->d:Lhlq;

    aput-object v1, v0, v6

    sget-object v1, Lhlq;->e:Lhlq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhlq;->f:Lhlq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhlq;->g:Lhlq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhlq;->h:Lhlq;

    aput-object v2, v0, v1

    sput-object v0, Lhlq;->i:[Lhlq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhlq;
    .locals 1

    const-class v0, Lhlq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhlq;

    return-object v0
.end method

.method public static values()[Lhlq;
    .locals 1

    sget-object v0, Lhlq;->i:[Lhlq;

    invoke-virtual {v0}, [Lhlq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhlq;

    return-object v0
.end method
