.class final Lazl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawd;


# instance fields
.field final a:Lawc;

.field final b:Lcom/google/android/gms/cast/CastDevice;

.field c:Z

.field d:Z

.field e:Z

.field final synthetic f:Lazj;

.field private final g:Ljava/lang/String;

.field private final h:Lazk;

.field private final i:Lorg/json/JSONArray;


# direct methods
.method public constructor <init>(Lazj;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v3, 0x0

    iput-object p1, p0, Lazl;->f:Lazj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lazk;

    invoke-direct {v1, v3}, Lazk;-><init>(B)V

    iput-object v1, p0, Lazl;->h:Lazk;

    iput-boolean v3, p0, Lazl;->c:Z

    iput-boolean v3, p0, Lazl;->d:Z

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, p0, Lazl;->i:Lorg/json/JSONArray;

    iput-boolean v0, p0, Lazl;->e:Z

    new-instance v1, Lawc;

    invoke-static {p1}, Lazj;->a(Lazj;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lawc;-><init>(Landroid/content/Context;Lawd;)V

    iput-object v1, p0, Lazl;->a:Lawc;

    iput-object p2, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    const-string v1, "%s-%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lazj;->b(Lazj;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {}, Lazj;->a()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lazl;->g:Ljava/lang/String;

    invoke-static {p1}, Lazj;->c(Lazj;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {p1}, Lazj;->c(Lazj;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawh;

    iget-object v5, v0, Lawh;->c:Ljava/util/Set;

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    iget-object v6, v0, Lawh;->b:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v2, p0, Lazl;->i:Lorg/json/JSONArray;

    iget-object v0, v0, Lawh;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move v2, v3

    :cond_0
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    invoke-direct {p0}, Lazl;->b()V

    :cond_2
    if-eqz v1, :cond_3

    invoke-direct {p0}, Lazl;->c()V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    const-string v1, "Sending text message to %s: (ns=%s, dest=%s) %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "receiver-0"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lazw;

    invoke-direct {v0}, Lazw;-><init>()V

    invoke-virtual {v0, v5}, Lazw;->a(I)Lazw;

    move-result-object v0

    iget-object v1, p0, Lazl;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lazw;->a(Ljava/lang/String;)Lazw;

    move-result-object v0

    const-string v1, "receiver-0"

    invoke-virtual {v0, v1}, Lazw;->b(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lazw;->c(Ljava/lang/String;)Lazw;

    move-result-object v0

    invoke-virtual {v0, v5}, Lazw;->b(I)Lazw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lazw;->d(Ljava/lang/String;)Lazw;

    move-result-object v0

    iget-object v1, p0, Lazl;->a:Lawc;

    invoke-virtual {v0}, Lazw;->d()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lawc;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazl;->c:Z

    iget-boolean v0, p0, Lazl;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lazl;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lazl;->d()V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazl;->d:Z

    iget-boolean v0, p0, Lazl;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lazl;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lazl;->d()V

    :cond_0
    return-void
.end method

.method private d()V
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lazl;->a:Lawc;

    invoke-virtual {v0}, Lawc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "urn:x-cast:com.google.cast.tp.connection"

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "type"

    const-string v5, "CLOSE"

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lazl;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v0, p0, Lazl;->a:Lawc;

    invoke-virtual {v0}, Lawc;->b()V

    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lazl;->f:Lazj;

    invoke-static {v0}, Lazj;->c(Lazj;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawh;

    iget-object v1, p0, Lazl;->h:Lazk;

    iget-object v6, v0, Lawh;->b:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v7, v1, Lazk;->b:Ljava/util/Set;

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    iget-object v1, v1, Lazk;->a:Ljava/util/Set;

    iget-object v6, v0, Lawh;->c:Ljava/util/Set;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_1

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v4, "Failed to send disconnect message"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v4, v5}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v4, "Failed to build disconnect message"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v4, v5}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    iget-boolean v0, p0, Lazl;->e:Z

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0, v4}, Lazl;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V

    :goto_3
    return-void

    :cond_5
    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    const-string v1, "rejected device: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "type"

    const-string v2, "CONNECT"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "package"

    iget-object v2, p0, Lazl;->f:Lazj;

    invoke-static {v2}, Lazj;->b(Lazj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "origin"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "urn:x-cast:com.google.cast.tp.connection"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lazl;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lazl;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "requestId"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "GET_APP_AVAILABILITY"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "appId"

    iget-object v2, p0, Lazl;->i:Lorg/json/JSONArray;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "urn:x-cast:com.google.cast.receiver"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lazl;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lazl;->d:Z

    if-nez v0, :cond_1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "requestId"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "GET_STATUS"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "urn:x-cast:com.google.cast.receiver"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lazl;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v2, "Failed to send messages"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v2, "Failed to build messages"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 5

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    const-string v1, "Connection to %s:%d (%s) failed with error %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    iget-object v1, p0, Lazl;->f:Lazj;

    invoke-static {v1}, Lazj;->d(Lazj;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lazn;

    invoke-direct {v2, p0, v0}, Lazn;-><init>(Lazl;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V
    .locals 2

    iget-object v0, p0, Lazl;->f:Lazj;

    invoke-static {v0}, Lazj;->d(Lazj;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lazm;

    invoke-direct {v1, p0, p1, p2}, Lazm;-><init>(Lazl;Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    const-string v1, "onMessageReceived: %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lazl;->g:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0}, Lazw;->a([B)Lazw;

    move-result-object v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v2, "Received a protobuf: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    iget v1, v0, Lazw;->b:I

    if-nez v1, :cond_0

    iget-object v0, v0, Lazw;->c:Ljava/lang/String;

    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "requestId"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v2, v8

    if-eqz v0, :cond_0

    const-wide/16 v4, 0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lazl;->h:Lazk;

    invoke-virtual {v0, v1}, Lazk;->b(Lorg/json/JSONObject;)V

    invoke-direct {p0}, Lazl;->b()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v2, "Received an unparseable protobuf: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lizj;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x2

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lazl;->h:Lazk;

    invoke-virtual {v0, v1}, Lazk;->a(Lorg/json/JSONObject;)V

    invoke-direct {p0}, Lazl;->c()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v1

    const-string v2, "Failed to parse response: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized request ID: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    invoke-static {}, Lazj;->b()Laye;

    move-result-object v0

    const-string v1, "Device filter disconnected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
