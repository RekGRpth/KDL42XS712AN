.class final Lavg;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lavd;


# direct methods
.method constructor <init>(Lavd;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lavg;->c:Lavd;

    iput-object p2, p0, Lavg;->a:Ljava/lang/Runnable;

    iput-object p3, p0, Lavg;->b:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lavg;->c:Lavd;

    iget-object v0, v0, Lavd;->c:Lavh;

    iget-object v1, p0, Lavg;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lavh;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "OtpFlow"

    const-string v2, "Unexpected exception during OTP generation."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lavg;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lavg;->c:Lavd;

    iget-object v0, v0, Lavd;->d:Landroid/os/Handler;

    iget-object v1, p0, Lavg;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lavg;->c:Lavd;

    iget-object v0, v0, Lavd;->b:Lavn;

    invoke-interface {v0}, Lavn;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lavg;->c:Lavd;

    iget-object v0, v0, Lavd;->b:Lavn;

    iget-object v1, p0, Lavg;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lavn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 4

    iget-object v0, p0, Lavg;->c:Lavd;

    iget-object v0, v0, Lavd;->d:Landroid/os/Handler;

    iget-object v1, p0, Lavg;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
