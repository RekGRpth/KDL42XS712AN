.class public final Lehp;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:[Lehq;

.field public c:I

.field public d:I

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-wide v2, p0, Lehp;->a:J

    invoke-static {}, Lehq;->c()[Lehq;

    move-result-object v0

    iput-object v0, p0, Lehp;->b:[Lehq;

    iput v1, p0, Lehp;->c:I

    iput v1, p0, Lehp;->d:I

    iput-wide v2, p0, Lehp;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lehp;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const-wide/16 v4, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v1, p0, Lehp;->a:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-wide v2, p0, Lehp;->a:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lehp;->b:[Lehq;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lehp;->b:[Lehq;

    array-length v1, v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lehp;->b:[Lehq;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lehp;->b:[Lehq;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x3

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    :cond_3
    iget v1, p0, Lehp;->c:I

    if-eqz v1, :cond_4

    const/4 v1, 0x4

    iget v2, p0, Lehp;->c:I

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lehp;->d:I

    if-eqz v1, :cond_5

    const/4 v1, 0x5

    iget v2, p0, Lehp;->d:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-wide v1, p0, Lehp;->e:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_6

    const/4 v1, 0x6

    iget-wide v2, p0, Lehp;->e:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lehp;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lehp;->a:J

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lehp;->b:[Lehq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lehq;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lehp;->b:[Lehq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lehq;

    invoke-direct {v3}, Lehq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lehp;->b:[Lehq;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lehq;

    invoke-direct {v3}, Lehq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lehp;->b:[Lehq;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->i()I

    move-result v0

    iput v0, p0, Lehp;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehp;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lehp;->e:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x25 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const-wide/16 v3, 0x0

    iget-wide v0, p0, Lehp;->a:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-wide v1, p0, Lehp;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_0
    iget-object v0, p0, Lehp;->b:[Lehq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lehp;->b:[Lehq;

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lehp;->b:[Lehq;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lehp;->b:[Lehq;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lizn;->a(ILizs;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget v0, p0, Lehp;->c:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lehp;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->b(II)V

    :cond_3
    iget v0, p0, Lehp;->d:I

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lehp;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_4
    iget-wide v0, p0, Lehp;->e:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lehp;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_5
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehp;

    iget-wide v2, p0, Lehp;->a:J

    iget-wide v4, p1, Lehp;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lehp;->b:[Lehq;

    iget-object v3, p1, Lehp;->b:[Lehq;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lehp;->c:I

    iget v3, p1, Lehp;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lehp;->d:I

    iget v3, p1, Lehp;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lehp;->e:J

    iget-wide v4, p1, Lehp;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget-wide v0, p0, Lehp;->a:J

    iget-wide v2, p0, Lehp;->a:J

    ushr-long/2addr v2, v5

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lehp;->b:[Lehq;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehp;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehp;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehp;->e:J

    iget-wide v3, p0, Lehp;->e:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method
