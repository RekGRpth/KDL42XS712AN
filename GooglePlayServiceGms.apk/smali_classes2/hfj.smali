.class final Lhfj;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhfd;


# direct methods
.method constructor <init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhfj;->f:Lhfd;

    iput-object p4, p0, Lhfj;->d:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    iput-object p5, p0, Lhfj;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/16 v0, 0x12

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    iget-object v0, p0, Lhfj;->d:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;->b()Ljas;

    move-result-object v0

    iget-object v1, p0, Lhfj;->f:Lhfd;

    invoke-static {v1}, Lhfd;->b(Lhfd;)Lhef;

    move-result-object v1

    iget-object v2, p0, Lhfj;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_0
    iput-object v1, v0, Ljas;->a:Ljan;

    iget-object v1, p0, Lhfj;->f:Lhfd;

    iget-object v1, p0, Lhfj;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lhfd;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v1

    iput-boolean v1, v0, Ljas;->b:Z

    iget-object v1, p0, Lhfj;->f:Lhfd;

    invoke-static {v1}, Lhfd;->c(Lhfd;)Lhfo;

    move-result-object v1

    iget-object v2, p0, Lhfj;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2}, Lhfd;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lhfo;->a:Landroid/content/Context;

    new-instance v4, Lhfw;

    invoke-direct {v4, v1, v2, p1, v0}, Lhfw;-><init>(Lhfo;Ljava/lang/String;Lhgm;Ljas;)V

    const-string v0, "create_wallet_objects"

    invoke-static {v3, v4, v0}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
