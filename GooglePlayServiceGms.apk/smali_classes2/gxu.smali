.class public final Lgxu;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Ljava/lang/CharSequence;

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Ljava/util/List;

.field private final f:Landroid/view/LayoutInflater;

.field private g:Lgxv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "phoneNumberSources are required"

    invoke-static {v0, v1}, Lwe;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lgxu;->c:Landroid/content/Context;

    const v0, 0x7f04014e    # com.google.android.gms.R.layout.wallet_row_phone_number_hint_spinner

    iput v0, p0, Lgxu;->d:I

    iput-object p2, p0, Lgxu;->e:Ljava/util/List;

    iget-object v0, p0, Lgxu;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgxu;->f:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgxu;->a:Ljava/util/ArrayList;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    iget-object v0, p0, Lgxu;->f:Landroid/view/LayoutInflater;

    iget v1, p0, Lgxu;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1}, Lgxu;->a(I)Lgur;

    move-result-object v1

    const v0, 0x7f0a0355    # com.google.android.gms.R.id.contact_name

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lgur;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgxu;->c:Landroid/content/Context;

    const v3, 0x104000e    # android.R.string.unknownName

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f0a0317    # com.google.android.gms.R.id.phone_number

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lgur;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    invoke-virtual {v1}, Lgur;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(I)Lgur;
    .locals 1

    iget-object v0, p0, Lgxu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgur;

    return-object v0
.end method

.method static synthetic a(Lgxu;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lgxu;->e:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lgxu;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lgxu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lgxu;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lgxu;->g:Lgxv;

    if-nez v0, :cond_0

    new-instance v0, Lgxv;

    invoke-direct {v0, p0}, Lgxv;-><init>(Lgxu;)V

    iput-object v0, p0, Lgxu;->g:Lgxv;

    :cond_0
    iget-object v0, p0, Lgxu;->g:Lgxv;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lgxu;->a(I)Lgur;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lgxu;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
