.class public final Lhjw;
.super Lhin;
.source "SourceFile"


# instance fields
.field final g:[Lhin;

.field h:Lhin;

.field final i:Lhoj;

.field final j:Lhte;


# direct methods
.method public constructor <init>(Lidu;Lhof;Lhoj;Lhjf;Lhkl;Lhkd;Liha;Lhte;Lhkr;)V
    .locals 13

    const-string v2, "NetworkCollector"

    new-instance v6, Lhiq;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Lhiq;-><init>(Lhoj;)V

    sget-object v7, Lhir;->a:Lhir;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    invoke-direct/range {v1 .. v7}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    move-object/from16 v0, p3

    iput-object v0, p0, Lhjw;->i:Lhoj;

    new-instance v1, Lhjd;

    iget-object v5, p0, Lhjw;->e:Lhiq;

    invoke-static {p1}, Lhjd;->a(Lidu;)Livi;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v8}, Lhjd;-><init>(Lidu;Lhof;Lhkl;Lhiq;Livi;Lhkd;Lhjf;)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lhjw;->j:Lhte;

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lhiu;

    iget-object v3, p0, Lhjw;->e:Lhiq;

    move-object/from16 v0, p5

    invoke-direct {v2, p1, p2, v0, v3}, Lhiu;-><init>(Lidu;Lhof;Lhkl;Lhiq;)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lhjn;

    iget-object v6, p0, Lhjw;->e:Lhiq;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v7}, Lhjn;-><init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lhja;

    iget-object v6, p0, Lhjw;->e:Lhiq;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    invoke-direct/range {v2 .. v8}, Lhja;-><init>(Lidu;Lhof;Lhkl;Lhiq;Liha;Lhkr;)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lhkg;

    iget-object v6, p0, Lhjw;->e:Lhiq;

    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    move-object v8, v1

    move-object/from16 v9, p7

    move-object/from16 v10, p9

    invoke-direct/range {v2 .. v11}, Lhkg;-><init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhjd;Liha;Lhkr;Ljava/util/Random;)V

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lhka;

    invoke-virtual/range {p9 .. p9}, Lhkr;->b()Lhlb;

    move-result-object v4

    iget-object v6, p0, Lhjw;->e:Lhiq;

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lhka;-><init>(Lidu;Lhof;Lhlb;Lhkl;Lhiq;Liha;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    new-array v1, v1, [Lhin;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lhin;

    iput-object v1, p0, Lhjw;->g:[Lhin;

    iget-object v1, p0, Lhjw;->g:[Lhin;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, p0, Lhjw;->h:Lhin;

    return-void
.end method

.method private a(Lhin;)V
    .locals 5

    iget-object v0, p0, Lhjw;->h:Lhin;

    if-eq v0, p1, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjw;->a:Ljava/lang/String;

    const-string v1, "Current collector changed from %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lhjw;->h:Lhin;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhjw;->h:Lhin;

    invoke-virtual {v0}, Lhin;->a()V

    iget-object v0, p0, Lhjw;->h:Lhin;

    invoke-virtual {v0}, Lhin;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lhjw;->g:[Lhin;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget-object v5, p0, Lhjw;->h:Lhin;

    if-eq v4, v5, :cond_2

    invoke-virtual {v4}, Lhin;->a()V

    invoke-virtual {v4}, Lhin;->e()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0, v4}, Lhjw;->a(Lhin;)V

    iput-object v4, p0, Lhjw;->h:Lhin;

    :cond_0
    iget-object v0, p0, Lhjw;->h:Lhin;

    iget-object v2, p0, Lhjw;->g:[Lhin;

    aget-object v2, v2, v1

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lhjw;->h:Lhin;

    invoke-virtual {v0}, Lhin;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhjw;->g:[Lhin;

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lhjw;->a(Lhin;)V

    iget-object v0, p0, Lhjw;->g:[Lhin;

    aget-object v0, v0, v1

    iput-object v0, p0, Lhjw;->h:Lhin;

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(IIZ)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2, p3}, Lhin;->a(IIZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(Lhtf;)V
    .locals 4

    iget-object v0, p0, Lhjw;->j:Lhte;

    invoke-virtual {v0}, Lhte;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(Lhtf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    :cond_1
    return-void
.end method

.method final a(Lhuv;)V
    .locals 5

    if-eqz p1, :cond_1

    iget-object v0, p0, Lhjw;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lhuv;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v2, p0, Lhjw;->i:Lhoj;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v2, Lhoj;->b:Lhok;

    invoke-virtual {p1, v1}, Lhuv;->a(I)Lhut;

    move-result-object v3

    iget-wide v3, v3, Lhut;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-eqz v0, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lhol;->a(F)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lidr;)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(Lidr;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(Lids;)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(Lids;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(Lidw;Ljava/lang/Object;)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2}, Lhin;->a(Lidw;Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(Livi;)V
    .locals 1

    iget-object v0, p0, Lhjw;->h:Lhin;

    invoke-virtual {v0, p1}, Lhin;->a(Livi;)V

    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->a(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final b(Lhuv;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhjw;->i:Lhoj;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lhjw;->i:Lhoj;

    if-eqz p1, :cond_0

    iget-object v0, v2, Lhoj;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v3

    move v0, v1

    :goto_0
    iget-object v5, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    invoke-virtual {p1, v0}, Lhuv;->a(I)Lhut;

    move-result-object v5

    iget-wide v5, v5, Lhut;->b:J

    invoke-virtual {v2, v5, v6, v3, v4}, Lhoj;->a(JJ)Lhol;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lhjw;->g:[Lhin;

    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3, p1}, Lhin;->b(Lhuv;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final b(Z)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->b(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final c(Z)V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Lhin;->c(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lhjw;->h:Lhin;

    invoke-virtual {v0}, Lhin;->e()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 4

    iget-object v1, p0, Lhjw;->g:[Lhin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lhin;->f()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lhjw;->a()V

    return-void
.end method
