.class public final Lbsg;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/TrashResourceRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbsg;->c:Lcom/google/android/gms/drive/internal/TrashResourceRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 2

    iget-object v0, p0, Lbsg;->c:Lcom/google/android/gms/drive/internal/TrashResourceRequest;

    const-string v1, "Invalid trash request."

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsg;->c:Lcom/google/android/gms/drive/internal/TrashResourceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/TrashResourceRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid trash request."

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsg;->b:Lbrc;

    iget-object v1, p0, Lbsg;->c:Lcom/google/android/gms/drive/internal/TrashResourceRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/TrashResourceRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-interface {v0, v1}, Lbrc;->c(Lcom/google/android/gms/drive/DriveId;)V

    iget-object v0, p0, Lbsg;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->a()V

    return-void
.end method
