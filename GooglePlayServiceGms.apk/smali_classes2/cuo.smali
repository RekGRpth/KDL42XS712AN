.class final Lcuo;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final A:[Ljava/lang/String;

.field private static final B:Ljava/util/HashMap;

.field private static final C:Ljava/util/HashMap;

.field private static final D:Ldhu;

.field private static final m:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final n:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final o:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final p:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final q:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final r:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final s:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final t:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final u:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final v:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final w:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final x:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final y:[Ljava/lang/String;

.field private static final z:[Ljava/lang/String;


# instance fields
.field private final E:Ldmn;

.field private final F:Ldmo;

.field private final G:Ldlz;

.field private final H:Lbmi;

.field private final I:Lcvy;

.field private final J:Lcvy;

.field private final K:Lcvz;

.field final a:Lcve;

.field final b:Lcve;

.field final c:Lcve;

.field final d:Lcve;

.field final e:Lcve;

.field final f:Lcve;

.field final g:Lcve;

.field final h:Lcve;

.field final i:Lcve;

.field final j:Lcve;

.field final k:Lcve;

.field final l:[Lcve;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v4, 0x7f0d0080    # com.google.android.gms.R.dimen.games_image_download_size_game_icon

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->m:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->n:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->o:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->p:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->q:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->r:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->s:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->t:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->u:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->v:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->w:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcuo;->x:Ljava/util/concurrent/locks/ReentrantLock;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "external_game_id"

    aput-object v1, v0, v2

    sput-object v0, Lcuo;->y:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "REPLACE_ME"

    aput-object v1, v0, v2

    sput-object v0, Lcuo;->z:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "external_game_id"

    aput-object v1, v0, v2

    const-string v1, "muted"

    aput-object v1, v0, v3

    sput-object v0, Lcuo;->A:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcuo;->B:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcuo;->C:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcus;

    const-string v2, "game_icon_image_url"

    invoke-direct {v1, v2, v4}, Lcus;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcus;

    const-string v2, "game_hi_res_image_url"

    const v3, 0x7f0d0081    # com.google.android.gms.R.dimen.games_image_download_size_game_hi_res

    invoke-direct {v1, v2, v3}, Lcus;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcuo;->B:Ljava/util/HashMap;

    const-string v2, "ICON"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcus;

    const-string v1, "badge_icon_image_url"

    invoke-direct {v0, v1, v4}, Lcus;-><init>(Ljava/lang/String;I)V

    sget-object v1, Lcuo;->C:Ljava/util/HashMap;

    const-string v2, "ICON"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ldhu;->a()Ldhv;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "display_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "primary_category"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "secondary_category"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_description"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "developer_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_icon_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_hi_res_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_hi_res_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "featured_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "featured_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "play_enabled_game"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "last_played_server_time"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "gameplay_acl_status"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "availability"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "owned"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "achievement_total_count"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "achievement_unlocked_count"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "leaderboard_count"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "formatted_price"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "price_micros"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "formatted_full_price"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "full_price_micros"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "muted"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "installed"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "package_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "real_time_support"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "turn_based_support"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "badge_type"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "badge_title"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "badge_description"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "badge_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    invoke-virtual {v0}, Ldhv;->a()Ldhu;

    move-result-object v0

    sput-object v0, Lcuo;->D:Ldhu;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;Lbmi;Lbmi;)V
    .locals 3

    const-string v0, "GameAgent"

    sget-object v1, Lcuo;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Ldmn;

    invoke-direct {v0, p2}, Ldmn;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuo;->E:Ldmn;

    new-instance v0, Ldmo;

    invoke-direct {v0, p3}, Ldmo;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuo;->F:Ldmo;

    new-instance v0, Ldlz;

    invoke-direct {v0, p3}, Ldlz;-><init>(Lbmi;)V

    iput-object v0, p0, Lcuo;->G:Ldlz;

    iput-object p4, p0, Lcuo;->H:Lbmi;

    new-instance v0, Lcvy;

    sget-object v1, Lcuo;->D:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcvy;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcuo;->I:Lcvy;

    new-instance v0, Lcvy;

    sget-object v1, Lcuo;->D:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcvy;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcuo;->J:Lcvy;

    new-instance v0, Lcvz;

    sget-object v1, Lcuo;->D:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcvz;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcuo;->K:Lcvz;

    new-instance v0, Lcve;

    const-string v1, "FeaturedGames"

    sget-object v2, Lcuo;->n:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->a:Lcve;

    new-instance v0, Lcve;

    const-string v1, "MultiplayerGames"

    sget-object v2, Lcuo;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->b:Lcve;

    new-instance v0, Lcve;

    const-string v1, "PopularGames"

    sget-object v2, Lcuo;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->c:Lcve;

    new-instance v0, Lcve;

    const-string v1, "RecentlyPlayedGames"

    sget-object v2, Lcuo;->q:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->d:Lcve;

    new-instance v0, Lcve;

    const-string v1, "RecommendedGames"

    sget-object v2, Lcuo;->r:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->e:Lcve;

    new-instance v0, Lcve;

    const-string v1, "DownloadedGames"

    sget-object v2, Lcuo;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->f:Lcve;

    new-instance v0, Lcve;

    const-string v1, "InstalledGames"

    sget-object v2, Lcuo;->t:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->g:Lcve;

    new-instance v0, Lcve;

    const-string v1, "HiddenGames"

    sget-object v2, Lcuo;->u:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->h:Lcve;

    new-instance v0, Lcve;

    const-string v1, "GameSearch"

    sget-object v2, Lcuo;->v:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->i:Lcve;

    new-instance v0, Lcve;

    const-string v1, "GameSearchSuggestions"

    sget-object v2, Lcuo;->w:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->j:Lcve;

    new-instance v0, Lcve;

    const-string v1, "NamedGamesCollection"

    sget-object v2, Lcuo;->x:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcuo;->k:Lcve;

    const/16 v0, 0x8

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcuo;->a:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcuo;->b:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcuo;->c:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcuo;->e:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcuo;->f:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcuo;->g:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcuo;->h:Lcve;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcuo;->l:[Lcve;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZLjava/util/ArrayList;)J
    .locals 11

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZZLjava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZZLjava/util/ArrayList;)J
    .locals 13

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    invoke-static {p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual/range {p10 .. p10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move-object/from16 v0, p3

    invoke-static {p1, v0}, Lcuo;->a(Landroid/content/Context;Ldmf;)Ldnb;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Lcuo;->a(Ldnb;Ldno;)Ldnq;

    move-result-object v4

    const/4 v9, 0x0

    move-object v1, p1

    move-object/from16 v2, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-static/range {v1 .. v9}, Lcuo;->a(Landroid/content/Context;Ldmf;Ldnb;Ldnq;Ljava/lang/Long;Ljava/lang/Integer;ZZZ)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "gameplay_acl_status"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    if-eqz p9, :cond_1

    const-string v2, "muted"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {p2}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v7, "instance_game_id=?"

    sget-object v8, Lcuo;->z:[Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v11}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p10 .. p10}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Ldmf;->getInstances()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v2, v1

    :goto_1
    if-ge v2, v9, :cond_3

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldnb;

    iget-object v10, v1, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v1}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v10, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v1, "package_name"

    invoke-virtual {v10, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Ledw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    const-string v12, "installed"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v10, v12, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_2
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v10, "instance_game_id"

    invoke-virtual {v1, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "_id=?"

    sget-object v5, Lcuo;->z:[Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "target_instance"

    add-int/2addr v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    move-object/from16 v0, p10

    invoke-static {p2, v4, v0, v11}, Lcuo;->a(Lcom/google/android/gms/common/server/ClientContext;Ldnq;Ljava/util/ArrayList;I)V

    if-nez p3, :cond_6

    const-wide/16 v1, -0x1

    goto/16 :goto_0

    :cond_5
    if-eqz v4, :cond_4

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "platform_type"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "instance_display_name"

    const-string v8, "display_name"

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ldnq;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Ledw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    const-string v8, "package_name"

    invoke-virtual {v2, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "installed"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "instance_game_id"

    invoke-virtual {v1, v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "_id=?"

    sget-object v3, Lcuo;->z:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "target_instance"

    invoke-virtual {v1, v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-virtual/range {p3 .. p3}, Ldmf;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZLjava/util/ArrayList;)J
    .locals 11

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZZLjava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Ldmf;Ldnb;Ldnq;Ljava/lang/Long;Ljava/lang/Integer;ZZZ)Landroid/content/ContentValues;
    .locals 5

    if-eqz p1, :cond_7

    iget-object v0, p1, Lbni;->a:Landroid/content/ContentValues;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "external_game_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "display_name"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "game_description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "developer_name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "achievement_total_count"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "leaderboard_count"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "muted"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcum;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "primary_category"

    invoke-virtual {p1}, Ldmf;->getCategory()Ldmg;

    move-result-object v2

    invoke-virtual {v2}, Ldmg;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "secondary_category"

    invoke-virtual {p1}, Ldmf;->getCategory()Ldmg;

    move-result-object v2

    invoke-virtual {v2}, Ldmg;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "play_enabled_game"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz p6, :cond_0

    const-string v1, "metadata_version"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    if-eqz p7, :cond_1

    const-string v1, "metadata_sync_requested"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    if-eqz p8, :cond_2

    const-string v1, "achievement_unlocked_count"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    :goto_0
    const-string v1, "gameplay_acl_status"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz p4, :cond_3

    const-string v1, "last_played_server_time"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    if-eqz p3, :cond_b

    iget-object v1, p3, Lbni;->a:Landroid/content/ContentValues;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "availability"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "owned"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "formatted_price"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "price_micros"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "formatted_full_price"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "full_price_micros"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcum;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    :goto_1
    if-eqz p8, :cond_6

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    iget-object v2, p2, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-virtual {p2}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v1

    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    if-eqz p3, :cond_5

    invoke-virtual {p3}, Ldnq;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v2, "installed"

    if-eqz v1, :cond_c

    invoke-static {p0, v1}, Ledw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_6
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Ldmf;->getAssets()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcuo;->a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    :goto_3
    return-object v0

    :cond_7
    if-eqz p3, :cond_a

    iget-object v0, p3, Lbni;->a:Landroid/content/ContentValues;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "external_game_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "display_name"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "game_description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "developer_name"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcum;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p3, Lbni;->a:Landroid/content/ContentValues;

    const-string v2, "primary_category"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_8

    const-string v2, "primary_category"

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    const-string v2, "secondary_category"

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v1, "play_enabled_game"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "achievement_total_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz p8, :cond_9

    const-string v1, "achievement_unlocked_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_9
    const-string v1, "leaderboard_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "metadata_version"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "GameAgent"

    const-string v1, "Received application with no app data and no Market data!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_b
    const-string v1, "availability"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "owned"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_d
    invoke-virtual {p3}, Ldnq;->getImages()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcuo;->a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    goto/16 :goto_3
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvy;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZLcuq;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 17

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcvy;->a(Ljava/lang/Object;)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v4

    if-eqz p11, :cond_0

    invoke-virtual/range {p3 .. p4}, Lcvy;->c(Ljava/lang/Object;)V

    :cond_0
    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move/from16 v6, p9

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcvy;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    const/4 v15, 0x0

    if-nez p10, :cond_2

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4, v5}, Lcvy;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4, v5}, Lcvy;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v15

    :cond_3
    invoke-static/range {p1 .. p1}, Lcuo;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    if-eqz p5, :cond_5

    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcuo;->F:Ldmo;

    const-string v10, "android:7"

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v16, "ANDROID"

    move-object/from16 v7, p2

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v6 .. v16}, Ldmo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ldml;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    if-eqz p12, :cond_4

    invoke-virtual {v2}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p12

    invoke-interface {v0, v3}, Lcuq;->a(Ljava/util/ArrayList;)V

    :cond_4
    invoke-virtual {v2}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v2}, Ldml;->b()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-wide v13, v4

    invoke-direct/range {v6 .. v14}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Lcwf;Ljava/lang/String;J)V

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcuo;->F:Ldmo;

    const-string v10, "android:7"

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v16, "ANDROID"

    move-object/from16 v9, p7

    invoke-static/range {v9 .. v16}, Ldmo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v6, v2, Ldmo;->a:Lbmi;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const-class v11, Ldml;

    move-object/from16 v7, p2

    invoke-virtual/range {v6 .. v11}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Ldml;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "GameAgent"

    invoke-static {v2, v3}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v4, v5}, Lcvy;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {p3 .. p4}, Lcvy;->d(Ljava/lang/Object;)V

    :cond_6
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 16

    if-eqz p6, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcvy;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lcuo;->a()V

    :cond_0
    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcvy;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    const-string v2, "played"

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcvy;->a(Ljava/lang/Object;JIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    const-string v2, "played"

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcvy;->a(Ljava/lang/Object;)V

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    const-string v2, "played"

    invoke-virtual {v1, v2, v3, v4}, Lcvy;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    const-string v2, "played"

    invoke-virtual {v1, v2, v3, v4}, Lcvy;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v14

    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcuo;->F:Ldmo;

    const-string v7, "me"

    const-string v8, "played"

    const-string v9, "android:7"

    invoke-static/range {p1 .. p1}, Lcuo;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const-string v15, "ANDROID"

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v15}, Ldmo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ldml;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    invoke-virtual {v12}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GameAgent"

    const-string v3, "Unable to retrieve list of recently played games"

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "GameAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_4
    invoke-static/range {p2 .. p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "last_played_server_time > 0"

    const/4 v4, 0x0

    const-string v5, "last_played_server_time DESC"

    const/16 v6, 0x1f4

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :cond_5
    new-instance v5, Lcuu;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    invoke-direct/range {v5 .. v10}, Lcuu;-><init>(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ZB)V

    invoke-virtual {v5, v1}, Lcuu;->a(Ljava/util/ArrayList;)V

    if-eqz v12, :cond_6

    invoke-virtual {v12}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v12}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcvy;->a(Ljava/lang/Object;)V

    invoke-virtual {v12}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v12}, Ldml;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcuo;->J:Lcvy;

    const-string v11, "played"

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide v12, v3

    invoke-direct/range {v5 .. v13}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Lcwf;Ljava/lang/String;J)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->J:Lcvy;

    const-string v2, "played"

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v12}, Ldml;->getItems()Ljava/util/ArrayList;

    move-result-object v13

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_9

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmr;

    invoke-virtual {v1}, Ldmr;->c()Ljava/lang/Integer;

    move-result-object v7

    if-nez v7, :cond_8

    invoke-virtual {v1}, Ldmr;->getGamesData()Ldmf;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ldmf;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v5}, Lcum;->a(Ljava/util/ArrayList;)Ldmj;

    move-result-object v11

    const/4 v10, 0x0

    const/4 v1, 0x1

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    if-nez v10, :cond_a

    if-eqz v1, :cond_b

    :cond_a
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcuo;->G:Ldlz;

    const-string v7, "me"

    const/4 v1, 0x0

    invoke-static {v1}, Lddy;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p1 .. p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v11}, Ldlz;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldmj;)Ldob;

    move-result-object v2

    invoke-virtual {v2}, Ldob;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    invoke-virtual {v2}, Ldob;->b()Ljava/lang/String;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v10

    goto :goto_3

    :catch_1
    move-exception v1

    const-string v2, "GameAgent"

    const-string v5, "Unable to retrieve list of achievements"

    invoke-static {v2, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "GameAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_d

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldoa;

    invoke-virtual {v1}, Ldoa;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    invoke-virtual {v14, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    :cond_d
    const/4 v1, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v1

    :goto_5
    if-ge v5, v6, :cond_6

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmr;

    invoke-virtual {v1}, Ldmr;->getGamesData()Ldmf;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {v1}, Ldmr;->getGamesData()Ldmf;

    move-result-object v2

    invoke-virtual {v2}, Ldmf;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v1, Lbnk;->a:Ljava/util/HashMap;

    const-string v7, "unlockedAchievementCount"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/ArrayList;)Lcup;
    .locals 10

    const/4 v8, 0x0

    const-string v6, "ANDROID"

    const/4 v5, 0x0

    const/4 v0, 0x1

    new-instance v9, Lcup;

    invoke-direct {v9}, Lcup;-><init>()V

    invoke-static {p4}, Lcum;->a(Ljava/util/ArrayList;)Ldmj;

    move-result-object v7

    :goto_0
    if-nez v0, :cond_0

    if-eqz v5, :cond_3

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcuo;->F:Ldmo;

    const-string v2, "android:7"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p2

    invoke-virtual/range {v0 .. v7}, Ldmo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldmj;)Ldmk;

    move-result-object v0

    invoke-virtual {v0}, Ldmk;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ldmk;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldmk;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, v9, Lcup;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-virtual {v0}, Ldmk;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ldmk;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, v9, Lcup;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v0, v8

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v0, "GameAgent"

    const-string v1, "Unable to retrieve applications from network"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object v9
.end method

.method static synthetic a(Lcuo;)Ldmn;
    .locals 1

    iget-object v0, p0, Lcuo;->E:Ldmn;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ldmf;)Ldnb;
    .locals 9

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldmf;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldmf;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v2, -0x1

    invoke-virtual {p1}, Ldmf;->getInstances()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v3

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    invoke-virtual {v0}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v7, v6, Lbni;->a:Landroid/content/ContentValues;

    const-string v8, "package_name"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Ledw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    if-gez v2, :cond_4

    invoke-virtual {v6}, Ldnc;->b()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private static a(Ldnb;Ldno;)Ldnq;
    .locals 7

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldno;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldno;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p1}, Ldno;->getInstances()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnq;

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Ldnb;->getAndroidInstance()Ldnc;

    move-result-object v0

    iget-object v0, v0, Lbni;->a:Landroid/content/ContentValues;

    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnq;

    invoke-virtual {v0}, Ldnq;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnq;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Leeo;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PANO"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01f7    # com.google.android.gms.R.string.games_device_category

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcuo;->J:Lcvy;

    const-string v1, "played"

    invoke-virtual {v0, v1}, Lcvy;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)V
    .locals 9

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcur;

    iget-object v5, v0, Lcur;->a:Ljava/lang/String;

    invoke-static {p1, v5}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "metadata_version"

    iget-wide v7, v0, Lcur;->c:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "metadata_sync_requested"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v5

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GameAgent"

    invoke-static {v0, v3, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Lcwf;Ljava/lang/String;J)V
    .locals 21

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {p2 .. p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v19

    const/4 v2, 0x0

    move v11, v2

    :goto_0
    if-ge v11, v12, :cond_3

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldmr;

    invoke-virtual {v2}, Ldmr;->getGamesData()Ldmf;

    move-result-object v3

    invoke-virtual {v2}, Ldmr;->getMarketData()Ldno;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcuo;->a(Landroid/content/Context;Ldmf;)Ldnb;

    move-result-object v4

    invoke-static {v4, v5}, Lcuo;->a(Ldnb;Ldno;)Ldnq;

    move-result-object v5

    invoke-virtual {v2}, Ldmr;->b()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2}, Ldmr;->c()Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v10}, Lcuo;->a(Landroid/content/Context;Ldmf;Ldnb;Ldnq;Ljava/lang/Long;Ljava/lang/Integer;ZZZ)Landroid/content/ContentValues;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v2, "installed"

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "installed"

    invoke-virtual {v4, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const-string v2, "game_icon_image_url"

    invoke-virtual {v4, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "game_hi_res_image_url"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "featured_image_url"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-static {v0, v2, v13}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v0, v3, v13}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-static {v0, v6, v13}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v5}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v2, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v3, v2

    :goto_1
    if-ge v3, v9, :cond_2

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldnp;

    iget-object v10, v2, Lbni;->a:Landroid/content/ContentValues;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lcuo;->a(Landroid/content/Context;Ldnp;Landroid/content/ContentValues;)V

    const-string v2, "badge_icon_image_url"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v0, v2, v13}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v2

    new-instance v20, Landroid/content/ContentValues;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_1
    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_7

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "GameAgent"

    invoke-static {v2, v13, v3}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v2, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v2

    :goto_6
    if-ge v4, v6, :cond_8

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    const-string v7, "game_icon_image_url"

    const-string v8, "game_icon_image_uri"

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v7, "game_hi_res_image_url"

    const-string v8, "game_hi_res_image_uri"

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v7, "featured_image_url"

    const-string v8, "featured_image_uri"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v7, "badge_icon_image_url"

    const-string v8, "badge_icon_image_uri"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_6

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    :cond_8
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, v18

    move-object/from16 v7, p4

    move-wide/from16 v9, p7

    invoke-virtual/range {v2 .. v10}, Lcwf;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ldnp;Landroid/content/ContentValues;)V
    .locals 6

    invoke-virtual {p1}, Ldnp;->getImages()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmz;

    sget-object v1, Lcuo;->C:Ljava/util/HashMap;

    invoke-virtual {v0}, Ldmz;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcus;

    if-eqz v1, :cond_0

    new-instance v5, Lbkg;

    invoke-virtual {v0}, Ldmz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lbkg;-><init>(Ljava/lang/String;)V

    iget v0, v1, Lcus;->b:I

    invoke-virtual {v5, p0, v0}, Lbkg;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lcus;->a:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V
    .locals 11

    const/4 v4, 0x0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmz;

    sget-object v1, Lcuo;->B:Ljava/util/HashMap;

    invoke-virtual {v0}, Ldmz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v3, v4

    :goto_1
    if-ge v3, v8, :cond_3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcus;

    new-instance v9, Lbkg;

    invoke-virtual {v0}, Ldmz;->c()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lbkg;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcus;->b:I

    invoke-virtual {v9, p0, v10}, Lbkg;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v9

    invoke-virtual {v9}, Lbkh;->a()Ljava/lang/String;

    move-result-object v9

    iget-object v2, v2, Lcus;->a:Ljava/lang/String;

    invoke-virtual {p2, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    const-string v1, "FEATURE_GRAPHIC"

    invoke-virtual {v0}, Ldmz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lbkg;

    invoke-virtual {v0}, Ldmz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbkg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lbkg;->a(I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "featured_image_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/server/ClientContext;Ldnq;Ljava/util/ArrayList;I)V
    .locals 10

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p0}, Ldiy;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Ldnq;->getBadges()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnp;

    iget-object v6, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v0}, Ldnp;->getImages()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_3

    :cond_2
    const-string v6, "GameAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Badge "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " does not have exactly one image"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const-string v8, "badge_icon_image_url"

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v6, "badge_game_id"

    invoke-virtual {v0, v6, p3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    invoke-static {p1, p2}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcuo;->y:[Ljava/lang/String;

    const-string v3, "package_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v5

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J
    .locals 12

    const/4 v11, 0x0

    const/4 v1, 0x1

    const-wide/16 v2, -0x1

    invoke-static {p2, p3}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-string v0, "last_connection_local_time"

    invoke-static {p1, v6, v0}, Lcum;->c(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    const-string v0, "GameAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Attempting to update connection time for game "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " which has no local record!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v7

    cmp-long v0, v7, v2

    if-nez v0, :cond_0

    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unable to resolve ID for game "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v2

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v2

    sub-long v7, v2, v4

    const-wide/32 v9, 0x2932e00

    cmp-long v0, v7, v9

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "last_connection_local_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v0, :cond_1

    const-string v2, "metadata_sync_requested"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v6, v7, v11, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz v0, :cond_2

    invoke-static {p2}, Ldrn;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    :cond_2
    new-instance v0, Lcuv;

    invoke-direct {v0, p0, p2}, Lcuv;-><init>(Lcuo;Lcom/google/android/gms/common/server/ClientContext;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcuo;->a()V

    move-wide v0, v4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IIZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    move/from16 v0, p5

    invoke-virtual {p0, v0}, Lcuo;->a(I)Lcve;

    move-result-object v1

    invoke-virtual {v1}, Lcve;->d()V

    const/4 v9, 0x1

    const/4 v13, 0x0

    iget-object v7, p0, Lcuo;->I:Lcvy;

    packed-switch p5, :pswitch_data_0

    const-string v1, "GameAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected collection type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    const-string v5, "featured"

    const/4 v6, 0x0

    move-object v4, v7

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v7, p3

    move-object v8, v5

    move/from16 v10, p4

    move/from16 v11, p6

    move/from16 v12, p7

    invoke-direct/range {v1 .. v13}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvy;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZLcuq;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string v5, "multiplayer"

    const/4 v6, 0x0

    move-object v4, v7

    goto :goto_1

    :pswitch_2
    const-string v5, "all"

    const/4 v6, 0x0

    move-object v4, v7

    goto :goto_1

    :pswitch_3
    const-string v5, "played"

    iget-object v4, p0, Lcuo;->J:Lcvy;

    const/4 v6, 0x1

    const/4 v9, 0x0

    goto :goto_1

    :pswitch_4
    const-string v5, "recommended"

    const/4 v6, 0x1

    move-object v4, v7

    goto :goto_1

    :pswitch_5
    const-string v5, "downloaded"

    const/4 v6, 0x1

    move-object v4, v7

    goto :goto_1

    :pswitch_6
    const-string v5, "installed"

    const/4 v6, 0x1

    move-object v4, v7

    goto :goto_1

    :pswitch_7
    const-string v10, "hidden"

    const/4 v8, 0x1

    const/4 v9, 0x0

    new-instance v1, Lcuu;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v6}, Lcuu;-><init>(Lcuo;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ZB)V

    move-object v4, v7

    move-object v13, v1

    move v6, v8

    move-object v5, v10

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->h:Lcve;

    invoke-virtual {v1}, Lcve;->d()V

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v14

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcuo;->I:Lcvy;

    const-string v4, "hidden"

    invoke-virtual {v1, v4, v14, v15}, Lcvy;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v1

    move v12, v3

    :goto_0
    if-nez v11, :cond_7

    if-nez v2, :cond_0

    if-eqz v1, :cond_7

    :cond_0
    const/4 v13, 0x0

    const/4 v10, 0x0

    const/16 v5, 0x19

    const/4 v6, 0x7

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    :try_start_0
    invoke-virtual/range {v1 .. v8}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IIZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->f()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_1
    :goto_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "external_game_id"

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "muted"

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcuo;->A:[Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v3

    invoke-virtual {v3, v2}, Lbgt;->a(Ljava/util/HashMap;)Lbgt;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    return-object v1

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v4

    move v3, v12

    :goto_2
    if-ge v3, v4, :cond_6

    const-string v1, "external_game_id"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v5

    invoke-virtual {v2, v1, v3, v5}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcuo;->I:Lcvy;

    const-string v5, "hidden"

    invoke-virtual {v4, v5, v14, v15}, Lcvy;->b(Ljava/lang/Object;J)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    move v12, v3

    move v2, v13

    move v11, v1

    move-object v1, v4

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v2, v10

    :goto_4
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_4
    throw v1

    :cond_5
    move v12, v3

    move v2, v13

    move v11, v1

    move-object v1, v4

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    goto :goto_4

    :cond_6
    move v1, v11

    goto :goto_3

    :cond_7
    move v1, v9

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    iget-object v1, p0, Lcuo;->i:Lcve;

    invoke-virtual {v1}, Lcve;->d()V

    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcvz;->a(Ljava/lang/Object;)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v3

    if-eqz p7, :cond_0

    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcvz;->c(Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v2, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, Lcvz;->a(Ljava/lang/Object;JIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcuo;->K:Lcvz;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    const/4 v11, 0x0

    if-nez p6, :cond_2

    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcvz;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcvz;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v11

    :cond_3
    :try_start_0
    iget-object v1, p0, Lcuo;->F:Ldmo;

    const-string v6, "android:7"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v12, p4

    invoke-static/range {v5 .. v12}, Ldmo;->a(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v5, v1, Ldmo;->a:Lbmi;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-class v10, Ldmm;

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v10}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldmm;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ldmm;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v1}, Ldmm;->b()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcuo;->K:Lcvz;

    move-object v5, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move-object/from16 v11, p4

    move-wide v12, v3

    invoke-direct/range {v5 .. v13}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;Lcwf;Ljava/lang/String;J)V

    iget-object v1, p0, Lcuo;->K:Lcvz;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GameAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcvz;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcuo;->K:Lcvz;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcvz;->d(Ljava/lang/Object;)V

    :cond_4
    iget-object v1, p0, Lcuo;->K:Lcvz;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    iget-object v1, p0, Lcuo;->k:Lcve;

    invoke-virtual {v1}, Lcve;->d()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "custom_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcuo;->I:Lcvy;

    const/4 v13, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move/from16 v6, p6

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p9

    move/from16 v10, p5

    move/from16 v11, p7

    move/from16 v12, p8

    invoke-direct/range {v1 .. v13}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvy;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZLcuq;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    return-object v1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLjava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    const/4 v13, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    if-eqz p4, :cond_6

    :try_start_0
    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "ANDROID"

    invoke-static/range {p3 .. p3}, Lcum;->a(Ljava/lang/String;)Ldmj;

    move-result-object v8

    iget-object v1, p0, Lcuo;->F:Ldmo;

    const-string v3, "android:7"

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v1 .. v8}, Ldmo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldmj;)Ldmk;

    move-result-object v1

    invoke-virtual {v1}, Ldmk;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    new-instance v1, Lsp;

    new-instance v2, Lrz;

    const/16 v3, 0x194

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lrz;-><init>(I[BLjava/util/Map;Z)V

    invoke-direct {v1, v2}, Lsp;-><init>(Lrz;)V

    throw v1
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    move-object v4, v11

    :goto_0
    const-string v5, "GameAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unable to retrieve 1P application "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from network"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "GameAgent"

    invoke-static {v1, v5}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/4 v5, 0x3

    const/16 v6, 0x194

    invoke-static {v1, v6}, Lbng;->a(Lsp;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "GameAgent"

    const-string v2, "Game ID (%s) was not found on server"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmr;

    invoke-virtual {v1}, Ldmr;->getGamesData()Ldmf;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    :try_start_2
    invoke-virtual {v1}, Ldmr;->getMarketData()Ldno;
    :try_end_2
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    :try_start_3
    invoke-virtual {v1}, Ldmr;->b()Ljava/lang/Long;
    :try_end_3
    .catch Lsp; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v2

    :try_start_4
    invoke-virtual {v1}, Ldmr;->c()Ljava/lang/Integer;
    :try_end_4
    .catch Lsp; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v7

    move-object v6, v2

    move-object v5, v3

    move v11, v13

    :goto_2
    if-nez v4, :cond_3

    if-eqz v5, :cond_4

    :cond_3
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v10}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZLjava/util/ArrayList;)J

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "GameAgent"

    invoke-static {v1, v10, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_4
    if-nez p5, :cond_8

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_a

    const-string v1, "GameAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not associated with package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Check the application ID in your manifest."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_1

    :cond_5
    move-object v7, v12

    move-object v6, v2

    move v11, v5

    move-object v5, v3

    goto :goto_2

    :cond_6
    :try_start_5
    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ANDROID"

    iget-object v3, p0, Lcuo;->E:Ldmn;

    move-object/from16 v0, p3

    invoke-static {v0, v1, v2}, Ldmn;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v3, Ldmn;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldmf;

    move-object/from16 v2, p2

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldmf;
    :try_end_5
    .catch Lsp; {:try_start_5 .. :try_end_5} :catch_1

    move-object v7, v12

    move-object v6, v9

    move-object v5, v10

    move-object v4, v1

    move v11, v13

    goto :goto_2

    :catch_1
    move-exception v1

    const-string v2, "GameAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to retrieve application "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from network"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GameAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_7
    const/4 v1, 0x3

    move-object v7, v12

    move-object v6, v9

    move-object v5, v10

    move-object v4, v11

    move v11, v1

    goto/16 :goto_2

    :cond_8
    invoke-static/range {p2 .. p3}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "package_name=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p5, v3, v4

    invoke-static {p1, v1, v2, v3}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_a
    invoke-static/range {p2 .. p3}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1, v11}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_1

    :catch_2
    move-exception v1

    move-object v2, v9

    move-object v3, v10

    goto/16 :goto_0

    :catch_3
    move-exception v1

    move-object v2, v9

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0
.end method

.method public final a(I)Lcve;
    .locals 1

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcuo;->k:Lcve;

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcuo;->a:Lcve;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcuo;->b:Lcve;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcuo;->c:Lcve;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcuo;->d:Lcve;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcuo;->e:Lcve;

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcuo;->f:Lcve;

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcuo;->g:Lcve;

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcuo;->h:Lcve;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 20

    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static/range {p2 .. p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcuw;->a:[Ljava/lang/String;

    const-string v7, "metadata_sync_requested=1"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_0
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    move-object v4, v15

    :goto_1
    return-object v4

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v10}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/ArrayList;)Lcup;

    move-result-object v18

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, v18

    iget-object v6, v0, Lcup;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v16, v5

    :goto_2
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v18

    iget-object v5, v0, Lcup;->a:Ljava/util/ArrayList;

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldmr;

    invoke-virtual {v5}, Ldmr;->getGamesData()Ldmf;

    move-result-object v8

    invoke-virtual {v5}, Ldmr;->getMarketData()Ldno;

    move-result-object v9

    invoke-virtual {v5}, Ldmr;->b()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5}, Ldmr;->c()Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v5 .. v14}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZLjava/util/ArrayList;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    if-nez v8, :cond_3

    const/4 v5, 0x0

    move-object v6, v5

    :goto_3
    if-eqz v6, :cond_2

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    new-instance v8, Lcur;

    invoke-direct {v8, v6, v5, v7}, Lcur;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v5, v16, 0x1

    move/from16 v16, v5

    goto :goto_2

    :cond_3
    invoke-virtual {v8}, Ldmf;->b()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iget-object v6, v0, Lcup;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    :goto_4
    if-ge v6, v7, :cond_5

    move-object/from16 v0, v18

    iget-object v5, v0, Lcup;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldnf;

    invoke-virtual {v5}, Ldnf;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    :cond_5
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_6

    const-string v5, "GameAgent"

    invoke-static {v4, v14, v5}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_6
    move-object v4, v15

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    move v0, v2

    :goto_0
    iget-object v1, p0, Lcuo;->l:[Lcve;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcuo;->l:[Lcve;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcve;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p1, p2, p3}, Lcuo;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v2, v3

    :cond_1
    :goto_1
    return v2

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v0, 0x80

    :try_start_0
    invoke-virtual {v1, p3, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    const-string v5, "com.google.android.gms.games.APP_ID"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    if-eqz p4, :cond_1

    const-string v0, "GameAgent"

    const-string v1, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of your manifest"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "GameAgent"

    const-string v3, "Caller attempted to insert game data for non-existent package."

    invoke-static {v1, v3, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    const-string v5, "com.google.android.gms.games.APP_ID"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v0, "GameAgent"

    const-string v1, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of your manifest"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    if-nez v0, :cond_6

    const-string v0, "GameAgent"

    const-string v1, "Your application doesn\'t have a name associated to it."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    if-nez v1, :cond_7

    const-string v0, "GameAgent"

    const-string v1, "Your application doesn\'t have an icon associated to it."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    invoke-static {p1, p2, v6}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v7, v4, v8

    if-nez v7, :cond_8

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    invoke-virtual {v1, v5, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "external_game_id"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "display_name"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "primary_category"

    const-string v7, "unknown"

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "icon_image_bytes"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v1, "metadata_version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "metadata_sync_requested"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "play_enabled_game"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "availability"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    :cond_8
    cmp-long v1, v4, v8

    if-nez v1, :cond_9

    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed to insert game record for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "instance_game_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "instance_display_name"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "package_name"

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "platform_type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    invoke-virtual {p0, p1, p2, p3, v3}, Lcuo;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V

    invoke-static {p2}, Ldrn;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    move v2, v3

    goto/16 :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    iget-object v0, p0, Lcuo;->d:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    invoke-virtual {p3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IIZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 18

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static/range {p2 .. p2}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcuw;->a:[Ljava/lang/String;

    const-string v7, "metadata_version<0"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_0
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v10}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/ArrayList;)Lcup;

    move-result-object v16

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, v16

    iget-object v6, v0, Lcup;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v17

    move v15, v5

    :goto_2
    move/from16 v0, v17

    if-ge v15, v0, :cond_3

    move-object/from16 v0, v16

    iget-object v5, v0, Lcup;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldmr;

    invoke-virtual {v5}, Ldmr;->getGamesData()Ldmf;

    move-result-object v8

    invoke-virtual {v5}, Ldmr;->getMarketData()Ldno;

    move-result-object v9

    invoke-virtual {v5}, Ldmr;->b()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5}, Ldmr;->c()Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v5 .. v14}, Lcuo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldmf;Ldno;Ljava/lang/Long;Ljava/lang/Integer;ZZLjava/util/ArrayList;)J

    add-int/lit8 v5, v15, 0x1

    move v15, v5

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    move-object/from16 v0, v16

    iget-object v6, v0, Lcup;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    :goto_3
    if-ge v6, v7, :cond_4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcup;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldnf;

    invoke-virtual {v5}, Ldnf;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_3

    :cond_4
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    const-string v5, "GameAgent"

    invoke-static {v4, v14, v5}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto/16 :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 14

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcuo;->l:[Lcve;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcuo;->l:[Lcve;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcve;->d()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcuo;->I:Lcvy;

    iget-object v1, v1, Lcwf;->a:Ldl;

    invoke-virtual {v1}, Ldl;->b()V

    invoke-static/range {p1 .. p3}, Lcuo;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_2

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v7, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcut;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    move-wide v4, v7

    :goto_2
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v6, 0x1

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Ldiz;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v8, "installed"

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v6, v8, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const-wide/16 v12, 0x0

    cmp-long v6, v4, v12

    if-gez v6, :cond_9

    const/4 v6, 0x2

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-lez v6, :cond_5

    const/4 v6, 0x1

    :goto_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    move/from16 v6, p4

    :cond_4
    if-eqz v6, :cond_9

    :goto_4
    move-wide v4, v2

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    :cond_6
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-gez v2, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    :cond_7
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-lez v2, :cond_8

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lbiq;->a(Z)V

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "target_instance"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, "GameAgent"

    invoke-static {v1, v10, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    :cond_9
    move-wide v2, v4

    goto :goto_4
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcuo;->h:Lcve;

    invoke-virtual {v2}, Lcve;->d()V

    :try_start_0
    invoke-static {p3}, Lcum;->a(Ljava/lang/String;)Ldmj;

    move-result-object v2

    if-eqz p4, :cond_0

    iget-object v3, p0, Lcuo;->F:Ldmo;

    const-string v4, "players/me/applications/mute"

    iget-object v3, v3, Ldmo;->a:Lbmi;

    const/4 v5, 0x1

    invoke-virtual {v3, p2, v5, v4, v2}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "muted"

    if-eqz p4, :cond_2

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2, p3}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v1, p0, Lcuo;->I:Lcvy;

    const-string v2, "hidden"

    invoke-virtual {v1, v2}, Lcvy;->c(Ljava/lang/Object;)V

    :goto_2
    return v0

    :cond_0
    iget-object v3, p0, Lcuo;->F:Ldmo;

    const-string v4, "players/me/applications/unmute"

    iget-object v3, v3, Ldmo;->a:Lbmi;

    const/4 v5, 0x1

    invoke-virtual {v3, p2, v5, v4, v2}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "GameAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/4 v0, 0x6

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x3

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcuo;->j:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object p3

    :goto_1
    invoke-static {p1}, Lcum;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    :try_start_1
    iget-object v2, p0, Lcuo;->H:Lbmi;

    const-string v3, "/SuggRequest?json=1&query=%1$s&hl=%2$s&gl=%3$s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v3, Ldqs;

    invoke-virtual {v2, p2, v0, v3}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldqs;
    :try_end_1
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v0}, Ldqs;->b()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lbhk;

    sget-object v0, Ldja;->a:[Ljava/lang/String;

    const-string v4, "suggestion"

    invoke-direct {v3, v0, v4, v8, v8}, Lbhk;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_1

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "suggestion"

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqt;

    invoke-virtual {v0}, Ldqt;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lbhk;->a(Landroid/content/ContentValues;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lbhk;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method
