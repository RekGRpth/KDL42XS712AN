.class public abstract Lclt;
.super Lclu;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method protected constructor <init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p5}, Lclu;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    const-string v0, "Entryspec must not be null"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-void
.end method

.method protected constructor <init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lclu;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "entrySqlId"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;)Lcfp;
    .locals 2

    invoke-super {p0, p1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0, v1}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcfz;Lbsp;)Lcml;
    .locals 2

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, p2, v0}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcmd;

    iget-object v1, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {v0, v1}, Lcmd;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, v0, p2}, Lclt;->a(Lcfz;Lcfp;Lbsp;)Lcml;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lcfz;Lcfp;Lbsp;)Lcml;
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcoy;)V
    .locals 3

    invoke-virtual {p2}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lclt;->d(Lcfz;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p2}, Lcoy;->f()Lcfz;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lclt;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V

    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
.end method

.method protected final a(Lcoy;)V
    .locals 3

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lclt;->d(Lcfz;)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcmd;

    invoke-direct {v0, v1}, Lcmd;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcfp;->p()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcme;

    invoke-direct {v0, v1}, Lcme;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    throw v0

    :cond_1
    return-void
.end method

.method protected final a(Lclu;)Z
    .locals 2

    invoke-super {p0, p1}, Lclu;->a(Lclu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1}, Lclu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcfz;)Lbsp;
    .locals 1

    invoke-super {p0, p1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclu;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "entrySqlId"

    iget-object v2, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->hashCode()I

    move-result v0

    return v0
.end method
