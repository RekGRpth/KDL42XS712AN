.class final Lcvl;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final j:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final k:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final l:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final m:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final n:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final o:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final p:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final q:Ljava/util/ArrayList;

.field private static final r:Ldhu;


# instance fields
.field final a:Lcve;

.field final b:Lcve;

.field final c:Lcve;

.field final d:Lcve;

.field final e:Lcve;

.field final f:Lcve;

.field final g:Lcve;

.field final h:[Lcve;

.field private final s:Ldoj;

.field private final t:Ldok;

.field private final u:Lgee;

.field private final v:Lgex;

.field private final w:Lbmr;

.field private final x:Lcwb;

.field private final y:Lcwc;

.field private final z:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->i:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->j:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->k:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->l:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->m:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->n:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->o:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvl;->p:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcvl;->q:Ljava/util/ArrayList;

    const-string v1, "person"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ldhu;->a()Ldhv;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_icon_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_hi_res_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_hi_res_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "is_in_circles"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "last_updated"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "most_recent_external_game_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "most_recent_activity_timestamp"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    invoke-virtual {v0}, Ldhv;->a()Ldhu;

    move-result-object v0

    sput-object v0, Lcvl;->r:Ldhu;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;Lbmi;Lbmi;Lbmi;)V
    .locals 3

    const-string v0, "PlayerAgent"

    sget-object v1, Lcvl;->i:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Lbmr;

    invoke-direct {v0}, Lbmr;-><init>()V

    iput-object v0, p0, Lcvl;->w:Lbmr;

    new-instance v0, Ldoj;

    invoke-direct {v0, p2}, Ldoj;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvl;->s:Ldoj;

    new-instance v0, Ldok;

    invoke-direct {v0, p3}, Ldok;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvl;->t:Ldok;

    new-instance v0, Lgee;

    invoke-direct {v0, p4}, Lgee;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvl;->u:Lgee;

    new-instance v0, Lgex;

    invoke-direct {v0, p5}, Lgex;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvl;->v:Lgex;

    new-instance v0, Lcwb;

    sget-object v1, Lcvl;->r:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcwb;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcvl;->x:Lcwb;

    new-instance v0, Lcwc;

    sget-object v1, Lcvl;->r:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcwc;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcvl;->y:Lcwc;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcvl;->z:Ljava/util/HashMap;

    new-instance v0, Lcve;

    const-string v1, "RecentPlayersInCircles"

    sget-object v2, Lcvl;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->a:Lcve;

    new-instance v0, Lcve;

    const-string v1, "RecentlyPlayedWith"

    sget-object v2, Lcvl;->k:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->b:Lcve;

    new-instance v0, Lcve;

    const-string v1, "CircledPlayers"

    sget-object v2, Lcvl;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->c:Lcve;

    new-instance v0, Lcve;

    const-string v1, "VisiblePlayers"

    sget-object v2, Lcvl;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->d:Lcve;

    new-instance v0, Lcve;

    const-string v1, "SuggestedPlayers"

    sget-object v2, Lcvl;->n:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->e:Lcve;

    new-instance v0, Lcve;

    const-string v1, "ConnectedPlayers"

    sget-object v2, Lcvl;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->f:Lcve;

    new-instance v0, Lcve;

    const-string v1, "SearchPlayers"

    sget-object v2, Lcvl;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    iput-object v0, p0, Lcvl;->g:Lcve;

    const/4 v0, 0x7

    new-array v0, v0, [Lcve;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcvl;->a:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcvl;->b:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcvl;->c:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcvl;->d:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcvl;->e:Lcve;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcvl;->f:Lcve;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcvl;->h:[Lcve;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvn;Ljava/lang/String;JJ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    iget-object v0, p3, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p3, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v7, "profile_icon_image_url"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "profile_hi_res_image_url"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v2}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v6, v0, v2}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v2, v1}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v3, :cond_3

    iget-object v0, p3, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v7, "profile_icon_image_url"

    const-string v8, "profile_icon_image_uri"

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v0, v7, v8, v6, v1}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v7, "profile_hi_res_image_url"

    const-string v8, "profile_hi_res_image_uri"

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v0, v7, v8, v6, v1}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcvl;->x:Lcwb;

    iget-object v2, p3, Lcvn;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p3, Lcvn;->b:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v1, p4

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    invoke-virtual/range {v0 .. v10}, Lcwb;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V

    iget-object v0, p0, Lcvl;->x:Lcwb;

    const-string v1, "total_count"

    iget v2, p3, Lcvn;->c:I

    invoke-virtual {v0, p4, v1, v2}, Lcwb;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    iget-object v0, p0, Lcvl;->x:Lcwb;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, p4, v1, v2}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 18

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static/range {p1 .. p2}, Lfey;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    move-object/from16 v16, v2

    :goto_0
    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-gtz v2, :cond_2

    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v2, v3, v6}, Lfey;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZI)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    move-object/from16 v16, v2

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v10, Lfxv;

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Lfxv;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v10}, Lfxv;->a()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcwb;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcvl;->x:Lcwb;

    const/4 v7, 0x0

    move-object/from16 v3, p4

    invoke-virtual/range {v2 .. v7}, Lcwb;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v7, 0x0

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    invoke-static/range {p2 .. p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v13

    const/4 v2, 0x0

    move v8, v2

    :goto_2
    if-ge v8, v6, :cond_4

    invoke-virtual {v10, v8}, Lfxv;->b(I)Lfxl;

    move-result-object v2

    invoke-interface {v2}, Lfxl;->p()Lfxq;

    move-result-object v2

    invoke-interface {v2}, Lfxq;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v3, Lbki;

    invoke-direct {v3, v2}, Lbki;-><init>(Ljava/lang/String;)V

    const v7, 0x7f0d0084    # com.google.android.gms.R.dimen.games_image_download_size_player_icon

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v7}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v3

    invoke-virtual {v3}, Lbkh;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Lbki;

    invoke-direct {v7, v2}, Lbki;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0d0085    # com.google.android.gms.R.dimen.games_image_download_size_player_hi_res

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v2}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v2

    invoke-virtual {v2}, Lbkh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v13, v3, v9}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v13, v2, v9}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v7, v3

    move-object v3, v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    move-object v3, v7

    goto :goto_3

    :cond_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v6, :cond_6

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v6, :cond_7

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lbiq;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v8, "PlayerAgent"

    invoke-static {v2, v9, v8}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v8, v2

    :goto_6
    if-ge v8, v6, :cond_a

    invoke-virtual {v10, v8}, Lfxv;->b(I)Lfxl;

    move-result-object v2

    invoke-interface {v2}, Lfxl;->m()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    const-string v17, "external_player_id"

    invoke-interface {v2}, Lfxl;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "profile_name"

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_updated"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "is_in_circles"

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v14, "profile_icon_image_uri"

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v15, v14, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "profile_icon_image_url"

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "profile_hi_res_image_uri"

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_9

    const/4 v2, 0x0

    :goto_8
    invoke-virtual {v15, v14, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "profile_hi_res_image_url"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    :cond_8
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentProviderResult;

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    :cond_9
    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentProviderResult;

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_8

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcwb;->c(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcvl;->x:Lcwb;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p4

    move-wide v14, v4

    invoke-virtual/range {v7 .. v15}, Lcwb;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcvl;->x:Lcwb;

    const-string v3, "total_count"

    move-object/from16 v0, p4

    invoke-virtual {v2, v0, v3, v6}, Lcwb;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcvl;->x:Lcwb;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v2, v0, v3, v4}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v2
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcwb;->a(Ljava/lang/Object;)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v5

    if-eqz p9, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Lcwb;->c(Ljava/lang/Object;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v4, p4

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v3 .. v8}, Lcwb;->a(Ljava/lang/Object;JIZ)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v4, v5}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    const/4 v13, 0x0

    if-nez p8, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v5, v6}, Lcwb;->a(Ljava/lang/Object;J)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v5, v6}, Lcwb;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v13

    :cond_3
    :try_start_0
    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v14

    const-string v3, "circled"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v3

    invoke-static {v3}, Lbiq;->a(Z)V

    const/16 v16, 0x1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p5

    move/from16 v12, p7

    invoke-direct/range {v7 .. v16}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JI)Lcvn;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    move-object v10, v3

    :goto_2
    if-nez v10, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v5, v6}, Lcwb;->a(Ljava/lang/Object;J)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Lcwb;->d(Ljava/lang/Object;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->x:Lcwb;

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v4, v5}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    goto :goto_0

    :cond_5
    :try_start_1
    const-string v3, "suggested"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v3

    invoke-static {v3}, Lbiq;->a(Z)V

    const/16 v16, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p5

    move/from16 v12, p7

    invoke-direct/range {v7 .. v16}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JI)Lcvn;

    move-result-object v3

    goto :goto_1

    :cond_6
    const-string v3, "visible:"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, Lbiq;->a(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcvl;->u:Lgee;

    move-object/from16 v16, v0

    sget-object v20, Lcvl;->q:Ljava/util/ArrayList;

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v19, p5

    move-object/from16 v23, v13

    invoke-virtual/range {v16 .. v23}, Lgee;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v3, v14, v15, v4}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcvn;

    move-result-object v3

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    :cond_8
    const-string v3, "players1p:"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v3

    invoke-static {v3}, Lbiq;->a(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->t:Ldok;

    const/4 v4, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v1, v4, v13}, Ldok;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v7, v3, Ldok;->a:Lbmi;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const-class v12, Ldof;

    move-object/from16 v8, p2

    invoke-virtual/range {v7 .. v12}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v3

    check-cast v3, Ldof;

    invoke-virtual {v3}, Ldof;->getItems()Ljava/util/ArrayList;

    move-result-object v10

    if-nez v10, :cond_9

    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v17

    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    move v9, v4

    :goto_4
    move/from16 v0, v17

    if-ge v9, v0, :cond_f

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldmv;

    invoke-virtual {v4}, Ldmv;->getDisplayPlayer()Ldnz;

    move-result-object v7

    iget-object v0, v7, Lbni;->a:Landroid/content/ContentValues;

    move-object/from16 v16, v0

    invoke-virtual {v4}, Ldmv;->getLastPlayed()Ldmu;

    move-result-object v7

    invoke-virtual {v4}, Ldmv;->getLastPlayedWith()Ldmu;

    move-result-object v8

    if-nez v7, :cond_a

    const/4 v4, 0x0

    :goto_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_b

    invoke-virtual {v7}, Ldmu;->b()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v11, "most_recent_external_game_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v11, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    if-nez v8, :cond_d

    const-wide/16 v12, -0x1

    :goto_7
    move-object/from16 v11, p1

    invoke-static/range {v11 .. v16}, Lcvl;->a(Landroid/content/Context;JJLandroid/content/ContentValues;)V

    if-nez v7, :cond_e

    const-wide/16 v7, -0x1

    :goto_8
    const-string v4, "most_recent_activity_timestamp"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto :goto_4

    :cond_a
    invoke-virtual {v7}, Ldmu;->c()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_b
    const-string v4, "most_recent_external_game_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v4, "most_recent_game_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6

    :catch_0
    move-exception v3

    invoke-static {}, Ldac;->a()Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "PlayerAgent"

    invoke-static {v3, v4}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_c
    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_d
    :try_start_2
    invoke-virtual {v8}, Ldmu;->d()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    goto :goto_7

    :cond_e
    invoke-virtual {v7}, Ldmu;->d()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    goto :goto_8

    :cond_f
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v9

    const/4 v4, 0x0

    move v8, v4

    :goto_9
    move/from16 v0, v17

    if-ge v8, v0, :cond_11

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    const-string v7, "most_recent_external_game_id"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    move-object/from16 v0, p2

    invoke-static {v0, v11, v12}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v7

    const-string v11, "game_icon_image_uri"

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v11, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_9

    :cond_11
    new-instance v4, Lcvn;

    invoke-virtual {v3}, Ldof;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v3, v7}, Lcvn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v3, v4

    goto/16 :goto_1

    :cond_12
    const-string v3, "players:"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v3

    if-nez v3, :cond_13

    const/4 v3, 0x1

    :goto_a
    invoke-static {v3}, Lbiq;->a(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcvl;->s:Ldoj;

    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v4, v13}, Ldoj;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v7, v3, Ldoj;->a:Lbmi;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const-class v12, Ldog;

    move-object/from16 v8, p2

    invoke-virtual/range {v7 .. v12}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v3

    check-cast v3, Ldog;

    invoke-virtual {v3}, Ldog;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-nez v7, :cond_14

    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_13
    const/4 v3, 0x0

    goto :goto_a

    :cond_14
    move-object/from16 v0, p1

    invoke-static {v0, v7, v14, v15}, Lcvl;->a(Landroid/content/Context;Ljava/util/ArrayList;J)Ljava/util/ArrayList;

    move-result-object v8

    new-instance v4, Lcvn;

    invoke-virtual {v3}, Ldog;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v4, v8, v3, v7}, Lcvn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v3, v4

    goto/16 :goto_1

    :cond_15
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Unrecognized cache key "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_0

    :cond_16
    const-string v3, "playedWith"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    const-wide/32 v14, 0x1d4c0

    :goto_b
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-wide v12, v5

    invoke-direct/range {v7 .. v15}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvn;Ljava/lang/String;JJ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    goto/16 :goto_0

    :cond_17
    const-wide/32 v14, 0x1b7740

    goto :goto_b
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    if-nez p4, :cond_1

    if-eqz p3, :cond_1

    invoke-static {p2}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "external_player_id = ?"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p3, v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_1
    if-nez p3, :cond_2

    :try_start_0
    const-string v0, "me"

    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcvl;->t:Ldok;

    invoke-static {v0}, Ldok;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Ldok;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldmv;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldmv;

    invoke-virtual {v0}, Ldmv;->getDisplayPlayer()Ldnz;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    iget-object v0, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-static {p1, v0}, Lcum;->b(Landroid/content/Context;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v0, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {p2}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    const-string v3, "profile_icon_image_url"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "profile_hi_res_image_url"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    move-object v0, p3

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcvl;->s:Ldoj;

    invoke-static {v0}, Ldoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Ldoj;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldnz;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldnz;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "PlayerAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to load player "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_4
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IJLjava/lang/String;)Lcvn;
    .locals 10

    const/4 v0, 0x0

    move v9, v0

    move-object/from16 v6, p7

    move-object v2, p3

    move-object v1, p2

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcvl;->v:Lgex;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v5

    const/4 v8, -0x1

    move-object v3, p0

    move-object v4, p1

    move-wide v6, p5

    invoke-direct/range {v3 .. v8}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcvn;

    move-result-object v0

    iget-object v3, v0, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcvn;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    const/4 v3, 0x3

    if-ge v9, v3, :cond_1

    iget-object v6, v0, Lcvn;->b:Ljava/lang/String;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;J)Lcvn;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcvl;->v:Lgex;

    invoke-virtual {v1, p2, p3}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "external_player_id"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "profile_name"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "last_updated"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "is_in_circles"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lgks;

    move-result-object v1

    invoke-interface {v1}, Lgks;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lbki;

    invoke-direct {v3, v1}, Lbki;-><init>(Ljava/lang/String;)V

    const v1, 0x7f0d0084    # com.google.android.gms.R.dimen.games_image_download_size_player_icon

    invoke-virtual {v3, p1, v1}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v1

    invoke-virtual {v1}, Lbkh;->a()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v1, v0

    :goto_1
    const-string v4, "profile_icon_image_url"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "profile_hi_res_image_url"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcvn;

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v3, v2, v4}, Lcvn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v1, Lbki;

    invoke-direct {v1, v3}, Lbki;-><init>(Ljava/lang/String;)V

    const v4, 0x7f0d0085    # com.google.android.gms.R.dimen.games_image_download_size_player_hi_res

    invoke-virtual {v1, p1, v4}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v1

    invoke-virtual {v1}, Lbkh;->a()Ljava/lang/String;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    :catch_0
    move-exception v1

    const/16 v2, 0x194

    invoke-static {v1, v2}, Lbng;->a(Lsp;I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PlayerAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JI)Lcvn;
    .locals 8

    iget-object v0, p0, Lcvl;->v:Lgex;

    sget-object v4, Lcvl;->q:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p7

    move/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcvn;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcvn;
    .locals 7

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxl;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "external_player_id"

    invoke-interface {v0}, Lfxl;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "profile_name"

    invoke-interface {v0}, Lfxl;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "last_updated"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "is_in_circles"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0}, Lfxl;->p()Lfxq;

    move-result-object v0

    invoke-interface {v0}, Lfxq;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lbki;

    invoke-direct {v5, v0}, Lbki;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0d0084    # com.google.android.gms.R.dimen.games_image_download_size_player_icon

    invoke-virtual {v5, p0, v0}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    move-object v0, v1

    :goto_2
    const-string v6, "profile_icon_image_url"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "profile_hi_res_image_url"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v0, Lbki;

    invoke-direct {v0, v5}, Lbki;-><init>(Ljava/lang/String;)V

    const v6, 0x7f0d0085    # com.google.android.gms.R.dimen.games_image_download_size_player_hi_res

    invoke-virtual {v0, p0, v6}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    new-instance v1, Lcvn;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->j()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lcvn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcvn;
    .locals 9

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-nez p2, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    invoke-interface {v0}, Lgkp;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcvl;->q:Ljava/util/ArrayList;

    invoke-interface {v0}, Lgkp;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {v0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v6

    const-string v1, "external_player_id"

    invoke-virtual {v5, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "profile_name"

    invoke-interface {v0}, Lgkp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "last_updated"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "is_in_circles"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0}, Lgkp;->h()Lgks;

    move-result-object v1

    invoke-interface {v1}, Lgks;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v7, Lbki;

    invoke-direct {v7, v1}, Lbki;-><init>(Ljava/lang/String;)V

    const v1, 0x7f0d0084    # com.google.android.gms.R.dimen.games_image_download_size_player_icon

    invoke-virtual {v7, p1, v1}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v1

    invoke-virtual {v1}, Lbkh;->a()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    move-object v1, v2

    :goto_2
    const-string v8, "profile_icon_image_url"

    invoke-virtual {v5, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "profile_hi_res_image_url"

    invoke-virtual {v5, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lgkp;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcvl;->z:Ljava/util/HashMap;

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcvl;->z:Ljava/util/HashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_3
    new-instance v1, Lbki;

    invoke-direct {v1, v7}, Lbki;-><init>(Ljava/lang/String;)V

    const v8, 0x7f0d0085    # com.google.android.gms.R.dimen.games_image_download_size_player_hi_res

    invoke-virtual {v1, p1, v8}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v1

    invoke-virtual {v1}, Lbkh;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    new-instance v2, Lcvn;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v2, v3, v0, v1}, Lcvn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;J)Ljava/util/ArrayList;
    .locals 9

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnz;

    iget-object v5, v0, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v0}, Ldnz;->getLastPlayedWith()Ldny;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, -0x1

    :goto_1
    move-object v0, p0

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcvl;->a(Landroid/content/Context;JJLandroid/content/ContentValues;)V

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ldny;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_1

    :cond_1
    return-object v8
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 9

    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-ltz v5, :cond_0

    invoke-static {p1, v0}, Ldjb;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "game_icon_image_url"

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v3, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    return-object v2
.end method

.method private static a(Landroid/content/Context;JJLandroid/content/ContentValues;)V
    .locals 2

    const-string v0, "profile_icon_image_url"

    invoke-virtual {p5, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lbki;

    invoke-direct {v1, v0}, Lbki;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0d0085    # com.google.android.gms.R.dimen.games_image_download_size_player_hi_res

    invoke-virtual {v1, p0, v0}, Lbki;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v0

    invoke-virtual {v0}, Lbkh;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "profile_hi_res_image_url"

    invoke-virtual {p5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v0, "played_with_timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "last_updated"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void

    :cond_0
    const-string v0, "profile_hi_res_image_url"

    invoke-virtual {p5, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 7

    invoke-static {p0, p1}, Lcvl;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0x32

    if-lt v5, v6, :cond_0

    invoke-static {p0, p1, v3, v1, v2}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_0
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-static {p0, p1, v3, v1, v2}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v2, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    sget-object v0, Ldei;->b:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcum;->d(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_3
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 6

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lfey;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {p1, v0}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "is_in_circles"

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 6

    const/4 v5, 0x1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcvm;->a:[Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v0, 0x0

    if-ne v4, v5, :cond_1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    if-nez v4, :cond_0

    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    iget-object v0, p0, Lcvl;->e:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    iget-object v0, p0, Lcvl;->v:Lgex;

    iget-object v1, p0, Lcvl;->w:Lbmr;

    iget-object v2, p0, Lcvl;->w:Lbmr;

    const/4 v3, 0x0

    invoke-static {v3, p3}, Lgex;->a(Lgfc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgex;->a:Lbmi;

    invoke-virtual {v0, p1, v3, v1, v2}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lsk;Lsj;)V

    iget-object v0, p0, Lcvl;->z:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v0, p0, Lcvl;->x:Lcwb;

    invoke-virtual {v0, p2}, Lcwb;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcvl;->x:Lcwb;

    const-string v5, "suggested"

    const-string v6, "external_player_id"

    iget-object v0, v3, Lcwf;->a:Ldl;

    invoke-virtual {v0, v5}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcwg;->a:Lbhk;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcwg;->a:Lbhk;

    iget-object v7, v1, Lbhk;->c:Lbgt;

    iget-object v1, v7, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_1

    iget-object v1, v7, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v7, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    :cond_1
    iget-boolean v1, v3, Lcwf;->b:Z

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lcwf;->a(Ljava/lang/String;Lcwg;)V

    :cond_2
    if-eqz v4, :cond_3

    iget-object v0, p0, Lcvl;->v:Lgex;

    const/4 v1, 0x2

    invoke-static {v1}, Ldee;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcvl;->w:Lbmr;

    iget-object v6, p0, Lcvl;->w:Lbmr;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_3
    const-string v0, "PlayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to record rejection action for player; could not find suggestion ID for player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 7

    iget-object v0, p0, Lcvl;->e:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    iget-object v0, p0, Lcvl;->z:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {p4}, Ldee;->a(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcvl;->v:Lgex;

    iget-object v5, p0, Lcvl;->w:Lbmr;

    iget-object v6, p0, Lcvl;->w:Lbmr;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "PlayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to record action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for player; could not find suggestion ID for player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ldmp;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ldmp;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v2, Ldmq;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ldmq;-><init>(Ljava/lang/Boolean;Ljava/util/ArrayList;)V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcvl;->t:Ldok;

    const-string v3, "players/me/contactsettings"

    iget-object v1, v1, Ldok;->a:Lbmi;

    const/4 v4, 0x2

    invoke-virtual {v1, p1, v4, v3, v2}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/4 v0, 0x6

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    const-string v1, "Calling circled from 3P context!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcvl;->c:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    const-string v0, "circled"

    invoke-direct {p0, p1, p2, p3, v0}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v4, "circled"

    const-string v5, "circled"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    iget-object v1, p0, Lcvl;->f:Lcve;

    invoke-virtual {v1}, Lcve;->d()V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connected"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcvl;->x:Lcwb;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcwb;->a(Ljava/lang/Object;)V

    if-eqz p6, :cond_0

    iget-object v1, p0, Lcvl;->x:Lcwb;

    invoke-virtual {v1, v2}, Lcwb;->c(Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcvl;->x:Lcwb;

    const/16 v5, 0x32

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcwb;->a(Ljava/lang/Object;JIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcvl;->x:Lcwb;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    const/4 v13, 0x0

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    packed-switch p5, :pswitch_data_0

    :goto_1
    iget-object v5, p0, Lcvl;->v:Lgex;

    invoke-static/range {p4 .. p4}, Lfmq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "connected"

    const/4 v8, 0x0

    invoke-static {v8, v6, v7, v1}, Lgex;->a(Lgfa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v5, v5, Lgex;->a:Lbmi;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-class v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v10}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    const/4 v10, 0x1

    move-object v5, p0

    move-object v6, p1

    move-wide v8, v3

    invoke-direct/range {v5 .. v10}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcvn;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :goto_2
    if-nez v8, :cond_5

    iget-object v1, p0, Lcvl;->x:Lcwb;

    invoke-virtual {v1, v2, v3, v4}, Lcwb;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcvl;->x:Lcwb;

    invoke-virtual {v1, v2}, Lcwb;->d(Ljava/lang/Object;)V

    :cond_2
    iget-object v1, p0, Lcvl;->x:Lcwb;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    :try_start_1
    const-string v1, "none"

    goto :goto_1

    :pswitch_1
    const-string v1, "nonAdsCommercial"

    goto :goto_1

    :pswitch_2
    const-string v1, "ads"

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcvl;->u:Lgee;

    const-string v7, "me"

    const-string v8, "connected"

    sget-object v9, Lcvl;->q:Ljava/util/ArrayList;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v6, p2

    invoke-virtual/range {v5 .. v12}, Lgee;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    move-result-object v1

    const/4 v5, 0x1

    invoke-static {p1, v1, v3, v4, v5}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcvn;
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-static {}, Ldac;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "PlayerAgent"

    invoke-static {v1, v5}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_4
    move-object v8, v13

    goto :goto_2

    :cond_5
    const-wide/32 v12, 0x1b7740

    move-object v5, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move-object v9, v2

    move-wide v10, v3

    invoke-direct/range {v5 .. v13}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvn;Ljava/lang/String;JJ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling getPlayedWithPlayers from 1P context!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0, p2, p4}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcve;

    move-result-object v0

    invoke-virtual {v0}, Lcve;->d()V

    const-string v0, "playedWith"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "players:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-virtual {p0, p2, p4}, Lcvl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcve;

    move-result-object v0

    invoke-virtual {v0}, Lcve;->d()V

    const-string v0, "playedWith"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "circled"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "players1p:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p5, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, p5

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    invoke-direct {p0, p1, p2, p4, p3}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    const/4 v10, 0x1

    const/4 v5, 0x0

    new-instance v3, Lblt;

    invoke-static {p2}, Ldjo;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    move v0, v5

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    const-string v1, "external_player_id"

    aget-object v2, p3, v0

    const-string v4, "=?"

    iget-object v6, v3, Lblt;->b:Ljava/lang/StringBuilder;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v6, v3, Lblt;->b:Ljava/lang/StringBuilder;

    :goto_1
    iget-object v6, v3, Lblt;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v3, Lblt;->c:[Ljava/lang/String;

    invoke-static {v1, v2}, Lblt;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lblt;->c:[Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v6, v3, Lblt;->b:Ljava/lang/StringBuilder;

    const-string v7, " OR ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lblt;->a:Landroid/net/Uri;

    const-string v1, "external_player_id"

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v2

    move v1, v5

    :goto_2
    array-length v0, p3

    if-ge v1, v0, :cond_6

    aget-object v4, p3, v1

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_5

    :cond_2
    invoke-direct {p0, p1, p2, v4, v10}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_3
    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_6
    iget-object v1, v3, Lblt;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Lblt;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lblt;->c:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    array-length v2, p3

    if-eq v1, v2, :cond_3

    invoke-static {v10}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcvl;->t:Ldok;

    const-string v3, "players/me/contactsettings"

    iget-object v0, v0, Ldok;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldmq;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldmq;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_0
    move-object v0, v7

    goto :goto_0

    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "mobile_notifications_enabled"

    invoke-virtual {v0}, Ldmq;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v0}, Ldmq;->getPerChannelSettings()Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_2

    move v1, v6

    :goto_2
    move v2, v6

    :goto_3
    if-ge v2, v1, :cond_3

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmp;

    invoke-virtual {v0}, Ldmp;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ldec;->a(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0}, Ldmp;->c()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    packed-switch v5, :pswitch_data_0

    const-string v5, "PlayerAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Ignoring unknown channel "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ldmp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_2

    :pswitch_0
    const-string v0, "match_notifications_enabled"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_4

    :pswitch_1
    const-string v0, "request_notifications_enabled"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_4

    :cond_3
    sget-object v0, Lczj;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    move-result-object v0

    invoke-virtual {v0, v6}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcve;
    .locals 3

    const-string v0, "circled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fetching recent players from circles is only valid in a 1P context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcvl;->a:Lcve;

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "playedWith"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcvl;->b:Lcve;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown player collection type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcvl;->h:[Lcve;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcvl;->h:[Lcve;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcve;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcvl;->x:Lcwb;

    iget-object v0, v0, Lcwf;->a:Ldl;

    invoke-virtual {v0}, Ldl;->b()V

    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling visible from 1P context!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcvl;->d:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    invoke-static {p2}, Lcwb;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcwb;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "visible"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 15

    iget-object v1, p0, Lcvl;->g:Lcve;

    invoke-virtual {v1}, Lcve;->d()V

    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcwc;->a(Ljava/lang/Object;)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v3

    if-eqz p7, :cond_0

    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcwc;->c(Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v2, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, Lcwc;->a(Ljava/lang/Object;JIZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcvl;->y:Lcwc;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    const/4 v12, 0x0

    if-nez p6, :cond_2

    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcwc;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcwc;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v12

    :cond_3
    const/4 v1, 0x0

    sget-object v2, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p4

    move-wide v9, v3

    invoke-direct/range {v5 .. v10}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;J)Lcvn;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_b

    move-object v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-wide v10, v3

    invoke-direct/range {v5 .. v12}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IJLjava/lang/String;)Lcvn;

    move-result-object v1

    move-object v14, v1

    :goto_1
    if-nez v14, :cond_6

    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v3, v4}, Lcwc;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcvl;->y:Lcwc;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcwc;->d(Ljava/lang/Object;)V

    :cond_5
    iget-object v1, p0, Lcvl;->y:Lcwc;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :cond_6
    iget-object v1, v14, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static/range {p2 .. p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v9

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_7

    iget-object v1, v14, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    const-string v10, "profile_icon_image_url"

    invoke-virtual {v1, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "profile_hi_res_image_url"

    invoke-virtual {v1, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v10, v5}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v9, v1, v5}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_7
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v6, :cond_8

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v6, :cond_9

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "PlayerAgent"

    invoke-static {v1, v5, v2}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v1, 0x0

    move v5, v1

    :goto_5
    if-ge v5, v6, :cond_a

    iget-object v1, v14, Lcvn;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    const-string v10, "profile_icon_image_url"

    const-string v11, "profile_icon_image_uri"

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-static {v1, v10, v11, v9, v2}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v10, "profile_hi_res_image_url"

    const-string v11, "profile_hi_res_image_uri"

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-static {v1, v10, v11, v9, v2}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    :cond_a
    iget-object v5, p0, Lcvl;->y:Lcwc;

    iget-object v7, v14, Lcvn;->a:Ljava/util/ArrayList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, v14, Lcvn;->b:Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v6, p4

    move-wide v12, v3

    invoke-virtual/range {v5 .. v13}, Lcwc;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    iget-object v1, p0, Lcvl;->y:Lcwc;

    const-string v2, "total_count"

    iget v3, v14, Lcvn;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwc;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    iget-object v1, p0, Lcvl;->y:Lcwc;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_0

    :cond_b
    move-object v14, v1

    goto/16 :goto_1
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    iget-object v0, p0, Lcvl;->e:Lcve;

    invoke-virtual {v0}, Lcve;->d()V

    const-string v4, "suggested"

    const-string v5, "suggestions"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcvl;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
