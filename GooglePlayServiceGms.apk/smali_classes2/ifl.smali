.class public final Lifl;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/telephony/TelephonyManager;IJ)Lhtf;
    .locals 8

    const/4 v1, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v6

    if-eq v6, v5, :cond_1

    if-ne v6, v2, :cond_3

    :cond_1
    move v1, v5

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v3

    move v5, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v7}, Lifl;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Landroid/telephony/CellLocation;IJ)Lhtf;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-eq v6, v3, :cond_4

    const/16 v5, 0x8

    if-eq v6, v5, :cond_4

    const/16 v5, 0x9

    if-eq v6, v5, :cond_4

    const/16 v5, 0xa

    if-eq v6, v5, :cond_4

    const/16 v5, 0xf

    if-ne v6, v5, :cond_5

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    if-eq v6, v1, :cond_6

    const/4 v3, 0x5

    if-eq v6, v3, :cond_6

    const/4 v3, 0x6

    if-eq v6, v3, :cond_6

    const/16 v3, 0xc

    if-eq v6, v3, :cond_6

    const/4 v3, 0x7

    if-ne v6, v3, :cond_7

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    const/16 v2, 0xd

    if-eq v6, v2, :cond_2

    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Landroid/telephony/CellLocation;IJ)Lhtf;
    .locals 36

    const/4 v9, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const v4, 0x7fffffff

    const v3, 0x7fffffff

    const/4 v1, -0x1

    const/4 v12, -0x1

    move-object/from16 v0, p4

    instance-of v2, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_4

    check-cast p4, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v8

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v5

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lifs;->a(Landroid/telephony/gsm/GsmCellLocation;)I

    move-result v9

    const/4 v2, 0x1

    const/4 v10, 0x4

    move/from16 v0, p1

    if-ne v0, v10, :cond_3

    const/high16 v10, 0x10000

    if-lt v5, v10, :cond_2

    const v10, 0xfffffff

    if-gt v5, v10, :cond_2

    const/16 p1, 0x3

    move/from16 v28, v2

    move/from16 v29, v3

    move/from16 v30, v4

    move/from16 v31, v5

    move/from16 v32, v8

    move/from16 v33, v9

    move/from16 v4, p1

    :goto_0
    const/4 v2, 0x2

    if-eq v4, v2, :cond_7

    if-eqz p2, :cond_7

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    const/4 v3, 0x3

    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    move v6, v2

    :cond_0
    :goto_1
    if-eqz p0, :cond_7

    const-string v2, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    const/4 v3, 0x3

    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v12

    :goto_2
    sget-object v23, Lhtf;->a:Ljava/util/Collection;

    if-eqz p3, :cond_9

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    new-instance v34, Ljava/util/HashSet;

    move-object/from16 v0, v34

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v35

    :cond_1
    :goto_3
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/telephony/NeighboringCellInfo;

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getNetworkType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_3

    :pswitch_1
    const/4 v1, 0x1

    if-ne v4, v1, :cond_1

    new-instance v1, Lhtc;

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v5

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v8

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v9

    sget-object v10, Lhtf;->a:Ljava/util/Collection;

    const v13, 0x7fffffff

    const v14, 0x7fffffff

    move-wide/from16 v2, p6

    invoke-direct/range {v1 .. v14}, Lhtc;-><init>(JIIIIIILjava/util/Collection;IIII)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    const/16 p1, 0x1

    :cond_3
    move/from16 v28, v2

    move/from16 v29, v3

    move/from16 v30, v4

    move/from16 v31, v5

    move/from16 v32, v8

    move/from16 v33, v9

    move/from16 v4, p1

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p4

    instance-of v2, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_5

    check-cast p4, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v8

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v5

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v7

    const/4 v6, 0x0

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v4

    invoke-virtual/range {p4 .. p4}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v3

    const/4 v2, 0x0

    const/16 p1, 0x2

    move/from16 v28, v2

    move/from16 v29, v3

    move/from16 v30, v4

    move/from16 v31, v5

    move/from16 v32, v8

    move/from16 v33, v9

    move/from16 v4, p1

    goto/16 :goto_0

    :cond_5
    const/4 v13, 0x0

    :cond_6
    :goto_4
    return-object v13

    :catch_0
    move-exception v2

    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_0

    const-string v3, "RealCellState"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Error parsing MCC/MNC from operator \""

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :catch_1
    move-exception v2

    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_7

    const-string v3, "RealCellState"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Error parsing MCC/MNC from home operator \""

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    move v11, v1

    goto/16 :goto_2

    :pswitch_2
    const/4 v1, 0x3

    if-ne v4, v1, :cond_1

    new-instance v13, Lhtk;

    const/16 v17, -0x1

    const/16 v20, -0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v21

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v22

    sget-object v23, Lhtf;->a:Ljava/util/Collection;

    const v26, 0x7fffffff

    const v27, 0x7fffffff

    move-wide/from16 v14, p6

    move/from16 v16, v4

    move/from16 v18, v6

    move/from16 v19, v7

    move/from16 v24, v11

    move/from16 v25, v12

    invoke-direct/range {v13 .. v27}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_8
    invoke-static/range {v34 .. v34}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v23

    :cond_9
    if-eqz v28, :cond_a

    new-instance v13, Lhtk;

    move-wide/from16 v14, p6

    move/from16 v16, v4

    move/from16 v17, v31

    move/from16 v18, v6

    move/from16 v19, v7

    move/from16 v20, v32

    move/from16 v21, v33

    move/from16 v22, p5

    move/from16 v24, v11

    move/from16 v25, v12

    move/from16 v26, v30

    move/from16 v27, v29

    invoke-direct/range {v13 .. v27}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    :goto_5
    const-string v1, "RealCellState"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_6

    const-string v1, "RealCellState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RealCellState()  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lhtf;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_a
    new-instance v1, Lhtc;

    move-wide/from16 v2, p6

    move/from16 v5, v31

    move/from16 v8, v32

    move/from16 v9, p5

    move-object/from16 v10, v23

    move/from16 v13, v30

    move/from16 v14, v29

    invoke-direct/range {v1 .. v14}, Lhtc;-><init>(JIIIIIILjava/util/Collection;IIII)V

    move-object v13, v1

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
