.class public final Ldjy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    iput-object p1, p0, Ldjy;->a:Landroid/content/Context;

    iput-object p2, p0, Ldjy;->b:Lcom/google/android/gms/common/server/ClientContext;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    iget-object v0, p0, Ldjy;->a:Landroid/content/Context;

    invoke-static {v0}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldjy;->a:Landroid/content/Context;

    iget-object v2, p0, Ldjy;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v0, v2, p1}, Lcun;->g(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Lcun;->a()V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Lcun;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcun;->a()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Ldpi;
    .locals 4

    iget-object v0, p0, Ldjy;->a:Landroid/content/Context;

    invoke-static {v0}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldjy;->a:Landroid/content/Context;

    iget-object v0, p0, Ldjy;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v0, p1, p2}, Lcun;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Ldpi;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Lcun;->a()V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "RealTimeDataManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to report p2p status for room  = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcun;->a()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcun;->a()V

    throw v0
.end method

.method public final a(Ldpi;)Z
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Ldjy;->a:Landroid/content/Context;

    invoke-static {v1}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v2

    :try_start_0
    iget-object v1, p0, Ldjy;->a:Landroid/content/Context;

    iget-object v3, p0, Ldjy;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2, v1, v3, p1}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpi;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "RealTimeDataManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Empty data holder with status "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " returned for status update to room "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ldpi;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V
    :try_end_2
    .catch Lamq; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v2}, Lcun;->a()V

    :goto_0
    return v0

    :cond_0
    :try_start_3
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V
    :try_end_3
    .catch Lamq; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v2}, Lcun;->a()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v1
    :try_end_4
    .catch Lamq; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v1

    invoke-virtual {v2}, Lcun;->a()V

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lcun;->a()V

    throw v0
.end method
