.class public final Lbwo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lbuh;

.field final b:Lbwq;

.field final c:Ljava/util/Map;

.field final d:Lcfz;

.field private final e:Lbst;

.field private final f:Lbsr;

.field private final g:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lbuh;Lbwq;Lbst;Lcfz;Lbsr;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbwo;->c:Ljava/util/Map;

    sget-object v0, Lbqs;->n:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbwo;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuh;

    iput-object v0, p0, Lbwo;->a:Lbuh;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwq;

    iput-object v0, p0, Lbwo;->b:Lbwq;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbst;

    iput-object v0, p0, Lbwo;->e:Lbst;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbwo;->d:Lcfz;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsr;

    iput-object v0, p0, Lbwo;->f:Lbsr;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;Lcfp;Ljava/lang/String;)Lbwm;
    .locals 7

    iget-object v1, p0, Lbwo;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p2}, Lbwo;->a(Lcfp;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FileDownloader"

    const-string v2, "Up-to-date file is already available locally: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbwp;

    invoke-direct {v0, p0, p1, p2, p3}, Lbwp;-><init>(Lbwo;Lbsp;Lcfp;Ljava/lang/String;)V

    iget-object v2, v0, Lbwp;->a:Lbwj;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lbwj;->a(I)V

    invoke-virtual {v0}, Lbwp;->a()Lbwm;

    move-result-object v0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Lbrm;

    const/16 v2, 0xa

    const-string v3, "No content is available for this file."

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p2}, Lcfp;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "Local document not available locally. This should not happen"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lbwo;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwp;

    if-nez v0, :cond_3

    const-string v0, "FileDownloader"

    const-string v2, "Queueing new download for: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lbwp;

    invoke-direct {v0, p0, p1, p2, p3}, Lbwp;-><init>(Lbwo;Lbsp;Lcfp;Ljava/lang/String;)V

    iget-object v2, p0, Lbwo;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lbwo;->g:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    iput-object v2, v0, Lbwp;->b:Ljava/util/concurrent/Future;

    :goto_1
    invoke-virtual {v0}, Lbwp;->a()Lbwm;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    :cond_3
    const-string v2, "FileDownloader"

    const-string v3, "Joining existing download task for: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method final a(Lbwp;Lorg/apache/http/HttpResponse;Lbsy;)V
    .locals 9

    const/4 v7, 0x0

    :try_start_0
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Response entity is null."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :catch_0
    move-exception v0

    move-object v1, v7

    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    move-object v7, v1

    :goto_1
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0

    :cond_1
    :try_start_2
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_1
    move-exception v0

    goto :goto_1

    :sswitch_0
    iget-object v0, p0, Lbwo;->b:Lbwq;

    invoke-static {p1}, Lbwp;->b(Lbwp;)Lcfp;

    move-result-object v2

    new-instance v3, Lbws;

    iget-object v4, v0, Lbwq;->a:Lbst;

    invoke-virtual {v4}, Lbst;->a()Lbsy;

    move-result-object v4

    invoke-virtual {v2}, Lcfp;->Z()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lbws;-><init>(Lbwq;Lbsy;Ljava/lang/String;)V

    iget-object v0, v0, Lbwq;->b:Ldl;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p3, v3, Lbws;->a:Lbsy;

    const-wide/16 v5, 0x0

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    :goto_2
    new-instance v0, Lbwt;

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p1, Lbwp;->a:Lbwj;

    invoke-direct/range {v0 .. v6}, Lbwt;-><init>(Ljava/io/InputStream;Lbwk;JJ)V
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p3}, Lbsy;->b()Lbsx;

    move-result-object v1

    invoke-virtual {v1}, Lbsx;->a()J

    move-result-wide v2

    cmp-long v2, v5, v2

    if-lez v2, :cond_4

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Range response starts after requested start."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :catch_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :sswitch_1
    if-nez p3, :cond_2

    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server returned partial content but full content was requested."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-string v0, "Content-Range"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Partial response is missing range header."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbtz;->a(Ljava/lang/String;)Lbtz;

    move-result-object v0

    iget-wide v5, v0, Lbtz;->c:J

    iget-wide v3, v0, Lbtz;->d:J
    :try_end_4
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :cond_4
    :goto_3
    :try_start_5
    invoke-virtual {v1}, Lbsx;->a()J

    move-result-wide v2

    cmp-long v2, v5, v2

    if-gez v2, :cond_5

    invoke-virtual {v1}, Lbsx;->a()J

    move-result-wide v2

    sub-long/2addr v2, v5

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    add-long/2addr v5, v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcbr;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    new-instance v1, Lbsw;

    iget-object v2, p0, Lbwo;->d:Lcfz;

    invoke-static {p1}, Lbwp;->a(Lbwp;)Lbsp;

    move-result-object v3

    invoke-static {p1}, Lbwp;->b(Lbwp;)Lcfp;

    move-result-object v4

    invoke-virtual {v4}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-static {p1}, Lbwp;->b(Lbwp;)Lcfp;

    move-result-object v5

    invoke-virtual {v5}, Lcfp;->Z()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lbsw;-><init>(Lcfz;Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Lbsy;->a(Lbsu;)Ljava/lang/Object;

    iget-object v1, p0, Lbwo;->b:Lbwq;

    invoke-static {p1}, Lbwp;->b(Lbwp;)Lcfp;

    move-result-object v2

    iget-object v1, v1, Lbwq;->b:Ldl;

    invoke-virtual {v2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldl;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lbwo;->f:Lbsr;

    invoke-virtual {v1}, Lbsr;->a()V
    :try_end_5
    .catch Ljava/text/ParseException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_2
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xce -> :sswitch_1
    .end sparse-switch
.end method

.method final a(Lcfp;)Z
    .locals 2

    invoke-virtual {p1}, Lcfp;->Z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcfp;->aa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwo;->d:Lcfz;

    invoke-virtual {p1}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, Lcfz;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcgs;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcgs;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-interface {v1}, Lcgs;->close()V

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcgs;->close()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcfp;->ab()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbwo;->e:Lbst;

    invoke-virtual {v1, v0}, Lbst;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
