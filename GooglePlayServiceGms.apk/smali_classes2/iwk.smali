.class public final Liwk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liwi;


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private final b:Ljava/util/LinkedList;

.field private c:Liwl;

.field private final d:I

.field private final e:D

.field private final f:I

.field private g:Lixh;

.field private h:Lixh;

.field private i:Lixh;


# direct methods
.method public constructor <init>(Ljava/util/Collection;IDI)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Liwk;->d:I

    iput-wide p3, p0, Liwk;->e:D

    iput p5, p0, Liwk;->f:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Liwk;->b:Ljava/util/LinkedList;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwi;

    iget-object v2, p0, Liwk;->b:Ljava/util/LinkedList;

    new-instance v3, Liwl;

    invoke-direct {v3, v0}, Liwl;-><init>(Liwi;)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iput-object v4, p0, Liwk;->c:Liwl;

    iput-object v4, p0, Liwk;->g:Lixh;

    iput-object v4, p0, Liwk;->h:Lixh;

    return-void
.end method

.method private static a(Liwi;Lixh;Lixh;Lixh;)V
    .locals 7

    const/4 v1, 0x0

    const-wide v2, -0x10000000000001L

    move v0, v1

    :goto_0
    iget v4, p2, Lixh;->c:I

    if-ge v0, v4, :cond_1

    iget-object v4, p2, Lixh;->d:[D

    aget-wide v4, v4, v0

    cmpl-double v6, v4, v2

    if-lez v6, :cond_0

    move-wide v2, v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p2, Lixh;->b:I

    int-to-double v4, v0

    mul-double/2addr v2, v4

    const-wide v4, 0x42d6bcc41e900000L    # 1.0E14

    mul-double/2addr v2, v2

    div-double v2, v4, v2

    invoke-interface {p0}, Liwi;->b()Lixh;

    move-result-object v0

    invoke-virtual {v0}, Lixh;->b()V

    :goto_1
    iget v4, v0, Lixh;->a:I

    if-ge v1, v4, :cond_2

    invoke-virtual {v0, v1, v1, v2, v3}, Lixh;->a(IID)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {p0}, Liwi;->a()Lixh;

    move-result-object v0

    invoke-virtual {v0}, Lixh;->b()V

    invoke-interface {p0, p1, p2, p3}, Liwi;->b(Lixh;Lixh;Lixh;)V

    return-void
.end method

.method private c()V
    .locals 7

    const/4 v3, 0x0

    const/4 v1, -0x1

    iget-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwl;

    iget v5, v0, Liwl;->a:I

    if-le v5, v1, :cond_2

    iget v1, v0, Liwl;->a:I

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1
    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget-object v0, p0, Liwk;->c:Liwl;

    if-ne v2, v0, :cond_1

    iput-object v3, p0, Liwk;->c:Liwl;

    :cond_1
    return-void

    :cond_2
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method private d()Liwl;
    .locals 6

    iget-object v0, p0, Liwk;->c:Liwl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liwk;->c:Liwl;

    iget v0, v0, Liwl;->a:I

    iget v1, p0, Liwk;->f:I

    if-le v0, v1, :cond_2

    :cond_0
    const/4 v2, 0x0

    const v1, 0x7fffffff

    iget-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwl;

    iget v4, v0, Liwl;->a:I

    if-ge v4, v1, :cond_3

    iget v1, v0, Liwl;->a:I

    move v5, v1

    move-object v1, v0

    move v0, v5

    :goto_1
    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    iput-object v2, p0, Liwk;->c:Liwl;

    :cond_2
    iget-object v0, p0, Liwk;->c:Liwl;

    return-object v0

    :cond_3
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Lixh;
    .locals 1

    invoke-direct {p0}, Liwk;->d()Liwl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Liwl;->b:Liwi;

    invoke-interface {v0}, Liwi;->a()Lixh;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lixh;Lixh;)V
    .locals 3

    iget-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwl;

    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    iget-object v2, p0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwl;

    const/4 v1, 0x0

    iput v1, v0, Liwl;->a:I

    iget-object v1, v0, Liwl;->b:Liwi;

    invoke-interface {v1, p1, p2}, Liwi;->a(Lixh;Lixh;)V

    iput-object v0, p0, Liwk;->c:Liwl;

    iget-object v1, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lixh;Lixh;Lixh;)V
    .locals 2

    iget-object v0, p0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwl;

    iget-object v0, v0, Liwl;->b:Liwi;

    invoke-interface {v0, p1, p2, p3}, Liwi;->a(Lixh;Lixh;Lixh;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()Lixh;
    .locals 1

    invoke-direct {p0}, Liwk;->d()Liwl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Liwl;->b:Liwi;

    invoke-interface {v0}, Liwi;->b()Lixh;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lixh;Lixh;Lixh;)V
    .locals 20

    move-object/from16 v0, p1

    iget v3, v0, Lixh;->a:I

    move-object/from16 v0, p2

    iget v4, v0, Lixh;->b:I

    move-object/from16 v0, p0

    iget-object v5, v0, Liwk;->g:Lixh;

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Liwk;->g:Lixh;

    iget v5, v5, Lixh;->a:I

    if-eq v5, v3, :cond_1

    :cond_0
    new-instance v5, Lixh;

    const/4 v6, 0x1

    invoke-direct {v5, v3, v6}, Lixh;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v5, v0, Liwk;->g:Lixh;

    new-instance v5, Lixh;

    invoke-direct {v5, v3, v3}, Lixh;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v5, v0, Liwk;->h:Lixh;

    new-instance v5, Lixh;

    invoke-direct {v5, v3, v4}, Lixh;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v5, v0, Liwk;->i:Lixh;

    :cond_1
    const/4 v10, 0x0

    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liwl;

    iget-object v6, v3, Liwl;->b:Liwi;

    invoke-interface {v6}, Liwi;->a()Lixh;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Liwk;->g:Lixh;

    move-object/from16 v0, p2

    invoke-static {v0, v6, v7}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Liwk;->g:Lixh;

    move-object/from16 v0, p0

    iget-object v7, v0, Liwk;->g:Lixh;

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, Lixj;->f(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Liwk;->g:Lixh;

    const-wide/16 v11, 0x0

    const/4 v6, 0x0

    :goto_1
    iget v14, v7, Lixh;->c:I

    if-ge v6, v14, :cond_2

    iget-object v14, v7, Lixh;->d:[D

    aget-wide v14, v14, v6

    mul-double/2addr v14, v14

    add-double/2addr v11, v14

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    iget v11, v3, Liwl;->a:I

    add-int/lit8 v11, v11, 0x1

    iput v11, v3, Liwl;->a:I

    iget v11, v3, Liwl;->a:I

    move-object/from16 v0, p0

    iget v12, v0, Liwk;->d:I

    if-le v11, v12, :cond_4

    invoke-interface {v13}, Ljava/util/ListIterator;->remove()V

    move-object/from16 v0, p0

    iget-object v6, v0, Liwk;->c:Liwl;

    if-ne v3, v6, :cond_3

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Liwk;->c:Liwl;

    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v6, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    cmpg-double v11, v6, v8

    if-gez v11, :cond_b

    iget-object v4, v3, Liwl;->b:Liwi;

    invoke-interface {v4}, Liwi;->b()Lixh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Liwk;->i:Lixh;

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Liwk;->i:Lixh;

    move-object/from16 v0, p0

    iget-object v5, v0, Liwk;->h:Lixh;

    move-object/from16 v0, p2

    invoke-static {v4, v0, v5}, Lixj;->d(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Liwk;->h:Lixh;

    iget v4, v5, Lixh;->a:I

    iget v8, v5, Lixh;->b:I

    if-eq v4, v8, :cond_5

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Matrix must be square to take the trace but is size %d x %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v5, Lixh;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v5, v5, Lixh;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v7

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_5
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    :goto_2
    iget v10, v5, Lixh;->c:I

    if-ge v4, v10, :cond_6

    iget-object v10, v5, Lixh;->d:[D

    aget-wide v10, v10, v4

    add-double/2addr v8, v10

    iget v10, v5, Lixh;->a:I

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v4, v10

    goto :goto_2

    :cond_6
    move-object/from16 v0, p2

    iget v4, v0, Lixh;->a:I

    int-to-double v4, v4

    div-double v4, v8, v4

    move-wide/from16 v16, v4

    move-wide/from16 v18, v6

    move-wide/from16 v5, v18

    move-object v7, v3

    move-wide/from16 v3, v16

    :goto_3
    move-wide v8, v5

    move-object v10, v7

    move-wide/from16 v16, v3

    move-wide/from16 v4, v16

    goto/16 :goto_0

    :cond_7
    if-eqz v10, :cond_8

    move-object/from16 v0, p0

    iget-wide v6, v0, Liwk;->e:D

    cmpl-double v3, v8, v6

    if-lez v3, :cond_a

    mul-double v6, v8, v8

    cmpl-double v3, v6, v4

    if-lez v3, :cond_a

    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct/range {p0 .. p0}, Liwk;->c()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Liwk;->b:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liwl;

    const/4 v4, 0x0

    iput v4, v3, Liwl;->a:I

    iget-object v4, v3, Liwl;->b:Liwi;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v4, v0, v1, v2}, Liwk;->a(Liwi;Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Liwk;->a:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :goto_4
    return-void

    :cond_a
    iget-object v3, v10, Liwl;->b:Liwi;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, Liwi;->b(Lixh;Lixh;Lixh;)V

    const/4 v3, 0x0

    iput v3, v10, Liwl;->a:I

    goto :goto_4

    :cond_b
    move-object v7, v10

    move-wide/from16 v16, v4

    move-wide/from16 v3, v16

    move-wide v5, v8

    goto :goto_3
.end method
