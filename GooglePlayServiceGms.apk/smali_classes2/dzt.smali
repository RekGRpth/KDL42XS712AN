.class final Ldzt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/database/CharArrayBuffer;

.field final d:Landroid/view/View;

.field final e:Landroid/widget/TextView;

.field final f:Landroid/database/CharArrayBuffer;

.field final g:Landroid/view/View;

.field final h:Landroid/widget/TextView;

.field final i:Landroid/widget/TextView;

.field final j:Landroid/view/View;

.field final synthetic k:Ldzs;


# direct methods
.method public constructor <init>(Ldzs;Landroid/view/View;)V
    .locals 2

    const/16 v1, 0x40

    iput-object p1, p0, Ldzt;->k:Ldzs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Ldzt;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzt;->b:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldzt;->c:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aD:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzt;->d:Landroid/view/View;

    sget v0, Lxa;->aF:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzt;->e:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldzt;->f:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aM:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzt;->g:Landroid/view/View;

    sget v0, Lxa;->S:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzt;->h:Landroid/widget/TextView;

    sget v0, Lxa;->aL:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldzt;->i:Landroid/widget/TextView;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzt;->j:Landroid/view/View;

    invoke-static {p1}, Ldzs;->a(Ldzs;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzt;->j:Landroid/view/View;

    invoke-static {p1}, Ldzs;->b(Ldzs;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
