.class Lhyy;
.super Lbqh;
.source "SourceFile"


# instance fields
.field final c:Lhys;

.field final synthetic d:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhyy;->d:Lhyt;

    invoke-direct {p0}, Lbqh;-><init>()V

    iput-object p2, p0, Lhyy;->c:Lhys;

    return-void
.end method


# virtual methods
.method protected final a(Lhzp;)Lbqh;
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lill;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lhyv;->a:[I

    invoke-virtual {p1}, Lhzp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->d(Lhyt;)Lhzg;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->e(Lhyt;)Lhzh;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->f(Lhyt;)Lhzf;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->g(Lhyt;)Lhzi;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->h(Lhyt;)Lhza;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lhyy;->d:Lhyt;

    invoke-static {v0}, Lhyt;->i(Lhyt;)Lhyw;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 1

    const-string v0, "\nDump of current state:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "\n    "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhyy;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 3

    const-string v0, "GeofencerStateMachine"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhyy;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final b(Landroid/os/Message;)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhyy;->d:Lhyt;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lhyt;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhym;

    iget-object v1, v0, Lhym;->d:Ljava/lang/Object;

    check-cast v1, Ljava/io/PrintWriter;

    iget-object v4, p0, Lhyy;->c:Lhys;

    const-string v2, "Registered geofences:\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->h:Lhxq;

    invoke-virtual {v2}, Lhxq;->d()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, v4, Lhys;->h:Lhxq;

    invoke-virtual {v2, v1}, Lhxq;->a(Ljava/io/PrintWriter;)V

    :goto_1
    const-string v2, "\nGeofences in blacklist:\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->i:Lhxq;

    invoke-virtual {v2}, Lhxq;->d()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, v4, Lhys;->i:Lhxq;

    invoke-virtual {v2, v1}, Lhxq;->a(Ljava/io/PrintWriter;)V

    :goto_2
    iget-object v2, v4, Lhys;->d:Lhxw;

    iget-object v2, v2, Lhxw;->c:Landroid/util/Pair;

    if-eqz v2, :cond_1

    const-string v2, "\nLast location:\n    "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->d:Lhxw;

    iget-object v2, v2, Lhxw;->c:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Landroid/location/Location;

    const-string v5, "Time="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v2, "\n    "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->d:Lhxw;

    iget-object v2, v2, Lhxw;->c:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    :cond_1
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->l:Lhyl;

    invoke-interface {v2, v1}, Lhyl;->a(Ljava/io/PrintWriter;)V

    const-string v2, "\nLocation update interval="

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->g:Lhzk;

    iget v2, v2, Lhzk;->f:I

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v4, Lhys;->f:Lhyi;

    iget-object v4, v2, Lhyi;->h:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    const-string v5, "Dump of ActivityDetector:\n    Interval="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v2, v2, Lhyi;->j:I

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lhyy;->d:Lhyt;

    invoke-static {v2}, Lhyt;->c(Lhyt;)Lbqg;

    move-result-object v2

    check-cast v2, Lhyy;

    invoke-virtual {v2, v1}, Lhyy;->a(Ljava/io/PrintWriter;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lhyy;->d:Lhyt;

    invoke-virtual {v2, v1}, Lhyt;->a(Ljava/io/PrintWriter;)V

    invoke-virtual {v0, v8}, Lhym;->a(Ljava/lang/Object;)V

    move v0, v3

    goto/16 :goto_0

    :cond_2
    const-string v2, "    <none>"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    const-string v2, "    <none>"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :sswitch_1
    const-string v1, "GeofencerStateMachine"

    const-string v2, "SM_STOP_CMD should never be sent for now."

    invoke-static {v1, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhym;

    invoke-virtual {v0, v8}, Lhym;->a(Ljava/lang/Object;)V

    move v0, v3

    goto/16 :goto_0

    :sswitch_3
    iget-object v1, p0, Lhyy;->c:Lhys;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lhys;->a(Landroid/content/Intent;)V

    move v0, v3

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lhyy;->c:Lhys;

    invoke-virtual {v0}, Lhys;->g()V

    move v0, v3

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x9 -> :sswitch_3
        0xc -> :sswitch_4
        0x62 -> :sswitch_2
        0x63 -> :sswitch_0
    .end sparse-switch
.end method

.method public c()V
    .locals 3

    const-string v0, "GeofencerStateMachine"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exit: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhyy;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
