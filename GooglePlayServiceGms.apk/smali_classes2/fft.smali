.class public Lfft;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lbpe;

.field private final d:Lbmi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lfft;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfft;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lbmi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfft;->b:Landroid/content/Context;

    iget-object v0, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    iput-object v0, p0, Lfft;->c:Lbpe;

    iput-object p2, p0, Lfft;->d:Lbmi;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lfft;
    .locals 1

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->g()Lfft;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lfft;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lbmi;

    sget-object v1, Lfbd;->b:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lfft;

    invoke-direct {v1, p0, v0}, Lfft;-><init>(Landroid/content/Context;Lbmi;)V

    return-object v1
.end method

.method private c(Ljava/lang/String;)[B
    .locals 4

    iget-object v0, p0, Lfft;->d:Lbmi;

    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lbmi;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleIS"

    const-string v2, "Unable to load image from the server."

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->a()Lfbe;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lfbe;->a(I)V

    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->a()Lfbe;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lfbe;->a(J)V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    new-instance v0, Lbki;

    invoke-direct {v0, p1}, Lbki;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbki;->b()Lbkh;

    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lfjp;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbki;->a(I)Lbkh;

    iput-boolean p2, v0, Lbki;->d:Z

    invoke-virtual {v0}, Lbki;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)[B
    .locals 4

    invoke-static {p1}, Lirg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lfft;->d:Lbmi;

    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lbmi;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lffl;->b:[B

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)[B
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lfft;->b(Ljava/lang/String;Z)[B
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lffl;->b:[B

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)[B
    .locals 4

    invoke-static {p1}, Lirg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfft;->d(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, v0}, Lfft;->c(Ljava/lang/String;)[B
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lfjp;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lffl;->a:[B

    goto :goto_0

    :cond_0
    const-string v1, "PeopleIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    sget-object v0, Lffl;->b:[B

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Z)[B
    .locals 11

    const-wide/16 v9, 0x3e8

    const/4 v7, 0x3

    iget-object v0, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v0

    iget-object v1, p0, Lfft;->c:Lbpe;

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    div-long/2addr v1, v9

    invoke-virtual {v0}, Lfbe;->b()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-gez v0, :cond_0

    new-instance v0, Lsp;

    const-string v1, "No request was sent, since we are currently backing off. A request at this time would likely fail."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PeopleIS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1}, Lfft;->c(Ljava/lang/String;)[B
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v2, v0

    if-eqz p2, :cond_2

    invoke-static {v2}, Lfjp;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lffl;->a:[B

    goto :goto_0

    :cond_2
    const-string v0, "PeopleIS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v3

    iget-object v0, p0, Lfft;->b:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->a()Lfbe;

    move-result-object v4

    iget-object v0, v4, Lfbe;->a:Landroid/content/SharedPreferences;

    const-string v1, "avatar_fetch_backoff_sec"

    const/4 v5, 0x0

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sget-object v0, Lfbd;->D:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v0, Lfbd;->C:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v6, "PeopleService"

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "PeopleIS"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "avatar backoff: lb="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bos="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exp="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-lez v1, :cond_5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v5, :cond_5

    mul-int/lit8 v0, v0, 0x2

    move v1, v0

    :goto_1
    sget-object v0, Lfbd;->E:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v4, v0}, Lfbe;->a(I)V

    if-lez v0, :cond_4

    iget-object v1, p0, Lfft;->c:Lbpe;

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v4

    div-long/2addr v4, v9

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v3}, Lfbe;->b()J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lfbe;->a(J)V

    iget-object v1, p0, Lfft;->b:Landroid/content/Context;

    const-string v3, "PeopleIS"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "avatar backoff="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " delay until="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v1, v3, v0, v4}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    throw v2

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;Z)[B
    .locals 2

    invoke-static {p1}, Lirg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lfft;->d(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lfft;->a(Ljava/lang/String;Z)[B

    move-result-object v0

    return-object v0
.end method
