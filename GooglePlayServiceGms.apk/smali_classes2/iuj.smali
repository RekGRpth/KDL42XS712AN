.class public final Liuj;
.super Lizp;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Liug;

.field public e:Liuk;

.field public f:Liui;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizp;-><init>()V

    iput-object v0, p0, Liuj;->a:Ljava/lang/Integer;

    iput-object v0, p0, Liuj;->b:Ljava/lang/String;

    iput-object v0, p0, Liuj;->c:Ljava/lang/String;

    iput-object v0, p0, Liuj;->d:Liug;

    iput-object v0, p0, Liuj;->e:Liuk;

    iput-object v0, p0, Liuj;->f:Liui;

    iput-object v0, p0, Liuj;->q:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Liuj;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizp;->a()I

    move-result v0

    iget-object v1, p0, Liuj;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Liuj;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Liuj;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Liuj;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Liuj;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Liuj;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Liuj;->d:Liug;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Liuj;->d:Liug;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Liuj;->e:Liuk;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Liuj;->e:Liuk;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Liuj;->f:Liui;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Liuj;->f:Liui;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Liuj;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liuj;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Liuj;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liuj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liuj;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Liuj;->d:Liug;

    if-nez v0, :cond_1

    new-instance v0, Liug;

    invoke-direct {v0}, Liug;-><init>()V

    iput-object v0, p0, Liuj;->d:Liug;

    :cond_1
    iget-object v0, p0, Liuj;->d:Liug;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Liuj;->e:Liuk;

    if-nez v0, :cond_2

    new-instance v0, Liuk;

    invoke-direct {v0}, Liuk;-><init>()V

    iput-object v0, p0, Liuj;->e:Liuk;

    :cond_2
    iget-object v0, p0, Liuj;->e:Liuk;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Liuj;->f:Liui;

    if-nez v0, :cond_3

    new-instance v0, Liui;

    invoke-direct {v0}, Liui;-><init>()V

    iput-object v0, p0, Liuj;->f:Liui;

    :cond_3
    iget-object v0, p0, Liuj;->f:Liui;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Liuj;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liuj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Liuj;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liuj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Liuj;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liuj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Liuj;->d:Liug;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Liuj;->d:Liug;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    iget-object v0, p0, Liuj;->e:Liuk;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Liuj;->e:Liuk;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    iget-object v0, p0, Liuj;->f:Liui;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Liuj;->f:Liui;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_5
    invoke-super {p0, p1}, Lizp;->a(Lizn;)V

    return-void
.end method
