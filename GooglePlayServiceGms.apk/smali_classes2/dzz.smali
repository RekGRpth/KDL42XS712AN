.class public final Ldzz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lbhb;

.field final b:Lbhb;


# direct methods
.method public constructor <init>(Lbgo;)V
    .locals 8

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbhb;

    invoke-direct {v0}, Lbhb;-><init>()V

    iput-object v0, p0, Ldzz;->a:Lbhb;

    new-instance v0, Lbhb;

    invoke-direct {v0}, Lbhb;-><init>()V

    iput-object v0, p0, Ldzz;->b:Lbhb;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v5

    move v4, v0

    move-object v3, v1

    :goto_0
    if-ge v4, v5, :cond_4

    invoke-virtual {p1, v4}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/InvitationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/InvitationEntity;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->k()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    :cond_0
    iget-object v2, p0, Ldzz;->a:Lbhb;

    invoke-virtual {v2, v0}, Lbhb;->a(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v1, v3

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    if-eqz v1, :cond_2

    new-instance v3, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Ldzz;->b:Lbhb;

    invoke-virtual {v1, v3}, Lbhb;->a(Ljava/lang/Object;)V

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v3

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    new-instance v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Ldzz;->b:Lbhb;

    invoke-virtual {v1, v0}, Lbhb;->a(Ljava/lang/Object;)V

    :cond_5
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    instance-of v0, p1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzz;->b:Lbhb;

    invoke-virtual {v0, p1}, Lbhb;->b(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldzz;->a:Lbhb;

    invoke-virtual {v0, p1}, Lbhb;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
