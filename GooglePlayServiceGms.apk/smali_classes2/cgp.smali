.class public abstract Lcgp;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lcgs;


# instance fields
.field private a:Z

.field private final b:I


# direct methods
.method protected constructor <init>(I)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    iput-boolean v0, p0, Lcgp;->a:Z

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Size must be nonnegative."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcgp;->b:I

    return-void
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Lcgp;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Result list is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(I)Ljava/lang/Object;
.end method

.method protected abstract a()V
.end method

.method public final b()Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcgp;->c()V

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lcgp;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcgp;->close()V

    throw v0
.end method

.method public final close()V
    .locals 1

    iget-boolean v0, p0, Lcgp;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgp;->a:Z

    invoke-virtual {p0}, Lcgp;->a()V

    :cond_0
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcgp;->c()V

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcgp;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p0, p1}, Lcgp;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    invoke-direct {p0}, Lcgp;->c()V

    iget v0, p0, Lcgp;->b:I

    return v0
.end method
