.class public final Lelk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lemp;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/io/File;

.field private final d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/io/File;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lelk;->a:Ljava/lang/String;

    iput-object p2, p0, Lelk;->b:Landroid/content/Context;

    iput-object p3, p0, Lelk;->c:Ljava/io/File;

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lelk;->d:J

    return-void
.end method

.method private b()Lelj;
    .locals 6

    :try_start_0
    new-instance v0, Lelj;

    iget-object v1, p0, Lelk;->b:Landroid/content/Context;

    iget-object v2, p0, Lelk;->c:Ljava/io/File;

    iget-object v3, p0, Lelk;->a:Ljava/lang/String;

    iget-wide v4, p0, Lelk;->d:J

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Unable to create storage"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lelk;->b()Lelj;

    move-result-object v0

    return-object v0
.end method
