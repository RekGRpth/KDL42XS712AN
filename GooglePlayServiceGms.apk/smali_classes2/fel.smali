.class public final Lfel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:F

.field private e:J

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfel;->a:Lcom/google/android/gms/people/profile/AvatarView;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-boolean v0, p0, Lfel;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lfel;->c:F

    iget v1, p0, Lfel;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v0, p0, Lfel;->e:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lfel;->e:J

    sub-long v0, v2, v0

    :goto_1
    iget v4, p0, Lfel;->d:F

    long-to-float v0, v0

    mul-float/2addr v0, v4

    iget v1, p0, Lfel;->c:F

    iget v4, p0, Lfel;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    iget v1, p0, Lfel;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lfel;->b:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    :cond_2
    iget v1, p0, Lfel;->c:F

    iget v4, p0, Lfel;->b:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_4

    iget v1, p0, Lfel;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lfel;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_4

    :cond_3
    iget v0, p0, Lfel;->b:F

    iget v1, p0, Lfel;->c:F

    sub-float/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lfel;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-static {v1, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;F)V

    iget v1, p0, Lfel;->c:F

    add-float/2addr v0, v1

    iput v0, p0, Lfel;->c:F

    iget v0, p0, Lfel;->c:F

    iget v1, p0, Lfel;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfel;->f:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfel;->g:Z

    :cond_5
    iput-wide v2, p0, Lfel;->e:J

    :cond_6
    iget-boolean v0, p0, Lfel;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfel;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_1
.end method
