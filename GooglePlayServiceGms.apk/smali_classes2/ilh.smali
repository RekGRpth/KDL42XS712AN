.class final Lilh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:[I

.field final synthetic b:Lilg;


# direct methods
.method constructor <init>(Lilg;[I)V
    .locals 0

    iput-object p1, p0, Lilh;->b:Lilg;

    iput-object p2, p0, Lilh;->a:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lilh;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lilh;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lilh;->b:Lilg;

    iget-object v3, v3, Lilg;->a:[Lilf;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lilf;->h()I

    move-result v3

    if-ge v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 8

    const/4 v4, -0x1

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v0, 0x0

    move v3, v4

    :goto_0
    iget-object v5, p0, Lilh;->a:[I

    array-length v5, v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lilh;->b:Lilg;

    iget-object v5, v5, Lilg;->a:[Lilf;

    aget-object v5, v5, v0

    iget-object v6, p0, Lilh;->a:[I

    aget v6, v6, v0

    invoke-virtual {v5}, Lilf;->h()I

    move-result v7

    if-ge v6, v7, :cond_0

    invoke-virtual {v5, v6}, Lilf;->d(I)J

    move-result-wide v5

    cmp-long v7, v5, v1

    if-lez v7, :cond_0

    move-wide v1, v5

    move v3, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-ne v3, v4, :cond_2

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "There are no more log descriptions"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lilh;->b:Lilg;

    iget-object v0, v0, Lilg;->a:[Lilf;

    aget-object v0, v0, v3

    iget-object v1, p0, Lilh;->a:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lilf;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lilh;->a:[I

    aget v2, v1, v3

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v3

    return-object v0
.end method

.method public final remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t remove log descriptions."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
