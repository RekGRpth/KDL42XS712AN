.class public final Lids;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Livi;


# instance fields
.field final b:Licm;

.field final c:Lidq;

.field volatile d:Livi;

.field volatile e:J

.field private final f:Lils;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Livi;

    sget-object v1, Lihj;->aw:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    sput-object v0, Lids;->a:Livi;

    return-void
.end method

.method public constructor <init>(Licm;Lidq;Lils;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lids;->a:Livi;

    iput-object v0, p0, Lids;->d:Livi;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lids;->e:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lids;->g:Ljava/lang/Object;

    iput-object p1, p0, Lids;->b:Licm;

    iput-object p2, p0, Lids;->c:Lidq;

    iput-object p3, p0, Lids;->f:Lils;

    return-void
.end method

.method private static a(Lidq;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lidq;->d()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_params"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    invoke-virtual {v0, p1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Livi;->b(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Livi;J)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lids;->a:Livi;

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Livi;->d(I)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    add-long/2addr v1, p2

    iget-object v3, p0, Lids;->b:Licm;

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private w()Livi;
    .locals 4

    iget-object v1, p0, Lids;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lids;->d:Livi;

    sget-object v2, Lids;->a:Livi;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lids;->d:Livi;

    iget-wide v2, p0, Lids;->e:J

    invoke-direct {p0, v0, v2, v3}, Lids;->a(Livi;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lids;->x()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lids;->d:Livi;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized x()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lids;->g:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lids;->d:Livi;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Livi;->c(I)I

    move-result v0

    sget-object v2, Lids;->a:Livi;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Livi;->c(I)I

    move-result v2

    sget-object v3, Lids;->a:Livi;

    iput-object v3, p0, Lids;->d:Livi;

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lids;->e:J

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lids;->f:Lils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lids;->f:Lils;

    invoke-interface {v0, p0}, Lils;->a(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    const/4 v8, 0x1

    iget-object v0, p0, Lids;->c:Lidq;

    invoke-static {v0}, Lids;->a(Lidq;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lids;->x()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v4}, Lids;->a(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "NlpParametersState"

    const-string v1, "Using NlpParameterId: %d."

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lids;->d:Livi;

    invoke-virtual {v4, v8}, Livi;->c(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iget-object v0, p0, Lids;->b:Licm;

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v0

    iget-object v5, p0, Lids;->b:Licm;

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    sub-long v5, v0, v5

    cmp-long v7, v2, v0

    if-lez v7, :cond_4

    :goto_1
    sub-long/2addr v0, v5

    sget-object v2, Lihj;->aw:Livk;

    invoke-static {v4, v2}, Lilv;->a(Ljava/io/InputStream;Livk;)Livi;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lids;->a(Livi;J)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lids;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iput-object v2, p0, Lids;->d:Livi;

    iput-wide v0, p0, Lids;->e:J

    iget-object v0, p0, Lids;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    sget-object v1, Lids;->a:Livi;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lids;->f:Lils;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lids;->f:Lils;

    invoke-interface {v0, p0}, Lils;->a(Ljava/lang/Object;)V

    :cond_3
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    :try_start_5
    invoke-static {v4}, Lids;->a(Ljava/io/Closeable;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lids;->x()V

    goto :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_1
    move-exception v0

    :try_start_7
    invoke-direct {p0}, Lids;->x()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    invoke-static {v4}, Lids;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    :cond_5
    :try_start_9
    invoke-direct {p0}, Lids;->x()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_a
    invoke-static {v4}, Lids;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_0
.end method

.method public final a(Livi;)V
    .locals 9

    iget-object v2, p0, Lids;->g:Ljava/lang/Object;

    monitor-enter v2

    if-eqz p1, :cond_2

    :try_start_0
    iget-object v0, p0, Lids;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    int-to-long v0, v0

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Livi;->c(I)I

    move-result v3

    int-to-long v3, v3

    iput-object p1, p0, Lids;->d:Livi;

    iget-object v5, p0, Lids;->b:Licm;

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    iput-wide v5, p0, Lids;->e:J

    cmp-long v0, v3, v0

    if-eqz v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NlpParametersState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Updating NlpParameters. New version: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lids;->c:Lidq;

    invoke-static {v0}, Lids;->a(Lidq;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lids;->d:Livi;

    sget-object v4, Lids;->a:Livi;

    if-ne v1, v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_1
    :goto_0
    :try_start_2
    invoke-static {v0}, Lids;->a(Ljava/io/Closeable;)V

    :goto_1
    iget-object v0, p0, Lids;->f:Lils;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lids;->f:Lils;

    invoke-interface {v0, p0}, Lils;->a(Ljava/lang/Object;)V

    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :cond_3
    :try_start_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    :cond_4
    iget-object v1, p0, Lids;->c:Lidq;

    invoke-interface {v1, v3}, Lidq;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v0, 0x2

    :try_start_4
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v3, p0, Lids;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    iget-wide v4, p0, Lids;->e:J

    iget-object v0, p0, Lids;->b:Licm;

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v0, p0, Lids;->d:Livi;

    invoke-virtual {v0}, Livi;->f()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_7
    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_5

    const-string v1, "NlpParametersState"

    const-string v3, "Failed to write parameter state."

    invoke-static {v1, v3}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :cond_5
    :try_start_8
    invoke-static {v0}, Lids;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_2
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_3
    :try_start_9
    invoke-static {v1}, Lids;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_3
    move-exception v0

    goto :goto_3

    :catchall_4
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    sget-object v1, Lids;->a:Livi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lids;->a(I)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lids;->a(I)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lids;->a(I)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lids;->a(I)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lids;->a(I)Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 4

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final m()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final p()I
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    return v0
.end method

.method public final q()Lidt;
    .locals 9

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v5

    new-instance v0, Lidt;

    const/16 v1, 0x16

    invoke-virtual {v5, v1}, Livi;->c(I)I

    move-result v1

    int-to-long v1, v1

    const/16 v3, 0x22

    invoke-virtual {v5, v3}, Livi;->c(I)I

    move-result v3

    int-to-long v3, v3

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Livi;->c(I)I

    move-result v5

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lidt;-><init>(JJJ)V

    return-object v0
.end method

.method public final r()Lidt;
    .locals 9

    const-wide/16 v7, 0x3e8

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v5

    new-instance v0, Lidt;

    const/16 v1, 0x15

    invoke-virtual {v5, v1}, Livi;->c(I)I

    move-result v1

    int-to-long v1, v1

    mul-long/2addr v1, v7

    const/16 v3, 0x1f

    invoke-virtual {v5, v3}, Livi;->c(I)I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v3, v7

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Livi;->c(I)I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lidt;-><init>(JJJ)V

    return-object v0
.end method

.method public final s()Lidt;
    .locals 7

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v5

    new-instance v0, Lidt;

    const/16 v1, 0x17

    invoke-virtual {v5, v1}, Livi;->c(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x24

    invoke-virtual {v5, v3}, Livi;->c(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Livi;->c(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lidt;-><init>(JJJ)V

    return-object v0
.end method

.method public final t()Lidt;
    .locals 7

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v5

    new-instance v0, Lidt;

    const/16 v1, 0x18

    invoke-virtual {v5, v1}, Livi;->c(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x26

    invoke-virtual {v5, v3}, Livi;->c(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x27

    invoke-virtual {v5, v6}, Livi;->c(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lidt;-><init>(JJJ)V

    return-object v0
.end method

.method public final u()J
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public final v()J
    .locals 2

    invoke-direct {p0}, Lids;->w()Livi;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method
