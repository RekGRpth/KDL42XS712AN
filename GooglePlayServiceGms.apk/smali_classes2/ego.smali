.class public final Lego;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    iget-wide v2, p0, Lego;->a:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-wide v2, p0, Lego;->b:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lego;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lego;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "Herrevad"

    const-string v2, "Not all required fields have been set before getHerrevadId()"

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lego;->a:J

    iget-object v0, p0, Lego;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lego;->b:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    move v2, v1

    move v0, v1

    :goto_2
    const/16 v4, 0xa

    if-ge v2, v4, :cond_3

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v4, p0, Lego;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v0, v4, :cond_3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lego;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v0, v2, :cond_4

    move v0, v1

    :cond_4
    sget-object v1, Legk;->f:Lbgm;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbgm;->a(Ljava/lang/Object;)V

    sget-object v1, Legk;->g:Lbgm;

    iget-wide v2, p0, Lego;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbgm;->a(Ljava/lang/Object;)V

    move v1, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lego;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method
