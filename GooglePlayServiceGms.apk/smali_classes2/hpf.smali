.class public final enum Lhpf;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhpf;

.field public static final enum b:Lhpf;

.field public static final enum c:Lhpf;

.field public static final enum d:Lhpf;

.field public static final enum e:Lhpf;

.field private static final synthetic f:[Lhpf;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhpf;

    const-string v1, "IN_CAR"

    invoke-direct {v0, v1, v2}, Lhpf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpf;->a:Lhpf;

    new-instance v0, Lhpf;

    const-string v1, "ON_BICYCLE"

    invoke-direct {v0, v1, v3}, Lhpf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpf;->b:Lhpf;

    new-instance v0, Lhpf;

    const-string v1, "ON_FOOT"

    invoke-direct {v0, v1, v4}, Lhpf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpf;->c:Lhpf;

    new-instance v0, Lhpf;

    const-string v1, "STILL"

    invoke-direct {v0, v1, v5}, Lhpf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpf;->d:Lhpf;

    new-instance v0, Lhpf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lhpf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpf;->e:Lhpf;

    const/4 v0, 0x5

    new-array v0, v0, [Lhpf;

    sget-object v1, Lhpf;->a:Lhpf;

    aput-object v1, v0, v2

    sget-object v1, Lhpf;->b:Lhpf;

    aput-object v1, v0, v3

    sget-object v1, Lhpf;->c:Lhpf;

    aput-object v1, v0, v4

    sget-object v1, Lhpf;->d:Lhpf;

    aput-object v1, v0, v5

    sget-object v1, Lhpf;->e:Lhpf;

    aput-object v1, v0, v6

    sput-object v0, Lhpf;->f:[Lhpf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhpf;
    .locals 1

    const-class v0, Lhpf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhpf;

    return-object v0
.end method

.method public static values()[Lhpf;
    .locals 1

    sget-object v0, Lhpf;->f:[Lhpf;

    invoke-virtual {v0}, [Lhpf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpf;

    return-object v0
.end method
