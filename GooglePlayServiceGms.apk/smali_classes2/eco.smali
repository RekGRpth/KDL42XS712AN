.class public abstract Leco;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# static fields
.field protected static final a:Lecq;

.field private static final f:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private static final g:Landroid/graphics/Rect;

.field private static final h:Landroid/graphics/Rect;


# instance fields
.field protected final b:Landroid/content/Context;

.field protected final c:Lbhw;

.field protected final d:Landroid/widget/FrameLayout;

.field protected e:Landroid/view/View;

.field private final i:Landroid/view/animation/Animation;

.field private final j:Landroid/view/animation/Animation;

.field private final k:Ldax;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lecq;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lecq;-><init>(Landroid/os/Looper;B)V

    sput-object v0, Leco;->a:Lecq;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Leco;->g:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Leco;->h:Landroid/graphics/Rect;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ldax;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leco;->l:Z

    iput-object p1, p0, Leco;->b:Landroid/content/Context;

    invoke-static {p1}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v0

    iput-object v0, p0, Leco;->c:Lbhw;

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Leco;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    iput-object p2, p0, Leco;->k:Ldax;

    iget-object v0, p0, Leco;->b:Landroid/content/Context;

    const v1, 0x7f05000d    # com.google.android.gms.R.anim.show_popup

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Leco;->i:Landroid/view/animation/Animation;

    iget-object v0, p0, Leco;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Leco;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v0, p0, Leco;->b:Landroid/content/Context;

    const v1, 0x7f05000c    # com.google.android.gms.R.anim.hide_popup

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Leco;->j:Landroid/view/animation/Animation;

    iget-object v0, p0, Leco;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Leco;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    return-void
.end method

.method static synthetic a(Leco;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Leco;->k:Ldax;

    iget-object v0, v0, Ldax;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    new-instance v0, Lecr;

    invoke-direct {v0, v2}, Lecr;-><init>(B)V

    const/4 v1, 0x1

    new-array v1, v1, [Leco;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lecr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method static synthetic b(Leco;)V
    .locals 2

    iget-object v0, p0, Leco;->e:Landroid/view/View;

    iget-object v1, p0, Leco;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic c(Leco;)V
    .locals 3

    sget-object v1, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leco;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    sget-object v2, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Leco;->e()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()V
    .locals 5

    const/16 v0, 0xc

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v1, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leco;

    invoke-direct {v0}, Leco;->g()V

    sget-object v3, Leco;->a:Lecq;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lecq;->removeMessages(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void
.end method

.method static synthetic d(Leco;)V
    .locals 0

    invoke-direct {p0}, Leco;->f()V

    return-void
.end method

.method private e()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Leco;->b:Landroid/content/Context;

    iget-object v1, p0, Leco;->k:Ldax;

    iget v1, v1, Ldax;->c:I

    invoke-static {v0, v1}, Lect;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/WindowManager;

    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v7}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v0, 0x18

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/4 v0, -0x3

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v2}, Landroid/widget/FrameLayout;->measure(II)V

    iget-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    iget-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v2, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v0, 0xc

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x3e8

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Leco;->k:Ldax;

    iget-object v0, v0, Ldax;->a:Landroid/os/IBinder;

    iput-object v0, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/16 v0, 0x33

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    sget-object v0, Leco;->g:Landroid/graphics/Rect;

    iget-object v3, p0, Leco;->k:Ldax;

    iget v3, v3, Ldax;->d:I

    iput v3, v0, Landroid/graphics/Rect;->left:I

    sget-object v0, Leco;->g:Landroid/graphics/Rect;

    iget-object v3, p0, Leco;->k:Ldax;

    iget v3, v3, Ldax;->e:I

    iput v3, v0, Landroid/graphics/Rect;->top:I

    sget-object v0, Leco;->g:Landroid/graphics/Rect;

    iget-object v3, p0, Leco;->k:Ldax;

    iget v3, v3, Ldax;->g:I

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    sget-object v0, Leco;->g:Landroid/graphics/Rect;

    iget-object v3, p0, Leco;->k:Ldax;

    iget v3, v3, Ldax;->f:I

    iput v3, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Leco;->k:Ldax;

    iget v0, v0, Ldax;->b:I

    if-nez v0, :cond_0

    const/16 v0, 0x31

    :cond_0
    const/16 v3, 0x11

    invoke-static {v3}, Lbpz;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    sget-object v3, Leco;->g:Landroid/graphics/Rect;

    sget-object v4, Leco;->h:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v5

    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    :goto_0
    sget-object v0, Leco;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    sget-object v0, Leco;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    :goto_1
    :try_start_0
    iget-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-interface {v6, v0, v7}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Leco;->l:Z

    iget-object v0, p0, Leco;->e:Landroid/view/View;

    iget-object v1, p0, Leco;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_2
    return-void

    :cond_1
    sget-object v3, Leco;->g:Landroid/graphics/Rect;

    sget-object v4, Leco;->h:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x7d5

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    iget-object v0, p0, Leco;->k:Ldax;

    iget v0, v0, Ldax;->b:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "BasePopup"

    const-string v1, "Cannot show the popup as the given window token is not valid. Either the given view is not attached to a window or you tried to connect the GoogleApiClient in the same lifecycle step as the creation of the GoogleApiClient. See GoogleApiClient.Builder.create() and  GoogleApiClient.connect() for more information."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Leco;->f()V

    goto :goto_2
.end method

.method private f()V
    .locals 2

    sget-object v1, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Leco;->g()V

    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Leco;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leco;

    invoke-direct {v0}, Leco;->e()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private g()V
    .locals 2

    :try_start_0
    iget-boolean v0, p0, Leco;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leco;->b:Landroid/content/Context;

    iget-object v1, p0, Leco;->k:Ldax;

    iget v1, v1, Ldax;->c:I

    invoke-static {v0, v1}, Lect;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Leco;->d:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leco;->l:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "BasePopup"

    const-string v1, "Popup is not attached to a window, so not attempting to remove it."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Landroid/widget/ImageView;Landroid/net/Uri;I)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p0, Leco;->c:Lbhw;

    iget-object v2, p0, Leco;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, p2}, Lbhw;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v1, "BasePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse image content for icon URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_1

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected final b()V
    .locals 4

    iget-object v0, p0, Leco;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040067    # com.google.android.gms.R.layout.games_info_popup

    iget-object v2, p0, Leco;->d:Landroid/widget/FrameLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Leco;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0154    # com.google.android.gms.R.id.info_popup

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leco;->e:Landroid/view/View;

    invoke-virtual {p0}, Leco;->a()V

    return-void
.end method

.method protected final c()Z
    .locals 1

    iget-object v0, p0, Leco;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    iget-object v0, p0, Leco;->i:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    sget-object v0, Leco;->a:Lecq;

    sget-object v1, Leco;->a:Lecq;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Lecq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lecq;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Leco;->j:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    sget-object v0, Leco;->a:Lecq;

    new-instance v1, Lecp;

    invoke-direct {v1, p0}, Lecp;-><init>(Leco;)V

    invoke-virtual {v0, v1}, Lecq;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
