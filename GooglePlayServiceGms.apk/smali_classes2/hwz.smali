.class public final Lhwz;
.super Lhxa;
.source "SourceFile"


# instance fields
.field private final c:Lhwk;

.field private final d:Landroid/location/LocationListener;

.field private final e:Lhwf;

.field private final f:Lhvw;

.field private final k:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Lhwk;Landroid/location/LocationListener;Lhwf;Lhvw;Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0}, Lhxa;-><init>()V

    iput-object p1, p0, Lhwz;->c:Lhwk;

    iput-object p2, p0, Lhwz;->d:Landroid/location/LocationListener;

    iput-object p3, p0, Lhwz;->e:Lhwf;

    iput-object p4, p0, Lhwz;->f:Lhvw;

    iput-object p5, p0, Lhwz;->k:Landroid/os/Looper;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    const/4 v7, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x0

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhwz;->c:Lhwk;

    invoke-virtual {v0}, Lhwk;->a()Landroid/location/LocationManager;

    move-result-object v0

    invoke-static {v0}, Lift;->a(Landroid/location/LocationManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhwz;->e:Lhwf;

    iget-object v1, v0, Lhwf;->a:Landroid/location/LocationManager;

    iget-object v0, v0, Lhwf;->h:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v1, v0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, Lhwz;->f:Lhvw;

    iget-wide v1, p0, Lhxa;->a:J

    invoke-virtual {v0, v1, v2}, Lhvw;->a(J)V

    iget-object v0, p0, Lhwz;->c:Lhwk;

    const-string v1, "gps"

    iget-wide v2, p0, Lhxa;->a:J

    iget-object v4, p0, Lhwz;->d:Landroid/location/LocationListener;

    iget-object v5, p0, Lhwz;->k:Landroid/os/Looper;

    iget-object v6, p0, Lhxe;->i:Ljava/util/Collection;

    invoke-virtual/range {v0 .. v7}, Lhwk;->a(Ljava/lang/String;JLandroid/location/LocationListener;Landroid/os/Looper;Ljava/util/Collection;Z)V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Gps enabled with interval %s[ms]"

    new-array v1, v7, [Ljava/lang/Object;

    iget-wide v2, p0, Lhxa;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Gps not enabled because no gps device exists"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhwz;->e:Lhwf;

    iget-object v1, v0, Lhwf;->a:Landroid/location/LocationManager;

    iget-object v2, v0, Lhwf;->h:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    iput v8, v0, Lhwf;->c:I

    iput-boolean v8, v0, Lhwf;->d:Z

    iput-boolean v8, v0, Lhwf;->e:Z

    iput-boolean v8, v0, Lhwf;->f:Z

    iget-object v0, p0, Lhwz;->f:Lhvw;

    invoke-virtual {v0}, Lhvw;->e()V

    iget-object v0, p0, Lhwz;->c:Lhwk;

    invoke-virtual {v0}, Lhwk;->a()Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lhwz;->d:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GPS Disabled"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lhwz;->f:Lhvw;

    invoke-virtual {v0, p1}, Lhvw;->b(Z)V

    :cond_0
    invoke-super {p0, p1}, Lhxa;->a(Z)V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Gps location ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhwz;->a(Ljava/lang/StringBuilder;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
