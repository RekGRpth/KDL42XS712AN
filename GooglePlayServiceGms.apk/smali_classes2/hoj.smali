.class public final Lhoj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lidz;


# instance fields
.field public final a:Lhom;

.field public final b:Lhok;

.field public final c:Lidu;

.field final d:Lids;

.field public e:J

.field f:Z

.field public final g:Lidx;


# direct methods
.method public constructor <init>(Lidu;Lids;)V
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lidq;->c()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, p2}, Lhoj;-><init>(Lidu;Ljava/io/File;Lids;)V

    return-void
.end method

.method private constructor <init>(Lidu;Ljava/io/File;Lids;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhom;

    invoke-direct {v0}, Lhom;-><init>()V

    iput-object v0, p0, Lhoj;->a:Lhom;

    new-instance v0, Lhok;

    iget-object v1, p0, Lhoj;->a:Lhom;

    invoke-direct {v0, v1}, Lhok;-><init>(Lhom;)V

    iput-object v0, p0, Lhoj;->b:Lhok;

    iput-object p1, p0, Lhoj;->c:Lidu;

    iput-object p3, p0, Lhoj;->d:Lids;

    new-instance v0, Lidx;

    const/4 v1, 0x2

    invoke-interface {p1}, Lidu;->h()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p1}, Lidu;->i()[B

    move-result-object v4

    sget-object v5, Lihj;->aN:Livk;

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    iput-object v0, p0, Lhoj;->g:Lidx;

    invoke-virtual {p0}, Lhoj;->b()V

    return-void
.end method


# virtual methods
.method public final a(JJ)Lhol;
    .locals 4

    iget-object v0, p0, Lhoj;->b:Lhok;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-nez v0, :cond_1

    new-instance v0, Livi;

    sget-object v1, Lihj;->aO:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Livi;->a(IJ)Livi;

    new-instance v1, Lhol;

    invoke-direct {v1, v0}, Lhol;-><init>(Livi;)V

    iget-object v0, p0, Lhoj;->b:Lhok;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lhok;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-nez v0, :cond_0

    iget-object v0, p0, Lhoj;->a:Lhom;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lhom;->a(I)V

    :cond_0
    move-object v0, v1

    :cond_1
    iget-object v1, v0, Lhol;->a:Livi;

    const/4 v2, 0x3

    invoke-static {p3, p4}, Lhol;->b(J)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    return-object v0
.end method

.method public final a()Livi;
    .locals 4

    new-instance v1, Livi;

    sget-object v0, Lihj;->aN:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    iget-object v0, p0, Lhoj;->b:Lhok;

    invoke-virtual {v0}, Lhok;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const/4 v3, 0x3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    iget-object v0, v0, Lhol;->a:Livi;

    invoke-virtual {v1, v3, v0}, Livi;->a(ILivi;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iget-object v2, p0, Lhoj;->c:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    const/4 v0, 0x2

    iget-wide v2, p0, Lhoj;->e:J

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    const/4 v0, 0x4

    iget-boolean v2, p0, Lhoj;->f:Z

    invoke-virtual {v1, v0, v2}, Livi;->a(IZ)Livi;

    return-object v1
.end method

.method public final a(JZ)V
    .locals 0

    iput-wide p1, p0, Lhoj;->e:J

    iput-boolean p3, p0, Lhoj;->f:Z

    return-void
.end method

.method public final a(Livi;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4194997000000000L    # 8.64E7

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    iget-object v2, p0, Lhoj;->c:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-virtual {p0, v0, v1, v4}, Lhoj;->a(JZ)V

    iget-object v0, p0, Lhoj;->b:Lhok;

    invoke-virtual {v0}, Lhok;->clear()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SeenDevicesCache"

    const-string v1, "Clearing device cache, last refresh: %d"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lhoj;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(Livi;)V
    .locals 13

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Livi;->d(I)J

    move-result-wide v0

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Livi;->d(I)J

    move-result-wide v2

    add-long v8, v2, v0

    iget-object v2, p0, Lhoj;->c:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    iget-object v4, p0, Lhoj;->c:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v6

    sub-long v4, v2, v6

    cmp-long v10, v8, v2

    if-lez v10, :cond_3

    :goto_0
    sub-long/2addr v2, v4

    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Livi;->b(I)Z

    move-result v8

    invoke-virtual {p0, v2, v3, v8}, Lhoj;->a(JZ)V

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Livi;->k(I)I

    move-result v2

    if-ge v8, v2, :cond_5

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v8}, Livi;->c(II)Livi;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v9}, Livi;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_2

    new-instance v10, Lhol;

    invoke-direct {v10, v9}, Lhol;-><init>(Livi;)V

    iget-object v2, v10, Lhol;->a:Livi;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v10, Lhol;->a:Livi;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Livi;->c(I)I

    move-result v2

    int-to-long v2, v2

    const-wide/32 v11, 0x5265c00

    mul-long/2addr v2, v11

    invoke-static/range {v0 .. v7}, Lhol;->a(JJJJ)J

    move-result-wide v2

    iget-object v11, v10, Lhol;->a:Livi;

    const/4 v12, 0x2

    invoke-static {v2, v3}, Lhol;->b(J)I

    move-result v2

    invoke-virtual {v11, v12, v2}, Livi;->e(II)Livi;

    :cond_0
    iget-object v2, v10, Lhol;->a:Livi;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v10, Lhol;->a:Livi;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Livi;->c(I)I

    move-result v2

    int-to-long v2, v2

    const-wide/32 v11, 0x5265c00

    mul-long/2addr v2, v11

    invoke-static/range {v0 .. v7}, Lhol;->a(JJJJ)J

    move-result-wide v2

    iget-object v11, v10, Lhol;->a:Livi;

    const/4 v12, 0x3

    invoke-static {v2, v3}, Lhol;->b(J)I

    move-result v2

    invoke-virtual {v11, v12, v2}, Livi;->e(II)Livi;

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Livi;->d(I)J

    move-result-wide v2

    iget-object v9, p0, Lhoj;->b:Lhok;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v2, v10}, Lhok;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    :cond_3
    move-wide v2, v8

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    return-void
.end method

.method public final c()V
    .locals 12

    iget-object v0, p0, Lhoj;->c:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    sub-long v3, v0, v2

    const-wide/32 v5, 0x240c8400

    sub-long v5, v0, v5

    iget-object v0, p0, Lhoj;->b:Lhok;

    invoke-virtual {v0}, Lhok;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhol;

    iget-object v2, v1, Lhol;->a:Livi;

    const/4 v8, 0x3

    invoke-virtual {v2, v8}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, v1, Lhol;->a:Livi;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v1

    move v2, v1

    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhol;

    iget-object v8, v1, Lhol;->a:Livi;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v1, v1, Lhol;->a:Livi;

    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Livi;->c(I)I

    move-result v1

    :goto_2
    const v8, 0x7fffffff

    if-ne v2, v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    const v1, 0x7fffffff

    move v2, v1

    goto :goto_1

    :cond_2
    const v1, 0x7fffffff

    goto :goto_2

    :cond_3
    int-to-long v8, v2

    const-wide/32 v10, 0x5265c00

    mul-long/2addr v8, v10

    cmp-long v2, v8, v5

    if-gez v2, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    iget-object v1, p0, Lhoj;->a:Lhom;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lhom;->a(I)V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SeenDevicesCache"

    const-string v2, "Discarding %d because never seen for a long time."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    int-to-long v1, v1

    const-wide/32 v8, 0x5265c00

    mul-long/2addr v1, v8

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Lhoj;->a:Lhom;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lhom;->a(I)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhol;

    iget-object v2, v1, Lhol;->a:Livi;

    const/4 v8, 0x2

    invoke-static {v2, v8}, Lilv;->a(Livi;I)V

    iget-object v2, v1, Lhol;->a:Livi;

    const/4 v8, 0x4

    invoke-static {v2, v8}, Lilv;->a(Livi;I)V

    iget-object v2, v1, Lhol;->a:Livi;

    const/4 v8, 0x5

    invoke-static {v2, v8}, Lilv;->a(Livi;I)V

    iget-object v1, v1, Lhol;->a:Livi;

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lilv;->a(Livi;I)V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SeenDevicesCache"

    const-string v2, "Removing cache result of %d because it\'s too old."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    return-void
.end method
