.class public abstract enum Lbux;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbux;

.field public static final enum b:Lbux;

.field public static final enum c:Lbux;

.field public static final enum d:Lbux;

.field private static final synthetic f:[Lbux;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lbuy;

    const-string v1, "CHANGELOG"

    const-string v2, "changes"

    invoke-direct {v0, v1, v2}, Lbuy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbux;->a:Lbux;

    new-instance v0, Lbuz;

    const-string v1, "FULL"

    const-string v2, "full"

    invoke-direct {v0, v1, v2}, Lbuz;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbux;->b:Lbux;

    new-instance v0, Lbva;

    const-string v1, "QUERY"

    const-string v2, "query"

    invoke-direct {v0, v1, v2}, Lbva;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbux;->c:Lbux;

    new-instance v0, Lbvb;

    const-string v1, "APPDATA"

    const-string v2, "appData"

    invoke-direct {v0, v1, v2}, Lbvb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbux;->d:Lbux;

    const/4 v0, 0x4

    new-array v0, v0, [Lbux;

    const/4 v1, 0x0

    sget-object v2, Lbux;->a:Lbux;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbux;->b:Lbux;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbux;->c:Lbux;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbux;->d:Lbux;

    aput-object v2, v0, v1

    sput-object v0, Lbux;->f:[Lbux;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbux;->e:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lbux;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lbux;
    .locals 5

    invoke-static {}, Lbux;->values()[Lbux;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lbux;->e:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown FeedType value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbux;
    .locals 1

    const-class v0, Lbux;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbux;

    return-object v0
.end method

.method public static values()[Lbux;
    .locals 1

    sget-object v0, Lbux;->f:[Lbux;

    invoke-virtual {v0}, [Lbux;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbux;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)Lbuv;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbux;->e:Ljava/lang/String;

    return-object v0
.end method
