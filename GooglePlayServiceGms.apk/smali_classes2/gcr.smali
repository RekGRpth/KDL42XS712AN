.class public final Lgcr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lfsy;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcr;->b:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgcr;->a:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lgcr;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v1, Lbmx;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    invoke-virtual {v1, p1}, Lbmx;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/revoke?token="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lfrx;->a:Lbmi;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v2, v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    invoke-static {p1, v1}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lgcr;->a:Lfsy;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfsy;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcr;->a:Lfsy;

    invoke-interface {v0, v8, v1}, Lfsy;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcr;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcr;->a:Lfsy;

    invoke-interface {v1, v8, v0}, Lfsy;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcr;->a:Lfsy;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v7}, Lfsy;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lgcr;->a:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcr;->a:Lfsy;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfsy;->a(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
