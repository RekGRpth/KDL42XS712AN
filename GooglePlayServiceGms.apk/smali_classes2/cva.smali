.class final Lcva;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:Ldhu;


# instance fields
.field private final d:Ldnm;

.field private final e:Ldnn;

.field private final f:Ldpl;

.field private final g:Ldpm;

.field private final h:Lcwa;

.field private final i:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcva;->a:Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "score_order"

    aput-object v2, v0, v1

    sput-object v0, Lcva;->b:[Ljava/lang/String;

    invoke-static {}, Ldhu;->a()Ldhv;

    move-result-object v0

    const-string v1, "instance_id"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "page_type"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "default_display_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "default_display_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "default_display_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "rank"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "display_rank"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "raw_score"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "display_score"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "achieved_timestamp"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "score_tag"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_icon_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_hi_res_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "profile_hi_res_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "last_updated"

    sget-object v2, Ldhw;->d:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "is_in_circles"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    invoke-virtual {v0}, Ldhv;->a()Ldhu;

    move-result-object v0

    sput-object v0, Lcva;->c:Ldhu;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;Lbmi;)V
    .locals 2

    const-string v0, "LeaderboardAgent"

    sget-object v1, Lcva;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Ldnm;

    invoke-direct {v0, p2}, Ldnm;-><init>(Lbmi;)V

    iput-object v0, p0, Lcva;->d:Ldnm;

    new-instance v0, Ldnn;

    invoke-direct {v0, p3}, Ldnn;-><init>(Lbmi;)V

    iput-object v0, p0, Lcva;->e:Ldnn;

    new-instance v0, Ldpl;

    invoke-direct {v0, p2}, Ldpl;-><init>(Lbmi;)V

    iput-object v0, p0, Lcva;->f:Ldpl;

    new-instance v0, Ldpm;

    invoke-direct {v0, p3}, Ldpm;-><init>(Lbmi;)V

    iput-object v0, p0, Lcva;->g:Ldpm;

    new-instance v0, Lcwa;

    sget-object v1, Lcva;->c:Ldhu;

    iget-object v1, v1, Ldhu;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcwa;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcva;->h:Lcwa;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcva;->i:Ljava/util/HashMap;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    const-wide/16 v7, -0x1

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-gez v0, :cond_0

    const-string v0, "LeaderboardAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Game information not present for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v7

    :goto_0
    return-wide v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcva;->d:Ldnm;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, p4, v1}, Ldnm;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Ldng;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, -0x1

    move-object v0, p1

    move-object v1, p2

    invoke-static/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldng;JILjava/util/ArrayList;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    move-wide v0, v7

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "LeaderboardAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to retrieve leaderboard "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v7

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "LeaderboardAgent"

    invoke-static {v0, v6, v1}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    move-wide v0, v7

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;
    .locals 3

    invoke-static {p0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "leaderboard_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "timespan"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "collection"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 7

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    sget-object v0, Lcva;->c:Ldhu;

    iget-object v2, v0, Ldhu;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    sget-object v4, Lcva;->c:Ldhu;

    invoke-virtual {v4, v3}, Ldhu;->a(Ljava/lang/String;)Ldhw;

    move-result-object v4

    sget-object v5, Lcvb;->a:[I

    invoke-virtual {v4}, Ldhw;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type should not be in cursor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_2
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ldnl;)Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p0}, Ldnl;->getPlayerScore()Ldnh;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    const-string v2, "display_score"

    const-string v3, "player_display_score"

    invoke-static {v1, v2, v0, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v2, "raw_score"

    const-string v3, "player_raw_score"

    invoke-static {v1, v2, v0, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v2, "display_rank"

    const-string v3, "player_display_rank"

    invoke-static {v1, v2, v0, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v2, "rank"

    const-string v3, "player_rank"

    invoke-static {v1, v2, v0, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v2, "score_tag"

    const-string v3, "player_score_tag"

    invoke-static {v1, v2, v0, v3}, Lcum;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    :goto_0
    const-string v1, "total_scores"

    invoke-virtual {p0}, Ldnl;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0

    :cond_0
    const-string v1, "player_display_score"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "player_raw_score"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "player_display_rank"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "player_rank"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "player_score_tag"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ldjf;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "sorting_rank,name,external_leaderboard_id,timespan DESC,collection"

    move-object v0, p0

    move-object v3, v2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 20

    invoke-direct/range {p0 .. p6}, Lcva;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)J

    move-result-wide v17

    const-wide/16 v4, -0x1

    cmp-long v4, v17, v4

    if-nez v4, :cond_0

    const-string v4, "LeaderboardAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No instance found for leaderboard "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {p6 .. p6}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {p5 .. p5}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x4

    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcva;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    new-instance v4, Lcwe;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v4, v0, v1}, Lcwe;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcva;->h:Lcwa;

    invoke-virtual {v5, v4}, Lcwa;->a(Ljava/lang/Object;)V

    new-instance v19, Lcwd;

    move-object/from16 v0, v19

    move-wide/from16 v1, v17

    move/from16 v3, p8

    invoke-direct {v0, v1, v2, v3}, Lcwd;-><init>(JI)V

    if-eqz p9, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcva;->h:Lcwa;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcwa;->c(Ljava/lang/Object;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcva;->h:Lcwa;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v5

    invoke-interface {v5}, Lbpe;->a()J

    move-result-wide v5

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v5, v6}, Lcwa;->a(Ljava/lang/Object;J)Z

    move-result v4

    if-nez v4, :cond_4

    const/16 v16, 0x0

    invoke-static/range {p1 .. p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    const/4 v12, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v4 .. v13}, Lcva;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;

    move-result-object v9

    if-eqz v9, :cond_2

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, v17

    move/from16 v8, p8

    move-object v10, v14

    invoke-static/range {v4 .. v10}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILdnl;Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "LeaderboardAgent"

    invoke-static {v4, v14, v5}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_3
    move-object/from16 v0, p2

    move-wide/from16 v1, v17

    invoke-static {v0, v1, v2}, Ldjh;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v11

    sget-object v4, Lcva;->c:Ldhu;

    iget-object v12, v4, Ldhu;->a:[Ljava/lang/String;

    const-string v13, "page_type=?"

    const/4 v4, 0x1

    new-array v14, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p8 .. p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v14, v4

    const-string v15, "rank ASC"

    move-object/from16 v10, p1

    invoke-static/range {v10 .. v15}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v11

    if-nez v9, :cond_6

    :try_start_0
    invoke-virtual {v11}, Landroid/database/AbstractWindowedCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x4

    :goto_1
    move v7, v4

    :goto_2
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-wide/from16 v8, v17

    move/from16 v10, p8

    invoke-direct/range {v4 .. v11}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IJILandroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v11}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_4
    invoke-static {}, Ldfh;->a()Ldfi;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ldfi;->a(Ljava/lang/String;)Ldfi;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ldfi;->b(Ljava/lang/String;)Ldfi;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ldfi;->a(I)Ldfi;

    move-result-object v4

    move/from16 v0, p6

    invoke-virtual {v4, v0}, Ldfi;->b(I)Ldfi;

    move-result-object v4

    move/from16 v0, p8

    invoke-virtual {v4, v0}, Ldfi;->c(I)Ldfi;

    move-result-object v4

    invoke-virtual {v4}, Ldfi;->a()Ldfh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcva;->h:Lcwa;

    iget-object v4, v4, Ldfh;->a:Landroid/os/Bundle;

    move-object/from16 v0, v19

    move/from16 v1, p7

    invoke-virtual {v5, v0, v4, v1}, Lcwa;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x3

    goto :goto_1

    :catchall_0
    move-exception v4

    invoke-virtual {v11}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v4

    :cond_6
    move/from16 v7, v16

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct/range {p0 .. p9}, Lcva;->c(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct/range {p0 .. p9}, Lcva;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    const/4 v2, 0x0

    const/4 v4, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x1

    move v1, v0

    move-object v0, v4

    :goto_0
    if-nez v0, :cond_0

    if-eqz v1, :cond_3

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcva;->e:Ldnn;

    invoke-virtual {v1, p2, p3, v7, v0}, Ldnn;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldni;

    move-result-object v0

    invoke-virtual {v0}, Ldni;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ldni;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcva;->d:Ldnm;

    invoke-static {v7, v4, v0}, Ldnm;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Ldnm;->a:Lbmi;

    const-class v5, Ldnj;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldnj;

    invoke-virtual {v0}, Ldnj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ldnj;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_1

    :cond_3
    return-object v6
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IJILandroid/database/Cursor;)V
    .locals 9

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static/range {p7 .. p7}, Lcva;->a(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p2, p4, p5}, Ldjf;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcvc;->a:[Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    packed-switch p6, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown page type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :pswitch_0
    const/4 v0, 0x2

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    new-instance v1, Lcwd;

    invoke-direct {v1, p4, p5, p6}, Lcwd;-><init>(JI)V

    iget-object v0, p0, Lcva;->h:Lcwa;

    invoke-virtual {v0, v1}, Lcwa;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcva;->h:Lcwa;

    const/4 v6, -0x1

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v7

    move v3, p3

    invoke-virtual/range {v0 .. v8}, Lcwa;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    return-void

    :pswitch_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILdnl;I)V
    .locals 19

    invoke-virtual/range {p6 .. p6}, Ldnl;->getItems()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v6, v3

    :goto_0
    invoke-virtual/range {p6 .. p6}, Ldnl;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p6 .. p6}, Ldnl;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v3

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v10

    invoke-static/range {p6 .. p6}, Lcva;->a(Ldnl;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static/range {p2 .. p4}, Ldjf;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v5, v4, v3, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static/range {p2 .. p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v12

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_1

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldnh;

    iget-object v0, v3, Lbni;->a:Landroid/content/ContentValues;

    move-object/from16 v16, v0

    const-string v17, "instance_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v17, "page_type"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v3}, Ldnh;->getPlayer()Ldnz;

    move-result-object v3

    iget-object v3, v3, Lbni;->a:Landroid/content/ContentValues;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcum;->b(Landroid/content/Context;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v3, "last_updated"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    const-string v3, "profile_icon_image_url"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3, v15}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "profile_hi_res_image_url"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3, v15}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v6, :cond_2

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v6, :cond_3

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, Lbiq;->a(Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "LeaderboardAgent"

    invoke-static {v3, v15, v4}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    const/4 v3, 0x0

    move v9, v3

    :goto_4
    if-ge v9, v6, :cond_4

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    const-string v15, "profile_icon_image_url"

    const-string v16, "profile_icon_image_uri"

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v16

    invoke-static {v3, v15, v0, v12, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v15, "default_display_image_url"

    const-string v16, "default_display_image_uri"

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v16

    invoke-static {v3, v15, v0, v12, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v15, "profile_hi_res_image_url"

    const-string v16, "profile_hi_res_image_uri"

    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object/from16 v0, v16

    invoke-static {v3, v15, v0, v12, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    add-int/lit8 v3, v9, 0x1

    move v9, v3

    goto :goto_4

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :cond_4
    new-instance v4, Lcwd;

    move-wide/from16 v0, p3

    move/from16 v2, p5

    invoke-direct {v4, v0, v1, v2}, Lcwd;-><init>(JI)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcva;->h:Lcwa;

    const/4 v6, 0x0

    move/from16 v9, p7

    invoke-virtual/range {v3 .. v11}, Lcwa;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILdnl;Ljava/util/ArrayList;)V
    .locals 11

    invoke-virtual/range {p5 .. p5}, Ldnl;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_0
    invoke-virtual/range {p5 .. p5}, Ldnl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p5 .. p5}, Ldnl;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v5

    invoke-interface {v5}, Lbpe;->a()J

    move-result-wide v5

    invoke-static/range {p5 .. p5}, Lcva;->a(Ldnl;)Landroid/content/ContentValues;

    move-result-object v7

    if-nez p4, :cond_2

    const-string v3, "top_page_token_next"

    invoke-virtual {v7, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-static {p1, p2, p3}, Ldjf;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1, p2, p3}, Ldjh;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v7, "page_type=?"

    invoke-virtual {v1, v7, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v2, :cond_3

    invoke-static {p1}, Ldjh;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldnh;

    iget-object v8, v1, Lbni;->a:Landroid/content/ContentValues;

    const-string v9, "instance_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "page_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1}, Ldnh;->getPlayer()Ldnz;

    move-result-object v1

    const-string v9, "player_id"

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    invoke-static {p0, p1, v1, v5, v6}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0

    :cond_2
    const/4 v8, 0x1

    if-ne p4, v8, :cond_0

    const-string v8, "window_page_token_next"

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "window_page_token_prev"

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldng;JILjava/util/ArrayList;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p6}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p2, Lbni;->a:Landroid/content/ContentValues;

    const-string v2, "game_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-ltz p5, :cond_1

    const-string v2, "sorting_rank"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    const-string v2, "is_board_icon_default"

    const-string v3, "board_icon_image_url"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-static {p1}, Ldji;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Ldjf;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v0, v7, v6}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0, v7, v5}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0, v5, v6}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0, v5, v5}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0, v6, v6}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0, v6, v5}, Lcva;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v4, Lbkg;

    invoke-direct {v4, v2}, Lbkg;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0d0083    # com.google.android.gms.R.dimen.games_image_download_size_leaderboard

    invoke-virtual {v4, p0, v2}, Lbkg;->a(Landroid/content/Context;I)Lbkh;

    move-result-object v2

    invoke-virtual {v2}, Lbkh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 13

    invoke-static {p1}, Ldjg;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    new-instance v5, Lblt;

    invoke-direct {v5, v2}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v1, "external_game_id"

    invoke-virtual {v5, v1, p2}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "external_leaderboard_id"

    move-object/from16 v0, p3

    invoke-virtual {v5, v1, v0}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "external_player_id"

    move-object/from16 v0, p4

    invoke-virtual {v5, v1, v0}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v9, -0x1

    const-wide/16 v7, -0x1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcvd;->a:[Ljava/lang/String;

    invoke-virtual {v5}, Lblt;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v5, Lblt;->c:[Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v1, 0x2

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    move-wide v8, v6

    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const-wide/16 v6, 0x0

    cmp-long v1, v8, v6

    if-lez v1, :cond_1

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-wide/from16 v6, p5

    invoke-static/range {v1 .. v7}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JJ)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "raw_score"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "score_tag"

    move-object/from16 v0, p9

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v8, v9}, Ldjg;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "client_context_id"

    invoke-static {p0, p1}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "external_game_id"

    invoke-virtual {v1, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "external_leaderboard_id"

    move-object/from16 v0, p3

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "external_player_id"

    move-object/from16 v0, p4

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "raw_score"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "achieved_timestamp"

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "score_tag"

    move-object/from16 v0, p9

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    invoke-static {p1}, Ldrn;->b(Lcom/google/android/gms/common/server/ClientContext;)V

    goto :goto_1

    :cond_2
    move-wide v4, v7

    move-wide v11, v9

    move-wide v8, v11

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;JZ)V
    .locals 9

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-instance v6, Ljava/util/ArrayList;

    add-int/lit8 v0, v7, 0x1

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_3

    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldng;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ldng;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p1

    move-object v1, p2

    move-wide v3, p4

    invoke-static/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldng;JILjava/util/ArrayList;)V

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    if-eqz p6, :cond_4

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x1f4

    if-gt v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    const-string v1, "Attempting to preserve too many leaderboards!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    const-string v0, "external_leaderboard_id NOT IN "

    invoke-static {v0, v8}, Lblv;->a(Ljava/lang/String;Ljava/util/Collection;)Lblv;

    move-result-object v0

    invoke-static {p2, p4, p5}, Ldji;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lblv;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lblv;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "LeaderboardAgent"

    invoke-static {v0, v6, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcva;->h:Lcwa;

    new-instance v1, Lcwe;

    invoke-direct {v1, p1, p2}, Lcwe;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcwa;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcva;->h:Lcwa;

    iget-object v0, v0, Lcwf;->a:Ldl;

    invoke-virtual {v0}, Ldl;->b()V

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JJ)Z
    .locals 5

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ldji;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v4, Lcva;->b:[Ljava/lang/String;

    invoke-static {p0, v0, v4}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v4

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    if-ne v0, v1, :cond_0

    move v0, v2

    :goto_1
    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown score order "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    cmp-long v0, p5, p3

    if-gez v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_1

    :pswitch_1
    cmp-long v0, p5, p3

    if-lez v0, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)J
    .locals 4

    const-wide/16 v0, -0x1

    invoke-static {p2, p4}, Ldji;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p1, v2}, Lcum;->b(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p2, p4}, Ldjf;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lblt;

    invoke-direct {v1, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v2, "timespan"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "collection"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lblt;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lblt;->c:[Ljava/lang/String;

    invoke-static {p1, v0, v2, v1}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;
    .locals 11

    const/4 v9, 0x0

    const/4 v1, 0x1

    move/from16 v0, p7

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v10, p0, Lcva;->f:Ldpl;

    invoke-static/range {p5 .. p5}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v7, 0x0

    move-object v1, p3

    move-object/from16 v4, p9

    move-object/from16 v6, p8

    invoke-static/range {v1 .. v8}, Ldpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v10, Ldpl;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldnl;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldnl;

    :goto_0
    return-object v1

    :cond_0
    iget-object v7, p0, Lcva;->f:Ldpl;

    invoke-static/range {p5 .. p5}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p3

    move-object/from16 v4, p9

    move-object/from16 v6, p8

    invoke-static/range {v1 .. v6}, Ldpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v7, Ldpl;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldnl;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldnl;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to retrieve leaderboard scores for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "LeaderboardAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    move-object v1, v9

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z
    .locals 13

    const/4 v6, 0x1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {p2}, Ldjg;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    if-nez p3, :cond_1

    sget-object v2, Lcvd;->a:[Ljava/lang/String;

    const-string v3, "account_name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    move-object v7, v0

    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1, v0, v10}, Lcum;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    iget-object v0, p0, Lcva;->f:Ldpl;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, Ldpl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Ldoi;

    const/4 v0, 0x1

    invoke-direct {p0, v1, v2}, Lcva;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v6

    :goto_1
    if-eqz v0, :cond_0

    :try_start_2
    invoke-static {v1, v11, v12}, Ldjg;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcum;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    move v6, v2

    goto :goto_0

    :cond_1
    sget-object v2, Lcvd;->a:[Ljava/lang/String;

    const-string v3, "account_name=? AND external_leaderboard_id=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v0, 0x1

    aput-object p3, v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    :try_start_3
    invoke-static {}, Ldac;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "LeaderboardAgent"

    invoke-static {v0, v3}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_2
    invoke-static {v0}, Lbng;->a(Lsp;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "LeaderboardAgent"

    const-string v3, "Could not submit score, will try again later"

    invoke-static {v0, v3}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "LeaderboardAgent"

    invoke-static {v0, v8, v1}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    return v6

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private c(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;
    .locals 12

    const/4 v10, 0x0

    const/4 v1, 0x1

    move/from16 v0, p7

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v11, p0, Lcva;->g:Ldpm;

    invoke-static/range {p5 .. p5}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p4 .. p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p3

    move-object/from16 v4, p9

    move-object/from16 v6, p8

    invoke-static/range {v1 .. v9}, Ldpm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v11, Ldpm;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldnl;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldnl;

    :goto_0
    return-object v1

    :cond_0
    iget-object v7, p0, Lcva;->g:Ldpm;

    invoke-static/range {p5 .. p5}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p4 .. p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p3

    move-object/from16 v4, p9

    move-object/from16 v6, p8

    invoke-static/range {v1 .. v6}, Ldpm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v7, Ldpm;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldnl;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldnl;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to retrieve leaderboard scores for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p4 .. p4}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "LeaderboardAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    move-object v1, v10

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v8

    :try_start_0
    iget-object v10, p0, Lcva;->f:Ldpl;

    invoke-static/range {p5 .. p5}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p6 .. p6}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p3

    move-object v2, p4

    invoke-static/range {v1 .. v7}, Ldpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v10, Ldpl;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Ldoe;

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldoe;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ldoe;->getPlayer()Ldnz;

    move-result-object v3

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ldoe;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v1

    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error getting scores for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LeaderboardAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_0
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v4, 0x2

    if-ge v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v4, "Found multiple entries for player (%s), in leaderboard (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    const/4 v6, 0x1

    aput-object p4, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-virtual {v1}, Ldoe;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldod;

    packed-switch p6, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_0
    invoke-virtual {v1}, Ldod;->getPublicRank()Ldnk;

    move-result-object v2

    :goto_2
    iget-object v1, v1, Lbni;->a:Landroid/content/ContentValues;

    iget-object v4, v3, Lbni;->a:Landroid/content/ContentValues;

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    if-eqz v2, :cond_5

    const-string v4, "rank"

    invoke-virtual {v2}, Ldnk;->c()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "display_rank"

    invoke-virtual {v2}, Ldnk;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    const-string v2, "last_updated"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, v3, Lbni;->a:Landroid/content/ContentValues;

    invoke-static {p1, v2}, Lcum;->b(Landroid/content/Context;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-static {p1, v1}, Lcum;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    invoke-static {p2}, Ldjd;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    const-string v4, "profile_icon_image_url"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v3}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "profile_hi_res_image_url"

    invoke-virtual {v1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v3}, Lcum;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "LeaderboardAgent"

    invoke-static {v5, v3, v6}, Lcum;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v4, :cond_3

    const-string v5, "profile_icon_image_url"

    const-string v6, "profile_icon_image_uri"

    invoke-static {v1, v5, v6, v3, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    const-string v5, "default_display_image_url"

    const-string v6, "default_display_image_uri"

    invoke-static {v1, v5, v6, v3, v4}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    :cond_3
    if-eqz v2, :cond_4

    const-string v4, "profile_hi_res_image_url"

    const-string v5, "profile_hi_res_image_uri"

    invoke-static {v1, v4, v5, v3, v2}, Lcum;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    :cond_4
    sget-object v2, Lcva;->c:Ldhu;

    iget-object v2, v2, Ldhu;->a:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {v1}, Ldod;->getSocialRank()Ldnk;

    move-result-object v2

    goto/16 :goto_2

    :cond_5
    const-string v2, "rank"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "display_rank"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b021d    # com.google.android.gms.R.string.games_leaderboard_rank_unknown

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    invoke-direct/range {p0 .. p6}, Lcva;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II)J

    move-result-wide v11

    const-wide/16 v1, -0x1

    cmp-long v1, v11, v1

    if-nez v1, :cond_0

    const-string v1, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No instance found for leaderboard "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p6 .. p6}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p5 .. p5}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v2

    new-instance v13, Lcwd;

    move/from16 v0, p8

    invoke-direct {v13, v11, v12, v0}, Lcwd;-><init>(JI)V

    iget-object v1, p0, Lcva;->h:Lcwa;

    invoke-virtual {v1, v13, v2, v3}, Lcwa;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcva;->h:Lcwa;

    packed-switch p9, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown page direction "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p9

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual {v4, v13, v2, v3}, Lcwf;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v9

    :goto_1
    if-eqz v9, :cond_2

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v1 .. v10}, Lcva;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Ldnl;

    move-result-object v7

    if-eqz v7, :cond_4

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-wide v4, v11

    move/from16 v6, p8

    move/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILdnl;I)V

    :cond_2
    :goto_2
    invoke-static {}, Ldfh;->a()Ldfi;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ldfi;->a(Ljava/lang/String;)Ldfi;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ldfi;->b(Ljava/lang/String;)Ldfi;

    move-result-object v1

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Ldfi;->a(I)Ldfi;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ldfi;->b(I)Ldfi;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Ldfi;->c(I)Ldfi;

    move-result-object v1

    invoke-virtual {v1}, Ldfi;->a()Ldfh;

    move-result-object v1

    iget-object v2, p0, Lcva;->h:Lcwa;

    iget-object v1, v1, Ldfh;->a:Landroid/os/Bundle;

    const/4 v3, -0x1

    invoke-virtual {v2, v13, v1, v3}, Lcwf;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_1
    iget-object v1, v4, Lcwf;->a:Ldl;

    invoke-virtual {v1, v13}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    if-eqz v1, :cond_3

    invoke-virtual {v4, v13, v2, v3}, Lcwf;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, v1, Lcwg;->a:Lbhk;

    iget-object v9, v1, Lbhk;->a:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcva;->h:Lcwa;

    invoke-virtual {v1, v13}, Lcwa;->d(Ljava/lang/Object;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    const/4 v6, 0x0

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gez v0, :cond_0

    const-string v0, "LeaderboardAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Game information not present for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    if-nez p5, :cond_1

    :try_start_0
    iget-object v0, p0, Lcva;->d:Ldnm;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, p4, v2}, Ldnm;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Ldng;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move v7, v6

    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;JZ)V

    invoke-static {p1, p2, p4, v7}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve leaderboard "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    move v7, v0

    move-object v0, v1

    goto :goto_1

    :cond_1
    move v7, v6

    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    const-string v0, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Game information not present for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-nez p4, :cond_1

    invoke-static {p2, p3}, Ldji;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcva;->i:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v2, v8

    const-wide/32 v8, 0x36ee80

    cmp-long v0, v2, v8

    if-gtz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p2, p3}, Ldji;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcva;->i:Ljava/util/HashMap;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v2

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;JZ)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move v5, v7

    :goto_2
    invoke-static {p2, p3}, Ldjf;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "sorting_rank,name,external_leaderboard_id,timespan DESC,collection"

    move-object v0, p1

    move-object v2, v10

    move-object v3, v10

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "LeaderboardAgent"

    const-string v2, "Unable to retrieve leaderboard list"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v5, 0x3

    goto :goto_2

    :cond_3
    move v5, v7

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Ldfr;
    .locals 14

    if-nez p11, :cond_0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-wide/from16 v7, p6

    move-wide/from16 v9, p8

    move-object/from16 v11, p10

    invoke-static/range {v2 .. v11}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    new-instance v2, Ldfr;

    const/4 v3, 0x5

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v2, v3, v0, v1}, Ldfr;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcva;->f:Ldpl;

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v3, p2

    move-object/from16 v4, p5

    move-object/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Ldpl;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Ldoi;

    move-result-object v9

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-direct {p0, v0, v1}, Lcva;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v9}, Ldoi;->getUnbeatenScores()Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v8, v2

    :goto_1
    if-ge v8, v12, :cond_3

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Ldoh;

    invoke-virtual {v6}, Ldoh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ldef;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    new-instance v2, Ldfs;

    invoke-virtual {v6}, Ldoh;->c()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v6}, Ldoh;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Ldoh;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Ldfs;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v13, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v3, v2

    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "LeaderboardAgent"

    invoke-static {v3, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/4 v2, 0x6

    invoke-static {v3}, Lbng;->a(Lsp;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "LeaderboardAgent"

    const-string v3, "Could not submit score. Deferring for later"

    invoke-static {v2, v3}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v12, 0x5

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-wide/from16 v7, p6

    move-wide/from16 v9, p8

    move-object/from16 v11, p10

    invoke-static/range {v2 .. v11}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    move v2, v12

    :goto_2
    new-instance v3, Ldfr;

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v3, v2, v0, v1}, Ldfr;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_2
    const-string v3, "LeaderboardAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Encountered a hard error while submitting score for leaderboard "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and player "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v9}, Ldoi;->b()Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v8, v2

    :goto_3
    if-ge v8, v12, :cond_4

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ldef;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    new-instance v2, Ldfs;

    invoke-virtual {v9}, Ldoi;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    move-wide/from16 v3, p6

    move-object/from16 v6, p10

    invoke-direct/range {v2 .. v7}, Ldfs;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v13, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_3

    :cond_4
    new-instance v2, Ldfr;

    const/4 v3, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v2, v3, v0, v1, v10}, Ldfr;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcva;->h:Lcwa;

    iget-object v0, v0, Lcwf;->a:Ldl;

    invoke-virtual {v0}, Ldl;->b()V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;)V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcva;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 9

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-static {p1, p2, p3}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v1, 0x0

    cmp-long v1, v4, v1

    if-gez v1, :cond_0

    const-string v1, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find game info while syncing for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v0

    :goto_0
    return v6

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    move v2, v6

    :goto_1
    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcva;->e:Ldnn;

    invoke-virtual {v2, p2, p3, v7, v1}, Ldnn;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldni;

    move-result-object v2

    invoke-virtual {v2}, Ldni;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ldni;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Ldni;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v2, v0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v1, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to sync leaderboards for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    move v6, v0

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;JZ)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcva;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IIIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
