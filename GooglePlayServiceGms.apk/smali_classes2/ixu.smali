.class public final Lixu;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/io/InputStream;

.field protected b:Lixr;

.field protected c:I

.field protected d:Ljava/lang/Object;

.field protected volatile e:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lixu;->d:Ljava/lang/Object;

    iput-object p1, p0, Lixu;->a:Ljava/io/InputStream;

    iput p2, p0, Lixu;->c:I

    return-void
.end method

.method private a([BII)I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lixu;->c:I

    if-lez v1, :cond_2

    :try_start_0
    iget-object v0, p0, Lixu;->a:Ljava/io/InputStream;

    iget v1, p0, Lixu;->c:I

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    if-lez v0, :cond_0

    iget v1, p0, Lixu;->c:I

    sub-int/2addr v1, v0

    iput v1, p0, Lixu;->c:I

    :cond_0
    if-lez v0, :cond_1

    iget v1, p0, Lixu;->c:I

    if-nez v1, :cond_2

    :cond_1
    invoke-direct {p0}, Lixu;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lixu;->c()V

    throw v0
.end method

.method private c()V
    .locals 2

    iget-object v1, p0, Lixu;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lixu;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lixu;->e:Z

    iget-object v0, p0, Lixu;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lixu;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private d()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lixu;->c:I

    if-lez v1, :cond_1

    :try_start_0
    iget-object v0, p0, Lixu;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ltz v0, :cond_0

    iget v1, p0, Lixu;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lixu;->c:I

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lixu;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lixu;->c()V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v1, p0, Lixu;->d:Ljava/lang/Object;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lixu;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lixu;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 9

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lixu;->c:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixu;->b:Lixr;

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lixr;

    const/high16 v1, 0x10000

    iget v2, p0, Lixu;->c:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v1}, Lixr;-><init>(I)V

    iput-object v0, p0, Lixu;->b:Lixr;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x400

    new-array v4, v0, [B

    :goto_1
    :try_start_1
    iget v0, p0, Lixu;->c:I

    if-lez v0, :cond_9

    const/4 v0, 0x0

    array-length v1, v4

    invoke-direct {p0, v4, v0, v1}, Lixu;->a([BII)I

    move-result v1

    if-lez v1, :cond_8

    iget-object v5, p0, Lixu;->b:Lixr;

    iget-object v6, v5, Lixr;->a:Ljava/lang/Object;

    monitor-enter v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move v2, v3

    :cond_2
    :goto_2
    if-lez v1, :cond_7

    :goto_3
    :try_start_2
    iget v0, v5, Lixr;->c:I

    iget v7, v5, Lixr;->d:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-ne v0, v7, :cond_3

    :try_start_3
    iget-object v0, v5, Lixr;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    iget-object v0, v5, Lixr;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_4
    iget v0, v5, Lixr;->c:I

    if-gez v0, :cond_4

    const/4 v0, 0x0

    iput v0, v5, Lixr;->c:I

    :cond_4
    iget v0, v5, Lixr;->d:I

    iget v7, v5, Lixr;->c:I

    if-ge v0, v7, :cond_6

    iget v0, v5, Lixr;->c:I

    iget v7, v5, Lixr;->d:I

    sub-int/2addr v0, v7

    :goto_4
    if-le v0, v1, :cond_5

    move v0, v1

    :cond_5
    iget-object v7, v5, Lixr;->b:[B

    iget v8, v5, Lixr;->d:I

    invoke-static {v4, v2, v7, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v2, v0

    sub-int/2addr v1, v0

    iget v7, v5, Lixr;->d:I

    add-int/2addr v0, v7

    iput v0, v5, Lixr;->d:I

    iget v0, v5, Lixr;->d:I

    iget-object v7, v5, Lixr;->b:[B

    array-length v7, v7

    if-ne v0, v7, :cond_2

    const/4 v0, 0x0

    iput v0, v5, Lixr;->d:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v6

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_1
    move-exception v0

    iget-object v0, p0, Lixu;->b:Lixr;

    invoke-virtual {v0}, Lixr;->b()V

    goto :goto_0

    :cond_6
    :try_start_6
    iget-object v0, v5, Lixr;->b:[B

    array-length v0, v0

    iget v7, v5, Lixr;->d:I

    sub-int/2addr v0, v7

    goto :goto_4

    :cond_7
    iget-object v0, v5, Lixr;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :cond_8
    :try_start_7
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Premature EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v0

    iget-object v1, p0, Lixu;->b:Lixr;

    invoke-virtual {v1}, Lixr;->b()V

    throw v0

    :cond_9
    iget-object v0, p0, Lixu;->b:Lixr;

    invoke-virtual {v0}, Lixr;->b()V

    goto/16 :goto_0
.end method

.method public final declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    :cond_0
    invoke-virtual {p0}, Lixu;->read()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gez v0, :cond_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized read()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lixu;->b:Lixr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixu;->b:Lixr;

    invoke-virtual {v0}, Lixr;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lixu;->d()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final read([B)I
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lixu;->read([BII)I

    move-result v0

    return v0
.end method

.method public final declared-synchronized read([BII)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lixu;->b:Lixr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixu;->b:Lixr;

    invoke-virtual {v0, p1, p2, p3}, Lixr;->a([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lixu;->a([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
