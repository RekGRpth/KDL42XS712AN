.class public final Licc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lhsz;

.field private final b:Libw;

.field private final c:Libw;

.field private final d:Libn;

.field private final e:Lhou;

.field private final f:Lhod;

.field private final g:Lice;

.field private final h:Licf;

.field private final i:Licm;


# direct methods
.method private constructor <init>(Liby;Libd;Lhod;Lhou;Libn;Lice;Licf;Licm;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lhsz;->a()Lhsz;

    move-result-object v0

    iput-object v0, p0, Licc;->a:Lhsz;

    iput-object p1, p0, Licc;->b:Libw;

    iput-object p2, p0, Licc;->c:Libw;

    iput-object p3, p0, Licc;->f:Lhod;

    iput-object p5, p0, Licc;->d:Libn;

    iput-object p4, p0, Licc;->e:Lhou;

    iput-object p6, p0, Licc;->g:Lice;

    iput-object p7, p0, Licc;->h:Licf;

    iput-object p8, p0, Licc;->i:Licm;

    return-void
.end method

.method private static a(Ljava/util/Map;)Lhus;
    .locals 5

    const/4 v1, -0x1

    sget-object v0, Lhus;->a:Lhus;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    iget-object v0, v0, Lhuq;->b:Lhus;

    invoke-virtual {v0}, Lhus;->ordinal()I

    move-result v4

    if-le v4, v2, :cond_2

    invoke-static {}, Lhus;->a()Lhus;

    move-result-object v1

    if-ne v0, v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lhus;->ordinal()I

    move-result v1

    :goto_2
    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private static a(Lhuu;Lhuu;)Lhuu;
    .locals 2

    if-eqz p0, :cond_0

    iget-object v0, p0, Lhuu;->c:Lhug;

    if-nez v0, :cond_2

    :cond_0
    move-object p0, p1

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    if-eqz p1, :cond_1

    iget-object v0, p1, Lhuu;->c:Lhug;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhuu;->c:Lhug;

    iget v0, v0, Lhug;->f:I

    iget-object v1, p1, Lhuu;->c:Lhug;

    iget v1, v1, Lhug;->f:I

    if-le v0, v1, :cond_1

    move-object p0, p1

    goto :goto_0
.end method

.method private a(Lhuv;)Lhuv;
    .locals 5

    const/4 v1, 0x0

    if-nez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const/4 v2, 0x1

    move v0, v1

    :goto_1
    iget-object v3, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_5

    iget-object v3, p0, Licc;->a:Lhsz;

    invoke-virtual {p1, v0}, Lhuv;->a(I)Lhut;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhsz;->a(Lhut;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_3
    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p1, v1}, Lhuv;->a(I)Lhut;

    move-result-object v0

    iget-object v3, p0, Licc;->a:Lhsz;

    invoke-virtual {v3, v0}, Lhsz;->a(Lhut;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    new-instance v0, Lhuv;

    iget-wide v3, p1, Lhuv;->a:J

    invoke-direct {v0, v3, v4, v2}, Lhuv;-><init>(JLjava/util/ArrayList;)V

    move-object p1, v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public static a(Libt;Lhou;Lhod;Lidu;)Licc;
    .locals 9

    new-instance v1, Liby;

    invoke-direct {v1}, Liby;-><init>()V

    :try_start_0
    invoke-interface {p3}, Lidu;->l()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Libm;->a(Ljava/io/InputStream;)Libm;

    move-result-object v0

    invoke-virtual {v0}, Libm;->a()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    new-instance v2, Libd;

    invoke-direct {v2, v0}, Libd;-><init>(Ljava/util/List;)V

    new-instance v5, Libn;

    invoke-direct {v5, p0}, Libn;-><init>(Libp;)V

    invoke-static {p3}, Lice;->a(Lidu;)Lice;

    move-result-object v6

    new-instance v7, Licf;

    invoke-interface {p3}, Lidu;->j()Licm;

    move-result-object v0

    invoke-direct {v7, v0}, Licf;-><init>(Licm;)V

    new-instance v0, Licc;

    invoke-interface {p3}, Lidu;->j()Licm;

    move-result-object v8

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v8}, Licc;-><init>(Liby;Libd;Lhod;Lhou;Libn;Lice;Licf;Licm;)V

    return-object v0

    :catch_0
    move-exception v0

    sget-boolean v2, Licj;->e:Z

    if-eqz v2, :cond_0

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not load metric model: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private a(Ljava/util/Set;Lhug;)Licd;
    .locals 20

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Licc;->i:Licm;

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v14

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v10, v1

    :cond_0
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v1, v0, Licc;->e:Lhou;

    invoke-virtual {v1, v9, v14, v15}, Lhou;->a(Ljava/lang/Object;J)Lhno;

    move-result-object v17

    if-eqz v17, :cond_3

    move-object/from16 v0, v17

    iget-object v1, v0, Lhno;->d:Ljava/lang/Object;

    move-object v3, v1

    check-cast v3, Lhuq;

    invoke-virtual {v3}, Lhuq;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v3, Lhuq;->b:Lhus;

    sget-object v2, Lhus;->a:Lhus;

    if-eq v1, v2, :cond_2

    const/4 v11, 0x1

    if-eqz p2, :cond_f

    move-object/from16 v0, p2

    iget v1, v0, Lhug;->f:I

    int-to-double v1, v1

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v18, v1, v4

    iget v1, v3, Lhuq;->d:I

    int-to-double v1, v1

    iget v3, v3, Lhuq;->e:I

    int-to-double v3, v3

    move-object/from16 v0, p2

    iget v5, v0, Lhug;->d:I

    int-to-double v5, v5

    move-object/from16 v0, p2

    iget v7, v0, Lhug;->e:I

    int-to-double v7, v7

    invoke-static/range {v1 .. v8}, Lhtn;->a(DDDD)D

    move-result-wide v1

    cmpl-double v1, v1, v18

    if-lez v1, :cond_f

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_1

    move-object/from16 v0, v17

    iget-object v1, v0, Lhno;->d:Ljava/lang/Object;

    invoke-interface {v12, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object/from16 v0, v17

    iget-object v1, v0, Lhno;->d:Ljava/lang/Object;

    invoke-interface {v13, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lhuq;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lhuq;->d()V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_0

    :cond_4
    if-nez p2, :cond_6

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Licc;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_5

    const-string v1, "WifiLocator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AP set of size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has no overlap"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    new-instance v1, Licd;

    sget-object v2, Lhty;->b:Lhty;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    :goto_2
    return-object v1

    :cond_6
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_a

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_a

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Licc;->a(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_7

    const-string v1, "WifiLocator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Diversity, rehabilitating ap list of size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_8

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Licc;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Licd;

    sget-object v2, Lhty;->b:Lhty;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_2

    :cond_8
    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    :cond_9
    :goto_3
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v8

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v1, v0, Licc;->e:Lhou;

    invoke-virtual {v1, v3, v14, v15}, Lhou;->a(Ljava/lang/Object;J)Lhno;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Licc;->e:Lhou;

    const/4 v2, 0x0

    iget v4, v4, Lhno;->c:I

    invoke-static {}, Lhuq;->c()Lhuq;

    move-result-object v5

    move-wide v6, v14

    invoke-virtual/range {v1 .. v7}, Lhou;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    goto :goto_4

    :cond_a
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_9

    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    goto :goto_3

    :cond_b
    if-lez v8, :cond_d

    const/4 v1, 0x5

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v8, v1, :cond_c

    const-string v1, "Good cache hits. Computing WiFi location locally"

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v8, v10, v2}, Licc;->a(Ljava/lang/String;III)V

    new-instance v1, Licd;

    sget-object v2, Lhty;->a:Lhty;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v12, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_c
    const-string v1, "Not enough positive cache hits compared to misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v8, v10, v2}, Licc;->a(Ljava/lang/String;III)V

    new-instance v1, Licd;

    sget-object v2, Lhty;->c:Lhty;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_d
    if-lez v10, :cond_e

    const-string v1, "Too many cache  misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v8, v10, v2}, Licc;->a(Ljava/lang/String;III)V

    new-instance v1, Licd;

    sget-object v2, Lhty;->c:Lhty;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_e
    const-string v1, "Too many no-location APs. Will not compute a location nor go to the server."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v8, v10, v2}, Licc;->a(Ljava/lang/String;III)V

    new-instance v1, Licd;

    sget-object v2, Lhty;->b:Lhty;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Licd;-><init>(Licc;Lhty;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_f
    move v1, v11

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;III)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " hasLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int v1, p1, p2

    sub-int v1, p3, v1

    const-string v2, " noLocation="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " cacheMiss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "WifiLocator"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/util/Collection;)Z
    .locals 12

    const/4 v9, 0x0

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    move v0, v9

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lhuq;

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhuq;

    if-eq v8, v6, :cond_2

    iget v0, v8, Lhug;->d:I

    int-to-double v0, v0

    iget v2, v8, Lhug;->e:I

    int-to-double v2, v2

    iget v4, v6, Lhug;->d:I

    int-to-double v4, v4

    iget v6, v6, Lhug;->e:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lhtn;->a(DDDD)D

    move-result-wide v0

    const-wide v2, 0x40b3880000000000L    # 5000.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    move v0, v9

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;)Z
    .locals 10

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v0, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v0

    move v4, v5

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v8, 0xfcffff000000L

    and-long/2addr v0, v8

    if-eqz v4, :cond_1

    move-wide v2, v0

    move v4, v6

    goto :goto_0

    :cond_1
    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    move v0, v5

    :goto_1
    if-eqz v0, :cond_0

    :goto_2
    return v5

    :cond_2
    move v0, v6

    goto :goto_1

    :cond_3
    move v5, v6

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/List;Lhug;)Lhuu;
    .locals 19

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->a:Lhsz;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhuv;

    if-eqz v2, :cond_0

    iget-object v3, v2, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_0

    invoke-virtual {v2, v4}, Lhuv;->a(I)Lhut;

    move-result-object v9

    invoke-interface {v5, v9}, Lhta;->a(Lhut;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v10, v9, Lhut;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-wide v10, v9, Lhut;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v6, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget v9, v9, Lhut;->d:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_3
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    div-int/lit8 v6, v4, 0x2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_4

    add-int/lit8 v4, v6, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    :goto_2
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v9, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_2

    :cond_5
    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Licc;->a(Ljava/util/Set;Lhug;)Licd;

    move-result-object v2

    const/4 v15, 0x0

    const/high16 v16, -0x80000000

    iget-object v8, v2, Licd;->b:Ljava/util/Map;

    iget-object v10, v2, Licd;->a:Lhty;

    iget-object v2, v2, Licd;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-interface {v9, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhuv;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Licc;->a(Lhuv;)Lhuv;

    move-result-object v7

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Licc;->g:Lice;

    invoke-virtual {v2, v9}, Lice;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->h:Licf;

    if-nez v4, :cond_9

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Licc;->d:Libn;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Libn;->a(Ljava/util/Map;Ljava/util/Map;)Libx;

    move-result-object v11

    if-eqz v11, :cond_1a

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v4, 0x2

    if-lt v2, v4, :cond_1a

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_7

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Indoor localizer returned: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lhuu;

    invoke-virtual {v11}, Libx;->a()Lhug;

    move-result-object v3

    sget-object v4, Lhty;->a:Lhty;

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->i:Licm;

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    invoke-direct/range {v2 .. v8}, Lhuu;-><init>(Lhug;Lhty;JLhuv;Ljava/util/Map;)V

    invoke-virtual {v11}, Libx;->a()Lhug;

    move-result-object v3

    check-cast v3, Lhtl;

    if-eqz v3, :cond_19

    iget-object v15, v3, Lhtl;->b:Ljava/lang/String;

    if-eqz v15, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Licc;->f:Lhod;

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->i:Licm;

    invoke-interface {v5}, Licm;->b()J

    move-result-wide v5

    invoke-interface {v4, v15, v5, v6}, Lhod;->a(Ljava/lang/String;J)I

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_8

    const-string v4, "WifiLocator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Incremented executions for level "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget v4, v3, Lhtl;->f:I

    const/16 v5, 0x7530

    if-ge v4, v5, :cond_c

    :goto_5
    return-object v2

    :cond_9
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v6, v5, Licf;->a:Licm;

    invoke-interface {v6}, Licm;->a()J

    move-result-wide v11

    iget-wide v13, v5, Licf;->b:J

    sub-long v13, v11, v13

    const-wide/16 v17, 0x1f40

    cmp-long v6, v13, v17

    if-gez v6, :cond_a

    iget-object v6, v5, Licf;->c:Ljava/util/Map;

    if-eqz v6, :cond_a

    iget-object v6, v5, Licf;->c:Ljava/util/Map;

    invoke-interface {v2, v6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_a
    invoke-interface {v2, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    sget-boolean v6, Licj;->b:Z

    if-eqz v6, :cond_b

    const-string v6, "WifiSignalAugmenter"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Orig AP count: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " Augmented AP count: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v6, v13}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iput-wide v11, v5, Licf;->b:J

    iput-object v4, v5, Licf;->c:Ljava/util/Map;

    goto/16 :goto_4

    :cond_c
    iget v0, v3, Lhtl;->c:I

    move/from16 v16, v0

    move-object/from16 v17, v2

    :goto_6
    sget-object v2, Lhty;->a:Lhty;

    if-eq v10, v2, :cond_d

    new-instance v2, Lhuu;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Licc;->i:Licm;

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v5

    move-object v4, v10

    invoke-direct/range {v2 .. v8}, Lhuu;-><init>(Lhug;Lhty;JLhuv;Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Licc;->a(Lhuu;Lhuu;)Lhuu;

    move-result-object v2

    goto :goto_5

    :cond_d
    invoke-static {v8}, Licc;->a(Ljava/util/Map;)Lhus;

    move-result-object v2

    sget-object v3, Lhus;->a:Lhus;

    if-ne v2, v3, :cond_11

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_e

    const-string v2, "WifiLocator"

    const-string v3, "No APs found with known confidence values. Not computing a location"

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    const/4 v2, 0x0

    :goto_7
    if-eqz v2, :cond_f

    invoke-virtual {v2}, Libx;->a()Lhug;

    move-result-object v3

    if-nez v3, :cond_15

    :cond_f
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_10

    const-string v2, "WifiLocator"

    const-string v3, "Locator did not find a location"

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    new-instance v2, Lhuu;

    const/4 v3, 0x0

    sget-object v4, Lhty;->b:Lhty;

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->i:Licm;

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    invoke-direct/range {v2 .. v8}, Lhuu;-><init>(Lhug;Lhty;JLhuv;Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Licc;->a(Lhuu;Lhuu;)Lhuu;

    move-result-object v2

    goto/16 :goto_5

    :cond_11
    sget-object v3, Lhus;->b:Lhus;

    if-ne v2, v3, :cond_13

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_12

    const-string v2, "WifiLocator"

    const-string v3, "Computing location using circle intersection."

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Licc;->b:Libw;

    invoke-interface {v2, v8, v9}, Libw;->a(Ljava/util/Map;Ljava/util/Map;)Libx;

    move-result-object v2

    goto :goto_7

    :cond_13
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_14

    const-string v2, "WifiLocator"

    const-string v3, "Computing location using MaxLre."

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Licc;->c:Libw;

    invoke-interface {v2, v8, v9}, Libw;->a(Ljava/util/Map;Ljava/util/Map;)Libx;

    move-result-object v2

    goto :goto_7

    :cond_15
    invoke-virtual {v2}, Libx;->a()Lhug;

    move-result-object v3

    invoke-static {v3}, Liba;->c(Lhug;)Z

    move-result v3

    if-nez v3, :cond_17

    sget-boolean v3, Licj;->d:Z

    if-eqz v3, :cond_16

    const-string v3, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Locator found a location that did not have sane values: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    new-instance v2, Lhuu;

    const/4 v3, 0x0

    sget-object v4, Lhty;->b:Lhty;

    move-object/from16 v0, p0

    iget-object v5, v0, Licc;->i:Licm;

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    invoke-direct/range {v2 .. v8}, Lhuu;-><init>(Lhug;Lhty;JLhuv;Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Licc;->a(Lhuu;Lhuu;)Lhuu;

    move-result-object v2

    goto/16 :goto_5

    :cond_17
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_18

    const-string v3, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Finished computing WiFi location: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    invoke-virtual {v2}, Libx;->a()Lhug;

    move-result-object v3

    new-instance v9, Lhtl;

    iget v10, v3, Lhug;->d:I

    iget v11, v3, Lhug;->e:I

    iget v12, v3, Lhug;->f:I

    invoke-virtual {v2}, Libx;->b()I

    move-result v13

    const/4 v14, 0x0

    invoke-direct/range {v9 .. v16}, Lhtl;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V

    new-instance v2, Lhuu;

    sget-object v4, Lhty;->a:Lhty;

    move-object/from16 v0, p0

    iget-object v3, v0, Licc;->i:Licm;

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v5

    move-object v3, v9

    invoke-direct/range {v2 .. v8}, Lhuu;-><init>(Lhug;Lhty;JLhuv;Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Licc;->a(Lhuu;Lhuu;)Lhuu;

    move-result-object v2

    goto/16 :goto_5

    :cond_19
    move-object/from16 v17, v2

    goto/16 :goto_6

    :cond_1a
    move-object/from16 v17, v3

    goto/16 :goto_6
.end method
