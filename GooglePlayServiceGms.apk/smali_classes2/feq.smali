.class public final Lfeq;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lfex;

.field public b:Lfes;

.field public c:Lfet;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lfeq;->a:Lfex;

    iput-object v0, p0, Lfeq;->b:Lfes;

    iput-object v0, p0, Lfeq;->c:Lfet;

    const/4 v0, -0x1

    iput v0, p0, Lfeq;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lfeq;->a:Lfex;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lfeq;->a:Lfex;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lfeq;->b:Lfes;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lfeq;->b:Lfes;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lfeq;->c:Lfet;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lfeq;->c:Lfet;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lfeq;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lfeq;->a:Lfex;

    if-nez v0, :cond_1

    new-instance v0, Lfex;

    invoke-direct {v0}, Lfex;-><init>()V

    iput-object v0, p0, Lfeq;->a:Lfex;

    :cond_1
    iget-object v0, p0, Lfeq;->a:Lfex;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lfeq;->b:Lfes;

    if-nez v0, :cond_2

    new-instance v0, Lfes;

    invoke-direct {v0}, Lfes;-><init>()V

    iput-object v0, p0, Lfeq;->b:Lfes;

    :cond_2
    iget-object v0, p0, Lfeq;->b:Lfes;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lfeq;->c:Lfet;

    if-nez v0, :cond_3

    new-instance v0, Lfet;

    invoke-direct {v0}, Lfet;-><init>()V

    iput-object v0, p0, Lfeq;->c:Lfet;

    :cond_3
    iget-object v0, p0, Lfeq;->c:Lfet;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Lfeq;->a:Lfex;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lfeq;->a:Lfex;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Lfeq;->b:Lfes;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lfeq;->b:Lfes;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Lfeq;->c:Lfet;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lfeq;->c:Lfet;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfeq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfeq;

    iget-object v2, p0, Lfeq;->a:Lfex;

    if-nez v2, :cond_3

    iget-object v2, p1, Lfeq;->a:Lfex;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfeq;->a:Lfex;

    iget-object v3, p1, Lfeq;->a:Lfex;

    invoke-virtual {v2, v3}, Lfex;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lfeq;->b:Lfes;

    if-nez v2, :cond_5

    iget-object v2, p1, Lfeq;->b:Lfes;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lfeq;->b:Lfes;

    iget-object v3, p1, Lfeq;->b:Lfes;

    invoke-virtual {v2, v3}, Lfes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lfeq;->c:Lfet;

    if-nez v2, :cond_7

    iget-object v2, p1, Lfeq;->c:Lfet;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lfeq;->c:Lfet;

    iget-object v3, p1, Lfeq;->c:Lfet;

    invoke-virtual {v2, v3}, Lfet;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lfeq;->a:Lfex;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfeq;->b:Lfes;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfeq;->c:Lfet;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lfeq;->a:Lfex;

    invoke-virtual {v0}, Lfex;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfeq;->b:Lfes;

    invoke-virtual {v0}, Lfes;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lfeq;->c:Lfet;

    invoke-virtual {v1}, Lfet;->hashCode()I

    move-result v1

    goto :goto_2
.end method
