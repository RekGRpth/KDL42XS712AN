.class final Ldyz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbel;


# instance fields
.field final synthetic a:Lbeh;

.field final synthetic b:Lbeh;

.field final synthetic c:Lbeh;

.field final synthetic d:Lbeh;

.field final synthetic e:Ldyx;


# direct methods
.method constructor <init>(Ldyx;Lbeh;Lbeh;Lbeh;Lbeh;)V
    .locals 0

    iput-object p1, p0, Ldyz;->e:Ldyx;

    iput-object p2, p0, Ldyz;->a:Lbeh;

    iput-object p3, p0, Ldyz;->b:Lbeh;

    iput-object p4, p0, Ldyz;->c:Lbeh;

    iput-object p5, p0, Ldyz;->d:Lbeh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lbek;)V
    .locals 11

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-static {v0}, Ldyx;->b(Ldyx;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->x_()V

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-static {v0}, Ldyx;->c(Ldyx;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-virtual {v0}, Ldyx;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Leee;->b(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-virtual {v0}, Ldyx;->Q()V

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-static {v0}, Ldyx;->d(Ldyx;)Z

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-virtual {v0}, Ldyx;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldyz;->e:Ldyx;

    invoke-virtual {v0}, Ldyx;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Ldyz;->a:Lbeh;

    if-eqz v1, :cond_e

    iget-object v0, p0, Ldyz;->a:Lbeh;

    invoke-interface {v0}, Lbeh;->c()Lbek;

    move-result-object v0

    check-cast v0, Lctx;

    invoke-interface {v0}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    move-object v3, v0

    move v2, v1

    :goto_1
    iget-object v0, p0, Ldyz;->b:Lbeh;

    invoke-interface {v0}, Lbeh;->c()Lbek;

    move-result-object v0

    check-cast v0, Lctx;

    invoke-interface {v0}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v6

    iget-object v1, p0, Ldyz;->c:Lbeh;

    invoke-interface {v1}, Lbeh;->c()Lbek;

    move-result-object v1

    check-cast v1, Lctx;

    invoke-interface {v1}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v8

    :try_start_0
    iget-object v7, p0, Ldyz;->e:Ldyx;

    invoke-static {v7}, Ldyx;->e(Ldyx;)Ldvn;

    move-result-object v7

    invoke-virtual {v7, v2}, Ldvn;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->f(Ldyx;)Ldvn;

    move-result-object v2

    invoke-virtual {v2, v6}, Ldvn;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->g(Ldyx;)Ldvn;

    move-result-object v2

    invoke-virtual {v2, v8}, Ldvn;->d(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    invoke-interface {v3}, Lctx;->a()Lcts;

    move-result-object v2

    invoke-virtual {v2}, Lcts;->b()V

    invoke-interface {v0}, Lctx;->a()Lcts;

    move-result-object v0

    invoke-virtual {v0}, Lcts;->b()V

    invoke-interface {v1}, Lctx;->a()Lcts;

    move-result-object v0

    invoke-virtual {v0}, Lcts;->b()V

    goto :goto_0

    :cond_4
    :try_start_1
    invoke-static {v8}, Leee;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->a(Ldyx;)Ldyp;

    move-result-object v2

    invoke-virtual {v2}, Ldyp;->h()V

    :cond_5
    iget-object v2, p0, Ldyz;->a:Lbeh;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-interface {v3}, Lctx;->a()Lcts;

    move-result-object v6

    invoke-static {v2, v6}, Ldyx;->a(Ldyx;Lbgo;)Lbgo;

    :goto_2
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->h(Ldyx;)Ldyp;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ldyp;->c(Z)V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->i(Ldyx;)Lbgo;

    move-result-object v2

    invoke-virtual {v2}, Lbgo;->a()I

    move-result v2

    if-lez v2, :cond_d

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->h(Ldyx;)Ldyp;

    move-result-object v2

    iget-object v6, p0, Ldyz;->e:Ldyx;

    invoke-static {v6}, Ldyx;->j(Ldyx;)Z

    move-result v6

    invoke-virtual {v2, v6}, Ldyp;->c(Z)V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    iget-object v6, p0, Ldyz;->e:Ldyx;

    invoke-static {v6}, Ldyx;->h(Ldyx;)Ldyp;

    move-result-object v6

    invoke-static {v2, v6}, Ldyx;->a(Ldyx;Ldyp;)Ldyp;

    move v6, v5

    :goto_3
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->h(Ldyx;)Ldyp;

    move-result-object v2

    iget-object v7, p0, Ldyz;->e:Ldyx;

    invoke-static {v7}, Ldyx;->i(Ldyx;)Lbgo;

    move-result-object v7

    invoke-virtual {v2, v7}, Ldyp;->a(Lbgo;)V

    invoke-interface {v0}, Lctx;->a()Lcts;

    move-result-object v2

    iget-object v7, p0, Ldyz;->e:Ldyx;

    new-instance v9, Lbgx;

    const-string v10, "external_player_id"

    invoke-direct {v9, v2, v10}, Lbgx;-><init>(Lbgo;Ljava/lang/String;)V

    invoke-static {v7, v9}, Ldyx;->a(Ldyx;Lbgx;)Lbgx;

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->i(Ldyx;)Lbgo;

    move-result-object v2

    invoke-virtual {v2}, Lbgo;->a()I

    move-result v9

    move v7, v4

    :goto_4
    if-ge v7, v9, :cond_7

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->i(Ldyx;)Lbgo;

    move-result-object v2

    invoke-virtual {v2, v7}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/Player;

    iget-object v10, p0, Ldyz;->e:Ldyx;

    invoke-static {v10}, Ldyx;->k(Ldyx;)Lbgx;

    move-result-object v10

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lbgx;->a(Ljava/lang/String;)V

    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_4

    :cond_6
    iget-object v2, p0, Ldyz;->e:Ldyx;

    new-instance v6, Lbhb;

    invoke-direct {v6}, Lbhb;-><init>()V

    invoke-static {v2, v6}, Ldyx;->a(Ldyx;Lbgo;)Lbgo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    invoke-interface {v3}, Lctx;->a()Lcts;

    move-result-object v3

    invoke-virtual {v3}, Lcts;->b()V

    invoke-interface {v0}, Lctx;->a()Lcts;

    move-result-object v0

    invoke-virtual {v0}, Lcts;->b()V

    invoke-interface {v1}, Lctx;->a()Lcts;

    move-result-object v0

    invoke-virtual {v0}, Lcts;->b()V

    throw v2

    :cond_7
    :try_start_2
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->l(Ldyx;)Ldyp;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ldyp;->c(Z)V

    if-nez v6, :cond_c

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->k(Ldyx;)Lbgx;

    move-result-object v2

    invoke-virtual {v2}, Lbgx;->a()I

    move-result v2

    if-lez v2, :cond_c

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->l(Ldyx;)Ldyp;

    move-result-object v2

    iget-object v6, p0, Ldyz;->e:Ldyx;

    invoke-static {v6}, Ldyx;->j(Ldyx;)Z

    move-result v6

    invoke-virtual {v2, v6}, Ldyp;->c(Z)V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    iget-object v6, p0, Ldyz;->e:Ldyx;

    invoke-static {v6}, Ldyx;->l(Ldyx;)Ldyp;

    move-result-object v6

    invoke-static {v2, v6}, Ldyx;->a(Ldyx;Ldyp;)Ldyp;

    move v2, v5

    :goto_5
    iget-object v5, p0, Ldyz;->e:Ldyx;

    invoke-static {v5}, Ldyx;->l(Ldyx;)Ldyp;

    move-result-object v5

    iget-object v6, p0, Ldyz;->e:Ldyx;

    invoke-static {v6}, Ldyx;->k(Ldyx;)Lbgx;

    move-result-object v6

    invoke-virtual {v5, v6}, Ldyp;->a(Lbgo;)V

    invoke-interface {v1}, Lctx;->a()Lcts;

    move-result-object v5

    iget-object v6, p0, Ldyz;->e:Ldyx;

    new-instance v7, Lbhf;

    const-string v9, "profile_name"

    invoke-direct {v7, v5, v9}, Lbhf;-><init>(Lbgo;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ldyx;->a(Ldyx;Lbhf;)Lbhf;

    iget-object v5, p0, Ldyz;->e:Ldyx;

    invoke-static {v5}, Ldyx;->a(Ldyx;)Ldyp;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ldyp;->c(Z)V

    if-nez v2, :cond_8

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->a(Ldyx;)Ldyp;

    move-result-object v2

    iget-object v5, p0, Ldyz;->e:Ldyx;

    invoke-static {v5}, Ldyx;->j(Ldyx;)Z

    move-result v5

    invoke-virtual {v2, v5}, Ldyp;->c(Z)V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    iget-object v5, p0, Ldyz;->e:Ldyx;

    invoke-static {v5}, Ldyx;->a(Ldyx;)Ldyp;

    move-result-object v5

    invoke-static {v2, v5}, Ldyx;->a(Ldyx;Ldyp;)Ldyp;

    :cond_8
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->a(Ldyx;)Ldyp;

    move-result-object v2

    iget-object v5, p0, Ldyz;->e:Ldyx;

    invoke-static {v5}, Ldyx;->m(Ldyx;)Lbhf;

    move-result-object v5

    invoke-virtual {v2, v5}, Ldyp;->a(Lbgo;)V

    iget-object v2, p0, Ldyz;->d:Lbeh;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ldyz;->d:Lbeh;

    invoke-interface {v2}, Lbeh;->c()Lbek;

    move-result-object v2

    check-cast v2, Lctx;

    invoke-interface {v2}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v5

    if-nez v5, :cond_a

    invoke-interface {v2}, Lctx;->a()Lcts;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    :try_start_3
    invoke-virtual {v5}, Lcts;->a()I

    move-result v6

    :goto_6
    if-ge v4, v6, :cond_9

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->b(Ldyx;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v7

    invoke-virtual {v5, v4}, Lcts;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/Player;

    invoke-virtual {v7, v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_9
    :try_start_4
    invoke-virtual {v5}, Lcts;->b()V

    :cond_a
    invoke-static {v8}, Leee;->a(I)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->n(Ldyx;)Z

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->o(Ldyx;)V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-virtual {v2}, Ldyx;->W()V

    :goto_7
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->b(Ldyx;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->O()V

    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->b(Ldyx;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->R()V

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    invoke-virtual {v5}, Lcts;->b()V

    throw v2

    :cond_b
    iget-object v2, p0, Ldyz;->e:Ldyx;

    invoke-static {v2}, Ldyx;->p(Ldyx;)Leds;

    move-result-object v2

    const v4, 0x7f0b02d8    # com.google.android.gms.R.string.games_player_search_list_generic_error

    const v5, 0x7f0b02d5    # com.google.android.gms.R.string.games_search_players_no_results

    invoke-virtual {v2, v8, v4, v5}, Leds;->a(III)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_7

    :cond_c
    move v2, v6

    goto/16 :goto_5

    :cond_d
    move v6, v4

    goto/16 :goto_3

    :cond_e
    move-object v3, v0

    move v2, v4

    goto/16 :goto_1
.end method
