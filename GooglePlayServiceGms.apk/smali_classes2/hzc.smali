.class final Lhzc;
.super Lhyy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhzc;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyy;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "NoGeofenceState"

    return-object v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lhzc;->c:Lhys;

    iget-object v0, v0, Lhys;->h:Lhxq;

    invoke-virtual {v0}, Lhxq;->d()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "No geofences should be registered in NoGeofenceState."

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processMessage, current state=NoGeofenceState msg="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lhzc;->a:Lhyt;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Lhyt;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p1}, Lhzc;->b(Landroid/os/Message;)Z

    move-result v1

    :cond_1
    :goto_1
    :pswitch_1
    return v1

    :cond_2
    move v0, v2

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhzc;->a:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "Network location disabled."

    invoke-static {v0, v2}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhzc;->a:Lhyt;

    iget-object v2, p0, Lhzc;->a:Lhyt;

    invoke-static {v2}, Lhyt;->n(Lhyt;)Lhyz;

    move-result-object v2

    invoke-static {v0, v2}, Lhyt;->e(Lhyt;Lbqg;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lhzc;->a:Lhyt;

    invoke-static {v0, p1}, Lhyt;->a(Lhyt;Landroid/os/Message;)V

    iget-object v0, p0, Lhzc;->a:Lhyt;

    iget-object v2, p0, Lhzc;->a:Lhyt;

    invoke-static {v2}, Lhyt;->k(Lhyt;)Lhzb;

    move-result-object v2

    invoke-static {v0, v2}, Lhyt;->f(Lhyt;Lbqg;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhzv;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhzv;->a(Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lhzc;->c:Lhys;

    invoke-virtual {v0}, Lhys;->e()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    invoke-super {p0}, Lhyy;->b()V

    iget-object v0, p0, Lhzc;->c:Lhys;

    iget-object v1, v0, Lhys;->g:Lhzk;

    invoke-virtual {v1}, Lhzk;->a()V

    iget-object v0, v0, Lhys;->f:Lhyi;

    invoke-virtual {v0}, Lhyi;->a()V

    return-void
.end method
