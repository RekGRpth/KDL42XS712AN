.class public final Lhka;
.super Lhin;
.source "SourceFile"


# instance fields
.field A:Z

.field B:J

.field C:Livi;

.field D:J

.field E:Liha;

.field F:Z

.field private final G:Ljava/util/Random;

.field g:Z

.field h:Z

.field i:Z

.field j:Lhtf;

.field k:Lhuv;

.field l:Z

.field m:Lhuv;

.field n:Lidr;

.field o:Z

.field p:Livi;

.field q:J

.field r:Z

.field s:Z

.field t:Z

.field u:Z

.field v:Z

.field w:J

.field x:J

.field y:Lihe;

.field final z:Lhlb;


# direct methods
.method public constructor <init>(Lidu;Lhof;Lhlb;Lhkl;Lhiq;Liha;)V
    .locals 7

    const-string v1, "PassiveCollector"

    sget-object v6, Lhir;->b:Lhir;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhka;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->j:Lhtf;

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->k:Lhuv;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->l:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->m:Lhuv;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->o:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->q:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->r:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->s:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->t:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->u:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->v:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->w:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->x:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->y:Lihe;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->A:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->B:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->D:J

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lhka;->G:Ljava/util/Random;

    iget-object v0, p2, Lhof;->c:Lids;

    invoke-virtual {p0, v0}, Lhka;->a(Lids;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Initializing smart-collection to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lhka;->F:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p6, p0, Lhka;->E:Liha;

    iput-object p3, p0, Lhka;->z:Lhlb;

    return-void
.end method

.method private static a(ZZZ)I
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    move v2, v0

    :goto_0
    add-int/lit8 v3, v2, 0x0

    if-eqz p1, :cond_1

    move v2, v0

    :goto_1
    add-int/2addr v2, v3

    if-eqz p2, :cond_2

    :goto_2
    add-int/2addr v0, v2

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private a(IJZZZ)V
    .locals 7

    const/4 v4, 0x0

    if-eqz p5, :cond_0

    if-nez p4, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhka;->l:Z

    iget-object v0, p0, Lhka;->m:Lhuv;

    iput-object v0, p0, Lhka;->k:Lhuv;

    :cond_0
    if-eqz p4, :cond_4

    iget-object v2, p0, Lhka;->n:Lidr;

    :goto_1
    if-eqz p6, :cond_5

    iget-object v3, p0, Lhka;->j:Lhtf;

    :goto_2
    if-eqz p5, :cond_1

    iget-object v4, p0, Lhka;->m:Lhuv;

    :cond_1
    iget-object v0, p0, Lhka;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v0

    iget-boolean v5, p0, Lhka;->h:Z

    move v6, p1

    invoke-static/range {v0 .. v6}, Lhka;->a(JLidr;Lhtf;Lhuv;ZI)Livi;

    move-result-object v0

    iget-object v1, p0, Lhka;->e:Lhiq;

    invoke-virtual {v1, v2, v3, v4}, Lhiq;->a(Lidr;Lhtf;Lhuv;)V

    iget-object v1, p0, Lhka;->C:Livi;

    if-nez v1, :cond_2

    new-instance v1, Livi;

    sget-object v2, Lihj;->Q:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    iput-object v1, p0, Lhka;->C:Livi;

    iput-wide p2, p0, Lhka;->D:J

    :cond_2
    iget-object v1, p0, Lhka;->C:Livi;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move-object v2, v4

    goto :goto_1

    :cond_5
    move-object v3, v4

    goto :goto_2
.end method

.method private a(IZZZ)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {p2, p3, p4}, Lhka;->a(ZZZ)I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p4, :cond_2

    if-ne p1, v1, :cond_2

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhka;->a:Ljava/lang/String;

    const-string v2, "Cell changed, but could not be paired somehow."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne p1, v2, :cond_4

    if-eqz p3, :cond_3

    if-nez p2, :cond_4

    :cond_3
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhka;->a:Ljava/lang/String;

    const-string v2, "Gps changed but GPS or WiFi scan could not be paired."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lhka;->b:Lidu;

    const/4 v1, 0x3

    invoke-interface {v0, v1, v3}, Lidu;->a(ILilx;)V

    sget-object v0, Lhir;->f:Lhir;

    iput-object v0, p0, Lhka;->f:Lhir;

    iget-object v0, p0, Lhka;->C:Livi;

    iput-object v0, p0, Lhka;->p:Livi;

    iget-object v0, p0, Lhka;->d:Lhkl;

    iget-object v1, p0, Lhka;->b:Lidu;

    iget-object v2, p0, Lhka;->p:Livi;

    invoke-virtual {v0, v1, v2}, Lhkl;->a(Lidu;Livi;)V

    iget-object v0, p0, Lhka;->b:Lidu;

    iget-object v1, p0, Lhka;->p:Livi;

    invoke-interface {v0, v1}, Lidu;->b(Livi;)V

    iput-object v3, p0, Lhka;->C:Livi;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->D:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhka;->r:Z

    iget-object v0, p0, Lhka;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhka;->q:J

    iget-wide v0, p0, Lhka;->q:J

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lhka;->h(J)V

    return-void
.end method

.method private g(J)Z
    .locals 4

    const/4 v1, 0x4

    iget-object v0, p0, Lhka;->C:Livi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhka;->C:Livi;

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhka;->C:Livi;

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    iget-wide v0, p0, Lhka;->D:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final h()V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    iget-wide v2, p0, Lhka;->w:J

    cmp-long v0, v2, v8

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->b:Lidu;

    iget-object v2, p0, Lhka;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lidu;->a(Ljava/lang/String;Z)V

    const-wide/16 v0, 0x0

    const-wide/32 v2, 0x1d4c0

    iget-object v4, p0, Lhka;->b:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lhka;->w:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lhka;->E:Liha;

    iget-object v3, p0, Lhka;->y:Lihe;

    invoke-virtual {v2, v3, v0, v1}, Liha;->a(Lihe;J)V

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->y:Lihe;

    invoke-direct {p0}, Lhka;->i()V

    iput-wide v8, p0, Lhka;->w:J

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private h(J)V
    .locals 5

    iget-wide v0, p0, Lhka;->B:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lhka;->B:J

    iget-object v0, p0, Lhka;->b:Lidu;

    const/4 v1, 0x3

    iget-wide v2, p0, Lhka;->B:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lhka;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->b:Lidu;

    invoke-interface {v0, v2}, Lidu;->a(I)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->B:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->r:Z

    iget-object v0, p0, Lhka;->b:Lidu;

    invoke-interface {v0, v2}, Lidu;->b(I)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhka;->s:Z

    iput-boolean v0, p0, Lhka;->t:Z

    iput-boolean v0, p0, Lhka;->u:Z

    iput-boolean v0, p0, Lhka;->v:Z

    iput-boolean v0, p0, Lhka;->A:Z

    return-void
.end method

.method private k()Z
    .locals 1

    iget-boolean v0, p0, Lhka;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhka;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhka;->B:J

    :cond_0
    return-void
.end method

.method public final a(IIZ)V
    .locals 4

    invoke-static {p1, p2}, Lilv;->a(II)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    if-nez p3, :cond_0

    float-to-double v0, v0

    const-wide v2, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhka;->g:Z

    :cond_1
    iput-boolean p3, p0, Lhka;->h:Z

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhtf;)V
    .locals 3

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhtf;->i()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->j:Lhtf;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iget-object v2, p0, Lhka;->j:Lhtf;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->j:Lhtf;

    invoke-virtual {v2, p1}, Lhtf;->b(Lhtf;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lhka;->j:Lhtf;

    invoke-virtual {v0}, Lhtf;->h()I

    move-result v0

    invoke-virtual {p1}, Lhtf;->h()I

    move-result v2

    if-eq v0, v2, :cond_1

    move v0, v1

    :cond_3
    iput-object p1, p0, Lhka;->j:Lhtf;

    invoke-direct {p0}, Lhka;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_4

    iput-boolean v1, p0, Lhka;->t:Z

    :cond_4
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCellState(): Updated to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhka;->j:Lhtf;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lidr;)V
    .locals 9

    const/4 v8, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lhka;->n:Lidr;

    invoke-direct {p0}, Lhka;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhka;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhka;->m:Lhuv;

    iget-object v1, p0, Lhka;->n:Lidr;

    invoke-static {v0, v1}, Lhka;->a(Lhuv;Lidr;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v8, p0, Lhka;->u:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhka;->e:Lhiq;

    iget-object v2, v0, Lhiq;->a:Lidr;

    if-eqz v2, :cond_0

    iget-object v6, p0, Lhka;->n:Lidr;

    invoke-interface {v2}, Lidr;->b()D

    move-result-wide v0

    invoke-interface {v2}, Lidr;->c()D

    move-result-wide v2

    invoke-interface {v6}, Lidr;->b()D

    move-result-wide v4

    invoke-interface {v6}, Lidr;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iput-boolean v8, p0, Lhka;->v:Z

    goto :goto_0
.end method

.method public final a(Lids;)V
    .locals 3

    invoke-virtual {p1}, Lids;->f()Z

    move-result v0

    iput-boolean v0, p0, Lhka;->F:Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Now setting smart-collection to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lhka;->F:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Livi;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhka;->p:Livi;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final b(Lhuv;)V
    .locals 9

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lhka;->m:Lhuv;

    iget-object v0, p0, Lhka;->e:Lhiq;

    iget-object v5, v0, Lhiq;->d:Lhoj;

    iget-object v6, p0, Lhka;->m:Lhuv;

    move v1, v2

    :goto_1
    iget-object v0, v6, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    invoke-virtual {v6, v1}, Lhuv;->a(I)Lhut;

    move-result-object v0

    iget-wide v7, v0, Lhut;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v0, v5, Lhoj;->b:Lhok;

    invoke-virtual {v0, v7}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    iget-object v8, v5, Lhoj;->a:Lhom;

    if-eqz v0, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v8, v3}, Lhom;->a(Z)V

    if-nez v0, :cond_5

    const/high16 v0, -0x40800000    # -1.0f

    :goto_3
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_6

    iget-object v3, p0, Lhka;->G:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    cmpg-float v0, v3, v0

    if-gez v0, :cond_6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found important AP: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_4
    iput-boolean v4, p0, Lhka;->A:Z

    invoke-direct {p0}, Lhka;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->k:Lhuv;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhka;->m:Lhuv;

    iget-wide v0, v0, Lhuv;->a:J

    iget-object v2, p0, Lhka;->k:Lhuv;

    iget-wide v2, v2, Lhuv;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    :cond_3
    iget-boolean v0, p0, Lhka;->s:Z

    iget-object v1, p0, Lhka;->e:Lhiq;

    iget-object v2, p0, Lhka;->m:Lhuv;

    iget-object v3, p0, Lhka;->b:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lhiq;->a(Lhuv;J)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lhka;->s:Z

    iget-boolean v0, p0, Lhka;->s:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhka;->m:Lhuv;

    iget-object v2, v2, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " APs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v0}, Lhol;->a()F

    move-result v0

    goto/16 :goto_3

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_7
    move v4, v2

    goto :goto_4
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lhka;->i:Z

    return-void
.end method

.method protected final b(J)Z
    .locals 22

    invoke-direct/range {p0 .. p0}, Lhka;->k()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lhka;->j()V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-direct/range {p0 .. p2}, Lhka;->g(J)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct/range {p0 .. p0}, Lhka;->g()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    iget-wide v2, v2, Lhuv;->a:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->e:Lhiq;

    iget-wide v4, v4, Lhiq;->b:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_7

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->j:Lhtf;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->j:Lhtf;

    invoke-virtual {v3}, Lhtf;->f()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->e:Lhiq;

    iget-wide v5, v5, Lhiq;->c:J

    cmp-long v3, v3, v5

    if-gtz v3, :cond_8

    :cond_3
    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lhka;->s:Z

    if-eqz v4, :cond_4

    if-eqz v2, :cond_4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lhka;->s:Z

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lhka;->t:Z

    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lhka;->t:Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->m:Lhuv;

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->m:Lhuv;

    iget-wide v4, v4, Lhuv;->a:J

    sub-long v4, p1, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0xafc80

    cmp-long v4, v4, v6

    if-gtz v4, :cond_9

    const/4 v4, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-wide v5, v0, Lhka;->x:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhka;->x:J

    sub-long v5, p1, v5

    const-wide/32 v7, 0x493e0

    cmp-long v5, v5, v7

    if-gez v5, :cond_a

    const/4 v5, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhka;->s:Z

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhka;->A:Z

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhka;->u:Z

    if-nez v6, :cond_d

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhka;->F:Z

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhka;->i:Z

    if-nez v6, :cond_d

    if-eqz v4, :cond_d

    if-nez v5, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->b:Lidu;

    invoke-interface {v4}, Lidu;->k()Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v4, 0x0

    :cond_6
    :goto_5
    if-eqz v4, :cond_d

    sget-object v2, Lhir;->c:Lhir;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhka;->f:Lhir;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lhka;->l:Z

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v4, 0x0

    goto :goto_3

    :cond_a
    const/4 v5, 0x0

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->E:Liha;

    iget-object v5, v4, Liha;->c:Lihd;

    const-wide/32 v6, 0x1d4c0

    const/4 v8, 0x1

    invoke-virtual {v4, v5, v6, v7, v8}, Liha;->a(Lihd;JZ)Lihe;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lhka;->y:Lihe;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->y:Lihe;

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->b:Lidu;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lidu;->a(ILilx;)V

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lhka;->r:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->b:Lidu;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhka;->a:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Lidu;->a(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->b:Lidu;

    invoke-interface {v5}, Lidu;->j()Licm;

    move-result-object v5

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lhka;->w:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhka;->w:J

    const-wide/32 v7, 0x1d4c0

    add-long/2addr v5, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lhka;->h(J)V

    goto :goto_5

    :cond_c
    const/4 v4, 0x0

    goto :goto_6

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lhka;->A:Z

    if-nez v4, :cond_f

    if-eqz v2, :cond_f

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->u:Z

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->v:Z

    if-eqz v2, :cond_f

    :cond_e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lhka;->u:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lhka;->v:Z

    :cond_f
    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->s:Z

    if-nez v2, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->t:Z

    if-nez v2, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->u:Z

    if-nez v2, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->v:Z

    if-eqz v2, :cond_37

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->s:Z

    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->A:Z

    if-eqz v2, :cond_1c

    const/16 v2, 0x10

    move/from16 v17, v2

    :goto_7
    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_37

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->b:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    if-eqz v2, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    invoke-interface {v2}, Lidr;->f()J

    move-result-wide v2

    sub-long v2, v18, v2

    const-wide/32 v4, 0xafc80

    cmp-long v2, v2, v4

    if-gtz v2, :cond_21

    const/4 v2, 0x1

    move v9, v2

    :goto_8
    const/4 v11, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    if-nez v2, :cond_22

    :cond_11
    const/4 v2, 0x0

    :goto_9
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->m:Lhuv;

    if-eqz v3, :cond_3b

    const/4 v3, 0x2

    move/from16 v0, v17

    if-eq v0, v3, :cond_12

    const/16 v3, 0x10

    move/from16 v0, v17

    if-ne v0, v3, :cond_28

    :cond_12
    const/4 v3, 0x1

    const/4 v4, 0x1

    if-eqz v2, :cond_3c

    if-eqz v9, :cond_13

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_13

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->a:Ljava/lang/String;

    const-string v6, "Location will not be paired because of significant move."

    invoke-static {v5, v6}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    if-eqz v9, :cond_27

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_27

    const/4 v2, 0x1

    :goto_b
    move v10, v3

    move v11, v4

    move v12, v2

    :goto_c
    const/4 v15, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    invoke-virtual {v2}, Lhtf;->f()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->n:Lidr;

    invoke-interface {v4}, Lidr;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2a

    :cond_14
    const/4 v2, 0x0

    :goto_d
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v13, v2

    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    invoke-virtual {v2}, Lhtf;->f()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->m:Lhuv;

    iget-wide v4, v4, Lhuv;->a:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2f

    :cond_15
    const/4 v2, 0x0

    :goto_f
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->j:Lhtf;

    if-eqz v3, :cond_3a

    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->e:Lhiq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->j:Lhtf;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhka;->n:Lidr;

    invoke-virtual {v3, v4, v5}, Lhiq;->a(Lhtf;Lidr;)Z

    move-result v3

    if-eqz v11, :cond_16

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v2, v4, :cond_17

    :cond_16
    if-eqz v12, :cond_39

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eq v13, v2, :cond_39

    if-nez v3, :cond_39

    :cond_17
    const/4 v15, 0x1

    move v2, v15

    :goto_11
    if-nez v10, :cond_18

    if-eqz v9, :cond_38

    if-nez v3, :cond_38

    :cond_18
    const/4 v8, 0x1

    move v13, v2

    :goto_12
    invoke-static {v12, v11, v13}, Lhka;->a(ZZZ)I

    move-result v2

    invoke-static {v9, v10, v8}, Lhka;->a(ZZZ)I

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v12, v11, v13}, Lhka;->a(IZZZ)Z

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v9, v10, v8}, Lhka;->a(IZZZ)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lhka;->c:Lhof;

    iget-object v6, v6, Lhof;->c:Lids;

    invoke-virtual {v6}, Lids;->j()Z

    move-result v6

    if-nez v6, :cond_35

    if-lt v2, v3, :cond_19

    if-nez v4, :cond_35

    :cond_19
    if-eqz v5, :cond_34

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->a:Ljava/lang/String;

    const-string v3, "Unfiltered collection contains more data, dropping the filtered one."

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    move-object/from16 v2, p0

    move/from16 v3, v17

    move-wide/from16 v4, v18

    move v6, v9

    move v7, v10

    invoke-direct/range {v2 .. v8}, Lhka;->a(IJZZZ)V

    const/4 v2, 0x1

    :goto_13
    if-eqz v2, :cond_1b

    invoke-direct/range {p0 .. p0}, Lhka;->j()V

    :cond_1b
    invoke-direct/range {p0 .. p2}, Lhka;->g(J)Z

    move-result v2

    if-eqz v2, :cond_37

    invoke-direct/range {p0 .. p0}, Lhka;->g()V

    const/4 v2, 0x1

    :goto_14
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lhka;->A:Z

    goto/16 :goto_0

    :cond_1c
    const/4 v2, 0x2

    move/from16 v17, v2

    goto/16 :goto_7

    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->u:Z

    if-eqz v2, :cond_1e

    const/4 v2, 0x4

    move/from16 v17, v2

    goto/16 :goto_7

    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->v:Z

    if-eqz v2, :cond_1f

    const/16 v2, 0xb

    move/from16 v17, v2

    goto/16 :goto_7

    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhka;->t:Z

    if-eqz v2, :cond_20

    const/4 v2, 0x1

    move/from16 v17, v2

    goto/16 :goto_7

    :cond_20
    const/4 v2, -0x1

    move/from16 v17, v2

    goto/16 :goto_7

    :cond_21
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_8

    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    invoke-interface {v2}, Lidr;->f()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    iget-wide v5, v2, Lhuv;->a:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x7d0

    invoke-virtual/range {v2 .. v8}, Lhlb;->a(JJJ)Ljava/lang/Boolean;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x1f40

    invoke-virtual/range {v2 .. v8}, Lhlb;->b(JJJ)Ljava/lang/Boolean;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x4e20

    invoke-virtual/range {v2 .. v8}, Lhlb;->c(JJJ)Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v12, :cond_23

    if-eqz v13, :cond_23

    if-nez v2, :cond_24

    :cond_23
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_24
    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_25

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_25

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_26

    :cond_25
    const/4 v2, 0x1

    goto/16 :goto_9

    :cond_26
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_27
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_28
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhka;->l:Z

    if-eqz v3, :cond_3b

    if-eqz v9, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lhka;->m:Lhuv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhka;->n:Lidr;

    invoke-static {v3, v4}, Lhka;->a(Lhuv;Lidr;)Z

    move-result v3

    if-eqz v3, :cond_3b

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eq v2, v4, :cond_29

    const/4 v2, 0x1

    :goto_15
    move v10, v3

    move v11, v2

    move v12, v9

    goto/16 :goto_c

    :cond_29
    const/4 v2, 0x0

    goto :goto_15

    :cond_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->n:Lidr;

    invoke-interface {v2}, Lidr;->f()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    invoke-virtual {v2}, Lhtf;->f()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x4e20

    invoke-virtual/range {v2 .. v8}, Lhlb;->a(JJJ)Ljava/lang/Boolean;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/32 v7, 0x13880

    invoke-virtual/range {v2 .. v8}, Lhlb;->b(JJJ)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/32 v7, 0x30d40

    invoke-virtual/range {v2 .. v8}, Lhlb;->c(JJJ)Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v13, :cond_2b

    if-eqz v20, :cond_2b

    if-nez v2, :cond_2c

    :cond_2b
    const/4 v2, 0x0

    move-object v13, v2

    goto/16 :goto_e

    :cond_2c
    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2d

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2d

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2e

    :cond_2d
    const/4 v2, 0x1

    goto/16 :goto_d

    :cond_2e
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_2f
    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->m:Lhuv;

    iget-wide v3, v2, Lhuv;->a:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->j:Lhtf;

    invoke-virtual {v2}, Lhtf;->f()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x7d0

    invoke-virtual/range {v2 .. v8}, Lhlb;->a(JJJ)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x1f40

    invoke-virtual/range {v2 .. v8}, Lhlb;->b(JJJ)Ljava/lang/Boolean;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lhka;->z:Lhlb;

    const-wide/16 v7, 0x4e20

    invoke-virtual/range {v2 .. v8}, Lhlb;->c(JJJ)Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v20, :cond_30

    if-eqz v21, :cond_30

    if-nez v2, :cond_31

    :cond_30
    const/4 v2, 0x0

    goto/16 :goto_10

    :cond_31
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_32

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_32

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_33

    :cond_32
    const/4 v2, 0x1

    goto/16 :goto_f

    :cond_33
    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_34
    const/4 v2, 0x0

    goto/16 :goto_13

    :cond_35
    if-eqz v4, :cond_36

    move-object/from16 v2, p0

    move/from16 v3, v17

    move-wide/from16 v4, v18

    move v6, v12

    move v7, v11

    move v8, v13

    invoke-direct/range {v2 .. v8}, Lhka;->a(IJZZZ)V

    const/4 v2, 0x1

    goto/16 :goto_13

    :cond_36
    const/4 v2, 0x0

    goto/16 :goto_13

    :cond_37
    move/from16 v2, v16

    goto/16 :goto_14

    :cond_38
    move v8, v14

    move v13, v2

    goto/16 :goto_12

    :cond_39
    move v2, v15

    goto/16 :goto_11

    :cond_3a
    move v8, v14

    move v13, v15

    goto/16 :goto_12

    :cond_3b
    move v12, v9

    goto/16 :goto_c

    :cond_3c
    move v10, v3

    move v11, v4

    move v12, v9

    goto/16 :goto_c
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Lhka;->o:Z

    return-void
.end method

.method protected final c(J)Z
    .locals 6

    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-wide v2, p0, Lhka;->w:J

    sub-long v2, p1, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    iput-wide p1, p0, Lhka;->x:J

    iget-boolean v3, p0, Lhka;->F:Z

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lhka;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lhka;->i:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lhka;->u:Z

    if-nez v3, :cond_0

    if-eqz v2, :cond_3

    :cond_0
    invoke-direct {p0}, Lhka;->h()V

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhka;->f:Lhir;

    move v0, v1

    :goto_2
    return v0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    const-wide/16 p1, -0x1

    goto :goto_1

    :cond_3
    iget-wide v1, p0, Lhka;->w:J

    add-long/2addr v1, v4

    invoke-direct {p0, v1, v2}, Lhka;->h(J)V

    goto :goto_2

    :cond_4
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhka;->a:Ljava/lang/String;

    const-string v2, "collection disabled or battery is low or screen is on, gps turnoff"

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lhka;->h()V

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhka;->f:Lhir;

    move v0, v1

    goto :goto_2
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lhin;->f()V

    return-void
.end method

.method protected final f(J)Z
    .locals 5

    const-wide/16 v3, 0x3a98

    const/4 v0, 0x0

    iget-object v1, p0, Lhka;->p:Livi;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0}, Lhka;->i()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhka;->q:J

    sget-object v1, Lhir;->b:Lhir;

    iput-object v1, p0, Lhka;->f:Lhir;

    :goto_0
    return v0

    :cond_0
    iget-wide v1, p0, Lhka;->q:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    invoke-direct {p0}, Lhka;->i()V

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lhka;->q:J

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lhka;->h(J)V

    goto :goto_0
.end method
