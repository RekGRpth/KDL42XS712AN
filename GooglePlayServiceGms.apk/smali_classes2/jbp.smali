.class public final Ljbp;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:F

.field private c:Z

.field private d:F

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput v0, p0, Ljbp;->b:F

    iput v0, p0, Ljbp;->d:F

    const/4 v0, -0x1

    iput v0, p0, Ljbp;->e:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbp;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbp;->b()I

    :cond_0
    iget v0, p0, Ljbp;->e:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput-boolean v1, p0, Ljbp;->a:Z

    iput v0, p0, Ljbp;->b:F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput-boolean v1, p0, Ljbp;->c:Z

    iput v0, p0, Ljbp;->d:F

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x15 -> :sswitch_1
        0x1d -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Ljbp;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Ljbp;->b:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_0
    iget-boolean v0, p0, Ljbp;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget v1, p0, Ljbp;->d:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljbp;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Ljbp;->b:F

    invoke-static {v0}, Lizh;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Ljbp;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget v2, p0, Ljbp;->d:F

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Ljbp;->e:I

    return v0
.end method
