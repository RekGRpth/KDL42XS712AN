.class public final Lbrn;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

.field private final d:Lcht;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p5}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    iput-object p3, p0, Lbrn;->d:Lcht;

    iput-object p4, p0, Lbrn;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    const-string v1, "Invalid add event listener request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v0

    iget-object v1, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-static {v0, v1}, Lchc;->a(ILcom/google/android/gms/drive/DriveId;)Z

    move-result v0

    const-string v1, "Invalid add event listener request: invalid drive id"

    invoke-static {v0, v1}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbrn;->d:Lcht;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrn;->b:Lbrc;

    iget-object v1, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v2

    iget-object v3, p0, Lbrn;->d:Lcht;

    invoke-interface {v0, v1, v2, v3}, Lbrc;->a(Lcom/google/android/gms/drive/DriveId;ILcht;)V

    :cond_0
    iget-object v0, p0, Lbrn;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbrn;->b:Lbrc;

    iget-object v1, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lbrn;->c:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v2

    iget-object v3, p0, Lbrn;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lbrc;->a(Lcom/google/android/gms/drive/DriveId;ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbrn;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->a()V

    return-void
.end method
