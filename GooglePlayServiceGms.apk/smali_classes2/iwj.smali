.class public final Liwj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liwi;


# instance fields
.field private final a:Lixh;

.field private final b:Lixh;

.field private final c:Lixi;

.field private final d:Lixi;

.field private final e:Lixg;


# direct methods
.method public constructor <init>(Lixh;Lixh;Lixi;Lixi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Liwj;->a:Lixh;

    iput-object p2, p0, Liwj;->b:Lixh;

    new-instance v0, Lixg;

    invoke-direct {v0}, Lixg;-><init>()V

    iput-object v0, p0, Liwj;->e:Lixg;

    iput-object p3, p0, Liwj;->c:Lixi;

    iput-object p4, p0, Liwj;->d:Lixi;

    return-void
.end method


# virtual methods
.method public final a()Lixh;
    .locals 1

    iget-object v0, p0, Liwj;->a:Lixh;

    return-object v0
.end method

.method public final a(Lixh;Lixh;)V
    .locals 1

    iget-object v0, p0, Liwj;->a:Lixh;

    invoke-virtual {v0, p1}, Lixh;->a(Lixh;)V

    iget-object v0, p0, Liwj;->b:Lixh;

    invoke-virtual {v0, p2}, Lixh;->a(Lixh;)V

    return-void
.end method

.method public final a(Lixh;Lixh;Lixh;)V
    .locals 3

    iget-object v0, p0, Liwj;->c:Lixi;

    iget-object v1, p0, Liwj;->a:Lixh;

    iget v1, v1, Lixh;->a:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lixi;->b(II)V

    iget-object v0, p0, Liwj;->a:Lixh;

    iget-object v1, p0, Liwj;->c:Lixi;

    invoke-static {p1, v0, v1}, Lixj;->a(Lixh;Lixh;Lixh;)V

    iget-object v0, p0, Liwj;->a:Lixh;

    iget-object v1, p0, Liwj;->c:Lixi;

    invoke-virtual {v0, v1}, Lixh;->a(Lixh;)V

    iget-object v0, p0, Liwj;->c:Lixi;

    iget-object v1, p0, Liwj;->a:Lixh;

    iget v1, v1, Lixh;->a:I

    iget-object v2, p0, Liwj;->a:Lixh;

    iget v2, v2, Lixh;->a:I

    invoke-virtual {v0, v1, v2}, Lixi;->b(II)V

    iget-object v0, p0, Liwj;->b:Lixh;

    iget-object v1, p0, Liwj;->c:Lixi;

    invoke-static {p1, v0, v1}, Lixj;->a(Lixh;Lixh;Lixh;)V

    iget-object v0, p0, Liwj;->c:Lixi;

    iget-object v1, p0, Liwj;->b:Lixh;

    invoke-static {v0, p1, v1}, Lixj;->d(Lixh;Lixh;Lixh;)V

    iget-object v0, p0, Liwj;->b:Lixh;

    iget-object v1, p0, Liwj;->b:Lixh;

    invoke-static {v0, p2, v1}, Lixj;->e(Lixh;Lixh;Lixh;)V

    if-eqz p3, :cond_0

    iget-object v0, p0, Liwj;->a:Lixh;

    iget-object v1, p0, Liwj;->a:Lixh;

    invoke-static {v0, p3, v1}, Lixj;->e(Lixh;Lixh;Lixh;)V

    :cond_0
    return-void
.end method

.method public final b()Lixh;
    .locals 1

    iget-object v0, p0, Liwj;->b:Lixh;

    return-object v0
.end method

.method public final b(Lixh;Lixh;Lixh;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p1

    iget v2, v0, Lixh;->a:I

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->a:Lixh;

    iget v3, v3, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->b:Lixh;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p2

    invoke-static {v0, v1, v2}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p1

    iget v2, v0, Lixh;->a:I

    move-object/from16 v0, p1

    iget v3, v0, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p2

    invoke-static {v1, v0, v2}, Lixj;->d(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lixj;->e(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Liwj;->e:Lixg;

    move-object/from16 v0, p0

    iget-object v11, v0, Liwj;->d:Lixi;

    iget v1, v11, Lixh;->a:I

    iget v2, v11, Lixh;->b:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Input matrix must be square (is size %d x %d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v11, Lixh;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v11, Lixh;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget v1, v11, Lixh;->a:I

    iget-object v2, v10, Lixg;->a:Lixi;

    if-eqz v2, :cond_1

    iget-object v2, v10, Lixg;->a:Lixi;

    iget v2, v2, Lixi;->a:I

    if-ge v2, v1, :cond_2

    :cond_1
    new-instance v2, Lixi;

    invoke-direct {v2, v1, v1}, Lixi;-><init>(II)V

    iput-object v2, v10, Lixg;->a:Lixi;

    :cond_2
    iget-object v2, v10, Lixg;->a:Lixi;

    invoke-virtual {v2, v1, v1}, Lixi;->b(II)V

    iget v2, v11, Lixh;->a:I

    const/4 v1, 0x0

    :goto_0
    iget v3, v11, Lixh;->c:I

    if-ge v1, v3, :cond_8

    const-wide/16 v3, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v2, :cond_7

    iget-object v5, v11, Lixh;->d:[D

    aget-wide v7, v5, v9

    iget v5, v11, Lixh;->a:I

    sub-int v6, v9, v5

    iget v5, v11, Lixh;->a:I

    sub-int v5, v1, v5

    :goto_2
    if-ltz v5, :cond_3

    iget-object v12, v10, Lixg;->a:Lixi;

    iget-object v12, v12, Lixi;->d:[D

    aget-wide v12, v12, v6

    iget-object v14, v10, Lixg;->a:Lixi;

    iget-object v14, v14, Lixi;->d:[D

    aget-wide v14, v14, v5

    mul-double/2addr v12, v14

    sub-double/2addr v7, v12

    iget v12, v11, Lixh;->a:I

    sub-int/2addr v6, v12

    iget v12, v11, Lixh;->a:I

    sub-int/2addr v5, v12

    goto :goto_2

    :cond_3
    if-ne v9, v1, :cond_6

    const-wide/16 v3, 0x0

    cmpg-double v3, v7, v3

    if-gez v3, :cond_5

    const-wide v3, -0x40af9db22d0e5604L    # -0.001

    cmpg-double v3, v7, v3

    if-gez v3, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Input matrix must be positive definite\nMatrix: %s\nInternal Sum: %f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v11, v3, v4

    const/4 v4, 0x1

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const-wide/16 v3, 0x0

    :goto_3
    iget-object v5, v10, Lixg;->a:Lixi;

    iget-object v5, v5, Lixi;->d:[D

    aput-wide v3, v5, v9

    :goto_4
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_1

    :cond_5
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    goto :goto_3

    :cond_6
    iget-object v5, v10, Lixg;->a:Lixi;

    iget-object v5, v5, Lixi;->d:[D

    div-double v6, v7, v3

    aput-wide v6, v5, v9

    goto :goto_4

    :cond_7
    iget v3, v11, Lixh;->a:I

    add-int/2addr v2, v3

    iget v3, v11, Lixh;->a:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    goto :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p1

    iget v2, v0, Lixh;->a:I

    move-object/from16 v0, p1

    iget v3, v0, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->e:Lixg;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->c:Lixi;

    invoke-virtual {v1, v2}, Lixg;->a(Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->a:Lixh;

    iget v2, v2, Lixh;->a:I

    move-object/from16 v0, p1

    iget v3, v0, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p2

    invoke-static {v0, v1, v2}, Lixj;->c(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->a:Lixh;

    iget v2, v2, Lixh;->a:I

    move-object/from16 v0, p1

    iget v3, v0, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->b:Lixh;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->c:Lixi;

    invoke-static {v1, v2, v3}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p1

    iget v2, v0, Lixh;->a:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->a:Lixh;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p2

    invoke-static {v0, v1, v2}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2}, Lixj;->f(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->a:Lixh;

    invoke-static {v1, v2, v3}, Lixj;->b(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->a:Lixh;

    iget v2, v2, Lixh;->a:I

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->a:Lixh;

    iget v3, v3, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p2

    invoke-static {v1, v0, v2}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->a:Lixh;

    iget v2, v2, Lixh;->a:I

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->a:Lixh;

    iget v3, v3, Lixh;->a:I

    invoke-virtual {v1, v2, v3}, Lixi;->b(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->d:Lixi;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->b:Lixh;

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->c:Lixi;

    invoke-static {v1, v2, v3}, Lixj;->a(Lixh;Lixh;Lixh;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Liwj;->b:Lixh;

    move-object/from16 v0, p0

    iget-object v2, v0, Liwj;->c:Lixi;

    move-object/from16 v0, p0

    iget-object v3, v0, Liwj;->b:Lixh;

    invoke-static {v1, v2, v3}, Lixj;->f(Lixh;Lixh;Lixh;)V

    return-void
.end method
