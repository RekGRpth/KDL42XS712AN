.class final Lhvj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lilp;

.field final synthetic b:Lhvb;

.field private final c:Landroid/app/PendingIntent;

.field private final d:Landroid/app/AlarmManager;

.field private final e:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lhvb;Landroid/content/Context;Landroid/app/AlarmManager;Landroid/os/Looper;)V
    .locals 5

    const/4 v4, 0x1

    iput-object p1, p0, Lhvj;->b:Lhvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lhvk;

    invoke-direct {v1, p0, p4, p1}, Lhvk;-><init>(Lhvj;Landroid/os/Looper;Lhvb;)V

    iput-object p3, p0, Lhvj;->d:Landroid/app/AlarmManager;

    new-instance v0, Lhvl;

    invoke-direct {v0, p0, p1}, Lhvl;-><init>(Lhvj;Lhvb;)V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.google.android.gms.flp.EXPIRATION_ALARM"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p2, v0, v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    const/4 v0, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.flp.EXPIRATION_ALARM"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {p2, v0, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lhvj;->c:Landroid/app/PendingIntent;

    invoke-static {p1}, Lhvb;->i(Lhvb;)Landroid/content/Context;

    move-result-object v0

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v2, "GCoreFlp"

    invoke-virtual {v0, v4, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lhvj;->e:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lhvj;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    new-instance v0, Lilp;

    iget-object v2, p0, Lhvj;->e:Landroid/os/PowerManager$WakeLock;

    invoke-direct {v0, v2, v1}, Lilp;-><init>(Landroid/os/PowerManager$WakeLock;Landroid/os/Handler;)V

    iput-object v0, p0, Lhvj;->a:Lilp;

    return-void
.end method

.method static synthetic a(Lhvj;)V
    .locals 11

    const-wide v3, 0x7fffffffffffffffL

    const-wide/16 v9, 0x64

    const/4 v8, 0x3

    iget-object v0, p0, Lhvj;->b:Lhvb;

    invoke-static {v0}, Lhvb;->c(Lhvb;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v1, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvh;

    iget-object v0, v0, Lhvh;->b:Lhwl;

    iget-object v0, v0, Lhwl;->a:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->e()J

    move-result-wide v5

    cmp-long v0, v5, v1

    if-gez v0, :cond_3

    move-wide v0, v5

    :goto_1
    move-wide v1, v0

    goto :goto_0

    :cond_0
    cmp-long v0, v1, v3

    if-gez v0, :cond_2

    const/16 v0, 0x13

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvj;->d:Landroid/app/AlarmManager;

    add-long/2addr v1, v9

    iget-object v3, p0, Lhvj;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lhvj;->d:Landroid/app/AlarmManager;

    add-long/2addr v1, v9

    iget-object v3, p0, Lhvj;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lhvj;->d:Landroid/app/AlarmManager;

    iget-object v1, p0, Lhvj;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_2

    :cond_3
    move-wide v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Leqc;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lhvj;->a:Lilp;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, p1}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lhwl;Landroid/app/PendingIntent;IZ)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lhvi;

    invoke-direct {v0, p1, p4, p2, p3}, Lhvi;-><init>(Lhwl;ZLandroid/app/PendingIntent;I)V

    iget-object v1, p0, Lhvj;->a:Lilp;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3, v3, v0}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lhwl;Leqc;ZILjava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lhvi;

    move-object v1, p1

    move v2, p3

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lhvi;-><init>(Lhwl;ZLeqc;ILjava/lang/String;)V

    iget-object v1, p0, Lhvj;->a:Lilp;

    invoke-virtual {v1, v6, v6, v6, v0}, Lilp;->a(IIILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 3

    iget-object v0, p0, Lhvj;->a:Lilp;

    iget-object v1, v0, Lilp;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v1, v0, Lilp;->a:Landroid/os/Handler;

    new-instance v2, Lilr;

    invoke-direct {v2, v0, p1}, Lilr;-><init>(Lilp;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
