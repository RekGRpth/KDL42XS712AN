.class public final Lept;
.super Lepp;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lepp;-><init>()V

    iput-object p1, p0, Lept;->a:Landroid/content/Context;

    iput-object p2, p0, Lept;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lept;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lept;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Landroid/os/Bundle;Lepl;)V
    .locals 3

    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p1, v0, p0}, Lepl;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "AddressService"

    const-string v2, "Exception getting requestUserAddress intent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic b(Lept;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lept;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lepl;Lcom/google/android/gms/identity/intents/UserAddressRequest;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Lgru;

    iget-object v0, p0, Lept;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lgru;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const-string v2, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    :cond_0
    new-instance v2, Lepu;

    invoke-direct {v2, p0, p3, p2, p1}, Lepu;-><init>(Lept;Landroid/os/Bundle;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lepl;)V

    if-nez v0, :cond_3

    iget-object v0, v1, Lgru;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v3, v0

    if-nez v3, :cond_2

    :cond_1
    new-array v3, v4, [Lipv;

    invoke-interface {v2, v3}, Lgrw;->a([Lipv;)V

    :cond_2
    aget-object v0, v0, v4

    invoke-virtual {v1, v0, p2, v2}, Lgru;->a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lgrw;)V

    :goto_0
    return-void

    :cond_3
    invoke-virtual {v1, v0, p2, v2}, Lgru;->a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lgrw;)V

    goto :goto_0
.end method
