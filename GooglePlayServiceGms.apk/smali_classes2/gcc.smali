.class public final Lgcc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lftb;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lftb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcc;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lgcc;->c:I

    iput-object p3, p0, Lgcc;->d:Ljava/lang/String;

    iput-object p4, p0, Lgcc;->e:Landroid/net/Uri;

    iput-object p5, p0, Lgcc;->f:Ljava/lang/String;

    iput-object p6, p0, Lgcc;->g:Ljava/lang/String;

    iput-object p7, p0, Lgcc;->h:Ljava/lang/String;

    iput-object p8, p0, Lgcc;->b:Lftb;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x0

    const/4 v9, 0x0

    :try_start_0
    iget-object v2, p0, Lgcc;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v3, p0, Lgcc;->c:I

    iget-object v4, p0, Lgcc;->d:Ljava/lang/String;

    iget-object v5, p0, Lgcc;->e:Landroid/net/Uri;

    iget-object v6, p0, Lgcc;->f:Ljava/lang/String;

    iget-object v7, p0, Lgcc;->g:Ljava/lang/String;

    iget-object v8, p0, Lgcc;->h:Ljava/lang/String;

    iget-object v0, p2, Lfrx;->c:Lfsj;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lfsj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-result-object v0

    iget-object v1, p0, Lgcc;->b:Lftb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcc;->b:Lftb;

    invoke-interface {v0, v11, v1, v9}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcc;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcc;->b:Lftb;

    invoke-interface {v1, v11, v0, v9}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcc;->b:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v9, v9}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgcc;->b:Lftb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcc;->b:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    :cond_0
    return-void
.end method
