.class public final Ljaw;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljbg;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Linv;

.field public f:Ljava/lang/String;

.field public g:Lipv;

.field public h:[Ljava/lang/String;

.field public i:Z

.field public j:J

.field public k:[I

.field public l:Z

.field public m:Ljbf;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v2, p0, Ljaw;->a:Ljbg;

    const-string v0, ""

    iput-object v0, p0, Ljaw;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljaw;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljaw;->d:Ljava/lang/String;

    iput-object v2, p0, Ljaw;->e:Linv;

    const-string v0, ""

    iput-object v0, p0, Ljaw;->f:Ljava/lang/String;

    iput-object v2, p0, Ljaw;->g:Lipv;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljaw;->h:[Ljava/lang/String;

    iput-boolean v3, p0, Ljaw;->i:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljaw;->j:J

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Ljaw;->k:[I

    iput-boolean v3, p0, Ljaw;->l:Z

    iput-object v2, p0, Ljaw;->m:Ljbf;

    const/4 v0, -0x1

    iput v0, p0, Ljaw;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v2, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ljaw;->a:Ljbg;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v3, p0, Ljaw;->a:Ljbg;

    invoke-static {v1, v3}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljaw;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v3, p0, Ljaw;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljaw;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v3, p0, Ljaw;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljaw;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    iget-object v3, p0, Ljaw;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljaw;->e:Linv;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v3, p0, Ljaw;->e:Linv;

    invoke-static {v1, v3}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ljaw;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v3, p0, Ljaw;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Ljaw;->g:Lipv;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v3, p0, Ljaw;->g:Lipv;

    invoke-static {v1, v3}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Ljaw;->h:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Ljaw;->h:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    iget-object v5, p0, Ljaw;->h:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    iget-object v5, p0, Ljaw;->h:[Ljava/lang/String;

    aget-object v5, v5, v1

    if-eqz v5, :cond_7

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lizn;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_8
    add-int/2addr v0, v3

    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Ljaw;->i:Z

    if-eqz v1, :cond_a

    const/16 v1, 0x9

    iget-boolean v3, p0, Ljaw;->i:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_a
    iget-wide v3, p0, Ljaw;->j:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_b

    const/16 v1, 0xa

    iget-wide v3, p0, Ljaw;->j:J

    invoke-static {v1, v3, v4}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Ljaw;->k:[I

    if-eqz v1, :cond_d

    iget-object v1, p0, Ljaw;->k:[I

    array-length v1, v1

    if-lez v1, :cond_d

    move v1, v2

    :goto_1
    iget-object v3, p0, Ljaw;->k:[I

    array-length v3, v3

    if-ge v2, v3, :cond_c

    iget-object v3, p0, Ljaw;->k:[I

    aget v3, v3, v2

    invoke-static {v3}, Lizn;->a(I)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_c
    add-int/2addr v0, v1

    iget-object v1, p0, Ljaw;->k:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_d
    iget-boolean v1, p0, Ljaw;->l:Z

    if-eqz v1, :cond_e

    const/16 v1, 0xc

    iget-boolean v2, p0, Ljaw;->l:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Ljaw;->m:Ljbf;

    if-eqz v1, :cond_f

    const/16 v1, 0xd

    iget-object v2, p0, Ljaw;->m:Ljbf;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Ljaw;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljaw;->a:Ljbg;

    if-nez v0, :cond_1

    new-instance v0, Ljbg;

    invoke-direct {v0}, Ljbg;-><init>()V

    iput-object v0, p0, Ljaw;->a:Ljbg;

    :cond_1
    iget-object v0, p0, Ljaw;->a:Ljbg;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljaw;->e:Linv;

    if-nez v0, :cond_2

    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    iput-object v0, p0, Ljaw;->e:Linv;

    :cond_2
    iget-object v0, p0, Ljaw;->e:Linv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljaw;->g:Lipv;

    if-nez v0, :cond_3

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    iput-object v0, p0, Ljaw;->g:Lipv;

    :cond_3
    iget-object v0, p0, Ljaw;->g:Lipv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljaw;->h:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljaw;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ljaw;->h:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljaw;->h:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljaw;->i:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Ljaw;->j:J

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x58

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_8

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lizm;->a()I

    :cond_7
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_8
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljaw;->k:[I

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    if-nez v0, :cond_a

    array-length v3, v5

    if-ne v2, v3, :cond_a

    iput-object v5, p0, Ljaw;->k:[I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Ljaw;->k:[I

    array-length v0, v0

    goto :goto_5

    :cond_a
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_b

    iget-object v4, p0, Ljaw;->k:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljaw;->k:[I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_c

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    if-eqz v0, :cond_10

    invoke-virtual {p1, v2}, Lizm;->e(I)V

    iget-object v2, p0, Ljaw;->k:[I

    if-nez v2, :cond_e

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_d

    iget-object v0, p0, Ljaw;->k:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_8
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_f

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_e
    iget-object v2, p0, Ljaw;->k:[I

    array-length v2, v2

    goto :goto_7

    :cond_f
    iput-object v4, p0, Ljaw;->k:[I

    :cond_10
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljaw;->l:Z

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ljaw;->m:Ljbf;

    if-nez v0, :cond_11

    new-instance v0, Ljbf;

    invoke-direct {v0}, Ljbf;-><init>()V

    iput-object v0, p0, Ljaw;->m:Ljbf;

    :cond_11
    iget-object v0, p0, Ljaw;->m:Ljbf;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
        0x6a -> :sswitch_e
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Ljaw;->a:Ljbg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Ljaw;->a:Ljbg;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Ljaw;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Ljaw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljaw;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v2, p0, Ljaw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Ljaw;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    iget-object v2, p0, Ljaw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljaw;->e:Linv;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v2, p0, Ljaw;->e:Linv;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_4
    iget-object v0, p0, Ljaw;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    iget-object v2, p0, Ljaw;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Ljaw;->g:Lipv;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v2, p0, Ljaw;->g:Lipv;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_6
    iget-object v0, p0, Ljaw;->h:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljaw;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljaw;->h:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ljaw;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    iget-boolean v0, p0, Ljaw;->i:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    iget-boolean v2, p0, Ljaw;->i:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_9
    iget-wide v2, p0, Ljaw;->j:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_a

    const/16 v0, 0xa

    iget-wide v2, p0, Ljaw;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    :cond_a
    iget-object v0, p0, Ljaw;->k:[I

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljaw;->k:[I

    array-length v0, v0

    if-lez v0, :cond_b

    :goto_1
    iget-object v0, p0, Ljaw;->k:[I

    array-length v0, v0

    if-ge v1, v0, :cond_b

    const/16 v0, 0xb

    iget-object v2, p0, Ljaw;->k:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_b
    iget-boolean v0, p0, Ljaw;->l:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xc

    iget-boolean v1, p0, Ljaw;->l:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_c
    iget-object v0, p0, Ljaw;->m:Ljbf;

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget-object v1, p0, Ljaw;->m:Ljbf;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_d
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljaw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljaw;

    iget-object v2, p0, Ljaw;->a:Ljbg;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljaw;->a:Ljbg;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljaw;->a:Ljbg;

    iget-object v3, p1, Ljaw;->a:Ljbg;

    invoke-virtual {v2, v3}, Ljbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljaw;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Ljaw;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljaw;->b:Ljava/lang/String;

    iget-object v3, p1, Ljaw;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljaw;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Ljaw;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljaw;->c:Ljava/lang/String;

    iget-object v3, p1, Ljaw;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljaw;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Ljaw;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljaw;->d:Ljava/lang/String;

    iget-object v3, p1, Ljaw;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljaw;->e:Linv;

    if-nez v2, :cond_b

    iget-object v2, p1, Ljaw;->e:Linv;

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ljaw;->e:Linv;

    iget-object v3, p1, Ljaw;->e:Linv;

    invoke-virtual {v2, v3}, Linv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Ljaw;->f:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Ljaw;->f:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Ljaw;->f:Ljava/lang/String;

    iget-object v3, p1, Ljaw;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Ljaw;->g:Lipv;

    if-nez v2, :cond_f

    iget-object v2, p1, Ljaw;->g:Lipv;

    if-eqz v2, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Ljaw;->g:Lipv;

    iget-object v3, p1, Ljaw;->g:Lipv;

    invoke-virtual {v2, v3}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Ljaw;->h:[Ljava/lang/String;

    iget-object v3, p1, Ljaw;->h:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-boolean v2, p0, Ljaw;->i:Z

    iget-boolean v3, p1, Ljaw;->i:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-wide v2, p0, Ljaw;->j:J

    iget-wide v4, p1, Ljaw;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Ljaw;->k:[I

    iget-object v3, p1, Ljaw;->k:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-boolean v2, p0, Ljaw;->l:Z

    iget-boolean v3, p1, Ljaw;->l:Z

    if-eq v2, v3, :cond_15

    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget-object v2, p0, Ljaw;->m:Ljbf;

    if-nez v2, :cond_16

    iget-object v2, p1, Ljaw;->m:Ljbf;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v2, p0, Ljaw;->m:Ljbf;

    iget-object v3, p1, Ljaw;->m:Ljbf;

    invoke-virtual {v2, v3}, Ljbf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Ljaw;->a:Ljbg;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->e:Linv;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljaw;->g:Lipv;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljaw;->h:[Ljava/lang/String;

    invoke-static {v4}, Lizq;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ljaw;->i:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Ljaw;->j:J

    iget-wide v6, p0, Ljaw;->j:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljaw;->k:[I

    invoke-static {v4}, Lizq;->a([I)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Ljaw;->l:Z

    if-eqz v4, :cond_8

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljaw;->m:Ljbf;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljaw;->a:Ljbg;

    invoke-virtual {v0}, Ljbg;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljaw;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljaw;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljaw;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Ljaw;->e:Linv;

    invoke-virtual {v0}, Linv;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljaw;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v0, p0, Ljaw;->g:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_7
    move v0, v3

    goto :goto_7

    :cond_8
    move v2, v3

    goto :goto_8

    :cond_9
    iget-object v1, p0, Ljaw;->m:Ljbf;

    invoke-virtual {v1}, Ljbf;->hashCode()I

    move-result v1

    goto :goto_9
.end method
