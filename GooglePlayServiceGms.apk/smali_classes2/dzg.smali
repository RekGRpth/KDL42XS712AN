.class public Ldzg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbdx;
.implements Lbdy;


# instance fields
.field private final a:I

.field public b:Ldvn;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iput p1, p0, Ldzg;->a:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Ldzg;->b:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->a(Lbdx;)V

    iget-object v0, p0, Ldzg;->b:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->a(Lbdy;)V

    return-void
.end method

.method protected final S()Lbdu;
    .locals 1

    iget-object v0, p0, Ldzg;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->j()Lbdu;

    move-result-object v0

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    iget v0, p0, Ldzg;->a:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public a(Lbdu;)V
    .locals 0

    return-void
.end method

.method public final c(I)V
    .locals 2

    const-string v0, "GamesFragment"

    const-string v1, "Unexpected call to onConnectionSuspended - subclasses should unregister as a listener in onStop() and clear data in onDestroyView()"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldvn;

    iput-object v0, p0, Ldzg;->b:Ldvn;

    return-void
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Ldzg;->b:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->b(Lbdx;)V

    iget-object v0, p0, Ldzg;->b:Ldvn;

    invoke-virtual {v0, p0}, Ldvn;->b(Lbdy;)V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g_()V

    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Ldzg;->S()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-virtual {p0, v0}, Ldzg;->a(Lbdu;)V

    return-void
.end method
