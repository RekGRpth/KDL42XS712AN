.class public final enum Lcfb;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcfb;

.field private static final synthetic c:[Lcfb;


# instance fields
.field private final b:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    new-instance v0, Lcfb;

    const-string v1, "NULL_HOLDER"

    invoke-static {}, Lcfa;->d()Lcfa;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "nullHolder"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcfb;-><init>(Ljava/lang/String;Lcdq;)V

    sput-object v0, Lcfb;->a:Lcfb;

    new-array v0, v6, [Lcfb;

    const/4 v1, 0x0

    sget-object v2, Lcfb;->a:Lcfb;

    aput-object v2, v0, v1

    sput-object v0, Lcfb;->c:[Lcfb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcdq;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcfb;->b:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcfb;
    .locals 1

    const-class v0, Lcfb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcfb;

    return-object v0
.end method

.method public static values()[Lcfb;
    .locals 1

    sget-object v0, Lcfb;->c:[Lcfb;

    invoke-virtual {v0}, [Lcfb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcfb;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcfb;->b:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcfb;->b:Lcdp;

    return-object v0
.end method
