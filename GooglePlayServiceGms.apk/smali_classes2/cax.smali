.class public final enum Lcax;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcax;

.field public static final enum b:Lcax;

.field public static final enum c:Lcax;

.field public static final enum d:Lcax;

.field public static final enum e:Lcax;

.field public static final enum f:Lcax;

.field public static final enum g:Lcax;

.field private static final synthetic l:[Lcax;


# instance fields
.field private final h:Lcar;

.field private final i:Lcar;

.field private final j:Lcar;

.field private final k:Lcar;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcax;

    const-string v1, "TODAY"

    const v2, 0x7f0b0081    # com.google.android.gms.R.string.drive_time_range_today

    invoke-direct {v0, v1, v4, v2}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->a:Lcax;

    new-instance v0, Lcax;

    const-string v1, "YESTERDAY"

    const v2, 0x7f0b0082    # com.google.android.gms.R.string.drive_time_range_yesterday

    invoke-direct {v0, v1, v5, v2}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->b:Lcax;

    new-instance v0, Lcax;

    const-string v1, "THIS_WEEK"

    const v2, 0x7f0b007f    # com.google.android.gms.R.string.drive_time_range_this_week

    invoke-direct {v0, v1, v6, v2}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->c:Lcax;

    new-instance v0, Lcax;

    const-string v1, "THIS_MONTH"

    const v2, 0x7f0b007e    # com.google.android.gms.R.string.drive_time_range_this_month

    invoke-direct {v0, v1, v7, v2}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->d:Lcax;

    new-instance v0, Lcax;

    const-string v1, "THIS_YEAR"

    const v2, 0x7f0b0080    # com.google.android.gms.R.string.drive_time_range_this_year

    invoke-direct {v0, v1, v8, v2}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->e:Lcax;

    new-instance v0, Lcax;

    const-string v1, "LAST_YEAR"

    const/4 v2, 0x5

    const v3, 0x7f0b007c    # com.google.android.gms.R.string.drive_time_range_last_year

    invoke-direct {v0, v1, v2, v3}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->f:Lcax;

    new-instance v0, Lcax;

    const-string v1, "OLDER"

    const/4 v2, 0x6

    const v3, 0x7f0b007d    # com.google.android.gms.R.string.drive_time_range_older

    invoke-direct {v0, v1, v2, v3}, Lcax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcax;->g:Lcax;

    const/4 v0, 0x7

    new-array v0, v0, [Lcax;

    sget-object v1, Lcax;->a:Lcax;

    aput-object v1, v0, v4

    sget-object v1, Lcax;->b:Lcax;

    aput-object v1, v0, v5

    sget-object v1, Lcax;->c:Lcax;

    aput-object v1, v0, v6

    sget-object v1, Lcax;->d:Lcax;

    aput-object v1, v0, v7

    sget-object v1, Lcax;->e:Lcax;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcax;->f:Lcax;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcax;->g:Lcax;

    aput-object v2, v0, v1

    sput-object v0, Lcax;->l:[Lcax;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v2, v1}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcax;->h:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v1, v1}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcax;->i:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v1, v2}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcax;->j:Lcar;

    new-instance v0, Lcas;

    invoke-direct {v0, p3, v2, v2}, Lcas;-><init>(IZZ)V

    iput-object v0, p0, Lcax;->k:Lcar;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcax;
    .locals 1

    const-class v0, Lcax;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcax;

    return-object v0
.end method

.method public static values()[Lcax;
    .locals 1

    sget-object v0, Lcax;->l:[Lcax;

    invoke-virtual {v0}, [Lcax;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcax;

    return-object v0
.end method


# virtual methods
.method public final a(ZZ)Lcar;
    .locals 1

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcax;->i:Lcar;

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    iget-object v0, p0, Lcax;->h:Lcar;

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcax;->j:Lcar;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcax;->k:Lcar;

    goto :goto_0
.end method
