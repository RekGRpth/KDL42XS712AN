.class public final Lfem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:Z

.field private e:F

.field private f:F

.field private g:F

.field private h:J

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfem;->a:Lcom/google/android/gms/people/profile/AvatarView;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfem;->i:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfem;->j:Z

    return-void
.end method

.method public final a(FFFF)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lfem;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iput p3, p0, Lfem;->b:F

    iput p4, p0, Lfem;->c:F

    iput p2, p0, Lfem;->e:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lfem;->h:J

    iput p1, p0, Lfem;->f:F

    iget v0, p0, Lfem;->e:F

    iget v3, p0, Lfem;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lfem;->d:Z

    iget v0, p0, Lfem;->e:F

    iget v3, p0, Lfem;->f:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x43960000    # 300.0f

    div-float/2addr v0, v3

    iput v0, p0, Lfem;->g:F

    iput-boolean v1, p0, Lfem;->i:Z

    iput-boolean v2, p0, Lfem;->j:Z

    iget-object v0, p0, Lfem;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final run()V
    .locals 4

    iget-boolean v0, p0, Lfem;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lfem;->h:J

    sub-long/2addr v0, v2

    iget v2, p0, Lfem;->f:F

    iget v3, p0, Lfem;->g:F

    long-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    iget-object v1, p0, Lfem;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v2, p0, Lfem;->b:F

    iget v3, p0, Lfem;->c:F

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V

    iget v1, p0, Lfem;->e:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lfem;->d:Z

    iget v2, p0, Lfem;->e:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-ne v1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lfem;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v1, p0, Lfem;->e:F

    iget v2, p0, Lfem;->b:F

    iget v3, p0, Lfem;->c:F

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V

    invoke-virtual {p0}, Lfem;->a()V

    :cond_3
    iget-boolean v0, p0, Lfem;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfem;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
