.class public final Lhnc;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/List;I)Ljava/util/List;
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-ge p1, v0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x2

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 19

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v2, 0x1

    if-ne v5, v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lhnc;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lhnc;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lhnc;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lhnc;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    div-int/lit8 v8, v5, 0x2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v8, :cond_1

    const-wide v10, -0x3fe6de04abbbd2e8L    # -6.283185307179586

    int-to-double v12, v4

    mul-double/2addr v10, v12

    int-to-double v12, v5

    div-double/2addr v10, v12

    new-instance v12, Lhnd;

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    invoke-direct {v12, v13, v14, v10, v11}, Lhnd;-><init>(DD)V

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhnd;

    iget-wide v10, v12, Lhnd;->a:D

    iget-wide v13, v2, Lhnd;->a:D

    mul-double/2addr v10, v13

    iget-wide v13, v12, Lhnd;->b:D

    iget-wide v15, v2, Lhnd;->b:D

    mul-double/2addr v13, v15

    sub-double/2addr v10, v13

    iget-wide v13, v12, Lhnd;->a:D

    iget-wide v15, v2, Lhnd;->b:D

    mul-double/2addr v13, v15

    iget-wide v15, v12, Lhnd;->b:D

    iget-wide v0, v2, Lhnd;->a:D

    move-wide/from16 v17, v0

    mul-double v15, v15, v17

    add-double v12, v13, v15

    new-instance v14, Lhnd;

    invoke-direct {v14, v10, v11, v12, v13}, Lhnd;-><init>(DD)V

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhnd;

    new-instance v10, Lhnd;

    iget-wide v11, v2, Lhnd;->a:D

    iget-wide v15, v14, Lhnd;->a:D

    add-double/2addr v11, v15

    iget-wide v15, v2, Lhnd;->b:D

    iget-wide v0, v14, Lhnd;->b:D

    move-wide/from16 v17, v0

    add-double v15, v15, v17

    move-wide v0, v15

    invoke-direct {v10, v11, v12, v0, v1}, Lhnd;-><init>(DD)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v10, Lhnd;

    iget-wide v11, v2, Lhnd;->a:D

    iget-wide v15, v14, Lhnd;->a:D

    sub-double/2addr v11, v15

    iget-wide v15, v2, Lhnd;->b:D

    iget-wide v13, v14, Lhnd;->b:D

    sub-double v13, v15, v13

    invoke-direct {v10, v11, v12, v13, v14}, Lhnd;-><init>(DD)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_1
    invoke-interface {v3, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v2, v3

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must be a power of 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    new-instance v0, Lhnd;

    const-wide/16 v5, 0x0

    invoke-direct {v0, v3, v4, v5, v6}, Lhnd;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lhnc;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
