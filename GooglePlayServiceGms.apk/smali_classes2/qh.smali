.class public Lqh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lps;


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lqj;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lqh;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lqh;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lqh;->c:Ljava/lang/String;

    new-instance v0, Lqj;

    invoke-direct {v0, v1, v1, v1}, Lqj;-><init>(Ljava/lang/String;Ljava/lang/String;Lqw;)V

    iput-object v0, p0, Lqh;->b:Lqj;

    return-void
.end method

.method private constructor <init>(Lqj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lqh;->c:Ljava/lang/String;

    iput-object p1, p0, Lqh;->b:Lqj;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lqc;->b(Ljava/lang/String;)V

    invoke-static {p2}, Lqc;->a(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lqr;->a(Ljava/lang/String;Ljava/lang/String;)Lqq;

    move-result-object v1

    iget-object v2, p0, Lqh;->b:Lqj;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3, v0}, Lql;->a(Lqj;Lqq;ZLqw;)Lqj;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz p3, :cond_0

    invoke-virtual {v2}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lpr;

    const-string v1, "Property must be simple when a value type is requested"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    packed-switch p3, :pswitch_data_0

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move-object v0, v1

    :cond_2
    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Ljava/lang/Boolean;

    invoke-static {v1}, Lpv;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/lang/Integer;

    invoke-static {v1}, Lpv;->b(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Ljava/lang/Long;

    invoke-static {v1}, Lpv;->c(Ljava/lang/String;)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Ljava/lang/Double;

    invoke-static {v1}, Lpv;->d(Ljava/lang/String;)D

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    :pswitch_4
    invoke-static {v1}, Lpv;->e(Ljava/lang/String;)Lpq;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-static {v1}, Lpv;->e(Ljava/lang/String;)Lpq;

    move-result-object v0

    invoke-interface {v0}, Lpq;->i()Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-static {v1}, Lpv;->f(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a()Lqj;
    .locals 1

    iget-object v0, p0, Lqh;->b:Lqj;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lqh;->c:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    invoke-static {p1}, Lqc;->b(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lpr;

    const-string v1, "Empty array name"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Lpr;

    const-string v1, "Empty specific language"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_3
    if-eqz p3, :cond_4

    invoke-static {p3}, Lqf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    invoke-static {p4}, Lqf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, p2}, Lqr;->a(Ljava/lang/String;Ljava/lang/String;)Lqq;

    move-result-object v0

    iget-object v2, p0, Lqh;->b:Lqj;

    const/4 v3, 0x1

    new-instance v5, Lqw;

    const/16 v6, 0x1e00

    invoke-direct {v5, v6}, Lqw;-><init>(I)V

    invoke-static {v2, v0, v3, v5}, Lql;->a(Lqj;Lqq;ZLqw;)Lqj;

    move-result-object v5

    if-nez v5, :cond_5

    new-instance v0, Lpr;

    const-string v1, "Failed to find or create array node"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_5
    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->k()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v5}, Lqj;->f()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v5}, Lqj;->l()Lqw;

    move-result-object v0

    invoke-virtual {v0}, Lqw;->l()Lqw;

    :cond_6
    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {v5}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    invoke-virtual {v0}, Lqj;->h()Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "xml:lang"

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lqj;->c(I)Lqj;

    move-result-object v8

    invoke-virtual {v8}, Lqj;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    :cond_8
    new-instance v0, Lpr;

    const-string v1, "Language qualifier must be first"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_9
    new-instance v0, Lpr;

    const-string v1, "Specified property is no alt-text array"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_a
    const-string v7, "x-default"

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lqj;->c(I)Lqj;

    move-result-object v8

    invoke-virtual {v8}, Lqj;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v2, 0x1

    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_b

    invoke-virtual {v5}, Lqj;->c()I

    move-result v0

    const/4 v6, 0x1

    if-le v0, v6, :cond_b

    invoke-virtual {v5, v3}, Lqj;->c(Lqj;)V

    invoke-virtual {v5, v3}, Lqj;->b(Lqj;)V

    :cond_b
    invoke-static {v5, v1, v4}, Lql;->b(Lqj;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    aget-object v0, v1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v0, 0x1

    aget-object v0, v1, v0

    check-cast v0, Lqj;

    const-string v1, "x-default"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    packed-switch v6, :pswitch_data_0

    new-instance v0, Lpr;

    const-string v1, "Unexpected result from ChooseLocalizedText"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lpr;-><init>(Ljava/lang/String;I)V

    throw v0

    :pswitch_0
    const-string v0, "x-default"

    invoke-static {v5, v0, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-nez v1, :cond_c

    invoke-static {v5, v4, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_2
    if-nez v0, :cond_d

    invoke-virtual {v5}, Lqj;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    const-string v0, "x-default"

    invoke-static {v5, v0, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    return-void

    :pswitch_1
    if-nez v1, :cond_f

    if-eqz v2, :cond_e

    if-eq v3, v0, :cond_e

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lqj;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v3, p5}, Lqj;->d(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v0, p5}, Lqj;->d(Ljava/lang/String;)V

    move v0, v2

    goto :goto_2

    :cond_f
    sget-boolean v1, Lqh;->a:Z

    if-nez v1, :cond_11

    if-eqz v2, :cond_10

    if-eq v3, v0, :cond_11

    :cond_10
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_11
    invoke-virtual {v5}, Lqj;->g()Ljava/util/Iterator;

    move-result-object v4

    :cond_12
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    if-eq v0, v3, :cond_12

    invoke-virtual {v0}, Lqj;->k()Ljava/lang/String;

    move-result-object v6

    if-eqz v3, :cond_13

    invoke-virtual {v3}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0, p5}, Lqj;->d(Ljava/lang/String;)V

    goto :goto_3

    :cond_13
    const/4 v1, 0x0

    goto :goto_4

    :cond_14
    if-eqz v3, :cond_17

    invoke-virtual {v3, p5}, Lqj;->d(Ljava/lang/String;)V

    move v0, v2

    goto :goto_2

    :pswitch_2
    if-eqz v2, :cond_15

    if-eq v3, v0, :cond_15

    if-eqz v3, :cond_15

    invoke-virtual {v3}, Lqj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lqj;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {v3, p5}, Lqj;->d(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {v0, p5}, Lqj;->d(Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_2

    :pswitch_3
    invoke-static {v5, v4, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_17

    const/4 v0, 0x1

    goto/16 :goto_2

    :pswitch_4
    if-eqz v3, :cond_16

    invoke-virtual {v5}, Lqj;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_16

    invoke-virtual {v3, p5}, Lqj;->d(Ljava/lang/String;)V

    :cond_16
    invoke-static {v5, v4, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_2

    :pswitch_5
    invoke-static {v5, v4, p5}, Lql;->a(Lqj;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_17

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_17
    move v0, v2

    goto/16 :goto_2

    :cond_18
    move-object v9, v2

    move v2, v3

    move-object v3, v9

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1}, Lqc;->b(Ljava/lang/String;)V

    invoke-static {p2}, Lqc;->a(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lqr;->a(Ljava/lang/String;Ljava/lang/String;)Lqq;

    move-result-object v1

    iget-object v2, p0, Lqh;->b:Lqj;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Lql;->a(Lqj;Lqq;ZLqw;)Lqj;
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lqh;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, v0}, Lqh;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lqh;->b:Lqj;

    invoke-virtual {v0}, Lqj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqj;

    new-instance v1, Lqh;

    invoke-direct {v1, v0}, Lqh;-><init>(Lqj;)V

    return-object v1
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Double;
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lqh;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    return-object v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 1

    const/4 v0, 0x6

    invoke-direct {p0, p1, p2, v0}, Lqh;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    return-object v0
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lqh;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
