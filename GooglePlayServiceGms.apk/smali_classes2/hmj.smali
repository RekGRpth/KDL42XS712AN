.class final Lhmj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 12

    const-wide v10, 0x3fc999999999999aL    # 0.2

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const/4 v7, 0x0

    const-wide v5, 0x3fc3333333333333L    # 0.15

    const/16 v4, 0x64

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fee64b2314013ecL    # 0.949792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9295a6c5d206c8L    # 0.018149

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd27e8ccdd93c47L    # 0.288974

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40247eff5c6c11a1L    # 10.248042

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe7d8dde7a743a6L    # 0.745223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f544028e4fb97bbL    # 0.001236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe399999999999aL    # 0.6125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb2311e85fd04a3L    # 0.071062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024838130164841L    # 10.256845

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_6
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff4bf961804d984L    # 1.296774

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd2a6266fd651b1L    # 0.291391

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b43526527a205L    # 1.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5522a6f3f52fc2L    # 0.00129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f28611fd5885d31L    # 1.86E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c087442c7fbadL    # 0.001711

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5212513b5bf6a1L    # 0.001103

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c5b445ed4a1bL    # 9.886141

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023d3b4916ca46eL    # 9.913487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f367a95c853c148L    # 3.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f09b0ab2e1693c0L    # 4.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f24940bbb1f255fL    # 1.57E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f50be9424e59296L    # 0.001022

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f31d3671ac14c66L    # 2.72E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f40e8858ff75968L    # 5.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40253e47dc37a3dbL    # 10.621642

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3efe68a0d349be90L    # 2.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f8982cb20fb2L    # 0.156024

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eed5c31593e5fb7L    # 1.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f37225b749adc90L    # 3.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f201f31f46ed246L    # 1.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f092a737110e454L    # 4.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4733226c3b927dL    # 7.08E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4066c2acb85a5L    # 0.156446

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f264840e1719f80L    # 1.7E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ba7fc32ebe597L    # 4.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffdc7db2b346131L    # 1.861293

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f47b95a294141eaL    # 7.24E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f39e30014f8b589L    # 3.95E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb3bb1f255f351aL    # 0.077074

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f41c2a023209678L    # 5.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0d20d130df9beL    # 0.525641

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7572580c308febL    # 0.005236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8eb24a6a875d5L    # 0.778704

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f455d5f56a7ac82L    # 6.52E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd7d2c7b890d5a6L    # 0.37224

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f53404ea4a8c155L    # 0.001175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f41d3671ac14c66L    # 5.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f58266772d5e072L    # 0.001474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f749c6f36ef8056L    # 0.005032

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd614b9cb6848bfL    # 0.345015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdfe679463cfb33L    # 0.498442

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd35bff04577d95L    # 0.30249

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4025f254e6e221c9L    # 10.973304

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f57c1bda5119ce0L    # 0.00145

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ffa82ababead4f6L    # 1.656902

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31b1d92b7fe08bL    # 2.7E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27dae81882adc5L    # 1.82E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f25a07b352a8438L    # 1.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fda0ee06d938152L    # 0.407158

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f0bae89eba6bL    # 0.155784

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f068b5cbff47736L    # 4.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40229216616b54e3L    # 9.285327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd0e38eb0318b93L    # 0.263889

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26052502eec7c9L    # 1.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f543bf727136a40L    # 0.001235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff59b65ecc3e319L    # 1.350439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f535daad601ffb5L    # 0.001182

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f459817b95a2941L    # 6.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f400e6afcce1c58L    # 4.9E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd55bccaf709b74L    # 0.333728

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f31f4f50a02b841L    # 2.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5aef6f8f041462L    # 0.001644

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f456e264e48626fL    # 6.54E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2cf7878b7a1c26L    # 2.21E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f723b3636f3b214L    # 0.004451

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f014d2f5dbb9cfaL    # 3.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe3b33333333333L    # 0.615625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f940d4dc65c7043L    # 0.019582

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3eaab042528aeL    # 0.155599

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f230164840e171aL    # 1.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc407b352a84381L    # 0.156485

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd031593e5fb720L    # 0.253012

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5d19157abb8801L    # 0.001776

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd6d3f9e7b80a9eL    # 0.356688

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f95a63f9a49c2c2L    # 0.021142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f93e10060780fdcL    # 0.019413

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0de26916440f24L    # 5.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff6e529bae46cfdL    # 1.430948

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efe68a0d349be90L    # 2.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0bc98a222d5172L    # 5.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd884fd2a62aa19L    # 0.383117

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa864a9cdc44391L    # 0.047643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff1559a30984e40L    # 1.083399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402158672756861fL    # 8.672662

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c219eb6390c91L    # 0.001717

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5aeb3dd11be6e6L    # 0.001643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4088d6d3b6cbeL    # 0.156511

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc404a72ead9275L    # 0.156392

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f6a0dbad3a60L    # 0.155964

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddedb2f661f18dL    # 0.467633

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a8262456f75daL    # 8.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc6666666666666L    # 0.175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f311ada76d97b31L    # 2.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f081e03f705857bL    # 4.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40235847d3d4280bL    # 9.672423

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86883771865519L    # 0.011002

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fff7a2f05a708eeL    # 1.96733

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa2795703f2d3c8L    # 0.036082

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4008f10a99b6f5cbL    # 3.117696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fba515ce9e5e248L    # 0.102804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40213457e23f24d9L    # 8.602233

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f41cb039ef0f16fL    # 5.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40194203a322af57L    # 6.314467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3a6937d1fe64f5L    # 4.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f47ebaf102363b2L    # 7.3E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400aed4801f75105L    # 3.36586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400bd6976bc1effaL    # 3.479781

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f526e978d4fdf3bL    # 0.001125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe13f2b239a38fcL    # 0.538961

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f8a8f3a9b068L    # 0.156026

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe904a0e410b631L    # 0.781815

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f216ebd4cfd08d5L    # 1.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fad2999567dcL    # 0.156092

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f344028e4fb97bbL    # 3.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdb666666666666L    # 0.428125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4027d03dddb1209fL    # 11.906722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff84bd33d29563bL    # 1.518512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d8e8640208180L    # 4.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402768bac710cb29L    # 11.70455

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc64dc22ab25b31L    # 0.174248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdb8f2170931013L    # 0.430611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f36df3f961804daL    # 3.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40214a54c5543287L    # 8.645178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f745d851654d61bL    # 0.004972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f526202539756c9L    # 0.001122

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe050507a6bd6e9L    # 0.509804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdf35efa615a8dfL    # 0.487667

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fccec8d5c74751dL    # 0.225969

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f627b2cc70867aeL    # 0.002256

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd07da61e0c5a81L    # 0.257669

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3700cd855970b5L    # 3.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f4cb1897a67aL    # 0.155908

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc2c0d6f544bb1bL    # 0.14651

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4e81cb46bacf74L    # 9.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6192641b328b6eL    # 0.002145

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7fd7a13c254a3cL    # 0.007774

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4012d411c2a02321L    # 4.707099

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9eba27ae9ab29eL    # 0.030007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3e03f705857affL    # 4.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021a557ff9b5632L    # 8.822937

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0cd5f99c38b04bL    # 5.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd0da48b6523704L    # 0.263323

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f91b93037d63023L    # 0.017308

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe16bb341e14be0L    # 0.544397

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc408e15011904bL    # 0.156521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f523810e8858ff7L    # 0.001112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f56c61522a6f3f5L    # 0.00139

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c0010c6f7a0b6L    # 0.001709

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd31f36262cba73L    # 0.29878

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbb3eab367a0f91L    # 0.106425

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021388f21709310L    # 8.610467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    iget-wide v0, p1, Lhmx;->f:D

    cmpg-double v0, v0, v8

    if-gtz v0, :cond_ac

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84a3400b88ca3eL    # 0.010077

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdfe9ccb7d41744L    # 0.498645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4024121ab4b72c52L    # 10.03536

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f49008205ff1d82L    # 7.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4009c58cd20afa2fL    # 3.22146

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ca3a4b5568e82L    # 8.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdb25e13b18dac2L    # 0.424187

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dac57e23f24eL    # 2.356822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d14e3bcd35a86L    # 0.00355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddee30caa326e1L    # 0.467663

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4bda5119ce075fL    # 8.5E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5dc50ce4ead0c4L    # 0.001817

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fac8e4755ffe6d6L    # 0.055773

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f66f6512a94ff00L    # 0.002803

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd0d20f2becedd5L    # 0.262821

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba779207d4e098L    # 0.103387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f844a1f080303c0L    # 0.009907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcb03e20ccff21bL    # 0.211056

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f90589acbc8c0cfL    # 0.015963

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f314d2f5dbb9cfaL    # 2.64E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7276fb09203a32L    # 0.004508

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa0f32378ab0c89L    # 0.033105

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd9b024f6598e1L    # 0.231293

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f499780baa582dcL    # 7.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f51276fb09203a3L    # 0.001047

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa11dffc5479d4eL    # 0.033432

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f63ff25e56cd6c3L    # 0.002441

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f24727dcbddb984L    # 1.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f896e9bbf0dc769L    # 0.012418

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f88dd616f86a099L    # 0.012141

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6620685553ef6bL    # 0.002701

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef81e03f705857bL    # 2.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022c21d96e9bbf1L    # 9.379132

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37b95a294141eaL    # 3.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4025cbd0bfa09460L    # 10.898077

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f509d0635a426bbL    # 0.001014

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f229cbab649d389L    # 1.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f48bd66277c45ccL    # 7.55E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f79210385c67dfeL    # 0.006135

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f36ce789e774eecL    # 3.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40006bb44e50c5ebL    # 2.05259

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe18c62e4d1a650L    # 0.548387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f945c358afc47e5L    # 0.019883

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2626b2f23033a4L    # 1.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4cffeb074a771dL    # 8.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffecafe60c38f36L    # 1.924559

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40281d1efb6dca08L    # 12.056877

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef1d3671ac14c66L    # 1.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe25080b673c4f4L    # 0.572327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x400222046c764ae0L    # 2.26661

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022f16228134480L    # 9.471452

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f426202539756c9L    # 5.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdfce5b4245f5aeL    # 0.49697

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa35a63f9a49c2cL    # 0.037799

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd83a2595bbbe88L    # 0.378549

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401226f2e8c0485aL    # 4.538036

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb127c393682731L    # 0.067013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcf0196d8f4f93cL    # 0.242236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a60d4562e09ffL    # 0.00161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023d136e71cda2bL    # 9.908622

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f877318fc504817L    # 0.01145

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd06a012599ed7cL    # 0.25647

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401c1bb7b6bb1290L    # 7.027068

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4000bce63a5c1c61L    # 2.092236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a1986b9c304cdL    # 0.001593

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc0cccccccccccdL    # 0.13125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402862af9a8cdea0L    # 12.192746

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7097c80841ede1L    # 0.004051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbccccccccccccdL    # 0.1125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3fea8112ba16e8L    # 4.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4626b2f23033a4L    # 6.76E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402f191f4f50a02cL    # 15.549067

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc0f0307f23cc8eL    # 0.13233

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe01bdc69f8c21eL    # 0.503401

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8d8b60f1b25f63L    # 0.014426

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40020acc0bdcad15L    # 2.255272

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f4f93bc0a06eaL    # 0.030577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f20a569b17481b2L    # 1.27E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5819d2391d5800L    # 0.001471

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb6ebd4cfd08d4cL    # 0.089536

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbb333333333333L    # 0.10625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6ce8d972cd7cf6L    # 0.003529

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402180574b407033L    # 8.750666

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb0e7ff583a53b9L    # 0.06604

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7f4b1ee2435697L    # 0.00764

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f516659d12caddeL    # 0.001062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4020e29131ec0b56L    # 8.442514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f301f31f46ed246L    # 2.46E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde15fcc1871e6dL    # 0.470092

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa2f31b152f3c2eL    # 0.037011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2fb82c2bd7f51fL    # 2.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fca12f901083dbcL    # 0.203704

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f93a7daa4fca42bL    # 0.019195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbe1e1d2178f68cL    # 0.117647

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4873ffac1d29eL    # 0.320755

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fa76cce5f7403deL    # 0.045752

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40434ce2e2b8c75cL    # 38.600674

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb458255b035bd5L    # 0.07947

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9475a31a4bdba1L    # 0.01998

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402440f217093101L    # 10.126847

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f90c4dec1c1d6d0L    # 0.016376

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f473b85e80bed74L    # 7.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f223810e8858ff7L    # 1.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d7fd82773e250L    # 0.003601

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8ead4f5903a75L    # 0.778666

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f86f0068db8bac7L    # 0.0112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f626a65cf67b1c0L    # 0.002248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e8c47a17f4129L    # 0.003729

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc8acb85a4f00efL    # 0.192771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4711947cfa26a2L    # 7.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3cf7878b7a1c26L    # 4.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffa1eb313be22e6L    # 1.632495

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdc71c53f39d1b3L    # 0.444444

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034b0a7d6733ebcL    # 20.690061

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f64d940789613d3L    # 0.002545

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020ac286f8ad256L    # 8.336246

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fa4e5d5b24e9c45L    # 0.040816

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021838c111ada77L    # 8.756928

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9e6fb4c3c18b50L    # 0.029723

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b7e0ac7da1ec5L    # 8.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6b4bb5e0f7fcfcL    # 0.003332

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcc88f861a60d45L    # 0.22293

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb8e0b912dba4d7L    # 0.097179

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc57b84db9c7369L    # 0.167832

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f433c1ce6c093d9L    # 5.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3cd5f99c38b04bL    # 4.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff2fe74afd54541L    # 1.187123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403164a3400b88caL    # 17.393116

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fce213a0c6b484dL    # 0.235389

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc45625a682b628L    # 0.158879

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f092a737110e454L    # 4.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd249235f809918L    # 0.285714

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f670b49e01de269L    # 0.002813

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa958b827fa1a0dL    # 0.049505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde0bdcad14a0a1L    # 0.469474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc1f383f0bae89fL    # 0.140244

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4003799fe43675deL    # 2.434387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402258203e63e8deL    # 9.172121

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc1ae147ae147aeL    # 0.138125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f96b484d76ab580L    # 0.022173

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fda5d63886594afL    # 0.41195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f59e731d2e0e304L    # 0.001581

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f881f969e3c9689L    # 0.011779

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe05a72a7bd48cbL    # 0.511041

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    invoke-virtual {p1, v5, v6}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f746a1a500d5e8dL    # 0.004984

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe10da3c21187e8L    # 0.532915

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a0cf1800a7c5bL    # 7.95E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f31b1d92b7fe08bL    # 2.7E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4376d549731099L    # 5.94E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f73c89f40a2877fL    # 0.00483

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd5ee73e681a9b9L    # 0.342679

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0de26916440f24L    # 5.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f610a137f38c543L    # 0.00208

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd31f36262cba73L    # 0.29878

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f46f0068db8bac7L    # 7.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7989df1172ef0bL    # 0.006235

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa59e6256366d7aL    # 0.042224

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc76a26e5471716L    # 0.182927

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401180aa64c2f838L    # 4.37565

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4003f56861e92924L    # 2.494828

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f32dfd694ccab3fL    # 2.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd1f383f0bae89fL    # 0.280488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f801b003686a4caL    # 0.007864

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff5acc1ca3a4b55L    # 1.354677

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f96efc371da37efL    # 0.022399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc32fdb8fde2ef5L    # 0.149898

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002c0d23d4f15e8L    # 2.344151

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f141205bc01aL    # 0.1558

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f44e7ee9142b303L    # 6.38E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f715054ac29bf16L    # 0.004227

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402ced60c7c0f451L    # 14.463629

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e4712e40852b5L    # 0.003696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b0ff10ecb74deL    # 0.006607

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbb1e8e60807358L    # 0.105935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f057eed45e9185dL    # 4.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4061d6da4052d667L    # 142.714142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40219b59146e4c0eL    # 8.803414

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffc82a8869c66d3L    # 1.781899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b068123810e88L    # 0.003299

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f421ee675147f13L    # 5.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fcce0c9d9d3458dL    # 0.22561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f41fd5885d31338L    # 5.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401a20daa0cae643L    # 6.532084

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f80624dd2f1a9fcL    # 0.008

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc533d29563a9f4L    # 0.165644

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff8fa2df9378ee3L    # 1.561079

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcb4067cf1c3266L    # 0.212903

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40310eda33bd9caeL    # 17.058017

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa5eb313be22e5eL    # 0.04281

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0095f2452c5a0L    # 0.062643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f16ce789e774eecL    # 8.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f90685553ef6b5dL    # 0.016023

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd85c7cd898b2eaL    # 0.380645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb19f2ba9d1f601L    # 0.068835

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400411a75cd0bb6fL    # 2.50862

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f722424a276b7edL    # 0.004429

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9431bde82d7b63L    # 0.019721

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f858ea7ce0fc2ecL    # 0.010526

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffd84449dbec248L    # 1.844792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401d23aeee957471L    # 7.284847

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f14727dcbddb984L    # 7.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3faa16c61522a6f4L    # 0.050955

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e279dd3bafd97L    # 0.003681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40503f4adff822bcL    # 64.988945

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd026c3b927d45aL    # 0.252366

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fac50efdc9c4da9L    # 0.055305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40369fbf1e8e6080L    # 22.62401

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021b4061847f562L    # 8.851609

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fab6134ce3de615L    # 0.053476

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc0bf65dbfceb79L    # 0.130841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb7333333333333L    # 0.090625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4007e4a9cdc44391L    # 2.986652

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe40dda48b65237L    # 0.626691

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44057082491afcL    # 6.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405e90b1e18efbb1L    # 122.260857

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403b538561d4306eL    # 27.326254

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f2e48e8a71deL    # 0.15585

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fae28cbd1244a62L    # 0.058905

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc05857afea3df7L    # 0.127696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f635fc3b4f61672L    # 0.002365

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4035c1fd5885d313L    # 21.757772

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbfb9f127f5e84fL    # 0.123931

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f92f0e0a84be404L    # 0.018497

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40204c3547e06962L    # 8.148844

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400b71f2dc2b0ea2L    # 3.430639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb6c54bcf0b6b6eL    # 0.088948

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb1b7281fd9ba1bL    # 0.069201

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6314445aa2e3c5L    # 0.002329

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f993f6c2699c7bdL    # 0.024656

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    invoke-virtual {p1, v10, v11}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc5ab8e8ea39c52L    # 0.169298

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f90fe047d3d4281L    # 0.016594

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffe3358f2e05ccdL    # 1.887536

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    invoke-virtual {p1, v7}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc416ce789e774fL    # 0.156946

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcf4c3c18b502acL    # 0.244514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f95bf6a0dbad3a6L    # 0.021238

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401be314445aa2e4L    # 6.971757

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403923e425aee632L    # 25.1402

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc323e5b8561d43L    # 0.149533

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc10b417ca2120eL    # 0.133156

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f55d2d01c0ca601L    # 0.001332

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8ad080b673c4f4L    # 0.013093

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcd439de481f538L    # 0.228626

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40222e48c7192ea5L    # 9.090399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6394317acc4ef9L    # 0.00239

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4344806290eed0L    # 5.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d5c31593e5fb7L    # 0.003584

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f30e7ff583a54L    # 0.03046

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1754b05b7cfe58L    # 8.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdb8398a65492ffL    # 0.429907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f536a400fba8827L    # 0.001185

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1711947cfa26a2L    # 8.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff68a9cdc443915L    # 1.408841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe5e63e8dda48b6L    # 0.684356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f951b0ccbc05dL    # 7.895611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb794317acc4ef9L    # 0.092105

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4033aa7a63736cdfL    # 19.66593

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0986fcdee34fcL    # 0.064826

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3adea897635e74L    # 4.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91d81f10667f91L    # 0.017426

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fef8663c74fb54aL    # 0.985155

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401850de093532e8L    # 6.078972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a1554fbdad752L    # 1.99E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4015d7fe08aefb2bL    # 5.46093

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6b994e1a3f4667L    # 0.003369

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdad76683c297c0L    # 0.419397

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7e02ea960b6fa0L    # 0.007327

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b5525cc426352L    # 0.006673

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4682f944241c3fL    # 6.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40003cdee34fc611L    # 2.029722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4690de093532eL    # 0.159456

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    invoke-virtual {p1, v5, v6}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40144272c94b380dL    # 5.064891

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    invoke-virtual {p1, v7}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fcff6fd21ff2e49L    # 0.249725

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40002efb2aae2974L    # 2.02294

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6741d084e831adL    # 0.002839

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    invoke-virtual {p1, v8, v9}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400e1294dd72367eL    # 3.759073

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d94d0dcfcc5b9L    # 0.003611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f47ebaf102363b2L    # 7.3E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fab0d73860999ddL    # 0.052837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    invoke-virtual {p1, v10, v11}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40219dba0a526959L    # 8.80806

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb76cbd987c6328L    # 0.091503

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403773441bb8c32bL    # 23.450258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ca

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9ce032db1e9f27L    # 0.028199

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9696a26e547171L    # 0.022059

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9e09784ec636b1L    # 0.029333

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
