.class public final Lbrt;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lbrl;

.field private final d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;


# direct methods
.method public constructor <init>(Lbrc;Lbrl;Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p4}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrt;->c:Lbrl;

    iput-object p3, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    const-string v1, "Invalid close request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    const-string v1, "Invalid close request: no contents"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Invalid close request: doesn\'t include save state"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid close request: invalid request"

    invoke-static {v0, v1}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbrt;->c:Lbrl;

    iget-object v1, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget-object v3, p0, Lbrt;->d:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, p1, v1, v2, v3}, Lbrl;->a(Lbsp;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Z)V

    iget-object v0, p0, Lbrt;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->a()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
