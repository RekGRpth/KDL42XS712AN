.class public final enum Leyt;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Leyt;

.field public static final enum b:Leyt;

.field private static final synthetic c:[Leyt;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Leyt;

    const-string v1, "Standard"

    invoke-direct {v0, v1, v2}, Leyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Leyt;->a:Leyt;

    new-instance v0, Leyt;

    const-string v1, "NearestNeighbor"

    invoke-direct {v0, v1, v3}, Leyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Leyt;->b:Leyt;

    const/4 v0, 0x2

    new-array v0, v0, [Leyt;

    sget-object v1, Leyt;->a:Leyt;

    aput-object v1, v0, v2

    sget-object v1, Leyt;->b:Leyt;

    aput-object v1, v0, v3

    sput-object v0, Leyt;->c:[Leyt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Leyt;
    .locals 1

    const-class v0, Leyt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Leyt;

    return-object v0
.end method

.method public static values()[Leyt;
    .locals 1

    sget-object v0, Leyt;->c:[Leyt;

    invoke-virtual {v0}, [Leyt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Leyt;

    return-object v0
.end method
