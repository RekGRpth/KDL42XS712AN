.class public final Lhke;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lilt;

.field final b:Ljava/util/List;

.field final c:Lhkf;

.field private final d:J


# direct methods
.method public constructor <init>(Lilt;Ljava/util/List;Lhkf;ZLjava/util/Calendar;Ljava/util/Calendar;J)V
    .locals 11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhke;->a:Lilt;

    move-wide/from16 v0, p7

    iput-wide v0, p0, Lhke;->d:J

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_1

    sget-object v2, Lilt;->c:Ljava/util/Comparator;

    invoke-static {p2, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lilt;

    invoke-virtual {v2, p1}, Lilt;->a(Lilt;)Lilt;

    move-result-object v4

    if-eqz v4, :cond_6

    if-eqz v3, :cond_6

    iget-wide v7, v4, Lilt;->a:J

    iget-wide v9, v3, Lilt;->b:J

    cmp-long v2, v7, v9

    if-gez v2, :cond_6

    iget-wide v7, v3, Lilt;->b:J

    iget-wide v9, v4, Lilt;->b:J

    cmp-long v2, v7, v9

    if-gez v2, :cond_0

    new-instance v2, Lilt;

    iget-wide v9, v4, Lilt;->b:J

    invoke-direct {v2, v7, v8, v9, v10}, Lilt;-><init>(JJ)V

    :goto_1
    if-eqz v2, :cond_5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v3, v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lilt;

    invoke-virtual {v2, p1}, Lilt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object p3, Lhkf;->a:Lhkf;

    :cond_3
    iput-object p3, p0, Lhke;->c:Lhkf;

    if-eqz p4, :cond_4

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1, v3}, Lhke;->a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lhke;->b:Ljava/util/List;

    :goto_3
    return-void

    :cond_4
    iput-object v3, p0, Lhke;->b:Ljava/util/List;

    goto :goto_3

    :cond_5
    move-object v2, v3

    goto :goto_2

    :cond_6
    move-object v2, v4

    goto :goto_1
.end method

.method private static a(JJLjava/util/List;)J
    .locals 5

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0, p0, p1}, Lilt;->c(J)J

    move-result-wide v2

    cmp-long v4, v2, p2

    if-gtz v4, :cond_0

    sub-long/2addr p2, v2

    goto :goto_0

    :cond_0
    iget-wide v0, v0, Lilt;->a:J

    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long/2addr v0, p2

    :goto_1
    return-wide v0

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_1
.end method

.method private a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;
    .locals 8

    const-wide/16 v0, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0}, Lilt;->a()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0x2

    div-long/2addr v1, v3

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iget-object v3, p0, Lhke;->a:Lilt;

    iget-wide v3, v3, Lilt;->a:J

    invoke-static {v0, v3, v4}, Lilv;->a(Ljava/util/Calendar;J)V

    new-instance v3, Ljava/util/Random;

    iget-wide v4, p0, Lhke;->d:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    xor-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v3

    long-to-double v0, v1

    mul-double/2addr v0, v3

    double-to-long v0, v0

    iget-object v2, p0, Lhke;->a:Lilt;

    iget-wide v2, v2, Lilt;->a:J

    invoke-static {v2, v3, v0, v1, p3}, Lhke;->a(JJLjava/util/List;)J

    move-result-wide v2

    if-eqz p2, :cond_4

    iget-object v0, p0, Lhke;->a:Lilt;

    invoke-virtual {v0, p2}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1, p2}, Lilv;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gez v4, :cond_4

    move-wide v1, v0

    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0, v1, v2}, Lilt;->a(J)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0, v1, v2}, Lilt;->b(J)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Lilt;

    iget-wide v6, v0, Lilt;->b:J

    invoke-direct {v5, v1, v2, v6, v7}, Lilt;-><init>(JJ)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_4
    move-wide v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)Lilt;
    .locals 3

    iget-object v0, p0, Lhke;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0, p1}, Lilt;->b(Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SensorCollectionTimeSpan [targetTimeSpan="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhke;->a:Lilt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhke;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpanType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhke;->c:Lhkf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
