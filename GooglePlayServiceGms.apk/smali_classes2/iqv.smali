.class public final Liqv;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Liqc;

.field public b:Liqc;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Liqv;->a:Liqc;

    iput-object v0, p0, Liqv;->b:Liqc;

    const/4 v0, -0x1

    iput v0, p0, Liqv;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Liqv;->a:Liqc;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Liqv;->a:Liqc;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Liqv;->b:Liqc;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Liqv;->b:Liqc;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Liqv;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liqv;->a:Liqc;

    if-nez v0, :cond_1

    new-instance v0, Liqc;

    invoke-direct {v0}, Liqc;-><init>()V

    iput-object v0, p0, Liqv;->a:Liqc;

    :cond_1
    iget-object v0, p0, Liqv;->a:Liqc;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Liqv;->b:Liqc;

    if-nez v0, :cond_2

    new-instance v0, Liqc;

    invoke-direct {v0}, Liqc;-><init>()V

    iput-object v0, p0, Liqv;->b:Liqc;

    :cond_2
    iget-object v0, p0, Liqv;->b:Liqc;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Liqv;->a:Liqc;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Liqv;->a:Liqc;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Liqv;->b:Liqc;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Liqv;->b:Liqc;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liqv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liqv;

    iget-object v2, p0, Liqv;->a:Liqc;

    if-nez v2, :cond_3

    iget-object v2, p1, Liqv;->a:Liqc;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Liqv;->a:Liqc;

    iget-object v3, p1, Liqv;->a:Liqc;

    invoke-virtual {v2, v3}, Liqc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liqv;->b:Liqc;

    if-nez v2, :cond_5

    iget-object v2, p1, Liqv;->b:Liqc;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Liqv;->b:Liqc;

    iget-object v3, p1, Liqv;->b:Liqc;

    invoke-virtual {v2, v3}, Liqc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Liqv;->a:Liqc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqv;->b:Liqc;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Liqv;->a:Liqc;

    invoke-virtual {v0}, Liqc;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Liqv;->b:Liqc;

    invoke-virtual {v1}, Liqc;->hashCode()I

    move-result v1

    goto :goto_1
.end method
