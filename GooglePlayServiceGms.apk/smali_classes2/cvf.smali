.class final Lcvf;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static c:Ljava/util/ArrayList;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final e:Ldns;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "match_sync_token"

    aput-object v2, v0, v1

    sput-object v0, Lcvf;->b:[Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcvf;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;)V
    .locals 2

    const-string v0, "MultiplayerAgent"

    sget-object v1, Lcvf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcvf;->f:J

    new-instance v0, Ldns;

    invoke-direct {v0, p2}, Ldns;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvf;->e:Ldns;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "last_modified_timestamp DESC"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    invoke-static {p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Lblt;

    invoke-direct {v3, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    const-string v0, "metadata_version"

    const-string v1, "0"

    const-string v2, ">=?"

    invoke-virtual {v3, v0, v1, v2}, Lblt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string v0, "external_game_id"

    invoke-virtual {v3, v0, p2}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    packed-switch p3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sort order "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v4, "last_modified_timestamp DESC"

    :goto_0
    iget-object v1, v3, Lblt;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Lblt;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lblt;->c:[Ljava/lang/String;

    move-object v0, p0

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :pswitch_1
    const-string v4, "inviter_in_circles DESC, CASE WHEN inviter_in_circles=0 THEN most_recent_invitation ELSE NULL END DESC, CASE WHEN inviter_in_circles=0 THEN external_inviter_id ELSE NULL END,last_modified_timestamp DESC"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvg;
    .locals 13

    move-object/from16 v5, p3

    :goto_0
    :try_start_0
    invoke-static {p2}, Lcum;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v12

    iget-object v8, p0, Lcvf;->e:Ldns;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcvf;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lcwh;->n:Lbfy;

    invoke-virtual {v3}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const-string v6, "ANDROID"

    const/4 v4, 0x0

    move-object/from16 v7, p4

    invoke-static/range {v1 .. v7}, Ldns;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v6, v8, Ldns;->a:Lbmi;

    const/4 v8, 0x1

    const/4 v10, 0x0

    const-class v11, Ldnt;

    move-object v7, v12

    invoke-virtual/range {v6 .. v11}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldnt;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    invoke-virtual {v1}, Ldnt;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1}, Ldnt;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ldnt;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v4, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v1, v4}, Lbiq;->a(ZLjava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v3, v0}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvg;

    move-result-object v1

    iget v4, v1, Lcvg;->c:I

    if-nez v4, :cond_0

    iget-object v3, v1, Lcvg;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, v1, Lcvg;->b:Ljava/lang/String;

    move-object v3, v1

    :cond_0
    if-nez v2, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    new-instance v2, Lcvg;

    invoke-direct {v2, v1, v3, v4}, Lcvg;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v1, v2

    :goto_3
    return-object v1

    :catch_0
    move-exception v1

    const/16 v2, 0x19a

    invoke-static {v1, v2}, Lbng;->a(Lsp;I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {p2}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "match_sync_token"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "MultiplayerAgent"

    invoke-static {v2, v1, v3}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    const-string v1, "MultiplayerAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Token "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is invalid. Retrying with no token."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Ldac;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "MultiplayerAgent"

    invoke-static {v1, v2}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_2
    new-instance v1, Lcvg;

    invoke-direct {v1}, Lcvg;-><init>()V

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    move-object v1, v2

    goto/16 :goto_2
.end method

.method private static a(Ldms;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ldms;->getRoom()Ldow;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldms;->getRoom()Ldow;

    move-result-object v0

    invoke-virtual {v0}, Ldow;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v0

    invoke-virtual {v0}, Ldpp;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected entity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 3

    sget-object v1, Lcvf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcvf;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcvf;->c:Ljava/util/ArrayList;

    const-string v2, "real_time"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Leeo;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcvf;->c:Ljava/util/ArrayList;

    const-string v2, "turn_based"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcvf;->c:Ljava/util/ArrayList;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ldfu;

    invoke-static {p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {p0, v0, v3}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Ldfu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v2}, Ldfu;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "inviter_in_circles"

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->k()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldfu;->b()V

    throw v0

    :cond_1
    invoke-virtual {v2}, Ldfu;->b()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "MultiplayerAgent"

    invoke-static {v0, v1, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_2
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvg;Ljava/util/HashMap;Z)V
    .locals 19

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcvg;->b:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-static/range {p2 .. p2}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "match_sync_token"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p3

    iget-object v13, v0, Lcvg;->a:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    move v11, v4

    :goto_0
    if-ge v11, v14, :cond_f

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ldms;

    const/4 v4, 0x0

    invoke-virtual {v6}, Ldms;->getRoom()Ldow;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v6}, Ldms;->getRoom()Ldow;

    move-result-object v5

    invoke-virtual {v5}, Ldow;->e()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4}, Lcvf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v5}, Ldow;->f()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    const-string v5, "MultiplayerAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Received tombstone for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Lcum;->a(I)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcvh;

    const/4 v5, -0x1

    const/4 v7, 0x1

    invoke-direct {v4, v15, v5, v7}, Lcvh;-><init>(IIZ)V

    :goto_1
    move-object v5, v4

    :goto_2
    if-eqz v5, :cond_1

    iget v4, v5, Lcvh;->a:I

    iget v7, v5, Lcvh;->b:I

    if-eq v4, v7, :cond_e

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_1

    invoke-static {v6}, Lcvf;->a(Ldms;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Ldow;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lbiq;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-nez v4, :cond_4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v7}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v16, -0x1

    cmp-long v8, v8, v16

    if-nez v8, :cond_3

    const-string v4, "MultiplayerAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "No game found matching external game ID "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {v5}, Ldow;->f()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_4
    invoke-static {v5}, Lbiq;->a(Z)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v9

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-static/range {v4 .. v10}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I

    new-instance v4, Lcvh;

    const/4 v5, 0x1

    const/4 v7, 0x1

    invoke-direct {v4, v15, v5, v7}, Lcvh;-><init>(IIZ)V

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    :cond_6
    invoke-virtual {v6}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-virtual {v6}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v15

    invoke-virtual {v15}, Ldpp;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v5}, Lcvf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v7

    const/4 v4, -0x1

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Ldjk;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v8, "version"

    const/4 v9, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v8, v9}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v4

    :cond_7
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v16

    invoke-virtual {v15}, Ldpp;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v8, 0x5

    if-ne v4, v8, :cond_9

    move-object/from16 v0, p2

    invoke-static {v0, v5, v10}, Lcvi;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/ArrayList;)V

    const/4 v4, -0x1

    if-eq v7, v4, :cond_8

    const/4 v4, 0x1

    :goto_5
    new-instance v5, Lcvh;

    const/4 v7, -0x1

    move/from16 v0, v16

    invoke-direct {v5, v0, v7, v4}, Lcvh;-><init>(IIZ)V

    move-object v4, v5

    :goto_6
    move-object v5, v4

    goto/16 :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    :cond_9
    invoke-virtual {v15}, Ldpp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lbiq;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-nez v4, :cond_b

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v7}, Lcum;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v17, -0x1

    cmp-long v8, v8, v17

    if-nez v8, :cond_a

    const-string v4, "MultiplayerAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "No game found matching external game ID "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_6

    :cond_a
    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    invoke-virtual {v15}, Ldpp;->g()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_c

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v9

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-static/range {v4 .. v10}, Lcvi;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I

    new-instance v4, Lcvh;

    const/4 v5, 0x1

    const/4 v7, 0x1

    move/from16 v0, v16

    invoke-direct {v4, v0, v5, v7}, Lcvh;-><init>(IIZ)V

    goto :goto_6

    :cond_c
    move-object/from16 v0, p2

    invoke-static {v0, v5}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v9

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-static/range {v4 .. v10}, Lcvi;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldms;JLbpe;Ljava/util/ArrayList;)I

    new-instance v4, Lcvh;

    invoke-virtual {v15}, Ldpp;->e()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v7, 0x0

    move/from16 v0, v16

    invoke-direct {v4, v0, v5, v7}, Lcvh;-><init>(IIZ)V

    goto/16 :goto_6

    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected entity: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbiq;->b(Ljava/lang/Object;)V

    move-object v5, v4

    goto/16 :goto_2

    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v4, 0x1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_10

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "MultiplayerAgent"

    invoke-static {v4, v10, v5}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v4

    :cond_10
    if-nez v4, :cond_11

    const-string v4, "MultiplayerAgent"

    const-string v5, "Failed to store matches"

    invoke-static {v4, v5}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_7
    return-void

    :cond_11
    invoke-virtual {v12}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_12

    sget-object v4, Ldei;->a:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcum;->d(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_12
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, p5

    invoke-static {v0, v1, v2, v3, v12}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvg;ZLjava/util/HashMap;)V

    invoke-static/range {p1 .. p2}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v4

    invoke-interface {v4}, Lbpe;->a()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcvf;->f:J

    goto :goto_7
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvg;ZLjava/util/HashMap;)V
    .locals 14

    invoke-static {p0, p1}, Lcvf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ldjm;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    move-object/from16 v0, p2

    iget-object v9, v0, Lcvg;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v5, v1

    :goto_0
    if-ge v5, v10, :cond_9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldms;

    invoke-virtual {v1}, Ldms;->getRoom()Ldow;

    move-result-object v2

    invoke-virtual {v1}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v11

    if-nez v2, :cond_1

    if-nez v11, :cond_1

    const-string v2, "MultiplayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown type of entity. Ignoring "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcvf;->a(Ldms;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Ldms;->getRoom()Ldow;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ldms;->getRoom()Ldow;

    move-result-object v2

    invoke-virtual {v2}, Ldow;->b()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    :goto_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcvh;

    if-nez v2, :cond_4

    const-string v1, "MultiplayerAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already processed entity. Ignoring "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v2

    invoke-virtual {v2}, Ldpp;->b()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected entity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbiq;->b(Ljava/lang/Object;)V

    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_2

    :cond_4
    if-eqz p3, :cond_7

    const/4 v4, 0x0

    iget-boolean v13, v2, Lcvh;->c:Z

    if-eqz v13, :cond_6

    invoke-virtual {v2}, Lcvh;->a()Z

    move-result v4

    invoke-static {p0, p1, v3, v12, v4}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    :cond_5
    :goto_3
    if-eqz v4, :cond_7

    const-string v2, "MultiplayerAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v11, "Notification "

    invoke-direct {v4, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " consumed by listener for game "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Deleting."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "external_sub_id=?"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcum;->a(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ldms;->getNotification()Ldmt;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v1}, Ldmt;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v2, v3, v1}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    if-eqz v11, :cond_5

    invoke-virtual {v2}, Lcvh;->a()Z

    move-result v4

    invoke-static {p0, p1, v3, v11, v4}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ldpp;Z)Z

    move-result v4

    goto :goto_3

    :cond_7
    invoke-virtual {v1}, Ldms;->getNotification()Ldmt;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v12, v1, v3

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "external_sub_id=?"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcum;->a(I)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iget-boolean v1, v2, Lcvh;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v6, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    if-nez v1, :cond_8

    const/4 v1, 0x0

    :goto_4
    const-string v2, "image_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_4

    :cond_9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "MultiplayerAgent"

    invoke-static {v1, v7, v2}, Lcum;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_a
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ldpp;Z)Z
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p3}, Ldpp;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Ldjj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    :try_start_0
    invoke-virtual {p3}, Ldpp;->d()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcux;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 6

    const/4 v2, 0x0

    invoke-static {}, Lcux;->a()Lcux;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p3}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3, v2}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcux;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 5

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Ldfu;

    invoke-static {p1}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {p0, v0, v3}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Ldfu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v2}, Ldfu;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldfu;->b()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Ldfu;->b()V

    return-object v1
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    invoke-static {p1, p2}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcum;->b(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    invoke-static {p1}, Ldis;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcvf;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcvf;->f:J

    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 9

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcvf;->f:J

    sub-long v1, v0, v2

    sget-object v0, Lcwh;->q:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-gtz v0, :cond_0

    const-string v0, "MultiplayerAgent"

    const-string v1, "Returning cached entities"

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v6

    :cond_0
    invoke-static {p1, p2}, Lcvf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcvg;

    move-result-object v3

    const-string v0, "MultiplayerAgent"

    const-string v1, "Received %s multiplayer entities during sync"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v4, v3, Lcvg;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, v3, Lcvg;->c:I

    if-eqz v0, :cond_1

    iget v6, v3, Lcvg;->c:I

    goto :goto_0

    :cond_1
    iget-object v2, v3, Lcvg;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v6

    :goto_1
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldms;

    invoke-virtual {v0}, Ldms;->getRoom()Ldow;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v0}, Ldms;->getRoom()Ldow;

    move-result-object v0

    invoke-virtual {v0}, Ldow;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v0}, Ldms;->getTurnBasedMatch()Ldpp;

    move-result-object v0

    invoke-virtual {v0}, Ldpp;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed entity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-static {p1, p2, v7}, Lcum;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v4

    if-nez p3, :cond_6

    :goto_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcvf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcvg;Ljava/util/HashMap;Z)V

    goto/16 :goto_0

    :cond_6
    move v5, v6

    goto :goto_3
.end method
