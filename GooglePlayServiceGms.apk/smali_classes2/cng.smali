.class final Lcng;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcnf;


# direct methods
.method private constructor <init>(Lcnf;)V
    .locals 0

    iput-object p1, p0, Lcng;->a:Lcnf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcnf;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcng;-><init>(Lcnf;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    :try_start_0
    iget-object v2, p0, Lcng;->a:Lcnf;

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Started downloading pinned content."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v2, Lcnf;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :goto_0
    iget-object v3, v2, Lcnf;->e:Ljava/util/Map;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, v2, Lcnf;->a:Lcos;

    invoke-interface {v0}, Lcos;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Stopped downloading pinned content because the device is offline."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    :goto_1
    return-void

    :cond_0
    iget-object v0, v2, Lcnf;->b:Lcfz;

    invoke-interface {v0}, Lcfz;->t()Lcgs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :try_start_2
    invoke-interface {v4}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    iget-object v1, v2, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v6

    sget-object v1, Lbqs;->o:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v6, v1, :cond_2

    iget-object v1, v2, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2, v0}, Lcnf;->a(Lcfp;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PinnedContentDownloader"

    const-string v6, "Queueing download of file (%d of %d): %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v2, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget-object v9, Lbqs;->o:Lbfy;

    invoke-virtual {v9}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v6, v7}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v1, Lcnh;

    invoke-direct {v1, v2, v0}, Lcnh;-><init>(Lcnf;Lcfp;)V

    iget-object v6, v2, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v2, Lcnf;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v4}, Lcgs;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_2
    :try_start_5
    invoke-interface {v4}, Lcgs;->close()V

    iget-object v0, v2, Lcnf;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Finished downloading pinned content."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    goto/16 :goto_1

    :cond_3
    iget-object v0, v2, Lcnf;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "PinnedContentDownloader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pinned content download task failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
