.class final Lfpe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lfpd;

.field private final b:Lfxh;

.field private final c:Ljava/lang/String;

.field private d:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lfpd;Lfxh;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lfpe;->a:Lfpd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lfpe;->b:Lfxh;

    iput-object p3, p0, Lfpe;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lfpe;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lfpe;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lfpe;)Lfxh;
    .locals 1

    iget-object v0, p0, Lfpe;->b:Lfxh;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->b(Lfpd;)Lbmi;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->a(Lfpd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfpe;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbmi;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    array-length v2, v0

    iget-object v3, p0, Lfpe;->a:Lfpd;

    invoke-static {v3}, Lfpd;->d(Lfpd;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lfpe;->d:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lfpe;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "ListAppsImageManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed image URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfpe;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfpe;->a:Lfpd;

    invoke-static {v0}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfpe;->a:Lfpd;

    invoke-static {v1}, Lfpd;->c(Lfpd;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
