.class public final Lbwv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwz;


# instance fields
.field private final a:Lcfz;

.field private final b:Lcfc;

.field private final c:Ljava/util/Set;

.field private final d:I

.field private final e:Lbvu;


# direct methods
.method public constructor <init>(Lcoy;Lcfc;ILbvu;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbwv;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lbwv;->b:Lcfc;

    iput p3, p0, Lbwv;->d:I

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvu;

    iput-object v0, p0, Lbwv;->e:Lbvu;

    iget-object v0, p0, Lbwv;->a:Lcfz;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lcfz;->a(Lcfc;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbwv;->c:Ljava/util/Set;

    return-void
.end method

.method private a(Ljava/util/Set;Lbvt;Lbwi;)V
    .locals 5

    iget-object v0, p0, Lbwv;->a:Lcfz;

    iget-object v1, p0, Lbwv;->b:Lcfc;

    new-instance v2, Lbut;

    invoke-direct {v2, p1}, Lbut;-><init>(Ljava/util/Set;)V

    const-wide v3, 0x7fffffffffffffffL

    invoke-interface {v0, v1, v2, v3, v4}, Lcfz;->a(Lcfc;Lbuv;J)Lcgb;

    move-result-object v0

    new-instance v1, Lbww;

    invoke-direct {v1, v0, p3}, Lbww;-><init>(Lcgb;Lbwi;)V

    iget-object v0, v0, Lcgb;->a:Lbuw;

    iget-object v2, p0, Lbwv;->b:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    iget v3, p0, Lbwv;->d:I

    invoke-virtual {p2, v0, v2, v3, v1}, Lbvt;->a(Lbuw;Ljava/lang/String;ILbvs;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 6

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lbwv;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, Lbwv;->a:Lcfz;

    iget-object v3, p0, Lbwv;->b:Lcfc;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Lcfz;->a(Lcfc;J)Lbsp;

    move-result-object v0

    iget-object v2, p0, Lbwv;->a:Lcfz;

    invoke-interface {v2, v0}, Lcfz;->a(Lbsp;)Lcfg;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcfg;->c:Z

    invoke-virtual {v0}, Lcfg;->k()V

    goto :goto_0
.end method

.method public final a(Lbvt;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbwv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbwv;->e:Lbvu;

    iget-object v1, p0, Lbwv;->b:Lcfc;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2, v3}, Lbvu;->a(Landroid/content/SyncResult;Lcfc;Ljava/lang/Boolean;Z)Lbvs;

    move-result-object v0

    iget-object v1, p0, Lbwv;->e:Lbvu;

    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v0, v1, v2}, Lbvu;->a(Lbvs;J)Lbwi;

    move-result-object v1

    sget-object v0, Lbth;->g:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwv;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v3, p1, v1}, Lbwv;->a(Ljava/util/Set;Lbvt;Lbwi;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbwv;->c:Ljava/util/Set;

    invoke-direct {p0, v0, p1, v1}, Lbwv;->a(Ljava/util/Set;Lbvt;Lbwi;)V

    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lbwv;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
