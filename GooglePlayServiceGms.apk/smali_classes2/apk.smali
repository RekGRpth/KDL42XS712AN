.class Lapk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic j:Z

.field private static final k:Ljava/lang/String;


# instance fields
.field public final a:Z

.field public b:Ljava/lang/String;

.field public c:Landroid/accounts/Account;

.field public d:Z

.field protected final e:Laoy;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field protected final i:Lapj;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lapk;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lapk;->j:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lapk;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lapk;->k:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Laoy;

    invoke-direct {v0, p1}, Laoy;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Laoy;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lapk;-><init>(Laoy;Ljava/lang/String;B)V

    return-void
.end method

.method private constructor <init>(Laoy;Ljava/lang/String;B)V
    .locals 6

    const/4 v5, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GLSUser"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lapk;->a:Z

    iput-object p1, p0, Lapk;->e:Laoy;

    invoke-static {}, Lapj;->a()Lapj;

    move-result-object v0

    iput-object v0, p0, Lapk;->i:Lapj;

    iput-object p2, p0, Lapk;->b:Ljava/lang/String;

    iget-object v0, p0, Lapk;->e:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    iget-object v1, p0, Lapk;->b:Ljava/lang/String;

    iget-object v2, p0, Lapk;->e:Laoy;

    iget-object v2, v2, Laoy;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lapk;->d:Z

    iget-object v0, p1, Laoy;->a:Landroid/content/Context;

    new-instance v1, Lbhv;

    invoke-direct {v1, v0}, Lbhv;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lapk;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Using [AuthUri: %s, SetupUri: %s, CreateProfileUri: %s]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GLSUser"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Laqc;->a:Lbfy;

    invoke-virtual {v4}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Laqc;->b:Lbfy;

    invoke-virtual {v4}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    sget-object v3, Laqc;->d:Lbfy;

    invoke-virtual {v3}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lapk;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lapk;->b:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    :cond_1
    iget-boolean v0, p0, Lapk;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lapk;->b:Ljava/lang/String;

    iget-object v1, p0, Lapk;->e:Laoy;

    iget-object v1, v1, Laoy;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v0

    iget-object v1, v0, Laoz;->e:Ljava/lang/String;

    iput-object v1, p0, Lapk;->h:Ljava/lang/String;

    iget-object v0, v0, Laoz;->d:Ljava/lang/String;

    iput-object v0, p0, Lapk;->g:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Laso;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5

    invoke-static {}, Lapj;->a()Lapj;

    invoke-virtual {p1, p3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    if-eqz p2, :cond_1

    sget-object v0, Lapl;->b:[I

    invoke-virtual {p3}, Laso;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    sget-object v0, Lapu;->f:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    sget-object v0, Lapu;->B:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_1
    return-object p1

    :pswitch_0
    sget-object v0, Lapu;->z:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lapk;->e(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/util/List;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto :goto_0

    :cond_2
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No consent details provided for requests to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "oauth2"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Laso;->A:Laso;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lapu;->q:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lapk;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "%s fillErrorResponse: BAD_AUTH response info: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lapk;->k:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GLSUser"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-object v1, Laso;->c:Laso;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    if-eqz v0, :cond_0

    sget-object v0, Laso;->c:Laso;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;
    .locals 4

    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    iget-boolean v0, p0, Lapk;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captcha url is ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Laox;->a(I)V

    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->i:Lapj;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {v0, v1, p3}, Lapj;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v1, Laso;->m:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Laso;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Laox;->a()V

    :goto_2
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/accounts/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->b:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Lapk;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bitmap response is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v1, "X-Google-Captcha-Error"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v1, Laso;->m:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Laso;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Laox;->a()V

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v2, Laso;->a:Laso;

    invoke-direct {v0, v2, p1, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Laso;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {}, Laox;->a()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {}, Laox;->a()V

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    sget-boolean v0, Lapk;->j:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lapk;->e:Laoy;

    invoke-virtual {v0, p2}, Laoy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0, p1, p4}, Lapk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v2, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v2, v0, v1, p3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    if-nez p3, :cond_1

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.visible_actions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".visible_graph"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p5, :cond_3

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.visible_actions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".pacl.data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz p6, :cond_0

    invoke-virtual {p6}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p6}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ".visible_graph"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v0}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const-string v0, "weblogin:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p3}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not caching since unable to generate a cache key for uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", service "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0, p4}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lapk;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EXP:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 5

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, " Request: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lapt;->b:Lapt;

    invoke-virtual {v4}, Lapt;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lapt;->d:Lapt;

    invoke-virtual {v4}, Lapt;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lapt;->O:Lapt;

    invoke-virtual {v4}, Lapt;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " RESULT: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " {"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " } Message: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lapk;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 5

    const-string v0, "access_secret"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "parent_aid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const-string v0, "GLSUser"

    const-string v1, "importAccountForCloning: Secret or parent aid found null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "parent_aid"

    invoke-static {p2, v1, v3}, Lapk;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v3, "oauthAccessToken"

    invoke-static {p2, v1, v3}, Lapk;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v3, "sha1hash"

    invoke-static {p2, v1, v3}, Lapk;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    sget-object v3, Lapu;->i:Lapu;

    invoke-virtual {v3}, Lapu;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v1, v3}, Lapk;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v3, "flags"

    invoke-static {p2, v1, v3}, Lapk;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    new-instance v3, Landroid/accounts/Account;

    const-string v4, "com.google"

    invoke-direct {v3, p1, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "perm."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v2, p0, Lapk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v0, p0, Lapk;->f:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lapk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v2, p0, Lapk;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    iget-object v3, p0, Lapk;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lapk;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object v1, p0, Lapk;->h:Ljava/lang/String;

    iput-object v1, p0, Lapk;->g:Ljava/lang/String;

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private static e(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Laum;

    invoke-direct {v2}, Laum;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Laum;

    iget-object v0, v0, Laum;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laup;

    invoke-static {v0}, Lapb;->a(Laup;)Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "GLSUser"

    const-string v3, "Failed to parse consent data"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    sget-object v2, Lapu;->i:Lapu;

    invoke-virtual {v2}, Lapu;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    sget-object v2, Lapu;->i:Lapu;

    invoke-virtual {v2}, Lapu;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-static {v4, v5, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v4, p0, Lapk;->c:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {v4, v3, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Ljava/util/Map;
    .locals 8

    const/4 v1, 0x0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_0

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 6

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v2, v1, Lapj;->c:Landroid/accounts/AccountManager;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "access_secret"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "parent_aid"

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v5, "parent_aid"

    invoke-virtual {v0, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "oauthAccessToken"

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v4, "oauthAccessToken"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sha1hash"

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v4, "sha1hash"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lapu;->i:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    sget-object v4, Lapu;->i:Lapu;

    invoke-virtual {v4}, Lapu;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "flags"

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v4, "flags"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    goto :goto_1

    :cond_2
    new-instance v0, Lbhv;

    iget-object v4, p0, Lapk;->e:Laoy;

    iget-object v4, v4, Laoy;->a:Landroid/content/Context;

    invoke-direct {v0, v4}, Lbhv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lbhv;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 7

    if-nez p5, :cond_0

    invoke-virtual {p1, p4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_0
    if-eqz p3, :cond_4

    const-string v1, ","

    invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    sget-object v5, Lapu;->A:Lapu;

    invoke-virtual {v5}, Lapu;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->l()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_1
    sget-object v5, Lapu;->C:Lapu;

    invoke-virtual {v5}, Lapu;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->n()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0, p3}, Lapk;->f(Ljava/lang/String;)V

    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p1, p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_5
    if-eqz p6, :cond_6

    if-eqz p7, :cond_6

    invoke-virtual {p1, p6}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->f(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->g(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_6
    if-eqz p8, :cond_7

    invoke-virtual {p1, p8}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_7
    if-eqz p9, :cond_8

    move-object/from16 v0, p9

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->h(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_8
    if-eqz p10, :cond_9

    move-object/from16 v0, p10

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->i(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_9
    sget-object v1, Laso;->a:Laso;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;ILjava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 13

    const-string v1, "SID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LSID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object/from16 v0, p4

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    :goto_0
    sget-object v1, Lapu;->c:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/lang/String;

    sget-object v1, Lapu;->n:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_4

    if-eqz p5, :cond_2

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    :goto_2
    return-object p1

    :cond_1
    sget-object v1, Lapu;->a:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    goto :goto_0

    :cond_2
    if-nez v12, :cond_3

    if-eqz v8, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    sget-object v1, Laso;->r:Laso;

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Laso;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const-string v2, "auto"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lapk;->k:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " Permission for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to access "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " will be managed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v1, "1"

    sget-object v2, Lapu;->o:Lapu;

    invoke-virtual {v2}, Lapu;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "remotely."

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    move-object v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :goto_3
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_5
    const-string v1, "locally."

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "1"

    move-object v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    goto :goto_3

    :cond_6
    sget-object v1, Laso;->r:Laso;

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Laso;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, p2, v1}, Lapk;->a(Ljava/util/Map;Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_8
    if-eqz v12, :cond_11

    sget-object v1, Lapu;->s:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    iget-boolean v2, p0, Lapk;->a:Z

    if-eqz v2, :cond_9

    const-string v2, "GLSUser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " canUpgradePlusValue: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-boolean v1, p0, Lapk;->d:Z

    if-eqz v1, :cond_d

    const-string v1, "SID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "LSID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_a
    const-string v1, "SID"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v1, "LSID"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    if-eqz v5, :cond_b

    const-string v2, "SID"

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p3

    move-object/from16 v4, p6

    invoke-direct/range {v1 .. v6}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    if-eqz v7, :cond_c

    const-string v2, "LSID"

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p3

    move-object/from16 v4, p6

    move-object v5, v7

    invoke-direct/range {v1 .. v6}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_6
    iget-object v1, p0, Lapk;->l:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-direct {p0}, Lapk;->b()V

    const/4 v1, 0x0

    iput-object v1, p0, Lapk;->l:Ljava/lang/String;

    :cond_d
    sget-object v1, Lapu;->b:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v1, Lapu;->i:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v1, Lapu;->u:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    sget-object v1, Lapu;->v:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    sget-object v1, Lapu;->k:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    sget-object v1, Lapu;->w:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    sget-object v1, Lapu;->x:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    iget-boolean v1, p0, Lapk;->d:Z

    if-nez v1, :cond_10

    const/4 v6, 0x1

    :goto_7
    move-object v1, p0

    move-object v2, p1

    move-object v5, v12

    invoke-virtual/range {v1 .. v11}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object p1

    goto/16 :goto_2

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_f
    sget-object v1, Lapu;->p:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p3

    move-object/from16 v4, p6

    move-object v5, v12

    invoke-direct/range {v1 .. v6}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_10
    const/4 v6, 0x0

    goto :goto_7

    :cond_11
    if-nez v8, :cond_12

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t get error message from reply:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Laso;->k:Laso;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v8

    :cond_12
    const-string v1, "badauth"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    sget-object v1, Laso;->b:Laso;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v8

    :cond_13
    invoke-static {v8}, Lapc;->a(Ljava/lang/String;)Laso;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Laso;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object p1

    sget-object v2, Laso;->l:Laso;

    if-ne v1, v2, :cond_15

    sget-object v1, Lapu;->e:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lapu;->g:Lapu;

    invoke-virtual {v2}, Lapu;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-direct {p0, v1, v2, v0}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_14
    :goto_8
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GLS error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lapk;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_15
    sget-object v2, Laso;->b:Laso;

    if-ne v1, v2, :cond_17

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_16

    invoke-direct {p0}, Lapk;->d()V

    goto :goto_8

    :cond_16
    iget-object v1, p0, Lapk;->g:Ljava/lang/String;

    if-nez v1, :cond_14

    invoke-direct {p0}, Lapk;->d()V

    goto :goto_8

    :cond_17
    sget-object v2, Laso;->q:Laso;

    if-ne v1, v2, :cond_14

    invoke-direct {p0}, Lapk;->d()V

    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v4, "1"

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 21

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->e:Laoy;

    iget-object v3, v3, Laoy;->a:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-static {v3, v0, v14}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v4, Laso;->v:Laso;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v3

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1e

    const-string v3, "ac2dm"

    move-object/from16 v17, v3

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v20

    if-nez v17, :cond_1d

    const-string v3, "ac2dm"

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->a:Z

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lapk;->k:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " Looking for cached grant for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to access "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on behalf of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "GLSUser"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->d:Z

    if-eqz v4, :cond_f

    invoke-direct/range {p0 .. p0}, Lapk;->c()Z

    move-result v4

    if-eqz v4, :cond_f

    if-nez v3, :cond_9

    const/4 v7, 0x0

    :cond_3
    :goto_3
    if-eqz v7, :cond_f

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->a:Z

    if-eqz v4, :cond_4

    const-string v4, "GLSUser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Return from cache "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->s()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->b:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v13}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v3

    :goto_4
    if-nez v3, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    move-result-object v9

    sget-object v3, Lapl;->a:[I

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->h()Laro;

    move-result-object v4

    invoke-virtual {v4}, Laro;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_10

    if-nez v19, :cond_10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lapk;->a:Z

    if-eqz v3, :cond_5

    const-string v3, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has_permission because running in pre SDK 11 AccountManager"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/4 v10, 0x1

    :cond_6
    :goto_5
    if-eqz v10, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->i:Lapj;

    iget-object v3, v3, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lapk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v8, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->c:Landroid/accounts/Account;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".pacl.visible_actions"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lapk;->c:Landroid/accounts/Account;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ".pacl.data"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    new-instance v8, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    invoke-direct {v8, v5, v6}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-nez v9, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->c:Landroid/accounts/Account;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".all_visible"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lapk;->c:Landroid/accounts/Account;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ".visible_graph"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v5, :cond_8

    if-eqz v3, :cond_1b

    :cond_8
    new-instance v9, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-direct {v9, v5, v3}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;-><init>(ZLjava/lang/String;)V

    move-object/from16 v16, v9

    move-object v15, v8

    :goto_6
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->g()Z

    move-result v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->f()Z

    move-result v9

    const/4 v11, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->c()Landroid/os/Bundle;

    move-result-object v13

    move-object/from16 v3, p0

    move-object/from16 v4, v17

    move-object/from16 v5, v18

    move-object/from16 v12, p1

    move-object/from16 v14, p3

    move-object/from16 v17, p4

    invoke-virtual/range {v3 .. v17}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v3

    goto/16 :goto_0

    :cond_9
    if-nez v19, :cond_a

    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_a
    const-string v4, "weblogin:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_b
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v3, v1}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->a:Z

    if-eqz v4, :cond_d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "No cache hit because unable to get cache key for uid: %s, service: %s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GLSUser"

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->i:Lapj;

    iget-object v5, v5, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v5, v6, v4}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->i:Lapj;

    iget-object v5, v5, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lapk;->c:Landroid/accounts/Account;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "EXP:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    cmp-long v4, v4, v8

    if-gez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->i:Lapj;

    iget-object v4, v4, Lapj;->c:Landroid/accounts/AccountManager;

    const-string v5, "com.google"

    invoke-virtual {v4, v5, v7}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_4

    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v5

    const-string v6, "1"

    move-object/from16 v3, p0

    move-object v7, v14

    invoke-direct/range {v3 .. v9}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    const/4 v10, 0x1

    move-object/from16 v16, v9

    move-object v15, v8

    goto/16 :goto_6

    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v12

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v16}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    const/4 v10, 0x0

    move-object/from16 v16, v9

    move-object v15, v8

    goto/16 :goto_6

    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->i:Lapj;

    iget-object v3, v3, Lapj;->d:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lapk;->a:Z

    if-eqz v3, :cond_11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Not has_permission because %s = %s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSUser"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v17, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lapk;->i:Lapj;

    iget-object v7, v7, Lapj;->d:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    const/4 v10, 0x0

    goto/16 :goto_5

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->i:Lapj;

    iget-object v3, v3, Lapj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v4, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v0, v19

    if-ne v0, v4, :cond_15

    const/4 v3, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->e:Laoy;

    move/from16 v0, v19

    invoke-virtual {v5, v4, v0}, Laoy;->a(II)Z

    move-result v4

    if-nez v3, :cond_13

    if-eqz v4, :cond_16

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lapk;->a:Z

    if-eqz v5, :cond_14

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has_permission because either isSameUser (%b) or isSameSignature (%b)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "GLSUser"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v10

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    const/4 v10, 0x1

    goto/16 :goto_5

    :cond_15
    const/4 v3, 0x0

    goto :goto_7

    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->i:Lapj;

    iget-object v3, v3, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->c:Landroid/accounts/Account;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lapk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "1"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->e:Laoy;

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Laoy;->a(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->e:Laoy;

    invoke-virtual {v3, v4}, Laoy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_17
    if-nez v3, :cond_18

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :cond_18
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "perm."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->i:Lapj;

    iget-object v4, v4, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v4, v5, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "1"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, v17

    move/from16 v12, v19

    move-object/from16 v14, v20

    invoke-direct/range {v10 .. v16}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :cond_19
    const-string v3, "1"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "perm."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->i:Lapj;

    iget-object v4, v4, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v4, v5, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "1"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, v17

    move/from16 v12, v19

    move-object/from16 v14, v20

    invoke-direct/range {v10 .. v16}, Lapk;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :cond_1a
    const-string v3, "1"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lapk;->a:Z

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has_permission: %b - tokenType: %s, uid: %d, packageName:  %s)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSUser"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v17, v5, v6

    const/4 v6, 0x2

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object v20, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_1b
    move-object/from16 v16, v9

    move-object v15, v8

    goto/16 :goto_6

    :cond_1c
    move-object/from16 v16, v9

    move-object v15, v8

    goto/16 :goto_6

    :cond_1d
    move-object/from16 v3, v17

    goto/16 :goto_2

    :cond_1e
    move-object/from16 v17, v3

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 15

    const-string v1, "ac2dm"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lapk;->e:Laoy;

    invoke-virtual {v0}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v4, p1

    move-object/from16 v11, p2

    invoke-virtual/range {v0 .. v14}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lapk;->b()V

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 15

    const-string v1, "ac2dm"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lapk;->e:Laoy;

    invoke-virtual {v0}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v11, p3

    invoke-virtual/range {v0 .. v14}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lapk;->b()V

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZLandroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 16

    :try_start_0
    const-string v2, "LSID"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lapk;->e:Laoy;

    invoke-virtual {v1}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move/from16 v6, p8

    move/from16 v7, p9

    move-object/from16 v11, p10

    invoke-virtual/range {v1 .. v15}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lapk;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "%s LSID wasn\'t retrieved due to status: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lapk;->k:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v2

    invoke-virtual {v2}, Laso;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GLSUser"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v2, Laso;->b:Laso;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lapk;->i:Lapj;

    iget-object v3, v3, Lapj;->a:Landroid/content/Context;

    const-string v4, "device_country"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->e:Laoy;

    iget-object v4, v4, Laoy;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->i:Lapj;

    iget-object v5, v5, Lapj;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lapk;->c:Landroid/accounts/Account;

    const-string v7, "parent_aid"

    invoke-virtual {v5, v6, v7}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lape;

    move-object/from16 v0, p0

    iget-object v7, v0, Lapk;->e:Laoy;

    invoke-direct {v6, v7}, Lape;-><init>(Laoy;)V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lape;->e(Ljava/lang/String;)Lape;

    move-result-object v6

    new-instance v7, Lbhv;

    move-object/from16 v0, p0

    iget-object v8, v0, Lapk;->e:Laoy;

    iget-object v8, v8, Laoy;->a:Landroid/content/Context;

    invoke-direct {v7, v8}, Lbhv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lbhv;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lape;->c(Ljava/lang/String;)Lape;

    move-result-object v6

    invoke-virtual {v6, v4}, Lape;->a(Ljava/lang/String;)Lape;

    move-result-object v4

    invoke-virtual {v4, v3}, Lape;->b(Ljava/lang/String;)Lape;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->f:Z

    invoke-virtual {v3, v4}, Lape;->a(Z)Lape;

    move-result-object v3

    const-string v4, "LSID"

    invoke-virtual {v3, v4, v1}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v3, Lapt;->z:Lapt;

    move-object/from16 v0, p2

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v3, Lapt;->y:Lapt;

    move-object/from16 v0, p3

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v3, Lapt;->B:Lapt;

    move-object/from16 v0, p4

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v3, Lapt;->J:Lapt;

    move/from16 v0, p5

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    sget-object v3, Lapt;->I:Lapt;

    move/from16 v0, p6

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    sget-object v3, Lapt;->E:Lapt;

    move-object/from16 v0, p7

    invoke-virtual {v1, v3, v0}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Lape;->b(Z)Lape;

    move-result-object v1

    invoke-virtual {v1, v5}, Lape;->d(Ljava/lang/String;)Lape;

    move-result-object v1

    new-instance v3, Lorg/json/JSONStringer;

    invoke-direct {v3}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v3}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    iget-object v1, v1, Lape;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v5

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lapk;->a:Z

    if-eqz v2, :cond_2

    const-string v2, "GLSUser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " createProfile JSONException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v2, Laso;->m:Laso;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    :try_start_1
    sget-object v1, Lapg;->aA:Lapg;

    invoke-virtual {v1}, Lapg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    sget-object v1, Lapg;->aB:Lapg;

    invoke-virtual {v1}, Lapg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->e:Laoy;

    iget v4, v4, Laoy;->d:I

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    invoke-virtual {v3}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    move-object/from16 v0, p0

    iget-object v1, v0, Lapk;->i:Lapj;

    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->e:Laoy;

    iget-object v5, v5, Laoy;->c:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lapj;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lapk;->i:Lapj;

    sget-object v1, Laqc;->d:Lbfy;

    invoke-virtual {v1}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v6, "createProfile"

    invoke-virtual {v5, v1, v3, v6, v4}, Lapj;->b(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " createProfile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lapk;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lapc;->a(Lorg/json/JSONObject;)Laso;

    move-result-object v3

    sget-object v4, Laso;->a:Laso;

    if-eq v4, v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lapk;->a:Z

    if-eqz v4, :cond_4

    const-string v4, "%s createProfile SetupServlet request failed with status: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lapk;->k:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v3}, Laso;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GLSUser"

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    sget-object v4, Lapu;->i:Lapu;

    invoke-virtual {v4}, Lapu;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, Lapu;->i:Lapu;

    invoke-virtual {v4}, Lapu;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lapk;->f(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lapk;->a:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lapk;->k:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " Requesting Google to grant "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " access to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on behalf of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "GLSUser"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, 0x0

    if-eqz p3, :cond_2

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lapk;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lapk;->b(Ljava/lang/String;)V

    move/from16 v16, v2

    :goto_0
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move/from16 v8, p6

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-virtual/range {v2 .. v15}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v5

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v2, p0

    move-object/from16 v6, p1

    move/from16 v7, p7

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    invoke-virtual/range {v2 .. v10}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;ILjava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v16, :cond_1

    invoke-direct/range {p0 .. p0}, Lapk;->b()V

    :cond_1
    return-object v2

    :cond_2
    if-eqz p4, :cond_3

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lapk;->g:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lapk;->h:Ljava/lang/String;

    :cond_3
    move/from16 v16, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLandroid/os/Bundle;ZLjava/lang/String;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/List;
    .locals 7

    new-instance v3, Lape;

    iget-object v1, p0, Lapk;->e:Laoy;

    invoke-direct {v3, v1}, Lape;-><init>(Laoy;)V

    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->a:Landroid/content/Context;

    const-string v2, "device_country"

    const/4 v4, 0x0

    invoke-static {v1, v2, v4}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lbhv;

    iget-object v4, p0, Lapk;->e:Laoy;

    iget-object v4, v4, Laoy;->a:Landroid/content/Context;

    invoke-direct {v2, v4}, Lbhv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lbhv;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lapk;->e:Laoy;

    iget-object v4, v4, Laoy;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1}, Lape;->b(Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v5, Lapt;->D:Lapt;

    move/from16 v0, p10

    invoke-virtual {v1, v5, v0}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    invoke-virtual {v1, v4}, Lape;->a(Ljava/lang/String;)Lape;

    move-result-object v1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v5, Lapt;->G:Lapt;

    invoke-virtual {v1, v5, v4}, Lape;->a(Lapt;I)Lape;

    move-result-object v1

    iget-object v4, p0, Lapk;->e:Laoy;

    iget v4, v4, Laoy;->d:I

    sget-object v5, Lapt;->s:Lapt;

    invoke-virtual {v1, v5, v4}, Lape;->a(Lapt;I)Lape;

    const-string v4, "HOSTED_OR_GOOGLE"

    const-string v5, "accountType"

    invoke-virtual {v1, v5, v4}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    move-result-object v1

    iget-object v4, p0, Lapk;->e:Laoy;

    invoke-virtual {v4, p3}, Laoy;->b(I)Z

    move-result v4

    sget-object v5, Lapt;->t:Lapt;

    invoke-virtual {v1, v5, v4}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    invoke-virtual {v1, p2}, Lape;->e(Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v4, Lapt;->p:Lapt;

    invoke-virtual {v1, v4, p7}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    sget-object v4, Lapt;->C:Lapt;

    move/from16 v0, p9

    invoke-virtual {v1, v4, v0}, Lape;->a(Lapt;Z)Lape;

    move-result-object v1

    invoke-virtual {v1, p5}, Lape;->b(Z)Lape;

    move-result-object v1

    sget-object v4, Lapt;->c:Lapt;

    invoke-virtual {v1, v4, p1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    const-string v4, "android"

    sget-object v5, Lapt;->i:Lapt;

    invoke-virtual {v1, v5, v4}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    invoke-virtual {v1, v2}, Lape;->c(Ljava/lang/String;)Lape;

    move-result-object v1

    iget-boolean v2, p0, Lapk;->f:Z

    invoke-virtual {v1, v2}, Lape;->a(Z)Lape;

    if-eqz p11, :cond_0

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_0
    :goto_0
    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v4, "parent_aid"

    invoke-virtual {v1, v2, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v3, v1}, Lape;->d(Ljava/lang/String;)Lape;

    :cond_1
    if-eqz p12, :cond_2

    invoke-virtual/range {p12 .. p12}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lapt;->K:Lapt;

    invoke-virtual {v3, v2, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    invoke-virtual/range {p12 .. p12}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, v3, Lape;->a:Ljava/util/List;

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v5, Lapt;->L:Lapt;

    invoke-virtual {v5}, Lapt;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p13, :cond_3

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->a()Z

    move-result v4

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lapt;->N:Lapt;

    if-eqz v4, :cond_8

    const-string v1, "1"

    :goto_1
    invoke-virtual {v3, v5, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v5

    sget-object v6, Lapt;->M:Lapt;

    if-eqz v4, :cond_9

    const-string v1, ""

    :goto_2
    invoke-virtual {v5, v6, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_4

    if-eqz p3, :cond_b

    :cond_4
    sget-object v1, Lapt;->q:Lapt;

    sget-object v2, Lapt;->r:Lapt;

    invoke-virtual {v3, v1, v2, p4}, Lape;->a(Lapt;Lapt;Ljava/lang/String;)Lape;

    if-eqz p6, :cond_b

    sget-object v1, Lapt;->v:Lapt;

    invoke-virtual {v1}, Lapt;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lapt;->v:Lapt;

    invoke-virtual {v3, v2, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    const-string v1, "clientPackageName"

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    sget-object v2, Lapt;->P:Lapt;

    sget-object v4, Lapt;->Q:Lapt;

    invoke-virtual {v3, v2, v4, v1}, Lape;->a(Lapt;Lapt;Ljava/lang/String;)Lape;

    :cond_5
    invoke-virtual {p6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "_opt_"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "_opt_"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    goto :goto_3

    :cond_7
    sget-object v4, Lapt;->k:Lapt;

    invoke-virtual {v3, v4, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    move-result-object v1

    sget-object v4, Lapt;->j:Lapt;

    invoke-virtual {v1, v4, v2}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    goto/16 :goto_0

    :cond_8
    const-string v1, "0"

    goto :goto_1

    :cond_9
    move-object v1, v2

    goto :goto_2

    :cond_a
    sget-object v4, Lapt;->u:Lapt;

    invoke-virtual {v4}, Lapt;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lape;->a(Ljava/lang/String;Ljava/lang/String;)Lape;

    goto :goto_3

    :cond_b
    iget-object v1, p0, Lapk;->h:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lapk;->h:Ljava/lang/String;

    sget-object v2, Lapt;->d:Lapt;

    invoke-virtual {v3, v2, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    :goto_4
    sget-object v1, Lapt;->O:Lapt;

    move-object/from16 v0, p14

    invoke-virtual {v3, v1, v0}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v3, Lape;->a:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1

    :cond_c
    if-nez p8, :cond_d

    iget-object v1, p0, Lapk;->g:Ljava/lang/String;

    sget-object v2, Lapt;->b:Lapt;

    invoke-virtual {v3, v2, v1}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    goto :goto_4

    :cond_d
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    sget-object v1, Lapt;->w:Lapt;

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Lape;->a(Lapt;Z)Lape;

    :cond_e
    sget-object v1, Lapt;->b:Lapt;

    invoke-virtual {v3, v1, p8}, Lape;->a(Lapt;Ljava/lang/String;)Lape;

    goto :goto_4
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/Map;
    .locals 18

    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    const/16 v16, 0x0

    invoke-direct/range {p0 .. p0}, Lapk;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " No secrets - returning BAD_AUTH %s %s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lapk;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lapk;->i:Lapj;

    invoke-virtual {v2, v1}, Lapj;->a(Ljava/lang/String;)V

    sget-object v1, Laso;->b:Laso;

    sget-object v2, Laso;->M:Ljava/lang/String;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, v17

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v4

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v6, p4

    move-object/from16 v7, p9

    move/from16 v8, p5

    move-object/from16 v9, p3

    move/from16 v10, p6

    move/from16 v11, p7

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    invoke-virtual/range {v1 .. v15}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLandroid/os/Bundle;ZLjava/lang/String;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    :try_start_0
    new-instance v3, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v3, v7}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lapk;->i:Lapj;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lapj;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lapk;->i:Lapj;

    sget-object v2, Laqc;->a:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lapk;->b:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ":"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v1 .. v6}, Lapj;->a(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const v3, 0x320d2

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Laox;->a(ILjava/lang/String;)V

    invoke-static {v2}, Lapk;->g(Ljava/lang/String;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    :try_start_2
    const-string v2, "x-status"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v2, v1}, Lapk;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V

    sget-object v1, Lapu;->c:Lapu;

    invoke-virtual {v1}, Lapu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-static {v1}, Lapc;->a(Ljava/lang/String;)Laso;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    :goto_1
    const v4, 0x320d2

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object p1, v5, v2

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " getAuthtoken("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") -> "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lapk;->a(Ljava/lang/String;)V

    move-object v1, v3

    goto/16 :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    :cond_1
    sget-object v2, Laso;->m:Laso;

    invoke-virtual {v2}, Laso;->ordinal()I

    move-result v2

    goto :goto_2

    :catch_1
    move-exception v1

    move-object/from16 v2, v17

    :goto_3
    :try_start_3
    sget-object v16, Laso;->m:Laso;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IOException in getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lapk;->a(Ljava/lang/String;)V

    sget-object v3, Lapu;->c:Lapu;

    invoke-virtual {v3}, Lapu;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Laso;->m:Laso;

    invoke-virtual {v4}, Laso;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v1, v4}, Lapk;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const v3, 0x320d2

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-nez v16, :cond_2

    const/4 v1, 0x0

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " getAuthtoken("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lapk;->a(Ljava/lang/String;)V

    move-object v1, v2

    goto/16 :goto_0

    :cond_2
    sget-object v1, Laso;->m:Laso;

    invoke-virtual {v1}, Laso;->ordinal()I

    move-result v1

    goto :goto_4

    :catchall_0
    move-exception v1

    move-object v2, v1

    const v3, 0x320d2

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-nez v16, :cond_3

    const/4 v1, 0x0

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lapk;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " getAuthtoken("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lapk;->a(Ljava/lang/String;)V

    throw v2

    :cond_3
    sget-object v1, Laso;->m:Laso;

    invoke-virtual {v1}, Laso;->ordinal()I

    move-result v1

    goto :goto_5

    :catch_2
    move-exception v1

    move-object v2, v3

    goto/16 :goto_3

    :cond_4
    move-object/from16 v1, v16

    goto/16 :goto_1
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lapk;->i:Lapj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->i:Lapj;

    invoke-virtual {v0, p1}, Lapj;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final a(Ljava/util/Map;Z)V
    .locals 4

    const/4 v2, 0x0

    sget-object v0, Lapu;->r:Lapu;

    invoke-virtual {v0}, Lapu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lapk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lapk;->g:Ljava/lang/String;

    iput-object v2, p0, Lapk;->h:Ljava/lang/String;

    iput-object v2, p0, Lapk;->m:Ljava/lang/String;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lapk;->i:Lapj;

    iget-object v1, v1, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v0, p0, Lapk;->f:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lapk;->c:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lapk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lapk;->i:Lapj;

    iget-object v0, v0, Lapj;->a:Landroid/content/Context;

    iget-object v1, p0, Lapk;->b:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lapq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapk;->h:Ljava/lang/String;

    iget-object v0, p0, Lapk;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lapq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapk;->m:Ljava/lang/String;

    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    iput-object v0, p0, Lapk;->l:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapk;->g:Ljava/lang/String;

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lapk;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapk;->h:Ljava/lang/String;

    iput-object p1, p0, Lapk;->l:Ljava/lang/String;

    return-void
.end method

.method final d(Ljava/lang/String;)V
    .locals 4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "flags"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lapk;->g:Ljava/lang/String;

    :goto_0
    iget-boolean v2, p0, Lapk;->f:Z

    if-eqz v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-gt v2, v3, :cond_4

    const-string v2, "oauthAccessToken"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    iget-object v2, p0, Lapk;->m:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "sha1hash"

    iget-object v3, p0, Lapk;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    sget-object v2, Lapu;->i:Lapu;

    invoke-virtual {v2}, Lapu;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lapk;->i:Lapj;

    iget-object v2, v2, Lapj;->c:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lapk;->c:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v0, v1}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lapk;->l:Ljava/lang/String;

    return-void

    :cond_3
    iget-object v0, p0, Lapk;->h:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v2, "oauthAccessToken"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
