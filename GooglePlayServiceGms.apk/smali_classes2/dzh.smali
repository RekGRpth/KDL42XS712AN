.class public final Ldzh;
.super Ldxh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ldxh;-><init>()V

    iput-object p1, p0, Ldzh;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzh;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    iput p1, p0, Ldzh;->c:I

    iput p2, p0, Ldzh;->e:I

    invoke-virtual {p0}, Ldzh;->notifyDataSetChanged()V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Ldzh;->b:Landroid/view/LayoutInflater;

    sget v1, Lxc;->p:I

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Ldzi;

    invoke-direct {v0, p0, p2}, Ldzi;-><init>(Ldzh;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, v0, Ldzi;->d:Ldzh;

    iget v1, v1, Ldzh;->e:I

    if-lez v1, :cond_0

    iget-object v1, v0, Ldzi;->d:Ldzh;

    iget-object v1, v1, Ldzh;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxf;->D:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Ldzi;->d:Ldzh;

    iget v4, v4, Ldzh;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, v0, Ldzi;->d:Ldzh;

    iget v4, v4, Ldzh;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Ldzi;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Ldzi;->b:Landroid/widget/ProgressBar;

    iget-object v3, v0, Ldzi;->d:Ldzh;

    iget v3, v3, Ldzh;->e:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v2, v0, Ldzi;->b:Landroid/widget/ProgressBar;

    iget-object v3, v0, Ldzi;->d:Ldzh;

    iget v3, v3, Ldzh;->c:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    sget v2, Lxf;->C:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, v0, Ldzi;->d:Ldzh;

    iget v4, v4, Ldzh;->c:I

    int-to-float v4, v4

    iget-object v5, v0, Ldzi;->d:Ldzh;

    iget v5, v5, Ldzh;->e:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Ldzi;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzi;

    goto :goto_0
.end method
