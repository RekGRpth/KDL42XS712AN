.class final Lgsc;
.super Lhgn;
.source "SourceFile"


# instance fields
.field final synthetic a:Lgsb;


# direct methods
.method public constructor <init>(Lgsb;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lgsc;->a:Lgsb;

    invoke-direct {p0, p2}, Lhgn;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 8

    const/4 v4, 0x2

    const/4 v7, 0x0

    iget-object v0, p0, Lgsc;->b:Landroid/content/Context;

    invoke-static {v0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Fetching auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v0, p0, Lgsc;->a:Lgsb;

    iget-object v1, p0, Lgsc;->b:Landroid/content/Context;

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->b(Lgsb;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lgsc;->a:Lgsb;

    invoke-static {v3}, Lgsb;->c(Lgsb;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgsb;->a(Lgsb;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v0, p0, Lgsc;->a:Lgsb;

    invoke-static {v0}, Lgsb;->d(Lgsb;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GoogleAuthUtil returned a null token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgsc;->a:Lgsb;

    invoke-static {v0, v4}, Lgsb;->a(Lgsb;I)V

    :goto_0
    return-object v7

    :catch_0
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UserRecoverableAuthException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgsc;->a:Lgsb;

    invoke-static {v0, v4}, Lgsb;->a(Lgsb;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "IOException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgsc;->a:Lgsb;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lgsb;->a(Lgsb;I)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GoogleAuthException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgsc;->a:Lgsb;

    invoke-static {v0, v4}, Lgsb;->a(Lgsb;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgsc;->b:Landroid/content/Context;

    invoke-static {v0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgsc;->a:Lgsb;

    invoke-static {v2}, Lgsb;->a(Lgsb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Successfully received auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lgsc;->a:Lgsb;

    invoke-static {v0}, Lgsb;->f(Lgsb;)Lsf;

    move-result-object v0

    new-instance v1, Lgsk;

    iget-object v2, p0, Lgsc;->b:Landroid/content/Context;

    iget-object v3, p0, Lgsc;->a:Lgsb;

    invoke-static {v3}, Lgsb;->e(Lgsb;)I

    move-result v3

    invoke-static {v3}, Lgsb;->a(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lhgm;

    iget-object v5, p0, Lgsc;->a:Lgsb;

    invoke-static {v5}, Lgsb;->c(Lgsb;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lgsc;->a:Lgsb;

    invoke-static {v6}, Lgsb;->d(Lgsb;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lhgm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lgsc;->a:Lgsb;

    invoke-direct {v1, v2, v3, v4, v5}, Lgsk;-><init>(Landroid/content/Context;Ljava/lang/String;Lhgm;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lsf;->a(Lsc;)Lsc;

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lgsc;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
