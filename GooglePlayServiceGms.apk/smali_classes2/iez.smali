.class final Liez;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Liev;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Landroid/net/wifi/WifiManager;

.field private e:I

.field private f:Lhtf;


# direct methods
.method private constructor <init>(Liev;)V
    .locals 2

    iput-object p1, p0, Liez;->a:Liev;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Liez;->b:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Liez;->c:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Liez;->d:Landroid/net/wifi/WifiManager;

    const/16 v0, -0x270f

    iput v0, p0, Liez;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Liez;->f:Lhtf;

    return-void
.end method

.method synthetic constructor <init>(Liev;B)V
    .locals 0

    invoke-direct {p0, p1}, Liez;-><init>(Liev;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->arg2:I

    const/16 v3, 0x10e1

    if-ne v0, v3, :cond_0

    move v10, v1

    :goto_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p1, Landroid/os/Message;->arg2:I

    const/16 v3, 0x2156

    if-ne v0, v3, :cond_1

    move v10, v2

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No wakelock mode specified for command "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v3, Licn;->b:Licn;

    invoke-virtual {v0, v3}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_3

    :goto_1
    invoke-interface {v0, v1}, Lick;->a(Z)V

    iget-object v0, p0, Liez;->b:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->q:Lifb;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Liez;->a:Liev;

    iget-boolean v1, v1, Liev;->A:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->p:Lifa;

    :goto_2
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->r:Liey;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->t:Lhsk;

    invoke-virtual {v0}, Lhsk;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_2

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "Wake lock is held after the callback runner quit."

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v0, Licj;->a:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wake lock is still held. Check for program errors."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->o:Lifa;

    goto :goto_2

    :cond_5
    :goto_3
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->t:Lhsk;

    invoke-virtual {v0}, Lhsk;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->t:Lhsk;

    invoke-virtual {v0}, Lhsk;->b()V

    goto :goto_3

    :pswitch_1
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->l:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Livi;

    invoke-interface {v1, v0}, Lick;->a(Livi;)V

    :cond_6
    :goto_4
    if-eqz v10, :cond_7

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->t:Lhsk;

    invoke-virtual {v0}, Lhsk;->b()V

    :cond_7
    return-void

    :pswitch_2
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->m:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Livi;

    invoke-interface {v1, v0}, Lick;->b(Livi;)V

    goto :goto_4

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v9, v0

    check-cast v9, Lifd;

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->n:Licp;

    iget v5, v9, Lifd;->a:I

    iget v6, v9, Lifd;->b:I

    iget v7, v9, Lifd;->c:I

    iget-boolean v8, v9, Lifd;->d:Z

    new-instance v0, Licq;

    sget-object v2, Licn;->c:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v8}, Licq;-><init>(Licp;Licn;JIIIZ)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    iget v1, v9, Lifd;->a:I

    iget v2, v9, Lifd;->b:I

    iget v3, v9, Lifd;->c:I

    iget-boolean v4, v9, Lifd;->d:Z

    iget-object v5, v9, Lifd;->e:Lilx;

    invoke-interface/range {v0 .. v5}, Lick;->a(IIIZLilx;)V

    goto :goto_4

    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_8

    const/16 v0, -0x270f

    iput v0, p0, Liez;->e:I

    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v3

    iget-object v4, p0, Liez;->b:Landroid/telephony/TelephonyManager;

    iget v5, p0, Liez;->e:I

    invoke-virtual {v3, v4, v5, v0, v1}, Lifs;->a(Landroid/telephony/TelephonyManager;IJ)[Lhtf;

    move-result-object v0

    if-eqz v0, :cond_9

    array-length v1, v0

    if-nez v1, :cond_a

    :cond_9
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v6}, Licp;->a(Lhtf;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v6}, Lick;->a(Lhtf;)V

    iput-object v6, p0, Liez;->f:Lhtf;

    goto :goto_4

    :cond_a
    :goto_5
    array-length v1, v0

    if-ge v2, v1, :cond_6

    aget-object v1, v0, v2

    iput-object v1, p0, Liez;->f:Lhtf;

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->n:Licp;

    iget-object v3, p0, Liez;->f:Lhtf;

    invoke-virtual {v1, v3}, Licp;->a(Lhtf;)V

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v1, v1, Liev;->z:Lick;

    iget-object v3, p0, Liez;->f:Lhtf;

    invoke-interface {v1, v3}, Lick;->a(Lhtf;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/telephony/SignalStrength;

    iget-object v1, p0, Liez;->f:Lhtf;

    if-eqz v1, :cond_6

    iget-object v1, p0, Liez;->f:Lhtf;

    instance-of v1, v1, Lhtk;

    if-nez v1, :cond_b

    iget-object v1, p0, Liez;->f:Lhtf;

    instance-of v1, v1, Lhtc;

    if-eqz v1, :cond_6

    :cond_b
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    iput v0, p0, Liez;->e:I

    :goto_6
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->n:Licp;

    iget v5, p0, Liez;->e:I

    new-instance v0, Lidk;

    sget-object v2, Licn;->h:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lidk;-><init>(Licp;Licn;JI)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Liez;->f:Lhtf;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget v3, p0, Liez;->e:I

    invoke-virtual {v0, v1, v2, v3}, Lhtf;->a(JI)Lhtf;

    move-result-object v0

    iput-object v0, p0, Liez;->f:Lhtf;

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    iget-object v1, p0, Liez;->f:Lhtf;

    invoke-interface {v0, v1}, Lick;->a(Lhtf;)V

    goto/16 :goto_4

    :cond_c
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    iput v0, p0, Liez;->e:I

    goto :goto_6

    :pswitch_6
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v2}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v2}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_7
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_8
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v4}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v4}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_9
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v5}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v5}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_a
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_b
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_c
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_d
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_e
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_f
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_10
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_11
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Licp;->a(I)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Lick;->a(I)V

    goto/16 :goto_4

    :pswitch_12
    iget-object v0, p0, Liez;->a:Liev;

    iget-boolean v0, v0, Liev;->A:Z

    if-nez v0, :cond_6

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhue;

    new-instance v2, Lifn;

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->r:Liey;

    invoke-virtual {v0}, Liey;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lifn;-><init>(Landroid/location/Location;JI)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v2}, Licp;->a(Lidr;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v2}, Lick;->a(Lidr;)V

    goto/16 :goto_4

    :pswitch_13
    iget-object v0, p0, Liez;->a:Liev;

    iget-boolean v0, v0, Liev;->A:Z

    if-eqz v0, :cond_6

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhue;

    new-instance v2, Lifn;

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->r:Liey;

    invoke-virtual {v0}, Liey;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lifn;-><init>(Landroid/location/Location;JI)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v2}, Licp;->a(Lidr;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v2}, Lick;->a(Lidr;)V

    goto/16 :goto_4

    :pswitch_14
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, [Lhuv;

    if-eqz v6, :cond_d

    array-length v0, v6

    if-lez v0, :cond_d

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->n:Licp;

    aget-object v5, v6, v2

    new-instance v0, Lics;

    sget-object v2, Licn;->r:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lics;-><init>(Licp;Licn;JLhuv;)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    :cond_d
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v6}, Lick;->a([Lhuv;)V

    goto/16 :goto_4

    :pswitch_15
    iget v0, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_e

    move v0, v1

    :goto_7
    iget v3, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_f

    :goto_8
    iget-object v2, p0, Liez;->a:Liev;

    iget-object v2, v2, Liev;->n:Licp;

    invoke-virtual {v2, v0}, Licp;->c(Z)V

    iget-object v2, p0, Liez;->a:Liev;

    iget-object v2, v2, Liev;->z:Lick;

    invoke-interface {v2, v0, v1}, Lick;->a(ZZ)V

    goto/16 :goto_4

    :cond_e
    move v0, v2

    goto :goto_7

    :cond_f
    move v1, v2

    goto :goto_8

    :pswitch_16
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "scale"

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const-string v3, "level"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string v3, "plugged"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_10

    move v7, v1

    :goto_9
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->n:Licp;

    new-instance v0, Lidi;

    sget-object v2, Licn;->f:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v7}, Lidi;-><init>(Licp;Licn;JIIZ)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v5, v6, v7}, Lick;->a(IIZ)V

    goto/16 :goto_4

    :cond_10
    move v7, v2

    goto :goto_9

    :pswitch_17
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_11

    :goto_a
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v1}, Licp;->b(Z)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v1}, Lick;->b(Z)V

    goto/16 :goto_4

    :cond_11
    move v1, v2

    goto :goto_a

    :pswitch_18
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_12

    :goto_b
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    invoke-virtual {v0, v1}, Licp;->a(Z)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v1}, Lick;->c(Z)V

    goto/16 :goto_4

    :cond_12
    move v1, v2

    goto :goto_b

    :pswitch_19
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_13

    move v5, v1

    :goto_c
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->n:Licp;

    new-instance v0, Lidl;

    sget-object v2, Licn;->i:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lidl;-><init>(Licp;Licn;JZ)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0, v5}, Lick;->d(Z)V

    goto/16 :goto_4

    :cond_13
    move v5, v2

    goto :goto_c

    :pswitch_1a
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->k:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Livi;

    invoke-interface {v1, v0}, Lick;->c(Livi;)V

    goto/16 :goto_4

    :pswitch_1b
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->j:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Livi;

    invoke-interface {v1, v0}, Lick;->d(Livi;)V

    goto/16 :goto_4

    :pswitch_1c
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, p0, Liez;->c:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Liez;->d:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Liez;->a:Liev;

    iget-object v3, v3, Liev;->z:Lick;

    invoke-static {v0, v1, v2, v3}, Liev;->a(Liev;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lick;)V

    goto/16 :goto_4

    :pswitch_1d
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->p:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lids;

    invoke-interface {v1, v0}, Lick;->a(Lids;)V

    goto/16 :goto_4

    :pswitch_1e
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v1, Licn;->P:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhud;

    invoke-interface {v1, v0}, Lick;->a(Lhud;)V

    goto/16 :goto_4

    :pswitch_1f
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhlt;

    invoke-interface {v1, v0}, Lick;->a(Lhlt;)V

    goto/16 :goto_4

    :pswitch_20
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhue;

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v2, v1, Liev;->z:Lick;

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v2, v1, v3, v4}, Lick;->a(IJ)V

    goto/16 :goto_4

    :pswitch_21
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v1, v0, Liev;->z:Lick;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-interface {v1, v0}, Lick;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto/16 :goto_4

    :pswitch_22
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Liew;

    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    iget v1, v5, Liew;->a:I

    iget v2, v5, Liew;->b:I

    iget-boolean v3, v5, Liew;->c:Z

    iget-boolean v4, v5, Liew;->d:Z

    iget-object v5, v5, Liew;->e:Lilx;

    invoke-interface/range {v0 .. v5}, Lick;->a(IIZZLilx;)V

    goto/16 :goto_4

    :pswitch_23
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhue;

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v2, v1, Liev;->z:Lick;

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lick;->a(ZLjava/lang/String;)V

    goto/16 :goto_4

    :pswitch_24
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhue;

    iget-object v1, p0, Liez;->a:Liev;

    iget-object v2, v1, Liev;->z:Lick;

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Lidw;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    invoke-interface {v2, v1, v0}, Lick;->a(Lidw;Ljava/lang/Object;)V

    goto/16 :goto_4

    :pswitch_25
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_4

    :pswitch_26
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0}, Lick;->b()V

    goto/16 :goto_4

    :pswitch_27
    iget-object v0, p0, Liez;->a:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0}, Lick;->c()V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_13
        :pswitch_17
        :pswitch_2
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1c
        :pswitch_1b
        :pswitch_1d
        :pswitch_1e
        :pswitch_21
        :pswitch_22
        :pswitch_1f
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_20
    .end packed-switch
.end method
