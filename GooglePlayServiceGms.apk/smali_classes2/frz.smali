.class public final Lfrz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lget;

.field public final b:Lgbh;

.field private final c:Lgbf;


# direct methods
.method public constructor <init>(Lbmi;Lbmi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lget;

    invoke-direct {v0, p1}, Lget;-><init>(Lbmi;)V

    iput-object v0, p0, Lfrz;->a:Lget;

    new-instance v0, Lgbf;

    invoke-direct {v0, p2}, Lgbf;-><init>(Lbmi;)V

    iput-object v0, p0, Lfrz;->c:Lgbf;

    new-instance v0, Lgbh;

    invoke-direct {v0, p2}, Lgbh;-><init>(Lbmi;)V

    iput-object v0, p0, Lfrz;->b:Lgbh;

    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;)Landroid/content/pm/ApplicationInfo;
    .locals 9

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_0
    move v6, v3

    move-object v4, v5

    :goto_1
    if-ge v6, v2, :cond_0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {p0, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x40

    invoke-virtual {p0, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v8, :cond_4

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v8, v8

    if-lez v8, :cond_4

    iget-object v7, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lbox;->a([BZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v4, :cond_2

    move-object v4, v5

    :cond_0
    return-object v4

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_2
    move-object v4, v1

    :cond_3
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_4
    move-object v1, v4

    goto :goto_2
.end method

.method private static a()Ljava/util/ArrayList;
    .locals 4

    sget-object v0, Lfsr;->H:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    :try_start_0
    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2

    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lfrz;->c:Lgbf;

    invoke-static/range {p1 .. p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-static {v3, v0, v2}, Lgbf;->a(Lgbg;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v1, Lgbf;->a:Lbmi;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/plus/service/lso/AuthApps;

    move-object/from16 v2, p2

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/lso/AuthApps;

    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;->f()I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps;->f()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    move v3, v1

    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {}, Lfrz;->a()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const-string v1, "SHA1"

    invoke-static {v1}, Lbox;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0b03a5    # com.google.android.gms.R.string.plus_manage_moment_acl_separator

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v1, 0x0

    move v5, v1

    :goto_2
    if-ge v5, v3, :cond_a

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbkf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "display_name"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v4, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "icon_url"

    invoke-virtual {v14, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "revoke_handle"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->i()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v4, v2

    :cond_1
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v4, 0x1

    :cond_2
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v16

    if-lez v16, :cond_3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_6

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lfmq;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_6
    :goto_4
    if-nez v2, :cond_7

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    const-string v15, "application_id"

    invoke-virtual {v14, v15, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "is_aspen"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_8

    const-string v2, "scopes"

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-static {v10, v11, v1}, Lfrz;->a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string v2, "display_name"

    invoke-virtual {v10, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "application_info"

    invoke-static {v1}, Lfwm;->a(Landroid/content/pm/ApplicationInfo;)[B

    move-result-object v1

    invoke-virtual {v14, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_9
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_2

    :cond_a
    new-instance v1, Lfsa;

    invoke-direct {v1}, Lfsa;-><init>()V

    invoke-static {v8, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object v1, Lfwm;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v4

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v3, :cond_b

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    invoke-virtual {v4, v1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_b
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v15

    goto :goto_4
.end method
