.class public final Lbst;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcfz;

.field final b:Lcby;

.field private final c:Lcdu;

.field private final d:Lcon;


# direct methods
.method public constructor <init>(Lcdu;Lcfz;Lcon;Lcby;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lbst;->c:Lcdu;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbst;->a:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcon;

    iput-object v0, p0, Lbst;->d:Lcon;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    iput-object v0, p0, Lbst;->b:Lcby;

    return-void
.end method


# virtual methods
.method public final a()Lbsy;
    .locals 7

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x0

    invoke-virtual {p0, v5, v0}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, v5}, Lcfz;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    new-instance v0, Lbsy;

    iget-object v1, p0, Lbst;->c:Lcdu;

    iget-object v2, p0, Lbst;->a:Lcfz;

    iget-object v3, p0, Lbst;->d:Lcon;

    const/high16 v6, 0x20000000

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lbsy;-><init>(Lcdu;Lcfz;Lcon;Lbst;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lbsy;
    .locals 7

    invoke-virtual {p0, p1}, Lbst;->b(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, v5}, Lcfz;->e(Ljava/lang/String;)V

    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v2, 0x0

    invoke-virtual {p0, v5, v2}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    invoke-static {v1, v0}, Lcbr;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    new-instance v0, Lbsy;

    iget-object v1, p0, Lbst;->c:Lcdu;

    iget-object v2, p0, Lbst;->a:Lcfz;

    iget-object v3, p0, Lbst;->d:Lcon;

    const/high16 v6, 0x30000000

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lbsy;-><init>(Lcdu;Lcfz;Lcon;Lbst;Ljava/lang/String;I)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    throw v1

    :catchall_1
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    throw v2

    :catchall_2
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    throw v1
.end method

.method final a(Ljava/lang/String;I)Ljava/io/File;
    .locals 3

    if-nez p2, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lbst;->b:Lcby;

    invoke-interface {v1}, Lcby;->c()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lbst;->b:Lcby;

    invoke-interface {v0}, Lcby;->d()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot get path for a file on encrypted shared storage because shared storage is not available."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported storageType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 8

    const/4 v6, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, v3, Lcfx;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, v4}, Lcfz;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lbud; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v1, v3, Lcfx;->c:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {p0, v1, v5}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    iget-object v0, v3, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-static {v0, v2, v1}, Lcbr;->a(Ljava/security/Key;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v3, v4}, Lcfx;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcfx;->k()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v0, p0, Lbst;->a:Lcfz;

    invoke-interface {v0, v4}, Lcfz;->f(Ljava/lang/String;)V
    :try_end_6
    .catch Lbud; {:try_start_6 .. :try_end_6} :catch_0

    :cond_1
    iget-object v0, p0, Lbst;->d:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    iput-wide v0, v3, Lcfx;->g:J

    invoke-virtual {v3}, Lcfx;->k()V

    iget-object v0, v3, Lcfx;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v6}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_8
    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, v4}, Lcfz;->f(Ljava/lang/String;)V

    throw v0
    :try_end_8
    .catch Lbud; {:try_start_8 .. :try_end_8} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_1
    if-eqz v2, :cond_2

    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :cond_2
    if-eqz v1, :cond_3

    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_3
    throw v0

    :catchall_3
    move-exception v0

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_4
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_4
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :catchall_5
    move-exception v0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4

    iget-object v0, p0, Lbst;->a:Lcfz;

    invoke-interface {v0, p1}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v1, Lcfx;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, v1, Lcfx;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v0, v1, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    const/4 v0, 0x2

    iget-object v3, v1, Lcfx;->d:Ljavax/crypto/SecretKey;

    invoke-virtual {v2, v0, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    new-instance v3, Ljava/io/FileInputStream;

    iget-object v0, v1, Lcfx;->c:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v0, Ljavax/crypto/CipherInputStream;

    invoke-direct {v0, v3, v2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lbst;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->d(Ljava/lang/String;)Lcfx;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, v1, Lcfx;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, v1, Lcfx;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lbst;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "ContentManager"

    const-string v3, "Content with hash %s was deleted outside of ContentManager."

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    invoke-static {v2, v1, v3, v4}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
