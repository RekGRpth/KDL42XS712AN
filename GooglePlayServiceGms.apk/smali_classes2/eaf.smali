.class public final Leaf;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/content/res/Resources;

.field private final i:Landroid/view/LayoutInflater;

.field private final j:Leag;

.field private final k:Z

.field private final l:I

.field private final m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldvn;Leag;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Leaf;-><init>(Ldvn;Leag;I)V

    return-void
.end method

.method public constructor <init>(Ldvn;Leag;I)V
    .locals 2

    sget v0, Lxb;->j:I

    invoke-direct {p0, p1, v0, p3}, Ldwx;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Leaf;->g:Landroid/content/Context;

    invoke-virtual {p1}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Leaf;->h:Landroid/content/res/Resources;

    invoke-virtual {p1}, Ldvn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leaf;->i:Landroid/view/LayoutInflater;

    iput-object p2, p0, Leaf;->j:Leag;

    invoke-virtual {p1}, Ldvn;->l()Z

    move-result v0

    iput-boolean v0, p0, Leaf;->k:Z

    iget-object v0, p0, Leaf;->h:Landroid/content/res/Resources;

    sget v1, Lwy;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Leaf;->l:I

    iget-object v0, p0, Leaf;->h:Landroid/content/res/Resources;

    sget v1, Lwy;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Leaf;->m:I

    return-void
.end method

.method static synthetic a(Leaf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leaf;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Leaf;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Leaf;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Leaf;)Leag;
    .locals 1

    iget-object v0, p0, Leaf;->j:Leag;

    return-object v0
.end method

.method static synthetic d(Leaf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leaf;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 16

    check-cast p4, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-static/range {p1 .. p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Leah;

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v11

    iget-object v1, v9, Leah;->o:Leaf;

    iget-object v1, v1, Leaf;->n:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Leee;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v13

    iget-object v1, v9, Leah;->o:Leaf;

    iget-boolean v1, v1, Leaf;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, v9, Leah;->c:Landroid/view/View;

    iget-object v2, v9, Leah;->o:Leaf;

    iget v2, v2, Leaf;->l:I

    iget-object v3, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    iget-object v4, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    iget-object v5, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    iget-object v1, v9, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    :goto_0
    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v9, Leah;->o:Leaf;

    iget-object v1, v1, Leaf;->h:Landroid/content/res/Resources;

    sget v2, Lxf;->bn:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    :goto_1
    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->x()Ljava/lang/String;

    move-result-object v14

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->h()I

    move-result v1

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->g()I

    move-result v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Match is in an undefined state."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, v9, Leah;->c:Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    iget-object v4, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    iget-object v5, v9, Leah;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    iget-object v1, v9, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v9, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v11}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->e:I

    iget-object v4, v9, Leah;->o:Leaf;

    iget-boolean v4, v4, Ldvj;->d:Z

    if-eqz v4, :cond_1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_2
    iget-object v1, v9, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, v9, Leah;->o:Leaf;

    iget v2, v2, Leaf;->m:I

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, v9, Leah;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_2

    :cond_2
    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->i()Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    goto :goto_1

    :pswitch_0
    iget-object v1, v9, Leah;->o:Leaf;

    iget-object v1, v1, Leaf;->g:Landroid/content/Context;

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Leee;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    sget v1, Lwx;->l:I

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-virtual {v9, v13, v12, v3, v14}, Leah;->a(Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    :goto_3
    iget-object v3, v9, Leah;->o:Leaf;

    iget-boolean v3, v3, Leaf;->k:Z

    if-eqz v3, :cond_8

    iget-object v3, v9, Leah;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v9, Leah;->f:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x6

    if-lt v2, v3, :cond_9

    iget-object v3, v9, Leah;->n:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_9

    iget-object v3, v9, Leah;->o:Leaf;

    iget-object v3, v3, Leaf;->h:Landroid/content/res/Resources;

    sget v4, Lxf;->aE:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v9, Leah;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v9, Leah;->g:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    const/4 v2, 0x0

    if-eqz v14, :cond_a

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->y()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_4

    :cond_3
    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v2

    :cond_4
    :goto_6
    if-eqz v2, :cond_b

    iget-object v3, v9, Leah;->i:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v9, Leah;->h:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, v9, Leah;->h:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    iget-object v2, v9, Leah;->l:Landroid/view/View;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, v9, Leah;->m:Landroid/view/View;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, v9, Leah;->n:Landroid/widget/TextView;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v1, 0xb

    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v9, Leah;->a:Landroid/view/ViewGroup;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    :cond_5
    iget-object v1, v9, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    sget v2, Lwx;->o:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a(I)V

    :goto_8
    iget-object v1, v9, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MosaicView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-virtual {v1, v9}, Lcom/google/android/gms/games/ui/widget/MosaicView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :pswitch_1
    iget-object v1, v9, Leah;->o:Leaf;

    iget-object v1, v1, Leaf;->g:Landroid/content/Context;

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Leee;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-virtual {v9, v13, v12, v3, v14}, Leah;->a(Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_3

    :pswitch_2
    packed-switch v2, :pswitch_data_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid match status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for a completed match"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_3
    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->q()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v9, Leah;->n:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_9
    iget-object v1, v9, Leah;->o:Leaf;

    iget-object v1, v1, Leaf;->g:Landroid/content/Context;

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v2

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    const/16 v5, 0xb

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/16 v5, 0xc

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/16 v5, 0xd

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/16 v5, 0xe

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    const/4 v5, 0x5

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-ltz v4, :cond_7

    const-wide/32 v4, 0x5265c00

    const-wide/32 v6, 0x5265c00

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->getRelativeDateTimeString(Landroid/content/Context;JJJI)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_a
    sget v2, Lwx;->o:I

    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-virtual {v9, v13, v12, v3, v14}, Leah;->a(Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V

    move v15, v2

    move-object v2, v1

    move v1, v15

    goto/16 :goto_3

    :cond_6
    iget-object v1, v9, Leah;->n:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    :pswitch_4
    iget-object v1, v9, Leah;->n:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    :pswitch_5
    iget-object v1, v9, Leah;->n:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    :cond_7
    invoke-static {v1, v2, v3}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_8
    iget-object v3, v9, Leah;->e:Landroid/database/CharArrayBuffer;

    invoke-interface {v11, v3}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v3, v9, Leah;->d:Landroid/widget/TextView;

    iget-object v4, v9, Leah;->e:Landroid/database/CharArrayBuffer;

    iget-object v4, v4, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v5, 0x0

    iget-object v6, v9, Leah;->e:Landroid/database/CharArrayBuffer;

    iget v6, v6, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/TextView;->setText([CII)V

    iget-object v3, v9, Leah;->j:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v3, v9, Leah;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v9, Leah;->j:Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v9, Leah;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v9, Leah;->f:Landroid/widget/TextView;

    iget-object v3, v9, Leah;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_9
    iget-object v2, v9, Leah;->g:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_a
    iget-object v2, v9, Leah;->o:Leaf;

    iget-object v2, v2, Leaf;->h:Landroid/content/res/Resources;

    sget v3, Lxf;->aI:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    :cond_b
    iget-object v2, v9, Leah;->i:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v9, Leah;->h:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_c
    const/16 v2, 0xb

    invoke-static {v2}, Lbpz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, v9, Leah;->a:Landroid/view/ViewGroup;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    :cond_d
    iget-object v2, v9, Leah;->k:Lcom/google/android/gms/games/ui/widget/MosaicView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/widget/MosaicView;->a(I)V

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lbgo;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-super {p0, v0}, Ldwx;->a(Lbgo;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbgx;

    const-string v1, "external_match_id"

    invoke-direct {v0, p1, v1}, Lbgx;-><init>(Lbgo;Ljava/lang/String;)V

    invoke-super {p0, v0}, Ldwx;->a(Lbgo;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Leaf;->n:Ljava/lang/String;

    iput-object p2, p0, Leaf;->o:Ljava/lang/String;

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Leaf;->i:Landroid/view/LayoutInflater;

    sget v1, Lxc;->z:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Leah;

    invoke-direct {v1, p0, v0}, Leah;-><init>(Leaf;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
