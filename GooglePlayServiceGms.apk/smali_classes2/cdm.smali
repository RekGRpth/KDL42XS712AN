.class public final enum Lcdm;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcdm;

.field public static final enum b:Lcdm;

.field private static final synthetic d:[Lcdm;


# instance fields
.field private final c:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v0, Lcdm;

    const-string v1, "PARENT_ENTRY_SQL_ID"

    invoke-static {}, Lcdl;->d()Lcdl;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "parentEntryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    invoke-virtual {v3, v4, v8}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcdm;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdm;->a:Lcdm;

    new-instance v0, Lcdm;

    const-string v1, "CHILD_ENTRY_SQL_ID"

    invoke-static {}, Lcdl;->d()Lcdl;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "childEntryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    invoke-virtual {v3, v4, v8}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcdm;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdm;->b:Lcdm;

    const/4 v0, 0x2

    new-array v0, v0, [Lcdm;

    sget-object v1, Lcdm;->a:Lcdm;

    aput-object v1, v0, v7

    sget-object v1, Lcdm;->b:Lcdm;

    aput-object v1, v0, v6

    sput-object v0, Lcdm;->d:[Lcdm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcdm;->c:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdm;
    .locals 1

    const-class v0, Lcdm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdm;

    return-object v0
.end method

.method public static values()[Lcdm;
    .locals 1

    sget-object v0, Lcdm;->d:[Lcdm;

    invoke-virtual {v0}, [Lcdm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdm;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcdm;->c:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcdm;->c:Lcdp;

    return-object v0
.end method
