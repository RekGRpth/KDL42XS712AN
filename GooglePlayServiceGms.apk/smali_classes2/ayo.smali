.class final Layo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbai;


# instance fields
.field final synthetic a:Laze;

.field final synthetic b:Layl;


# direct methods
.method constructor <init>(Layl;Laze;)V
    .locals 0

    iput-object p1, p0, Layo;->b:Layl;

    iput-object p2, p0, Layo;->a:Laze;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onDisconnected: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Lays;

    invoke-direct {v1, p0, p1}, Lays;-><init>(Layo;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 5

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "onApplicationDisconnected: statusCode=%d, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layv;

    invoke-direct {v1, p0, p1, p2}, Layv;-><init>(Layo;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onApplicationConnected: appId=%s, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v6, v0, Lnz;->c:Lob;

    new-instance v0, Layt;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Layt;-><init>(Layo;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;DZ)V
    .locals 2

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layw;

    invoke-direct {v1, p0, p1, p2, p3}, Layw;-><init>(Layo;Ljava/lang/String;D)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;JI)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 0

    return-void
.end method

.method public final a_(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layu;

    invoke-direct {v1, p0, p1}, Layu;-><init>(Layo;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 5

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onConnectionFailed: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layr;

    invoke-direct {v1, p0}, Layr;-><init>(Layo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final j_()V
    .locals 3

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onConnected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layp;

    invoke-direct {v1, p0}, Layp;-><init>(Layo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k_()V
    .locals 3

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onConnectedWithoutApp"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layo;->b:Layl;

    iget-object v0, v0, Lnz;->c:Lob;

    new-instance v1, Layq;

    invoke-direct {v1, p0}, Layq;-><init>(Layo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
