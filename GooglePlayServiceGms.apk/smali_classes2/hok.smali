.class public final Lhok;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lhom;


# direct methods
.method public constructor <init>(Lhom;)V
    .locals 3

    const/16 v2, 0x3e8

    const/high16 v0, 0x3f400000    # 0.75f

    const/4 v1, 0x1

    invoke-direct {p0, v2, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput v2, p0, Lhok;->a:I

    iput-object p1, p0, Lhok;->b:Lhom;

    return-void
.end method

.method public static synthetic a(Lhok;)I
    .locals 1

    iget v0, p0, Lhok;->a:I

    return v0
.end method


# virtual methods
.method protected final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 3

    invoke-virtual {p0}, Lhok;->size()I

    move-result v0

    iget v1, p0, Lhok;->a:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lhok;->b:Lhom;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lhom;->a(I)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
