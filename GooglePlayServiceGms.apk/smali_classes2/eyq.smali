.class public abstract Leyq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/nio/FloatBuffer;

.field protected b:Ljava/nio/FloatBuffer;

.field protected c:Ljava/nio/ShortBuffer;

.field protected d:Ljava/util/Vector;

.field protected e:[F

.field protected f:[F

.field protected g:[F

.field protected h:Leyw;

.field protected i:Ljava/util/HashSet;

.field protected final j:Leyq;

.field private final k:[F

.field private final l:[F

.field private final m:Leyk;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x4

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Leyq;->a:Ljava/nio/FloatBuffer;

    iput-object v2, p0, Leyq;->b:Ljava/nio/FloatBuffer;

    iput-object v2, p0, Leyq;->c:Ljava/nio/ShortBuffer;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Leyq;->d:Ljava/util/Vector;

    new-array v0, v1, [F

    iput-object v0, p0, Leyq;->e:[F

    new-array v0, v1, [F

    iput-object v0, p0, Leyq;->f:[F

    new-array v0, v1, [F

    iput-object v0, p0, Leyq;->g:[F

    new-array v0, v3, [F

    iput-object v0, p0, Leyq;->k:[F

    new-array v0, v3, [F

    iput-object v0, p0, Leyq;->l:[F

    new-instance v0, Leyk;

    invoke-direct {v0}, Leyk;-><init>()V

    iput-object v0, p0, Leyq;->m:Leyk;

    iput-object v2, p0, Leyq;->h:Leyw;

    iput-object v2, p0, Leyq;->i:Ljava/util/HashSet;

    iget-object v0, p0, Leyq;->e:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iput-object v2, p0, Leyq;->j:Leyq;

    return-void
.end method


# virtual methods
.method protected final a(FFF)V
    .locals 11

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Leyq;->e:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iget-object v0, p0, Leyq;->e:[F

    neg-float v2, p1

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Leyq;->e:[F

    move v6, v1

    move v7, p2

    move v8, v4

    move v9, v3

    move v10, v3

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Leyq;->e:[F

    neg-float v7, p3

    move v6, v1

    move v8, v3

    move v9, v3

    move v10, v4

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Leyq;->f:[F

    iget-object v2, p0, Leyq;->e:[F

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    return-void
.end method

.method protected final a(IFFF)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Leyq;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v0, p0, Leyq;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v2, p3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v0, p0, Leyq;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1, p4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method

.method protected final a(II)V
    .locals 2

    mul-int/lit8 v0, p1, 0x3

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Leyq;->a:Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Leyq;->c:Ljava/nio/ShortBuffer;

    mul-int/lit8 v0, p1, 0x2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Leyq;->b:Ljava/nio/FloatBuffer;

    return-void
.end method

.method protected final a(IS)V
    .locals 1

    iget-object v0, p0, Leyq;->c:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    return-void
.end method

.method protected a(Leyk;F)V
    .locals 0

    return-void
.end method

.method public final a(Leyw;)V
    .locals 0

    iput-object p1, p0, Leyq;->h:Leyw;

    return-void
.end method

.method public abstract a([F)V
.end method

.method public final a([FLeyk;F)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v6, p0, Leyq;->m:Leyk;

    iget-object v0, p0, Leyq;->k:[F

    iget v2, p2, Leyk;->a:F

    aput v2, v0, v1

    iget-object v0, p0, Leyq;->k:[F

    iget v2, p2, Leyk;->b:F

    aput v2, v0, v7

    iget-object v0, p0, Leyq;->k:[F

    iget v2, p2, Leyk;->c:F

    aput v2, v0, v8

    iget-object v0, p0, Leyq;->k:[F

    const/4 v2, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v0, v2

    iget-object v0, p0, Leyq;->l:[F

    iget-object v2, p0, Leyq;->f:[F

    iget-object v4, p0, Leyq;->k:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    iget-object v0, p0, Leyq;->l:[F

    aget v0, v0, v1

    iput v0, v6, Leyk;->a:F

    iget-object v0, p0, Leyq;->l:[F

    aget v0, v0, v7

    iput v0, v6, Leyk;->b:F

    iget-object v0, p0, Leyq;->l:[F

    aget v0, v0, v8

    iput v0, v6, Leyk;->c:F

    iget-object v0, p0, Leyq;->m:Leyk;

    invoke-virtual {p0, v0, p3}, Leyq;->a(Leyk;F)V

    invoke-virtual {p0, p1}, Leyq;->b([F)V

    return-void
.end method

.method public final b([F)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Leyq;->g:[F

    iget-object v4, p0, Leyq;->e:[F

    move-object v2, p1

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v0, p0, Leyq;->i:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyq;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyq;

    iget-object v2, p0, Leyq;->g:[F

    invoke-virtual {v0, v2}, Leyq;->b([F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Leyq;->g:[F

    invoke-virtual {p0, v0}, Leyq;->a([F)V

    return-void
.end method
