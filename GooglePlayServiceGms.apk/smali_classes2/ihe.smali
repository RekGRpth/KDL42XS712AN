.class public final Lihe;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:Lihd;

.field private c:J


# direct methods
.method private constructor <init>(Lihd;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lihe;->b:Lihd;

    iput-wide p2, p0, Lihe;->a:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lihe;->c:J

    return-void
.end method

.method synthetic constructor <init>(Lihd;JB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lihe;-><init>(Lihd;J)V

    return-void
.end method


# virtual methods
.method final declared-synchronized a(J)J
    .locals 6

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_0
    iget-wide v0, p0, Lihe;->a:J

    iget-wide v2, p0, Lihe;->c:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lihe;->c:J

    add-long/2addr v2, v0

    iget-wide v4, p0, Lihe;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lihe;->c:J

    iget-object v2, p0, Lihe;->b:Lihd;

    invoke-virtual {v2, v0, v1}, Lihd;->c(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
