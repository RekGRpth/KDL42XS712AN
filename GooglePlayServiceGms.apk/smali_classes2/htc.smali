.class public final Lhtc;
.super Lhtf;
.source "SourceFile"


# instance fields
.field private final n:I


# direct methods
.method public constructor <init>(JIIIIIILjava/util/Collection;IIII)V
    .locals 14

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p8

    invoke-direct/range {v1 .. v13}, Lhtf;-><init>(JIIIILjava/util/Collection;IIIII)V

    move/from16 v0, p7

    iput v0, p0, Lhtc;->n:I

    return-void
.end method


# virtual methods
.method public final a(JI)Lhtf;
    .locals 14

    new-instance v0, Lhtc;

    iget v3, p0, Lhtc;->k:I

    iget v4, p0, Lhtc;->b:I

    iget v5, p0, Lhtc;->c:I

    iget v6, p0, Lhtc;->d:I

    iget v7, p0, Lhtc;->n:I

    iget-object v9, p0, Lhtc;->j:Ljava/util/Collection;

    iget v10, p0, Lhtc;->e:I

    iget v11, p0, Lhtc;->f:I

    iget v12, p0, Lhtc;->g:I

    iget v13, p0, Lhtc;->h:I

    move-wide v1, p1

    move/from16 v8, p3

    invoke-direct/range {v0 .. v13}, Lhtc;-><init>(JIIIIIILjava/util/Collection;IIII)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lhtc;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhtc;->j()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtc;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtc;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtc;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtc;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtc;->m:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhtc;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Livi;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lhtc;->n:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    return-void
.end method

.method public final a(Lhtf;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lhtc;

    if-eqz v1, :cond_0

    check-cast p1, Lhtc;

    iget v1, p0, Lhtc;->n:I

    iget v2, p1, Lhtc;->n:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method final b()Z
    .locals 2

    iget v0, p0, Lhtc;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " lac: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhtc;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lhtc;

    if-eqz v1, :cond_0

    check-cast p1, Lhtc;

    invoke-super {p0, p1}, Lhtf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lhtc;->n:I

    iget v2, p1, Lhtc;->n:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    invoke-super {p0}, Lhtf;->hashCode()I

    move-result v0

    iget v1, p0, Lhtc;->n:I

    mul-int/lit16 v1, v1, 0xd79

    xor-int/2addr v0, v1

    return v0
.end method
