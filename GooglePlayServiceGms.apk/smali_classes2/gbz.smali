.class public final Lgbz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lftb;

.field private final c:Lcom/google/android/gms/plus/model/posts/Comment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lftb;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgbz;->b:Lftb;

    iput-object p3, p0, Lgbz;->c:Lcom/google/android/gms/plus/model/posts/Comment;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lgbz;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgbz;->c:Lcom/google/android/gms/plus/model/posts/Comment;

    iget-object v2, p2, Lfrx;->c:Lfsj;

    invoke-virtual {v2, v0, v1}, Lfsj;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Comment;)Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v0

    iget-object v1, p0, Lgbz;->b:Lftb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbz;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgbz;->b:Lftb;

    const/4 v2, 0x4

    invoke-interface {v1, v2, v0, v4}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "InsertCommentOperation"

    invoke-virtual {v0}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lgbz;->b:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgbz;->b:Lftb;

    if-eqz v0, :cond_0

    const-string v0, "InsertCommentOperation"

    const-string v1, "Unable to insert comment"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lgbz;->b:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    :cond_0
    return-void
.end method
