.class public final Lcak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field b:J

.field final synthetic c:Lcai;


# direct methods
.method public constructor <init>(Lcai;)V
    .locals 0

    iput-object p1, p0, Lcak;->c:Lcai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 8

    const-wide/16 v6, 0xd0

    iget-object v0, p0, Lcak;->c:Lcai;

    invoke-virtual {v0}, Lcai;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/16 v0, 0xd0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcak;->a:J

    iget-wide v4, p0, Lcak;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcak;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, Lcak;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final run()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcak;->c:Lcai;

    invoke-virtual {v0}, Lcai;->a()I

    move-result v0

    if-eq v0, v2, :cond_0

    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcak;->b:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcak;->a:J

    iget-object v0, p0, Lcak;->c:Lcai;

    invoke-virtual {v0, v2}, Lcai;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcak;->a()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcak;->c:Lcai;

    invoke-static {v0}, Lcai;->a(Lcai;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcak;->c:Lcai;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcai;->a(I)V

    goto :goto_0
.end method
