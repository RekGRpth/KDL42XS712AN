.class public final Lfsc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "default_avatar"

    invoke-static {v0}, Lbkn;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfsc;->a:Landroid/net/Uri;

    return-void
.end method

.method private static a(Lgdp;)Landroid/net/Uri;
    .locals 1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lgdp;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lfsc;->a:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Lgdp;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lftt;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lgdl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfsu;
    .locals 10

    invoke-static {p4}, Lfsc;->a(Lgdl;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-interface {p4}, Lgdl;->g()Lgdn;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    :goto_0
    if-nez v6, :cond_3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    const/4 v0, 0x4

    new-array v5, v0, [Landroid/net/Uri;

    if-eqz v2, :cond_5

    const/4 v4, 0x0

    if-nez p7, :cond_4

    sget-object v0, Lfsc;->a:Landroid/net/Uri;

    :goto_2
    aput-object v0, v5, v4

    move v4, v3

    :goto_3
    const/4 v7, 0x1

    if-le v1, v4, :cond_7

    add-int/lit8 v3, v4, 0x1

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdp;

    invoke-static {v0}, Lfsc;->a(Lgdp;)Landroid/net/Uri;

    move-result-object v0

    :goto_4
    aput-object v0, v5, v7

    const/4 v7, 0x2

    if-le v1, v3, :cond_8

    add-int/lit8 v4, v3, 0x1

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdp;

    invoke-static {v0}, Lfsc;->a(Lgdp;)Landroid/net/Uri;

    move-result-object v0

    move v3, v4

    :goto_5
    aput-object v0, v5, v7

    const/4 v4, 0x3

    if-le v1, v3, :cond_9

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdp;

    invoke-static {v0}, Lfsc;->a(Lgdp;)Landroid/net/Uri;

    move-result-object v0

    :goto_6
    aput-object v0, v5, v4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v6, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-interface {p4}, Lgdl;->g()Lgdn;

    move-result-object v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p4}, Lgdl;->g()Lgdn;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v0}, Lgdn;->b()Lgdo;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v0}, Lgdo;->c()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_c

    const/4 v0, 0x0

    move v3, v0

    :goto_8
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_c

    const/4 v0, 0x4

    if-ge v3, v0, :cond_c

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdp;

    invoke-interface {v0}, Lgdp;->b()Ljava/lang/String;

    move-result-object v0

    const-string v9, " "

    invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v9, 0x0

    aget-object v0, v0, v9

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_0
    invoke-interface {v0}, Lgdn;->b()Lgdo;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_0

    :cond_1
    invoke-interface {v0}, Lgdo;->c()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_0

    :cond_2
    move-object v6, v0

    goto/16 :goto_0

    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto/16 :goto_1

    :cond_4
    invoke-static/range {p7 .. p7}, Lftt;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    :cond_5
    const/4 v4, 0x0

    if-lez v1, :cond_6

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdp;

    invoke-static {v0}, Lfsc;->a(Lgdp;)Landroid/net/Uri;

    move-result-object v0

    :goto_9
    aput-object v0, v5, v4

    move v4, v3

    goto/16 :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_9

    :cond_7
    const/4 v0, 0x0

    move v3, v4

    goto/16 :goto_4

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_a
    invoke-interface {v0}, Lgdn;->b()Lgdo;

    move-result-object v0

    if-nez v0, :cond_b

    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_7

    :cond_b
    invoke-interface {v0}, Lgdo;->b()D

    move-result-wide v0

    double-to-int v0, v0

    move v1, v0

    goto/16 :goto_7

    :cond_c
    invoke-static {v4, v6, v2, v1, v7}, Lfse;->a(Landroid/content/res/Resources;Ljava/util/Locale;ZILjava/util/ArrayList;)Lfsf;

    move-result-object v1

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, v1, Lfsf;->a:Ljava/lang/String;

    aput-object v3, v4, v0

    const/4 v0, 0x1

    iget-object v3, v1, Lfsf;->b:Ljava/lang/String;

    aput-object v3, v4, v0

    const/4 v0, 0x2

    iget-object v3, v1, Lfsf;->c:Ljava/lang/String;

    aput-object v3, v4, v0

    const/4 v0, 0x3

    iget-object v3, v1, Lfsf;->d:Ljava/lang/String;

    aput-object v3, v4, v0

    invoke-interface {p4}, Lgdl;->b()Ljava/lang/String;

    move-result-object v7

    if-eqz v2, :cond_d

    const-string v0, "com.google.android.gms.plus.action.UNDO_PLUS_ONE"

    :goto_a
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;

    invoke-virtual {v3, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.TOKEN"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.URL"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    new-instance v0, Lfsu;

    iget-object v3, v1, Lfsf;->d:Ljava/lang/String;

    move-object v1, p3

    move-object v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lfsu;-><init>(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_d
    const-string v0, "com.google.android.gms.plus.action.PLUS_ONE"

    goto :goto_a
.end method

.method static a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;I)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;->g()Lgdn;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Lgdn;->b()Lgdo;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    if-eqz v0, :cond_0

    const-string v1, "count"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->d(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b()D

    move-result-wide v2

    int-to-double v4, p1

    add-double/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;D)V

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;Z)V
    .locals 2

    const-string v0, "isSetByViewer"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Z)V

    return-void
.end method

.method public static a(Lgdl;)Z
    .locals 1

    invoke-interface {p0}, Lgdl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lgdl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lgdy;)Z
    .locals 2

    iget-object v0, p0, Lbni;->a:Landroid/content/ContentValues;

    const-string v1, "isSetByViewer"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
