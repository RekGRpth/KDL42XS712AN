.class public final Lhkg;
.super Lhin;
.source "SourceFile"


# static fields
.field static final g:Ljava/util/Set;

.field static final i:Ljava/util/List;


# instance fields
.field private final A:Ljava/util/Random;

.field private B:Z

.field private C:Ljava/lang/Integer;

.field private D:Lilt;

.field private E:Z

.field private F:Z

.field final h:Ljava/util/Set;

.field j:J

.field k:Lhit;

.field l:Z

.field m:J

.field n:Lhqy;

.field o:Z

.field p:Livi;

.field q:Z

.field r:Lhsi;

.field s:J

.field t:J

.field private final u:Lhjf;

.field private final v:Lhkd;

.field private final w:Lhjd;

.field private final x:Liha;

.field private final y:Lhkr;

.field private final z:Lhkb;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x10

    const/16 v5, 0xb

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-array v0, v3, [Lhrz;

    sget-object v1, Lhrz;->d:Lhrz;

    aput-object v1, v0, v4

    invoke-static {v0}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lhkg;->g:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lhkg;->i:Ljava/util/List;

    new-instance v1, Lilt;

    const/4 v2, 0x6

    invoke-direct {v1, v2, v4, v5}, Lilt;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lhkg;->i:Ljava/util/List;

    new-instance v1, Lilt;

    invoke-direct {v1, v5, v3, v6}, Lilt;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lhkg;->i:Ljava/util/List;

    new-instance v1, Lilt;

    const/16 v2, 0x17

    invoke-direct {v1, v6, v3, v2}, Lilt;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhjd;Lhkd;Liha;Lhkr;Ljava/util/Random;Lhkb;)V
    .locals 8

    const-string v2, "SCollector"

    sget-object v7, Lhir;->b:Lhir;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    const/4 v1, 0x4

    new-array v1, v1, [Lhrz;

    const/4 v2, 0x0

    sget-object v3, Lhrz;->g:Lhrz;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lhrz;->a:Lhrz;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lhrz;->e:Lhrz;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lhrz;->d:Lhrz;

    aput-object v3, v1, v2

    invoke-static {v1}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lhkg;->h:Ljava/util/Set;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhkg;->j:J

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->k:Lhit;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->B:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->l:Z

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhkg;->m:J

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->n:Lhqy;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->o:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->p:Livi;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->q:Z

    new-instance v1, Lhkh;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lhkh;-><init>(Lhkg;B)V

    iput-object v1, p0, Lhkg;->r:Lhsi;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhkg;->s:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhkg;->t:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->E:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->F:Z

    iput-object p5, p0, Lhkg;->u:Lhjf;

    iput-object p7, p0, Lhkg;->v:Lhkd;

    move-object/from16 v0, p8

    iput-object v0, p0, Lhkg;->x:Liha;

    iput-object p6, p0, Lhkg;->w:Lhjd;

    move-object/from16 v0, p9

    iput-object v0, p0, Lhkg;->y:Lhkr;

    move-object/from16 v0, p11

    iput-object v0, p0, Lhkg;->z:Lhkb;

    move-object/from16 v0, p10

    iput-object v0, p0, Lhkg;->A:Ljava/util/Random;

    sget-object v1, Lhrz;->j:Lhrz;

    invoke-interface {p1, v1}, Lidu;->a(Lhrz;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhkg;->h:Ljava/util/Set;

    sget-object v2, Lhrz;->j:Lhrz;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    sget-object v1, Lhrz;->i:Lhrz;

    invoke-interface {p1, v1}, Lidu;->a(Lhrz;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhkg;->h:Ljava/util/Set;

    sget-object v2, Lhrz;->i:Lhrz;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    sget-object v1, Lhrz;->f:Lhrz;

    invoke-interface {p1, v1}, Lidu;->a(Lhrz;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhkg;->h:Ljava/util/Set;

    sget-object v2, Lhrz;->f:Lhrz;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public constructor <init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhjd;Liha;Lhkr;Ljava/util/Random;)V
    .locals 13

    invoke-static {}, Lhkd;->a()Lhkd;

    move-result-object v8

    new-instance v12, Lhkb;

    sget-object v1, Lhkg;->i:Ljava/util/List;

    move-object/from16 v0, p5

    invoke-direct {v12, v1, p1, v0}, Lhkb;-><init>(Ljava/util/List;Lidu;Lhjf;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v12}, Lhkg;-><init>(Lidu;Lhof;Lhkl;Lhiq;Lhjf;Lhjd;Lhkd;Liha;Lhkr;Ljava/util/Random;Lhkb;)V

    return-void
.end method

.method private a(JJJIZ)V
    .locals 14

    iget-object v1, p0, Lhkg;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->c()J

    move-result-wide v1

    add-long v3, p1, v1

    add-long v5, p5, v1

    iget-object v2, p0, Lhkg;->u:Lhjf;

    iget-object v1, p0, Lhkg;->k:Lhit;

    iget-object v7, v2, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    new-instance v12, Livi;

    sget-object v13, Lihj;->aX:Livk;

    invoke-direct {v12, v13}, Livi;-><init>(Livk;)V

    const/4 v13, 0x1

    invoke-virtual {v12, v13, v8, v9}, Livi;->a(IJ)Livi;

    const/4 v8, 0x2

    invoke-virtual {v12, v8, v10, v11}, Livi;->a(IJ)Livi;

    const/4 v8, 0x4

    move/from16 v0, p7

    invoke-virtual {v12, v8, v0}, Livi;->e(II)Livi;

    if-eqz v1, :cond_0

    const/4 v8, 0x5

    sget-object v9, Lhit;->b:Lhit;

    if-ne v1, v9, :cond_5

    const/4 v1, 0x2

    :goto_0
    invoke-virtual {v12, v8, v1}, Livi;->e(II)Livi;

    :cond_0
    iget-object v1, v2, Lhjf;->c:Livi;

    const/4 v8, 0x1

    invoke-virtual {v1, v8, v12}, Livi;->a(ILivi;)V

    invoke-virtual {v2}, Lhjf;->h()V

    iget-object v8, v2, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, v2, Lhjf;->c:Livi;

    const/4 v9, 0x2

    invoke-static {v1, v9}, Lilv;->a(Livi;I)V

    iget-object v1, v2, Lhjf;->c:Livi;

    const/16 v2, 0xb

    invoke-static {v1, v2}, Lilv;->a(Livi;I)V

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v1, p0, Lhkg;->u:Lhjf;

    invoke-virtual {v1}, Lhjf;->a()V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhkg;->a:Ljava/lang/String;

    const-string v2, "Collector exits with [%d], time [%s, %s]"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v3, v4}, Ljava/util/Date;-><init>(J)V

    aput-object v9, v7, v8

    const/4 v3, 0x2

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    aput-object v4, v7, v3

    invoke-static {v2, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-wide/16 v1, -0x1

    cmp-long v1, p3, v1

    if-eqz v1, :cond_3

    sub-long v1, p5, p3

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lhkg;->x:Liha;

    iget-object v4, v3, Liha;->f:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v4

    iget-object v6, v3, Liha;->a:Lihb;

    iget-object v6, v6, Lihb;->d:Lihd;

    invoke-virtual {v6, v1, v2, v4, v5}, Lihd;->a(JJ)Z

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "CollectionPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "gps tokens left for sensor collection: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Liha;->a:Lihb;

    iget-object v6, v6, Lihb;->d:Lihd;

    iget-wide v6, v6, Lihd;->e:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, v3, Liha;->a:Lihb;

    invoke-virtual {v1, v4, v5}, Lihb;->a(J)V

    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->n:Lhqy;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->q:Z

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lhkg;->m:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lhkg;->o:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->p:Livi;

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->C:Ljava/lang/Integer;

    const/4 v1, 0x0

    iput-object v1, p0, Lhkg;->D:Lilt;

    if-eqz p8, :cond_4

    iget-object v1, p0, Lhkg;->b:Lidu;

    const/4 v2, 0x6

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lidu;->a(IJLilx;)V

    :cond_4
    return-void

    :cond_5
    const/4 v1, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v8

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v7

    throw v1
.end method

.method static synthetic a(Lhkg;JJJI)V
    .locals 9

    const/4 v8, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lhkg;->a(JJJIZ)V

    return-void
.end method

.method private a(Ljava/util/Calendar;J)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p2, p3}, Lhkg;->g(J)Z

    move-result v0

    invoke-direct {p0, v0}, Lhkg;->d(Z)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    iget-object v1, p0, Lhkg;->z:Lhkb;

    iget-wide v2, p0, Lhkg;->t:J

    invoke-virtual {v1, p1, v2, v3}, Lhkb;->a(Ljava/util/Calendar;J)Lhkc;

    move-result-object v1

    iget-boolean v1, v1, Lhkc;->a:Z

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lhkg;->f:Lhir;

    sget-object v2, Lhir;->b:Lhir;

    if-ne v0, v2, :cond_1

    invoke-direct {p0, v5}, Lhkg;->e(Z)V

    iget-object v0, p0, Lhkg;->z:Lhkb;

    iget-wide v1, p0, Lhkg;->t:J

    invoke-virtual {v0, p1, v1, v2, v4}, Lhkb;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhkg;->h(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v4}, Lhkg;->e(Z)V

    invoke-direct {p0}, Lhkg;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    iget-object v0, p0, Lhkg;->z:Lhkb;

    iget-wide v1, p0, Lhkg;->t:J

    invoke-virtual {v0, p1, v1, v2, v4}, Lhkb;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhkg;->h(J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhkg;->z:Lhkb;

    iget-wide v1, p0, Lhkg;->t:J

    invoke-virtual {v0, p1, v1, v2, v5}, Lhkb;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lhkg;->h(J)V

    goto :goto_0
.end method

.method private d(Z)Lhue;
    .locals 12

    const/4 v5, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lhkg;->v:Lhkd;

    invoke-virtual {v0, p1}, Lhkd;->a(Z)Lhue;

    move-result-object v8

    iget-object v0, p0, Lhkg;->b:Lidu;

    invoke-interface {v0}, Lidu;->k()Z

    move-result v9

    iget-object v0, p0, Lhkg;->w:Lhjd;

    iget-object v0, v0, Lhjd;->l:Livi;

    if-eqz v0, :cond_1

    move v6, v5

    :goto_0
    iget-object v3, p0, Lhkg;->x:Liha;

    iget-object v0, v3, Liha;->a:Lihb;

    iget-object v0, v0, Lihb;->d:Lihd;

    const-wide/32 v1, 0x493e0

    iget-object v3, v3, Liha;->f:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v3

    invoke-virtual/range {v0 .. v5}, Lihd;->a(JJZ)Z

    move-result v3

    iget-object v0, v8, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lhkg;->B:Z

    if-eqz v0, :cond_2

    if-eqz v9, :cond_2

    if-eqz v6, :cond_2

    if-eqz v3, :cond_2

    move v1, v5

    :goto_1
    if-eqz v1, :cond_d

    iget-object v0, p0, Lhkg;->b:Lidu;

    invoke-interface {v0}, Lidu;->s()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_3

    array-length v10, v4

    move v2, v7

    move v0, v7

    :goto_2
    if-ge v2, v10, :cond_4

    aget-object v11, v4, v2

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v6, v7

    goto :goto_0

    :cond_2
    move v1, v7

    goto :goto_1

    :cond_3
    move v0, v7

    :cond_4
    const/4 v2, 0x3

    if-lt v0, v2, :cond_5

    move v0, v5

    :goto_3
    if-eqz v1, :cond_6

    if-nez v0, :cond_6

    :goto_4
    move v7, v0

    :goto_5
    if-nez v5, :cond_c

    iget-object v0, v8, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v8, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    :goto_6
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v7

    goto :goto_3

    :cond_6
    move v5, v7

    goto :goto_4

    :cond_7
    if-nez v9, :cond_8

    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    :cond_8
    if-nez v6, :cond_9

    const/16 v0, 0x1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    :cond_9
    if-nez v3, :cond_a

    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    :cond_a
    if-eqz v7, :cond_b

    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    :cond_b
    const/16 v0, 0x63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    goto :goto_6

    :cond_d
    move v5, v1

    goto :goto_5
.end method

.method private e(Z)V
    .locals 3

    const/16 v2, 0xb4

    iget-boolean v0, p0, Lhkg;->l:Z

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lhkg;->y:Lhkr;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lhkr;->a(IZ)V

    :goto_0
    iput-boolean p1, p0, Lhkg;->l:Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lhkg;->y:Lhkr;

    invoke-virtual {v0, v2}, Lhkr;->b(I)V

    goto :goto_0
.end method

.method private f(Z)V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhkg;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkg;->p:Livi;

    if-nez v0, :cond_0

    new-instance v0, Livi;

    sget-object v1, Lihj;->e:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iput-object v0, p0, Lhkg;->p:Livi;

    :cond_0
    new-instance v0, Livi;

    sget-object v1, Lihj;->d:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    invoke-virtual {v0, v6, p1}, Livi;->a(IZ)Livi;

    const/4 v1, 0x2

    iget-object v2, p0, Lhkg;->b:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lhkg;->m:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    iget-object v1, p0, Lhkg;->p:Livi;

    invoke-virtual {v1, v6, v0}, Livi;->a(ILivi;)V

    :cond_1
    return-void
.end method

.method private g()Z
    .locals 4

    iget-object v0, p0, Lhkg;->v:Lhkd;

    invoke-virtual {v0}, Lhkd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhkg;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cantSchedule: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhkg;->v:Lhkd;

    invoke-virtual {v3}, Lhkd;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method private g(J)Z
    .locals 4

    iget-wide v0, p0, Lhkg;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lhkg;->t:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Ljava/util/Calendar;
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lhkg;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    return-object v0
.end method

.method private h(J)V
    .locals 7

    iget-wide v0, p0, Lhkg;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lhkg;->j:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lhkg;->j:J

    iget-object v0, p0, Lhkg;->b:Lidu;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, p2, v2}, Lidu;->a(IJLilx;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Alarm scheduled at %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, Lhkg;->b:Lidu;

    invoke-interface {v5}, Lidu;->j()Licm;

    move-result-object v5

    invoke-interface {v5}, Licm;->c()J

    move-result-wide v5

    add-long/2addr v5, p1

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 3

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhkg;->j:J

    invoke-direct {p0}, Lhkg;->h()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lhkg;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lhkg;->a(Ljava/util/Calendar;J)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Detected activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhkg;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhkg;->s:J

    :cond_1
    return-void
.end method

.method final a(Lhtf;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhkg;->F:Z

    return-void
.end method

.method final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lhkg;->B:Z

    return-void
.end method

.method final b(Lhuv;)V
    .locals 9

    const/4 v8, 0x7

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lhin;->b(Lhuv;)V

    iget-object v0, p0, Lhkg;->f:Lhir;

    sget-object v1, Lhir;->b:Lhir;

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhkg;->e:Lhiq;

    iget-object v5, v0, Lhiq;->d:Lhoj;

    move v1, v2

    move v3, v2

    :goto_1
    iget-object v0, p1, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-virtual {p1, v1}, Lhuv;->a(I)Lhut;

    move-result-object v0

    iget-wide v6, v0, Lhut;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v6, v5, Lhoj;->b:Lhok;

    invoke-virtual {v6, v0}, Lhok;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhol;

    if-eqz v0, :cond_3

    iget-object v6, v0, Lhol;->a:Livi;

    invoke-virtual {v6, v8}, Livi;->i(I)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v0, v0, Lhol;->a:Livi;

    invoke-virtual {v0, v8}, Livi;->e(I)F

    move-result v0

    :goto_2
    const/4 v6, 0x0

    cmpl-float v6, v0, v6

    if-lez v6, :cond_4

    iget-object v6, p0, Lhkg;->A:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    cmpg-float v0, v6, v0

    if-gez v0, :cond_4

    move v0, v4

    :goto_3
    if-eqz v0, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    int-to-long v0, v3

    const-wide/16 v5, 0x3

    cmp-long v0, v0, v5

    if-ltz v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Got %d votes for sensor collection."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-wide v0, p1, Lhuv;->a:J

    iput-wide v0, p0, Lhkg;->t:J

    goto :goto_0
.end method

.method final b(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lhkg;->f(Z)V

    iput-boolean p1, p0, Lhkg;->E:Z

    return-void
.end method

.method protected final b(J)Z
    .locals 12

    iget-boolean v0, p0, Lhkg;->F:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lhkg;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhkg;->F:Z

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lhkg;->h()Ljava/util/Calendar;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lhkg;->g(J)Z

    move-result v10

    iget-wide v1, p0, Lhkg;->j:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    if-eqz v10, :cond_3

    iget-boolean v1, p0, Lhkg;->l:Z

    if-nez v1, :cond_3

    :cond_2
    invoke-direct {p0, v0, p1, p2}, Lhkg;->a(Ljava/util/Calendar;J)V

    :cond_3
    iget-object v1, p0, Lhkg;->z:Lhkb;

    iget-wide v2, p0, Lhkg;->t:J

    invoke-virtual {v1, v0, v2, v3}, Lhkb;->a(Ljava/util/Calendar;J)Lhkc;

    move-result-object v11

    iget-object v0, v11, Lhkc;->c:Lilt;

    if-nez v0, :cond_d

    const/4 v0, 0x0

    move-object v9, v0

    :goto_1
    iget-object v0, p0, Lhkg;->D:Lilt;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhkg;->D:Lilt;

    invoke-virtual {v0, v9}, Lilt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lhkg;->C:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to do collection in the last timespan. Reporting the failure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkg;->C:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-wide/16 v3, -0x1

    iget-object v0, p0, Lhkg;->C:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lhkg;->a(JJJIZ)V

    :cond_5
    :goto_2
    iput-object v9, p0, Lhkg;->D:Lilt;

    iget-boolean v0, v11, Lhkc;->a:Z

    if-eqz v0, :cond_c

    iget-object v0, v11, Lhkc;->b:Lhit;

    iput-object v0, p0, Lhkg;->k:Lhit;

    invoke-direct {p0, v10}, Lhkg;->d(Z)Lhue;

    move-result-object v1

    iget-object v0, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-wide v0, p0, Lhkg;->s:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_11

    iget-wide v0, p0, Lhkg;->s:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_11

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_16

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "On foot detected. Starting collection."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-wide v0, p0, Lhkg;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhkg;->j:J

    iget-object v0, p0, Lhkg;->b:Lidu;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lidu;->a(I)V

    :cond_7
    sget-object v0, Lhir;->h:Lhir;

    iput-object v0, p0, Lhkg;->f:Lhir;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhkg;->e(Z)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lhrz;->e:Lhrz;

    iget-object v1, p0, Lhkg;->u:Lhjf;

    invoke-virtual {v1}, Lhjf;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhrz;->d:Lhrz;

    iget-object v1, p0, Lhkg;->u:Lhjf;

    invoke-virtual {v1}, Lhjf;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhkg;->b:Lidu;

    sget-object v1, Lhrz;->j:Lhrz;

    invoke-interface {v0, v1}, Lidu;->a(Lhrz;)Z

    move-result v0

    if-eqz v0, :cond_12

    sget-object v0, Lhrz;->j:Lhrz;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    :goto_4
    iget-object v0, p0, Lhkg;->b:Lidu;

    sget-object v1, Lhrz;->i:Lhrz;

    invoke-interface {v0, v1}, Lidu;->a(Lhrz;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lhrz;->i:Lhrz;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget-object v0, p0, Lhkg;->b:Lidu;

    iget-object v1, p0, Lhkg;->h:Ljava/util/Set;

    iget-object v3, p0, Lhkg;->b:Lidu;

    invoke-interface {v3}, Lidu;->s()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    if-eqz v10, :cond_13

    const/16 v4, 0x12

    :goto_5
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lhkg;->w:Lhjd;

    iget-object v6, v5, Lhjd;->l:Livi;

    iget-object v7, p0, Lhkg;->r:Lhsi;

    iget-object v8, p0, Lhkg;->a:Ljava/lang/String;

    move v5, v10

    invoke-interface/range {v0 .. v8}, Lidu;->a(Ljava/util/Set;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Integer;ZLivi;Lhqq;Ljava/lang/String;)Lhqy;

    move-result-object v0

    iput-object v0, p0, Lhkg;->n:Lhqy;

    iget-object v0, p0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_14

    iput-wide p1, p0, Lhkg;->m:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhkg;->q:Z

    iput-boolean v10, p0, Lhkg;->o:Z

    iget-object v0, p0, Lhkg;->n:Lhqy;

    invoke-interface {v0}, Lhqy;->a()V

    iget-boolean v0, p0, Lhkg;->E:Z

    invoke-direct {p0, v0}, Lhkg;->f(Z)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Signal collector started, isInsightCollection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lhkg;->o:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_6
    iget-object v0, p0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_7
    if-nez v0, :cond_c

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Unable to start collection"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhkg;->f:Lhir;

    const-wide/16 v3, -0x1

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lhkg;->a(JJJIZ)V

    :cond_c
    :goto_8
    iget-object v0, p0, Lhkg;->f:Lhir;

    sget-object v1, Lhir;->b:Lhir;

    if-eq v0, v1, :cond_18

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lhkg;->z:Lhkb;

    iget-object v1, v11, Lhkc;->c:Lilt;

    iget-object v0, v0, Lhkb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    iget-wide v3, v1, Lilt;->a:J

    invoke-virtual {v0, v3, v4}, Lilt;->b(J)Z

    move-result v3

    if-eqz v3, :cond_e

    move-object v9, v0

    goto/16 :goto_1

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Did not find parent of subtimespan: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Unable to collect in the last timespan, but the failure reason is null"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_12
    iget-object v0, p0, Lhkg;->b:Lidu;

    sget-object v1, Lhrz;->f:Lhrz;

    invoke-interface {v0, v1}, Lidu;->a(Lhrz;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lhrz;->f:Lhrz;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_13
    const/16 v4, 0xe

    goto/16 :goto_5

    :cond_14
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Failed to create signal collector."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_16
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhkg;->C:Ljava/lang/Integer;

    goto/16 :goto_8

    :cond_17
    iget-object v0, v1, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lhkg;->C:Ljava/lang/Integer;

    goto/16 :goto_8

    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected final c()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lhkg;->n:Lhqy;

    if-nez v0, :cond_0

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhkg;->f:Lhir;

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhkg;->v:Lhkd;

    iget-boolean v2, p0, Lhkg;->o:Z

    invoke-virtual {v0, v2}, Lhkd;->a(Z)Lhue;

    move-result-object v0

    iget-object v0, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    const-string v2, "Bad device conditions, terminated early."

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iput-boolean v1, p0, Lhkg;->q:Z

    iget-object v0, p0, Lhkg;->n:Lhqy;

    invoke-interface {v0}, Lhqy;->b()V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    invoke-super {p0}, Lhin;->f()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sensor policy changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkg;->v:Lhkd;

    invoke-virtual {v2}, Lhkd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lhkg;->h()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lhkg;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lhkg;->a(Ljava/util/Calendar;J)V

    return-void
.end method
