.class final Lhau;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgxh;


# instance fields
.field final synthetic a:Lhar;


# direct methods
.method constructor <init>(Lhar;)V
    .locals 0

    iput-object p1, p0, Lhau;->a:Lhar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lhau;->a:Lhar;

    invoke-static {v0}, Lhar;->e(Lhar;)V

    return-void
.end method

.method public final a(Lioj;)V
    .locals 2

    iget-object v0, p0, Lhau;->a:Lhar;

    iget-boolean v0, v0, Lhar;->al:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    :cond_1
    iget-object v0, p0, Lhau;->a:Lhar;

    invoke-static {v0, p1}, Lhar;->a(Lhar;Lioj;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v0, p0, Lhau;->a:Lhar;

    invoke-static {v0}, Lhar;->d(Lhar;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-boolean v0, v0, Lhar;->al:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhau;->a:Lhar;

    invoke-static {v0, p1}, Lhar;->b(Lhar;Lioj;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v0, v0, Lhar;->ak:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    iget-object v0, p0, Lhau;->a:Lhar;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhar;->a(Lhar;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x75
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lioj;)V
    .locals 2

    iget-object v0, p0, Lhau;->a:Lhar;

    invoke-static {v0, p1}, Lhar;->a(Lhar;Lioj;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v1, p0, Lhau;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lgya;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-static {v0, v1}, Lhar;->a(Lhar;Lgya;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v1, p0, Lhau;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lgya;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-static {v0, v1}, Lhar;->a(Lhar;Lgya;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhau;->a:Lhar;

    iget-object v1, p0, Lhau;->a:Lhar;

    invoke-static {v1}, Lhar;->f(Lhar;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lgya;->c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgya;

    move-result-object v1

    invoke-static {v0, v1}, Lhar;->a(Lhar;Lgya;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x79
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
