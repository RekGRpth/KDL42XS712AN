.class public final Lazw;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lizf;

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput v1, p0, Lazw;->f:I

    const-string v0, ""

    iput-object v0, p0, Lazw;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lazw;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lazw;->a:Ljava/lang/String;

    iput v1, p0, Lazw;->b:I

    const-string v0, ""

    iput-object v0, p0, Lazw;->c:Ljava/lang/String;

    sget-object v0, Lizf;->a:Lizf;

    iput-object v0, p0, Lazw;->d:Lizf;

    const/4 v0, -0x1

    iput v0, p0, Lazw;->o:I

    return-void
.end method

.method public static a([B)Lazw;
    .locals 2

    new-instance v0, Lazw;

    invoke-direct {v0}, Lazw;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Lazw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lazw;->o:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lazw;->b()I

    :cond_0
    iget v0, p0, Lazw;->o:I

    return v0
.end method

.method public final a(I)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->e:Z

    iput p1, p0, Lazw;->f:I

    return-object p0
.end method

.method public final a(Lizf;)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->n:Z

    iput-object p1, p0, Lazw;->d:Lizf;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->g:Z

    iput-object p1, p0, Lazw;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lazw;->a(I)Lazw;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazw;->a(Ljava/lang/String;)Lazw;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazw;->b(Ljava/lang/String;)Lazw;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazw;->c(Ljava/lang/String;)Lazw;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lazw;->b(I)Lazw;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazw;->d(Ljava/lang/String;)Lazw;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->f()Lizf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazw;->a(Lizf;)Lazw;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lazw;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lazw;->f:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Lazw;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lazw;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lazw;->i:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lazw;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Lazw;->k:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lazw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Lazw;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lazw;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_4
    iget-boolean v0, p0, Lazw;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lazw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Lazw;->n:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lazw;->d:Lizf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizf;)V

    :cond_6
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lazw;->e:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lazw;->f:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lazw;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lazw;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lazw;->i:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lazw;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lazw;->k:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lazw;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lazw;->l:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Lazw;->b:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lazw;->m:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lazw;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lazw;->n:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lazw;->d:Lizf;

    invoke-static {v1, v2}, Lizh;->b(ILizf;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lazw;->o:I

    return v0
.end method

.method public final b(I)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->l:Z

    iput p1, p0, Lazw;->b:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->i:Z

    iput-object p1, p0, Lazw;->j:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->k:Z

    iput-object p1, p0, Lazw;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lazw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lazw;->m:Z

    iput-object p1, p0, Lazw;->c:Ljava/lang/String;

    return-object p0
.end method
