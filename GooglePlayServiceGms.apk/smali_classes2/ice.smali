.class final Lice;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:D

.field private final c:D


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "1.1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "1.5"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "1.6"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "2.1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "2.2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3.0"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3.1"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3.2"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "4.1"

    aput-object v2, v0, v1

    sput-object v0, Lice;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lice;->b:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lice;->c:D

    return-void
.end method

.method private constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lice;->b:D

    iput-wide p3, p0, Lice;->c:D

    return-void
.end method

.method public static a(Lidu;)Lice;
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-interface {p0}, Lidu;->n()Lidv;

    move-result-object v1

    iget-object v0, v1, Lidv;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiNormalizer"

    const-string v1, "Can\'t recognize null fingerprint"

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lice;

    invoke-direct {v0}, Lice;-><init>()V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, v1, Lidv;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-ge v0, v8, :cond_4

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_3

    const-string v0, "WifiNormalizer"

    const-string v2, "Can\'t recognize fingerprint \"%s\""

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v1, v1, Lidv;->c:Ljava/lang/String;

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lice;

    invoke-direct {v0}, Lice;-><init>()V

    goto :goto_0

    :cond_4
    sget-object v0, Lice;->a:[Ljava/lang/String;

    aget-object v0, v0, v6

    iget v3, v1, Lidv;->d:I

    sget-object v4, Lice;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_7

    sget-object v0, Lice;->a:[Ljava/lang/String;

    iget v1, v1, Lidv;->d:I

    aget-object v0, v0, v1

    :cond_5
    :goto_1
    const-string v1, "%s/%s/%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v4, v2, v6

    aput-object v4, v3, v6

    aget-object v4, v2, v7

    aput-object v4, v3, v7

    aput-object v0, v3, v8

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s/%s/"

    new-array v3, v8, [Ljava/lang/Object;

    aget-object v4, v2, v6

    aput-object v4, v3, v6

    aget-object v2, v2, v7

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "models.txt"

    invoke-interface {p0, v2}, Lidu;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_8

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_6

    const-string v0, "WifiNormalizer"

    const-string v1, "Asset file doesn\'t exist."

    invoke-static {v0, v1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    new-instance v0, Lice;

    invoke-direct {v0}, Lice;-><init>()V

    goto :goto_0

    :cond_7
    sget-boolean v3, Licj;->d:Z

    if-eqz v3, :cond_5

    const-string v3, "WifiNormalizer"

    const-string v4, "We don\'t have support for version %d yet."

    new-array v5, v7, [Ljava/lang/Object;

    iget v1, v1, Lidv;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {v3, v0, v1}, Lice;->a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lice;

    move-result-object v0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    sget-boolean v2, Licj;->e:Z

    if-eqz v2, :cond_1

    const-string v2, "WifiNormalizer"

    const-string v3, "close"

    invoke-static {v2, v3, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private static a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lice;
    .locals 19

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    const-wide/16 v9, 0x0

    const-wide v5, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const/4 v1, 0x0

    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_9

    const-string v3, "#"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, ","

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x3

    if-ge v4, v5, :cond_2

    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_1

    const-string v3, "WifiNormalizer"

    const-string v4, "Invalid line \"%s\"."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v16, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v3, Lice;

    invoke-direct {v3}, Lice;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v3

    :goto_1
    return-object v1

    :cond_2
    const/4 v4, 0x1

    :try_start_1
    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const/4 v6, 0x2

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/16 v11, 0x0

    cmpg-double v3, v4, v11

    if-ltz v3, :cond_3

    const-wide/high16 v11, -0x3fa7000000000000L    # -100.0

    cmpg-double v3, v6, v11

    if-ltz v3, :cond_3

    const-wide/high16 v11, 0x4059000000000000L    # 100.0

    cmpl-double v3, v6, v11

    if-lez v3, :cond_5

    :cond_3
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_4

    const-string v3, "WifiNormalizer"

    const-string v8, "Invalid parameters %.2f, %.2f."

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v11, v12

    const/4 v4, 0x1

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v11, v4

    invoke-static {v8, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    new-instance v3, Lice;

    invoke-direct {v3}, Lice;-><init>()V

    move-object v1, v3

    goto :goto_1

    :cond_5
    sget-boolean v3, Licj;->c:Z

    if-eqz v3, :cond_6

    const-string v3, "WifiNormalizer"

    const-string v8, "Recognized %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v2, v11, v12

    invoke-static {v8, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lice;

    invoke-direct {v3, v4, v5, v6, v7}, Lice;-><init>(DD)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v3

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_7

    const-string v3, "WifiNormalizer"

    const-string v4, "Invalid line \"%s\"."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v16, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    new-instance v3, Lice;

    invoke-direct {v3}, Lice;-><init>()V

    move-object v1, v3

    goto/16 :goto_1

    :cond_8
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ","

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v7, 0x3

    if-ge v4, v7, :cond_b

    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_0

    const-string v3, "WifiNormalizer"

    const-string v4, "Invalid line \"%s\"."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v16, v7, v8

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v3

    sget-boolean v4, Licj;->e:Z

    if-eqz v4, :cond_9

    const-string v4, "WifiNormalizer"

    const-string v5, "readLine"

    invoke-static {v4, v5, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_9
    if-eqz v1, :cond_e

    sget-boolean v3, Licj;->d:Z

    if-eqz v3, :cond_a

    const-string v3, "WifiNormalizer"

    const-string v4, "Can\'t recognize %s but fallback %s found"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    new-instance v1, Lice;

    invoke-direct {v1, v13, v14, v9, v10}, Lice;-><init>(DD)V

    goto/16 :goto_1

    :cond_b
    const/4 v4, 0x1

    :try_start_3
    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    const/4 v4, 0x2

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide/16 v17, 0x0

    cmpg-double v17, v11, v17

    if-ltz v17, :cond_c

    const-wide/high16 v17, -0x3fa7000000000000L    # -100.0

    cmpg-double v17, v7, v17

    if-ltz v17, :cond_c

    const-wide/high16 v17, 0x4059000000000000L    # 100.0

    cmpl-double v17, v7, v17

    if-lez v17, :cond_d

    :cond_c
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_0

    const-string v3, "WifiNormalizer"

    const-string v4, "Invalid parameters %.2f, %.2f."

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v17, v18

    const/4 v11, 0x1

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v17, v11

    move-object/from16 v0, v17

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :catch_2
    move-exception v3

    :try_start_4
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_0

    const-string v3, "WifiNormalizer"

    const-string v4, "Invalid line \"%s\"."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v16, v7, v8

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :cond_d
    cmpg-double v16, v3, v5

    if-gez v16, :cond_10

    move-object v1, v2

    move-wide v5, v7

    move-wide v7, v11

    :goto_2
    move-wide v9, v5

    move-wide v13, v7

    move-wide v5, v3

    goto/16 :goto_0

    :cond_e
    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_f

    const-string v1, "WifiNormalizer"

    const-string v3, "Can\'t recognize %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    new-instance v1, Lice;

    invoke-direct {v1}, Lice;-><init>()V

    goto/16 :goto_1

    :cond_10
    move-wide v3, v5

    move-wide v7, v13

    move-wide v5, v9

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Ljava/util/Map;
    .locals 8

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-wide v4, p0, Lice;->b:D

    int-to-double v6, v0

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lice;->c:D

    add-double/2addr v4, v6

    double-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method
