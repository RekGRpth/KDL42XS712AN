.class final Lhap;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhao;


# direct methods
.method constructor <init>(Lhao;)V
    .locals 0

    iput-object p1, p0, Lhap;->a:Lhao;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lhap;->a:Lhao;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lhao;->a(Lhao;I)V

    return-void
.end method

.method public final a(Lipy;)V
    .locals 4

    const/4 v2, 0x0

    invoke-static {p1}, Lhfx;->a(Lipy;)I

    move-result v0

    invoke-static {p1, v2}, Lhfx;->a(Lipy;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lipy;->d:Ljak;

    iget-object v1, v1, Ljak;->c:Ljal;

    iget-object v2, p0, Lhap;->a:Lhao;

    iget-object v3, v1, Ljal;->a:Ljava/lang/String;

    iget-object v1, v1, Ljal;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lhao;->a(Lhao;Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x198

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhap;->a:Lhao;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lhao;->a(Lhao;ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhap;->a:Lhao;

    invoke-static {v1, v0}, Lhao;->a(Lhao;I)V

    goto :goto_0
.end method

.method public final a(Ljap;)V
    .locals 3

    iget v0, p1, Ljap;->a:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "AuthenticateInstrumentF"

    const-string v1, "Unexpected authenticate instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhap;->a:Lhao;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lhao;->a(Lhao;I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lhap;->a:Lhao;

    iget-object v0, v0, Lhao;->h:Lhca;

    iget-object v1, p0, Lhap;->a:Lhao;

    invoke-static {v1}, Lhao;->a(Lhao;)Ljau;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhca;->a(Ljau;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lhap;->a:Lhao;

    iget-object v0, v0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lhap;->a:Lhao;

    invoke-virtual {v1}, Lhao;->T_()Lo;

    move-result-object v1

    const v2, 0x7f0b015d    # com.google.android.gms.R.string.wallet_error_cvc_invalid

    invoke-virtual {v1, v2}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->b(Lhao;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljav;J)V
    .locals 3

    iget-object v0, p1, Ljav;->i:[I

    array-length v0, v0

    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p1, Ljav;->i:[I

    aget v1, v1, v0

    packed-switch v1, :pswitch_data_0

    iget-object v0, p0, Lhap;->a:Lhao;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lhao;->a(Lhao;I)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lhap;->a:Lhao;

    iget-object v0, v0, Lhao;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lhap;->a:Lhao;

    invoke-virtual {v1}, Lhao;->T_()Lo;

    move-result-object v1

    const v2, 0x7f0b015d    # com.google.android.gms.R.string.wallet_error_cvc_invalid

    invoke-virtual {v1, v2}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->b(Lhao;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->c(Lhao;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->a(Lhao;)Ljau;

    move-result-object v0

    iget-object v1, p0, Lhap;->a:Lhao;

    invoke-static {v1}, Lhao;->d(Lhao;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1, p2, p3}, Lhfx;->a(Ljav;Ljau;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->d(Lhao;)Landroid/accounts/Account;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Landroid/accounts/Account;

    invoke-static {v0, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lgrt;->a:Landroid/accounts/Account;

    :goto_3
    const-string v2, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lhap;->a:Lhao;

    const/4 v2, -0x1

    invoke-static {v0, v2, v1}, Lhao;->a(Lhao;ILandroid/content/Intent;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0, p1}, Lhao;->a(Lhao;Ljav;)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->d(Lhao;)Landroid/accounts/Account;

    move-result-object v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhap;->a:Lhao;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lhao;->a(Lhao;I)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhap;->a:Lhao;

    invoke-static {v0}, Lhao;->e(Lhao;)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lhap;->a:Lhao;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lhao;->a(Lhao;I)V

    return-void
.end method
