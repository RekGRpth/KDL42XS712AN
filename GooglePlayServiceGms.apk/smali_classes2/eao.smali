.class public final Leao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/database/CharArrayBuffer;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/widget/TextView;

.field final f:Landroid/view/View;

.field final g:Ljava/util/ArrayList;

.field final h:Landroid/widget/TextView;

.field final i:Landroid/view/View;

.field final j:Landroid/view/View;

.field final synthetic k:Lean;


# direct methods
.method public constructor <init>(Lean;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Leao;->k:Lean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leao;->g:Ljava/util/ArrayList;

    sget v0, Lxa;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Leao;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->F:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leao;->b:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Leao;->c:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->G:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leao;->d:Landroid/widget/TextView;

    sget v0, Lxa;->ao:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leao;->e:Landroid/widget/TextView;

    iget-object v1, p0, Leao;->g:Ljava/util/ArrayList;

    sget v0, Lxa;->av:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Leao;->g:Ljava/util/ArrayList;

    sget v0, Lxa;->aw:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Leao;->g:Ljava/util/ArrayList;

    sget v0, Lxa;->ax:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Leao;->g:Ljava/util/ArrayList;

    sget v0, Lxa;->ay:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v0, Lxa;->u:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leao;->f:Landroid/view/View;

    sget v0, Lxa;->a:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leao;->h:Landroid/widget/TextView;

    iget-object v0, p0, Leao;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->n:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leao;->i:Landroid/view/View;

    iget-object v0, p0, Leao;->i:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leao;->j:Landroid/view/View;

    iget-object v0, p0, Leao;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Leao;->k:Lean;

    invoke-virtual {v0}, Lean;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v3, v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxa;->ah:I

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    invoke-interface {v1, v0}, Leab;->b(Lcom/google/android/gms/games/Game;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    sget v4, Lxa;->ad:I

    if-ne v3, v4, :cond_1

    iget-object v1, p0, Leao;->k:Lean;

    invoke-virtual {v1}, Lean;->e()Lbgo;

    move-result-object v1

    check-cast v1, Lbhb;

    invoke-virtual {v1, v0}, Lbhb;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Leao;->k:Lean;

    invoke-virtual {v1}, Lean;->notifyDataSetChanged()V

    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    invoke-interface {v1, v0}, Leab;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    move v0, v2

    goto :goto_0

    :cond_1
    sget v4, Lxa;->ai:I

    if-ne v3, v4, :cond_2

    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    iget-object v3, p0, Leao;->k:Lean;

    invoke-static {v3}, Lean;->c(Lean;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Leao;->k:Lean;

    invoke-static {v4}, Lean;->a(Lean;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v3, v4}, Leab;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lxa;->a:I

    if-ne v2, v3, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    invoke-interface {v1, v0}, Leab;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Leao;->k:Lean;

    invoke-static {v0}, Lean;->b(Lean;)Leab;

    move-result-object v0

    invoke-interface {v0, v1}, Leab;->a_(Lcom/google/android/gms/games/Game;)V

    goto :goto_0

    :cond_2
    sget v3, Lxa;->n:I

    if-ne v2, v3, :cond_3

    iget-object v1, p0, Leao;->k:Lean;

    invoke-virtual {v1}, Lean;->e()Lbgo;

    move-result-object v1

    check-cast v1, Lbhb;

    invoke-virtual {v1, v0}, Lbhb;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Leao;->k:Lean;

    invoke-virtual {v1}, Lean;->notifyDataSetChanged()V

    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    invoke-interface {v1, v0}, Leab;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    :cond_3
    sget v3, Lxa;->ap:I

    if-ne v2, v3, :cond_4

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    sget v1, Lxd;->b:I

    invoke-virtual {v0, v1}, Lov;->a(I)V

    new-instance v1, Ledk;

    invoke-direct {v1, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v1, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto :goto_0

    :cond_4
    sget v3, Lxa;->aB:I

    if-eq v2, v3, :cond_5

    sget v3, Lxa;->av:I

    if-eq v2, v3, :cond_5

    sget v3, Lxa;->aw:I

    if-eq v2, v3, :cond_5

    sget v3, Lxa;->ax:I

    if-eq v2, v3, :cond_5

    sget v3, Lxa;->ay:I

    if-eq v2, v3, :cond_5

    sget v3, Lxa;->u:I

    if-ne v2, v3, :cond_6

    :cond_5
    iget-object v1, p0, Leao;->k:Lean;

    invoke-static {v1}, Lean;->b(Lean;)Leab;

    move-result-object v1

    iget-object v2, p0, Leao;->k:Lean;

    invoke-static {v2}, Lean;->c(Lean;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Leao;->k:Lean;

    invoke-static {v3}, Lean;->a(Lean;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Leab;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v0, "PublicInvitListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
