.class public Lfmb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lfmb;


# instance fields
.field private final c:Lfmc;

.field private final d:Lfmc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lfmb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfmb;->a:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lfmb;->b:Lfmb;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfmc;

    const-class v1, Lfle;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lfmc;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lfmb;->c:Lfmc;

    new-instance v0, Lfmc;

    const-class v1, Lflf;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lfmc;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lfmb;->d:Lfmc;

    return-void
.end method

.method public static declared-synchronized a()Lfmb;
    .locals 2

    const-class v1, Lfmb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lfmb;->b:Lfmb;

    if-nez v0, :cond_0

    new-instance v0, Lfmb;

    invoke-direct {v0}, Lfmb;-><init>()V

    sput-object v0, Lfmb;->b:Lfmb;

    :cond_0
    sget-object v0, Lfmb;->b:Lfmb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lfmb;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lfle;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lfle;->f:[Lflf;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p1, Lfle;->f:[Lflf;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lflf;->d()Lflf;

    iget-object v2, p0, Lfmb;->d:Lfmc;

    invoke-virtual {v2, v1}, Lfmc;->a(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lfle;->d()Lfle;

    iget-object v0, p0, Lfmb;->c:Lfmc;

    invoke-virtual {v0, p1}, Lfmc;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Lfle;
    .locals 1

    iget-object v0, p0, Lfmb;->c:Lfmc;

    invoke-virtual {v0}, Lfmc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfle;

    return-object v0
.end method

.method public final c()Lflf;
    .locals 1

    iget-object v0, p0, Lfmb;->d:Lfmc;

    invoke-virtual {v0}, Lfmc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflf;

    return-object v0
.end method
