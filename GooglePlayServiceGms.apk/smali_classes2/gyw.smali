.class public final Lgyw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:Landroid/widget/EditText;

.field private final b:Lgwi;

.field private final c:Lgyo;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;Lgwi;Lgyo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgyw;->a:Landroid/widget/EditText;

    iput-object p2, p0, Lgyw;->b:Lgwi;

    iput-object p3, p0, Lgyw;->c:Lgyo;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    if-lez p4, :cond_0

    iget-object v0, p0, Lgyw;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyw;->b:Lgwi;

    invoke-interface {v0}, Lgwi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyw;->c:Lgyo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyw;->c:Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyw;->a:Landroid/widget/EditText;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method
