.class public final Lcna;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:J


# direct methods
.method protected constructor <init>(JLcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 6

    sget-object v1, Lcmr;->h:Lcmr;

    sget-object v5, Lcms;->b:Lcms;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-wide p1, p0, Lcna;->c:J

    return-void
.end method

.method protected constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->h:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcna;->c:J

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 4

    invoke-virtual {p2}, Lcfp;->l()V

    new-instance v0, Lcmj;

    iget-object v1, p0, Lclu;->b:Lcfc;

    iget-object v2, p3, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcms;->b:Lcms;

    invoke-direct {v0, v1, v2, v3}, Lcmj;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 0

    return-void
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "pendingUploadSqlId"

    iget-wide v2, p0, Lcna;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcna;

    iget-wide v2, p0, Lcna;->c:J

    iget-wide v4, p1, Lcna;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget-wide v0, p0, Lcna;->c:J

    iget-wide v2, p0, Lcna;->c:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "UndoCreateFileOp[%s, pendingUploadSqlId=%d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcna;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcna;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
