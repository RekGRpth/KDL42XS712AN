.class public abstract Lfqz;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/CheckBox;

.field private d:Lfra;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfqz;->a(Lfra;)V

    invoke-virtual {p0}, Lfqz;->f()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfqz;->d(Z)V

    return-void
.end method

.method public a(Lfra;)V
    .locals 0

    iput-object p1, p0, Lfqz;->d:Lfra;

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lfqz;->a:Ljava/lang/Object;

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v1, p0, Lfqz;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lfqz;->e:Z

    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfqz;->d(Z)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lfqz;->setClickable(Z)V

    invoke-virtual {p0, p1}, Lfqz;->setFocusable(Z)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lfqz;->e:Z

    return v0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void
.end method

.method public h()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lfqz;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-object v0, p0, Lfqz;->d:Lfra;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqz;->d:Lfra;

    invoke-interface {v0, p0, p2}, Lfra;->a(Lfqz;Z)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v1, p0, Lfqz;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f0a0262    # com.google.android.gms.R.id.top_border

    invoke-virtual {p0, v0}, Lfqz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfqz;->b:Landroid/view/View;

    const v0, 0x7f0a0264    # com.google.android.gms.R.id.audience_selection_checkbox

    invoke-virtual {p0, v0}, Lfqz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0, p0}, Lfqz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lfqz;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    return-void
.end method
