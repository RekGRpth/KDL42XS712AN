.class final Lgpc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgpb;

.field private final b:Lgox;

.field private final c:I


# direct methods
.method public constructor <init>(Lgpb;Landroid/content/res/Resources;)V
    .locals 1

    iput-object p1, p0, Lgpc;->a:Lgpb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lgox;

    invoke-direct {v0}, Lgox;-><init>()V

    iput-object v0, p0, Lgpc;->b:Lgox;

    const v0, 0x7f0d0180    # com.google.android.gms.R.dimen.plus_sharebox_mention_suggestion_min_space

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lgpc;->c:I

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-virtual {v0}, Lgpb;->T_()Lo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->a(Lgpb;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->d(Lgpb;)Z

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->e(Lgpb;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->f(Lgpb;)Lgpd;

    move-result-object v0

    invoke-interface {v0}, Lgpd;->f()Lgpe;

    move-result-object v0

    sget-object v1, Lbcz;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->g(Lgpb;)Z

    :cond_2
    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->f(Lgpb;)Lgpd;

    move-result-object v0

    invoke-interface {v0}, Lgpd;->i()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-virtual {v0}, Lgpb;->T_()Lo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->a(Lgpb;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lgpc;->b:Lgox;

    invoke-virtual {v1, p1, v0}, Lgox;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    iget-object v2, p0, Lgpc;->a:Lgpb;

    invoke-static {v2}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getThreshold()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v1, v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lgpc;->a:Lgpb;

    invoke-static {v1}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLocationOnScreen([I)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lgpc;->a:Lgpb;

    invoke-static {v2}, Lgpb;->a(Lgpb;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v2, 0x1

    aget v0, v0, v2

    iget-object v2, p0, Lgpc;->a:Lgpb;

    invoke-static {v2}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int v0, v1, v0

    iget v1, p0, Lgpc;->c:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lgpc;->a:Lgpb;

    invoke-static {v0}, Lgpb;->c(Lgpb;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lgpc;->a:Lgpb;

    invoke-static {v2}, Lgpb;->b(Lgpb;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
