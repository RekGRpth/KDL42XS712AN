.class public final Lbrr;
.super Lbqz;
.source "SourceFile"


# instance fields
.field private final b:Lbrb;

.field private final c:Lbrf;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[Ljava/lang/String;

.field private final h:I


# direct methods
.method public constructor <init>(Lbrb;Lbrf;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lbjv;I)V
    .locals 0

    invoke-direct {p0, p7}, Lbqz;-><init>(Landroid/os/IInterface;)V

    iput-object p1, p0, Lbrr;->b:Lbrb;

    iput-object p2, p0, Lbrr;->c:Lbrf;

    iput p3, p0, Lbrr;->d:I

    iput-object p4, p0, Lbrr;->e:Ljava/lang/String;

    iput-object p5, p0, Lbrr;->f:Ljava/lang/String;

    iput-object p6, p0, Lbrr;->g:[Ljava/lang/String;

    iput p8, p0, Lbrr;->h:I

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/api/DriveAsyncService;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ClientConnectionOperation"

    const-string v1, "Because of limited internal storage, this device requires an SD card to store Drive content, but no SD card is mounted."

    invoke-static {v0, v1}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v0, Lbjv;

    const/16 v1, 0x5dc

    invoke-interface {v0, v1, v8, v8}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a()Lbri;

    move-result-object v0

    iget-object v1, p0, Lbrr;->f:Ljava/lang/String;

    iget-object v2, p0, Lbrr;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lbri;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lbrr;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v0, Lbjv;

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lbsl; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    const-string v0, "ClientConnectionOperation"

    const-string v2, "Handling authorization failure"

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    iget-object v0, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v0, Lbjv;

    invoke-virtual {v1}, Lbsl;->a()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v3

    invoke-static {p1, v6, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v3, "pendingIntent"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-interface {v0, v9, v8, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {v0}, Lbox;->a(Landroid/content/Intent;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.account.AccountPickerActivity"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "allowableAccountTypes"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "setGmsCoreAccount"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v0, Lbjv;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lbrr;->d:I

    iget-object v3, p0, Lbrr;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v0, v3}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lbrr;->g:[Ljava/lang/String;

    array-length v3, v2

    move v0, v6

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lbrr;->b:Lbrb;

    iget v0, p0, Lbrr;->h:I

    invoke-static {p1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v3

    new-instance v2, Lbrd;

    invoke-direct {v2, v1, v0, v3}, Lbrd;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILcoy;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lbrd;->a(Z)Lbsp;

    move-result-object v0

    iget-object v0, v0, Lbsp;->a:Lcfc;

    iget-object v0, v0, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcoy;->k()Lbwd;

    move-result-object v1

    const/16 v3, 0x65

    invoke-virtual {v1, v0, v3}, Lbwd;->a(Ljava/lang/String;I)I

    iget-object v1, p0, Lbrr;->c:Lbrf;

    iget-object v0, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v0, Lbjv;

    invoke-interface {v0}, Lbjv;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    new-instance v0, Lbre;

    iget-object v3, v1, Lbrf;->a:Lbra;

    iget-object v4, v1, Lbrf;->b:Lbrl;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lbre;-><init>(Landroid/content/Context;Lbrc;Lbra;Lbrl;B)V

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.drive.root_id"

    invoke-interface {v2}, Lbrc;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "com.google.android.gms.drive.appdata_id"

    invoke-interface {v2}, Lbrc;->c()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lbrr;->a:Landroid/os/IInterface;

    check-cast v1, Lbjv;

    const/4 v2, 0x0

    invoke-interface {v0}, Lcho;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v1, v2, v0, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Lbsl; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v1}, Lbsl;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    invoke-interface {v0, v1, v8, v8}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_5
    const/16 v1, 0x8

    invoke-interface {v0, v1, v8, v8}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
