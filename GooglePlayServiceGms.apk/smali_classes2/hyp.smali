.class abstract Lhyp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private volatile b:Z

.field private volatile c:Ljava/lang/Object;

.field public final d:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhyp;-><init>(ILjava/lang/Object;)V

    return-void
.end method

.method protected constructor <init>(ILjava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhyp;->b:Z

    iput p1, p0, Lhyp;->a:I

    iput-object p2, p0, Lhyp;->d:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Lbqi;)V
    .locals 1

    iget v0, p0, Lhyp;->a:I

    invoke-virtual {p1, v0, p0}, Lbqi;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 1

    iput-object p1, p0, Lhyp;->c:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyp;->b:Z

    invoke-virtual {p0}, Lhyp;->a()V

    return-void
.end method

.method protected final c()Ljava/lang/Object;
    .locals 2

    iget-boolean v0, p0, Lhyp;->b:Z

    const-string v1, "getResultWhenAvailable called before result is set."

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhyp;->c:Ljava/lang/Object;

    return-object v0
.end method
