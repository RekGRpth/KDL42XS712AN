.class final Lfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsk;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Lfie;


# direct methods
.method constructor <init>(Lfie;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lfig;->c:Lfie;

    iput-object p2, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    iput-object p3, p0, Lfig;->b:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->q()V

    :try_start_0
    iget-object v0, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-static {v0}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lfig;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    iget-object v1, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v0, v1}, Lfip;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v1, v0, Lfid;->x:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->x:I

    :goto_0
    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    iget-object v1, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v0, v1}, Lfip;->d(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfhr;->b(Ljava/util/List;)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgld;

    const-string v4, "allCircles"

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0, v2}, Lfip;->j(Ljava/lang/String;)V

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v1, v0, Lfid;->A:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->A:I

    :cond_0
    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->t()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    invoke-virtual {v0}, Lfip;->s()V

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lfig;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->a(Lfie;)Lfip;

    move-result-object v0

    iget-object v1, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v0, v1}, Lfip;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v1, v0, Lfid;->y:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfid;->y:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "PeopleSync"

    const-string v2, "Failed"

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfig;->c:Lfie;

    invoke-static {v1}, Lfie;->a(Lfie;)Lfip;

    move-result-object v1

    invoke-virtual {v1}, Lfip;->s()V

    throw v0

    :cond_2
    :try_start_3
    const-string v4, "circle"

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lfig;->c:Lfie;

    invoke-static {v4}, Lfie;->a(Lfie;)Lfip;

    move-result-object v4

    iget-object v5, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-interface {v0}, Lgld;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lfip;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v4, v0, Lfid;->B:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lfid;->B:I

    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_4
    const-string v4, "person"

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lfig;->c:Lfie;

    invoke-static {v4}, Lfie;->a(Lfie;)Lfip;

    move-result-object v4

    iget-object v5, p0, Lfig;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-interface {v0}, Lgld;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lfip;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V

    iget-object v0, p0, Lfig;->c:Lfie;

    invoke-static {v0}, Lfie;->b(Lfie;)Lfid;

    move-result-object v0

    iget v4, v0, Lfid;->C:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lfid;->C:I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
