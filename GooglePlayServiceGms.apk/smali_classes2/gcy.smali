.class public final Lgcy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:[Ljava/lang/String;

.field private final f:[Ljava/lang/String;

.field private final g:I

.field private final h:[Ljava/lang/String;

.field private final i:Lbjv;

.field private final j:Ljava/lang/String;

.field private final k:Lgau;

.field private final l:Lgbk;

.field private final m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lbjv;Ljava/lang/String;Lgau;Lgbk;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcy;->a:Ljava/lang/String;

    iput-object p2, p0, Lgcy;->b:Ljava/lang/String;

    iput-object p3, p0, Lgcy;->c:Ljava/lang/String;

    iput-object p4, p0, Lgcy;->d:Ljava/lang/String;

    iput-object p5, p0, Lgcy;->e:[Ljava/lang/String;

    iput-object p6, p0, Lgcy;->f:[Ljava/lang/String;

    iput p7, p0, Lgcy;->g:I

    iput-object p9, p0, Lgcy;->i:Lbjv;

    iput-object p8, p0, Lgcy;->h:[Ljava/lang/String;

    iput-object p10, p0, Lgcy;->j:Ljava/lang/String;

    iput-object p11, p0, Lgcy;->k:Lgau;

    iput-object p12, p0, Lgcy;->l:Lgbk;

    iput-object p13, p0, Lgcy;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lbbo;
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    :try_start_0
    iget-object v0, p0, Lgcy;->k:Lgau;

    iget-object v2, p0, Lgcy;->c:Ljava/lang/String;

    iget v3, p0, Lgcy;->g:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lgcy;->f:[Ljava/lang/String;

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lgau;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lbbo;
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lfmu;

    invoke-direct {v1, p1, p2}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v1, Lfmu;->c:Landroid/content/Intent;

    iput v8, v1, Lfmu;->b:I

    const-string v0, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v7, v1, Lfmu;->a:Z

    :cond_0
    iget-object v0, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v1}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v0, v2, v1}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lfmu;

    invoke-direct {v0, p1, p2}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v8, v0, Lfmu;->b:I

    invoke-virtual {v0}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-static {p1, v1, v2, v0}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v0, Lfmu;

    invoke-direct {v0, p1, p2}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const-string v1, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v7, v0, Lfmu;->a:Z

    :cond_1
    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v1, v2, v0}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lgcy;->a:Ljava/lang/String;

    iget-object v4, p0, Lgcy;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lgau;->b:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    const-string v1, "application_name"

    iget-object v2, p0, Lgcy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lgcy;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->k()Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->d()V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lgcy;->g:I

    iget-object v2, p0, Lgcy;->a:Ljava/lang/String;

    iget-object v4, p0, Lgcy;->b:Ljava/lang/String;

    iget-object v5, p0, Lgcy;->c:Ljava/lang/String;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    iget-object v1, p0, Lgcy;->f:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b([Ljava/lang/String;)V

    const-string v1, "application_name"

    iget-object v2, p0, Lgcy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lgcy;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->k()Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->d()V

    return-object v0
.end method

.method private a(Landroid/content/Context;Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V

    return-void
.end method

.method private a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V
    .locals 4

    invoke-virtual {p2}, Lbbo;->c()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lbbo;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ValidateAccountOperatio"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no resolution provided for status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no resolution provided!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lgcy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lgbk;->d(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p2}, Lbbo;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "pendingIntent"

    invoke-virtual {p2}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    if-eqz p4, :cond_3

    const-string v1, "loaded_person"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_3
    iget-object v1, p0, Lgcy;->i:Lbjv;

    invoke-virtual {p2}, Lbbo;->c()I

    move-result v2

    invoke-interface {v1, v2, p3, v0}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lfrx;)[B
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p3, p1, p2}, Lfrx;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lfmu;

    invoke-direct {v1, p1, p2}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v1, Lfmu;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {p1, v1, v2, v0}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;)V

    new-array v0, v3, [B

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lfmu;

    invoke-direct {v0, p1, p2}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const-string v1, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfmu;->a:Z

    :cond_0
    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v1, v2, v0}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;)V

    new-array v0, v3, [B

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 10

    iget-object v0, p0, Lgcy;->l:Lgbk;

    invoke-interface {v0, p1}, Lgbk;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->a:Ljava/lang/String;

    iget-object v2, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lgbk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lgcy;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgcy;->e:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    const-string v0, "plus_one_placeholder_scope"

    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    array-length v1, v1

    if-eqz v1, :cond_3

    :cond_2
    if-eqz v0, :cond_6

    :cond_3
    sget-object v8, Lbbo;->a:Lbbo;

    iget-object v0, p0, Lgcy;->k:Lgau;

    iget-object v1, p0, Lgcy;->j:Ljava/lang/String;

    invoke-direct {p0, p1, v7}, Lgcy;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-direct {p0, v7}, Lgcy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    new-instance v5, Lgax;

    invoke-direct {v5}, Lgax;-><init>()V

    iget-object v6, p0, Lgcy;->f:[Ljava/lang/String;

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lgau;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v8, v0, v1}, Lgcy;->a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_0

    new-instance v5, Lgcz;

    invoke-direct {v5, v7}, Lgcz;-><init>(Ljava/lang/String;)V

    if-nez v7, :cond_4

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lgcy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lgcy;->a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    iput-boolean v0, v5, Lgcz;->c:Z

    :cond_4
    :goto_3
    iget-boolean v0, v5, Lgcz;->c:Z

    if-nez v0, :cond_0

    iget-object v6, v5, Lgcz;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v6}, Lgcy;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-direct {p0, v6}, Lgcy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    iget-object v0, p0, Lgcy;->k:Lgau;

    iget-object v1, p0, Lgcy;->h:[Ljava/lang/String;

    invoke-interface {v0, p1, v3, v4, v1}, Lgau;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->c()I

    move-result v1

    if-eqz v1, :cond_e

    invoke-direct {p0, p1, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lgcy;->k:Lgau;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lgau;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lgbk;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_a

    const/4 v2, 0x0

    :cond_8
    :goto_5
    iput-object v2, v5, Lgcz;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lgcz;->b:Z

    iget-object v0, v5, Lgcz;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgcy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    new-instance v1, Lfmu;

    invoke-direct {v1, p1, v0}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v0, 0x1

    iput-boolean v0, v1, Lfmu;->a:Z

    const/4 v0, 0x2

    iput v0, v1, Lfmu;->b:I

    invoke-virtual {v1}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {p1, v1, v2, v0}, Lgav;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lbbo;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lgcy;->a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    iput-boolean v0, v5, Lgcz;->c:Z

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lgbk;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v3, v0

    :goto_6
    if-ge v3, v6, :cond_c

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgcy;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v7

    invoke-direct {p0, p1, v7}, Lgcy;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lbbo;

    move-result-object v7

    invoke-virtual {v7}, Lbbo;->c()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    move v0, v1

    move-object v1, v2

    :goto_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_6

    :sswitch_0
    if-nez v2, :cond_b

    move v9, v1

    move-object v1, v0

    move v0, v9

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lgbk;->c(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_5

    :sswitch_1
    const/4 v0, 0x1

    move-object v1, v2

    goto :goto_7

    :cond_c
    if-eqz v1, :cond_d

    if-eqz v2, :cond_8

    :cond_d
    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lgbk;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_5

    :cond_e
    invoke-direct {p0, p1, v4}, Lgcy;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-direct {p0, p1, v0}, Lgcy;->a(Landroid/content/Context;Lbbo;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v0

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v1

    if-ne v0, v1, :cond_15

    const/4 v0, 0x1

    :goto_8
    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    invoke-static {v1, v2}, Lboz;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    iget-object v1, p0, Lgcy;->e:[Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.login"

    invoke-static {v1, v2}, Lboz;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    :cond_10
    const/4 v1, 0x1

    move v2, v1

    :goto_9
    const/4 v1, 0x0

    if-nez v0, :cond_17

    if-eqz v2, :cond_17

    invoke-direct {p0, p1, v4, p2}, Lgcy;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lfrx;)[B

    move-result-object v0

    if-eqz v0, :cond_11

    array-length v1, v0

    if-eqz v1, :cond_0

    :cond_11
    move-object v7, v0

    :goto_a
    iget-boolean v0, v5, Lgcz;->b:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lgcy;->l:Lgbk;

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v6}, Lgbk;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lgcy;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_12
    :goto_b
    if-nez v0, :cond_13

    const v0, 0x7f0b0381    # com.google.android.gms.R.string.plus_unknown_app_name

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_13
    const v1, 0x7f0b03a9    # com.google.android.gms.R.string.plus_cross_client_auth_toast_text

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    const/4 v0, 0x1

    aput-object v6, v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lgda;

    iget-object v5, p0, Lgcy;->b:Ljava/lang/String;

    invoke-direct {v2, p1, v0, v6, v5}, Lgda;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_14
    sget-object v8, Lbbo;->a:Lbbo;

    iget-object v0, p0, Lgcy;->k:Lgau;

    iget-object v1, p0, Lgcy;->j:Ljava/lang/String;

    new-instance v5, Lgax;

    invoke-direct {v5}, Lgax;-><init>()V

    iget-object v6, p0, Lgcy;->f:[Ljava/lang/String;

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lgau;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, p1, v8, v0, v7}, Lgcy;->a(Landroid/content/Context;Lbbo;Landroid/os/IBinder;[B)V

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lgcy;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_8

    :cond_16
    const/4 v1, 0x0

    move v2, v1

    goto :goto_9

    :catch_0
    move-exception v1

    goto :goto_b

    :cond_17
    move-object v7, v1

    goto :goto_a

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Lgcy;->i:Lbjv;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lgcy;->i:Lbjv;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
