.class public interface abstract Landroid/os/storage/IISOActionListener;
.super Ljava/lang/Object;
.source "IISOActionListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/storage/IISOActionListener$Stub;
    }
.end annotation


# static fields
.field public static final MOUNTED:I = 0x1

.field public static final UNMOUNTED:I = 0x2


# virtual methods
.method public abstract onISOEvent(Ljava/lang/String;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
