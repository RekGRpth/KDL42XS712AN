.class public final Landroid/view/PointerIcon;
.super Ljava/lang/Object;
.source "PointerIcon.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/PointerIcon;",
            ">;"
        }
    .end annotation
.end field

.field public static final STYLE_ARROW:I = 0x3e8

.field public static final STYLE_CUSTOM:I = -0x1

.field private static final STYLE_DEFAULT:I = 0x3e8

.field public static final STYLE_NULL:I = 0x0

.field private static final STYLE_OEM_FIRST:I = 0x2710

.field public static final STYLE_SPOT_ANCHOR:I = 0x7d2

.field public static final STYLE_SPOT_BLANK1:I = 0x7fc

.field public static final STYLE_SPOT_BLANK2:I = 0x7fd

.field public static final STYLE_SPOT_BLANK3:I = 0x7fe

.field public static final STYLE_SPOT_BLANK4:I = 0x7ff

.field public static final STYLE_SPOT_BLANK5:I = 0x800

.field public static final STYLE_SPOT_CLAW1:I = 0x7d3

.field public static final STYLE_SPOT_CLAW2:I = 0x7d4

.field public static final STYLE_SPOT_HAND:I = 0x7d5

.field public static final STYLE_SPOT_HOLD1:I = 0x7db

.field public static final STYLE_SPOT_HOLD2:I = 0x7dc

.field public static final STYLE_SPOT_HOLD3:I = 0x7dd

.field public static final STYLE_SPOT_HOLD4:I = 0x7de

.field public static final STYLE_SPOT_HOLD5:I = 0x7df

.field public static final STYLE_SPOT_HOLD6:I = 0x7e0

.field public static final STYLE_SPOT_HOLD7:I = 0x7e1

.field public static final STYLE_SPOT_HOLD8:I = 0x7e2

.field public static final STYLE_SPOT_HOLD9:I = 0x7e3

.field public static final STYLE_SPOT_HOVER:I = 0x7d0

.field public static final STYLE_SPOT_LBACK1:I = 0x7e4

.field public static final STYLE_SPOT_LBACK2:I = 0x7e5

.field public static final STYLE_SPOT_LBACK3:I = 0x7e6

.field public static final STYLE_SPOT_LBACK4:I = 0x7e7

.field public static final STYLE_SPOT_LBACK5:I = 0x7e8

.field public static final STYLE_SPOT_LBACK6:I = 0x7e9

.field public static final STYLE_SPOT_LBACK7:I = 0x7ea

.field public static final STYLE_SPOT_LBACK8:I = 0x7eb

.field public static final STYLE_SPOT_PAGEDOWN:I = 0x7f0

.field public static final STYLE_SPOT_PAGEDOWN1:I = 0x7f1

.field public static final STYLE_SPOT_PAGEDOWN2:I = 0x7f2

.field public static final STYLE_SPOT_PAGEDOWN3:I = 0x7f3

.field public static final STYLE_SPOT_PAGEUP:I = 0x7ec

.field public static final STYLE_SPOT_PAGEUP1:I = 0x7ed

.field public static final STYLE_SPOT_PAGEUP2:I = 0x7ee

.field public static final STYLE_SPOT_PAGEUP3:I = 0x7ef

.field public static final STYLE_SPOT_RBACK1:I = 0x7f4

.field public static final STYLE_SPOT_RBACK2:I = 0x7f5

.field public static final STYLE_SPOT_RBACK3:I = 0x7f6

.field public static final STYLE_SPOT_RBACK4:I = 0x7f7

.field public static final STYLE_SPOT_RBACK5:I = 0x7f8

.field public static final STYLE_SPOT_RBACK6:I = 0x7f9

.field public static final STYLE_SPOT_RBACK7:I = 0x7fa

.field public static final STYLE_SPOT_RBACK8:I = 0x7fb

.field public static final STYLE_SPOT_TOUCH:I = 0x7d1

.field public static final STYLE_SPOT_WAVE1:I = 0x7d6

.field public static final STYLE_SPOT_WAVE2:I = 0x7d7

.field public static final STYLE_SPOT_WAVE3:I = 0x7d8

.field public static final STYLE_SPOT_WAVE4:I = 0x7d9

.field public static final STYLE_SPOT_WAVE5:I = 0x7da

.field private static final TAG:Ljava/lang/String; = "PointerIcon"

.field private static final gNullIcon:Landroid/view/PointerIcon;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mHotSpotX:F

.field private mHotSpotY:F

.field private final mStyle:I

.field private mSystemIconResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/PointerIcon;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    sput-object v0, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    new-instance v0, Landroid/view/PointerIcon$1;

    invoke-direct {v0}, Landroid/view/PointerIcon$1;-><init>()V

    sput-object v0, Landroid/view/PointerIcon;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/view/PointerIcon;->mStyle:I

    return-void
.end method

.method synthetic constructor <init>(ILandroid/view/PointerIcon$1;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/view/PointerIcon$1;

    invoke-direct {p0, p1}, Landroid/view/PointerIcon;-><init>(I)V

    return-void
.end method

.method static synthetic access$102(Landroid/view/PointerIcon;I)I
    .locals 0
    .param p0    # Landroid/view/PointerIcon;
    .param p1    # I

    iput p1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    return p1
.end method

.method public static createCustomIcon(Landroid/graphics/Bitmap;FF)Landroid/view/PointerIcon;
    .locals 3
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # F
    .param p2    # F

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "bitmap must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {p0, p1, p2}, Landroid/view/PointerIcon;->validateHotSpot(Landroid/graphics/Bitmap;FF)V

    new-instance v0, Landroid/view/PointerIcon;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    iput-object p0, v0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    iput p1, v0, Landroid/view/PointerIcon;->mHotSpotX:F

    iput p2, v0, Landroid/view/PointerIcon;->mHotSpotY:F

    return-object v0
.end method

.method public static getDefaultIcon(Landroid/content/Context;)Landroid/view/PointerIcon;
    .locals 1
    .param p0    # Landroid/content/Context;

    const/16 v0, 0x3e8

    invoke-static {p0, v0}, Landroid/view/PointerIcon;->getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;

    move-result-object v0

    return-object v0
.end method

.method public static getNullIcon()Landroid/view/PointerIcon;
    .locals 1

    sget-object v0, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    return-object v0
.end method

.method public static getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v9, -0x1

    const/16 v8, 0x3e8

    if-nez p0, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "context must not be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    if-nez p1, :cond_1

    sget-object v4, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    :goto_0
    return-object v4

    :cond_1
    invoke-static {p1}, Landroid/view/PointerIcon;->getSystemIconStyleIndex(I)I

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v8}, Landroid/view/PointerIcon;->getSystemIconStyleIndex(I)I

    move-result v3

    :cond_2
    const/4 v4, 0x0

    sget-object v5, Lcom/android/internal/R$styleable;->Pointer:[I

    const v6, 0x10103f7    # android.R.attr.pointerStyle

    const/4 v7, 0x0

    invoke-virtual {p0, v4, v5, v6, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    if-ne v2, v9, :cond_4

    const-string v4, "PointerIcon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Missing theme resources for pointer icon style "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v8, :cond_3

    sget-object v4, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    goto :goto_0

    :cond_3
    invoke-static {p0, v8}, Landroid/view/PointerIcon;->getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;

    move-result-object v4

    goto :goto_0

    :cond_4
    new-instance v1, Landroid/view/PointerIcon;

    invoke-direct {v1, p1}, Landroid/view/PointerIcon;-><init>(I)V

    const/high16 v4, -0x1000000

    and-int/2addr v4, v2

    const/high16 v5, 0x1000000

    if-ne v4, v5, :cond_5

    iput v2, v1, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    :goto_1
    move-object v4, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v4, v2}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    goto :goto_1
.end method

.method private static getSystemIconStyleIndex(I)I
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const/16 v0, 0xa

    goto :goto_0

    :sswitch_5
    const/16 v0, 0xb

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x9

    goto :goto_0

    :sswitch_7
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_8
    const/4 v0, 0x5

    goto :goto_0

    :sswitch_9
    const/4 v0, 0x6

    goto :goto_0

    :sswitch_a
    const/4 v0, 0x7

    goto :goto_0

    :sswitch_b
    const/16 v0, 0x8

    goto :goto_0

    :sswitch_c
    const/16 v0, 0xc

    goto :goto_0

    :sswitch_d
    const/16 v0, 0xd

    goto :goto_0

    :sswitch_e
    const/16 v0, 0xe

    goto :goto_0

    :sswitch_f
    const/16 v0, 0xf

    goto :goto_0

    :sswitch_10
    const/16 v0, 0x10

    goto :goto_0

    :sswitch_11
    const/16 v0, 0x11

    goto :goto_0

    :sswitch_12
    const/16 v0, 0x12

    goto :goto_0

    :sswitch_13
    const/16 v0, 0x13

    goto :goto_0

    :sswitch_14
    const/16 v0, 0x14

    goto :goto_0

    :sswitch_15
    const/16 v0, 0x15

    goto :goto_0

    :sswitch_16
    const/16 v0, 0x16

    goto :goto_0

    :sswitch_17
    const/16 v0, 0x17

    goto :goto_0

    :sswitch_18
    const/16 v0, 0x18

    goto :goto_0

    :sswitch_19
    const/16 v0, 0x19

    goto :goto_0

    :sswitch_1a
    const/16 v0, 0x1a

    goto :goto_0

    :sswitch_1b
    const/16 v0, 0x1b

    goto :goto_0

    :sswitch_1c
    const/16 v0, 0x1c

    goto :goto_0

    :sswitch_1d
    const/16 v0, 0x1d

    goto :goto_0

    :sswitch_1e
    const/16 v0, 0x1e

    goto :goto_0

    :sswitch_1f
    const/16 v0, 0x1f

    goto :goto_0

    :sswitch_20
    const/16 v0, 0x20

    goto :goto_0

    :sswitch_21
    const/16 v0, 0x21

    goto :goto_0

    :sswitch_22
    const/16 v0, 0x22

    goto :goto_0

    :sswitch_23
    const/16 v0, 0x23

    goto :goto_0

    :sswitch_24
    const/16 v0, 0x24

    goto :goto_0

    :sswitch_25
    const/16 v0, 0x25

    goto :goto_0

    :sswitch_26
    const/16 v0, 0x26

    goto :goto_0

    :sswitch_27
    const/16 v0, 0x27

    goto :goto_0

    :sswitch_28
    const/16 v0, 0x28

    goto :goto_0

    :sswitch_29
    const/16 v0, 0x29

    goto :goto_0

    :sswitch_2a
    const/16 v0, 0x2a

    goto :goto_0

    :sswitch_2b
    const/16 v0, 0x2b

    goto :goto_0

    :sswitch_2c
    const/16 v0, 0x2c

    goto :goto_0

    :sswitch_2d
    const/16 v0, 0x2d

    goto :goto_0

    :sswitch_2e
    const/16 v0, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const/16 v0, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const/16 v0, 0x30

    goto/16 :goto_0

    :sswitch_31
    const/16 v0, 0x31

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x7d0 -> :sswitch_1
        0x7d1 -> :sswitch_2
        0x7d2 -> :sswitch_3
        0x7d3 -> :sswitch_4
        0x7d4 -> :sswitch_5
        0x7d5 -> :sswitch_6
        0x7d6 -> :sswitch_7
        0x7d7 -> :sswitch_8
        0x7d8 -> :sswitch_9
        0x7d9 -> :sswitch_a
        0x7da -> :sswitch_b
        0x7db -> :sswitch_c
        0x7dc -> :sswitch_d
        0x7dd -> :sswitch_e
        0x7de -> :sswitch_f
        0x7df -> :sswitch_10
        0x7e0 -> :sswitch_11
        0x7e1 -> :sswitch_12
        0x7e2 -> :sswitch_13
        0x7e3 -> :sswitch_14
        0x7e4 -> :sswitch_15
        0x7e5 -> :sswitch_16
        0x7e6 -> :sswitch_17
        0x7e7 -> :sswitch_18
        0x7e8 -> :sswitch_19
        0x7e9 -> :sswitch_1a
        0x7ea -> :sswitch_1b
        0x7eb -> :sswitch_1c
        0x7ec -> :sswitch_1d
        0x7ed -> :sswitch_1e
        0x7ee -> :sswitch_1f
        0x7ef -> :sswitch_20
        0x7f0 -> :sswitch_21
        0x7f1 -> :sswitch_22
        0x7f2 -> :sswitch_23
        0x7f3 -> :sswitch_24
        0x7f4 -> :sswitch_25
        0x7f5 -> :sswitch_26
        0x7f6 -> :sswitch_27
        0x7f7 -> :sswitch_28
        0x7f8 -> :sswitch_29
        0x7f9 -> :sswitch_2a
        0x7fa -> :sswitch_2b
        0x7fb -> :sswitch_2c
        0x7fc -> :sswitch_2d
        0x7fd -> :sswitch_2e
        0x7fe -> :sswitch_2f
        0x7ff -> :sswitch_30
        0x800 -> :sswitch_31
    .end sparse-switch
.end method

.method public static loadCustomIcon(Landroid/content/res/Resources;I)Landroid/view/PointerIcon;
    .locals 3
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "resources must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Landroid/view/PointerIcon;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    invoke-direct {v0, p0, p1}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    return-object v0
.end method

.method private loadResource(Landroid/content/res/Resources;I)V
    .locals 9
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v6

    :try_start_0
    const-string/jumbo v7, "pointer-icon"

    invoke-static {v6, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    sget-object v7, Lcom/android/internal/R$styleable;->PointerIcon:[I

    invoke-virtual {p1, v6, v7}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    if-nez v1, :cond_0

    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "<pointer-icon> is missing bitmap attribute."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    :catch_0
    move-exception v3

    :try_start_1
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Exception parsing pointer icon resource."

    invoke-direct {v7, v8, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v7

    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    throw v7

    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    instance-of v7, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v7, :cond_1

    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "<pointer-icon> bitmap attribute must refer to a bitmap drawable."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    iput v4, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    iput v5, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    return-void
.end method

.method private throwIfIconIsNotLoaded()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/PointerIcon;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The icon is not loaded."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static validateHotSpot(Landroid/graphics/Bitmap;FF)V
    .locals 2
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x0

    cmpg-float v0, p1, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "x hotspot lies outside of the bitmap area"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    cmpg-float v0, p2, v1

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "y hotspot lies outside of the bitmap area"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Landroid/view/PointerIcon;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Landroid/view/PointerIcon;

    iget v3, p0, Landroid/view/PointerIcon;->mStyle:I

    iget v4, v0, Landroid/view/PointerIcon;->mStyle:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    iget v4, v0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    if-eq v3, v4, :cond_5

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    iget v3, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    if-nez v3, :cond_0

    iget-object v3, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v4, v0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    if-ne v3, v4, :cond_6

    iget v3, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    iget v4, v0, Landroid/view/PointerIcon;->mHotSpotX:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_6

    iget v3, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    iget v4, v0, Landroid/view/PointerIcon;->mHotSpotY:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_6
    move v1, v2

    goto :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHotSpotX()F
    .locals 1

    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    return v0
.end method

.method public getHotSpotY()F
    .locals 1

    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    return v0
.end method

.method public getStyle()I
    .locals 1

    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    return v0
.end method

.method public isLoaded()Z
    .locals 1

    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNullIcon()Z
    .locals 1

    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Landroid/content/Context;)Landroid/view/PointerIcon;
    .locals 3
    .param p1    # Landroid/content/Context;

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "context must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget v1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    :cond_1
    move-object v0, p0

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Landroid/view/PointerIcon;

    iget v1, p0, Landroid/view/PointerIcon;->mStyle:I

    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    iget v1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    iput v1, v0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    invoke-direct {v0, v1, v2}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    :cond_0
    return-void
.end method
