.class public Landroid/webkit/L10nUtils;
.super Ljava/lang/Object;
.source "L10nUtils.java"


# static fields
.field private static mApplicationContext:Landroid/content/Context;

.field private static mIdsArray:[I

.field private static mStrings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x39

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/webkit/L10nUtils;->mIdsArray:[I

    return-void

    :array_0
    .array-data 4
        0x10403d0    # android.R.string.autofill_address_name_separator
        0x10403d1    # android.R.string.autofill_address_summary_name_format
        0x10403d2    # android.R.string.autofill_address_summary_separator
        0x10403d3    # android.R.string.autofill_address_summary_format
        0x10403d4    # android.R.string.autofill_attention_ignored_re
        0x10403d5    # android.R.string.autofill_region_ignored_re
        0x10403d6    # android.R.string.autofill_company_re
        0x10403d7    # android.R.string.autofill_address_line_1_re
        0x10403d8    # android.R.string.autofill_address_line_1_label_re
        0x10403d9    # android.R.string.autofill_address_line_2_re
        0x10403da    # android.R.string.autofill_address_line_3_re
        0x10403db    # android.R.string.autofill_country_re
        0x10403dc    # android.R.string.autofill_zip_code_re
        0x10403dd    # android.R.string.autofill_zip_4_re
        0x10403de    # android.R.string.autofill_city_re
        0x10403df    # android.R.string.autofill_state_re
        0x10403e0    # android.R.string.autofill_address_type_same_as_re
        0x10403e1    # android.R.string.autofill_address_type_use_my_re
        0x10403e2    # android.R.string.autofill_billing_designator_re
        0x10403e3    # android.R.string.autofill_shipping_designator_re
        0x10403e4    # android.R.string.autofill_email_re
        0x10403e5    # android.R.string.autofill_username_re
        0x10403e6    # android.R.string.autofill_name_re
        0x10403e7    # android.R.string.autofill_name_specific_re
        0x10403e8    # android.R.string.autofill_first_name_re
        0x10403e9    # android.R.string.autofill_middle_initial_re
        0x10403ea    # android.R.string.autofill_middle_name_re
        0x10403eb    # android.R.string.autofill_last_name_re
        0x10403ec    # android.R.string.autofill_phone_re
        0x10403ed    # android.R.string.autofill_area_code_re
        0x10403ee    # android.R.string.autofill_phone_prefix_re
        0x10403ef    # android.R.string.autofill_phone_suffix_re
        0x10403f0    # android.R.string.autofill_phone_extension_re
        0x10403f1    # android.R.string.autofill_name_on_card_re
        0x10403f2    # android.R.string.autofill_name_on_card_contextual_re
        0x10403f3    # android.R.string.autofill_card_cvc_re
        0x10403f4    # android.R.string.autofill_card_number_re
        0x10403f5    # android.R.string.autofill_expiration_month_re
        0x10403f6    # android.R.string.autofill_expiration_date_re
        0x10403f7    # android.R.string.autofill_card_ignored_re
        0x10403f8    # android.R.string.autofill_fax_re
        0x10403f9    # android.R.string.autofill_country_code_re
        0x10403fa    # android.R.string.autofill_area_code_notext_re
        0x10403fb    # android.R.string.autofill_phone_prefix_separator_re
        0x10403fc    # android.R.string.autofill_phone_suffix_separator_re
        0x10403fd    # android.R.string.autofill_province
        0x10403fe    # android.R.string.autofill_postal_code
        0x10403ff    # android.R.string.autofill_state
        0x1040400    # android.R.string.autofill_zip_code
        0x1040401    # android.R.string.autofill_county
        0x1040402    # android.R.string.autofill_island
        0x1040403    # android.R.string.autofill_district
        0x1040404    # android.R.string.autofill_department
        0x1040405    # android.R.string.autofill_prefecture
        0x1040406    # android.R.string.autofill_parish
        0x1040407    # android.R.string.autofill_area
        0x1040408    # android.R.string.autofill_emirate
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocalisedString(I)Ljava/lang/String;
    .locals 4
    .param p0    # I

    sget-object v2, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    if-nez v2, :cond_0

    invoke-static {p0}, Landroid/webkit/L10nUtils;->loadString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    invoke-static {p0}, Landroid/webkit/L10nUtils;->loadString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0
.end method

.method private static loadString(I)Ljava/lang/String;
    .locals 4
    .param p0    # I

    sget-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Landroid/webkit/L10nUtils;->mIdsArray:[I

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    :cond_0
    sget-object v1, Landroid/webkit/L10nUtils;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Landroid/webkit/L10nUtils;->mIdsArray:[I

    aget v2, v2, p0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public static setApplicationContext(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Landroid/webkit/L10nUtils;->mApplicationContext:Landroid/content/Context;

    return-void
.end method
