.class Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;
.super Landroid/widget/MediaController;
.source "HTML5VideoFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/HTML5VideoFullScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FullScreenMediaController"
.end annotation


# instance fields
.field fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

.field mVideoView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-direct {p0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/webkit/HTML5VideoFullScreen;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/webkit/HTML5VideoFullScreen;

    invoke-direct {p0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    iput-object p3, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    if-eqz v1, :cond_0

    # getter for: Landroid/webkit/HTML5VideoFullScreen;->mUserSeek:Z
    invoke-static {}, Landroid/webkit/HTML5VideoFullScreen;->access$1400()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->startSeek()V

    # setter for: Landroid/webkit/HTML5VideoFullScreen;->mUserSeek:Z
    invoke-static {v3}, Landroid/webkit/HTML5VideoFullScreen;->access$1402(Z)Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    invoke-virtual {v1}, Landroid/webkit/HTML5VideoFullScreen;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    invoke-virtual {v1}, Landroid/webkit/HTML5VideoFullScreen;->pause()V

    const v1, 0x7fffffff

    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->show(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    invoke-virtual {v1}, Landroid/webkit/HTML5VideoFullScreen;->start()V

    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->show(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    if-eqz v1, :cond_0

    # setter for: Landroid/webkit/HTML5VideoFullScreen;->mUserSeek:Z
    invoke-static {v0}, Landroid/webkit/HTML5VideoFullScreen;->access$1402(Z)Z

    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->moveSeekBarPos(I)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->fullScreenCtr:Landroid/webkit/HTML5VideoFullScreen;

    if-eqz v1, :cond_0

    # setter for: Landroid/webkit/HTML5VideoFullScreen;->mUserSeek:Z
    invoke-static {v0}, Landroid/webkit/HTML5VideoFullScreen;->access$1402(Z)Z

    invoke-virtual {p0, v3}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->moveSeekBarPos(I)Z

    goto :goto_0

    :cond_5
    invoke-super {p0, p1}, Landroid/widget/MediaController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public hide()V
    .locals 2

    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    invoke-super {p0}, Landroid/widget/MediaController;->hide()V

    return-void
.end method

.method public show()V
    .locals 2

    invoke-super {p0}, Landroid/widget/MediaController;->show()V

    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    return-void
.end method
