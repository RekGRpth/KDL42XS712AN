.class public Landroid/app/StandbyHintDialog;
.super Landroid/app/AlertDialog;
.source "StandbyHintDialog.java"


# static fields
.field static CALLBACK_DELAY:I = 0x0

.field static DISMISS_DELAY:I = 0x0

.field static final TAG:Ljava/lang/String; = "WindowManager"


# instance fields
.field mBeforDismiss:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field mCallbackRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field mContainer:Landroid/view/View;

.field mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field mHandlerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    sget v0, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    add-int/lit8 v0, v0, 0x0

    sput v0, Landroid/app/StandbyHintDialog;->CALLBACK_DELAY:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    const/4 v2, 0x0

    const v0, 0x103006d    # android.R.style.Theme_Holo_NoActionBar_Fullscreen

    invoke-direct {p0, p1, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Landroid/app/StandbyHintDialog;->mContextRef:Ljava/lang/ref/WeakReference;

    iput-object v2, p0, Landroid/app/StandbyHintDialog;->mCallbackRef:Ljava/lang/ref/WeakReference;

    iput-object v2, p0, Landroid/app/StandbyHintDialog;->mBeforDismiss:Ljava/lang/ref/WeakReference;

    iput-object v2, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    iput-object v2, p0, Landroid/app/StandbyHintDialog;->mContainer:Landroid/view/View;

    const-string v0, "WindowManager"

    const-string/jumbo v1, "new a StandbyHintDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/app/StandbyHintDialog;->mContextRef:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Landroid/app/StandbyHintDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x10900d7    # android.R.layout.standby_hint_dialog

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/app/StandbyHintDialog;->mContainer:Landroid/view/View;

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mContainer:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/app/StandbyHintDialog;->setView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/app/StandbyHintDialog;->isGlobal()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput v0, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    :goto_0
    sget v0, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    add-int/lit8 v0, v0, 0x32

    sput v0, Landroid/app/StandbyHintDialog;->CALLBACK_DELAY:I

    const-string v0, "WindowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CALLBACK_DELAY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/app/StandbyHintDialog;->CALLBACK_DELAY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/16 v0, 0x7d0

    sput v0, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    goto :goto_0
.end method

.method public static create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/StandbyHintDialog;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/os/Handler;

    new-instance v0, Landroid/app/StandbyHintDialog;

    invoke-direct {v0, p0, p1}, Landroid/app/StandbyHintDialog;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public isGlobal()Z
    .locals 9

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mContextRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/standbymode"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Global"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v8, v0, :cond_1

    move v7, v8

    :goto_0
    const-string v0, "WindowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StandbyDialog isGlobal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v7

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public setBeforDismissCallback(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/app/StandbyHintDialog;->mBeforDismiss:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public setHintEndCallback(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/app/StandbyHintDialog;->mCallbackRef:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public show()V
    .locals 4

    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    const-string v0, "WindowManager"

    const-string/jumbo v1, "standbyHintDialog is shown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, Landroid/app/StandbyHintDialog;->mBeforDismiss:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    sget v2, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    add-int/lit16 v2, v2, -0x12c

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Landroid/app/StandbyHintDialog$1;

    invoke-direct {v1, p0}, Landroid/app/StandbyHintDialog$1;-><init>(Landroid/app/StandbyHintDialog;)V

    sget v2, Landroid/app/StandbyHintDialog;->DISMISS_DELAY:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mCallbackRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mCallbackRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/StandbyHintDialog;->mHandlerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, Landroid/app/StandbyHintDialog;->mCallbackRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    sget v2, Landroid/app/StandbyHintDialog;->CALLBACK_DELAY:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
