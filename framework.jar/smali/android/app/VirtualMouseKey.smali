.class public Landroid/app/VirtualMouseKey;
.super Ljava/lang/Object;
.source "VirtualMouseKey.java"


# static fields
.field private static final AVERAGE_TIME:I = 0x2

.field private static final DEBUG:Z = false

.field private static final DEFAULT_LEGTH:I = 0xa

.field private static final DEFAULT_TIME_INTERVAL:I = 0x5dc

.field private static final DEFAULT_TIME_MILLISECONDS:I = 0x3e8

.field private static final SCALE:I = 0xf

.field private static final TAG:Ljava/lang/String; = "VirtualMouseKey"


# instance fields
.field private mLastdownTime1:J

.field private mLastdownTime2:J

.field private mStepLength:I

.field private mToolLibControlAble:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0xa

    iput v1, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    iput-wide v2, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    iput-wide v2, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    :try_start_0
    const-string/jumbo v1, "mouse_control_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    goto :goto_0
.end method

.method private closeInputDevice()V
    .locals 1

    iget-boolean v0, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/app/VirtualMouseKey;->nativeCloseInput()V

    :cond_0
    return-void
.end method

.method public static isHandleVirtualMouseKey(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p0    # Landroid/view/KeyEvent;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x16

    if-le v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mouseLeftClick()V
    .locals 1

    iget-boolean v0, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/app/VirtualMouseKey;->nativeMouseLeftClick()V

    :cond_0
    return-void
.end method

.method private moveCursor(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-boolean v0, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/app/VirtualMouseKey;->nativeMoveCursor(II)V

    :cond_0
    return-void
.end method

.method private native nativeCloseInput()V
.end method

.method private native nativeMouseLeftClick()V
.end method

.method private native nativeMoveCursor(II)V
.end method


# virtual methods
.method public final handleVirtualMouseKeyEx(IIJ)Z
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # J

    const/4 v4, 0x0

    iget-boolean v5, p0, Landroid/app/VirtualMouseKey;->mToolLibControlAble:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    const/16 v5, 0x13

    if-lt p2, v5, :cond_2

    const/16 v5, 0x16

    if-le p2, v5, :cond_3

    :cond_2
    const/16 v5, 0x42

    if-ne p2, v5, :cond_0

    :cond_3
    if-nez p1, :cond_0

    move-wide v0, p3

    iget-wide v5, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    sub-long v5, v0, v5

    iget-wide v7, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    iget-wide v9, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    sub-long/2addr v7, v9

    add-long/2addr v5, v7

    const-wide/16 v7, 0x2

    div-long v2, v5, v7

    const-wide/16 v5, 0x5dc

    cmp-long v5, v2, v5

    if-lez v5, :cond_5

    const/16 v5, 0xa

    iput v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    :cond_4
    :goto_1
    sparse-switch p2, :sswitch_data_0

    :goto_2
    const/4 v4, 0x1

    goto :goto_0

    :cond_5
    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_4

    const/high16 v5, 0x3f800000    # 1.0f

    long-to-float v6, v2

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    div-float/2addr v5, v6

    const/high16 v6, 0x41700000    # 15.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    goto :goto_1

    :sswitch_0
    iget v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    rsub-int/lit8 v5, v5, 0x0

    invoke-direct {p0, v4, v5}, Landroid/app/VirtualMouseKey;->moveCursor(II)V

    iget-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    iput-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    iput-wide v0, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    goto :goto_2

    :sswitch_1
    iget v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    invoke-direct {p0, v4, v5}, Landroid/app/VirtualMouseKey;->moveCursor(II)V

    iget-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    iput-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    iput-wide v0, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    goto :goto_2

    :sswitch_2
    iget v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    rsub-int/lit8 v5, v5, 0x0

    invoke-direct {p0, v5, v4}, Landroid/app/VirtualMouseKey;->moveCursor(II)V

    iget-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    iput-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    iput-wide v0, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    goto :goto_2

    :sswitch_3
    iget v5, p0, Landroid/app/VirtualMouseKey;->mStepLength:I

    invoke-direct {p0, v5, v4}, Landroid/app/VirtualMouseKey;->moveCursor(II)V

    iget-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    iput-wide v4, p0, Landroid/app/VirtualMouseKey;->mLastdownTime1:J

    iput-wide v0, p0, Landroid/app/VirtualMouseKey;->mLastdownTime2:J

    goto :goto_2

    :sswitch_4
    invoke-direct {p0}, Landroid/app/VirtualMouseKey;->mouseLeftClick()V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method
