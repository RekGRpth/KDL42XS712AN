.class public Lorg/apache/tools/mail/MailMessage;
.super Ljava/lang/Object;
.source "MailMessage.java"


# static fields
.field public static final DEFAULT_HOST:Ljava/lang/String; = "localhost"

.field public static final DEFAULT_PORT:I = 0x19

.field private static final OK_DATA:I = 0x162

.field private static final OK_DOT:I = 0xfa

.field private static final OK_FROM:I = 0xfa

.field private static final OK_HELO:I = 0xfa

.field private static final OK_QUIT:I = 0xdd

.field private static final OK_RCPT_1:I = 0xfa

.field private static final OK_RCPT_2:I = 0xfb

.field private static final OK_READY:I = 0xdc


# instance fields
.field private cc:Ljava/util/Vector;

.field private from:Ljava/lang/String;

.field private headersKeys:Ljava/util/Vector;

.field private headersValues:Ljava/util/Vector;

.field private host:Ljava/lang/String;

.field private in:Lorg/apache/tools/mail/SmtpResponseReader;

.field private out:Lorg/apache/tools/mail/MailPrintStream;

.field private port:I

.field private replyto:Ljava/util/Vector;

.field private socket:Ljava/net/Socket;

.field private to:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "localhost"

    const/16 v1, 0x19

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x19

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/mail/MailMessage;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x19

    iput v0, p0, Lorg/apache/tools/mail/MailMessage;->port:I

    iput p2, p0, Lorg/apache/tools/mail/MailMessage;->port:I

    iput-object p1, p0, Lorg/apache/tools/mail/MailMessage;->host:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->replyto:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->to:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->cc:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->headersKeys:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->headersValues:Ljava/util/Vector;

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->connect()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->sendHelo()V

    return-void
.end method

.method static sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v6, 0x28

    if-ne v0, v6, :cond_1

    add-int/lit8 v4, v4, 0x1

    if-nez v5, :cond_0

    move v1, v2

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/16 v6, 0x29

    if-ne v0, v6, :cond_2

    add-int/lit8 v4, v4, -0x1

    if-nez v1, :cond_0

    add-int/lit8 v5, v2, 0x1

    goto :goto_1

    :cond_2
    if-nez v4, :cond_3

    const/16 v6, 0x3c

    if-ne v0, v6, :cond_3

    add-int/lit8 v5, v2, 0x1

    goto :goto_1

    :cond_3
    if-nez v4, :cond_0

    const/16 v6, 0x3e

    if-ne v0, v6, :cond_0

    move v1, v2

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    move v1, v3

    :cond_5
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method


# virtual methods
.method public bcc(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/mail/MailMessage;->sendRcpt(Ljava/lang/String;)V

    return-void
.end method

.method public cc(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/mail/MailMessage;->sendRcpt(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->cc:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method connect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->host:Ljava/lang/String;

    iget v2, p0, Lorg/apache/tools/mail/MailMessage;->port:I

    invoke-direct {v0, v1, v2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->socket:Ljava/net/Socket;

    new-instance v0, Lorg/apache/tools/mail/MailPrintStream;

    new-instance v1, Ljava/io/BufferedOutputStream;

    iget-object v2, p0, Lorg/apache/tools/mail/MailMessage;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Lorg/apache/tools/mail/MailPrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    new-instance v0, Lorg/apache/tools/mail/SmtpResponseReader;

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/mail/SmtpResponseReader;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/apache/tools/mail/MailMessage;->in:Lorg/apache/tools/mail/SmtpResponseReader;

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->getReady()V

    return-void
.end method

.method disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    invoke-virtual {v0}, Lorg/apache/tools/mail/MailPrintStream;->close()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->in:Lorg/apache/tools/mail/SmtpResponseReader;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->in:Lorg/apache/tools/mail/SmtpResponseReader;

    invoke-virtual {v0}, Lorg/apache/tools/mail/SmtpResponseReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->socket:Ljava/net/Socket;

    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method flushHeaders()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->headersKeys:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->headersKeys:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->headersValues:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/mail/MailPrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    invoke-virtual {v3}, Lorg/apache/tools/mail/MailPrintStream;->println()V

    iget-object v3, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    invoke-virtual {v3}, Lorg/apache/tools/mail/MailPrintStream;->flush()V

    return-void
.end method

.method public from(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/mail/MailMessage;->sendFrom(Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/tools/mail/MailMessage;->from:Ljava/lang/String;

    return-void
.end method

.method public getPrintStream()Ljava/io/PrintStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->setFromHeader()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->setReplyToHeader()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->setToHeader()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->setCcHeader()V

    const-string v0, "X-Mailer"

    const-string v1, "org.apache.tools.mail.MailMessage (ant.apache.org)"

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->sendData()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->flushHeaders()V

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    return-object v0
.end method

.method getReady()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/mail/MailMessage;->in:Lorg/apache/tools/mail/SmtpResponseReader;

    invoke-virtual {v2}, Lorg/apache/tools/mail/SmtpResponseReader;->getResponse()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v0, v2, [I

    const/4 v2, 0x0

    const/16 v3, 0xdc

    aput v3, v0, v2

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/mail/MailMessage;->isResponseOK(Ljava/lang/String;[I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Didn\'t get introduction from server: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-void
.end method

.method isResponseOK(Ljava/lang/String;[I)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    aget v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public replyto(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->replyto:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method send(Ljava/lang/String;[I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->out:Lorg/apache/tools/mail/MailPrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/mail/MailPrintStream;->rawPrint(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->in:Lorg/apache/tools/mail/SmtpResponseReader;

    invoke-virtual {v1}, Lorg/apache/tools/mail/SmtpResponseReader;->getResponse()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/tools/mail/MailMessage;->isResponseOK(Ljava/lang/String;[I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Unexpected reply to command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method public sendAndClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->sendDot()V

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->sendQuit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->disconnect()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lorg/apache/tools/mail/MailMessage;->disconnect()V

    throw v0
.end method

.method sendData()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0x162

    aput v2, v0, v1

    const-string v1, "DATA"

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V

    return-void
.end method

.method sendDot()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0xfa

    aput v2, v0, v1

    const-string v1, "\r\n."

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V

    return-void
.end method

.method sendFrom(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0xfa

    aput v2, v0, v1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "MAIL FROM: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Lorg/apache/tools/mail/MailMessage;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V

    return-void
.end method

.method sendHelo()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v1, v2, [I

    const/4 v2, 0x0

    const/16 v3, 0xfa

    aput v3, v1, v2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "HELO "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V

    return-void
.end method

.method sendQuit()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x1

    new-array v1, v2, [I

    const/4 v2, 0x0

    const/16 v3, 0xdd

    aput v3, v1, v2

    :try_start_0
    const-string v2, "QUIT"

    invoke-virtual {p0, v2, v1}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/mail/ErrorInQuitException;

    invoke-direct {v2, v0}, Lorg/apache/tools/mail/ErrorInQuitException;-><init>(Ljava/io/IOException;)V

    throw v2
.end method

.method sendRcpt(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x2

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "RCPT TO: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Lorg/apache/tools/mail/MailMessage;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/mail/MailMessage;->send(Ljava/lang/String;[I)V

    return-void

    nop

    :array_0
    .array-data 4
        0xfa
        0xfb
    .end array-data
.end method

.method setCcHeader()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->cc:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Cc"

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->cc:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Lorg/apache/tools/mail/MailMessage;->vectorToList(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method setFromHeader()V
    .locals 2

    const-string v0, "From"

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->from:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->headersKeys:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->headersValues:Ljava/util/Vector;

    invoke-virtual {v0, p2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/mail/MailMessage;->port:I

    return-void
.end method

.method setReplyToHeader()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->replyto:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Reply-To"

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->replyto:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Lorg/apache/tools/mail/MailMessage;->vectorToList(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "Subject"

    invoke-virtual {p0, v0, p1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method setToHeader()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->to:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "To"

    iget-object v1, p0, Lorg/apache/tools/mail/MailMessage;->to:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Lorg/apache/tools/mail/MailMessage;->vectorToList(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public to(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/mail/MailMessage;->sendRcpt(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/mail/MailMessage;->to:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method vectorToList(Ljava/util/Vector;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/util/Vector;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
