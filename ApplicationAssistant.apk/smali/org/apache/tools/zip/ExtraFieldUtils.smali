.class public Lorg/apache/tools/zip/ExtraFieldUtils;
.super Ljava/lang/Object;
.source "ExtraFieldUtils.java"


# static fields
.field static class$org$apache$tools$zip$AsiExtraField:Ljava/lang/Class;

.field static class$org$apache$tools$zip$JarMarker:Ljava/lang/Class;

.field private static implementations:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->implementations:Ljava/util/Hashtable;

    sget-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$AsiExtraField:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.zip.AsiExtraField"

    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$AsiExtraField:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->register(Ljava/lang/Class;)V

    sget-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$JarMarker:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "org.apache.tools.zip.JarMarker"

    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$JarMarker:Ljava/lang/Class;

    :goto_1
    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->register(Ljava/lang/Class;)V

    return-void

    :cond_0
    sget-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$AsiExtraField:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/tools/zip/ExtraFieldUtils;->class$org$apache$tools$zip$JarMarker:Ljava/lang/Class;

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static createExtraField(Lorg/apache/tools/zip/ZipShort;)Lorg/apache/tools/zip/ZipExtraField;
    .locals 3
    .param p0    # Lorg/apache/tools/zip/ZipShort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    sget-object v2, Lorg/apache/tools/zip/ExtraFieldUtils;->implementations:Ljava/util/Hashtable;

    invoke-virtual {v2, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/zip/ZipExtraField;

    :goto_0
    return-object v2

    :cond_0
    new-instance v1, Lorg/apache/tools/zip/UnrecognizedExtraField;

    invoke-direct {v1}, Lorg/apache/tools/zip/UnrecognizedExtraField;-><init>()V

    invoke-virtual {v1, p0}, Lorg/apache/tools/zip/UnrecognizedExtraField;->setHeaderId(Lorg/apache/tools/zip/ZipShort;)V

    move-object v2, v1

    goto :goto_0
.end method

.method public static mergeCentralDirectoryData([Lorg/apache/tools/zip/ZipExtraField;)[B
    .locals 9
    .param p0    # [Lorg/apache/tools/zip/ZipExtraField;

    const/4 v8, 0x2

    const/4 v7, 0x0

    array-length v5, p0

    mul-int/lit8 v4, v5, 0x4

    const/4 v0, 0x0

    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_0

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getCentralDirectoryLength()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getValue()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-array v2, v4, [B

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_1
    array-length v5, p0

    if-ge v0, v5, :cond_1

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getBytes()[B

    move-result-object v5

    invoke-static {v5, v7, v2, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getCentralDirectoryLength()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getBytes()[B

    move-result-object v5

    add-int/lit8 v6, v3, 0x2

    invoke-static {v5, v7, v2, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getCentralDirectoryData()[B

    move-result-object v1

    add-int/lit8 v5, v3, 0x4

    array-length v6, v1

    invoke-static {v1, v7, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v5, v1

    add-int/lit8 v5, v5, 0x4

    add-int/2addr v3, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public static mergeLocalFileDataData([Lorg/apache/tools/zip/ZipExtraField;)[B
    .locals 9
    .param p0    # [Lorg/apache/tools/zip/ZipExtraField;

    const/4 v8, 0x2

    const/4 v7, 0x0

    array-length v5, p0

    mul-int/lit8 v4, v5, 0x4

    const/4 v0, 0x0

    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_0

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getLocalFileDataLength()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getValue()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-array v2, v4, [B

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_1
    array-length v5, p0

    if-ge v0, v5, :cond_1

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getBytes()[B

    move-result-object v5

    invoke-static {v5, v7, v2, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getLocalFileDataLength()Lorg/apache/tools/zip/ZipShort;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipShort;->getBytes()[B

    move-result-object v5

    add-int/lit8 v6, v3, 0x2

    invoke-static {v5, v7, v2, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v5, p0, v0

    invoke-interface {v5}, Lorg/apache/tools/zip/ZipExtraField;->getLocalFileDataData()[B

    move-result-object v1

    add-int/lit8 v5, v3, 0x4

    array-length v6, v1

    invoke-static {v1, v7, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v5, v1

    add-int/lit8 v5, v5, 0x4

    add-int/2addr v3, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public static parse([B)[Lorg/apache/tools/zip/ZipExtraField;
    .locals 11
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;
        }
    .end annotation

    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    const/4 v5, 0x0

    :goto_0
    array-length v8, p0

    add-int/lit8 v8, v8, -0x4

    if-gt v5, v8, :cond_1

    new-instance v0, Lorg/apache/tools/zip/ZipShort;

    invoke-direct {v0, p0, v5}, Lorg/apache/tools/zip/ZipShort;-><init>([BI)V

    new-instance v8, Lorg/apache/tools/zip/ZipShort;

    add-int/lit8 v9, v5, 0x2

    invoke-direct {v8, p0, v9}, Lorg/apache/tools/zip/ZipShort;-><init>([BI)V

    invoke-virtual {v8}, Lorg/apache/tools/zip/ZipShort;->getValue()I

    move-result v3

    add-int/lit8 v8, v5, 0x4

    add-int/2addr v8, v3

    array-length v9, p0

    if-le v8, v9, :cond_0

    new-instance v8, Ljava/util/zip/ZipException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "data starting at "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " is in unknown format"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    :try_start_0
    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->createExtraField(Lorg/apache/tools/zip/ZipShort;)Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v7

    add-int/lit8 v8, v5, 0x4

    invoke-interface {v7, p0, v8, v3}, Lorg/apache/tools/zip/ZipExtraField;->parseFromLocalFileData([BII)V

    invoke-virtual {v6, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v8, v3, 0x4

    add-int/2addr v5, v8

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v8, Ljava/util/zip/ZipException;

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v8

    :catch_1
    move-exception v1

    new-instance v8, Ljava/util/zip/ZipException;

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    array-length v8, p0

    if-eq v5, v8, :cond_2

    new-instance v8, Ljava/util/zip/ZipException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "data starting at "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " is in unknown format"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_2
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v8

    new-array v4, v8, [Lorg/apache/tools/zip/ZipExtraField;

    invoke-virtual {v6, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    return-object v4
.end method

.method public static register(Ljava/lang/Class;)V
    .locals 6
    .param p0    # Ljava/lang/Class;

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/zip/ZipExtraField;

    sget-object v3, Lorg/apache/tools/zip/ExtraFieldUtils;->implementations:Ljava/util/Hashtable;

    invoke-interface {v2}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v4

    invoke-virtual {v3, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    return-void

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " doesn\'t implement ZipExtraField"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_1
    move-exception v1

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " is not a concrete class"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_2
    move-exception v1

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\'s no-arg constructor is not public"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
