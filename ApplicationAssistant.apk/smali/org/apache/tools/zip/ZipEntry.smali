.class public Lorg/apache/tools/zip/ZipEntry;
.super Ljava/util/zip/ZipEntry;
.source "ZipEntry.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final PLATFORM_FAT:I = 0x0

.field private static final PLATFORM_UNIX:I = 0x3


# instance fields
.field private externalAttributes:J

.field private extraFields:Ljava/util/Vector;

.field private internalAttributes:I

.field private name:Ljava/lang/String;

.field private platform:I


# direct methods
.method protected constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v0, ""

    invoke-direct {p0, v0}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    iput v1, p0, Lorg/apache/tools/zip/ZipEntry;->internalAttributes:I

    iput v1, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/zip/ZipEntry;->externalAttributes:J

    iput-object v2, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    iput-object v2, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    iput v0, p0, Lorg/apache/tools/zip/ZipEntry;->internalAttributes:I

    iput v0, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/zip/ZipEntry;->externalAttributes:J

    iput-object v2, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    iput-object v2, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/zip/ZipEntry;)V
    .locals 4
    .param p1    # Ljava/util/zip/ZipEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/util/zip/ZipEntry;)V

    iput v1, p0, Lorg/apache/tools/zip/ZipEntry;->internalAttributes:I

    iput v1, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lorg/apache/tools/zip/ZipEntry;->externalAttributes:J

    iput-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    iput-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getExtra()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->parse([B)[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/zip/ZipEntry;->setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->setExtra()V

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/tools/zip/ZipEntry;)V
    .locals 2
    .param p1    # Lorg/apache/tools/zip/ZipEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lorg/apache/tools/zip/ZipEntry;-><init>(Ljava/util/zip/ZipEntry;)V

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getInternalAttributes()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/zip/ZipEntry;->setInternalAttributes(I)V

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getExternalAttributes()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/zip/ZipEntry;->setExternalAttributes(J)V

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getExtraFields()[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/zip/ZipEntry;->setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V

    return-void
.end method


# virtual methods
.method public addExtraField(Lorg/apache/tools/zip/ZipExtraField;)V
    .locals 5
    .param p1    # Lorg/apache/tools/zip/ZipExtraField;

    iget-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v3

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    :goto_0
    if-nez v0, :cond_2

    if-ge v2, v1, :cond_2

    iget-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/zip/ZipExtraField;

    invoke-interface {v4}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v4

    invoke-virtual {v4, v3}, Lorg/apache/tools/zip/ZipShort;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v4, p1, v2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    iget-object v4, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v4, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->setExtra()V

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    invoke-super {p0}, Ljava/util/zip/ZipEntry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/zip/ZipEntry;

    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    :goto_0
    iput-object v1, v0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getInternalAttributes()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipEntry;->setInternalAttributes(I)V

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExternalAttributes()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/zip/ZipEntry;->setExternalAttributes(J)V

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExtraFields()[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipEntry;->setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCentralDirectoryExtra()[B
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExtraFields()[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->mergeCentralDirectoryData([Lorg/apache/tools/zip/ZipExtraField;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExternalAttributes()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/tools/zip/ZipEntry;->externalAttributes:J

    return-wide v0
.end method

.method public getExtraFields()[Lorg/apache/tools/zip/ZipExtraField;
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    new-array v0, v1, [Lorg/apache/tools/zip/ZipExtraField;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Lorg/apache/tools/zip/ZipExtraField;

    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getInternalAttributes()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/zip/ZipEntry;->internalAttributes:I

    return v0
.end method

.method public getLocalFileDataExtra()[B
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExtra()[B

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    new-array v0, v1, [B

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPlatform()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    return v0
.end method

.method public getUnixMode()I
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExternalAttributes()J

    move-result-wide v0

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isDirectory()Z
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeExtraField(Lorg/apache/tools/zip/ZipShort;)V
    .locals 4
    .param p1    # Lorg/apache/tools/zip/ZipShort;

    iget-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v1

    :goto_0
    if-nez v0, :cond_2

    if-ge v2, v1, :cond_2

    iget-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/zip/ZipExtraField;

    invoke-interface {v3}, Lorg/apache/tools/zip/ZipExtraField;->getHeaderId()Lorg/apache/tools/zip/ZipShort;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/apache/tools/zip/ZipShort;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->removeElementAt(I)V

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    new-instance v3, Ljava/util/NoSuchElementException;

    invoke-direct {v3}, Ljava/util/NoSuchElementException;-><init>()V

    throw v3

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->setExtra()V

    return-void
.end method

.method public setComprSize(J)V
    .locals 0
    .param p1    # J

    invoke-virtual {p0, p1, p2}, Lorg/apache/tools/zip/ZipEntry;->setCompressedSize(J)V

    return-void
.end method

.method public setExternalAttributes(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/zip/ZipEntry;->externalAttributes:J

    return-void
.end method

.method protected setExtra()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->getExtraFields()[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/zip/ExtraFieldUtils;->mergeLocalFileDataData([Lorg/apache/tools/zip/ZipExtraField;)[B

    move-result-object v0

    invoke-super {p0, v0}, Ljava/util/zip/ZipEntry;->setExtra([B)V

    return-void
.end method

.method public setExtra([B)V
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lorg/apache/tools/zip/ExtraFieldUtils;->parse([B)[Lorg/apache/tools/zip/ZipExtraField;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/zip/ZipEntry;->setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V
    .locals 3
    .param p1    # [Lorg/apache/tools/zip/ZipExtraField;

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/zip/ZipEntry;->extraFields:Ljava/util/Vector;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->setExtra()V

    return-void
.end method

.method public setInternalAttributes(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/zip/ZipEntry;->internalAttributes:I

    return-void
.end method

.method protected setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/zip/ZipEntry;->name:Ljava/lang/String;

    return-void
.end method

.method protected setPlatform(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    return-void
.end method

.method public setUnixMode(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    shl-int/lit8 v2, p1, 0x10

    and-int/lit16 v0, p1, 0x80

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipEntry;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x10

    :cond_0
    or-int/2addr v0, v1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/zip/ZipEntry;->setExternalAttributes(J)V

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/tools/zip/ZipEntry;->platform:I

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
