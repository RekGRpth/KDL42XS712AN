.class public Lorg/apache/tools/zip/ZipFile;
.super Ljava/lang/Object;
.source "ZipFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/zip/ZipFile$1;,
        Lorg/apache/tools/zip/ZipFile$BoundedInputStream;,
        Lorg/apache/tools/zip/ZipFile$OffsetEntry;
    }
.end annotation


# static fields
.field private static final CFD_LOCATOR_OFFSET:I = 0x10

.field private static final CFH_LEN:I = 0x2a

.field private static final LFH_OFFSET_FOR_FILENAME_LENGTH:J = 0x1aL

.field private static final MIN_EOCD_SIZE:I = 0x16


# instance fields
.field private archive:Ljava/io/RandomAccessFile;

.field private encoding:Ljava/lang/String;

.field private entries:Ljava/util/Hashtable;

.field private nameMap:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v2, 0x1fd

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1, v2}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v1, p0, Lorg/apache/tools/zip/ZipFile;->entries:Ljava/util/Hashtable;

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1, v2}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v1, p0, Lorg/apache/tools/zip/ZipFile;->nameMap:Ljava/util/Hashtable;

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/zip/ZipFile;->encoding:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/tools/zip/ZipFile;->encoding:Ljava/lang/String;

    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    :try_start_0
    invoke-direct {p0}, Lorg/apache/tools/zip/ZipFile;->populateFromCentralDirectory()V

    invoke-direct {p0}, Lorg/apache/tools/zip/ZipFile;->resolveLocalFileHeaderData()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static access$300(Lorg/apache/tools/zip/ZipFile;)Ljava/io/RandomAccessFile;
    .locals 1
    .param p0    # Lorg/apache/tools/zip/ZipFile;

    iget-object v0, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    return-object v0
.end method

.method public static closeQuietly(Lorg/apache/tools/zip/ZipFile;)V
    .locals 1
    .param p0    # Lorg/apache/tools/zip/ZipFile;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static dosToJavaTime(J)J
    .locals 9
    .param p0    # J

    const/16 v8, 0xb

    const/4 v7, 0x5

    const/4 v6, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0x19

    shr-long v1, p0, v1

    const-wide/16 v3, 0x7f

    and-long/2addr v1, v3

    long-to-int v1, v1

    add-int/lit16 v1, v1, 0x7bc

    invoke-virtual {v0, v6, v1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    const/16 v2, 0x15

    shr-long v2, p0, v2

    const-wide/16 v4, 0xf

    and-long/2addr v2, v4

    long-to-int v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0x10

    shr-long v1, p0, v1

    long-to-int v1, v1

    and-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->set(II)V

    shr-long v1, p0, v8

    long-to-int v1, v1

    and-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0, v8, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    shr-long v2, p0, v7

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x3f

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    shl-long v2, p0, v6

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x3e

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    return-wide v1
.end method

.method protected static fromDosTime(Lorg/apache/tools/zip/ZipLong;)Ljava/util/Date;
    .locals 5
    .param p0    # Lorg/apache/tools/zip/ZipLong;

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipLong;->getValue()J

    move-result-wide v0

    new-instance v2, Ljava/util/Date;

    invoke-static {v0, v1}, Lorg/apache/tools/zip/ZipFile;->dosToJavaTime(J)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    return-object v2
.end method

.method private populateFromCentralDirectory()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/zip/ZipFile;->positionAtCentralDirectory()V

    const/16 v20, 0x2a

    move/from16 v0, v20

    new-array v3, v0, [B

    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v15, v0, [B

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/io/RandomAccessFile;->readFully([B)V

    invoke-static {v15}, Lorg/apache/tools/zip/ZipLong;->getValue([B)J

    move-result-wide v13

    sget-object v20, Lorg/apache/tools/zip/ZipOutputStream;->CFH_SIG:[B

    invoke-static/range {v20 .. v20}, Lorg/apache/tools/zip/ZipLong;->getValue([B)J

    move-result-wide v4

    :goto_0
    cmp-long v20, v13, v4

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/io/RandomAccessFile;->readFully([B)V

    const/4 v11, 0x0

    new-instance v19, Lorg/apache/tools/zip/ZipEntry;

    invoke-direct/range {v19 .. v19}, Lorg/apache/tools/zip/ZipEntry;-><init>()V

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v18

    add-int/lit8 v11, v11, 0x2

    shr-int/lit8 v20, v18, 0x8

    and-int/lit8 v20, v20, 0xf

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/zip/ZipEntry;->setPlatform(I)V

    add-int/lit8 v11, v11, 0x4

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/zip/ZipEntry;->setMethod(I)V

    add-int/lit8 v11, v11, 0x2

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lorg/apache/tools/zip/ZipFile;->dosToJavaTime(J)J

    move-result-wide v16

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/zip/ZipEntry;->setTime(J)V

    add-int/lit8 v11, v11, 0x4

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/zip/ZipEntry;->setCrc(J)V

    add-int/lit8 v11, v11, 0x4

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/zip/ZipEntry;->setCompressedSize(J)V

    add-int/lit8 v11, v11, 0x4

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/zip/ZipEntry;->setSize(J)V

    add-int/lit8 v11, v11, 0x4

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v10

    add-int/lit8 v11, v11, 0x2

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v8

    add-int/lit8 v11, v11, 0x2

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v7

    add-int/lit8 v11, v11, 0x2

    add-int/lit8 v11, v11, 0x2

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipShort;->getValue([BI)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/zip/ZipEntry;->setInternalAttributes(I)V

    add-int/lit8 v11, v11, 0x2

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/zip/ZipEntry;->setExternalAttributes(J)V

    add-int/lit8 v11, v11, 0x4

    new-array v9, v10, [B

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/io/RandomAccessFile;->readFully([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lorg/apache/tools/zip/ZipFile;->getString([B)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/zip/ZipEntry;->setName(Ljava/lang/String;)V

    new-instance v12, Lorg/apache/tools/zip/ZipFile$OffsetEntry;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Lorg/apache/tools/zip/ZipFile$OffsetEntry;-><init>(Lorg/apache/tools/zip/ZipFile$1;)V

    invoke-static {v3, v11}, Lorg/apache/tools/zip/ZipLong;->getValue([BI)J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-static {v12, v0, v1}, Lorg/apache/tools/zip/ZipFile$OffsetEntry;->access$202(Lorg/apache/tools/zip/ZipFile$OffsetEntry;J)J

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->entries:Ljava/util/Hashtable;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->nameMap:Ljava/util/Hashtable;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    new-array v6, v7, [B

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/io/RandomAccessFile;->readFully([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/apache/tools/zip/ZipFile;->getString([B)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/zip/ZipEntry;->setComment(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/io/RandomAccessFile;->readFully([B)V

    invoke-static {v15}, Lorg/apache/tools/zip/ZipLong;->getValue([B)J

    move-result-wide v13

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method private positionAtCentralDirectory()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x16

    sub-long v3, v6, v8

    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-ltz v6, :cond_0

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    sget-object v5, Lorg/apache/tools/zip/ZipOutputStream;->EOCD_SIG:[B

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    :goto_0
    const/4 v6, -0x1

    if-eq v1, v6, :cond_0

    const/4 v6, 0x0

    aget-byte v6, v5, v6

    if-ne v1, v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    const/4 v6, 0x1

    aget-byte v6, v5, v6

    if-ne v1, v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    const/4 v6, 0x2

    aget-byte v6, v5, v6

    if-ne v1, v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    const/4 v6, 0x3

    aget-byte v6, v5, v6

    if-ne v1, v6, :cond_1

    const/4 v2, 0x1

    :cond_0
    if-nez v2, :cond_2

    new-instance v6, Ljava/util/zip/ZipException;

    const-string v7, "archive is not a ZIP archive"

    invoke-direct {v6, v7}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    const-wide/16 v7, 0x1

    sub-long/2addr v3, v7

    invoke-virtual {v6, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    const-wide/16 v7, 0x10

    add-long/2addr v7, v3

    invoke-virtual {v6, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 v6, 0x4

    new-array v0, v6, [B

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    iget-object v6, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lorg/apache/tools/zip/ZipLong;->getValue([B)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V

    return-void
.end method

.method private resolveLocalFileHeaderData()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/zip/ZipFile;->getEntries()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/zip/ZipEntry;

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->entries:Ljava/util/Hashtable;

    invoke-virtual {v9, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/zip/ZipFile$OffsetEntry;

    invoke-static {v7}, Lorg/apache/tools/zip/ZipFile$OffsetEntry;->access$200(Lorg/apache/tools/zip/ZipFile$OffsetEntry;)J

    move-result-wide v5

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    const-wide/16 v10, 0x1a

    add-long/2addr v10, v5

    invoke-virtual {v9, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 v9, 0x2

    new-array v0, v9, [B

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v9, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    invoke-static {v0}, Lorg/apache/tools/zip/ZipShort;->getValue([B)I

    move-result v3

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v9, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    invoke-static {v0}, Lorg/apache/tools/zip/ZipShort;->getValue([B)I

    move-result v2

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v9, v3}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    new-array v4, v2, [B

    iget-object v9, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v9, v4}, Ljava/io/RandomAccessFile;->readFully([B)V

    invoke-virtual {v8, v4}, Lorg/apache/tools/zip/ZipEntry;->setExtra([B)V

    const-wide/16 v9, 0x1a

    add-long/2addr v9, v5

    const-wide/16 v11, 0x2

    add-long/2addr v9, v11

    const-wide/16 v11, 0x2

    add-long/2addr v9, v11

    int-to-long v11, v3

    add-long/2addr v9, v11

    int-to-long v11, v2

    add-long/2addr v9, v11

    invoke-static {v7, v9, v10}, Lorg/apache/tools/zip/ZipFile$OffsetEntry;->access$002(Lorg/apache/tools/zip/ZipFile$OffsetEntry;J)J

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/zip/ZipFile;->archive:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/zip/ZipFile;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method public getEntries()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/zip/ZipFile;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lorg/apache/tools/zip/ZipEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/zip/ZipFile;->nameMap:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/zip/ZipEntry;

    return-object v0
.end method

.method public getInputStream(Lorg/apache/tools/zip/ZipEntry;)Ljava/io/InputStream;
    .locals 7
    .param p1    # Lorg/apache/tools/zip/ZipEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/zip/ZipException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/zip/ZipFile;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/tools/zip/ZipFile$OffsetEntry;

    if-nez v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    :sswitch_0
    return-object v0

    :cond_0
    invoke-static {v6}, Lorg/apache/tools/zip/ZipFile$OffsetEntry;->access$000(Lorg/apache/tools/zip/ZipFile$OffsetEntry;)J

    move-result-wide v2

    new-instance v0, Lorg/apache/tools/zip/ZipFile$BoundedInputStream;

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getCompressedSize()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/zip/ZipFile$BoundedInputStream;-><init>(Lorg/apache/tools/zip/ZipFile;JJ)V

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getMethod()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v1, Ljava/util/zip/ZipException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Found unsupported compression method "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/tools/zip/ZipEntry;->getMethod()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_1
    invoke-virtual {v0}, Lorg/apache/tools/zip/ZipFile$BoundedInputStream;->addDummy()V

    new-instance v1, Ljava/util/zip/InflaterInputStream;

    new-instance v4, Ljava/util/zip/Inflater;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v1, v0, v4}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method protected getString([B)Ljava/lang/String;
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/zip/ZipFile;->encoding:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/zip/ZipFile;->encoding:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/util/zip/ZipException;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
