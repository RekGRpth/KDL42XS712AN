.class public Lorg/apache/tools/ant/taskdefs/SQLExec;
.super Lorg/apache/tools/ant/taskdefs/JDBCTask;
.source "SQLExec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;,
        Lorg/apache/tools/ant/taskdefs/SQLExec$OnError;,
        Lorg/apache/tools/ant/taskdefs/SQLExec$DelimiterType;
    }
.end annotation


# instance fields
.field private append:Z

.field private conn:Ljava/sql/Connection;

.field private delimiter:Ljava/lang/String;

.field private delimiterType:Ljava/lang/String;

.field private encoding:Ljava/lang/String;

.field private escapeProcessing:Z

.field private expandProperties:Z

.field private goodSql:I

.field private keepformat:Z

.field private onError:Ljava/lang/String;

.field private output:Ljava/io/File;

.field private print:Z

.field private resources:Lorg/apache/tools/ant/types/resources/Union;

.field private showheaders:Z

.field private showtrailers:Z

.field private sqlCommand:Ljava/lang/String;

.field private srcFile:Ljava/io/File;

.field private statement:Ljava/sql/Statement;

.field private totalSql:I

.field private transactions:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/JDBCTask;-><init>()V

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->goodSql:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->totalSql:I

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->resources:Lorg/apache/tools/ant/types/resources/Union;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    const-string v0, ";"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiter:Ljava/lang/String;

    const-string v0, "normal"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiterType:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->print:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showheaders:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showtrailers:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->output:Ljava/io/File;

    const-string v0, "abort"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->onError:Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->encoding:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->append:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->escapeProcessing:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->expandProperties:Z

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/SQLExec;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/SQLExec;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method private closeQuietly()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->isAutocommit()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->onError:Ljava/lang/String;

    const-string v1, "abort"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v0}, Ljava/sql/Connection;->rollback()V
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/SQLExec;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    return-void
.end method

.method public createTransaction()Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;-><init>(Lorg/apache/tools/ant/taskdefs/SQLExec;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method protected execSQL(Ljava/lang/String;Ljava/io/PrintStream;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    const-string v6, ""

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    iget v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->totalSql:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->totalSql:I

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "SQL: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6, p1}, Ljava/sql/Statement;->execute(Ljava/lang/String;)Z

    move-result v2

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6}, Ljava/sql/Statement;->getUpdateCount()I

    move-result v3

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6}, Ljava/sql/Statement;->getResultSet()Ljava/sql/ResultSet;

    move-result-object v1

    :cond_2
    if-nez v2, :cond_6

    const/4 v6, -0x1

    if-eq v3, v6, :cond_3

    add-int/2addr v4, v3

    :cond_3
    :goto_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6}, Ljava/sql/Statement;->getMoreResults()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6}, Ljava/sql/Statement;->getUpdateCount()I

    move-result v3

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v6}, Ljava/sql/Statement;->getResultSet()Ljava/sql/ResultSet;

    move-result-object v1

    :cond_4
    if-nez v2, :cond_2

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " rows affected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->print:Z

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showtrailers:Z

    if-eqz v6, :cond_5

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " rows affected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v6}, Ljava/sql/Connection;->getWarnings()Ljava/sql/SQLWarning;

    move-result-object v5

    :goto_2
    if-eqz v5, :cond_8

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " sql warning"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    invoke-virtual {v5}, Ljava/sql/SQLWarning;->getNextWarning()Ljava/sql/SQLWarning;

    move-result-object v5

    goto :goto_2

    :cond_6
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->print:Z

    if-eqz v6, :cond_3

    invoke-virtual {p0, v1, p2}, Lorg/apache/tools/ant/taskdefs/SQLExec;->printResults(Ljava/sql/ResultSet;Ljava/io/PrintStream;)V
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Failed to execute: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->onError:Ljava/lang/String;

    const-string v7, "continue"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v6

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/sql/ResultSet;->close()V

    :cond_7
    throw v6

    :cond_8
    :try_start_2
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v6}, Ljava/sql/Connection;->clearWarnings()V

    iget v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->goodSql:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->goodSql:I
    :try_end_2
    .catch Ljava/sql/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/sql/ResultSet;->close()V

    goto/16 :goto_0

    :cond_9
    :try_start_3
    invoke-virtual {v0}, Ljava/sql/SQLException;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/sql/ResultSet;->close()V

    goto/16 :goto_0
.end method

.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    :try_start_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-nez v8, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Source file or resource collection, transactions or sql statement must be set!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v8

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    throw v8

    :cond_0
    :try_start_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Source file does not exist!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->createTransaction()Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;

    move-result-object v7

    invoke-virtual {v7, v4}, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;->setSrcResource(Lorg/apache/tools/ant/types/Resource;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->createTransaction()Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;->setSrc(Ljava/io/File;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;->addText(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getConnection()Ljava/sql/Connection;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/SQLExec;->isValidRdbms(Ljava/sql/Connection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-nez v8, :cond_3

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    :goto_1
    return-void

    :cond_3
    :try_start_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v8}, Ljava/sql/Connection;->createStatement()Ljava/sql/Statement;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->escapeProcessing:Z

    invoke-interface {v8, v9}, Ljava/sql/Statement;->setEscapeProcessing(Z)V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/sql/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->output:Ljava/io/File;

    if-eqz v8, :cond_4

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Opening PrintStream to output file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->output:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    new-instance v3, Ljava/io/PrintStream;

    new-instance v8, Ljava/io/BufferedOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->output:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->append:Z

    invoke-direct {v9, v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v8, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v8}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    move-object v2, v3

    :cond_4
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :cond_5
    :goto_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;

    invoke-static {v8, v2}, Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;->access$000(Lorg/apache/tools/ant/taskdefs/SQLExec$Transaction;Ljava/io/PrintStream;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->isAutocommit()Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "Committing transaction"

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v8}, Ljava/sql/Connection;->commit()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v8

    if-eqz v2, :cond_6

    :try_start_4
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-eq v2, v9, :cond_6

    invoke-virtual {v2}, Ljava/io/PrintStream;->close()V

    :cond_6
    throw v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/sql/SQLException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    move-exception v0

    :try_start_5
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->closeQuietly()V

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v8, v0, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v8

    :try_start_6
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v9}, Ljava/sql/Statement;->close()V

    :cond_7
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v9}, Ljava/sql/Connection;->close()V
    :try_end_6
    .catch Ljava/sql/SQLException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_8
    :goto_3
    :try_start_7
    throw v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_9
    if-eqz v2, :cond_a

    :try_start_8
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-eq v2, v8, :cond_a

    invoke-virtual {v2}, Ljava/io/PrintStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/sql/SQLException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :cond_a
    :try_start_9
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    if-eqz v8, :cond_b

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v8}, Ljava/sql/Statement;->close()V

    :cond_b
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    if-eqz v8, :cond_c

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->conn:Ljava/sql/Connection;

    invoke-interface {v8}, Ljava/sql/Connection;->close()V
    :try_end_9
    .catch Ljava/sql/SQLException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_c
    :goto_4
    :try_start_a
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    iget v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->goodSql:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " of "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget v9, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->totalSql:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " SQL statements executed successfully"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->transactions:Ljava/util/Vector;

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->sqlCommand:Ljava/lang/String;

    goto/16 :goto_1

    :catch_1
    move-exception v0

    :try_start_b
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->closeQuietly()V

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v8, v0, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :catch_2
    move-exception v9

    goto :goto_3

    :catch_3
    move-exception v8

    goto :goto_4
.end method

.method public getExpandProperties()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->expandProperties:Z

    return v0
.end method

.method protected printResults(Ljava/io/PrintStream;)V
    .locals 2
    .param p1    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->statement:Ljava/sql/Statement;

    invoke-interface {v1}, Ljava/sql/Statement;->getResultSet()Ljava/sql/ResultSet;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/SQLExec;->printResults(Ljava/sql/ResultSet;Ljava/io/PrintStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/sql/ResultSet;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/sql/ResultSet;->close()V

    :cond_1
    throw v1
.end method

.method protected printResults(Ljava/sql/ResultSet;Ljava/io/PrintStream;)V
    .locals 8
    .param p1    # Ljava/sql/ResultSet;
    .param p2    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    if-eqz p1, :cond_5

    const-string v6, "Processing new result set."

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/SQLExec;->log(Ljava/lang/String;I)V

    invoke-interface {p1}, Ljava/sql/ResultSet;->getMetaData()Ljava/sql/ResultSetMetaData;

    move-result-object v5

    invoke-interface {v5}, Ljava/sql/ResultSetMetaData;->getColumnCount()I

    move-result v1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showheaders:Z

    if-eqz v6, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-interface {v5, v0}, Ljava/sql/ResultSetMetaData;->getColumnName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v5, v1}, Ljava/sql/ResultSetMetaData;->getColumnName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p2, v4}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/sql/ResultSet;->next()Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v3, 0x1

    const/4 v0, 0x1

    :goto_2
    if-gt v0, v1, :cond_4

    invoke-interface {p1, v0}, Ljava/sql/ResultSet;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-eqz v3, :cond_3

    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_4
    invoke-virtual {p2, v4}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Ljava/io/PrintStream;->println()V

    return-void
.end method

.method protected runStatements(Ljava/io/Reader;Ljava/io/PrintStream;)V
    .locals 8
    .param p1    # Ljava/io/Reader;
    .param p2    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    if-nez v5, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SQLExec;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v5, v1}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    if-nez v5, :cond_2

    const-string v5, "//"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "--"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v3, Ljava/util/StringTokenizer;

    invoke-direct {v3, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    const-string v5, "REM"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    if-nez v5, :cond_6

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    if-nez v5, :cond_3

    const-string v5, "--"

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_3

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiterType:Ljava/lang/String;

    const-string v6, "normal"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiter:Ljava/lang/String;

    invoke-static {v2, v5}, Lorg/apache/tools/ant/util/StringUtils;->endsWith(Ljava/lang/StringBuffer;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiterType:Ljava/lang/String;

    const-string v6, "row"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiter:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiter:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v2, v7, v5}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lorg/apache/tools/ant/taskdefs/SQLExec;->execSQL(Ljava/lang/String;Ljava/io/PrintStream;)V

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    const-string v6, ""

    invoke-virtual {v2, v7, v5, v6}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :cond_6
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-lez v5, :cond_8

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lorg/apache/tools/ant/taskdefs/SQLExec;->execSQL(Ljava/lang/String;Ljava/io/PrintStream;)V

    :cond_8
    return-void
.end method

.method public setAppend(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->append:Z

    return-void
.end method

.method public setDelimiter(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiter:Ljava/lang/String;

    return-void
.end method

.method public setDelimiterType(Lorg/apache/tools/ant/taskdefs/SQLExec$DelimiterType;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/SQLExec$DelimiterType;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/SQLExec$DelimiterType;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->delimiterType:Ljava/lang/String;

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setEscapeProcessing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->escapeProcessing:Z

    return-void
.end method

.method public setExpandProperties(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->expandProperties:Z

    return-void
.end method

.method public setKeepformat(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->keepformat:Z

    return-void
.end method

.method public setOnerror(Lorg/apache/tools/ant/taskdefs/SQLExec$OnError;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/SQLExec$OnError;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/SQLExec$OnError;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->onError:Ljava/lang/String;

    return-void
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->output:Ljava/io/File;

    return-void
.end method

.method public setPrint(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->print:Z

    return-void
.end method

.method public setShowheaders(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showheaders:Z

    return-void
.end method

.method public setShowtrailers(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->showtrailers:Z

    return-void
.end method

.method public setSrc(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SQLExec;->srcFile:Ljava/io/File;

    return-void
.end method
