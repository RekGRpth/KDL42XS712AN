.class public Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;
.super Ljava/lang/Object;
.source "MacroDef.java"

# interfaces
.implements Lorg/apache/tools/ant/TaskContainer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/MacroDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NestedSequential"
.end annotation


# instance fields
.field private nested:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getNested()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    return-object v0
.end method

.method public similar(Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;)Z
    .locals 6
    .param p1    # Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p1, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/UnknownElement;

    iget-object v4, p1, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->nested:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/UnknownElement;->similar(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method
