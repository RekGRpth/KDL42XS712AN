.class Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;
.super Ljava/lang/Object;
.source "ChangeLogParser.java"


# static fields
.field private static final CVS1129_INPUT_DATE:Ljava/text/SimpleDateFormat;

.field private static final GET_COMMENT:I = 0x3

.field private static final GET_DATE:I = 0x2

.field private static final GET_FILE:I = 0x1

.field private static final GET_PREVIOUS_REV:I = 0x5

.field private static final GET_REVISION:I = 0x4

.field private static final INPUT_DATE:Ljava/text/SimpleDateFormat;


# instance fields
.field private author:Ljava/lang/String;

.field private comment:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private final entries:Ljava/util/Hashtable;

.field private file:Ljava/lang/String;

.field private previousRevision:Ljava/lang/String;

.field private revision:Ljava/lang/String;

.field private status:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->INPUT_DATE:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss Z"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->CVS1129_INPUT_DATE:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sget-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->INPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    sget-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->CVS1129_INPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    return-void
.end method

.method private parseDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 5
    .param p1    # Ljava/lang/String;

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->INPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v2, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->CVS1129_INPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid date format: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private processComment(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "============================================================================="

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v0, v2, v3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->saveEntry()V

    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    :goto_0
    return-void

    :cond_0
    const-string v2, "----------------------------"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v0, v2, v3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    const/4 v2, 0x5

    iput v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    goto :goto_0
.end method

.method private processDate(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/16 v5, 0x3b

    const-string v3, "date:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const-string v3, "date: "

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->date:Ljava/lang/String;

    const-string v3, "author: "

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    const-string v3, "author: "

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->author:Ljava/lang/String;

    const/4 v3, 0x3

    iput v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    const-string v3, ""

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private processFile(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "Working file:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->file:Ljava/lang/String;

    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    :cond_0
    return-void
.end method

.method private processGetPreviousRevision(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "revision "

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Unexpected line from CVS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "revision "

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->previousRevision:Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->saveEntry()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->previousRevision:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->revision:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    return-void
.end method

.method private processRevision(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "revision"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->revision:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "======"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    goto :goto_0
.end method

.method private saveEntry()V
    .locals 6

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->date:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->author:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->date:Ljava/lang/String;

    invoke-direct {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->author:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    invoke-direct {v1, v0, v3, v4}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;-><init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    invoke-virtual {v3, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->file:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->revision:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->previousRevision:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v5}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->addFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    goto :goto_0
.end method


# virtual methods
.method public getEntrySetAsArray()[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;
    .locals 5

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->size()I

    move-result v4

    new-array v0, v4, [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    const/4 v2, 0x0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->entries:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    aput-object v4, v0, v2

    move v2, v3

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->file:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->date:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->author:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->comment:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->revision:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->previousRevision:Ljava/lang/String;

    return-void
.end method

.method public stdout(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->status:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->reset()V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->processFile(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->processRevision(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->processDate(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->processComment(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->processGetPreviousRevision(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
