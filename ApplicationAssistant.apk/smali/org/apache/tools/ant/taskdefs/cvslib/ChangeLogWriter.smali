.class public Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;
.super Ljava/lang/Object;
.source "ChangeLogWriter.java"


# static fields
.field private static final DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

.field private static final OUTPUT_DATE:Ljava/text/SimpleDateFormat;

.field private static final OUTPUT_TIME:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_DATE:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_TIME:Ljava/text/SimpleDateFormat;

    new-instance v1, Lorg/apache/tools/ant/util/DOMElementWriter;

    invoke-direct {v1}, Lorg/apache/tools/ant/util/DOMElementWriter;-><init>()V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sget-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    sget-object v1, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_TIME:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private printEntry(Lorg/w3c/dom/Document;Ljava/io/PrintWriter;Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    .locals 8
    .param p1    # Lorg/w3c/dom/Document;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v5, "entry"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v5, "date"

    sget-object v6, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_DATE:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getDate()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "time"

    sget-object v6, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->OUTPUT_TIME:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getDate()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "author"

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getAuthor()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendCDATAElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getFiles()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/taskdefs/cvslib/RCSFile;

    const-string v5, "file"

    invoke-static {v0, v5}, Lorg/apache/tools/ant/util/DOMUtils;->createChildElement(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    const-string v5, "name"

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/cvslib/RCSFile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendCDATAElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "revision"

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/cvslib/RCSFile;->getRevision()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/cvslib/RCSFile;->getPreviousRevision()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v5, "prevrevision"

    invoke-static {v2, v5, v4}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v5, "msg"

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getComment()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lorg/apache/tools/ant/util/DOMUtils;->appendCDATAElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v6, 0x1

    const-string v7, "\t"

    invoke-virtual {v5, v0, p2, v6, v7}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public printChangeLog(Ljava/io/PrintWriter;[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    .locals 10
    .param p1    # Ljava/io/PrintWriter;
    .param p2    # [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    :try_start_0
    const-string v0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/util/DOMUtils;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v6

    const-string v0, "changelog"

    invoke-interface {v6, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v2, 0x0

    const-string v3, "\t"

    invoke-virtual {v0, v1, p1, v2, v3}, Lorg/apache/tools/ant/util/DOMElementWriter;->openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    const/4 v9, 0x0

    :goto_0
    array-length v0, p2

    if-ge v9, v0, :cond_0

    aget-object v8, p2, v9

    invoke-direct {p0, v6, p1, v8}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->printEntry(Lorg/w3c/dom/Document;Ljava/io/PrintWriter;Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v3, 0x0

    const-string v4, "\t"

    const/4 v5, 0x1

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/util/DOMElementWriter;->closeElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V

    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    invoke-virtual {p1}, Ljava/io/PrintWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v7

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
