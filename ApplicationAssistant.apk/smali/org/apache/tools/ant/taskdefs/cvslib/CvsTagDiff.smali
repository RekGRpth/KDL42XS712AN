.class public Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;
.super Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;
.source "CvsTagDiff.java"


# static fields
.field private static final DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

.field static final FILE_HAS_CHANGED:Ljava/lang/String; = " changed from revision "

.field static final FILE_IS_NEW:Ljava/lang/String; = " is new;"

.field static final FILE_STRING:Ljava/lang/String; = "File "

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field static final FILE_WAS_REMOVED:Ljava/lang/String; = " is removed"

.field static final REVISION:Ljava/lang/String; = "revision "

.field static final TO_STRING:Ljava/lang/String; = " to "


# instance fields
.field private mydestfile:Ljava/io/File;

.field private myendDate:Ljava/lang/String;

.field private myendTag:Ljava/lang/String;

.field private mypackage:Ljava/lang/String;

.field private mystartDate:Ljava/lang/String;

.field private mystartTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-instance v0, Lorg/apache/tools/ant/util/DOMElementWriter;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/DOMElementWriter;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;-><init>()V

    return-void
.end method

.method private parseRDiff(Ljava/io/File;)[Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;
    .locals 22
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v13, 0x0

    :try_start_0
    new-instance v14, Ljava/io/BufferedReader;

    new-instance v19, Ljava/io/FileReader;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "File "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mypackage:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v8

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    const/4 v6, 0x0

    :goto_0
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v19

    if-le v0, v8, :cond_1

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    invoke-virtual {v11, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    :goto_1
    const-string v19, " is new;"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v9, v0, :cond_3

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/4 v15, 0x0

    const/4 v10, -0x1

    const-string v19, "revision "

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v10, v0, :cond_0

    const-string v19, "revision "

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v10

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    :cond_0
    new-instance v6, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;

    invoke-direct {v6, v7, v15}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_2
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :cond_2
    const-string v19, "File "

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_1

    :cond_3
    const-string v19, " changed from revision "

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v9, v0, :cond_5

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v19, " to "

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v16

    const-string v19, " changed from revision "

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v9

    move/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const-string v19, " to "

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v16

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    new-instance v6, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;

    move-object/from16 v0, v17

    invoke-direct {v6, v7, v0, v12}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v4

    move-object v13, v14

    :goto_3
    :try_start_2
    new-instance v19, Lorg/apache/tools/ant/BuildException;

    const-string v20, "Error in parsing"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v19

    :goto_4
    if-eqz v13, :cond_4

    :try_start_3
    invoke-virtual {v13}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_4
    :goto_5
    throw v19

    :cond_5
    :try_start_4
    const-string v19, " is removed"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v9, v0, :cond_1

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/4 v15, 0x0

    const/4 v10, -0x1

    const-string v19, "revision "

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v10, v0, :cond_6

    const-string v19, "revision "

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v10

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    :cond_6
    new-instance v6, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v6, v7, v0, v15}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    goto/16 :goto_2

    :catchall_1
    move-exception v19

    move-object v13, v14

    goto :goto_4

    :cond_7
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v3, v0, [Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v14, :cond_8

    :try_start_5
    invoke-virtual {v14}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_8
    :goto_6
    return-object v3

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    goto :goto_6

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    goto/16 :goto_5

    :catch_3
    move-exception v4

    goto/16 :goto_3
.end method

.method private validate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mypackage:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Package/module must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mydestfile:Ljava/io/File;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Destfile must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartDate:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Start tag or start date must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartDate:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one of start tag and start date must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendDate:Ljava/lang/String;

    if-nez v0, :cond_4

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "End tag or end date must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendDate:Ljava/lang/String;

    if-eqz v0, :cond_5

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one of end tag and end date must be set."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-void
.end method

.method private writeTagDiff([Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;)V
    .locals 14
    .param p1    # [Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v13, 0x0

    const/4 v10, 0x0

    :try_start_0
    new-instance v11, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mydestfile:Ljava/io/File;

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v2, Ljava/io/PrintWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v3, "UTF-8"

    invoke-direct {v0, v11, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const-string v0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/util/DOMUtils;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v7

    const-string v0, "tagdiff"

    invoke-interface {v7, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "startTag"

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "endTag"

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v0, "cvsroot"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->getCvsRoot()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "package"

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mypackage:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v3, 0x0

    const-string v4, "\t"

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/tools/ant/util/DOMElementWriter;->openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/PrintWriter;->println()V

    const/4 v8, 0x0

    array-length v6, p1

    :goto_2
    if-ge v8, v6, :cond_4

    aget-object v0, p1, v8

    invoke-direct {p0, v7, v2, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->writeTagEntry(Lorg/w3c/dom/Document;Ljava/io/PrintWriter;Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_0
    const-string v0, "startDate"

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartDate:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v12

    move-object v10, v11

    :goto_3
    :try_start_2
    invoke-virtual {v12}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v10, :cond_1

    :try_start_3
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    :goto_4
    return-void

    :cond_2
    :try_start_4
    const-string v0, "endDate"

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendDate:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v9

    move-object v10, v11

    :goto_5
    :try_start_5
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v10, :cond_3

    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_3
    :goto_7
    throw v0

    :cond_4
    :try_start_7
    sget-object v0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v3, 0x0

    const-string v4, "\t"

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/util/DOMElementWriter;->closeElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V

    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v11, :cond_5

    :try_start_8
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    move-object v10, v11

    goto :goto_4

    :catch_2
    move-exception v9

    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v13}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    move-object v10, v11

    goto :goto_4

    :catch_3
    move-exception v9

    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v13}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    goto :goto_4

    :catch_4
    move-exception v9

    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v13}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->log(Ljava/lang/String;I)V

    goto :goto_7

    :catchall_1
    move-exception v0

    move-object v10, v11

    goto :goto_6

    :catch_5
    move-exception v9

    goto :goto_5

    :catch_6
    move-exception v12

    goto :goto_3

    :cond_5
    move-object v10, v11

    goto :goto_4
.end method

.method private writeTagEntry(Lorg/w3c/dom/Document;Ljava/io/PrintWriter;Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;)V
    .locals 5
    .param p1    # Lorg/w3c/dom/Document;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v2, "entry"

    invoke-interface {p1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Lorg/apache/tools/ant/util/DOMUtils;->createChildElement(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->getFile()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lorg/apache/tools/ant/util/DOMUtils;->appendCDATAElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->getRevision()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "revision"

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->getRevision()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->getPreviousRevision()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "prevrevision"

    invoke-virtual {p3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;->getPreviousRevision()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lorg/apache/tools/ant/util/DOMUtils;->appendTextElement(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-object v2, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->DOM_WRITER:Lorg/apache/tools/ant/util/DOMElementWriter;

    const/4 v3, 0x1

    const-string v4, "\t"

    invoke-virtual {v2, v0, p2, v3, v4}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->validate()V

    const-string v3, "rdiff"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    const-string v3, "-s"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, "-r"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    if-eqz v3, :cond_1

    const-string v3, "-r"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Ljava/util/StringTokenizer;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mypackage:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :goto_2
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    const-string v3, "-D"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartDate:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v3, "-D"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendDate:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->addCommandArgument(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v3, ""

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->setCommand(Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    sget-object v3, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v4, "cvstagdiff"

    const-string v5, ".log"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->deleteOnExit()V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->setOutput(Ljava/io/File;)V

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->execute()V

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->parseRDiff(Ljava/io/File;)[Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->writeTagDiff([Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_3
    return-void

    :catchall_0
    move-exception v3

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_4
    throw v3
.end method

.method public setDestFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mydestfile:Ljava/io/File;

    return-void
.end method

.method public setEndDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendDate:Ljava/lang/String;

    return-void
.end method

.method public setEndTag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->myendTag:Ljava/lang/String;

    return-void
.end method

.method public setPackage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mypackage:Ljava/lang/String;

    return-void
.end method

.method public setStartDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartDate:Ljava/lang/String;

    return-void
.end method

.method public setStartTag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsTagDiff;->mystartTag:Ljava/lang/String;

    return-void
.end method
