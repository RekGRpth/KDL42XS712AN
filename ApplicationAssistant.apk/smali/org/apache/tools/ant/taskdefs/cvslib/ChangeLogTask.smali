.class public Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;
.super Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;
.source "ChangeLogTask.java"


# instance fields
.field private cvsUsers:Ljava/util/Vector;

.field private destFile:Ljava/io/File;

.field private endDate:Ljava/util/Date;

.field private final filesets:Ljava/util/Vector;

.field private inputDir:Ljava/io/File;

.field private startDate:Ljava/util/Date;

.field private usersFile:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->cvsUsers:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->filesets:Ljava/util/Vector;

    return-void
.end method

.method private filterEntrySet([Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;
    .locals 6
    .param p1    # [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    const/4 v2, 0x0

    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_4

    aget-object v0, p1, v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getDate()Ljava/util/Date;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->startDate:Ljava/util/Date;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->startDate:Ljava/util/Date;

    invoke-virtual {v5, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->endDate:Ljava/util/Date;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->endDate:Ljava/util/Date;

    invoke-virtual {v5, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    invoke-virtual {v4, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v3, v5, [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    return-object v3
.end method

.method private loadUserlist(Ljava/util/Properties;)V
    .locals 3
    .param p1    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p1, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private replaceAuthorIdWithName(Ljava/util/Properties;[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    .locals 3
    .param p1    # Ljava/util/Properties;
    .param p2    # [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    const/4 v1, 0x0

    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_1

    aget-object v0, p2, v1

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Properties;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;->setAuthor(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->destFile:Ljava/io/File;

    if-nez v1, :cond_1

    const-string v0, "Destfile must be set."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "Destfile must be set."

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Cannot find base dir "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Cannot find user lookup list "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    return-void
.end method

.method private writeChangeLog([Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    .locals 9
    .param p1    # [Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->destFile:Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Ljava/io/PrintWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    const-string v7, "UTF-8"

    invoke-direct {v6, v2, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    new-instance v3, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;

    invoke-direct {v3}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;-><init>()V

    invoke-virtual {v3, v5, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogWriter;->printChangeLog(Ljava/io/PrintWriter;[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    move-object v1, v2

    :goto_0
    return-void

    :catch_0
    move-exception v4

    :goto_1
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v1, v2

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_3
    move-exception v4

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addUser(Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->cvsUsers:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public execute()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->validate()V

    new-instance v19, Ljava/util/Properties;

    invoke-direct/range {v19 .. v19}, Ljava/util/Properties;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->loadUserlist(Ljava/util/Properties;)V

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->cvsUsers:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v17

    :goto_0
    move/from16 v0, v17

    if-ge v11, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->cvsUsers:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;->validate()V

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;->getUserID()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsUser;->getDisplayname()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    const-string v20, "log"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->setCommand(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getTag()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_1

    new-instance v12, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;

    invoke-direct {v12}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setProject(Lorg/apache/tools/ant/Project;)V

    const-string v20, "cvsversion"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setTaskName(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getCvsRoot()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setCvsRoot(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getCvsRsh()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setCvsRsh(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getPassFile()Ljava/io/File;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setPassfile(Ljava/io/File;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setDest(Ljava/io/File;)V

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->execute()V

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->supportsCvsLogWithSOption()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "-S"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->addCommandArgument(Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->startDate:Ljava/util/Date;

    move-object/from16 v20, v0

    if-eqz v20, :cond_2

    new-instance v13, Ljava/text/SimpleDateFormat;

    const-string v20, "yyyy-MM-dd"

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, ">="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->startDate:Ljava/util/Date;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v20, "-d"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->addCommandArgument(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->addCommandArgument(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->filesets:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->filesets:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    :goto_1
    array-length v0, v8

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v11, v0, :cond_3

    aget-object v20, v8, v11

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->addCommandArgument(Ljava/lang/String;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_4
    new-instance v14, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;

    invoke-direct {v14}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;-><init>()V

    new-instance v10, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;

    invoke-direct {v10, v14}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;-><init>(Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->getCommand()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->setDest(Ljava/io/File;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->setExecuteStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-super/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->execute()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;->getErrors()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->log(Ljava/lang/String;I)V

    :cond_5
    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;->getEntrySetAsArray()[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->filterEntrySet([Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;

    move-result-object v9

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v9}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->replaceAuthorIdWithName(Ljava/util/Properties;[Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->writeChangeLog([Lorg/apache/tools/ant/taskdefs/cvslib/CVSEntry;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    return-void

    :catchall_0
    move-exception v20

    :try_start_3
    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;->getErrors()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->log(Ljava/lang/String;I)V

    :cond_6
    throw v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v20

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    throw v20
.end method

.method public setDaysinpast(I)V
    .locals 10
    .param p1    # I

    const-wide/16 v8, 0x3c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, p1

    const-wide/16 v6, 0x18

    mul-long/2addr v4, v6

    mul-long/2addr v4, v8

    mul-long/2addr v4, v8

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    sub-long v0, v2, v4

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->setStart(Ljava/util/Date;)V

    return-void
.end method

.method public setDestfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->destFile:Ljava/io/File;

    return-void
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->inputDir:Ljava/io/File;

    return-void
.end method

.method public setEnd(Ljava/util/Date;)V
    .locals 0
    .param p1    # Ljava/util/Date;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->endDate:Ljava/util/Date;

    return-void
.end method

.method public setStart(Ljava/util/Date;)V
    .locals 0
    .param p1    # Ljava/util/Date;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->startDate:Ljava/util/Date;

    return-void
.end method

.method public setUsersfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogTask;->usersFile:Ljava/io/File;

    return-void
.end method
