.class Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;
.super Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;
.source "RedirectingStreamHandler.java"


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingOutputStream;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingOutputStream;-><init>(Lorg/apache/tools/ant/taskdefs/cvslib/ChangeLogParser;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method getErrors()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;->getErr()Ljava/io/OutputStream;

    move-result-object v1

    check-cast v1, Ljava/io/ByteArrayOutputStream;

    const-string v2, "ASCII"

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->stop()V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;->getErr()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/RedirectingStreamHandler;->getOut()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
