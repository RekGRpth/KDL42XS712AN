.class public Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;
.super Ljava/lang/Object;
.source "Zip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Zip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArchiveState"
.end annotation


# instance fields
.field private outOfDate:Z

.field private resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;


# direct methods
.method constructor <init>(Z[[Lorg/apache/tools/ant/types/Resource;)V
    .locals 0
    .param p1    # Z
    .param p2    # [[Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->outOfDate:Z

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    return-void
.end method


# virtual methods
.method public getResourcesToAdd()[[Lorg/apache/tools/ant/types/Resource;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    return-object v0
.end method

.method public isOutOfDate()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->outOfDate:Z

    return v0
.end method

.method public isWithoutAnyResources()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->resourcesToAdd:[[Lorg/apache/tools/ant/types/Resource;

    aget-object v2, v2, v0

    array-length v2, v2

    if-lez v2, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
