.class public Lorg/apache/tools/ant/taskdefs/Sleep;
.super Lorg/apache/tools/ant/Task;
.source "Sleep.java"


# instance fields
.field private failOnError:Z

.field private hours:I

.field private milliseconds:I

.field private minutes:I

.field private seconds:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->failOnError:Z

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->seconds:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->hours:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->minutes:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->milliseconds:I

    return-void
.end method

.method private getSleepTime()J
    .locals 6

    const-wide/16 v4, 0x3c

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->hours:I

    int-to-long v0, v0

    mul-long/2addr v0, v4

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->minutes:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    mul-long/2addr v0, v4

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->seconds:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->milliseconds:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public doSleep(J)V
    .locals 1
    .param p1    # J

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public execute()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sleep;->validate()V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Sleep;->getSleepTime()J

    move-result-wide v1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "sleeping for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " milliseconds"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Sleep;->log(Ljava/lang/String;I)V

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Sleep;->doSleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->failOnError:Z

    if-eqz v4, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/Sleep;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public setFailOnError(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->failOnError:Z

    return-void
.end method

.method public setHours(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->hours:I

    return-void
.end method

.method public setMilliseconds(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->milliseconds:I

    return-void
.end method

.method public setMinutes(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->minutes:I

    return-void
.end method

.method public setSeconds(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Sleep;->seconds:I

    return-void
.end method

.method public validate()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Sleep;->getSleepTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Negative sleep periods are not supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
