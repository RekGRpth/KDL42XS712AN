.class Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;
.super Ljava/io/Reader;
.source "Concat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Concat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiReader"
.end annotation


# instance fields
.field private i:Ljava/util/Iterator;

.field private lastChars:[C

.field private lastPos:I

.field private needAddSeparator:Z

.field private reader:Ljava/io/Reader;

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Concat;


# direct methods
.method private constructor <init>(Lorg/apache/tools/ant/taskdefs/Concat;Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p2    # Lorg/apache/tools/ant/types/ResourceCollection;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    invoke-interface {p2}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->i:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lorg/apache/tools/ant/taskdefs/Concat;Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/Concat$1;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Concat;
    .param p2    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p3    # Lorg/apache/tools/ant/taskdefs/Concat$1;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;-><init>(Lorg/apache/tools/ant/taskdefs/Concat;Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method private addLastChar(C)V
    .locals 4
    .param p1    # C

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    array-length v1, v1

    add-int/lit8 v0, v1, -0x2

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    add-int/lit8 v3, v0, 0x1

    aget-char v2, v2, v3

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aput-char p1, v1, v2

    return-void
.end method

.method private getReader()Ljava/io/Reader;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->i:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->i:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Resource;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Concating "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->toLongString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/taskdefs/Concat;->log(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Ljava/io/BufferedReader;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Concat;->access$300(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    :goto_0
    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([CC)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    return-object v2

    :cond_1
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Concat;->access$300(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isMissingEndOfLine()Z
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    aget-char v1, v1, v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private nextReader()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->reader:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    :cond_0
    return-void
.end method

.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, -0x1

    const/4 v5, 0x0

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v3}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_0

    iput v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    iput-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    :cond_0
    :goto_0
    return v1

    :cond_1
    :goto_1
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->getReader()Ljava/io/Reader;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->getReader()Ljava/io/Reader;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/Reader;->read()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->nextReader()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Concat;->access$400(Lorg/apache/tools/ant/taskdefs/Concat;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->isMissingEndOfLine()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    iput v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    goto :goto_1

    :cond_2
    int-to-char v2, v0

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->addLastChar(C)V

    move v1, v0

    goto :goto_0
.end method

.method public read([CII)I
    .locals 8
    .param p1    # [C
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, -0x1

    const/4 v7, 0x0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->getReader()Ljava/io/Reader;

    move-result-object v4

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    if-eqz v4, :cond_9

    :cond_1
    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, p1, p2

    iget v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/Concat;->access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v4, v5, :cond_2

    iput v7, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    iput-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    :cond_2
    add-int/lit8 p3, p3, -0x1

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 v0, v0, 0x1

    if-nez p3, :cond_0

    move v3, v0

    :cond_3
    :goto_1
    return v3

    :cond_4
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->getReader()Ljava/io/Reader;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/Reader;->read([CII)I

    move-result v2

    if-eq v2, v3, :cond_5

    if-nez v2, :cond_6

    :cond_5
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->nextReader()V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Concat;->access$400(Lorg/apache/tools/ant/taskdefs/Concat;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->isMissingEndOfLine()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->needAddSeparator:Z

    iput v7, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastPos:I

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->this$0:Lorg/apache/tools/ant/taskdefs/Concat;

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Concat;->access$400(Lorg/apache/tools/ant/taskdefs/Concat;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v1, v2

    :goto_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->lastChars:[C

    array-length v4, v4

    sub-int v4, v2, v4

    if-le v1, v4, :cond_7

    if-gtz v1, :cond_8

    :cond_7
    sub-int/2addr p3, v2

    add-int/2addr p2, v2

    add-int/2addr v0, v2

    if-nez p3, :cond_0

    move v3, v0

    goto :goto_1

    :cond_8
    add-int v4, p2, v1

    add-int/lit8 v4, v4, -0x1

    aget-char v4, p1, v4

    invoke-direct {p0, v4}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;->addLastChar(C)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_9
    if-eqz v0, :cond_3

    move v3, v0

    goto :goto_1
.end method
