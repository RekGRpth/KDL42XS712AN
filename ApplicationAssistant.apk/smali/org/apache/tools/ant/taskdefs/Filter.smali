.class public Lorg/apache/tools/ant/taskdefs/Filter;
.super Lorg/apache/tools/ant/Task;
.source "Filter.java"


# instance fields
.field private filtersFile:Ljava/io/File;

.field private token:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->filtersFile:Ljava/io/File;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->token:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->value:Ljava/lang/String;

    if-nez v4, :cond_0

    move v0, v2

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->filtersFile:Ljava/io/File;

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->token:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->value:Ljava/lang/String;

    if-eqz v4, :cond_1

    move v1, v2

    :goto_1
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "both token and value parameters, or only a filtersFile parameter is required"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Filter;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Filter;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/Project;->getGlobalFilterSet()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Filter;->token:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Filter;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/types/FilterSet;->addFilter(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Filter;->readFilters()V

    :cond_4
    return-void
.end method

.method protected readFilters()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Reading filters from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Filter;->filtersFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Filter;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Filter;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->getGlobalFilterSet()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Filter;->filtersFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FilterSet;->readFiltersFromFile(Ljava/io/File;)V

    return-void
.end method

.method public setFiltersfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Filter;->filtersFile:Ljava/io/File;

    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Filter;->token:Ljava/lang/String;

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Filter;->value:Ljava/lang/String;

    return-void
.end method
