.class public Lorg/apache/tools/ant/taskdefs/ExecuteOn;
.super Lorg/apache/tools/ant/taskdefs/ExecTask;
.source "ExecuteOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/ExecuteOn$FileDirBoth;
    }
.end annotation


# instance fields
.field private addSourceFile:Z

.field protected destDir:Ljava/io/File;

.field protected filesets:Ljava/util/Vector;

.field private force:Z

.field private forwardSlash:Z

.field private ignoreMissing:Z

.field protected mapper:Lorg/apache/tools/ant/util/FileNameMapper;

.field protected mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field private maxParallel:I

.field private parallel:Z

.field private relative:Z

.field private resources:Lorg/apache/tools/ant/types/resources/Union;

.field private skipEmpty:Z

.field protected srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

.field protected srcIsFirst:Z

.field protected targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

.field protected type:Ljava/lang/String;

.field private verbose:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->relative:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->parallel:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->forwardSlash:Z

    const-string v0, "file"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->skipEmpty:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->destDir:Ljava/io/File;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->maxParallel:I

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->addSourceFile:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->verbose:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->ignoreMissing:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->force:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcIsFirst:Z

    return-void
.end method

.method private restrict([Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;
    .locals 3
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->force:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->destDir:Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {v0, p1, p2, v1, v2}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->createMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addDirset(Lorg/apache/tools/ant/types/DirSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/DirSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method protected checkConfiguration()V
    .locals 3

    const-string v0, "execon"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "!! execon is deprecated. Use apply instead. !!"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->checkConfiguration()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "no resources specified"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "targetfile specified without mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->destDir:Ljava/io/File;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v0, :cond_3

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "dest specified without mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    :cond_4
    return-void
.end method

.method protected createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>()V

    goto :goto_0
.end method

.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public createSrcfile()Lorg/apache/tools/ant/types/Commandline$Marker;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getTaskType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " doesn\'t support multiple "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "srcfile elements."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createMarker()Lorg/apache/tools/ant/types/Commandline$Marker;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    return-object v0
.end method

.method public createTargetfile()Lorg/apache/tools/ant/types/Commandline$Marker;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getTaskType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " doesn\'t support multiple "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "targetfile elements."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createMarker()Lorg/apache/tools/ant/types/Commandline$Marker;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcIsFirst:Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getCommandline(Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    new-array v1, v1, [Ljava/io/File;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getCommandline([Ljava/lang/String;[Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCommandline([Ljava/lang/String;[Ljava/io/File;)[Ljava/lang/String;
    .locals 17
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/io/File;

    sget-char v2, Ljava/io/File;->separatorChar:C

    new-instance v12, Ljava/util/Vector;

    invoke-direct {v12}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v13, :cond_4

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v3, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    aget-object v14, p1, v3

    invoke-interface {v13, v14}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    const/4 v4, 0x0

    :goto_1
    array-length v13, v9

    if-ge v4, v13, :cond_3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->relative:Z

    if-nez v13, :cond_2

    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->destDir:Ljava/io/File;

    aget-object v15, v9, v4

    invoke-direct {v13, v14, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->forwardSlash:Z

    if-eqz v13, :cond_0

    const/16 v13, 0x2f

    if-eq v2, v13, :cond_0

    const/16 v13, 0x2f

    invoke-virtual {v5, v2, v13}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-virtual {v1, v5}, Ljava/util/Hashtable;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    invoke-virtual {v12, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v1, v5, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    aget-object v5, v9, v4

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v12}, Ljava/util/Vector;->size()I

    move-result v13

    new-array v10, v13, [Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->addSourceFile:Z

    if-nez v13, :cond_5

    const/4 v13, 0x0

    new-array v0, v13, [Ljava/lang/String;

    move-object/from16 p1, v0

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v6

    array-length v13, v6

    move-object/from16 v0, p1

    array-length v14, v0

    add-int/2addr v13, v14

    array-length v14, v10

    add-int/2addr v13, v14

    new-array v7, v13, [Ljava/lang/String;

    array-length v8, v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/Commandline$Marker;->getPosition()I

    move-result v8

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    if-eqz v13, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->targetFilePos:Lorg/apache/tools/ant/types/Commandline$Marker;

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/Commandline$Marker;->getPosition()I

    move-result v11

    if-lt v8, v11, :cond_7

    if-ne v8, v11, :cond_9

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->srcIsFirst:Z

    if-eqz v13, :cond_9

    :cond_7
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v6, v13, v7, v14, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p1

    array-length v13, v0

    add-int/2addr v13, v8

    sub-int v14, v11, v8

    invoke-static {v6, v8, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v13, 0x0

    move-object/from16 v0, p1

    array-length v14, v0

    add-int/2addr v14, v11

    array-length v15, v10

    invoke-static {v10, v13, v7, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p1

    array-length v13, v0

    add-int/2addr v13, v11

    array-length v14, v10

    add-int/2addr v13, v14

    array-length v14, v6

    sub-int/2addr v14, v11

    invoke-static {v6, v11, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_3
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v3, v13, :cond_c

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->relative:Z

    if-nez v13, :cond_b

    add-int v13, v8, v3

    new-instance v14, Ljava/io/File;

    aget-object v15, p2, v3

    aget-object v16, p1, v3

    invoke-direct/range {v14 .. v16}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v13

    :goto_5
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->forwardSlash:Z

    if-eqz v13, :cond_8

    const/16 v13, 0x2f

    if-eq v2, v13, :cond_8

    add-int v13, v8, v3

    add-int v14, v8, v3

    aget-object v14, v7, v14

    const/16 v15, 0x2f

    invoke-virtual {v14, v2, v15}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v13

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v6, v13, v7, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v13, 0x0

    array-length v14, v10

    invoke-static {v10, v13, v7, v11, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v13, v10

    add-int/2addr v13, v11

    sub-int v14, v8, v11

    invoke-static {v6, v11, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p1

    array-length v13, v0

    add-int/2addr v13, v8

    array-length v14, v10

    add-int/2addr v13, v14

    array-length v14, v6

    sub-int/2addr v14, v8

    invoke-static {v6, v8, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v13, v10

    add-int/2addr v8, v13

    goto :goto_3

    :cond_a
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v6, v13, v7, v14, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p1

    array-length v13, v0

    add-int/2addr v13, v8

    array-length v14, v6

    sub-int/2addr v14, v8

    invoke-static {v6, v8, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3

    :cond_b
    add-int v13, v8, v3

    aget-object v14, p1, v3

    aput-object v14, v7, v13

    goto :goto_5

    :cond_c
    return-object v7
.end method

.method protected getDirs(Ljava/io/File;Lorg/apache/tools/ant/DirectoryScanner;)[Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p2}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->restrict([Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFiles(Ljava/io/File;Lorg/apache/tools/ant/DirectoryScanner;)[Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p2}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->restrict([Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFilesAndDirs(Lorg/apache/tools/ant/types/FileList;)[Ljava/lang/String;
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/FileList;->getFiles(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/tools/ant/types/FileList;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->restrict([Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected runExec(Lorg/apache/tools/ant/taskdefs/Execute;)V
    .locals 25
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/16 v21, 0x0

    const/16 v20, 0x0

    const/4 v12, 0x0

    :try_start_0
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    const/4 v13, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/Vector;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v13, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->filesets:Ljava/util/Vector;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/tools/ant/types/AbstractFileSet;

    instance-of v0, v11, Lorg/apache/tools/ant/types/DirSet;

    move/from16 v22, v0

    if-eqz v22, :cond_0

    const-string v22, "dir"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_0

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string v23, "Found a nested dirset but type is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, ". "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "Temporarily switching to type=\"dir\" on the"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " assumption that you really did mean"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " <dirset> not <fileset>."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    const-string v6, "dir"

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/apache/tools/ant/types/AbstractFileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/apache/tools/ant/types/AbstractFileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v7

    const-string v22, "dir"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getFiles(Ljava/io/File;Lorg/apache/tools/ant/DirectoryScanner;)[Ljava/lang/String;

    move-result-object v19

    const/16 v16, 0x0

    :goto_1
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    add-int/lit8 v21, v21, 0x1

    aget-object v22, v19, v16

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :cond_1
    const-string v22, "file"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getDirs(Ljava/io/File;Lorg/apache/tools/ant/DirectoryScanner;)[Ljava/lang/String;

    move-result-object v19

    const/16 v16, 0x0

    :goto_2
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    add-int/lit8 v20, v20, 0x1

    aget-object v22, v19, v16

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v22

    if-nez v22, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->skipEmpty:Z

    move/from16 v22, v0

    if-eqz v22, :cond_7

    const-string v22, "dir"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_4

    invoke-virtual {v7}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFilesCount()I

    move-result v22

    move/from16 v23, v22

    :goto_3
    const-string v22, "file"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_5

    invoke-virtual {v7}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirsCount()I

    move-result v22

    :goto_4
    add-int v14, v23, v22

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string v23, "Skipping fileset for directory "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, ". It is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    if-lez v14, :cond_6

    const-string v22, "up to date."

    :goto_5
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    :cond_3
    :goto_6
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    :cond_4
    const/16 v22, 0x0

    move/from16 v23, v22

    goto :goto_3

    :cond_5
    const/16 v22, 0x0

    goto :goto_4

    :cond_6
    const-string v22, "empty."

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->parallel:Z

    move/from16 v22, v0

    if-nez v22, :cond_3

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    const/16 v16, 0x0

    :goto_7
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_b

    aget-object v22, v19, v16

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getCommandline(Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    if-eqz v22, :cond_8

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->setupRedirector()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v23, v0

    aget-object v24, v19, v16

    invoke-virtual/range {v22 .. v24}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    if-nez v22, :cond_9

    if-eqz v12, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/tools/ant/taskdefs/Redirector;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->setStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    :cond_a
    invoke-virtual/range {p0 .. p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V

    const/4 v12, 0x1

    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    :cond_b
    invoke-virtual {v9}, Ljava/util/Vector;->removeAllElements()V

    invoke-virtual {v4}, Ljava/util/Vector;->removeAllElements()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_6

    :catch_0
    move-exception v8

    :try_start_1
    new-instance v22, Lorg/apache/tools/ant/BuildException;

    new-instance v23, Ljava/lang/StringBuffer;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuffer;-><init>()V

    const-string v24, "Execute failed: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v22
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v22

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->logFlush()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppendProperties(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/taskdefs/Redirector;->setProperties()V

    throw v22

    :cond_c
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v22, v0

    if-eqz v22, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->resources:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/tools/ant/types/resources/Union;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_d
    :goto_8
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_15

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v22

    if-nez v22, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->ignoreMissing:Z

    move/from16 v22, v0

    if-nez v22, :cond_d

    :cond_e
    const/4 v3, 0x0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move/from16 v22, v0

    if-eqz v22, :cond_f

    move-object/from16 v0, v18

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move-object v10, v0

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_f

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    :cond_f
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v17, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->restrict([Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    if-eqz v22, :cond_d

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v22

    if-eqz v22, :cond_10

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v22

    if-nez v22, :cond_14

    :cond_10
    const-string v22, "dir"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_14

    add-int/lit8 v21, v21, 0x1

    :goto_9
    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->parallel:Z

    move/from16 v22, v0

    if-nez v22, :cond_d

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getCommandline(Ljava/lang/String;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    if-eqz v22, :cond_11

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->setupRedirector()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    move-object/from16 v22, v0

    if-nez v22, :cond_12

    if-eqz v12, :cond_13

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/tools/ant/taskdefs/Redirector;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->setStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    :cond_13
    invoke-virtual/range {p0 .. p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V

    const/4 v12, 0x1

    invoke-virtual {v9}, Ljava/util/Vector;->removeAllElements()V

    invoke-virtual {v4}, Ljava/util/Vector;->removeAllElements()V

    goto/16 :goto_8

    :cond_14
    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v22

    if-eqz v22, :cond_d

    const-string v22, "file"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_d

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_9

    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->parallel:Z

    move/from16 v22, v0

    if-eqz v22, :cond_17

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v22

    if-gtz v22, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->skipEmpty:Z

    move/from16 v22, v0

    if-nez v22, :cond_17

    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v9, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->runParallel(Lorg/apache/tools/ant/taskdefs/Execute;Ljava/util/Vector;Ljava/util/Vector;)V

    const/4 v12, 0x1

    :cond_17
    if-eqz v12, :cond_18

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string v23, "Applied "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/types/Commandline;->getExecutable()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " to "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " file"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_19

    const-string v22, "s"

    :goto_a
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " and "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, " director"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-eq v0, v1, :cond_1a

    const-string v22, "ies"

    :goto_b
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->verbose:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1b

    const/16 v22, 0x2

    :goto_c
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->logFlush()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppendProperties(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/tools/ant/taskdefs/Redirector;->setProperties()V

    return-void

    :cond_19
    :try_start_3
    const-string v22, ""

    goto :goto_a

    :cond_1a
    const-string v22, "y"
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_b

    :cond_1b
    const/16 v22, 0x3

    goto :goto_c
.end method

.method protected runParallel(Lorg/apache/tools/ant/taskdefs/Execute;Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 11
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .param p2    # Ljava/util/Vector;
    .param p3    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v8

    new-array v6, v8, [Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v8

    new-array v0, v8, [Ljava/io/File;

    invoke-virtual {p3, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->maxParallel:I

    if-lez v8, :cond_0

    array-length v8, v6

    if-nez v8, :cond_2

    :cond_0
    invoke-virtual {p0, v6, v0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getCommandline([Ljava/lang/String;[Ljava/io/File;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v7

    const/4 v5, 0x0

    :goto_0
    if-lez v7, :cond_1

    iget v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->maxParallel:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v6, v5, v3, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-array v1, v4, [Ljava/io/File;

    const/4 v8, 0x0

    invoke-static {v0, v5, v1, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, v3, v1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->getCommandline([Ljava/lang/String;[Ljava/io/File;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->log(Ljava/lang/String;I)V

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->setupRedirector()V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    :cond_3
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-nez v8, :cond_4

    if-lez v5, :cond_5

    :cond_4
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Redirector;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v8

    invoke-virtual {p1, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V

    sub-int/2addr v7, v4

    add-int/2addr v5, v4

    goto :goto_0
.end method

.method public setAddsourcefile(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->addSourceFile:Z

    return-void
.end method

.method public setDest(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->destDir:Ljava/io/File;

    return-void
.end method

.method public setForce(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->force:Z

    return-void
.end method

.method public setForwardslash(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->forwardSlash:Z

    return-void
.end method

.method public setIgnoremissing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->ignoreMissing:Z

    return-void
.end method

.method public setMaxParallel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->maxParallel:I

    return-void
.end method

.method public setParallel(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->parallel:Z

    return-void
.end method

.method public setRelative(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->relative:Z

    return-void
.end method

.method public setSkipEmptyFilesets(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->skipEmpty:Z

    return-void
.end method

.method public setType(Lorg/apache/tools/ant/taskdefs/ExecuteOn$FileDirBoth;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecuteOn$FileDirBoth;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/ExecuteOn$FileDirBoth;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->type:Ljava/lang/String;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->verbose:Z

    return-void
.end method

.method protected setupRedirector()V
    .locals 2

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setupRedirector()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecuteOn;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppendProperties(Z)V

    return-void
.end method
