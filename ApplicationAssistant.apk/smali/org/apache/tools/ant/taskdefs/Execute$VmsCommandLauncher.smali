.class Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;
.super Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;
.source "Execute.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Execute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VmsCommandLauncher"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;-><init>()V

    return-void
.end method

.method private createCommandFile([Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;
    .locals 9
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Execute;->access$200()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v5

    const-string v6, "ANT"

    const-string v7, ".COM"

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->deleteOnExit()V

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/PrintWriter;

    new-instance v5, Ljava/io/FileWriter;

    invoke-direct {v5, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    :goto_0
    :try_start_1
    array-length v5, p2

    if-ge v1, v5, :cond_1

    aget-object v5, p2, v1

    const/16 v6, 0x3d

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    const-string v5, "$ DEFINE/NOLOG "

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    aget-object v5, p2, v1

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " \""

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    aget-object v5, p2, v1

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v5, 0x22

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->println(C)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "$ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, p1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_1
    array-length v5, p1

    if-ge v1, v5, :cond_2

    const-string v5, " -"

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    aget-object v5, p1, v1

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/io/PrintWriter;->close()V

    :cond_3
    return-object v4

    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    :cond_4
    throw v5

    :catchall_1
    move-exception v5

    move-object v2, v3

    goto :goto_2
.end method

.method private deleteAfter(Ljava/io/File;Ljava/lang/Process;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/Process;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher$1;

    invoke-direct {v0, p0, p2, p1}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher$1;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;Ljava/lang/Process;Ljava/io/File;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher$1;->start()V

    return-void
.end method


# virtual methods
.method public exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Process;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2, p3}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;->createCommandFile([Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-super {p0, p1, v2, p3}, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;->exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;->deleteAfter(Ljava/io/File;Ljava/lang/Process;)V

    return-object v1
.end method

.method public exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2, p3}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;->createCommandFile([Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-super {p0, p1, v2, p3, p4}, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;->exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;->deleteAfter(Ljava/io/File;Ljava/lang/Process;)V

    return-object v1
.end method
