.class public Lorg/apache/tools/ant/taskdefs/LogOutputStream;
.super Lorg/apache/tools/ant/util/LineOrientedOutputStream;
.source "LogOutputStream.java"


# instance fields
.field private level:I

.field private pc:Lorg/apache/tools/ant/ProjectComponent;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/ProjectComponent;I)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/ProjectComponent;
    .param p2    # I

    invoke-direct {p0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->level:I

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->pc:Lorg/apache/tools/ant/ProjectComponent;

    iput p2, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->level:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;I)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Task;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/ProjectComponent;I)V

    return-void
.end method


# virtual methods
.method public getMessageLevel()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->level:I

    return v0
.end method

.method protected processBuffer()V
    .locals 4

    :try_start_0
    invoke-super {p0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->processBuffer()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Impossible IOException caught: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected processLine(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->level:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->processLine(Ljava/lang/String;I)V

    return-void
.end method

.method protected processLine(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LogOutputStream;->pc:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    return-void
.end method
