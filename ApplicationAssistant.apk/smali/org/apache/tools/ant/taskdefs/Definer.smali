.class public abstract Lorg/apache/tools/ant/taskdefs/Definer;
.super Lorg/apache/tools/ant/taskdefs/DefBase;
.source "Definer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Definer$Format;,
        Lorg/apache/tools/ant/taskdefs/Definer$OnError;,
        Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;
    }
.end annotation


# static fields
.field private static final ANTLIB_XML:Ljava/lang/String; = "/antlib.xml"

.field private static resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;


# instance fields
.field private adaptTo:Ljava/lang/String;

.field private adaptToClass:Ljava/lang/Class;

.field private adapter:Ljava/lang/String;

.field private adapterClass:Ljava/lang/Class;

.field private classname:Ljava/lang/String;

.field private definerSet:Z

.field private file:Ljava/io/File;

.field private format:I

.field private name:Ljava/lang/String;

.field private onError:I

.field private resource:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;-><init>(Lorg/apache/tools/ant/taskdefs/Definer$1;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/DefBase;-><init>()V

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->format:I

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    return-void
.end method

.method private fileToURL()Ljava/net/URL;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "File "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " does not exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "File "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " is not a file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toURL()Ljava/net/URL;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "File "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " cannot use as URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    :pswitch_0
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_1

    :pswitch_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private loadAntlib(Ljava/lang/ClassLoader;Ljava/net/URL;)V
    .locals 4
    .param p1    # Ljava/lang/ClassLoader;
    .param p2    # Ljava/net/URL;

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p2, v3}, Lorg/apache/tools/ant/taskdefs/Antlib;->createAntlib(Lorg/apache/tools/ant/Project;Ljava/net/URL;Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/Antlib;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Antlib;->setClassLoader(Ljava/lang/ClassLoader;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Antlib;->setURI(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Antlib;->execute()V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/tools/ant/ProjectHelper;->addLocationToBuildException(Lorg/apache/tools/ant/BuildException;Lorg/apache/tools/ant/Location;)Lorg/apache/tools/ant/BuildException;

    move-result-object v2

    throw v2
.end method

.method public static makeResourceFromURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const-string v2, "antlib:"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "//"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "//"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".xml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "/antlib.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v3, 0x2e

    const/16 v4, 0x2f

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "/antlib.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private resourceToURLs(Ljava/lang/ClassLoader;)Ljava/util/Enumeration;
    .locals 6
    .param p1    # Ljava/lang/ClassLoader;

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/ClassLoader;->getResources(Ljava/lang/String;)Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Could not load definitions from resource "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ". It could not be found."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget v3, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Could not fetch resources named "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v3

    :pswitch_0
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x3

    invoke-virtual {p0, v1, v3}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private tooManyDefinitions()V
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one of the attributes name, file and resource can be set"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0
.end method


# virtual methods
.method protected addDefinition(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/ClassLoader;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Lorg/apache/tools/ant/ProjectHelper;->genComponentName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    const/4 v6, 0x1

    invoke-static {p3, v6, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    :cond_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapter:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapter:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v6, v7, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapterClass:Ljava/lang/Class;

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptTo:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptTo:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v6, v7, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptToClass:Ljava/lang/Class;

    :cond_2
    new-instance v2, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {v2}, Lorg/apache/tools/ant/AntTypeDefinition;-><init>()V

    invoke-virtual {v2, p2}, Lorg/apache/tools/ant/AntTypeDefinition;->setName(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassName(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->setClass(Ljava/lang/Class;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapterClass:Ljava/lang/Class;

    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdapterClass(Ljava/lang/Class;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptToClass:Ljava/lang/Class;

    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdaptToClass(Ljava/lang/Class;)V

    invoke-virtual {v2, p1}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassLoader(Ljava/lang/ClassLoader;)V

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/AntTypeDefinition;->checkClass(Lorg/apache/tools/ant/Project;)V

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v6

    invoke-virtual {v6, v2}, Lorg/apache/tools/ant/ComponentHelper;->addDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " class "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " cannot be found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v6, v4, v1, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v6
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v3

    iget v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_0

    :catch_2
    move-exception v5

    :try_start_2
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getTaskName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " A class needed by class "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " cannot be found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/NoClassDefFoundError;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v6, v4, v5, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v6
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_1

    :pswitch_1
    throw v3

    :pswitch_2
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "Warning: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, v8}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public execute()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->createLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    if-nez v6, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "name, file or resource attribute of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getTaskName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " is undefined"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v6

    const-string v7, "antlib:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/tools/ant/taskdefs/Definer;->makeResourceFromURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/taskdefs/Definer;->setResource(Ljava/lang/String;)V

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    if-nez v6, :cond_3

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "classname attribute of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getTaskName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " element "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "is undefined"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_2
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Only antlib URIs can be located from the URI alone,not the URI "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getURI()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_3
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    invoke-virtual {p0, v0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Definer;->addDefinition(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    if-eqz v6, :cond_6

    const-string v2, "You must not specify classname together with file or resource."

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v6, v2, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_6
    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    if-eqz v6, :cond_8

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->fileToURL()Ljava/net/URL;

    move-result-object v4

    if-eqz v4, :cond_4

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Definer$1;

    invoke-direct {v5, p0, v4}, Lorg/apache/tools/ant/taskdefs/Definer$1;-><init>(Lorg/apache/tools/ant/taskdefs/Definer;Ljava/net/URL;)V

    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/URL;

    iget v1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->format:I

    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ".xml"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v1, 0x1

    :cond_7
    if-nez v1, :cond_9

    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/Definer;->loadProperties(Ljava/lang/ClassLoader;Ljava/net/URL;)V

    goto :goto_0

    :cond_8
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Definer;->resourceToURLs(Ljava/lang/ClassLoader;)Ljava/util/Enumeration;

    move-result-object v5

    goto :goto_1

    :cond_9
    sget-object v6, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;->getStack()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_a

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Warning: Recursive loading of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " ignored"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " originally loaded at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    sget-object v7, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;->getStack()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V

    goto :goto_1

    :cond_a
    :try_start_0
    sget-object v6, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;->getStack()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/Definer;->loadAntlib(Ljava/lang/ClassLoader;Ljava/net/URL;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v6, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;->getStack()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :catchall_0
    move-exception v6

    sget-object v7, Lorg/apache/tools/ant/taskdefs/Definer;->resourceStack:Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Definer$ResourceStack;->getStack()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v6
.end method

.method public getClassname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getResource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    return-object v0
.end method

.method protected loadProperties(Ljava/lang/ClassLoader;Ljava/net/URL;)V
    .locals 6
    .param p1    # Ljava/lang/ClassLoader;
    .param p2    # Ljava/net/URL;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Could not load definitions from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Definer;->log(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v3, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    invoke-virtual {p0, p1, v4, v5}, Lorg/apache/tools/ant/taskdefs/Definer;->addDefinition(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v4

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v4

    :cond_1
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method public setAdaptTo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptTo:Ljava/lang/String;

    return-void
.end method

.method protected setAdaptToClass(Ljava/lang/Class;)V
    .locals 0
    .param p1    # Ljava/lang/Class;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adaptToClass:Ljava/lang/Class;

    return-void
.end method

.method public setAdapter(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapter:Ljava/lang/String;

    return-void
.end method

.method protected setAdapterClass(Ljava/lang/Class;)V
    .locals 0
    .param p1    # Ljava/lang/Class;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->adapterClass:Ljava/lang/Class;

    return-void
.end method

.method public setAntlib(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->tooManyDefinitions()V

    :cond_0
    const-string v0, "antlib:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Invalid antlib attribute - it must start with antlib:"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Definer;->setURI(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "antlib:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    const/16 v3, 0x2f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "/antlib.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    return-void
.end method

.method public setClassname(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->classname:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->tooManyDefinitions()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->file:Ljava/io/File;

    return-void
.end method

.method public setFormat(Lorg/apache/tools/ant/taskdefs/Definer$Format;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Definer$Format;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Definer$Format;->getIndex()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->format:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->tooManyDefinitions()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->name:Ljava/lang/String;

    return-void
.end method

.method public setOnError(Lorg/apache/tools/ant/taskdefs/Definer$OnError;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Definer$OnError;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Definer$OnError;->getIndex()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->onError:I

    return-void
.end method

.method public setResource(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Definer;->tooManyDefinitions()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Definer;->definerSet:Z

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Definer;->resource:Ljava/lang/String;

    return-void
.end method
