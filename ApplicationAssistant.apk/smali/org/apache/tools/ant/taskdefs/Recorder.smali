.class public Lorg/apache/tools/ant/taskdefs/Recorder;
.super Lorg/apache/tools/ant/Task;
.source "Recorder.java"

# interfaces
.implements Lorg/apache/tools/ant/SubBuildListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Recorder$VerbosityLevelChoices;,
        Lorg/apache/tools/ant/taskdefs/Recorder$ActionChoices;
    }
.end annotation


# static fields
.field private static recorderEntries:Ljava/util/Hashtable;


# instance fields
.field private append:Ljava/lang/Boolean;

.field private emacsMode:Z

.field private filename:Ljava/lang/String;

.field private loglevel:I

.field private start:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Recorder;->recorderEntries:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->filename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->append:Ljava/lang/Boolean;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->loglevel:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->emacsMode:Z

    return-void
.end method

.method private cleanup()V
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Recorder;->recorderEntries:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/Project;->removeBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    return-void
.end method


# virtual methods
.method public buildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->cleanup()V

    return-void
.end method

.method public buildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public execute()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->filename:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "No filename specified"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "setting a recorder for name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->filename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->filename:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Recorder;->getRecorder(Ljava/lang/String;Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/taskdefs/RecorderEntry;

    move-result-object v0

    iget v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->loglevel:I

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setMessageOutputLevel(I)V

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->emacsMode:Z

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setEmacsMode(Z)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->reopenFile()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setRecordState(Ljava/lang/Boolean;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setRecordState(Ljava/lang/Boolean;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->closeFile()V

    goto :goto_0
.end method

.method protected getRecorder(Ljava/lang/String;Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/taskdefs/RecorderEntry;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Recorder;->recorderEntries:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->append:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->openFile(Z)V

    :goto_0
    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setProject(Lorg/apache/tools/ant/Project;)V

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Recorder;->recorderEntries:Ljava/util/Hashtable;

    invoke-virtual {v2, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-object v0

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->append:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->openFile(Z)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    check-cast v0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;

    goto :goto_1
.end method

.method public init()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    return-void
.end method

.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public setAction(Lorg/apache/tools/ant/taskdefs/Recorder$ActionChoices;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Recorder$ActionChoices;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Recorder$ActionChoices;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "start"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->start:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setAppend(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->append:Ljava/lang/Boolean;

    return-void

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setEmacsMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->emacsMode:Z

    return-void
.end method

.method public setLoglevel(Lorg/apache/tools/ant/taskdefs/Recorder$VerbosityLevelChoices;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Recorder$VerbosityLevelChoices;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Recorder$VerbosityLevelChoices;->getLevel()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->loglevel:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Recorder;->filename:Ljava/lang/String;

    return-void
.end method

.method public subBuildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Recorder;->cleanup()V

    :cond_0
    return-void
.end method

.method public subBuildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public taskFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public taskStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method
