.class public Lorg/apache/tools/ant/taskdefs/BUnzip2;
.super Lorg/apache/tools/ant/taskdefs/Unpack;
.source "BUnzip2.java"


# static fields
.field private static final DEFAULT_EXTENSION:Ljava/lang/String; = ".bz2"

.field static class$org$apache$tools$ant$taskdefs$BUnzip2:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Unpack;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected extract()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->source:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->lastModified()J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->dest:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->lastModified()J

    move-result-wide v15

    cmp-long v13, v13, v15

    if-lez v13, :cond_3

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Expanding "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->source:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->dest:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/tools/ant/taskdefs/BUnzip2;->log(Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->dest:Ljava/io/File;

    invoke-direct {v10, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->srcResource:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    const/16 v13, 0x42

    if-eq v1, v13, :cond_0

    new-instance v13, Lorg/apache/tools/ant/BuildException;

    const-string v14, "Invalid bz2 file."

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/BUnzip2;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v13
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v7

    move-object v2, v3

    move-object v9, v10

    :goto_0
    :try_start_3
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Problem expanding bzip2 "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v13, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/BUnzip2;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v14

    invoke-direct {v13, v8, v7, v14}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v13

    :goto_1
    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v6}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v9}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v11}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v13

    :cond_0
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    const/16 v13, 0x5a

    if-eq v1, v13, :cond_1

    new-instance v13, Lorg/apache/tools/ant/BuildException;

    const-string v14, "Invalid bz2 file."

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/BUnzip2;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v13

    :catchall_1
    move-exception v13

    move-object v2, v3

    move-object v9, v10

    goto :goto_1

    :cond_1
    new-instance v12, Lorg/apache/tools/bzip2/CBZip2InputStream;

    invoke-direct {v12, v3}, Lorg/apache/tools/bzip2/CBZip2InputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/16 v13, 0x2000

    :try_start_5
    new-array v4, v13, [B

    const/4 v5, 0x0

    :cond_2
    const/4 v13, 0x0

    invoke-virtual {v10, v4, v13, v5}, Ljava/io/FileOutputStream;->write([BII)V

    const/4 v13, 0x0

    array-length v14, v4

    invoke-virtual {v12, v4, v13, v14}, Lorg/apache/tools/bzip2/CBZip2InputStream;->read([BII)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result v5

    const/4 v13, -0x1

    if-ne v5, v13, :cond_2

    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v6}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v12}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :cond_3
    return-void

    :catchall_2
    move-exception v13

    move-object v9, v10

    goto :goto_1

    :catchall_3
    move-exception v13

    move-object v2, v3

    move-object v11, v12

    move-object v9, v10

    goto :goto_1

    :catch_1
    move-exception v7

    goto :goto_0

    :catch_2
    move-exception v7

    move-object v9, v10

    goto :goto_0

    :catch_3
    move-exception v7

    move-object v2, v3

    move-object v11, v12

    move-object v9, v10

    goto :goto_0
.end method

.method protected getDefaultExtension()Ljava/lang/String;
    .locals 1

    const-string v0, ".bz2"

    return-object v0
.end method

.method protected supportsNonFileResources()Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->class$org$apache$tools$ant$taskdefs$BUnzip2:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.taskdefs.BUnzip2"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/BUnzip2;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->class$org$apache$tools$ant$taskdefs$BUnzip2:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/BUnzip2;->class$org$apache$tools$ant$taskdefs$BUnzip2:Ljava/lang/Class;

    goto :goto_0
.end method
