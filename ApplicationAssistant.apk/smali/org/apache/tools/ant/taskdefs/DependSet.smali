.class public Lorg/apache/tools/ant/taskdefs/DependSet;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "DependSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/DependSet$1;,
        Lorg/apache/tools/ant/taskdefs/DependSet$HideMissingBasedir;,
        Lorg/apache/tools/ant/taskdefs/DependSet$Newest;,
        Lorg/apache/tools/ant/taskdefs/DependSet$Oldest;,
        Lorg/apache/tools/ant/taskdefs/DependSet$Xest;,
        Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;
    }
.end annotation


# static fields
.field private static final DATE_ASC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

.field private static final DATE_DESC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

.field private static final NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# instance fields
.field private sources:Lorg/apache/tools/ant/types/resources/Union;

.field private targets:Lorg/apache/tools/ant/types/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Not;

    new-instance v1, Lorg/apache/tools/ant/types/resources/selectors/Exists;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/selectors/Exists;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/selectors/Not;-><init>(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    new-instance v0, Lorg/apache/tools/ant/types/resources/comparators/Date;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/comparators/Date;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->DATE_ASC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    new-instance v0, Lorg/apache/tools/ant/types/resources/comparators/Reverse;

    sget-object v1, Lorg/apache/tools/ant/taskdefs/DependSet;->DATE_ASC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/comparators/Reverse;-><init>(Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->DATE_DESC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method static access$000()Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    return-object v0
.end method

.method static access$100()Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->DATE_ASC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    return-object v0
.end method

.method static access$300()Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/DependSet;->DATE_DESC:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    return-object v0
.end method

.method private logFuture(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    new-instance v1, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    invoke-virtual {v1, p2}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/Restrict;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Warning: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " modified in the future."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private uptodate(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/types/ResourceCollection;)Z
    .locals 11
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Lorg/apache/tools/ant/types/ResourceCollection;

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x3

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Date;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/selectors/Date;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Lorg/apache/tools/ant/types/resources/selectors/Date;->setMillis(J)V

    sget-object v5, Lorg/apache/tools/ant/types/TimeComparison;->AFTER:Lorg/apache/tools/ant/types/TimeComparison;

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/types/resources/selectors/Date;->setWhen(Lorg/apache/tools/ant/types/TimeComparison;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0, v5, v0}, Lorg/apache/tools/ant/taskdefs/DependSet;->logFuture(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    new-instance v5, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-direct {v5, v7, v10}, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/DependSet$1;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " nonexistent targets"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v9}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    :goto_0
    return v6

    :cond_0
    new-instance v5, Lorg/apache/tools/ant/taskdefs/DependSet$Oldest;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-direct {v5, v7, v10}, Lorg/apache/tools/ant/taskdefs/DependSet$Oldest;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/DependSet$1;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/DependSet$Oldest;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/resources/FileResource;

    move-object v4, v5

    check-cast v4, Lorg/apache/tools/ant/types/resources/FileResource;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " is oldest target file"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v9}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {p0, v5, v0}, Lorg/apache/tools/ant/taskdefs/DependSet;->logFuture(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    new-instance v5, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v5, v7, v10}, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/DependSet$1;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/DependSet$NonExistent;->size()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " nonexistent sources"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v9}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    new-instance v5, Lorg/apache/tools/ant/taskdefs/DependSet$Newest;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v5, v7, v10}, Lorg/apache/tools/ant/taskdefs/DependSet$Newest;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/DependSet$1;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/DependSet$Newest;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/Resource;

    move-object v3, v5

    check-cast v3, Lorg/apache/tools/ant/types/Resource;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Resource;->toLongString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " is newest source"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v9}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/FileResource;->getLastModified()J

    move-result-wide v7

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v9

    cmp-long v5, v7, v9

    if-ltz v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    move v6, v5

    goto/16 :goto_0

    :cond_2
    move v5, v6

    goto :goto_1
.end method


# virtual methods
.method public addSrcfilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/DependSet;->createSources()Lorg/apache/tools/ant/types/resources/Union;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addSrcfileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/DependSet;->createSources()Lorg/apache/tools/ant/types/resources/Union;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addTargetfilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/DependSet;->createTargets()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addTargetfileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/DependSet;->createTargets()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/DependSet$HideMissingBasedir;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/tools/ant/taskdefs/DependSet$HideMissingBasedir;-><init>(Lorg/apache/tools/ant/types/FileSet;Lorg/apache/tools/ant/taskdefs/DependSet$1;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public declared-synchronized createSources()Lorg/apache/tools/ant/types/resources/Union;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized createTargets()Lorg/apache/tools/ant/types/Path;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/DependSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public execute()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "At least one set of source resources must be specified"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "At least one set of target files must be specified"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->sources:Lorg/apache/tools/ant/types/resources/Union;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/DependSet;->uptodate(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/types/ResourceCollection;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Deleting all target files."

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/DependSet;->log(Ljava/lang/String;I)V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Delete;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Delete;-><init>()V

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/taskdefs/Delete;->bindToOwner(Lorg/apache/tools/ant/Task;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/DependSet;->targets:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Delete;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Delete;->perform()V

    :cond_2
    return-void
.end method
