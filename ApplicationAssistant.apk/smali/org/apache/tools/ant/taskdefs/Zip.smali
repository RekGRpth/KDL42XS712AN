.class public Lorg/apache/tools/ant/taskdefs/Zip;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Zip.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;,
        Lorg/apache/tools/ant/taskdefs/Zip$Duplicate;,
        Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;
    }
.end annotation


# static fields
.field private static final EMPTY_CRC:J

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field protected addedDirs:Ljava/util/Hashtable;

.field private addedFiles:Ljava/util/Vector;

.field private addingNewFiles:Z

.field protected archiveType:Ljava/lang/String;

.field private baseDir:Ljava/io/File;

.field private comment:Ljava/lang/String;

.field private doCompress:Z

.field private doFilesonly:Z

.field private doUpdate:Z

.field protected doubleFilePass:Z

.field protected duplicate:Ljava/lang/String;

.field protected emptyBehavior:Ljava/lang/String;

.field private encoding:Ljava/lang/String;

.field protected entries:Ljava/util/Hashtable;

.field private filesetsFromGroupfilesets:Ljava/util/Vector;

.field private groupfilesets:Ljava/util/Vector;

.field private keepCompression:Z

.field private level:I

.field private resources:Ljava/util/Vector;

.field private roundUp:Z

.field private savedDoUpdate:Z

.field protected skipWriting:Z

.field protected zipFile:Ljava/io/File;

.field private zs:Lorg/apache/tools/ant/types/ZipScanner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    sput-wide v0, Lorg/apache/tools/ant/taskdefs/Zip;->EMPTY_CRC:J

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->entries:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->filesetsFromGroupfilesets:Ljava/util/Vector;

    const-string v0, "add"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->duplicate:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->savedDoUpdate:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    const-string v0, "zip"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    const-string v0, "skip"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->emptyBehavior:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedDirs:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedFiles:Ljava/util/Vector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doubleFilePass:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addingNewFiles:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->keepCompression:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->roundUp:Z

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->comment:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->level:I

    return-void
.end method

.method private declared-synchronized getZipScanner()Lorg/apache/tools/ant/types/ZipScanner;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zs:Lorg/apache/tools/ant/types/ZipScanner;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/ZipScanner;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/ZipScanner;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zs:Lorg/apache/tools/ant/types/ZipScanner;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zs:Lorg/apache/tools/ant/types/ZipScanner;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipScanner;->setEncoding(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zs:Lorg/apache/tools/ant/types/ZipScanner;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipScanner;->setSrc(Ljava/io/File;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zs:Lorg/apache/tools/ant/types/ZipScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static final isEmpty([[Lorg/apache/tools/ant/types/Resource;)Z
    .locals 2
    .param p0    # [[Lorg/apache/tools/ant/types/Resource;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-object v1, p0, v0

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method protected final addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-nez v4, :cond_3

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    const/16 v4, 0x2f

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedDirs:Ljava/util/Hashtable;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, p3, v4, p5}, Lorg/apache/tools/ant/taskdefs/Zip;->zipDir(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method protected final addResources(Lorg/apache/tools/ant/types/FileSet;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 33
    .param p1    # Lorg/apache/tools/ant/types/FileSet;
    .param p2    # [Lorg/apache/tools/ant/types/Resource;
    .param p3    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v6, ""

    const-string v23, ""

    const/16 v9, 0x41ed

    const v22, 0x81a4

    const/16 v32, 0x0

    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    if-eqz v4, :cond_0

    move-object/from16 v32, p1

    check-cast v32, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getDirMode(Lorg/apache/tools/ant/Project;)I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFileMode(Lorg/apache/tools/ant/Project;)I

    move-result v22

    :cond_0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "Both prefix and fullpath attributes must not be set on the same fileset."

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    move-object/from16 v0, p2

    array-length v4, v0

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "fullpath attribute may only be specified for filesets that specify a single file."

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    const-string v4, "/"

    invoke-virtual {v6, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "\\"

    invoke-virtual {v6, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_3
    const/4 v5, 0x0

    const-string v8, ""

    move-object/from16 v4, p0

    move-object/from16 v7, p3

    invoke-virtual/range {v4 .. v9}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    :cond_4
    const/16 v30, 0x0

    const/16 v20, 0x0

    const/4 v11, 0x0

    if-eqz v32, :cond_5

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v4

    if-nez v4, :cond_8

    :cond_5
    const/16 v20, 0x1

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v11

    :cond_6
    :goto_0
    const/16 v24, 0x0

    :goto_1
    move-object/from16 v0, p2

    array-length v4, v0

    move/from16 v0, v24

    if-ge v0, v4, :cond_16

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_9

    move-object/from16 v25, v23

    :goto_2
    sget-char v4, Ljava/io/File;->separatorChar:C

    const/16 v5, 0x2f

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v25

    const-string v4, ""

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    :cond_7
    :goto_3
    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    :cond_8
    move-object/from16 v0, v32

    instance-of v4, v0, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v4, :cond_6

    new-instance v31, Lorg/apache/tools/zip/ZipFile;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-direct {v0, v4, v5}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v30, v31

    goto :goto_0

    :cond_9
    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v25

    goto :goto_2

    :cond_a
    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "/"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-nez v4, :cond_f

    if-nez v20, :cond_f

    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual/range {v32 .. v32}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDirModeBeenSet()Z

    move-result v4

    if-nez v4, :cond_f

    const-string v4, "/"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v26

    const/4 v4, -0x1

    move/from16 v0, v26

    if-eq v0, v4, :cond_c

    const/4 v4, 0x0

    add-int/lit8 v5, v26, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v10, p0

    move-object/from16 v13, p3

    move-object v14, v6

    move v15, v9

    invoke-virtual/range {v10 .. v15}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    :cond_c
    if-eqz v30, :cond_e

    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lorg/apache/tools/zip/ZipFile;->getEntry(Ljava/lang/String;)Lorg/apache/tools/zip/ZipEntry;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lorg/apache/tools/zip/ZipEntry;->getUnixMode()I

    move-result v15

    move-object/from16 v10, p0

    move-object/from16 v12, v25

    move-object/from16 v13, p3

    move-object v14, v6

    invoke-virtual/range {v10 .. v15}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    :goto_4
    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_10

    if-eqz v20, :cond_10

    sget-object v4, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    aget-object v5, p2, v24

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v11, v5}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v21

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p3

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v4, v3}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v4

    if-eqz v30, :cond_d

    invoke-virtual/range {v30 .. v30}, Lorg/apache/tools/zip/ZipFile;->close()V

    :cond_d
    throw v4

    :cond_e
    :try_start_1
    aget-object v28, p2, v24

    check-cast v28, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    invoke-virtual/range {v28 .. v28}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getMode()I

    move-result v15

    move-object/from16 v10, p0

    move-object/from16 v12, v25

    move-object/from16 v13, p3

    move-object v14, v6

    invoke-virtual/range {v10 .. v15}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    goto :goto_4

    :cond_f
    move-object/from16 v10, p0

    move-object/from16 v12, v25

    move-object/from16 v13, p3

    move-object v14, v6

    move v15, v9

    invoke-virtual/range {v10 .. v15}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    goto :goto_4

    :cond_10
    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v30, :cond_14

    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lorg/apache/tools/zip/ZipFile;->getEntry(Ljava/lang/String;)Lorg/apache/tools/zip/ZipEntry;

    move-result-object v29

    if-eqz v29, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/tools/ant/taskdefs/Zip;->keepCompression:Z

    if-eqz v4, :cond_11

    invoke-virtual/range {v29 .. v29}, Lorg/apache/tools/zip/ZipEntry;->getMethod()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_12

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_11
    :try_start_2
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipFile;->getInputStream(Lorg/apache/tools/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v13

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v29 .. v29}, Lorg/apache/tools/zip/ZipEntry;->getTime()J

    move-result-wide v16

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v18

    invoke-virtual/range {v32 .. v32}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasFileModeBeenSet()Z

    move-result v4

    if-eqz v4, :cond_13

    move/from16 v19, v22

    :goto_6
    move-object/from16 v12, p0

    move-object/from16 v14, p3

    invoke-virtual/range {v12 .. v19}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    :cond_12
    const/4 v4, 0x0

    goto :goto_5

    :cond_13
    :try_start_4
    invoke-virtual/range {v29 .. v29}, Lorg/apache/tools/zip/ZipEntry;->getUnixMode()I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v19

    goto :goto_6

    :catchall_1
    move-exception v4

    :try_start_5
    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    throw v4

    :cond_14
    aget-object v28, p2, v24

    check-cast v28, Lorg/apache/tools/ant/types/resources/ArchiveResource;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v13, 0x0

    :try_start_6
    invoke-virtual/range {v28 .. v28}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    aget-object v4, p2, v24

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v16

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v18

    invoke-virtual/range {v32 .. v32}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasFileModeBeenSet()Z

    move-result v4

    if-eqz v4, :cond_15

    move/from16 v19, v22

    :goto_7
    move-object/from16 v12, p0

    move-object/from16 v14, p3

    invoke-virtual/range {v12 .. v19}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-static {v13}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    :cond_15
    :try_start_8
    invoke-virtual/range {v28 .. v28}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getMode()I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result v19

    goto :goto_7

    :catchall_2
    move-exception v4

    :try_start_9
    invoke-static {v13}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_16
    if-eqz v30, :cond_17

    invoke-virtual/range {v30 .. v30}, Lorg/apache/tools/zip/ZipFile;->close()V

    :cond_17
    return-void
.end method

.method protected final addResources(Lorg/apache/tools/ant/types/ResourceCollection;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 14
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # [Lorg/apache/tools/ant/types/Resource;
    .param p3    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    instance-of v1, p1, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v1, :cond_1

    check-cast p1, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual/range {p0 .. p3}, Lorg/apache/tools/ant/taskdefs/Zip;->addResources(Lorg/apache/tools/ant/types/FileSet;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/zip/ZipOutputStream;)V

    :cond_0
    return-void

    :cond_1
    const/4 v13, 0x0

    :goto_0
    move-object/from16 v0, p2

    array-length v1, v0

    if-ge v13, v1, :cond_0

    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v1

    sget-char v4, Ljava/io/File;->separatorChar:C

    const/16 v6, 0x2f

    invoke-virtual {v1, v4, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    const-string v1, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_3
    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-nez v1, :cond_2

    :cond_4
    const/4 v2, 0x0

    aget-object v1, p2, v13

    instance-of v1, v1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v1, :cond_5

    aget-object v1, p2, v13

    check-cast v1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v2

    :cond_5
    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "/"

    invoke-virtual {v3, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_6
    const-string v5, ""

    const/16 v6, 0x41ed

    move-object v1, p0

    move-object/from16 v4, p3

    invoke-virtual/range {v1 .. v6}, Lorg/apache/tools/ant/taskdefs/Zip;->addParentDirs(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    aget-object v1, p2, v13

    instance-of v1, v1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v1, :cond_7

    aget-object v1, p2, v13

    check-cast v1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v12

    const v1, 0x81a4

    move-object/from16 v0, p3

    invoke-virtual {p0, v12, v0, v3, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    goto :goto_1

    :cond_7
    const/4 v5, 0x0

    :try_start_0
    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    aget-object v1, p2, v13

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v8

    const/4 v10, 0x0

    const v11, 0x81a4

    move-object v4, p0

    move-object/from16 v6, p3

    move-object v7, v3

    invoke-virtual/range {v4 .. v11}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v1
.end method

.method public addZipGroupFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addZipfileset(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method protected cleanUp()V
    .locals 3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedDirs:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedFiles:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->entries:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addingNewFiles:Z

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->savedDoUpdate:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->filesetsFromGroupfilesets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ZipFileSet;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->filesetsFromGroupfilesets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    return-void
.end method

.method protected createEmptyZip(Ljava/io/File;)Z
    .locals 8
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x2

    const/4 v6, 0x1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Note: creating empty "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " archive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v7}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0x16

    :try_start_1
    new-array v0, v4, [B

    const/4 v4, 0x0

    const/16 v5, 0x50

    aput-byte v5, v0, v4

    const/4 v4, 0x1

    const/16 v5, 0x4b

    aput-byte v5, v0, v4

    const/4 v4, 0x2

    const/4 v5, 0x5

    aput-byte v5, v0, v4

    const/4 v4, 0x3

    const/4 v5, 0x6

    aput-byte v5, v0, v4

    invoke-virtual {v3, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_0
    return v6

    :catch_0
    move-exception v1

    :goto_1
    :try_start_3
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Could not create empty ZIP archive ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    invoke-direct {v4, v5, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_1

    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_3
    throw v4

    :catch_1
    move-exception v4

    goto :goto_0

    :catch_2
    move-exception v5

    goto :goto_3

    :catchall_1
    move-exception v4

    move-object v2, v3

    goto :goto_2

    :catch_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public execute()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doubleFilePass:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->executeMain()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->executeMain()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->executeMain()V

    goto :goto_0
.end method

.method public executeMain()V
    .locals 38
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->baseDir:Ljava/io/File;

    move-object/from16 v34, v0

    if-nez v34, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/util/Vector;->size()I

    move-result v34

    if-nez v34, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/util/Vector;->size()I

    move-result v34

    if-nez v34, :cond_0

    const-string v34, "zip"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_0

    new-instance v34, Lorg/apache/tools/ant/BuildException;

    const-string v35, "basedir attribute must be set, or at least one resource collection must be given!"

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    if-nez v34, :cond_1

    new-instance v34, Lorg/apache/tools/ant/BuildException;

    new-instance v35, Ljava/lang/StringBuffer;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuffer;-><init>()V

    const-string v36, "You must specify the "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, " file to create!"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-eqz v34, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->isFile()Z

    move-result v34

    if-nez v34, :cond_2

    new-instance v34, Lorg/apache/tools/ant/BuildException;

    new-instance v35, Ljava/lang/StringBuffer;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, " is not a file."

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-eqz v34, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->canWrite()Z

    move-result v34

    if-nez v34, :cond_3

    new-instance v34, Lorg/apache/tools/ant/BuildException;

    new-instance v35, Ljava/lang/StringBuffer;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, " is read-only."

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34

    :cond_3
    const/16 v25, 0x0

    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Zip;->addingNewFiles:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-nez v34, :cond_4

    const/16 v34, 0x0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    const-string v35, "ignoring update attribute as "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, " doesn\'t exist."

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    :cond_4
    const/16 v17, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/util/Vector;->size()I

    move-result v34

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_6

    const-string v34, "Processing groupfileset "

    const/16 v35, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/apache/tools/ant/FileScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v14

    invoke-interface/range {v26 .. v26}, Lorg/apache/tools/ant/FileScanner;->getBasedir()Ljava/io/File;

    move-result-object v7

    const/16 v19, 0x0

    :goto_1
    array-length v0, v14

    move/from16 v34, v0

    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_5

    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    const-string v35, "Adding file "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    aget-object v35, v14, v19

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, " to fileset"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    new-instance v33, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-direct/range {v33 .. v33}, Lorg/apache/tools/ant/types/ZipFileSet;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lorg/apache/tools/ant/types/ZipFileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    new-instance v34, Ljava/io/File;

    aget-object v35, v14, v19

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-direct {v0, v7, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v34}, Lorg/apache/tools/ant/types/ZipFileSet;->setSrc(Ljava/io/File;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->filesetsFromGroupfilesets:Ljava/util/Vector;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    :cond_6
    new-instance v30, Ljava/util/Vector;

    invoke-direct/range {v30 .. v30}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->baseDir:Ljava/io/File;

    move-object/from16 v34, v0

    if-eqz v34, :cond_7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getImplicitFileSet()Lorg/apache/tools/ant/types/FileSet;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lorg/apache/tools/ant/types/FileSet;->clone()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/tools/ant/types/FileSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->baseDir:Ljava/io/File;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_7
    const/16 v17, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/util/Vector;->size()I

    move-result v34

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/tools/ant/types/ResourceCollection;

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_8
    invoke-virtual/range {v30 .. v30}, Ljava/util/Vector;->size()I

    move-result v34

    move/from16 v0, v34

    new-array v0, v0, [Lorg/apache/tools/ant/types/ResourceCollection;

    move-object/from16 v16, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    const/16 v28, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, v34

    move/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Zip;->getResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isOutOfDate()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v34

    if-nez v34, :cond_9

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->cleanUp()V

    :goto_3
    return-void

    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-nez v34, :cond_a

    invoke-virtual/range {v27 .. v27}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isWithoutAnyResources()Z

    move-result v34

    if-eqz v34, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->createEmptyZip(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->cleanUp()V

    goto :goto_3

    :cond_a
    :try_start_2
    invoke-virtual/range {v27 .. v27}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->getResourcesToAdd()[[Lorg/apache/tools/ant/types/Resource;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_b

    sget-object v34, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v35, "zip"

    const-string v36, ".tmp"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v37

    invoke-virtual/range {v34 .. v37}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->deleteOnExit()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    sget-object v34, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v35, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/util/FileUtils;->rename(Ljava/io/File;Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_b
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_11

    const-string v5, "Updating "

    :goto_4
    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, ": "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/16 v31, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    move/from16 v34, v0

    if-nez v34, :cond_c

    new-instance v32, Lorg/apache/tools/zip/ZipOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lorg/apache/tools/zip/ZipOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipOutputStream;->setEncoding(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    move/from16 v34, v0

    if-eqz v34, :cond_12

    const/16 v34, 0x8

    :goto_5
    move-object/from16 v0, v32

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipOutputStream;->setMethod(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->level:I

    move/from16 v34, v0

    move-object/from16 v0, v32

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipOutputStream;->setLevel(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-object/from16 v31, v32

    :cond_c
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V

    const/16 v17, 0x0

    :goto_6
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v34, v0

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_13

    aget-object v34, v6, v17

    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v34, v0

    if-eqz v34, :cond_d

    aget-object v34, v16, v17

    aget-object v35, v6, v17

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Zip;->addResources(Lorg/apache/tools/ant/types/ResourceCollection;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/zip/ZipOutputStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_d
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    :catch_0
    move-exception v11

    :try_start_8
    new-instance v34, Lorg/apache/tools/ant/BuildException;

    new-instance v35, Ljava/lang/StringBuffer;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuffer;-><init>()V

    const-string v36, "Not allowed to rename old file ("

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, ") to temporary file"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_1
    move-exception v18

    :try_start_9
    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    const-string v35, "Problem creating "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, ": "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_e

    if-eqz v25, :cond_f

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->delete()Z

    move-result v34

    if-nez v34, :cond_f

    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, " (and the archive is probably corrupt but I could not delete it)"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v34, :cond_10

    if-eqz v25, :cond_10

    :try_start_a
    sget-object v34, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v35, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v25

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/util/FileUtils;->rename(Ljava/io/File;Ljava/io/File;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_10
    :goto_7
    :try_start_b
    new-instance v34, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v35

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    move-object/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v34
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v34

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->cleanUp()V

    throw v34

    :catch_2
    move-exception v11

    :try_start_c
    new-instance v34, Lorg/apache/tools/ant/BuildException;

    new-instance v35, Ljava/lang/StringBuffer;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuffer;-><init>()V

    const-string v36, "Unable to rename old file ("

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, ") to temporary file"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v34

    :cond_11
    const-string v5, "Building "
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_4

    :cond_12
    const/16 v34, 0x0

    goto/16 :goto_5

    :cond_13
    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_18

    const/16 v34, 0x0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Zip;->addingNewFiles:Z

    new-instance v22, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-direct/range {v22 .. v22}, Lorg/apache/tools/ant/types/ZipFileSet;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v34

    move-object/from16 v0, v22

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->setSrc(Ljava/io/File;)V

    const/16 v34, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->setDefaultexcludes(Z)V

    const/16 v17, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->addedFiles:Ljava/util/Vector;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/util/Vector;->size()I

    move-result v34

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_14

    invoke-virtual/range {v22 .. v22}, Lorg/apache/tools/ant/types/ZipFileSet;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->addedFiles:Ljava/util/Vector;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    add-int/lit8 v17, v17, 0x1

    goto :goto_8

    :cond_14
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v34

    move-object/from16 v0, v22

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lorg/apache/tools/ant/types/ZipScanner;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Lorg/apache/tools/ant/types/ZipScanner;->setEncoding(Ljava/lang/String;)V

    invoke-virtual {v10}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v13

    array-length v0, v13

    move/from16 v34, v0

    move/from16 v0, v34

    new-array v0, v0, [Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v23, v0

    const/16 v17, 0x0

    :goto_9
    array-length v0, v13

    move/from16 v34, v0

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_15

    aget-object v34, v13, v17

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/DirectoryScanner;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v34

    aput-object v34, v23, v17

    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    move/from16 v34, v0

    if-nez v34, :cond_17

    invoke-virtual {v10}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v8

    array-length v0, v8

    move/from16 v34, v0

    move/from16 v0, v34

    new-array v9, v0, [Lorg/apache/tools/ant/types/Resource;

    const/16 v17, 0x0

    :goto_a
    array-length v0, v8

    move/from16 v34, v0

    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_16

    aget-object v34, v8, v17

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/DirectoryScanner;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v34

    aput-object v34, v9, v17

    add-int/lit8 v17, v17, 0x1

    goto :goto_a

    :cond_16
    move-object/from16 v29, v23

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v34, v0

    array-length v0, v9

    move/from16 v35, v0

    add-int v34, v34, v35

    move/from16 v0, v34

    new-array v0, v0, [Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v23, v0

    const/16 v34, 0x0

    const/16 v35, 0x0

    array-length v0, v9

    move/from16 v36, v0

    move/from16 v0, v34

    move-object/from16 v1, v23

    move/from16 v2, v35

    move/from16 v3, v36

    invoke-static {v9, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v34, 0x0

    array-length v0, v9

    move/from16 v35, v0

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v29

    move/from16 v1, v34

    move-object/from16 v2, v23

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_17
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Zip;->addResources(Lorg/apache/tools/ant/types/FileSet;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/zip/ZipOutputStream;)V

    :cond_18
    if-eqz v31, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->comment:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/zip/ZipOutputStream;->setComment(Ljava/lang/String;)V

    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Zip;->finalizeZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    move/from16 v34, v0

    if-eqz v34, :cond_1a

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->delete()Z

    move-result v34

    if-nez v34, :cond_1a

    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    const-string v35, "Warning: unable to delete temporary file "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :cond_1a
    const/16 v28, 0x1

    if-eqz v31, :cond_1b

    :try_start_e
    invoke-virtual/range {v31 .. v31}, Lorg/apache/tools/zip/ZipOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->cleanUp()V

    goto/16 :goto_3

    :catch_3
    move-exception v12

    if-eqz v28, :cond_1b

    :try_start_f
    throw v12
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_1
    move-exception v34

    :goto_b
    if-eqz v31, :cond_1c

    :try_start_10
    invoke-virtual/range {v31 .. v31}, Lorg/apache/tools/zip/ZipOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :cond_1c
    :try_start_11
    throw v34

    :catch_4
    move-exception v12

    if-eqz v28, :cond_1c

    throw v12
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_5
    move-exception v11

    :try_start_12
    new-instance v34, Ljava/lang/StringBuffer;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, " (and I couldn\'t rename the temporary file "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    const-string v35, " back)"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-object v20

    goto/16 :goto_7

    :catchall_2
    move-exception v34

    move-object/from16 v31, v32

    goto :goto_b
.end method

.method protected finalizeZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 0
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    return-void
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getDestFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->level:I

    return v0
.end method

.method protected getNonFileSetResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;
    .locals 9
    .param p1    # [Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->grabNonFileSetResources([Lorg/apache/tools/ant/types/ResourceCollection;)[[Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Zip;->isEmpty([[Lorg/apache/tools/ant/types/Resource;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-direct {v5, p3, v1}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-direct {v5, v7, v1}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v5, :cond_2

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-direct {v5, v7, v1}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto :goto_0

    :cond_2
    array-length v5, p1

    new-array v3, v5, [[Lorg/apache/tools/ant/types/Resource;

    const/4 v0, 0x0

    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_9

    aget-object v5, v1, v0

    array-length v5, v5

    if-nez v5, :cond_4

    new-array v5, v6, [Lorg/apache/tools/ant/types/Resource;

    aput-object v5, v3, v0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_2
    aget-object v5, v1, v0

    array-length v5, v5

    if-ge v2, v5, :cond_6

    aget-object v5, v1, v0

    aget-object v5, v5, v2

    instance-of v5, v5, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v5, :cond_5

    aget-object v5, v1, v0

    aget-object v5, v5, v2

    check-cast v5, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "A zip file cannot include itself"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v5

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    aget-object v4, v1, v0

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-eqz v5, :cond_7

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/Zip;->selectFileResources([Lorg/apache/tools/ant/types/Resource;)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v4

    :cond_7
    new-instance v5, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v5}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getZipScanner()Lorg/apache/tools/ant/types/ZipScanner;

    move-result-object v8

    invoke-static {p0, v4, v5, v8}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v5

    aput-object v5, v3, v0

    if-nez p3, :cond_8

    aget-object v5, v3, v0

    array-length v5, v5

    if-lez v5, :cond_a

    :cond_8
    move p3, v7

    :goto_3
    if-eqz p3, :cond_3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v5, :cond_3

    :cond_9
    if-eqz p3, :cond_b

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v5, :cond_b

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-direct {v5, v7, v1}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0

    :cond_a
    move p3, v6

    goto :goto_3

    :cond_b
    new-instance v5, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-direct {v5, p3, v3}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0
.end method

.method protected getResourcesToAdd([Lorg/apache/tools/ant/types/FileSet;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;
    .locals 16
    .param p1    # [Lorg/apache/tools/ant/types/FileSet;
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual/range {p0 .. p1}, Lorg/apache/tools/ant/taskdefs/Zip;->grabResources([Lorg/apache/tools/ant/types/FileSet;)[[Lorg/apache/tools/ant/types/Resource;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/Zip;->isEmpty([[Lorg/apache/tools/ant/types/Resource;)Z

    move-result v13

    if-eqz v13, :cond_5

    if-eqz p3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-eqz v13, :cond_0

    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    const/4 v14, 0x1

    invoke-direct {v13, v14, v5}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    :goto_0
    return-object v13

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->emptyBehavior:Ljava/lang/String;

    const-string v14, "skip"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-eqz v13, :cond_2

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " archive "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " not updated because no new files were included."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_1
    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move/from16 v0, p3

    invoke-direct {v13, v0, v5}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto :goto_0

    :cond_2
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Warning: skipping "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " archive "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " because no files were included."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->emptyBehavior:Ljava/lang/String;

    const-string v14, "fail"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    new-instance v13, Lorg/apache/tools/ant/BuildException;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Cannot create "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " archive "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, ": no files were included."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v13

    :cond_4
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_1

    const/16 p3, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_6

    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    const/4 v14, 0x1

    invoke-direct {v13, v14, v5}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0

    :cond_6
    if-eqz p3, :cond_7

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v13, :cond_7

    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    const/4 v14, 0x1

    invoke-direct {v13, v14, v5}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p1

    array-length v13, v0

    new-array v8, v13, [[Lorg/apache/tools/ant/types/Resource;

    const/4 v4, 0x0

    :goto_2
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v4, v13, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->fileset:Lorg/apache/tools/ant/types/FileSet;

    instance-of v13, v13, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->fileset:Lorg/apache/tools/ant/types/FileSet;

    check-cast v13, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v14

    invoke-virtual {v13, v14}, Lorg/apache/tools/ant/types/ZipFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v13

    if-nez v13, :cond_a

    :cond_8
    aget-object v13, p1, v4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v14

    invoke-virtual {v13, v14}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v1

    const/4 v6, 0x0

    :goto_3
    aget-object v13, v5, v4

    array-length v13, v13

    if-ge v6, v13, :cond_a

    sget-object v13, Lorg/apache/tools/ant/taskdefs/Zip;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    aget-object v14, v5, v4

    aget-object v14, v14, v6

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v1, v14}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    new-instance v13, Lorg/apache/tools/ant/BuildException;

    const-string v14, "A zip file cannot include itself"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v13

    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_b
    const/4 v4, 0x0

    :goto_4
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v4, v13, :cond_11

    aget-object v13, v5, v4

    array-length v13, v13

    if-nez v13, :cond_d

    const/4 v13, 0x0

    new-array v13, v13, [Lorg/apache/tools/ant/types/Resource;

    aput-object v13, v8, v4

    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_d
    new-instance v7, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v7}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    aget-object v13, p1, v4

    instance-of v13, v13, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v13, :cond_e

    aget-object v12, p1, v4

    check-cast v12, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_12

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_12

    new-instance v2, Lorg/apache/tools/ant/util/MergingMapper;

    invoke-direct {v2}, Lorg/apache/tools/ant/util/MergingMapper;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Lorg/apache/tools/ant/util/MergingMapper;->setTo(Ljava/lang/String;)V

    move-object v7, v2

    :cond_e
    :goto_5
    aget-object v11, v5, v4

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-eqz v13, :cond_f

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lorg/apache/tools/ant/taskdefs/Zip;->selectFileResources([Lorg/apache/tools/ant/types/Resource;)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v11

    :cond_f
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getZipScanner()Lorg/apache/tools/ant/types/ZipScanner;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v11, v7, v13}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v13

    aput-object v13, v8, v4

    if-nez p3, :cond_10

    aget-object v13, v8, v4

    array-length v13, v13

    if-lez v13, :cond_14

    :cond_10
    const/16 p3, 0x1

    :goto_6
    if-eqz p3, :cond_c

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v13, :cond_c

    :cond_11
    if-eqz p3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    if-nez v13, :cond_15

    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    const/4 v14, 0x1

    invoke-direct {v13, v14, v5}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_e

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_e

    new-instance v3, Lorg/apache/tools/ant/util/GlobPatternMapper;

    invoke-direct {v3}, Lorg/apache/tools/ant/util/GlobPatternMapper;-><init>()V

    const-string v13, "*"

    invoke-virtual {v3, v13}, Lorg/apache/tools/ant/util/GlobPatternMapper;->setFrom(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v12, v13}, Lorg/apache/tools/ant/types/ZipFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v9

    const-string v13, "/"

    invoke-virtual {v9, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_13

    const-string v13, "\\"

    invoke-virtual {v9, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_13

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    :cond_13
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, "*"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lorg/apache/tools/ant/util/GlobPatternMapper;->setTo(Ljava/lang/String;)V

    move-object v7, v3

    goto/16 :goto_5

    :cond_14
    const/16 p3, 0x0

    goto/16 :goto_6

    :cond_15
    new-instance v13, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move/from16 v0, p3

    invoke-direct {v13, v0, v8}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    goto/16 :goto_0
.end method

.method protected getResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;
    .locals 17
    .param p1    # [Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v15, v0

    if-ge v9, v15, :cond_1

    aget-object v15, p1, v9

    instance-of v15, v15, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v15, :cond_0

    aget-object v15, p1, v9

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    aget-object v15, p1, v9

    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v15

    new-array v15, v15, [Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Lorg/apache/tools/ant/types/ResourceCollection;

    move-object v10, v15

    check-cast v10, [Lorg/apache/tools/ant/types/ResourceCollection;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v10, v1, v2}, Lorg/apache/tools/ant/taskdefs/Zip;->getNonFileSetResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move-result-object v3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v15

    new-array v15, v15, [Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Lorg/apache/tools/ant/types/FileSet;

    move-object v6, v15

    check-cast v6, [Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isOutOfDate()Z

    move-result v15

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v6, v1, v15}, Lorg/apache/tools/ant/taskdefs/Zip;->getResourcesToAdd([Lorg/apache/tools/ant/types/FileSet;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isOutOfDate()Z

    move-result v15

    if-nez v15, :cond_2

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isOutOfDate()Z

    move-result v15

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v10, v1, v15}, Lorg/apache/tools/ant/taskdefs/Zip;->getNonFileSetResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move-result-object v3

    :cond_2
    move-object/from16 v0, p1

    array-length v15, v0

    new-array v14, v15, [[Lorg/apache/tools/ant/types/Resource;

    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x0

    :goto_2
    move-object/from16 v0, p1

    array-length v15, v0

    if-ge v9, v15, :cond_4

    aget-object v15, p1, v9

    instance-of v15, v15, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v15, :cond_3

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->getResourcesToAdd()[[Lorg/apache/tools/ant/types/Resource;

    move-result-object v15

    add-int/lit8 v8, v7, 0x1

    aget-object v15, v15, v7

    aput-object v15, v14, v9

    move v7, v8

    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->getResourcesToAdd()[[Lorg/apache/tools/ant/types/Resource;

    move-result-object v15

    add-int/lit8 v13, v12, 0x1

    aget-object v15, v15, v12

    aput-object v15, v14, v9

    move v12, v13

    goto :goto_3

    :cond_4
    new-instance v15, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;->isOutOfDate()Z

    move-result v16

    move/from16 v0, v16

    invoke-direct {v15, v0, v14}, Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;-><init>(Z[[Lorg/apache/tools/ant/types/Resource;)V

    return-object v15
.end method

.method protected grabNonFileSetResources([Lorg/apache/tools/ant/types/ResourceCollection;)[[Lorg/apache/tools/ant/types/Resource;
    .locals 8
    .param p1    # [Lorg/apache/tools/ant/types/ResourceCollection;

    array-length v7, p1

    new-array v5, v7, [[Lorg/apache/tools/ant/types/Resource;

    const/4 v0, 0x0

    :goto_0
    array-length v7, p1

    if-ge v0, v7, :cond_3

    aget-object v7, p1, v0

    invoke-interface {v7}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v6, v2, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v2, v3

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/tools/ant/types/Resource;

    check-cast v7, [Lorg/apache/tools/ant/types/Resource;

    aput-object v7, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-object v5
.end method

.method protected grabResources([Lorg/apache/tools/ant/types/FileSet;)[[Lorg/apache/tools/ant/types/Resource;
    .locals 11
    .param p1    # [Lorg/apache/tools/ant/types/FileSet;

    array-length v9, p1

    new-array v5, v9, [[Lorg/apache/tools/ant/types/Resource;

    const/4 v2, 0x0

    :goto_0
    array-length v9, p1

    if-ge v2, v9, :cond_9

    const/4 v7, 0x1

    aget-object v9, p1, v2

    instance-of v9, v9, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v9, :cond_0

    aget-object v8, p1, v2

    check-cast v8, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/types/ZipFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/types/ZipFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v7, 0x1

    :cond_0
    :goto_1
    aget-object v9, p1, v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v6

    instance-of v9, v6, Lorg/apache/tools/ant/types/ZipScanner;

    if-eqz v9, :cond_1

    move-object v9, v6

    check-cast v9, Lorg/apache/tools/ant/types/ZipScanner;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lorg/apache/tools/ant/types/ZipScanner;->setEncoding(Ljava/lang/String;)V

    :cond_1
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-nez v9, :cond_5

    invoke-virtual {v6}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    :goto_2
    array-length v9, v0

    if-ge v3, v9, :cond_5

    const-string v9, ""

    aget-object v10, v0, v3

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-nez v7, :cond_3

    :cond_2
    aget-object v9, v0, v3

    invoke-virtual {v6, v9}, Lorg/apache/tools/ant/DirectoryScanner;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    :cond_5
    invoke-virtual {v6}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    :goto_3
    array-length v9, v1

    if-ge v3, v9, :cond_8

    const-string v9, ""

    aget-object v10, v1, v3

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    if-nez v7, :cond_7

    :cond_6
    aget-object v9, v1, v3

    invoke-virtual {v6, v9}, Lorg/apache/tools/ant/DirectoryScanner;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/tools/ant/types/Resource;

    aput-object v9, v5, v2

    aget-object v9, v5, v2

    invoke-virtual {v4, v9}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_9
    return-object v5
.end method

.method protected initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 0
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    return-void
.end method

.method protected final isAddingNewFiles()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addingNewFiles:Z

    return v0
.end method

.method public isCompress()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    return v0
.end method

.method public isInUpdateMode()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    return v0
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->resources:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->baseDir:Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->groupfilesets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    const-string v0, "add"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->duplicate:Ljava/lang/String;

    const-string v0, "zip"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->archiveType:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    const-string v0, "skip"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->emptyBehavior:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    return-void
.end method

.method protected selectFileResources([Lorg/apache/tools/ant/types/Resource;)[Lorg/apache/tools/ant/types/Resource;
    .locals 5
    .param p1    # [Lorg/apache/tools/ant/types/Resource;

    array-length v3, p1

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    new-instance v2, Ljava/util/Vector;

    array-length v3, p1

    invoke-direct {v2, v3}, Ljava/util/Vector;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_3

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Ignoring directory "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " as only files will be added."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v3

    array-length v4, p1

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v1, v3, [Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    move-object p1, v1

    goto :goto_0
.end method

.method public setBasedir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->baseDir:Ljava/io/File;

    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->comment:Ljava/lang/String;

    return-void
.end method

.method public setCompress(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    return-void
.end method

.method public setDestFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    return-void
.end method

.method public setDuplicate(Lorg/apache/tools/ant/taskdefs/Zip$Duplicate;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Zip$Duplicate;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Zip$Duplicate;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->duplicate:Ljava/lang/String;

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->setDestFile(Ljava/io/File;)V

    return-void
.end method

.method public setFilesonly(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    return-void
.end method

.method public setKeepCompression(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->keepCompression:Z

    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->level:I

    return-void
.end method

.method public setRoundUp(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->roundUp:Z

    return-void
.end method

.method public setUpdate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doUpdate:Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Zip;->savedDoUpdate:Z

    return-void
.end method

.method public setWhenempty(Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->emptyBehavior:Ljava/lang/String;

    return-void
.end method

.method public setZipfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->setDestFile(Ljava/io/File;)V

    return-void
.end method

.method protected zipDir(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/taskdefs/Zip;->zipDir(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I[Lorg/apache/tools/zip/ZipExtraField;)V

    return-void
.end method

.method protected zipDir(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I[Lorg/apache/tools/zip/ZipExtraField;)V
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # [Lorg/apache/tools/zip/ZipExtraField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v1, 0x7cf

    const/4 v5, 0x3

    const/4 v2, 0x0

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doFilesonly:Z

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "skipping directory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " for file-only archive"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v5}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedDirs:Ljava/util/Hashtable;

    invoke-virtual {v3, p3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "adding directory "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedDirs:Ljava/util/Hashtable;

    invoke-virtual {v3, p3, p3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    if-nez v3, :cond_0

    new-instance v0, Lorg/apache/tools/zip/ZipEntry;

    invoke-direct {v0, p3}, Lorg/apache/tools/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->roundUp:Z

    if-eqz v5, :cond_3

    :goto_1
    int-to-long v5, v1

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lorg/apache/tools/zip/ZipEntry;->setTime(J)V

    :goto_2
    const-wide/16 v3, 0x0

    invoke-virtual {v0, v3, v4}, Lorg/apache/tools/zip/ZipEntry;->setSize(J)V

    invoke-virtual {v0, v2}, Lorg/apache/tools/zip/ZipEntry;->setMethod(I)V

    sget-wide v1, Lorg/apache/tools/ant/taskdefs/Zip;->EMPTY_CRC:J

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/zip/ZipEntry;->setCrc(J)V

    invoke-virtual {v0, p4}, Lorg/apache/tools/zip/ZipEntry;->setUnixMode(I)V

    if-eqz p5, :cond_2

    invoke-virtual {v0, p5}, Lorg/apache/tools/zip/ZipEntry;->setExtraFields([Lorg/apache/tools/zip/ZipExtraField;)V

    :cond_2
    invoke-virtual {p2, v0}, Lorg/apache/tools/zip/ZipOutputStream;->putNextEntry(Lorg/apache/tools/zip/ZipEntry;)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Zip;->roundUp:Z

    if-eqz v5, :cond_5

    :goto_3
    int-to-long v5, v1

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lorg/apache/tools/zip/ZipEntry;->setTime(J)V

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3
.end method

.method protected zipFile(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V
    .locals 8
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v2, "A zip file cannot include itself"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Zip;->roundUp:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x7cf

    :goto_0
    int-to-long v4, v0

    add-long/2addr v4, v2

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    throw v0
.end method

.method protected zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    .locals 12
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/io/File;
    .param p7    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->entries:Ljava/util/Hashtable;

    invoke-virtual {v9, p3}, Ljava/util/Hashtable;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->duplicate:Ljava/lang/String;

    const-string v10, "preserve"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " already added, skipping"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->duplicate:Ljava/lang/String;

    const-string v10, "fail"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Duplicate file "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " was found and the duplicate "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "attribute is \'fail\'."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_1
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "duplicate file "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " found, adding."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    :goto_1
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->entries:Ljava/util/Hashtable;

    invoke-virtual {v9, p3, p3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->skipWriting:Z

    if-nez v9, :cond_6

    new-instance v8, Lorg/apache/tools/zip/ZipEntry;

    invoke-direct {v8, p3}, Lorg/apache/tools/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p4

    invoke-virtual {v8, v0, v1}, Lorg/apache/tools/zip/ZipEntry;->setTime(J)V

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    if-eqz v9, :cond_8

    const/16 v9, 0x8

    :goto_2
    invoke-virtual {v8, v9}, Lorg/apache/tools/zip/ZipEntry;->setMethod(I)V

    invoke-virtual {p2}, Lorg/apache/tools/zip/ZipOutputStream;->isSeekable()Z

    move-result v9

    if-nez v9, :cond_3

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->doCompress:Z

    if-nez v9, :cond_3

    const-wide/16 v6, 0x0

    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v9

    if-nez v9, :cond_9

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v9, 0x2000

    new-array v3, v9, [B

    const/4 v5, 0x0

    :cond_2
    int-to-long v9, v5

    add-long/2addr v6, v9

    const/4 v9, 0x0

    invoke-virtual {v4, v3, v9, v5}, Ljava/util/zip/CRC32;->update([BII)V

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    const/4 v9, 0x0

    array-length v10, v3

    invoke-virtual {p1, v3, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/4 v9, -0x1

    if-ne v5, v9, :cond_2

    new-instance p1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-direct {p1, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_3
    invoke-virtual {v8, v6, v7}, Lorg/apache/tools/zip/ZipEntry;->setSize(J)V

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lorg/apache/tools/zip/ZipEntry;->setCrc(J)V

    :cond_3
    move/from16 v0, p7

    invoke-virtual {v8, v0}, Lorg/apache/tools/zip/ZipEntry;->setUnixMode(I)V

    invoke-virtual {p2, v8}, Lorg/apache/tools/zip/ZipOutputStream;->putNextEntry(Lorg/apache/tools/zip/ZipEntry;)V

    const/16 v9, 0x2000

    new-array v3, v9, [B

    const/4 v5, 0x0

    :cond_4
    if-eqz v5, :cond_5

    const/4 v9, 0x0

    invoke-virtual {p2, v3, v9, v5}, Lorg/apache/tools/zip/ZipOutputStream;->write([BII)V

    :cond_5
    const/4 v9, 0x0

    array-length v10, v3

    invoke-virtual {p1, v3, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/4 v9, -0x1

    if-ne v5, v9, :cond_4

    :cond_6
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Zip;->addedFiles:Ljava/util/Vector;

    invoke-virtual {v9, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "adding entry "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Zip;->log(Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_2

    :cond_9
    const v9, 0x7fffffff

    invoke-virtual {p1, v9}, Ljava/io/InputStream;->mark(I)V

    const/16 v9, 0x2000

    new-array v3, v9, [B

    const/4 v5, 0x0

    :cond_a
    int-to-long v9, v5

    add-long/2addr v6, v9

    const/4 v9, 0x0

    invoke-virtual {v4, v3, v9, v5}, Ljava/util/zip/CRC32;->update([BII)V

    const/4 v9, 0x0

    array-length v10, v3

    invoke-virtual {p1, v3, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/4 v9, -0x1

    if-ne v5, v9, :cond_a

    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    goto :goto_3
.end method
