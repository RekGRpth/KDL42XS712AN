.class public Lorg/apache/tools/ant/taskdefs/Javadoc;
.super Lorg/apache/tools/ant/Task;
.source "Javadoc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$AccessType;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$Html;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;,
        Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field static final SCOPE_ELEMENTS:[Ljava/lang/String;


# instance fields
.field private author:Z

.field private bootclasspath:Lorg/apache/tools/ant/types/Path;

.field private bottom:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

.field private breakiterator:Z

.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private cmd:Lorg/apache/tools/ant/types/Commandline;

.field private destDir:Ljava/io/File;

.field private doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

.field private doctitle:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

.field private excludePackageNames:Ljava/util/Vector;

.field private executable:Ljava/lang/String;

.field private failOnError:Z

.field private footer:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

.field private group:Ljava/lang/String;

.field private groups:Ljava/util/Vector;

.field private header:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

.field private includeNoSourcePackages:Z

.field private links:Ljava/util/Vector;

.field private linksource:Z

.field private nestedSourceFiles:Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

.field private noqualifier:Ljava/lang/String;

.field private old:Z

.field private packageList:Ljava/lang/String;

.field private packageNames:Ljava/util/Vector;

.field private packageSets:Ljava/util/Vector;

.field private source:Ljava/lang/String;

.field private sourceFiles:Ljava/util/Vector;

.field private sourcePath:Lorg/apache/tools/ant/types/Path;

.field private tags:Ljava/util/Vector;

.field private useDefaultExcludes:Z

.field private useExternalFile:Z

.field private version:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "overview"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "packages"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "types"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "constructors"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "methods"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "fields"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->SCOPE_ELEMENTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->failOnError:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->destDir:Ljava/io/File;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourceFiles:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageNames:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v3}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->excludePackageNames:Ljava/util/Vector;

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->author:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->version:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->group:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->links:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->groups:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->tags:Ljava/util/Vector;

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useDefaultExcludes:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doctitle:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->header:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->footer:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bottom:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->source:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->linksource:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->breakiterator:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->includeNoSourcePackages:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->old:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->nestedSourceFiles:Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageSets:Ljava/util/Vector;

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/Javadoc;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Javadoc;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->includeNoSourcePackages:Z

    return v0
.end method

.method private addArgIf(ZLjava/lang/String;)V
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private addArgIfNotEmpty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Warning: Leaving out empty argument \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private addSourceFiles(Ljava/util/Vector;)V
    .locals 6
    .param p1    # Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->nestedSourceFiles:Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->access$000(Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;)Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v3}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "only file system based resources are supported by javadoc"

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    instance-of v4, v3, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v4, :cond_2

    move-object v1, v3

    check-cast v1, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FileSet;->hasPatterns()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FileSet;->hasSelectors()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FileSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FileSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v4

    const-string v5, "**/*.java"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->includeNoSourcePackages:Z

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FileSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v4

    const-string v5, "**/package.html"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    :cond_2
    invoke-interface {v3}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-direct {v5, v4}, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;-><init>(Ljava/io/File;)V

    invoke-virtual {p1, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private parsePackages(Ljava/util/Vector;Lorg/apache/tools/ant/types/Path;)V
    .locals 23
    .param p1    # Ljava/util/Vector;
    .param p2    # Lorg/apache/tools/ant/types/Path;

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageSets:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    new-instance v19, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct/range {v19 .. v19}, Lorg/apache/tools/ant/types/PatternSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageNames:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v20

    if-lez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageNames:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;->getName()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x2e

    const/16 v22, 0x2f

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v18

    const-string v20, "*"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, "*"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    :cond_0
    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/types/PatternSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/types/PatternSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v20

    const-string v21, "**"

    invoke-virtual/range {v20 .. v21}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->excludePackageNames:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;->getName()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x2e

    const/16 v22, 0x2f

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v18

    const-string v20, "*"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, "*"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    :cond_3
    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/types/PatternSet;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v16

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v13, v0, :cond_6

    new-instance v6, Ljava/io/File;

    aget-object v20, v16, v13

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v20

    if-eqz v20, :cond_5

    new-instance v9, Lorg/apache/tools/ant/types/DirSet;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/DirSet;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useDefaultExcludes:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lorg/apache/tools/ant/types/DirSet;->setDefaultexcludes(Z)V

    invoke-virtual {v9, v6}, Lorg/apache/tools/ant/types/DirSet;->setDir(Ljava/io/File;)V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/DirSet;->createPatternSet()Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/PatternSet;->addConfiguredPatternset(Lorg/apache/tools/ant/types/PatternSet;)V

    invoke-virtual {v7, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_5
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "Skipping "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    aget-object v21, v16, v13

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " since it is no directory."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto :goto_3

    :cond_6
    invoke-virtual {v7}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/types/DirSet;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lorg/apache/tools/ant/types/DirSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v4

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "scanning "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " for packages."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lorg/apache/tools/ant/types/DirSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x0

    const/4 v13, 0x0

    :goto_5
    array-length v0, v8

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v13, v0, :cond_9

    new-instance v17, Ljava/io/File;

    aget-object v20, v8, v13

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v20, Lorg/apache/tools/ant/taskdefs/Javadoc$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$1;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v12

    array-length v0, v12

    move/from16 v20, v0

    if-lez v20, :cond_7

    const-string v20, ""

    aget-object v21, v8, v13

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " contains source files in the default package,"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " you must specify them as source files"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " not packages."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_7
    :goto_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    :cond_8
    const/4 v5, 0x1

    aget-object v20, v8, v13

    sget-char v21, Ljava/io/File;->separatorChar:C

    const/16 v22, 0x2e

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_7

    invoke-virtual {v3, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    :cond_9
    if-eqz v5, :cond_a

    invoke-virtual/range {p2 .. p2}, Lorg/apache/tools/ant/types/Path;->createPathElement()Lorg/apache/tools/ant/types/Path$PathElement;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Path$PathElement;->setLocation(Ljava/io/File;)V

    goto/16 :goto_4

    :cond_a
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, " doesn\'t contain any packages, dropping it."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_4

    :cond_b
    return-void
.end method

.method private quoteString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/16 v3, 0x22

    const/16 v2, 0x27

    const/4 v1, -0x1

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->quoteString(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v3}, Lorg/apache/tools/ant/taskdefs/Javadoc;->quoteString(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private quoteString(Ljava/lang/String;C)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # C

    const/16 v3, 0x5c

    const/4 v2, -0x1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    const-string v1, "\\\\"

    invoke-direct {p0, p1, v3, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->replace(Ljava/lang/String;CLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "\\"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->replace(Ljava/lang/String;CLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private replace(Ljava/lang/String;CLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # C
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, p2, :cond_0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private writeExternalArgs(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 11
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v5, 0x0

    const/4 v3, 0x0

    :try_start_0
    sget-object v7, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v8, "javadocOptions"

    const-string v9, ""

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->deleteOnExit()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->getArguments()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->clearArgs()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    new-instance v4, Ljava/io/PrintWriter;

    new-instance v7, Ljava/io/FileWriter;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct {v7, v8, v9}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v4, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :goto_0
    :try_start_1
    array-length v7, v2

    if-ge v1, v7, :cond_3

    aget-object v6, v2, v1

    const-string v7, "-J-"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v4, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v3, v4

    :goto_2
    if-eqz v5, :cond_1

    :try_start_2
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_1
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Error creating or writing temporary file for javadoc options"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v7, v8, v0, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v7

    :goto_3
    sget-object v8, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    throw v7

    :cond_2
    :try_start_3
    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/Javadoc;->quoteString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :catchall_1
    move-exception v7

    move-object v3, v4

    goto :goto_3

    :cond_3
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    sget-object v7, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    return-void

    :catch_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public addBottom(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bottom:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    return-void
.end method

.method public addDoctitle(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doctitle:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    return-void
.end method

.method public addExcludePackage(Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->excludePackageNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createSourceFiles()Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFooter(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->footer:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    return-void
.end method

.method public addHeader(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->header:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    return-void
.end method

.method public addPackage(Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addPackageset(Lorg/apache/tools/ant/types/DirSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/DirSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageSets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addSource(Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourceFiles:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addTaglet(Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->tags:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createArg()Lorg/apache/tools/ant/types/Commandline$Argument;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    return-object v0
.end method

.method public createBootclasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createDoclet()Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    return-object v0
.end method

.method public createGroup()Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->groups:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createLink()Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->links:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createSourceFiles()Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->nestedSourceFiles:Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    return-object v0
.end method

.method public createSourcepath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createTag()Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->tags:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public execute()V
    .locals 58
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const-string v54, "javadoc2"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getTaskType()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_0

    const-string v54, "Warning: the task name <javadoc2> is deprecated. Use <javadoc> instead."

    const/16 v55, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_0
    const-string v54, "1.2"

    invoke-static/range {v54 .. v54}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v54

    if-nez v54, :cond_1

    const-string v54, "1.3"

    invoke-static/range {v54 .. v54}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v54

    if-nez v54, :cond_1

    const/16 v18, 0x1

    :goto_0
    if-eqz v18, :cond_2

    const-string v54, "1.4"

    invoke-static/range {v54 .. v54}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v54

    if-nez v54, :cond_2

    const/16 v19, 0x1

    :goto_1
    new-instance v32, Ljava/util/Vector;

    invoke-direct/range {v32 .. v32}, Ljava/util/Vector;-><init>()V

    new-instance v38, Lorg/apache/tools/ant/types/Path;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    move-object/from16 v0, v38

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    if-nez v54, :cond_3

    const-string v23, "sourcePath attribute must be set when specifying packagelist."

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v54

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_1
    const/16 v18, 0x0

    goto :goto_0

    :cond_2
    const/16 v19, 0x0

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    if-eqz v54, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    move-object/from16 v0, v38

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Path;->addExisting(Lorg/apache/tools/ant/types/Path;)V

    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->parsePackages(Ljava/util/Vector;Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual/range {v32 .. v32}, Ljava/util/Vector;->size()I

    move-result v54

    if-eqz v54, :cond_5

    invoke-virtual/range {v38 .. v38}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-nez v54, :cond_5

    const-string v23, "sourcePath attribute must be set when specifying package names."

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v54

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourceFiles:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/util/Vector;

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addSourceFiles(Ljava/util/Vector;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    move-object/from16 v54, v0

    if-nez v54, :cond_6

    invoke-virtual/range {v32 .. v32}, Ljava/util/Vector;->size()I

    move-result v54

    if-nez v54, :cond_6

    invoke-virtual/range {v40 .. v40}, Ljava/util/Vector;->size()I

    move-result v54

    if-nez v54, :cond_6

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "No source files and no packages have been specified."

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_6
    const-string v54, "Generating Javadoc"

    const/16 v55, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/types/Commandline;->clone()Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Lorg/apache/tools/ant/types/Commandline;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    move-object/from16 v54, v0

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doctitle:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v54, v0

    if-eqz v54, :cond_7

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-doctitle"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doctitle:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->getText()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->expand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->header:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v54, v0

    if-eqz v54, :cond_8

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-header"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->header:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->getText()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->expand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->footer:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v54, v0

    if-eqz v54, :cond_9

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-footer"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->footer:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->getText()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->expand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bottom:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v54, v0

    if-eqz v54, :cond_a

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-bottom"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bottom:Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->getText()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->expand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    if-nez v54, :cond_10

    new-instance v54, Lorg/apache/tools/ant/types/Path;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v55

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    const-string v55, "last"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-lez v54, :cond_b

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-classpath"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v55, v0

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_b
    invoke-virtual/range {v38 .. v38}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-lez v54, :cond_c

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-sourcepath"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->version:Z

    move/from16 v54, v0

    if-eqz v54, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-nez v54, :cond_d

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-version"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->author:Z

    move/from16 v54, v0

    if-eqz v54, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-nez v54, :cond_e

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-author"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-nez v54, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->destDir:Ljava/io/File;

    move-object/from16 v54, v0

    if-nez v54, :cond_11

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "destdir attribute must be set!"

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_f
    const-string v54, "javadoc"

    invoke-static/range {v54 .. v54}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJdkExecutable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    const-string v55, "ignore"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    goto/16 :goto_3

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-eqz v54, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->getName()Ljava/lang/String;

    move-result-object v54

    if-nez v54, :cond_12

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "The doclet name must be specified."

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v56

    invoke-direct/range {v54 .. v56}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v54

    :cond_12
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-doclet"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->getName()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->getPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    if-eqz v54, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->getPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    const-string v55, "ignore"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-eqz v54, :cond_13

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-docletpath"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->getParams()Ljava/util/Enumeration;

    move-result-object v6

    :cond_14
    :goto_4
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_16

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;

    invoke-virtual/range {v33 .. v33}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;->getName()Ljava/lang/String;

    move-result-object v54

    if-nez v54, :cond_15

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "Doclet parameters must have a name"

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_15
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    invoke-virtual/range {v33 .. v33}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;->getName()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v33}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;->getValue()Ljava/lang/String;

    move-result-object v54

    if-eqz v54, :cond_14

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    invoke-virtual/range {v33 .. v33}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletParam;->getValue()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_4

    :cond_16
    new-instance v4, Lorg/apache/tools/ant/types/Path;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-direct {v4, v0}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    if-eqz v54, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-virtual {v4, v0}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_17
    const-string v54, "ignore"

    move-object/from16 v0, v54

    invoke-virtual {v4, v0}, Lorg/apache/tools/ant/types/Path;->concatSystemBootClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-lez v54, :cond_18

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-bootclasspath"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->links:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->size()I

    move-result v54

    if-eqz v54, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->links:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_20

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v54

    if-eqz v54, :cond_19

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/String;->length()I

    move-result v54

    if-nez v54, :cond_1a

    :cond_19
    const-string v54, "No href was given for the link - skipping"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto :goto_5

    :cond_1a
    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->shouldResolveLink()Z

    move-result v54

    if-eqz v54, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v54

    if-eqz v54, :cond_1b

    :try_start_0
    sget-object v54, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, v54

    invoke-virtual {v0, v14}, Lorg/apache/tools/ant/util/FileUtils;->getFileURL(Ljava/io/File;)Ljava/net/URL;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    :cond_1b
    :goto_6
    if-nez v22, :cond_1c

    :try_start_1
    new-instance v3, Ljava/net/URL;

    const-string v54, "file://."

    move-object/from16 v0, v54

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    new-instance v54, Ljava/net/URL;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    invoke-direct {v0, v3, v1}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v22

    :cond_1c
    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->isLinkOffline()Z

    move-result v54

    if-eqz v54, :cond_1f

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getPackagelistLoc()Ljava/io/File;

    move-result-object v28

    if-nez v28, :cond_1d

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "The package list location for link "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, " must be provided "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, "because the link is "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, "offline"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :catch_0
    move-exception v9

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Warning: link location was invalid "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto :goto_6

    :catch_1
    move-exception v24

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Link href \""

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v20 .. v20}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->getHref()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    const-string v55, "\" is not a valid url - skipping link"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_1d
    new-instance v27, Ljava/io/File;

    const-string v54, "package-list"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v54

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    move-result v54

    if-eqz v54, :cond_1e

    :try_start_2
    sget-object v54, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, v54

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/FileUtils;->getFileURL(Ljava/io/File;)Ljava/net/URL;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-linkoffline"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_5

    :catch_2
    move-exception v9

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Warning: Package list location was invalid "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_1e
    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Warning: No package list was found at "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_1f
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-link"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->group:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_22

    new-instance v53, Ljava/util/StringTokenizer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->group:Ljava/lang/String;

    move-object/from16 v54, v0

    const-string v55, ","

    const/16 v56, 0x0

    invoke-direct/range {v53 .. v56}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_21
    :goto_7
    invoke-virtual/range {v53 .. v53}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v54

    if-eqz v54, :cond_22

    invoke-virtual/range {v53 .. v53}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v54, " "

    move-object/from16 v0, v54

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v41

    if-lez v41, :cond_21

    const/16 v54, 0x0

    move/from16 v0, v54

    move/from16 v1, v41

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v25

    add-int/lit8 v54, v41, 0x1

    move/from16 v0, v54

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-group"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_7

    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->groups:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->size()I

    move-result v54

    if-eqz v54, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->groups:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_8
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_25

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;->getTitle()Ljava/lang/String;

    move-result-object v50

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/Javadoc$GroupArgument;->getPackages()Ljava/lang/String;

    move-result-object v31

    if-eqz v50, :cond_23

    if-nez v31, :cond_24

    :cond_23
    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "The title and packages must be specified for group elements."

    invoke-direct/range {v54 .. v55}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v54

    :cond_24
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-group"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->expand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_8

    :cond_25
    if-nez v18, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_38

    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->tags:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :cond_27
    :goto_9
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_2b

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    instance-of v0, v7, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;

    move/from16 v54, v0

    if-eqz v54, :cond_2a

    move-object/from16 v44, v7

    check-cast v44, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    move-object/from16 v0, v44

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v47

    if-nez v47, :cond_28

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-tag"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    invoke-virtual/range {v44 .. v44}, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;->getParameter()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_9

    :cond_28
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    move-object/from16 v0, v44

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$TagArgument;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v11

    const/4 v15, 0x0

    :goto_a
    array-length v0, v11

    move/from16 v54, v0

    move/from16 v0, v54

    if-ge v15, v0, :cond_27

    new-instance v45, Ljava/io/File;

    aget-object v54, v11, v15

    move-object/from16 v0, v45

    move-object/from16 v1, v47

    move-object/from16 v2, v54

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_3
    new-instance v16, Ljava/io/BufferedReader;

    new-instance v54, Ljava/io/FileReader;

    move-object/from16 v0, v54

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/16 v21, 0x0

    :goto_b
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_29

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-tag"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_b

    :catch_3
    move-exception v17

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Couldn\'t read  tag file from "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v54

    :cond_29
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    add-int/lit8 v15, v15, 0x1

    goto :goto_a

    :cond_2a
    move-object/from16 v48, v7

    check-cast v48, Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-taglet"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    invoke-virtual/range {v48 .. v48}, Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;->getName()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v48 .. v48}, Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;->getPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    if-eqz v54, :cond_27

    invoke-virtual/range {v48 .. v48}, Lorg/apache/tools/ant/taskdefs/Javadoc$ExtensionInfo;->getPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v54

    const-string v55, "ignore"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v54

    if-eqz v54, :cond_27

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-tagletpath"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    goto/16 :goto_9

    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->source:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->source:Ljava/lang/String;

    move-object/from16 v37, v0

    :goto_c
    if-eqz v37, :cond_2c

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-source"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_2c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->linksource:Z

    move/from16 v54, v0

    if-eqz v54, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-nez v54, :cond_2d

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-linksource"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->breakiterator:Z

    move/from16 v54, v0

    if-eqz v54, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-eqz v54, :cond_2e

    if-eqz v19, :cond_2f

    :cond_2e
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-breakiterator"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->noqualifier:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    move-object/from16 v54, v0

    if-nez v54, :cond_30

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-noqualifier"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->noqualifier:Ljava/lang/String;

    move-object/from16 v55, v0

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_30
    :goto_d
    if-eqz v18, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_3d

    :cond_31
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->old:Z

    move/from16 v54, v0

    if-eqz v54, :cond_32

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    const-string v55, "-1.1"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_32
    :goto_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    move/from16 v54, v0

    if-eqz v54, :cond_33

    if-eqz v18, :cond_33

    move-object/from16 v0, p0

    move-object/from16 v1, v52

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->writeExternalArgs(Lorg/apache/tools/ant/types/Commandline;)V

    :cond_33
    const/16 v51, 0x0

    const/16 v42, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    move/from16 v54, v0

    if-eqz v54, :cond_35

    if-nez v51, :cond_34

    sget-object v54, Lorg/apache/tools/ant/taskdefs/Javadoc;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v55, "javadoc"

    const-string v56, ""

    const/16 v57, 0x0

    invoke-virtual/range {v54 .. v57}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->deleteOnExit()V

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "@"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_34
    new-instance v43, Ljava/io/PrintWriter;

    new-instance v54, Ljava/io/FileWriter;

    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v55

    const/16 v56, 0x1

    invoke-direct/range {v54 .. v56}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    move-object/from16 v42, v43

    :cond_35
    invoke-virtual/range {v32 .. v32}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_f
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_3f

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    move/from16 v54, v0

    if-eqz v54, :cond_3e

    move-object/from16 v0, v42

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_f

    :catch_4
    move-exception v6

    :try_start_6
    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->delete()Z

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    const-string v55, "Error creating temporary file"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v56

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v56

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v54
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v54

    if-eqz v42, :cond_36

    invoke-virtual/range {v42 .. v42}, Ljava/io/PrintWriter;->close()V

    :cond_36
    throw v54

    :cond_37
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    const-string v55, "ant.build.javac.source"

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    goto/16 :goto_c

    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->tags:Ljava/util/Vector;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Ljava/util/Vector;->isEmpty()Z

    move-result v54

    if-nez v54, :cond_39

    const-string v54, "-tag and -taglet options not supported on Javadoc < 1.4"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->source:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_3a

    const-string v54, "-source option not supported on Javadoc < 1.4"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_3a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->linksource:Z

    move/from16 v54, v0

    if-eqz v54, :cond_3b

    const-string v54, "-linksource option not supported on Javadoc < 1.4"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_3b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->breakiterator:Z

    move/from16 v54, v0

    if-eqz v54, :cond_3c

    const-string v54, "-breakiterator option not supported on Javadoc < 1.4"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    :cond_3c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->noqualifier:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_30

    const-string v54, "-noqualifier option not supported on Javadoc < 1.4"

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_d

    :cond_3d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->old:Z

    move/from16 v54, v0

    if-eqz v54, :cond_32

    const-string v54, "Javadoc 1.4 doesn\'t support the -1.1 switch anymore"

    const/16 v55, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    goto/16 :goto_e

    :cond_3e
    :try_start_7
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_3f
    invoke-virtual/range {v40 .. v40}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_10
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v54

    if-eqz v54, :cond_43

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;

    invoke-virtual/range {v36 .. v36}, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;->getFile()Ljava/io/File;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    move/from16 v54, v0

    if-eqz v54, :cond_42

    if-eqz v18, :cond_41

    const-string v54, " "

    move-object/from16 v0, v39

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v54

    const/16 v55, -0x1

    move/from16 v0, v54

    move/from16 v1, v55

    if-le v0, v1, :cond_41

    move-object/from16 v25, v39

    sget-char v54, Ljava/io/File;->separatorChar:C

    const/16 v55, 0x5c

    move/from16 v0, v54

    move/from16 v1, v55

    if-ne v0, v1, :cond_40

    sget-char v54, Ljava/io/File;->separatorChar:C

    const/16 v55, 0x2f

    move-object/from16 v0, v39

    move/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v25

    :cond_40
    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "\""

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    const-string v55, "\""

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v42

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_10

    :cond_41
    move-object/from16 v0, v42

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_10

    :cond_42
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_10

    :cond_43
    if-eqz v42, :cond_44

    invoke-virtual/range {v42 .. v42}, Ljava/io/PrintWriter;->close()V

    :cond_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_45

    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v54

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "@"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    move-object/from16 v56, v0

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {v54 .. v55}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_45
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->describeCommand()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    const-string v54, "Javadoc execution"

    const/16 v55, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc;->log(Ljava/lang/String;I)V

    new-instance v26, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;

    const/16 v54, 0x2

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move/from16 v2, v54

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;I)V

    new-instance v8, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;

    const/16 v54, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v54

    invoke-direct {v8, v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;I)V

    new-instance v10, Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v54, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    move-object/from16 v0, v54

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v8}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    move-object/from16 v0, v54

    invoke-direct {v10, v0}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    const/16 v54, 0x0

    move-object/from16 v0, v54

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    :try_start_8
    invoke-virtual/range {v52 .. v52}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v35

    if-eqz v35, :cond_47

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Javadoc;->failOnError:Z

    move/from16 v54, v0

    if-eqz v54, :cond_47

    new-instance v54, Lorg/apache/tools/ant/BuildException;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Javadoc returned "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v56

    invoke-direct/range {v54 .. v56}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v54
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catch_5
    move-exception v6

    :try_start_9
    new-instance v54, Lorg/apache/tools/ant/BuildException;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Javadoc failed: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v56

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v56

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v54
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v54

    if-eqz v51, :cond_46

    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->delete()Z

    const/16 v51, 0x0

    :cond_46
    invoke-virtual/range {v26 .. v26}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->logFlush()V

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->logFlush()V

    :try_start_a
    invoke-virtual/range {v26 .. v26}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->close()V

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    :goto_11
    throw v54

    :cond_47
    if-eqz v51, :cond_48

    invoke-virtual/range {v51 .. v51}, Ljava/io/File;->delete()Z

    const/16 v51, 0x0

    :cond_48
    invoke-virtual/range {v26 .. v26}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->logFlush()V

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->logFlush()V

    :try_start_b
    invoke-virtual/range {v26 .. v26}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->close()V

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Javadoc$JavadocOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :goto_12
    return-void

    :catch_6
    move-exception v55

    goto :goto_11

    :catch_7
    move-exception v54

    goto :goto_12
.end method

.method protected expand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAccess(Lorg/apache/tools/ant/taskdefs/Javadoc$AccessType;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javadoc$AccessType;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$AccessType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setAdditionalparam(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    return-void
.end method

.method public setAuthor(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->author:Z

    return-void
.end method

.method public setBootClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createBootclasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setBootclasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setBottom(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->addText(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addBottom(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V

    return-void
.end method

.method public setBreakiterator(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->breakiterator:Z

    return-void
.end method

.method public setCharset(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "-charset"

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIfNotEmpty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setDefaultexcludes(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useDefaultExcludes:Z

    return-void
.end method

.method public setDestdir(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->destDir:Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-d"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->destDir:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    return-void
.end method

.method public setDocencoding(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-docencoding"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setDoclet(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->setName(Ljava/lang/String;)V

    return-void
.end method

.method public setDocletPath(Lorg/apache/tools/ant/types/Path;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->setPath(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setDocletPathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;-><init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->doclet:Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$DocletInfo;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setDoctitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->addText(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addDoctitle(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-encoding"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setExcludePackageNames(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, ","

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;-><init>()V

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;->setName(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addExcludePackage(Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setExecutable(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->executable:Ljava/lang/String;

    return-void
.end method

.method public setExtdirs(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-extdirs"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setExtdirs(Lorg/apache/tools/ant/types/Path;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-extdirs"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setFailonerror(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->failOnError:Z

    return-void
.end method

.method public setFooter(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->addText(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addFooter(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V

    return-void
.end method

.method public setGroup(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->group:Ljava/lang/String;

    return-void
.end method

.method public setHeader(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$Html;->addText(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addHeader(Lorg/apache/tools/ant/taskdefs/Javadoc$Html;)V

    return-void
.end method

.method public setHelpfile(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-helpfile"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    return-void
.end method

.method public setIncludeNoSourcePackages(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->includeNoSourcePackages:Z

    return-void
.end method

.method public setLink(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createLink()Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->setHref(Ljava/lang/String;)V

    return-void
.end method

.method public setLinkoffline(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createLink()Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->setOffline(Z)V

    const-string v1, "The linkoffline attribute must include a URL and a package-list file location separated by a space"

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, " "

    const/4 v4, 0x0

    invoke-direct {v2, p1, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->setHref(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/Javadoc$LinkArgument;->setPackagelistLoc(Ljava/io/File;)V

    return-void
.end method

.method public setLinksource(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->linksource:Z

    return-void
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-locale"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setMaxmemory(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "-J-Xmx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setNodeprecated(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-nodeprecated"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setNodeprecatedlist(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-nodeprecatedlist"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setNohelp(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-nohelp"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setNoindex(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-noindex"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setNonavbar(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-nonavbar"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setNoqualifier(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->noqualifier:Ljava/lang/String;

    return-void
.end method

.method public setNotree(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-notree"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setOld(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->old:Z

    return-void
.end method

.method public setOverview(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-overview"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    return-void
.end method

.method public setPackage(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-package"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setPackageList(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->packageList:Ljava/lang/String;

    return-void
.end method

.method public setPackagenames(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, ","

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;-><init>()V

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;->setName(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addPackage(Lorg/apache/tools/ant/taskdefs/Javadoc$PackageName;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setPrivate(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-private"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setProtected(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-protected"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setPublic(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-public"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setSerialwarn(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-serialwarn"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->source:Ljava/lang/String;

    return-void
.end method

.method public setSourcefiles(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, ","

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;->setFile(Ljava/io/File;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addSource(Lorg/apache/tools/ant/taskdefs/Javadoc$SourceFile;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setSourcepath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->sourcePath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setSourcepathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->createSourcepath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setSplitindex(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-splitindex"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setStylesheetfile(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-stylesheetfile"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    return-void
.end method

.method public setUse(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-use"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setUseExternalFile(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->useExternalFile:Z

    return-void
.end method

.method public setVerbose(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "-verbose"

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIf(ZLjava/lang/String;)V

    return-void
.end method

.method public setVersion(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc;->version:Z

    return-void
.end method

.method public setWindowtitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "-windowtitle"

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/Javadoc;->addArgIfNotEmpty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
