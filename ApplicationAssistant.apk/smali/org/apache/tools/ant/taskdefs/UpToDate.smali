.class public Lorg/apache/tools/ant/taskdefs/UpToDate;
.super Lorg/apache/tools/ant/Task;
.source "UpToDate.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# instance fields
.field protected mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field private property:Ljava/lang/String;

.field private sourceFile:Ljava/io/File;

.field private sourceFileSets:Ljava/util/Vector;

.field private sourceResources:Lorg/apache/tools/ant/types/resources/Union;

.field private targetFile:Ljava/io/File;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFileSets:Ljava/util/Vector;

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceResources:Lorg/apache/tools/ant/types/resources/Union;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method private getMapper()Lorg/apache/tools/ant/util/FileNameMapper;
    .locals 3

    const/4 v0, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v2, :cond_0

    new-instance v1, Lorg/apache/tools/ant/util/MergingMapper;

    invoke-direct {v1}, Lorg/apache/tools/ant/util/MergingMapper;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/util/MergingMapper;->setTo(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    goto :goto_0
.end method

.method private getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->value:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "true"

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->createMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addSrcfiles(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFileSets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public createSrcResources()Lorg/apache/tools/ant/types/resources/Union;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceResources:Lorg/apache/tools/ant/types/resources/Union;

    return-object v0
.end method

.method public eval()Z
    .locals 12

    const/4 v10, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFileSets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceResources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    if-nez v8, :cond_0

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "At least one srcfile or a nested <srcfiles> or <srcresources> element must be set."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFileSets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-gtz v8, :cond_1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceResources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v8

    if-lez v8, :cond_2

    :cond_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    if-eqz v8, :cond_2

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Cannot specify both the srcfile attribute and a nested <srcfiles> or <srcresources> element."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    if-nez v8, :cond_3

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v8, :cond_3

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "The targetfile attribute or a nested mapper element must be set."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_3
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_4

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "The targetfile \""

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v8, "\" does not exist."

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x3

    invoke-virtual {p0, v6, v8}, Lorg/apache/tools/ant/taskdefs/UpToDate;->log(Ljava/lang/String;I)V

    :goto_0
    return v7

    :cond_4
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " not found."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_5
    const/4 v5, 0x1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v8, :cond_8

    if-eqz v5, :cond_7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-ltz v8, :cond_7

    move v5, v6

    :cond_6
    :goto_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFileSets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    if-eqz v5, :cond_b

    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v2, v8}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    if-eqz v5, :cond_a

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v2, v8}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v8

    invoke-virtual {v0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/UpToDate;->scanDir(Ljava/io/File;[Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    move v5, v6

    :goto_3
    goto :goto_2

    :cond_7
    move v5, v7

    goto :goto_1

    :cond_8
    new-instance v4, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v4, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    if-eqz v5, :cond_9

    new-array v8, v6, [Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v9

    invoke-virtual {v4, v8, v10, v10, v9}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;

    move-result-object v8

    array-length v8, v8

    if-nez v8, :cond_9

    move v5, v6

    :goto_4
    goto :goto_1

    :cond_9
    move v5, v7

    goto :goto_4

    :cond_a
    move v5, v7

    goto :goto_3

    :cond_b
    if-eqz v5, :cond_c

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceResources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->listResources()[Lorg/apache/tools/ant/types/Resource;

    move-result-object v3

    if-eqz v5, :cond_d

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v8

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-static {p0, v3, v8, v9}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v8

    array-length v8, v8

    if-nez v8, :cond_d

    move v5, v6

    :cond_c
    :goto_5
    move v7, v5

    goto/16 :goto_0

    :cond_d
    move v5, v7

    goto :goto_5
.end method

.method public execute()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x3

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->property:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "property attribute is required."

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->eval()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->property:Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "File \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\" is up-to-date."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lorg/apache/tools/ant/taskdefs/UpToDate;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "All target files are up-to-date."

    invoke-virtual {p0, v1, v4}, Lorg/apache/tools/ant/taskdefs/UpToDate;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected scanDir(Ljava/io/File;[Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;

    new-instance v2, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v2, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/UpToDate;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v1

    move-object v0, p1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v2, p2, p1, v0, v1}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->property:Ljava/lang/String;

    return-void
.end method

.method public setSrcfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->sourceFile:Ljava/io/File;

    return-void
.end method

.method public setTargetFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->targetFile:Ljava/io/File;

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/UpToDate;->value:Ljava/lang/String;

    return-void
.end method
