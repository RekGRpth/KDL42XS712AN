.class public Lorg/apache/tools/ant/taskdefs/PathConvert;
.super Lorg/apache/tools/ant/Task;
.source "PathConvert.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;,
        Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;
    }
.end annotation


# static fields
.field private static onWindows:Z


# instance fields
.field private dirSep:Ljava/lang/String;

.field private mapper:Lorg/apache/tools/ant/types/Mapper;

.field private path:Lorg/apache/tools/ant/types/resources/Union;

.field private pathSep:Ljava/lang/String;

.field private prefixMap:Ljava/util/Vector;

.field private property:Ljava/lang/String;

.field private refid:Lorg/apache/tools/ant/types/Reference;

.field private setonempty:Z

.field private targetOS:Ljava/lang/String;

.field private targetWindows:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "dos"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->onWindows:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->refid:Lorg/apache/tools/ant/types/Reference;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetOS:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetWindows:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->setonempty:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->property:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->prefixMap:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method static access$000()Z
    .locals 1

    sget-boolean v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->onWindows:Z

    return v0
.end method

.method private declared-synchronized getPath()Lorg/apache/tools/ant/types/resources/Union;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/resources/Union;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private mapElement(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->prefixMap:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->prefixMap:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eq v2, p1, :cond_1

    move-object p1, v2

    :cond_0
    return-object p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private noChildrenAllowed()Lorg/apache/tools/ant/BuildException;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "You must not specify nested elements when using the refid attribute."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private validateSetup()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "You must specify a path to convert"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    sget-object v1, Ljava/io/File;->pathSeparator:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetOS:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetWindows:Z

    if-eqz v2, :cond_4

    const-string v1, ";"

    :goto_0
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetWindows:Z

    if-eqz v2, :cond_5

    const-string v0, "\\"

    :cond_1
    :goto_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    :cond_3
    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    return-void

    :cond_4
    const-string v1, ":"

    goto :goto_0

    :cond_5
    const-string v0, "/"

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getPath()Lorg/apache/tools/ant/types/resources/Union;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->addMapper(Lorg/apache/tools/ant/types/Mapper;)V

    return-void
.end method

.method public addMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public createMap()Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/PathConvert$MapEntry;-><init>(Lorg/apache/tools/ant/taskdefs/PathConvert;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->prefixMap:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createPath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-object v0
.end method

.method public execute()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->isReference()Z

    move-result v19

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->refid:Lorg/apache/tools/ant/types/Reference;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v10

    instance-of v0, v10, Lorg/apache/tools/ant/types/ResourceCollection;

    move/from16 v19, v0

    if-nez v19, :cond_0

    new-instance v19, Lorg/apache/tools/ant/BuildException;

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "refid \'"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->refid:Lorg/apache/tools/ant/types/Reference;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string v21, "\' does not refer to a resource collection."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v19

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    throw v19

    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getPath()Lorg/apache/tools/ant/types/resources/Union;

    move-result-object v19

    check-cast v10, Lorg/apache/tools/ant/types/ResourceCollection;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    :cond_1
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->validateSetup()V

    sget-boolean v19, Lorg/apache/tools/ant/taskdefs/PathConvert;->onWindows:Z

    if-eqz v19, :cond_2

    const-string v5, "\\"

    :goto_0
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/types/resources/Union;->list()[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapper:Lorg/apache/tools/ant/types/Mapper;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapper:Lorg/apache/tools/ant/types/Mapper;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v7

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    :goto_1
    array-length v0, v4

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v6, v0, :cond_4

    aget-object v19, v4, v6

    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v8, 0x0

    :goto_2
    if-eqz v9, :cond_3

    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v8, v0, :cond_3

    aget-object v19, v9, v8

    move-object/from16 v0, v19

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_2
    const-string v5, "/"

    goto :goto_0

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Ljava/lang/String;

    move-object/from16 v0, v19

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    :cond_5
    const/4 v6, 0x0

    :goto_3
    array-length v0, v4

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v6, v0, :cond_9

    aget-object v19, v4, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/PathConvert;->mapElement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_6
    new-instance v16, Ljava/util/StringTokenizer;

    const/16 v19, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v3, v5, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_4
    invoke-virtual/range {v16 .. v16}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-virtual/range {v16 .. v16}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    move-object/from16 v17, v0

    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->setonempty:Z

    move/from16 v19, v0

    if-nez v19, :cond_a

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    if-lez v19, :cond_b

    :cond_a
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->property:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/PathConvert;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_b
    :goto_5
    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    return-void

    :cond_c
    :try_start_2
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Set property "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->property:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/PathConvert;->log(Ljava/lang/String;I)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/PathConvert;->property:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method

.method public isReference()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->refid:Lorg/apache/tools/ant/types/Reference;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDirSep(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->dirSep:Ljava/lang/String;

    return-void
.end method

.method public setPathSep(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->pathSep:Ljava/lang/String;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->property:Ljava/lang/String;

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->path:Lorg/apache/tools/ant/types/resources/Union;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->refid:Lorg/apache/tools/ant/types/Reference;

    return-void
.end method

.method public setSetonempty(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->setonempty:Z

    return-void
.end method

.method public setTargetos(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/PathConvert;->setTargetos(Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;)V

    return-void
.end method

.method public setTargetos(Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/PathConvert$TargetOs;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetOS:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetOS:Ljava/lang/String;

    const-string v1, "unix"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetOS:Ljava/lang/String;

    const-string v1, "tandem"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/PathConvert;->targetWindows:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
