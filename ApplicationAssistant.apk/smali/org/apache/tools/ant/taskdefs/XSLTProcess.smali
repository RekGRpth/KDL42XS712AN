.class public Lorg/apache/tools/ant/taskdefs/XSLTProcess;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "XSLTProcess.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/XSLTLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$1;,
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$StyleMapper;,
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;,
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$OutputProperty;,
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field public static final PROCESSOR_TRAX:Ljava/lang/String; = "trax"

.field private static final TRAX_LIAISON_CLASS:Ljava/lang/String; = "org.apache.tools.ant.taskdefs.optional.TraXLiaison"


# instance fields
.field private baseDir:Ljava/io/File;

.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private destDir:Ljava/io/File;

.field private factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

.field private fileDirParameter:Ljava/lang/String;

.field private fileNameParameter:Ljava/lang/String;

.field private force:Z

.field private inFile:Ljava/io/File;

.field private liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

.field private loader:Lorg/apache/tools/ant/AntClassLoader;

.field private mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field private outFile:Ljava/io/File;

.field private outputProperties:Ljava/util/Vector;

.field private params:Ljava/util/Vector;

.field private performDirectoryScan:Z

.field private processor:Ljava/lang/String;

.field private resources:Lorg/apache/tools/ant/types/resources/Union;

.field private reuseLoadedStylesheet:Z

.field private stylesheetLoaded:Z

.field private targetExtension:Ljava/lang/String;

.field private useImplicitFileset:Z

.field private xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

.field private xslFile:Ljava/lang/String;

.field private xslResource:Lorg/apache/tools/ant/types/Resource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    const-string v0, ".html"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->targetExtension:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileNameParameter:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileDirParameter:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->params:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->force:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outputProperties:Ljava/util/Vector;

    new-instance v0, Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/XMLCatalog;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->performDirectoryScan:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->reuseLoadedStylesheet:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resources:Lorg/apache/tools/ant/types/resources/Union;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->useImplicitFileset:Z

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/XSLTProcess;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/XSLTProcess;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->targetExtension:Ljava/lang/String;

    return-object v0
.end method

.method private checkDest()V
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    if-nez v1, :cond_0

    const-string v0, "destdir attributes must be set!"

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method private ensureDirectoryFor(Ljava/io/File;)V
    .locals 4
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Unable to create directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method private loadClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v1, :cond_0

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v1}, Lorg/apache/tools/ant/AntClassLoader;->setThreadContextLoader()V

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-static {p1, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method private process(Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V
    .locals 8
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x2

    :try_start_0
    invoke-virtual {p3}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "In file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " time: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Out file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " time: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Style file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " time: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->force:Z

    if-nez v3, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    cmp-long v3, v1, v3

    if-ltz v3, :cond_1

    :cond_0
    invoke-direct {p0, p2}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->ensureDirectoryFor(Ljava/io/File;)V

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Processing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    invoke-virtual {p0, p3}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->configureLiaison(Lorg/apache/tools/ant/types/Resource;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-direct {p0, v3, p1}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->setLiaisonDynamicFileParameters(Lorg/apache/tools/ant/taskdefs/XSLTLiaison;Ljava/io/File;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-interface {v3, p1, p2}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->transform(Ljava/io/File;Ljava/io/File;)V

    :goto_0
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Skipping input file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " because it is older than output file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " and so is the stylesheet "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Failed to process "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v7}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_2
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private process(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V
    .locals 14
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;
    .param p4    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v8

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v3, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_0

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Skipping "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " it is a directory."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v4

    :goto_1
    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    array-length v10, v7

    if-nez v10, :cond_4

    :cond_1
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Skipping "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " it cannot get mapped to output."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v2, v3

    :goto_2
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Failed to process "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_2
    new-instance v10, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v10, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    :cond_3
    :try_start_2
    new-instance v4, Lorg/apache/tools/ant/taskdefs/XSLTProcess$StyleMapper;

    const/4 v10, 0x0

    invoke-direct {v4, p0, v10}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$StyleMapper;-><init>(Lorg/apache/tools/ant/taskdefs/XSLTProcess;Lorg/apache/tools/ant/taskdefs/XSLTProcess$1;)V

    goto :goto_1

    :cond_4
    if-eqz v7, :cond_5

    array-length v10, v7

    const/4 v11, 0x1

    if-le v10, v11, :cond_6

    :cond_5
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Skipping "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " its mapping is ambiguos."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v6, Ljava/io/File;

    const/4 v10, 0x0

    aget-object v10, v7, v10

    move-object/from16 v0, p3

    invoke-direct {v6, v0, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->force:Z

    if-nez v10, :cond_7

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gtz v10, :cond_7

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    cmp-long v10, v8, v10

    if-lez v10, :cond_8

    :cond_7
    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->ensureDirectoryFor(Ljava/io/File;)V

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Processing "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->configureLiaison(Lorg/apache/tools/ant/types/Resource;)V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-direct {p0, v10, v3}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->setLiaisonDynamicFileParameters(Lorg/apache/tools/ant/taskdefs/XSLTLiaison;Ljava/io/File;)V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-interface {v10, v3, v6}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->transform(Ljava/io/File;Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_8
    move-object v5, v6

    goto/16 :goto_0

    :catch_1
    move-exception v1

    goto/16 :goto_2

    :catch_2
    move-exception v1

    move-object v2, v3

    move-object v5, v6

    goto/16 :goto_2
.end method

.method private processResources(Lorg/apache/tools/ant/types/Resource;)V
    .locals 6
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/resources/Union;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v3

    instance-of v5, v4, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v5, :cond_1

    move-object v1, v4

    check-cast v1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    invoke-direct {p0, v0, v3, v5, p1}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->process(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private resolveProcessor(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v2, "trax"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "org.apache.tools.ant.taskdefs.optional.TraXLiaison"

    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private setLiaisonDynamicFileParameters(Lorg/apache/tools/ant/taskdefs/XSLTLiaison;Ljava/io/File;)V
    .locals 6
    .param p1    # Lorg/apache/tools/ant/taskdefs/XSLTLiaison;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileNameParameter:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileNameParameter:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileDirParameter:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    invoke-static {v2, p2}, Lorg/apache/tools/ant/util/FileUtils;->getRelativePath(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileDirParameter:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x5c

    const/16 v5, 0x2f

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-interface {p1, v3, v2}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v2, "."

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->addMapper(Lorg/apache/tools/ant/types/Mapper;)V

    return-void
.end method

.method public addConfiguredStyle(Lorg/apache/tools/ant/types/resources/Resources;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/Resources;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The style element must be specified with exactly one nested resource."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/Resources;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->setXslResource(Lorg/apache/tools/ant/types/Resource;)V

    return-void
.end method

.method public addConfiguredXMLCatalog(Lorg/apache/tools/ant/types/XMLCatalog;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/XMLCatalog;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/XMLCatalog;->addConfiguredXMLCatalog(Lorg/apache/tools/ant/types/XMLCatalog;)V

    return-void
.end method

.method public addMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method protected configureLiaison(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/resources/FileResource;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;->setFile(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->configureLiaison(Lorg/apache/tools/ant/types/Resource;)V

    return-void
.end method

.method protected configureLiaison(Lorg/apache/tools/ant/types/Resource;)V
    .locals 8
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x2

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->reuseLoadedStylesheet:Z

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    :try_start_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Loading stylesheet "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    instance-of v4, v4, Lorg/apache/tools/ant/taskdefs/XSLTLiaison2;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    check-cast v4, Lorg/apache/tools/ant/taskdefs/XSLTLiaison2;

    invoke-interface {v4, p0}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison2;->configure(Lorg/apache/tools/ant/taskdefs/XSLTProcess;)V

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    instance-of v4, v4, Lorg/apache/tools/ant/taskdefs/XSLTLiaison3;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    check-cast v4, Lorg/apache/tools/ant/taskdefs/XSLTLiaison3;

    invoke-interface {v4, p1}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison3;->setStylesheet(Lorg/apache/tools/ant/types/Resource;)V

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->params:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->shouldUse()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->getExpression()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->addParam(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Failed to transform using stylesheet "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v7}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :cond_4
    :try_start_1
    instance-of v4, p1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v4, :cond_5

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move-object v4, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-interface {v5, v4}, Lorg/apache/tools/ant/taskdefs/XSLTLiaison;->setStylesheet(Ljava/io/File;)V

    goto :goto_0

    :cond_5
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " accepts the stylesheet only as a file"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createFactory()Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "\'factory\' element must be unique"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    return-object v0
.end method

.method public createOutputProperty()Lorg/apache/tools/ant/taskdefs/XSLTProcess$OutputProperty;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$OutputProperty;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$OutputProperty;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outputProperties:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createParam()Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->params:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public execute()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v13, 0x0

    const/4 v12, 0x0

    const-string v8, "style"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getTaskType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "Warning: the task name <style> is deprecated. Use <xslt> instead."

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    if-nez v8, :cond_1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    if-nez v8, :cond_1

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "specify the stylesheet either as a filename in style attribute or as a nested resource"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    if-eqz v8, :cond_2

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "specify the stylesheet either as a filename in style attribute or as a nested resource but not as both"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "input file "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " does not exist"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_3
    :try_start_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    if-nez v8, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getLiaison()Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    instance-of v8, v8, Lorg/apache/tools/ant/taskdefs/XSLTLoggerAware;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    check-cast v8, Lorg/apache/tools/ant/taskdefs/XSLTLoggerAware;

    invoke-interface {v8, p0}, Lorg/apache/tools/ant/taskdefs/XSLTLoggerAware;->setLogger(Lorg/apache/tools/ant/taskdefs/XSLTLogger;)V

    :cond_5
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Using "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_6

    sget-object v8, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "DEPRECATED - the \'style\' attribute should be relative to the project\'s"

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;)V

    const-string v8, "             basedir, not the tasks\'s basedir."

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;)V

    :cond_6
    new-instance v1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v1, v8}, Lorg/apache/tools/ant/types/resources/FileResource;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v1, v7}, Lorg/apache/tools/ant/types/resources/FileResource;->setFile(Ljava/io/File;)V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    :cond_7
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outFile:Ljava/io/File;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outFile:Ljava/io/File;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0, v8, v9, v10}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->process(Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    if-eqz v8, :cond_8

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v8}, Lorg/apache/tools/ant/AntClassLoader;->resetThreadContextLoader()V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v8}, Lorg/apache/tools/ant/AntClassLoader;->cleanup()V

    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    :cond_8
    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    iput-boolean v13, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    :goto_0
    return-void

    :cond_9
    :try_start_1
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->checkDest()V

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->useImplicitFileset:Z

    if-eqz v8, :cond_c

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getDirectoryScanner(Ljava/io/File;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Transforming into "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->log(Ljava/lang/String;I)V

    invoke-virtual {v6}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    :goto_1
    array-length v8, v4

    if-ge v2, v8, :cond_a

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    aget-object v9, v4, v2

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0, v8, v9, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->process(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_a
    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->performDirectoryScan:Z

    if-eqz v8, :cond_e

    invoke-virtual {v6}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    :goto_2
    array-length v8, v0

    if-ge v3, v8, :cond_e

    new-instance v8, Ljava/io/File;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    aget-object v10, v0, v3

    invoke-direct {v8, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    :goto_3
    array-length v8, v4

    if-ge v2, v8, :cond_b

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v10, v0, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    aget-object v10, v4, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0, v8, v9, v10, v11}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->process(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Lorg/apache/tools/ant/types/Resource;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_c
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v8

    if-nez v8, :cond_e

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "no resources specified"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v9}, Lorg/apache/tools/ant/AntClassLoader;->resetThreadContextLoader()V

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v9}, Lorg/apache/tools/ant/AntClassLoader;->cleanup()V

    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    :cond_d
    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    iput-boolean v13, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    throw v8

    :cond_e
    :try_start_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->processResources(Lorg/apache/tools/ant/types/Resource;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    if-eqz v8, :cond_f

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v8}, Lorg/apache/tools/ant/AntClassLoader;->resetThreadContextLoader()V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    invoke-virtual {v8}, Lorg/apache/tools/ant/AntClassLoader;->cleanup()V

    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->loader:Lorg/apache/tools/ant/AntClassLoader;

    :cond_f
    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    iput-boolean v13, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->stylesheetLoaded:Z

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    goto/16 :goto_0
.end method

.method public getFactory()Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->factory:Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;

    return-object v0
.end method

.method protected getLiaison()Lorg/apache/tools/ant/taskdefs/XSLTLiaison;
    .locals 3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->processor:Ljava/lang/String;

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->processor:Ljava/lang/String;

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resolveProcessor(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->liaison:Lorg/apache/tools/ant/taskdefs/XSLTLiaison;

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    :try_start_1
    const-string v2, "trax"

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->resolveProcessor(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getOutputProperties()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outputProperties:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getXMLCatalog()Lorg/apache/tools/ant/types/XMLCatalog;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

    return-object v0
.end method

.method public init()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->init()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xmlCatalog:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method public setBasedir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->baseDir:Ljava/io/File;

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setDestdir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->destDir:Ljava/io/File;

    return-void
.end method

.method public setExtension(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->targetExtension:Ljava/lang/String;

    return-void
.end method

.method public setFileDirParameter(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileDirParameter:Ljava/lang/String;

    return-void
.end method

.method public setFileNameParameter(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->fileNameParameter:Ljava/lang/String;

    return-void
.end method

.method public setForce(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->force:Z

    return-void
.end method

.method public setIn(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->inFile:Ljava/io/File;

    return-void
.end method

.method public setOut(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->outFile:Ljava/io/File;

    return-void
.end method

.method public setProcessor(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->processor:Ljava/lang/String;

    return-void
.end method

.method public setReloadStylesheet(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->reuseLoadedStylesheet:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScanIncludedDirectories(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->performDirectoryScan:Z

    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslFile:Ljava/lang/String;

    return-void
.end method

.method public setUseImplicitFileset(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->useImplicitFileset:Z

    return-void
.end method

.method public setXslResource(Lorg/apache/tools/ant/types/Resource;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess;->xslResource:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method
