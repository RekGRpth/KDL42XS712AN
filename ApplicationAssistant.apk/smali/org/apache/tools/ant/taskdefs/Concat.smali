.class public Lorg/apache/tools/ant/taskdefs/Concat;
.super Lorg/apache/tools/ant/Task;
.source "Concat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Concat$1;,
        Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;,
        Lorg/apache/tools/ant/taskdefs/Concat$TextElement;
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x2000

.field private static final EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static final NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# instance fields
.field private append:Z

.field private binary:Z

.field private destinationFile:Ljava/io/File;

.field private encoding:Ljava/lang/String;

.field private eolString:Ljava/lang/String;

.field private filterChains:Ljava/util/Vector;

.field private fixLastLine:Z

.field private footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

.field private forceOverwrite:Z

.field private header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

.field private outputEncoding:Ljava/lang/String;

.field private outputWriter:Ljava/io/Writer;

.field private rc:Lorg/apache/tools/ant/types/resources/Resources;

.field private textBuffer:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Concat;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Exists;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/selectors/Exists;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Concat;->EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Not;

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Concat;->EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/selectors/Not;-><init>(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Concat;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->forceOverwrite:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->fixLastLine:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->reset()V

    return-void
.end method

.method static access$200(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Concat;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->eolString:Ljava/lang/String;

    return-object v0
.end method

.method static access$300(Lorg/apache/tools/ant/taskdefs/Concat;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Concat;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method static access$400(Lorg/apache/tools/ant/taskdefs/Concat;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Concat;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->fixLastLine:Z

    return v0
.end method

.method private binaryCat(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 11
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Binary concatenation of "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " resources to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/Concat;->log(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-direct {v6, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v4, Lorg/apache/tools/ant/util/ConcatResourceInputStream;

    invoke-direct {v4, p1}, Lorg/apache/tools/ant/util/ConcatResourceInputStream;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object v0, v4

    check-cast v0, Lorg/apache/tools/ant/util/ConcatResourceInputStream;

    move-object v8, v0

    invoke-virtual {v8, p0}, Lorg/apache/tools/ant/util/ConcatResourceInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lorg/apache/tools/ant/taskdefs/StreamPumper;

    invoke-direct {v8, v4, v6}, Lorg/apache/tools/ant/taskdefs/StreamPumper;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Thread;->join()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_0
    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    if-eqz v6, :cond_0

    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_0
    return-void

    :catch_0
    move-exception v7

    :try_start_5
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Unable to open "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " for writing"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v8

    :goto_1
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    if-eqz v5, :cond_1

    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    :cond_1
    throw v8

    :catch_1
    move-exception v1

    :try_start_7
    invoke-virtual {v7}, Ljava/lang/Thread;->join()V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_0

    :catch_2
    move-exception v8

    goto :goto_0

    :catch_3
    move-exception v2

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Unable to close "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    :catch_4
    move-exception v2

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Unable to close "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    :catchall_1
    move-exception v8

    move-object v5, v6

    goto :goto_1

    :catchall_2
    move-exception v8

    move-object v3, v4

    move-object v5, v6

    goto :goto_1
.end method

.method private cat(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 10
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    const/4 v2, 0x0

    const/16 v7, 0x2000

    new-array v0, v7, [C

    const/4 v5, 0x0

    :try_start_0
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    if-eqz v7, :cond_4

    new-instance v6, Ljava/io/PrintWriter;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    invoke-direct {v6, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    move-object v5, v6

    :goto_0
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->access$000(Lorg/apache/tools/ant/taskdefs/Concat$TextElement;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v7, Ljava/io/StringReader;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Concat;->concatenate([CLjava/io/Writer;Ljava/io/Reader;)V

    :cond_0
    :goto_1
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v7

    if-lez v7, :cond_1

    new-instance v7, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;

    const/4 v8, 0x0

    invoke-direct {v7, p0, p1, v8}, Lorg/apache/tools/ant/taskdefs/Concat$MultiReader;-><init>(Lorg/apache/tools/ant/taskdefs/Concat;Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/taskdefs/Concat$1;)V

    invoke-direct {p0, v0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Concat;->concatenate([CLjava/io/Writer;Ljava/io/Reader;)V

    :cond_1
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->access$000(Lorg/apache/tools/ant/taskdefs/Concat$TextElement;)Z

    move-result v7

    if-eqz v7, :cond_9

    new-instance v7, Ljava/io/StringReader;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Concat;->concatenate([CLjava/io/Writer;Ljava/io/Reader;)V

    :cond_2
    :goto_2
    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    return-void

    :cond_4
    :try_start_1
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    if-nez v7, :cond_5

    new-instance v3, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    const/4 v7, 0x1

    invoke-direct {v3, p0, v7}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/Task;I)V

    move-object v2, v3

    :goto_3
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    if-nez v7, :cond_7

    new-instance v6, Ljava/io/PrintWriter;

    new-instance v7, Ljava/io/BufferedWriter;

    new-instance v8, Ljava/io/OutputStreamWriter;

    invoke-direct {v8, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v6, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    move-object v5, v6

    goto :goto_0

    :cond_5
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    :cond_6
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/Concat;->append:Z

    invoke-direct {v3, v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    move-object v2, v3

    goto :goto_3

    :cond_7
    new-instance v6, Ljava/io/PrintWriter;

    new-instance v7, Ljava/io/BufferedWriter;

    new-instance v8, Ljava/io/OutputStreamWriter;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    invoke-direct {v8, v2, v9}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v6, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    move-object v5, v6

    goto/16 :goto_0

    :cond_8
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Error while concatenating: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v7

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v7

    :cond_9
    :try_start_3
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2
.end method

.method private concatenate([CLjava/io/Writer;Ljava/io/Reader;)V
    .locals 4
    .param p1    # [C
    .param p2    # Ljava/io/Writer;
    .param p3    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    if-eqz v2, :cond_0

    new-instance v0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    const/16 v2, 0x2000

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setBufferSize(I)V

    invoke-virtual {v0, p3}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    new-instance p3, Ljava/io/BufferedReader;

    invoke-virtual {v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v2

    invoke-direct {p3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_0
    :goto_0
    array-length v2, p1

    invoke-virtual {p3, p1, v3, v2}, Ljava/io/Reader;->read([CII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p2}, Ljava/io/Writer;->flush()V

    return-void

    :cond_1
    invoke-virtual {p2, p1, v3, v1}, Ljava/io/Writer;->write([CII)V

    goto :goto_0
.end method

.method private sanitizeText()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method private validate()Lorg/apache/tools/ant/types/ResourceCollection;
    .locals 14

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->sanitizeText()V

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->binary:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    if-nez v10, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "destfile attribute is required for binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    if-eqz v10, :cond_1

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Nested text is incompatible with binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->encoding:Ljava/lang/String;

    if-nez v10, :cond_2

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    if-eqz v10, :cond_3

    :cond_2
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Seting input or output encoding is incompatible with binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_3
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    if-eqz v10, :cond_4

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Setting filters is incompatible with binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_4
    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->fixLastLine:Z

    if-eqz v10, :cond_5

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Setting fixlastline is incompatible with binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_5
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-nez v10, :cond_6

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-eqz v10, :cond_7

    :cond_6
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Nested header or footer is incompatible with binary concatenation"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_7
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    if-eqz v10, :cond_8

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Cannot specify both a destination file and an output writer"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_8
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    if-nez v10, :cond_9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    if-nez v10, :cond_9

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "At least one resource must be provided, or some text."

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_9
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    if-eqz v10, :cond_14

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    if-eqz v10, :cond_a

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Cannot include inline text when using resources."

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_a
    new-instance v3, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    sget-object v10, Lorg/apache/tools/ant/taskdefs/Concat;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-virtual {v3, v10}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v3, v10}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/Restrict;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_b

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " does not exist."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v8}, Lorg/apache/tools/ant/taskdefs/Concat;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_b
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    if-eqz v10, :cond_d

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/resources/Resources;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    instance-of v10, v4, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v10, :cond_c

    check-cast v4, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v1

    sget-object v10, Lorg/apache/tools/ant/taskdefs/Concat;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v10, v1, v11}, Lorg/apache/tools/ant/util/FileUtils;->fileNameEquals(Ljava/io/File;Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_c

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Input file \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\" is the same as the output file."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_d
    new-instance v0, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    sget-object v10, Lorg/apache/tools/ant/taskdefs/Concat;->EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    if-eqz v10, :cond_e

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/Concat;->forceOverwrite:Z

    if-eqz v10, :cond_10

    :cond_e
    move v5, v9

    :goto_1
    if-nez v5, :cond_12

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Restrict;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    if-nez v5, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-eqz v10, :cond_f

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v10

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-lez v10, :cond_11

    :cond_f
    move v5, v9

    :goto_3
    goto :goto_2

    :cond_10
    move v5, v8

    goto :goto_1

    :cond_11
    move v5, v8

    goto :goto_3

    :cond_12
    if-nez v5, :cond_13

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " is up-to-date."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/Concat;->log(Ljava/lang/String;I)V

    const/4 v0, 0x0

    :cond_13
    :goto_4
    return-object v0

    :cond_14
    new-instance v7, Lorg/apache/tools/ant/types/resources/StringResource;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/resources/StringResource;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/resources/StringResource;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/resources/StringResource;->setValue(Ljava/lang/String;)V

    move-object v0, v7

    goto :goto_4
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/Resources;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Resources;-><init>()V

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    goto :goto_0
.end method

.method public addFilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Concat;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Concat;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFilterChain(Lorg/apache/tools/ant/types/FilterChain;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FilterChain;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFooter(Lorg/apache/tools/ant/taskdefs/Concat$TextElement;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    return-void
.end method

.method public addHeader(Lorg/apache/tools/ant/taskdefs/Concat$TextElement;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    return-void
.end method

.method public addText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method public createPath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Concat;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-object v0
.end method

.method public execute()V
    .locals 3

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat;->validate()Lorg/apache/tools/ant/types/ResourceCollection;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    if-nez v1, :cond_1

    const-string v1, "No existing resources and no nested text, doing nothing"

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Concat;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->binary:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Concat;->binaryCat(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Concat;->cat(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Concat;->append:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->forceOverwrite:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->encoding:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Concat;->fixLastLine:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->filterChains:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->footer:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->header:Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Concat;->binary:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->textBuffer:Ljava/lang/StringBuffer;

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->eolString:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->rc:Lorg/apache/tools/ant/types/resources/Resources;

    return-void
.end method

.method public setAppend(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->append:Z

    return-void
.end method

.method public setBinary(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->binary:Z

    return-void
.end method

.method public setDestfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->destinationFile:Ljava/io/File;

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->encoding:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setEol(Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mac"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "\r"

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->eolString:Ljava/lang/String;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "lf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "unix"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const-string v1, "\n"

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->eolString:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "crlf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "dos"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_5
    const-string v1, "\r\n"

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->eolString:Ljava/lang/String;

    goto :goto_0
.end method

.method public setFixLastLine(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->fixLastLine:Z

    return-void
.end method

.method public setForce(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->forceOverwrite:Z

    return-void
.end method

.method public setOutputEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputEncoding:Ljava/lang/String;

    return-void
.end method

.method public setWriter(Ljava/io/Writer;)V
    .locals 0
    .param p1    # Ljava/io/Writer;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat;->outputWriter:Ljava/io/Writer;

    return-void
.end method
