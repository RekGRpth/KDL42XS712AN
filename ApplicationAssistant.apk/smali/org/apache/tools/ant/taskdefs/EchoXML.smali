.class public Lorg/apache/tools/ant/taskdefs/EchoXML;
.super Lorg/apache/tools/ant/util/XMLFragment;
.source "EchoXML.java"


# static fields
.field private static final ERROR_NO_XML:Ljava/lang/String; = "No nested XML specified"


# instance fields
.field private append:Z

.field private file:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/util/XMLFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 7

    new-instance v4, Lorg/apache/tools/ant/util/DOMElementWriter;

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->append:Z

    if-nez v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-direct {v4, v5}, Lorg/apache/tools/ant/util/DOMElementWriter;-><init>(Z)V

    const/4 v2, 0x0

    :try_start_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->file:Ljava/io/File;

    if-eqz v5, :cond_1

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->append:Z

    invoke-direct {v3, v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    move-object v2, v3

    :goto_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/EchoXML;->getFragment()Lorg/w3c/dom/DocumentFragment;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/DocumentFragment;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "No nested XML specified"

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v3, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    const/4 v5, 0x2

    invoke-direct {v3, p0, v5}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/ProjectComponent;I)V

    move-object v2, v3

    goto :goto_1

    :cond_2
    check-cast v1, Lorg/w3c/dom/Element;

    invoke-virtual {v4, v1, v2}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/OutputStream;)V
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    return-void

    :catch_1
    move-exception v0

    :try_start_3
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public setAppend(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->append:Z

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/EchoXML;->file:Ljava/io/File;

    return-void
.end method
