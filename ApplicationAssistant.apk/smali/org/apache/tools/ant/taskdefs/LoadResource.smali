.class public Lorg/apache/tools/ant/taskdefs/LoadResource;
.super Lorg/apache/tools/ant/Task;
.source "LoadResource.java"


# instance fields
.field private encoding:Ljava/lang/String;

.field private failOnError:Z

.field private final filterChains:Ljava/util/Vector;

.field private property:Ljava/lang/String;

.field private quiet:Z

.field private src:Lorg/apache/tools/ant/types/Resource;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->encoding:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->filterChains:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only single argument resource collections are supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public final addFilterChain(Lorg/apache/tools/ant/types/FilterChain;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FilterChain;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->filterChains:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public final execute()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v14, :cond_0

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    const-string v15, "source resource not defined"

    invoke-direct {v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    if-nez v14, :cond_1

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    const-string v15, "output property not defined"

    invoke-direct {v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    if-eqz v14, :cond_2

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    const-string v15, "quiet and failonerror cannot both be set to true"

    invoke-direct {v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v14

    if-nez v14, :cond_5

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " doesn\'t exist"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    if-eqz v14, :cond_3

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v14, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    if-eqz v14, :cond_4

    const/4 v14, 0x1

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v14}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V

    :goto_1
    return-void

    :cond_4
    const/4 v14, 0x0

    goto :goto_0

    :cond_5
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "loading "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " into property "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v9

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "resource size = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-wide/16 v16, -0x1

    cmp-long v14, v9, v16

    if-eqz v14, :cond_9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    :goto_2
    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V

    long-to-int v12, v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->encoding:Ljava/lang/String;

    if-nez v14, :cond_a

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v5, v6

    :goto_3
    const-string v13, ""

    if-eqz v12, :cond_7

    new-instance v4, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v4}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    const-wide/16 v14, -0x1

    cmp-long v14, v9, v14

    if-eqz v14, :cond_6

    invoke-virtual {v4, v12}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setBufferSize(I)V

    :cond_6
    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->filterChains:Ljava/util/Vector;

    invoke-virtual {v4, v14}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/LoadResource;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v14

    invoke-virtual {v4, v14}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->readFully(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v13

    :cond_7
    if-eqz v13, :cond_8

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_8

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/LoadResource;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    invoke-virtual {v14, v15, v13}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "loaded "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " characters"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " := "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_8
    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    move-object v2, v3

    goto/16 :goto_1

    :cond_9
    :try_start_2
    const-string v14, "unknown"
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :cond_a
    :try_start_3
    new-instance v6, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->encoding:Ljava/lang/String;

    invoke-direct {v6, v3, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v5, v6

    goto/16 :goto_3

    :catch_0
    move-exception v7

    :goto_4
    :try_start_4
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Unable to load resource: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    if-eqz v14, :cond_b

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/LoadResource;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v15

    invoke-direct {v14, v11, v7, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v14

    :goto_5
    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v14

    :cond_b
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    if-eqz v14, :cond_c

    const/4 v14, 0x3

    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v14}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_1

    :cond_c
    const/4 v14, 0x0

    goto :goto_6

    :catch_1
    move-exception v1

    :goto_7
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    if-eqz v14, :cond_d

    throw v1

    :cond_d
    invoke-virtual {v1}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    if-eqz v14, :cond_e

    const/4 v14, 0x3

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lorg/apache/tools/ant/taskdefs/LoadResource;->log(Ljava/lang/String;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_1

    :cond_e
    const/4 v14, 0x0

    goto :goto_8

    :catchall_1
    move-exception v14

    move-object v2, v3

    goto :goto_5

    :catch_2
    move-exception v1

    move-object v2, v3

    goto :goto_7

    :catch_3
    move-exception v7

    move-object v2, v3

    goto :goto_4
.end method

.method public final setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->encoding:Ljava/lang/String;

    return-void
.end method

.method public final setFailonerror(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    return-void
.end method

.method public final setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->property:Ljava/lang/String;

    return-void
.end method

.method public setQuiet(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->quiet:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/LoadResource;->failOnError:Z

    :cond_0
    return-void
.end method
