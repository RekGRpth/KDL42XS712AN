.class Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;
.super Ljava/lang/Object;
.source "Delete.java"

# interfaces
.implements Lorg/apache/tools/ant/types/ResourceCollection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Delete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReverseDirs"
.end annotation


# static fields
.field static final REVERSE:Ljava/util/Comparator;


# instance fields
.field private basedir:Ljava/io/File;

.field private dirs:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs$1;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs$1;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->REVERSE:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(Ljava/io/File;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->basedir:Ljava/io/File;

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->dirs:[Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->dirs:[Ljava/lang/String;

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->REVERSE:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-void
.end method


# virtual methods
.method public isFilesystemOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->basedir:Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->dirs:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;->dirs:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
