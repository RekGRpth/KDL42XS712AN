.class public Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;
.super Lorg/apache/tools/ant/types/AbstractFileSet;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncTarget"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/AbstractFileSet;-><init>()V

    return-void
.end method


# virtual methods
.method public setDir(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "preserveintarget doesn\'t support the dir attribute"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
