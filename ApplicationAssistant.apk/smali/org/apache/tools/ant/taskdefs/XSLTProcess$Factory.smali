.class public Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;
.super Ljava/lang/Object;
.source "XSLTProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/XSLTProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory$Attribute;
    }
.end annotation


# instance fields
.field private attributes:Ljava/util/Vector;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;->attributes:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addAttribute(Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory$Attribute;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory$Attribute;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;->attributes:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public getAttributes()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;->attributes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Factory;->name:Ljava/lang/String;

    return-void
.end method
