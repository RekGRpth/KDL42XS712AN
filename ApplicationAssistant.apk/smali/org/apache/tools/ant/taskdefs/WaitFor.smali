.class public Lorg/apache/tools/ant/taskdefs/WaitFor;
.super Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;
.source "WaitFor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;
    }
.end annotation


# instance fields
.field private checkEveryMillis:J

.field private checkEveryMultiplier:J

.field private maxWaitMillis:J

.field private maxWaitMultiplier:J

.field private timeoutProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x1

    const-string v0, "waitfor"

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;-><init>(Ljava/lang/String;)V

    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iput-wide v2, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMultiplier:J

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    iput-wide v2, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMultiplier:J

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v10, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->countConditions()I

    move-result v9

    if-le v9, v10, :cond_0

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "You must not nest more than one condition into "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getTaskName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->countConditions()I

    move-result v9

    if-ge v9, v10, :cond_1

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "You must nest a condition into "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getTaskName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getConditions()Ljava/util/Enumeration;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/condition/Condition;

    iget-wide v5, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iget-wide v3, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    :try_start_0
    iget-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iget-wide v11, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMultiplier:J

    mul-long/2addr v9, v11

    iput-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iget-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    iget-wide v11, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMultiplier:J

    mul-long/2addr v9, v11

    iput-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    add-long v1, v7, v9

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    cmp-long v9, v9, v1

    if-gez v9, :cond_3

    invoke-interface {v0}, Lorg/apache/tools/ant/taskdefs/condition/Condition;->eval()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->processSuccess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-wide v5, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iput-wide v3, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    :goto_1
    return-void

    :cond_2
    :try_start_1
    iget-wide v9, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v9

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->processTimeout()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-wide v5, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iput-wide v3, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    goto :goto_1

    :catchall_0
    move-exception v9

    iput-wide v5, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    iput-wide v3, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    throw v9
.end method

.method protected processSuccess()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ": condition was met"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/WaitFor;->log(Ljava/lang/String;I)V

    return-void
.end method

.method protected processTimeout()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ": timeout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/WaitFor;->log(Ljava/lang/String;I)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->timeoutProperty:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/WaitFor;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->timeoutProperty:Ljava/lang/String;

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setCheckEvery(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMillis:J

    return-void
.end method

.method public setCheckEveryUnit(Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;->getMultiplier()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->checkEveryMultiplier:J

    return-void
.end method

.method public setMaxWait(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMillis:J

    return-void
.end method

.method public setMaxWaitUnit(Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/WaitFor$Unit;->getMultiplier()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->maxWaitMultiplier:J

    return-void
.end method

.method public setTimeoutProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/WaitFor;->timeoutProperty:Ljava/lang/String;

    return-void
.end method
