.class public Lorg/apache/tools/ant/taskdefs/Touch;
.super Lorg/apache/tools/ant/Task;
.source "Touch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;
    }
.end annotation


# static fields
.field private static final DEFAULT_DF_FACTORY:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private dateTime:Ljava/lang/String;

.field private dateTimeConfigured:Z

.field private dfFactory:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

.field private file:Ljava/io/File;

.field private fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

.field private filesets:Ljava/util/Vector;

.field private millis:J

.field private mkdirs:Z

.field private resources:Lorg/apache/tools/ant/types/resources/Union;

.field private verbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Touch$1;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Touch$1;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Touch;->DEFAULT_DF_FACTORY:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Touch;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->filesets:Ljava/util/Vector;

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->verbose:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Touch;->DEFAULT_DF_FACTORY:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dfFactory:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    return-void
.end method

.method private getTimestamp()J
    .locals 4

    iget-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    goto :goto_0
.end method

.method private touch(Ljava/io/File;J)V
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # J

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Creating "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->verbose:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    invoke-virtual {p0, v2, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->log(Ljava/lang/String;I)V

    :try_start_0
    sget-object v1, Lorg/apache/tools/ant/taskdefs/Touch;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Touch;->mkdirs:Z

    invoke-virtual {v1, p1, v2}, Lorg/apache/tools/ant/util/FileUtils;->createNewFile(Ljava/io/File;Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Can not change modification date of read-only file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x3

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Could not create "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_2
    sget-object v1, Lorg/apache/tools/ant/taskdefs/Touch;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/tools/ant/util/FileUtils;->setFileLastModified(Ljava/io/File;J)V

    return-void
.end method

.method private touch(Lorg/apache/tools/ant/types/Resource;J)V
    .locals 6
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # J

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Touch;->fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

    if-nez v4, :cond_2

    instance-of v4, p1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v4, :cond_1

    check-cast p1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-direct {p0, v4, p2, p3}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Ljava/io/File;J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lorg/apache/tools/ant/types/resources/Touchable;

    invoke-interface {p1, p2, p3}, Lorg/apache/tools/ant/types/resources/Touchable;->touch(J)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Touch;->fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v4, v1

    if-lez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v2

    :goto_1
    const/4 v0, 0x0

    :goto_2
    array-length v4, v1

    if-ge v0, v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-direct {p0, v4, v2, v3}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Ljava/io/File;J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move-wide v2, p2

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Only one mapper may be added to the "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " task."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->fileNameMapper:Lorg/apache/tools/ant/util/FileNameMapper;

    return-void
.end method

.method public addConfiguredMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Touch;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addFilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Touch;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Touch;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method protected declared-synchronized checkConfiguration()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const-wide/16 v10, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Specify at least one source--a file or resource collection."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_0
    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Use a resource collection to touch directories."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTimeConfigured:Z

    if-nez v6, :cond_5

    iget-wide v4, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dfFactory:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    invoke-interface {v6}, Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;->getPrimaryFormat()Ljava/text/DateFormat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    const/4 v1, 0x0

    :try_start_2
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    :goto_0
    if-eqz v1, :cond_3

    :try_start_3
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v1, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v6

    :catch_0
    move-exception v2

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dfFactory:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    invoke-interface {v6}, Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;->getFallbackFormat()Ljava/text/DateFormat;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    if-nez v0, :cond_2

    move-object v1, v2

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J
    :try_end_4
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-wide v4

    goto :goto_0

    :catch_1
    move-exception v3

    move-object v1, v3

    goto :goto_0

    :cond_3
    cmp-long v6, v4, v10

    if-gez v6, :cond_4

    :try_start_5
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Date of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " results in negative "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "milliseconds value "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "relative to epoch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "(January 1, 1970, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "00:00:00 GMT)."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_4
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Setting millis to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " from datetime attribute"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    cmp-long v6, v8, v10

    if-gez v6, :cond_6

    const/4 v6, 0x4

    :goto_1
    invoke-virtual {p0, v7, v6}, Lorg/apache/tools/ant/taskdefs/Touch;->log(Ljava/lang/String;I)V

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Touch;->setMillis(J)V

    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTimeConfigured:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_5
    monitor-exit p0

    return-void

    :cond_6
    const/4 v6, 0x3

    goto :goto_1
.end method

.method public execute()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->checkConfiguration()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->touch()V

    return-void
.end method

.method public setDatetime(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Resetting datetime attribute to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->log(Ljava/lang/String;I)V

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTime:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dateTimeConfigured:Z

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    return-void
.end method

.method public setMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->millis:J

    return-void
.end method

.method public setMkdirs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->mkdirs:Z

    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Touch$2;

    invoke-direct {v0, p0, p1}, Lorg/apache/tools/ant/taskdefs/Touch$2;-><init>(Lorg/apache/tools/ant/taskdefs/Touch;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Touch;->dfFactory:Lorg/apache/tools/ant/taskdefs/Touch$DateFormatFactory;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Touch;->verbose:Z

    return-void
.end method

.method protected touch()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getTimestamp()J

    move-result-wide v0

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    if-eqz v10, :cond_0

    new-instance v10, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Touch;->file:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v10, v0, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Lorg/apache/tools/ant/types/Resource;J)V

    :cond_0
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Touch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/resources/Union;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/Resource;

    instance-of v10, v8, Lorg/apache/tools/ant/types/resources/Touchable;

    if-nez v10, :cond_1

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Can\'t touch "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_1
    invoke-direct {p0, v8, v0, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Lorg/apache/tools/ant/types/Resource;J)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    :goto_1
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Touch;->filesets:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-ge v5, v10, :cond_4

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Touch;->filesets:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v4, v10}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v4, v10}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v9

    const/4 v7, 0x0

    :goto_2
    array-length v10, v9

    if-ge v7, v10, :cond_3

    new-instance v10, Lorg/apache/tools/ant/types/resources/FileResource;

    aget-object v11, v9, v7

    invoke-direct {v10, v3, v11}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v10, v0, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Lorg/apache/tools/ant/types/Resource;J)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method protected touch(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Touch;->getTimestamp()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/tools/ant/taskdefs/Touch;->touch(Ljava/io/File;J)V

    return-void
.end method
