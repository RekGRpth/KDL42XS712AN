.class Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;
.super Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;
.source "Execute.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Execute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Java13CommandLauncher"
.end annotation


# instance fields
.field private myExecWithCWD:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$lang$Runtime:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "java.lang.Runtime"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Execute;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$lang$Runtime:Ljava/lang/Class;

    :goto_0
    const-string v2, "exec"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string v1, "[Ljava.lang.String;"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    :goto_1
    aput-object v1, v3, v4

    const/4 v4, 0x1

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_2

    const-string v1, "[Ljava.lang.String;"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    :goto_2
    aput-object v1, v3, v4

    const/4 v4, 0x2

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$io$File:Ljava/lang/Class;

    if-nez v1, :cond_3

    const-string v1, "java.io.File"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$io$File:Ljava/lang/Class;

    :goto_3
    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;->myExecWithCWD:Ljava/lang/reflect/Method;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$lang$Runtime:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->array$Ljava$lang$String:Ljava/lang/Class;

    goto :goto_2

    :cond_3
    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->class$java$io$File:Ljava/lang/Class;

    goto :goto_3
.end method


# virtual methods
.method public exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;
    .locals 6
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Execute:Java13CommandLauncher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p2}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {p1, v2, v3}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;->myExecWithCWD:Ljava/lang/reflect/Method;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Process;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/ThreadDeath;

    if-eqz v2, :cond_1

    check-cast v1, Ljava/lang/ThreadDeath;

    throw v1

    :cond_1
    instance-of v2, v1, Ljava/io/IOException;

    if-eqz v2, :cond_2

    check-cast v1, Ljava/io/IOException;

    throw v1

    :cond_2
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Unable to execute command"

    invoke-direct {v2, v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Unable to execute command"

    invoke-direct {v2, v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
