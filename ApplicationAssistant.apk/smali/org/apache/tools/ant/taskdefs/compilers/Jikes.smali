.class public Lorg/apache/tools/ant/taskdefs/compilers/Jikes;
.super Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.source "Jikes.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v15, "Using jikes compiler"

    const/16 v16, 0x3

    invoke-virtual/range {v14 .. v16}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    new-instance v3, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    if-eqz v14, :cond_15

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    invoke-virtual {v12}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v14

    if-lez v14, :cond_0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-sourcepath"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    invoke-virtual {v14, v12}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_0
    new-instance v2, Lorg/apache/tools/ant/types/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v2, v14}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v14

    if-nez v14, :cond_2

    :cond_1
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->includeJavaRuntime:Z

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v14

    invoke-virtual {v2, v14}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    const-string v14, "jikes.class.path"

    invoke-static {v14}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    new-instance v14, Lorg/apache/tools/ant/types/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v14, v15, v9}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->extdirs:Lorg/apache/tools/ant/types/Path;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v14}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v14

    if-lez v14, :cond_4

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-extdirs"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v14

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javac;->getExecutable()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_5

    const-string v6, "jikes"

    :cond_5
    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->deprecation:Z

    if-eqz v14, :cond_6

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-deprecation"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->destDir:Ljava/io/File;

    if-eqz v14, :cond_7

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-d"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->destDir:Ljava/io/File;

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    :cond_7
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-classpath"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    invoke-virtual {v14, v2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->encoding:Ljava/lang/String;

    if-eqz v14, :cond_8

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-encoding"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->encoding:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->debug:Z

    if-eqz v14, :cond_17

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javac;->getDebugLevel()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_16

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "-g:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->optimize:Z

    if-eqz v14, :cond_9

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-O"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->verbose:Z

    if-eqz v14, :cond_a

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-verbose"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->depend:Z

    if-eqz v14, :cond_b

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-depend"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->target:Ljava/lang/String;

    if-eqz v14, :cond_c

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-target"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->target:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    const-string v15, "build.compiler.emacs"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-static {v5}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "+E"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    const-string v15, "build.compiler.warnings"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v15, "!! the build.compiler.warnings property is deprecated. !!"

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v15, "!! Use the nowarn attribute instead. !!"

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    invoke-static {v13}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_e

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-nowarn"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javac;->getNowarn()Z

    move-result v14

    if-eqz v14, :cond_f

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-nowarn"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    const-string v15, "build.compiler.pedantic"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_10

    invoke-static {v10}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_10

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "+P"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->project:Lorg/apache/tools/ant/Project;

    const-string v15, "build.compiler.fulldepend"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_11

    invoke-static {v8}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_11

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "+F"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javac;->getSource()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_13

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-source"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v14}, Lorg/apache/tools/ant/taskdefs/Javac;->getSource()Ljava/lang/String;

    move-result-object v11

    const-string v14, "1.1"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_12

    const-string v14, "1.2"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_18

    :cond_12
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Jikes doesn\'t support \'-source "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, "\', will use \'-source 1.3\' instead"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "1.3"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_13
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->addCurrentCompilerArgs(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->size()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->getBootClassPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v14

    if-lez v14, :cond_14

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-bootclasspath"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    invoke-virtual {v14, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v7}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->executeExternalCompile([Ljava/lang/String;I)I

    move-result v14

    if-nez v14, :cond_19

    const/4 v14, 0x1

    :goto_3
    return v14

    :cond_15
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;->src:Lorg/apache/tools/ant/types/Path;

    goto/16 :goto_0

    :cond_16
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-g"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_17
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    const-string v15, "-g:none"

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_18
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v14

    invoke-virtual {v14, v11}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_2

    :cond_19
    const/4 v14, 0x0

    goto :goto_3
.end method
