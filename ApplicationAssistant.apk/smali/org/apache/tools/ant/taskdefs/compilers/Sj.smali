.class public Lorg/apache/tools/ant/taskdefs/compilers/Sj;
.super Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.source "Sj.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/Sj;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v4, "Using symantec java compiler"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Sj;->setupJavacCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Sj;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Javac;->getExecutable()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "sj"

    :cond_0
    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->size()I

    move-result v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Sj;->compileList:[Ljava/io/File;

    array-length v4, v4

    sub-int v2, v3, v4

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Lorg/apache/tools/ant/taskdefs/compilers/Sj;->executeExternalCompile([Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected getNoDebugArgument()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
