.class public abstract Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.super Ljava/lang/Object;
.source "DefaultCompilerAdapter.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapter;


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field protected static final lSep:Ljava/lang/String;


# instance fields
.field protected attributes:Lorg/apache/tools/ant/taskdefs/Javac;

.field protected bootclasspath:Lorg/apache/tools/ant/types/Path;

.field protected compileClasspath:Lorg/apache/tools/ant/types/Path;

.field protected compileList:[Ljava/io/File;

.field protected compileSourcepath:Lorg/apache/tools/ant/types/Path;

.field protected debug:Z

.field protected depend:Z

.field protected deprecation:Z

.field protected destDir:Ljava/io/File;

.field protected encoding:Ljava/lang/String;

.field protected extdirs:Lorg/apache/tools/ant/types/Path;

.field protected includeAntRuntime:Z

.field protected includeJavaRuntime:Z

.field protected location:Lorg/apache/tools/ant/Location;

.field protected memoryInitialSize:Ljava/lang/String;

.field protected memoryMaximumSize:Ljava/lang/String;

.field protected optimize:Z

.field protected project:Lorg/apache/tools/ant/Project;

.field protected src:Lorg/apache/tools/ant/types/Path;

.field protected target:Ljava/lang/String;

.field protected verbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    sget-object v0, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    sput-object v0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->lSep:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->debug:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->optimize:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->deprecation:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->depend:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->verbose:Z

    return-void
.end method


# virtual methods
.method protected addCurrentCompilerArgs(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Javac;->getCurrentCompilerArgs()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/Commandline;->addArguments([Ljava/lang/String;)V

    return-void
.end method

.method protected addExtdirsToClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/Path;->addExtdirs(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method protected assumeJava11()Z
    .locals 2

    const-string v0, "javac1.1"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected assumeJava12()Z
    .locals 2

    const-string v0, "javac1.2"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "classic"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.2"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "extJavac"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "1.2"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected assumeJava13()Z
    .locals 2

    const-string v0, "javac1.3"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "classic"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.3"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "modern"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1.3"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "extJavac"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1.3"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected assumeJava14()Z
    .locals 2

    const-string v0, "javac1.4"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "classic"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.4"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "modern"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1.4"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "extJavac"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1.4"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected assumeJava15()Z
    .locals 2

    const-string v0, "javac1.5"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "classic"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.5"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "modern"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1.5"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "extJavac"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1.5"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected assumeJava16()Z
    .locals 2

    const-string v0, "javac1.6"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "classic"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.6"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "modern"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1.6"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "extJavac"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Javac;->getCompilerVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1.6"

    invoke-static {v0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected executeExternalCompile([Ljava/lang/String;I)I
    .locals 1
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->executeExternalCompile([Ljava/lang/String;IZ)I

    move-result v0

    return v0
.end method

.method protected executeExternalCompile([Ljava/lang/String;IZ)I
    .locals 11
    .param p1    # [Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    const/4 v0, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-static {p1}, Lorg/apache/tools/ant/types/Commandline;->toString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v7

    const/16 v8, 0x1000

    if-le v7, v8, :cond_5

    if-ltz p2, :cond_5

    const/4 v4, 0x0

    :try_start_1
    sget-object v7, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v8, "files"

    const-string v9, ""

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/Javac;->getTempdir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->deleteOnExit()V

    new-instance v5, Ljava/io/PrintWriter;

    new-instance v7, Ljava/io/FileWriter;

    invoke-direct {v7, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, p2

    :goto_0
    :try_start_2
    array-length v7, p1

    if-ge v3, v7, :cond_2

    if-eqz p3, :cond_0

    aget-object v7, p1, v3

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-le v7, v8, :cond_0

    aget-object v7, p1, v3

    sget-char v8, Ljava/io/File;->separatorChar:C

    const/16 v9, 0x2f

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p1, v3

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, p1, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    aget-object v7, p1, v3

    invoke-virtual {v5, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v4, v5

    :goto_2
    :try_start_3
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Error creating temporary file"

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->location:Lorg/apache/tools/ant/Location;

    invoke-direct {v7, v8, v1, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v7

    :goto_3
    :try_start_4
    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v7

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    :cond_1
    throw v7

    :cond_2
    :try_start_5
    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V

    add-int/lit8 v7, p2, 0x1

    new-array v0, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {p1, v7, v0, v8, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "@"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, p2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_4
    :try_start_7
    new-instance v2, Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v7, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const/4 v9, 0x2

    const/4 v10, 0x1

    invoke-direct {v7, v8, v9, v10}, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;-><init>(Lorg/apache/tools/ant/Task;II)V

    invoke-direct {v2, v7}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    const-string v7, "openvms"

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lorg/apache/tools/ant/taskdefs/Execute;->setVMLauncher(Z)V

    :cond_3
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v7}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v7}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v2, v7}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Execute;->getExitValue()I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v7

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    :cond_4
    return v7

    :cond_5
    move-object v0, p1

    goto :goto_4

    :catch_1
    move-exception v1

    :try_start_8
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Error running "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, p1, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " compiler"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->location:Lorg/apache/tools/ant/Location;

    invoke-direct {v7, v8, v1, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_2
    move-exception v7

    move-object v4, v5

    goto/16 :goto_3

    :catch_2
    move-exception v1

    goto/16 :goto_2
.end method

.method protected getBootClassPath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_0
    const-string v1, "ignore"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Path;->concatSystemBootClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    return-object v1
.end method

.method protected getCompileClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v0, v2}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->destDir:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->destDir:Ljava/io/File;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->setLocation(Ljava/io/File;)V

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/types/Path;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    :cond_1
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->includeAntRuntime:Z

    if-eqz v2, :cond_3

    const-string v2, "last"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->addExisting(Lorg/apache/tools/ant/types/Path;)V

    :goto_0
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->includeJavaRuntime:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->addJavaRuntime()V

    :cond_2
    return-object v0

    :cond_3
    const-string v2, "ignore"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->addExisting(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public getJavac()Lorg/apache/tools/ant/taskdefs/Javac;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    return-object v0
.end method

.method protected getNoDebugArgument()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava11()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "-g:none"

    goto :goto_0
.end method

.method protected getProject()Lorg/apache/tools/ant/Project;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method protected logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v6, 0x3

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Compilation "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->describeArguments()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "File"

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileList:[Ljava/io/File;

    array-length v3, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v3, " to be compiled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileList:[Ljava/io/File;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileList:[Ljava/io/File;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    const-string v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public setJavac(Lorg/apache/tools/ant/taskdefs/Javac;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Javac;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getSrcdir()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->src:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getDestdir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->destDir:Ljava/io/File;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getEncoding()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->encoding:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getDebug()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->debug:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getOptimize()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->optimize:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getDeprecation()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->deprecation:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getDepend()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->depend:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getVerbose()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->verbose:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getTarget()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->target:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getBootclasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->bootclasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getExtdirs()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getFileList()[Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileList:[Ljava/io/File;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getSourcepath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->location:Lorg/apache/tools/ant/Location;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getIncludeantruntime()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->includeAntRuntime:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getIncludejavaruntime()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->includeJavaRuntime:Z

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getMemoryInitialSize()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryInitialSize:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Javac;->getMemoryMaximumSize()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryMaximumSize:Ljava/lang/String;

    return-void
.end method

.method protected setupJavacCommand()Lorg/apache/tools/ant/types/Commandline;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->setupJavacCommand(Z)Lorg/apache/tools/ant/types/Commandline;

    move-result-object v0

    return-object v0
.end method

.method protected setupJavacCommand(Z)Lorg/apache/tools/ant/types/Commandline;
    .locals 1
    .param p1    # Z

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    invoke-virtual {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->setupJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;Z)Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    return-object v0
.end method

.method protected setupJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;)Lorg/apache/tools/ant/types/Commandline;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->setupJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;Z)Lorg/apache/tools/ant/types/Commandline;

    move-result-object v0

    return-object v0
.end method

.method protected setupJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;Z)Lorg/apache/tools/ant/types/Commandline;
    .locals 10
    .param p1    # Lorg/apache/tools/ant/types/Commandline;
    .param p2    # Z

    const/4 v9, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    if-eqz v6, :cond_d

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava11()Z

    move-result v6

    if-eqz v6, :cond_e

    const-string v4, "-J-"

    :goto_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryInitialSize:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Javac;->isForkedJavac()Z

    move-result v6

    if-nez v6, :cond_f

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v7, "Since fork is false, ignoring memoryInitialSize setting."

    invoke-virtual {v6, v7, v9}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    :cond_0
    :goto_2
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryMaximumSize:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Javac;->isForkedJavac()Z

    move-result v6

    if-nez v6, :cond_10

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v7, "Since fork is false, ignoring memoryMaximumSize setting."

    invoke-virtual {v6, v7, v9}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_3
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Javac;->getNowarn()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-nowarn"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_2
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->deprecation:Z

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-deprecation"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_3
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->destDir:Ljava/io/File;

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-d"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->destDir:Ljava/io/File;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    :cond_4
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-classpath"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava11()Z

    move-result v6

    if-eqz v6, :cond_11

    new-instance v2, Lorg/apache/tools/ant/types/Path;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v2, v6}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getBootClassPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v6

    if-lez v6, :cond_5

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_5
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/types/Path;->addExtdirs(Lorg/apache/tools/ant/types/Path;)V

    :cond_6
    invoke-virtual {v2, v1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {v2, v5}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    invoke-virtual {v6, v2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_7
    :goto_4
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->encoding:Ljava/lang/String;

    if-eqz v6, :cond_8

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-encoding"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->encoding:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_8
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->debug:Z

    if-eqz v6, :cond_17

    if-eqz p2, :cond_16

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava11()Z

    move-result v6

    if-nez v6, :cond_16

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Javac;->getDebugLevel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_15

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "-g:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_9
    :goto_5
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->optimize:Z

    if-eqz v6, :cond_a

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-O"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_a
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->depend:Z

    if-eqz v6, :cond_b

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava11()Z

    move-result v6

    if-eqz v6, :cond_18

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-depend"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_b
    :goto_6
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->verbose:Z

    if-eqz v6, :cond_c

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-verbose"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->addCurrentCompilerArgs(Lorg/apache/tools/ant/types/Commandline;)V

    return-object p1

    :cond_d
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->src:Lorg/apache/tools/ant/types/Path;

    goto/16 :goto_0

    :cond_e
    const-string v4, "-J-X"

    goto/16 :goto_1

    :cond_f
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryInitialSize:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_10
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "mx"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->memoryMaximumSize:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_11
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    invoke-virtual {v6, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v6

    if-lez v6, :cond_12

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-sourcepath"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    invoke-virtual {v6, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_12
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->target:Ljava/lang/String;

    if-eqz v6, :cond_13

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-target"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->target:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getBootClassPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v6

    if-lez v6, :cond_14

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-bootclasspath"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    invoke-virtual {v6, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_14
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v6

    if-lez v6, :cond_7

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-extdirs"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    goto/16 :goto_4

    :cond_15
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-g"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_16
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-g"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_17
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getNoDebugArgument()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->getNoDebugArgument()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_18
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava12()Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v6

    const-string v7, "-Xdepend"

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_19
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v7, "depend attribute is not supported by the modern compiler"

    invoke-virtual {v6, v7, v9}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    goto/16 :goto_6
.end method

.method protected setupModernJavacCommand()Lorg/apache/tools/ant/types/Commandline;
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->setupModernJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;)Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    return-object v0
.end method

.method protected setupModernJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;)Lorg/apache/tools/ant/types/Commandline;
    .locals 7
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v6, 0x1

    invoke-virtual {p0, p1, v6}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->setupJavacCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;Z)Lorg/apache/tools/ant/types/Commandline;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Javac;->getSource()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava13()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    const-string v4, "-source"

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Javac;->getSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava14()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava15()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const-string v3, "1.1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "1.2"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    const-string v4, "1.3"

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-object p1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava15()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->assumeJava16()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_5
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Javac;->getTarget()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Javac;->getTarget()Ljava/lang/String;

    move-result-object v2

    const-string v3, "1.1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "1.2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "1.3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "1.4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_6
    move-object v0, v2

    const-string v3, "1.1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v0, "1.2"

    :cond_7
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v4, ""

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v4, "          WARNING"

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v4, ""

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v4, "The -source switch defaults to 1.5 in JDK 1.5 and 1.6."

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "If you specify -target "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " you now must also specify -source "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Ant will implicitly add -source "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " for you.  Please change your build file."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    const-string v4, "-source"

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
