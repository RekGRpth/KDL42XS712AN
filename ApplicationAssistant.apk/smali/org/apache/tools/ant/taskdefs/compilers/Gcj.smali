.class public Lorg/apache/tools/ant/taskdefs/compilers/Gcj;
.super Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.source "Gcj.java"


# static fields
.field private static final CONFLICT_WITH_DASH_C:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "-o"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "--main="

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "-D"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "-fjni"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "-L"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->CONFLICT_WITH_DASH_C:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v3, "Using gcj compiler"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->setupGCJCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->size()I

    move-result v1

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->executeExternalCompile([Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isNativeBuild()Z
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Javac;->getCurrentCompilerArgs()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-nez v3, :cond_1

    array-length v4, v0

    if-ge v1, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    if-nez v3, :cond_0

    sget-object v4, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->CONFLICT_WITH_DASH_C:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_0

    aget-object v4, v0, v1

    sget-object v5, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->CONFLICT_WITH_DASH_C:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method protected setupGCJCommand()Lorg/apache/tools/ant/types/Commandline;
    .locals 7

    new-instance v1, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v0, v4}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->getBootClassPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Path;->addExtdirs(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Javac;->getExecutable()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "gcj"

    :cond_1
    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->destDir:Ljava/io/File;

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    const-string v5, "-d"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->destDir:Ljava/io/File;

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->destDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->destDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "Can\'t make output directories. Maybe permission is wrong. "

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->src:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    const-string v5, "-classpath"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->encoding:Ljava/lang/String;

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "--encoding="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->encoding:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_4
    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->debug:Z

    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    const-string v5, "-g1"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_5
    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->optimize:Z

    if-eqz v4, :cond_6

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    const-string v5, "-O"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->isNativeBuild()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    const-string v5, "-C"

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;->addCurrentCompilerArgs(Lorg/apache/tools/ant/types/Commandline;)V

    return-object v1
.end method
