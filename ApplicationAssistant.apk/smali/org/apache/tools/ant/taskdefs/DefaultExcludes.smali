.class public Lorg/apache/tools/ant/taskdefs/DefaultExcludes;
.super Lorg/apache/tools/ant/Task;
.source "DefaultExcludes.java"


# instance fields
.field private add:Ljava/lang/String;

.field private defaultrequested:Z

.field private echo:Z

.field private logLevel:I

.field private remove:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->add:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->remove:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->defaultrequested:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->echo:Z

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->logLevel:I

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->defaultrequested:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->add:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->remove:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->echo:Z

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    const-string v4, "<defaultexcludes> task must set at least one attribute (echo=\"false\" doesn\'t count since that is the default"

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->defaultrequested:Z

    if-eqz v3, :cond_1

    invoke-static {}, Lorg/apache/tools/ant/DirectoryScanner;->resetDefaultExcludes()V

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->add:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->add:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/tools/ant/DirectoryScanner;->addDefaultExclude(Ljava/lang/String;)Z

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->remove:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->remove:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/tools/ant/DirectoryScanner;->removeDefaultExclude(Ljava/lang/String;)Z

    :cond_3
    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->echo:Z

    if-eqz v3, :cond_5

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Current Default Excludes:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {}, Lorg/apache/tools/ant/DirectoryScanner;->getDefaultExcludes()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_4

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->logLevel:I

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->log(Ljava/lang/String;I)V

    :cond_5
    return-void
.end method

.method public setAdd(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->add:Ljava/lang/String;

    return-void
.end method

.method public setDefault(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->defaultrequested:Z

    return-void
.end method

.method public setEcho(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->echo:Z

    return-void
.end method

.method public setRemove(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/DefaultExcludes;->remove:Ljava/lang/String;

    return-void
.end method
