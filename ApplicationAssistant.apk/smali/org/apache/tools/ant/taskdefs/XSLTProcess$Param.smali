.class public Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;
.super Ljava/lang/Object;
.source "XSLTProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/XSLTProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Param"
.end annotation


# instance fields
.field private expression:Ljava/lang/String;

.field private ifProperty:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private project:Lorg/apache/tools/ant/Project;

.field private unlessProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->expression:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getExpression()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->expression:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Expression attribute is missing."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->expression:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Name attribute is missing."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setExpression(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->expression:Ljava/lang/String;

    return-void
.end method

.method public setIf(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->ifProperty:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->name:Ljava/lang/String;

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method public setUnless(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->unlessProperty:Ljava/lang/String;

    return-void
.end method

.method public shouldUse()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->ifProperty:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->project:Lorg/apache/tools/ant/Project;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->ifProperty:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->unlessProperty:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->project:Lorg/apache/tools/ant/Project;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/XSLTProcess$Param;->unlessProperty:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
