.class public Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;
.super Ljava/lang/Object;
.source "Javadoc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Javadoc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResourceCollectionContainer"
.end annotation


# instance fields
.field private rcs:Ljava/util/ArrayList;

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Javadoc;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/taskdefs/Javadoc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->this$0:Lorg/apache/tools/ant/taskdefs/Javadoc;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->rcs:Ljava/util/ArrayList;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;)Ljava/util/Iterator;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->rcs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Javadoc$ResourceCollectionContainer;->rcs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method
