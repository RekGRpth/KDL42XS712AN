.class public Lorg/apache/tools/ant/taskdefs/Tar;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Tar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;,
        Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;,
        Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;
    }
.end annotation


# static fields
.field public static final FAIL:Ljava/lang/String; = "fail"

.field public static final GNU:Ljava/lang/String; = "gnu"

.field public static final OMIT:Ljava/lang/String; = "omit"

.field public static final TRUNCATE:Ljava/lang/String; = "truncate"

.field public static final WARN:Ljava/lang/String; = "warn"

.field static class$org$apache$tools$ant$taskdefs$Tar:Ljava/lang/Class;


# instance fields
.field baseDir:Ljava/io/File;

.field private compression:Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

.field fileSetFiles:Ljava/util/Vector;

.field filesets:Ljava/util/Vector;

.field private longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

.field private longWarningGiven:Z

.field private resourceCollections:Ljava/util/Vector;

.field tarFile:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->resourceCollections:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->fileSetFiles:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longWarningGiven:Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->compression:Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static final getFileNames(Lorg/apache/tools/ant/types/FileSet;)[Ljava/lang/String;
    .locals 7
    .param p0    # Lorg/apache/tools/ant/types/FileSet;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v0

    array-length v5, v3

    add-int/2addr v4, v5

    new-array v2, v4, [Ljava/lang/String;

    array-length v4, v0

    invoke-static {v0, v6, v2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v0

    array-length v5, v3

    invoke-static {v3, v6, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method

.method protected static final isFileFileSet(Lorg/apache/tools/ant/types/ResourceCollection;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/ResourceCollection;

    instance-of v0, p0, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->resourceCollections:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected archiveIsUpToDate(Lorg/apache/tools/ant/types/Resource;)Z
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v1

    invoke-static {v0, p1, v1, v2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->isOutOfDate(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;J)Z

    move-result v0

    return v0
.end method

.method protected archiveIsUpToDate([Ljava/lang/String;)Z
    .locals 1
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->baseDir:Ljava/io/File;

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Tar;->archiveIsUpToDate([Ljava/lang/String;Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method protected archiveIsUpToDate([Ljava/lang/String;Ljava/io/File;)Z
    .locals 3
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;

    new-instance v1, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v1, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    new-instance v0, Lorg/apache/tools/ant/util/MergingMapper;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/MergingMapper;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/util/MergingMapper;->setTo(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2, v0}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected asTarFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/ArchiveFileSet;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    instance-of v2, p1, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    if-eqz v2, :cond_1

    move-object v1, p1

    check-cast v1, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setPrefix(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setFullpath(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasFileModeBeenSet()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFileMode(Lorg/apache/tools/ant/Project;)I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->integerSetFileMode(I)V

    :cond_2
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDirModeBeenSet()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getDirMode(Lorg/apache/tools/ant/Project;)I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->integerSetDirMode(I)V

    :cond_3
    instance-of v2, p1, Lorg/apache/tools/ant/types/TarFileSet;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->hasUserNameBeenSet()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setUserName(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->hasGroupBeenSet()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getGroup()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setGroup(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->hasUserIdBeenSet()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getUid()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setUid(I)V

    :cond_6
    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->hasGroupIdBeenSet()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getGid()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setGid(I)V

    goto/16 :goto_0
.end method

.method protected check(Ljava/io/File;[Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, p2, p1}, Lorg/apache/tools/ant/taskdefs/Tar;->archiveIsUpToDate([Ljava/lang/String;Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    new-instance v3, Ljava/io/File;

    aget-object v4, p2, v0

    invoke-direct {v3, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "A tar file cannot include itself"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method protected check(Lorg/apache/tools/ant/types/ResourceCollection;)Z
    .locals 11
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    const/4 v8, 0x1

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/Tar;->isFileFileSet(Lorg/apache/tools/ant/types/ResourceCollection;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object v5, p1

    check-cast v5, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v5, v9}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v9

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/Tar;->getFileNames(Lorg/apache/tools/ant/types/FileSet;)[Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Tar;->check(Ljava/io/File;[Ljava/lang/String;)Z

    move-result v8

    :cond_0
    return v8

    :cond_1
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->supportsNonFileResources()Z

    move-result v9

    if-nez v9, :cond_2

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    const-string v10, "only filesystem resources are supported"

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_2
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v9

    if-eqz v9, :cond_7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    :cond_3
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    if-nez v4, :cond_4

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v1, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    move-object v4, v9

    check-cast v4, [Ljava/lang/String;

    sget-object v9, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    if-ne v0, v9, :cond_6

    const/4 v0, 0x0

    :cond_6
    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/Tar;->check(Ljava/io/File;[Ljava/lang/String;)Z

    move-result v9

    and-int/2addr v8, v9

    goto :goto_1

    :cond_7
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/taskdefs/Tar;->archiveIsUpToDate(Lorg/apache/tools/ant/types/Resource;)Z

    move-result v9

    and-int/2addr v8, v9

    goto :goto_2
.end method

.method public createTarFileSet()Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    if-nez v8, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "tarfile attribute must be set!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "tarfile is a directory!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v8

    if-nez v8, :cond_2

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Can not write to the specified tarfile!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    :try_start_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->baseDir:Ljava/io/File;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->baseDir:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "basedir does not exist!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v8

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    throw v8

    :cond_3
    :try_start_1
    new-instance v2, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v2, v8}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->baseDir:Ljava/io/File;

    invoke-virtual {v2, v8}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->setDir(Ljava/io/File;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v8, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_4
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-nez v8, :cond_5

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->resourceCollections:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-nez v8, :cond_5

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "You must supply either a basedir attribute or some nested resource collections."

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_5
    const/4 v7, 0x1

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/Tar;->check(Lorg/apache/tools/ant/types/ResourceCollection;)Z

    move-result v8

    and-int/2addr v7, v8

    goto :goto_0

    :cond_6
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->resourceCollections:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/Tar;->check(Lorg/apache/tools/ant/types/ResourceCollection;)Z

    move-result v8

    and-int/2addr v7, v8

    goto :goto_1

    :cond_7
    if-eqz v7, :cond_8

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Nothing to do: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " is up to date."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    :goto_2
    return-void

    :cond_8
    :try_start_2
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Building tar: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v5, 0x0

    :try_start_3
    new-instance v6, Lorg/apache/tools/tar/TarOutputStream;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->compression:Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

    new-instance v9, Ljava/io/BufferedOutputStream;

    new-instance v10, Ljava/io/FileOutputStream;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v9, v10}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v8, v9}, Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;->access$000(Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v6, v8}, Lorg/apache/tools/tar/TarOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v8, 0x1

    :try_start_4
    invoke-virtual {v6, v8}, Lorg/apache/tools/tar/TarOutputStream;->setDebug(Z)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isTruncateMode()Z

    move-result v8

    if-eqz v8, :cond_9

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lorg/apache/tools/tar/TarOutputStream;->setLongFileMode(I)V

    :goto_3
    const/4 v8, 0x0

    iput-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longWarningGiven:Z

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    invoke-virtual {p0, v8, v6}, Lorg/apache/tools/ant/taskdefs/Tar;->tar(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/tar/TarOutputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_4

    :catch_0
    move-exception v1

    move-object v5, v6

    :goto_5
    :try_start_5
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Problem creating TAR: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v8, v3, v1, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v8

    :goto_6
    :try_start_6
    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_9
    :try_start_7
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isFailMode()Z

    move-result v8

    if-nez v8, :cond_a

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isOmitMode()Z

    move-result v8

    if-eqz v8, :cond_b

    :cond_a
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lorg/apache/tools/tar/TarOutputStream;->setLongFileMode(I)V

    goto :goto_3

    :catchall_2
    move-exception v8

    move-object v5, v6

    goto :goto_6

    :cond_b
    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lorg/apache/tools/tar/TarOutputStream;->setLongFileMode(I)V

    goto :goto_3

    :cond_c
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Tar;->resourceCollections:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_d

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0, v8, v6}, Lorg/apache/tools/ant/taskdefs/Tar;->tar(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/tar/TarOutputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_7

    :cond_d
    :try_start_8
    invoke-static {v6}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tar;->filesets:Ljava/util/Vector;

    goto/16 :goto_2

    :catch_1
    move-exception v1

    goto :goto_5
.end method

.method public setBasedir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->baseDir:Ljava/io/File;

    return-void
.end method

.method public setCompression(Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->compression:Lorg/apache/tools/ant/taskdefs/Tar$TarCompressionMethod;

    return-void
.end method

.method public setDestFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    return-void
.end method

.method public setLongfile(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DEPRECATED - The setLongfile(String) method has been deprecated. Use setLongfile(Tar.TarLongFileMode) instead."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;)V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setLongfile(Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    return-void
.end method

.method public setTarfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    return-void
.end method

.method protected supportsNonFileResources()Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Tar;->class$org$apache$tools$ant$taskdefs$Tar:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.taskdefs.Tar"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Tar;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Tar;->class$org$apache$tools$ant$taskdefs$Tar:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/Tar;->class$org$apache$tools$ant$taskdefs$Tar:Ljava/lang/Class;

    goto :goto_0
.end method

.method protected tar(Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/tar/TarOutputStream;)V
    .locals 12
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Lorg/apache/tools/tar/TarOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    instance-of v9, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;

    if-eqz v9, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v0, v9}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_1

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    const-string v10, "fullpath attribute may only be specified for filesets that specify a single file."

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Tar;->asTarFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;

    move-result-object v8

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/Tar;->isFileFileSet(Lorg/apache/tools/ant/types/ResourceCollection;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v3, p1

    check-cast v3, Lorg/apache/tools/ant/types/FileSet;

    invoke-static {v3}, Lorg/apache/tools/ant/taskdefs/Tar;->getFileNames(Lorg/apache/tools/ant/types/FileSet;)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    :goto_0
    array-length v9, v2

    if-ge v4, v9, :cond_5

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v3, v9}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v9

    aget-object v10, v2, v4

    invoke-direct {v1, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aget-object v9, v2, v4

    sget-char v10, Ljava/io/File;->separatorChar:C

    const/16 v11, 0x2f

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v1, p2, v6, v8}, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile(Ljava/io/File;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/io/File;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v1, p2, v9, v8}, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile(Ljava/io/File;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V

    goto :goto_1

    :cond_4
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v7, p2, v9, v8}, Lorg/apache/tools/ant/taskdefs/Tar;->tarResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V

    goto :goto_2

    :cond_5
    return-void
.end method

.method protected tarFile(Ljava/io/File;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/tar/TarOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tar;->tarFile:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0, p2, p3, p4}, Lorg/apache/tools/ant/taskdefs/Tar;->tarResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V

    goto :goto_0
.end method

.method protected tarResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/tar/TarOutputStream;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;)V
    .locals 14
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/tar/TarOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_4

    move-object/from16 p3, v4

    :goto_1
    const-string v10, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getPreserveLeadingSlashes()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v10, 0x1

    if-le v6, v10, :cond_0

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    :cond_2
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_3
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x64

    if-lt v10, v11, :cond_7

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isOmitMode()Z

    move-result v10

    if-eqz v10, :cond_6

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Omitting: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_5

    const-string v10, "/"

    invoke-virtual {v7, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_5
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    :cond_6
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isWarnMode()Z

    move-result v10

    if-eqz v10, :cond_9

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Entry: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " longer than "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const/16 v11, 0x64

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " characters."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;I)V

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longWarningGiven:Z

    if-nez v10, :cond_7

    const-string v10, "Resulting tar file can only be processed successfully by GNU compatible tar commands"

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/Tar;->log(Ljava/lang/String;I)V

    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longWarningGiven:Z

    :cond_7
    new-instance v8, Lorg/apache/tools/tar/TarEntry;

    move-object/from16 v0, p3

    invoke-direct {v8, v0}, Lorg/apache/tools/tar/TarEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lorg/apache/tools/tar/TarEntry;->setModTime(J)V

    instance-of v10, p1, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    if-eqz v10, :cond_8

    move-object v1, p1

    check-cast v1, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getMode()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setMode(I)V

    instance-of v10, p1, Lorg/apache/tools/ant/types/resources/TarResource;

    if-eqz v10, :cond_8

    move-object v9, p1

    check-cast v9, Lorg/apache/tools/ant/types/resources/TarResource;

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/resources/TarResource;->getUserName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setUserName(Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/resources/TarResource;->getUid()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setUserId(I)V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/resources/TarResource;->getGroup()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setGroupName(Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/resources/TarResource;->getGid()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setGroupId(I)V

    :cond_8
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_12

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->size()I

    move-result v10

    int-to-long v10, v10

    const-wide v12, 0x1ffffffffL

    cmp-long v10, v10, v12

    if-lez v10, :cond_a

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " larger than "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-wide v12, 0x1ffffffffL

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " bytes."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_9
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Tar;->longFileMode:Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;

    invoke-virtual {v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarLongFileMode;->isFailMode()Z

    move-result v10

    if-eqz v10, :cond_7

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Entry: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " longer than "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const/16 v12, 0x64

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "characters."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v10

    :cond_a
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lorg/apache/tools/tar/TarEntry;->setSize(J)V

    if-eqz p4, :cond_b

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasFileModeBeenSet()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getMode()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setMode(I)V

    :cond_b
    :goto_2
    if-eqz p4, :cond_f

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasUserNameBeenSet()Z

    move-result v10

    if-eqz v10, :cond_c

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getUserName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setUserName(Ljava/lang/String;)V

    :cond_c
    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasGroupBeenSet()Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getGroup()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setGroupName(Ljava/lang/String;)V

    :cond_d
    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasUserIdBeenSet()Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getUid()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setUserId(I)V

    :cond_e
    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasGroupIdBeenSet()Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getGid()I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setGroupId(I)V

    :cond_f
    const/4 v5, 0x0

    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/tools/tar/TarOutputStream;->putNextEntry(Lorg/apache/tools/tar/TarEntry;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_11

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    const/16 v10, 0x2000

    new-array v2, v10, [B

    const/4 v3, 0x0

    :cond_10
    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v10, v3}, Lorg/apache/tools/tar/TarOutputStream;->write([BII)V

    const/4 v10, 0x0

    array-length v11, v2

    invoke-virtual {v5, v2, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v10, -0x1

    if-ne v3, v10, :cond_10

    :cond_11
    invoke-virtual/range {p2 .. p2}, Lorg/apache/tools/tar/TarOutputStream;->closeEntry()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_0

    :cond_12
    if-eqz p4, :cond_b

    invoke-virtual/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->hasDirModeBeenSet()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Tar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/taskdefs/Tar$TarFileSet;->getDirMode(Lorg/apache/tools/ant/Project;)I

    move-result v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/tar/TarEntry;->setMode(I)V

    goto :goto_2

    :catchall_0
    move-exception v10

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v10
.end method
