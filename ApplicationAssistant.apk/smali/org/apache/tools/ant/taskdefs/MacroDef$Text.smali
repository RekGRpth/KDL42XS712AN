.class public Lorg/apache/tools/ant/taskdefs/MacroDef$Text;
.super Ljava/lang/Object;
.source "MacroDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/MacroDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Text"
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private optional:Z

.field private trim:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    if-nez v2, :cond_0

    :cond_2
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->optional:Z

    iget-boolean v3, v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->optional:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->trim:Z

    iget-boolean v3, v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->trim:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOptional()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->optional:Z

    return v0
.end method

.method public getTrim()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->trim:Z

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->access$100(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->description:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef;->access$000(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Illegal name ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "] for attribute"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->name:Ljava/lang/String;

    return-void
.end method

.method public setOptional(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->optional:Z

    return-void
.end method

.method public setTrim(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->trim:Z

    return-void
.end method
