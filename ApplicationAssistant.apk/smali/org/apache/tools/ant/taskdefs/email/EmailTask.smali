.class public Lorg/apache/tools/ant/taskdefs/email/EmailTask;
.super Lorg/apache/tools/ant/Task;
.source "EmailTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/email/EmailTask$Encoding;
    }
.end annotation


# static fields
.field public static final AUTO:Ljava/lang/String; = "auto"

.field public static final MIME:Ljava/lang/String; = "mime"

.field public static final PLAIN:Ljava/lang/String; = "plain"

.field public static final UU:Ljava/lang/String; = "uu"

.field static class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

.field static class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;


# instance fields
.field private attachments:Lorg/apache/tools/ant/types/Path;

.field private bccList:Ljava/util/Vector;

.field private ccList:Ljava/util/Vector;

.field private charset:Ljava/lang/String;

.field private encoding:Ljava/lang/String;

.field private failOnError:Z

.field private from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

.field private headers:Ljava/util/Vector;

.field private host:Ljava/lang/String;

.field private includeFileNames:Z

.field private message:Lorg/apache/tools/ant/taskdefs/email/Message;

.field private messageMimeType:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private port:I

.field private replyToList:Ljava/util/Vector;

.field private ssl:Z

.field private subject:Ljava/lang/String;

.field private toList:Ljava/util/Vector;

.field private user:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-string v0, "auto"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v0, "localhost"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->host:Ljava/lang/String;

    const/16 v0, 0x19

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->port:I

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->subject:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->failOnError:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->includeFileNames:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->messageMimeType:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->replyToList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->headers:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->charset:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->user:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->password:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ssl:Z

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public addBcc(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addCc(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->createAttachments()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFrom(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Emails can only be from one address"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    return-void
.end method

.method public addMessage(Lorg/apache/tools/ant/taskdefs/email/Message;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one message can be sent in an email"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    return-void
.end method

.method public addReplyTo(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->replyToList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addTo(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createAttachments()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createHeader()Lorg/apache/tools/ant/taskdefs/email/Header;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/email/Header;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/email/Header;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->headers:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public execute()V
    .locals 15

    const/4 v14, 0x1

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    const/4 v7, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "mime"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "auto"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    if-eqz v11, :cond_1

    if-nez v1, :cond_1

    :cond_0
    :try_start_1
    const-string v12, "org.apache.tools.ant.taskdefs.email.MimeMailer"

    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    if-nez v11, :cond_4

    const-string v11, "org.apache.tools.ant.taskdefs.email.EmailTask"

    invoke-static {v11}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    sput-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v11}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;

    if-nez v11, :cond_5

    const-string v11, "org.apache.tools.ant.taskdefs.email.Mailer"

    invoke-static {v11}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    sput-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;

    :goto_1
    invoke-static {v12, v13, v11}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lorg/apache/tools/ant/taskdefs/email/Mailer;

    move-object v7, v0

    const/4 v1, 0x1

    const-string v11, "Using MIME mail"

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    if-nez v1, :cond_7

    :try_start_2
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->user:Ljava/lang/String;

    if-nez v11, :cond_2

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->password:Ljava/lang/String;

    if-eqz v11, :cond_7

    :cond_2
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "uu"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "plain"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    :cond_3
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "SMTP auth only possible with MIME mail"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v3

    :goto_2
    :try_start_3
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    if-nez v11, :cond_1a

    move-object v10, v3

    :goto_3
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Failed to send email: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->failOnError:Z

    if-eqz v11, :cond_1b

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v11

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    throw v11

    :cond_4
    :try_start_4
    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    goto :goto_0

    :cond_5
    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;
    :try_end_4
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v3

    :try_start_5
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    if-nez v11, :cond_6

    move-object v10, v3

    :goto_4
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Failed to initialise MIME mail: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V
    :try_end_5
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    :goto_5
    return-void

    :cond_6
    :try_start_6
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;

    move-result-object v10

    goto :goto_4

    :cond_7
    if-nez v1, :cond_9

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ssl:Z

    if-eqz v11, :cond_9

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "uu"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_8

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "plain"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    :cond_8
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "SSL only possible with MIME mail"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_6
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v3

    :goto_6
    :try_start_7
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Failed to send email: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->failOnError:Z

    if-eqz v11, :cond_1c

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v11, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_9
    :try_start_8
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "uu"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_a

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "auto"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_8
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v11

    if-eqz v11, :cond_1e

    if-nez v1, :cond_1e

    :cond_a
    :try_start_9
    const-string v12, "org.apache.tools.ant.taskdefs.email.UUMailer"

    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    if-nez v11, :cond_c

    const-string v11, "org.apache.tools.ant.taskdefs.email.EmailTask"

    invoke-static {v11}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    sput-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    :goto_7
    invoke-virtual {v11}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;

    if-nez v11, :cond_d

    const-string v11, "org.apache.tools.ant.taskdefs.email.Mailer"

    invoke-static {v11}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    sput-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;

    :goto_8
    invoke-static {v12, v13, v11}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lorg/apache/tools/ant/taskdefs/email/Mailer;

    move-object v7, v0

    const/4 v1, 0x1

    const-string v11, "Using UU mail"

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V
    :try_end_9
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-object v8, v7

    :goto_9
    :try_start_a
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "plain"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_b

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    const-string v12, "auto"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1d

    if-nez v1, :cond_1d

    :cond_b
    new-instance v7, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;

    invoke-direct {v7}, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;-><init>()V
    :try_end_a
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const/4 v1, 0x1

    :try_start_b
    const-string v11, "Using plain mail"

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    :goto_a
    if-nez v7, :cond_f

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Failed to initialise encoding: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_b
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_c
    :try_start_c
    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$EmailTask:Ljava/lang/Class;

    goto :goto_7

    :cond_d
    sget-object v11, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->class$org$apache$tools$ant$taskdefs$email$Mailer:Ljava/lang/Class;
    :try_end_c
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_8

    :catch_3
    move-exception v3

    :try_start_d
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    if-nez v11, :cond_e

    move-object v10, v3

    :goto_b
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Failed to initialise UU mail: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V
    :try_end_d
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    goto/16 :goto_5

    :cond_e
    :try_start_e
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;

    move-result-object v10

    goto :goto_b

    :cond_f
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    if-nez v11, :cond_10

    new-instance v11, Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-direct {v11}, Lorg/apache/tools/ant/taskdefs/email/Message;-><init>()V

    iput-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v12

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/taskdefs/email/Message;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_10
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    if-eqz v11, :cond_11

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->getAddress()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_12

    :cond_11
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "A from element is required"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_12
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_13

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_13

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_13

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "At least one of to, cc or bcc must be supplied"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_13
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->messageMimeType:Ljava/lang/String;

    if-eqz v11, :cond_15

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/email/Message;->isMimeTypeSpecified()Z

    move-result v11

    if-eqz v11, :cond_14

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "The mime type can only be specified in one location"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_14
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->messageMimeType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/taskdefs/email/Message;->setMimeType(Ljava/lang/String;)V

    :cond_15
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->charset:Ljava/lang/String;

    if-eqz v11, :cond_17

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/email/Message;->getCharset()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_16

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "The charset can only be specified in one location"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_16
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->charset:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/taskdefs/email/Message;->setCharset(Ljava/lang/String;)V

    :cond_17
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    if-eqz v11, :cond_18

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->attachments:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v11}, Lorg/apache/tools/ant/types/Path;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_c

    :cond_18
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Sending email: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->subject:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "From "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "ReplyTo "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->replyToList:Ljava/util/Vector;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "To "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Cc "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Bcc "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->host:Ljava/lang/String;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setHost(Ljava/lang/String;)V

    iget v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->port:I

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setPort(I)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->user:Ljava/lang/String;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setUser(Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->password:Ljava/lang/String;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setPassword(Ljava/lang/String;)V

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ssl:Z

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setSSL(Z)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setMessage(Lorg/apache/tools/ant/taskdefs/email/Message;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setFrom(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->replyToList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setReplyToList(Ljava/util/Vector;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setToList(Ljava/util/Vector;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setCcList(Ljava/util/Vector;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setBccList(Ljava/util/Vector;)V

    invoke-virtual {v7, v4}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setFiles(Ljava/util/Vector;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->subject:Ljava/lang/String;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setSubject(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setTask(Lorg/apache/tools/ant/Task;)V

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->includeFileNames:Z

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setIncludeFileNames(Z)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->headers:Ljava/util/Vector;

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->setHeaders(Ljava/util/Vector;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/email/Mailer;->send()V

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v2

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Sent email with "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " attachment"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    if-ne v2, v14, :cond_19

    const-string v11, ""

    :goto_d
    invoke-virtual {v12, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->log(Ljava/lang/String;I)V
    :try_end_e
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    goto/16 :goto_5

    :cond_19
    :try_start_f
    const-string v11, "s"
    :try_end_f
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_d

    :cond_1a
    :try_start_10
    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getCause()Ljava/lang/Throwable;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-result-object v10

    goto/16 :goto_3

    :cond_1b
    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    goto/16 :goto_5

    :cond_1c
    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    goto/16 :goto_5

    :catch_4
    move-exception v3

    move-object v7, v8

    goto/16 :goto_6

    :catch_5
    move-exception v3

    move-object v7, v8

    goto/16 :goto_2

    :cond_1d
    move-object v7, v8

    goto/16 :goto_a

    :cond_1e
    move-object v8, v7

    goto/16 :goto_9
.end method

.method public getCharset()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->charset:Ljava/lang/String;

    return-object v0
.end method

.method public getIncludeFileNames()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->includeFileNames:Z

    return v0
.end method

.method public setBccList(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->bccList:Ljava/util/Vector;

    new-instance v2, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setCcList(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ccList:Ljava/util/Vector;

    new-instance v2, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setCharset(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->charset:Ljava/lang/String;

    return-void
.end method

.method public setEncoding(Lorg/apache/tools/ant/taskdefs/email/EmailTask$Encoding;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailTask$Encoding;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/email/EmailTask$Encoding;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFailOnError(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->failOnError:Z

    return-void
.end method

.method public setFiles(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ", "

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->createAttachments()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    new-instance v2, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Emails can only be from one address"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    return-void
.end method

.method public setIncludefilenames(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->includeFileNames:Z

    return-void
.end method

.method public setMailhost(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->host:Ljava/lang/String;

    return-void
.end method

.method public setMailport(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->port:I

    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one message can be sent in an email"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/taskdefs/email/Message;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/email/Message;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method public setMessageFile(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one message can be sent in an email"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/taskdefs/email/Message;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/email/Message;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method public setMessageMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->messageMimeType:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->password:Ljava/lang/String;

    return-void
.end method

.method public setReplyTo(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->replyToList:Ljava/util/Vector;

    new-instance v1, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-direct {v1, p1}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setSSL(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->ssl:Z

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->subject:Ljava/lang/String;

    return-void
.end method

.method public setToList(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->toList:Ljava/util/Vector;

    new-instance v2, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setUser(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailTask;->user:Ljava/lang/String;

    return-void
.end method
