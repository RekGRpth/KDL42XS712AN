.class Lorg/apache/tools/ant/taskdefs/email/PlainMailer;
.super Lorg/apache/tools/ant/taskdefs/email/Mailer;
.source "PlainMailer.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/email/Mailer;-><init>()V

    return-void
.end method


# virtual methods
.method protected attach(Ljava/io/File;Ljava/io/PrintStream;)V
    .locals 11
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "File \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\" does not exist or is not "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "readable."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->includeFileNames:Z

    if-eqz v8, :cond_3

    invoke-virtual {p2}, Ljava/io/PrintStream;->println()V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v2, :cond_2

    const/16 v8, 0x3d

    invoke-virtual {p2, v8}, Ljava/io/PrintStream;->print(C)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/io/PrintStream;->println()V

    :cond_3
    const/16 v6, 0x400

    const/16 v8, 0x400

    new-array v0, v8, [B

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    array-length v8, v0

    invoke-direct {v4, v3, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    :goto_1
    invoke-virtual {v4, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v5

    const/4 v8, -0x1

    if-eq v5, v8, :cond_4

    const/4 v8, 0x0

    invoke-virtual {p2, v0, v8, v5}, Ljava/io/PrintStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v8

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    throw v8

    :cond_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    return-void
.end method

.method public send()V
    .locals 8

    :try_start_0
    new-instance v3, Lorg/apache/tools/mail/MailMessage;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->host:Ljava/lang/String;

    iget v6, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->port:I

    invoke-direct {v3, v5, v6}, Lorg/apache/tools/mail/MailMessage;-><init>(Ljava/lang/String;I)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->from(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->replyToList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->replyto(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "IO error sending mail"

    invoke-direct {v5, v6, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->toList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->to(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->ccList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->cc(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->bccList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->bcc(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->subject:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->subject:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lorg/apache/tools/mail/MailMessage;->setSubject(Ljava/lang/String;)V

    :cond_4
    const-string v5, "Date"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/email/Message;->getCharset()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    const-string v5, "Content-Type"

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/email/Message;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "; charset=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/email/Message;->getCharset()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->headers:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/taskdefs/email/Header;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/email/Header;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/email/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    const-string v5, "Content-Type"

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/email/Message;->getMimeType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/apache/tools/mail/MailMessage;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v3}, Lorg/apache/tools/mail/MailMessage;->getPrintStream()Ljava/io/PrintStream;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    invoke-virtual {v5, v4}, Lorg/apache/tools/ant/taskdefs/email/Message;->print(Ljava/io/PrintStream;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->files:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {p0, v5, v4}, Lorg/apache/tools/ant/taskdefs/email/PlainMailer;->attach(Ljava/io/File;Ljava/io/PrintStream;)V

    goto :goto_6

    :cond_7
    invoke-virtual {v3}, Lorg/apache/tools/mail/MailMessage;->sendAndClose()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method
