.class public Lorg/apache/tools/ant/taskdefs/email/Message;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "Message.java"


# instance fields
.field private buffer:Ljava/lang/StringBuffer;

.field private charset:Ljava/lang/String;

.field private messageSource:Ljava/io/File;

.field private mimeType:Ljava/lang/String;

.field private specified:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->buffer:Ljava/lang/StringBuffer;

    const-string v0, "text/plain"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->mimeType:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->specified:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->buffer:Ljava/lang/StringBuffer;

    const-string v0, "text/plain"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->mimeType:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->specified:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->buffer:Ljava/lang/StringBuffer;

    const-string v0, "text/plain"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->mimeType:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->specified:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/email/Message;->addText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method public getCharset()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public isMimeTypeSpecified()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->specified:Z

    return v0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 7
    .param p1    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    if-eqz v4, :cond_0

    new-instance v3, Ljava/io/PrintWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    invoke-direct {v4, p1, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    if-eqz v4, :cond_2

    new-instance v0, Ljava/io/FileReader;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/Message;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    invoke-virtual {v4, v2}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Ljava/io/FileReader;->close()V

    throw v4

    :cond_0
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, p1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V

    :goto_2
    invoke-virtual {v3}, Ljava/io/PrintWriter;->flush()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/email/Message;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->buffer:Ljava/lang/StringBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setCharset(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->charset:Ljava/lang/String;

    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->mimeType:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->specified:Z

    return-void
.end method

.method public setSrc(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Message;->messageSource:Ljava/io/File;

    return-void
.end method
