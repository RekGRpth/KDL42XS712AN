.class public Lorg/apache/tools/ant/taskdefs/LoadProperties;
.super Lorg/apache/tools/ant/Task;
.source "LoadProperties.java"


# instance fields
.field private encoding:Ljava/lang/String;

.field private final filterChains:Ljava/util/Vector;

.field private src:Lorg/apache/tools/ant/types/Resource;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->filterChains:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    return-void
.end method

.method private assertSrcIsJavaResource()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/JavaResource;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Resource;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "expected a java resource as source"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only a single source is supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only single argument resource collections are supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public final addFilterChain(Lorg/apache/tools/ant/types/FilterChain;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FilterChain;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->filterChains:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->assertSrcIsJavaResource()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/JavaResource;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public final execute()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v12, :cond_0

    new-instance v12, Lorg/apache/tools/ant/BuildException;

    const-string v13, "A source resource is required."

    invoke-direct {v12, v13}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v12

    :cond_0
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v12}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v12

    if-nez v12, :cond_2

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    instance-of v12, v12, Lorg/apache/tools/ant/types/resources/JavaResource;

    if-eqz v12, :cond_1

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Unable to find resource "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {p0, v12, v13}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_1
    new-instance v12, Lorg/apache/tools/ant/BuildException;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Source resource does not exist: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    iget-object v14, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v12

    :cond_2
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v12}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    invoke-direct {v1, v12}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    if-nez v12, :cond_5

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v3, v4

    :goto_1
    new-instance v2, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->filterChains:Ljava/util/Vector;

    invoke-virtual {v2, v12}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v12

    invoke-virtual {v2, v12}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->readFully(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_4

    const-string v12, "\n"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    :cond_3
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    if-nez v12, :cond_6

    new-instance v11, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object v10, v11

    :goto_2
    new-instance v8, Ljava/util/Properties;

    invoke-direct {v8}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v8, v10}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    new-instance v7, Lorg/apache/tools/ant/taskdefs/Property;

    invoke-direct {v7}, Lorg/apache/tools/ant/taskdefs/Property;-><init>()V

    invoke-virtual {v7, p0}, Lorg/apache/tools/ant/taskdefs/Property;->bindToOwner(Lorg/apache/tools/ant/Task;)V

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/taskdefs/Property;->addProperties(Ljava/util/Properties;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_4
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_0

    :cond_5
    :try_start_2
    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    invoke-direct {v4, v1, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object v3, v4

    goto :goto_1

    :cond_6
    new-instance v11, Ljava/io/ByteArrayInputStream;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v10, v11

    goto :goto_2

    :catch_0
    move-exception v5

    :goto_3
    :try_start_3
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Unable to load file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v12, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v13

    invoke-direct {v12, v6, v5, v13}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v12

    :goto_4
    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v12

    :catchall_1
    move-exception v12

    move-object v0, v1

    goto :goto_4

    :catch_1
    move-exception v5

    move-object v0, v1

    goto :goto_3
.end method

.method public getClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->assertSrcIsJavaResource()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->assertSrcIsJavaResource()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/JavaResource;->setClasspath(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->assertSrcIsJavaResource()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/JavaResource;->setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public final setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setResource(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->assertSrcIsJavaResource()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/LoadProperties;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/JavaResource;->setName(Ljava/lang/String;)V

    return-void
.end method

.method public final setSrcFile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/LoadProperties;->addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method
