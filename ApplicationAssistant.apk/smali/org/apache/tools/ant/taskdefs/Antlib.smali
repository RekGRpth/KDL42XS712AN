.class public Lorg/apache/tools/ant/taskdefs/Antlib;
.super Lorg/apache/tools/ant/Task;
.source "Antlib.java"

# interfaces
.implements Lorg/apache/tools/ant/TaskContainer;


# static fields
.field public static final TAG:Ljava/lang/String; = "antlib"

.field static class$org$apache$tools$ant$taskdefs$Antlib:Ljava/lang/Class;


# instance fields
.field private classLoader:Ljava/lang/ClassLoader;

.field private tasks:Ljava/util/List;

.field private uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->uri:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->tasks:Ljava/util/List;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static createAntlib(Lorg/apache/tools/ant/Project;Ljava/net/URL;Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/Antlib;
    .locals 8
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # Ljava/net/URL;
    .param p2    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URLConnection;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {p0}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/apache/tools/ant/ComponentHelper;->enterAntLib(Ljava/lang/String;)V

    :try_start_1
    new-instance v3, Lorg/apache/tools/ant/helper/ProjectHelper2;

    invoke-direct {v3}, Lorg/apache/tools/ant/helper/ProjectHelper2;-><init>()V

    invoke-virtual {v3, p0, p1}, Lorg/apache/tools/ant/helper/ProjectHelper2;->parseUnknownElement(Lorg/apache/tools/ant/Project;Ljava/net/URL;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v5

    const-string v6, "antlib"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Unexpected tag "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " expecting "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "antlib"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Lorg/apache/tools/ant/ComponentHelper;->exitAntLib()V

    throw v5

    :catch_0
    move-exception v1

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Unable to find "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    :cond_0
    :try_start_2
    new-instance v0, Lorg/apache/tools/ant/taskdefs/Antlib;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Antlib;-><init>()V

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/taskdefs/Antlib;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/taskdefs/Antlib;->setLocation(Lorg/apache/tools/ant/Location;)V

    const-string v5, "antlib"

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/taskdefs/Antlib;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Antlib;->init()V

    invoke-virtual {v4, v0}, Lorg/apache/tools/ant/UnknownElement;->configure(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v2}, Lorg/apache/tools/ant/ComponentHelper;->exitAntLib()V

    return-object v0
.end method

.method private getClassLoader()Ljava/lang/ClassLoader;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->classLoader:Ljava/lang/ClassLoader;

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Antlib;->class$org$apache$tools$ant$taskdefs$Antlib:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "org.apache.tools.ant.taskdefs.Antlib"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Antlib;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Antlib;->class$org$apache$tools$ant$taskdefs$Antlib:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->classLoader:Ljava/lang/ClassLoader;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->classLoader:Ljava/lang/ClassLoader;

    return-object v0

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/taskdefs/Antlib;->class$org$apache$tools$ant$taskdefs$Antlib:Ljava/lang/Class;

    goto :goto_0
.end method


# virtual methods
.method public addTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->tasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public execute()V
    .locals 7

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->tasks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/Antlib;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->maybeConfigure()V

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getRealThing()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v4, v0, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;

    if-nez v4, :cond_1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid task in antlib "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " does not "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "extend org.apache.tools.ant.taskdefs.AntlibDefinition"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    move-object v1, v0

    check-cast v1, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->uri:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;->setURI(Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Antlib;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;->setAntlibClassLoader(Ljava/lang/ClassLoader;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;->init()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;->execute()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected setClassLoader(Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Ljava/lang/ClassLoader;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->classLoader:Ljava/lang/ClassLoader;

    return-void
.end method

.method protected setURI(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Antlib;->uri:Ljava/lang/String;

    return-void
.end method
