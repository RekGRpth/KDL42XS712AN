.class public Lorg/apache/tools/ant/taskdefs/Echo;
.super Lorg/apache/tools/ant/Task;
.source "Echo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Echo$EchoLevel;
    }
.end annotation


# instance fields
.field protected append:Z

.field private encoding:Ljava/lang/String;

.field protected file:Ljava/io/File;

.field protected logLevel:I

.field protected message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->file:Ljava/io/File;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->append:Z

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->encoding:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->logLevel:I

    return-void
.end method


# virtual methods
.method public addText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Echo;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    return-void
.end method

.method public execute()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->file:Ljava/io/File;

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Echo;->logLevel:I

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Echo;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->file:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->encoding:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->encoding:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    new-instance v3, Ljava/io/FileWriter;

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->append:Z

    invoke-direct {v3, v0, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    move-object v2, v3

    :goto_1
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Ljava/io/Writer;->write(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    goto :goto_0

    :cond_2
    :try_start_1
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    new-instance v5, Ljava/io/FileOutputStream;

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Echo;->append:Z

    invoke-direct {v5, v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Echo;->encoding:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v3

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Echo;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v4

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    throw v4
.end method

.method public setAppend(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Echo;->append:Z

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Echo;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Echo;->file:Ljava/io/File;

    return-void
.end method

.method public setLevel(Lorg/apache/tools/ant/taskdefs/Echo$EchoLevel;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Echo$EchoLevel;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Echo$EchoLevel;->getLevel()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Echo;->logLevel:I

    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Echo;->message:Ljava/lang/String;

    return-void
.end method
