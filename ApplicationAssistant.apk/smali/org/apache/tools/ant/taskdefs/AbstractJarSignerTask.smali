.class public abstract Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;
.super Lorg/apache/tools/ant/Task;
.source "AbstractJarSignerTask.java"


# static fields
.field public static final ERROR_NO_SOURCE:Ljava/lang/String; = "jar must be set through jar attribute or nested filesets"

.field protected static final JARSIGNER_COMMAND:Ljava/lang/String; = "jarsigner"


# instance fields
.field protected alias:Ljava/lang/String;

.field protected filesets:Ljava/util/Vector;

.field protected jar:Ljava/io/File;

.field protected keypass:Ljava/lang/String;

.field protected keystore:Ljava/lang/String;

.field protected maxMemory:Ljava/lang/String;

.field private path:Lorg/apache/tools/ant/types/Path;

.field private redirector:Lorg/apache/tools/ant/types/RedirectorElement;

.field protected storepass:Ljava/lang/String;

.field protected storetype:Ljava/lang/String;

.field private sysProperties:Lorg/apache/tools/ant/types/Environment;

.field protected verbose:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->filesets:Ljava/util/Vector;

    new-instance v0, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->sysProperties:Lorg/apache/tools/ant/types/Environment;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method private createRedirector()Lorg/apache/tools/ant/types/RedirectorElement;
    .locals 5

    const/16 v4, 0xa

    new-instance v1, Lorg/apache/tools/ant/types/RedirectorElement;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/RedirectorElement;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storepass:Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storepass:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keypass:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keypass:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/RedirectorElement;->setInputString(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/RedirectorElement;->setLogInputString(Z)V

    :cond_1
    return-object v1
.end method


# virtual methods
.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addSysproperty(Lorg/apache/tools/ant/types/Environment$Variable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Environment$Variable;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->sysProperties:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    return-void
.end method

.method protected addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecTask;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->createArg()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method protected beginExecution()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->createRedirector()Lorg/apache/tools/ant/types/RedirectorElement;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->redirector:Lorg/apache/tools/ant/types/RedirectorElement;

    return-void
.end method

.method protected bindToKeystore(Lorg/apache/tools/ant/taskdefs/ExecTask;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecTask;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keystore:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "-keystore"

    invoke-virtual {p0, p1, v2}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keystore:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0, p1, v1}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storetype:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "-storetype"

    invoke-virtual {p0, p1, v2}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storetype:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keystore:Ljava/lang/String;

    goto :goto_0
.end method

.method protected createJarSigner()Lorg/apache/tools/ant/taskdefs/ExecTask;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/ExecTask;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;-><init>(Lorg/apache/tools/ant/Task;)V

    const-string v1, "jarsigner"

    invoke-static {v1}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJdkExecutable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setExecutable(Ljava/lang/String;)V

    const-string v1, "jarsigner"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setTaskType(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setFailonerror(Z)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->redirector:Lorg/apache/tools/ant/types/RedirectorElement;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->addConfiguredRedirector(Lorg/apache/tools/ant/types/RedirectorElement;)V

    return-object v0
.end method

.method public createPath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method protected createUnifiedSourcePath()Lorg/apache/tools/ant/types/Path;
    .locals 4

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    if-nez v3, :cond_0

    new-instance v1, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->createUnifiedSources()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Path;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/Path;

    move-object v1, v3

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method protected createUnifiedSources()Ljava/util/Vector;
    .locals 3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->filesets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->jar:Ljava/io/File;

    if-eqz v2, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FileSet;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/FileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->jar:Ljava/io/File;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/FileSet;->setFile(Ljava/io/File;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->jar:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v1
.end method

.method protected declareSysProperty(Lorg/apache/tools/ant/taskdefs/ExecTask;Lorg/apache/tools/ant/types/Environment$Variable;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecTask;
    .param p2    # Lorg/apache/tools/ant/types/Environment$Variable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "-J-D"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/tools/ant/types/Environment$Variable;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    return-void
.end method

.method protected endExecution()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->redirector:Lorg/apache/tools/ant/types/RedirectorElement;

    return-void
.end method

.method public getRedirector()Lorg/apache/tools/ant/types/RedirectorElement;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->redirector:Lorg/apache/tools/ant/types/RedirectorElement;

    return-object v0
.end method

.method protected hasResources()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->path:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->filesets:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlias(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->alias:Ljava/lang/String;

    return-void
.end method

.method protected setCommonOptions(Lorg/apache/tools/ant/taskdefs/ExecTask;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecTask;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->maxMemory:Ljava/lang/String;

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "-J-Xmx"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->maxMemory:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_0
    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->verbose:Z

    if-eqz v3, :cond_1

    const-string v3, "-verbose"

    invoke-virtual {p0, p1, v3}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->sysProperties:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Environment;->getVariablesVector()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/Environment$Variable;

    invoke-virtual {p0, p1, v2}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->declareSysProperty(Lorg/apache/tools/ant/taskdefs/ExecTask;Lorg/apache/tools/ant/types/Environment$Variable;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setJar(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->jar:Ljava/io/File;

    return-void
.end method

.method public setKeypass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keypass:Ljava/lang/String;

    return-void
.end method

.method public setKeystore(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->keystore:Ljava/lang/String;

    return-void
.end method

.method public setMaxmemory(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->maxMemory:Ljava/lang/String;

    return-void
.end method

.method public setStorepass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storepass:Ljava/lang/String;

    return-void
.end method

.method public setStoretype(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->storetype:Ljava/lang/String;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;->verbose:Z

    return-void
.end method
