.class public Lorg/apache/tools/ant/taskdefs/Manifest;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Manifest$Section;,
        Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;
    }
.end annotation


# static fields
.field public static final ATTRIBUTE_CLASSPATH:Ljava/lang/String; = "Class-Path"

.field public static final ATTRIBUTE_FROM:Ljava/lang/String; = "From"

.field public static final ATTRIBUTE_MANIFEST_VERSION:Ljava/lang/String; = "Manifest-Version"

.field public static final ATTRIBUTE_NAME:Ljava/lang/String; = "Name"

.field public static final ATTRIBUTE_SIGNATURE_VERSION:Ljava/lang/String; = "Signature-Version"

.field public static final DEFAULT_MANIFEST_VERSION:Ljava/lang/String; = "1.0"

.field public static final EOL:Ljava/lang/String; = "\r\n"

.field public static final ERROR_FROM_FORBIDDEN:Ljava/lang/String; = "Manifest attributes should not start with \"From\" in \""

.field public static final JAR_ENCODING:Ljava/lang/String; = "UTF-8"

.field public static final MAX_LINE_LENGTH:I = 0x48

.field public static final MAX_SECTION_LENGTH:I = 0x46

.field static class$org$apache$tools$ant$taskdefs$Manifest:Ljava/lang/Class;


# instance fields
.field private mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

.field private manifestVersion:Ljava/lang/String;

.field private sectionIndex:Ljava/util/Vector;

.field private sections:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "1.0"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 10
    .param p1    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v7, "1.0"

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    new-instance v7, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-direct {v7}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;-><init>()V

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    new-instance v7, Ljava/util/Hashtable;

    invoke-direct {v7}, Ljava/util/Hashtable;-><init>()V

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v7, v4}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->read(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    const-string v8, "Manifest-Version"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    const-string v8, "Manifest-Version"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->removeAttribute(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-direct {v5}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;-><init>()V

    if-nez v2, :cond_3

    new-instance v6, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;

    invoke-direct {v6, v1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    new-instance v7, Lorg/apache/tools/ant/taskdefs/ManifestException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Manifest sections should start with a \"Name\" attribute and not \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/taskdefs/ManifestException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getValue()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v5, v2}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->setName(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->read(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/taskdefs/Manifest;->addConfiguredSection(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->addAttributeAndCheck(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)Ljava/lang/String;

    goto :goto_1

    :cond_4
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static getDefaultManifest()Lorg/apache/tools/ant/taskdefs/Manifest;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    const-string v1, "/org/apache/tools/ant/defaultManifest.mf"

    sget-object v7, Lorg/apache/tools/ant/taskdefs/Manifest;->class$org$apache$tools$ant$taskdefs$Manifest:Ljava/lang/Class;

    if-nez v7, :cond_0

    const-string v7, "org.apache.tools.ant.taskdefs.Manifest"

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/Manifest;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    sput-object v7, Lorg/apache/tools/ant/taskdefs/Manifest;->class$org$apache$tools$ant$taskdefs$Manifest:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v7, v1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Could not find default manifest: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v3

    :goto_1
    :try_start_1
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Default manifest is invalid !!"

    invoke-direct {v7, v8, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v7

    :goto_2
    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v7

    :cond_0
    :try_start_2
    sget-object v7, Lorg/apache/tools/ant/taskdefs/Manifest;->class$org$apache$tools$ant$taskdefs$Manifest:Ljava/lang/Class;
    :try_end_2
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_1
    :try_start_3
    new-instance v6, Ljava/io/InputStreamReader;

    const-string v7, "UTF-8"

    invoke-direct {v6, v4, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    new-instance v2, Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-direct {v2, v6}, Lorg/apache/tools/ant/taskdefs/Manifest;-><init>(Ljava/io/Reader;)V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;

    const-string v7, "Created-By"

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "java.vm.version"

    invoke-static {v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "java.vm.vendor"

    invoke-static {v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->getMainSection()Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    move-result-object v7

    invoke-static {v7, v0}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->access$000(Lorg/apache/tools/ant/taskdefs/Manifest$Section;Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v6}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    move-object v5, v6

    :goto_3
    return-object v2

    :catch_1
    move-exception v3

    move-object v6, v5

    :goto_4
    :try_start_5
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    new-instance v2, Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-direct {v2, v5}, Lorg/apache/tools/ant/taskdefs/Manifest;-><init>(Ljava/io/Reader;)V
    :try_end_6
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto :goto_3

    :catch_2
    move-exception v3

    :goto_5
    :try_start_7
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Unable to read default manifest"

    invoke-direct {v7, v8, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_1
    move-exception v7

    move-object v5, v6

    goto :goto_2

    :catch_3
    move-exception v3

    move-object v5, v6

    goto :goto_5

    :catch_4
    move-exception v3

    move-object v5, v6

    goto/16 :goto_1

    :catch_5
    move-exception v3

    goto :goto_4
.end method


# virtual methods
.method public addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Attributes must have name and value"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Manifest-Version"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V

    goto :goto_0
.end method

.method public addConfiguredSection(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest$Section;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "Sections must have a name"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p1, p0, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Manifest;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    if-nez v2, :cond_0

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    iget-object v3, v0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0
.end method

.method public getMainSection()Lorg/apache/tools/ant/taskdefs/Manifest$Section;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    return-object v0
.end method

.method public getManifestVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSection(Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/Manifest$Section;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    return-object v0
.end method

.method public getSectionNames()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getWarnings()Ljava/util/Enumeration;
    .locals 6

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getWarnings()Ljava/util/Enumeration;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getWarnings()Ljava/util/Enumeration;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v5

    return-object v5
.end method

.method public hashCode()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;Z)V

    return-void
.end method

.method public merge(Lorg/apache/tools/ant/taskdefs/Manifest;Z)V
    .locals 6
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    if-eqz p1, :cond_4

    if-eqz p2, :cond_2

    iget-object v4, p1, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    :goto_0
    iget-object v4, p1, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Manifest;->getSectionNames()Ljava/util/Enumeration;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v4, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    iget-object v4, p1, Lorg/apache/tools/ant/taskdefs/Manifest;->sections:Ljava/util/Hashtable;

    invoke-virtual {v4, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    if-nez v2, :cond_3

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/Manifest;->addConfiguredSection(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    iget-object v5, p1, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->merge(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->merge(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->write(Ljava/io/PrintWriter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public write(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Manifest-Version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->manifestVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    const-string v6, "Signature-Version"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Signature-Version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    const-string v6, "Signature-Version"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->removeAttribute(Ljava/lang/String;)V

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v5, p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->write(Ljava/io/PrintWriter;)V

    if-eqz v3, :cond_1

    :try_start_0
    new-instance v4, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;

    const-string v5, "Signature-Version"

    invoke-direct {v4, v5, v3}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->mainSection:Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    invoke-virtual {v5, v4}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Manifest;->sectionIndex:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->getSection(Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->write(Ljava/io/PrintWriter;)V

    goto :goto_1

    :cond_2
    return-void

    :catch_0
    move-exception v5

    goto :goto_0
.end method
