.class public Lorg/apache/tools/ant/taskdefs/Delete;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Delete.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;
    }
.end annotation


# static fields
.field private static final DELETE_RETRY_SLEEP_MILLIS:I = 0xa

.field private static final EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

.field private static final REVERSE_FILESYSTEM:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;


# instance fields
.field private deleteOnExit:Z

.field protected dir:Ljava/io/File;

.field private failonerror:Z

.field protected file:Ljava/io/File;

.field protected filesets:Ljava/util/Vector;

.field protected includeEmpty:Z

.field private quiet:Z

.field private rcs:Lorg/apache/tools/ant/types/resources/Resources;

.field protected usedMatchingTask:Z

.field private verbosity:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/resources/comparators/Reverse;

    new-instance v1, Lorg/apache/tools/ant/types/resources/comparators/FileSystem;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/comparators/FileSystem;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/comparators/Reverse;-><init>(Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Delete;->REVERSE_FILESYSTEM:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Exists;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/selectors/Exists;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Delete;->EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->includeEmpty:Z

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->deleteOnExit:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    return-void
.end method

.method private delete(Ljava/io/File;)Z
    .locals 4
    .param p1    # Ljava/io/File;

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "windows"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->gc()V

    :cond_0
    const-wide/16 v2, 0xa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Delete;->deleteOnExit:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v2, :cond_2

    const/4 v0, 0x3

    :goto_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Failed to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ", calling deleteOnExit."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " This attempts to delete the file when the Ant jvm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " has exited and might not succeed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-virtual {p1}, Ljava/io/File;->deleteOnExit()V

    :cond_1
    :goto_2
    return v1

    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private handle(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    if-eqz v0, :cond_1

    instance-of v0, p1, Lorg/apache/tools/ant/BuildException;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/tools/ant/BuildException;

    :goto_0
    throw p1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    :goto_1
    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/Throwable;I)V

    return-void

    :cond_2
    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_1
.end method

.method private handle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/Exception;)V

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/resources/Resources;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Resources;-><init>()V

    :goto_1
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    goto :goto_1
.end method

.method public add(Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FileSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->add(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    return-void
.end method

.method public addAnd(Lorg/apache/tools/ant/types/selectors/AndSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/AndSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addAnd(Lorg/apache/tools/ant/types/selectors/AndSelector;)V

    return-void
.end method

.method public addContains(Lorg/apache/tools/ant/types/selectors/ContainsSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ContainsSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addContains(Lorg/apache/tools/ant/types/selectors/ContainsSelector;)V

    return-void
.end method

.method public addContainsRegexp(Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addContainsRegexp(Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;)V

    return-void
.end method

.method public addCustom(Lorg/apache/tools/ant/types/selectors/ExtendSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ExtendSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addCustom(Lorg/apache/tools/ant/types/selectors/ExtendSelector;)V

    return-void
.end method

.method public addDate(Lorg/apache/tools/ant/types/selectors/DateSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DateSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addDate(Lorg/apache/tools/ant/types/selectors/DateSelector;)V

    return-void
.end method

.method public addDepend(Lorg/apache/tools/ant/types/selectors/DependSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DependSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addDepend(Lorg/apache/tools/ant/types/selectors/DependSelector;)V

    return-void
.end method

.method public addDepth(Lorg/apache/tools/ant/types/selectors/DepthSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DepthSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addDepth(Lorg/apache/tools/ant/types/selectors/DepthSelector;)V

    return-void
.end method

.method public addFilename(Lorg/apache/tools/ant/types/selectors/FilenameSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FilenameSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addFilename(Lorg/apache/tools/ant/types/selectors/FilenameSelector;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addMajority(Lorg/apache/tools/ant/types/selectors/MajoritySelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/MajoritySelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addMajority(Lorg/apache/tools/ant/types/selectors/MajoritySelector;)V

    return-void
.end method

.method public addModified(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addModified(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;)V

    return-void
.end method

.method public addNone(Lorg/apache/tools/ant/types/selectors/NoneSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/NoneSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addNone(Lorg/apache/tools/ant/types/selectors/NoneSelector;)V

    return-void
.end method

.method public addNot(Lorg/apache/tools/ant/types/selectors/NotSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/NotSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addNot(Lorg/apache/tools/ant/types/selectors/NotSelector;)V

    return-void
.end method

.method public addOr(Lorg/apache/tools/ant/types/selectors/OrSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/OrSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addOr(Lorg/apache/tools/ant/types/selectors/OrSelector;)V

    return-void
.end method

.method public addPresent(Lorg/apache/tools/ant/types/selectors/PresentSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/PresentSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addPresent(Lorg/apache/tools/ant/types/selectors/PresentSelector;)V

    return-void
.end method

.method public addSelector(Lorg/apache/tools/ant/types/selectors/SelectSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/SelectSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addSelector(Lorg/apache/tools/ant/types/selectors/SelectSelector;)V

    return-void
.end method

.method public addSize(Lorg/apache/tools/ant/types/selectors/SizeSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/SizeSelector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->addSize(Lorg/apache/tools/ant/types/selectors/SizeSelector;)V

    return-void
.end method

.method public createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createPatternSet()Lorg/apache/tools/ant/types/PatternSet;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createPatternSet()Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v12, 0x3

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    if-eqz v11, :cond_0

    const-string v13, "DEPRECATED - Use of the implicit FileSet is deprecated.  Use a nested fileset element instead."

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v11, :cond_1

    move v11, v12

    :goto_0
    invoke-virtual {p0, v13, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    :cond_0
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    if-nez v11, :cond_2

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    if-nez v11, :cond_2

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v11

    if-nez v11, :cond_2

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    if-nez v11, :cond_2

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "At least one of the file or dir attributes, or a nested resource collection, must be set."

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_1
    iget v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_0

    :cond_2
    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v11, :cond_3

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    if-eqz v11, :cond_3

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "quiet and failonerror cannot both be set to true"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v11

    :cond_3
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_c

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_b

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Directory "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, " cannot be removed using the file attribute.  "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, "Use dir instead."

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v11, :cond_a

    move v11, v12

    :goto_1
    invoke-virtual {p0, v13, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    :cond_4
    :goto_2
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_6

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    if-nez v11, :cond_6

    iget v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    if-ne v11, v12, :cond_5

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Deleting directory "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;)V

    :cond_5
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->removeDir(Ljava/io/File;)V

    :cond_6
    new-instance v8, Lorg/apache/tools/ant/types/resources/Resources;

    invoke-direct {v8}, Lorg/apache/tools/ant/types/resources/Resources;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v8, v11}, Lorg/apache/tools/ant/types/resources/Resources;->setProject(Lorg/apache/tools/ant/Project;)V

    new-instance v2, Lorg/apache/tools/ant/types/resources/Resources;

    invoke-direct {v2}, Lorg/apache/tools/ant/types/resources/Resources;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v2, v11}, Lorg/apache/tools/ant/types/resources/Resources;->setProject(Lorg/apache/tools/ant/Project;)V

    const/4 v5, 0x0

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    if-eqz v11, :cond_7

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    if-eqz v11, :cond_7

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getImplicitFileSet()Lorg/apache/tools/ant/types/FileSet;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_7
    const/4 v4, 0x0

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v10

    :goto_3
    if-ge v4, v10, :cond_f

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    if-nez v11, :cond_8

    const-string v11, "Deleting fileset with no project specified; assuming executing project"

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v3, v11}, Lorg/apache/tools/ant/types/FileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_8
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->getDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-nez v11, :cond_e

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Directory does not exist:"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->getDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    :cond_9
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_a
    iget v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto/16 :goto_1

    :cond_b
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Deleting: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v11

    if-nez v11, :cond_4

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Unable to delete file "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Could not find file "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, " to delete."

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v11, :cond_d

    move v11, v12

    :goto_5
    invoke-virtual {p0, v13, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    goto/16 :goto_2

    :cond_d
    iget v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_5

    :cond_e
    invoke-virtual {v8, v3}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->includeEmpty:Z

    if-eqz v11, :cond_9

    new-instance v11, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->getDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner()Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v14

    invoke-virtual {v14}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v14

    invoke-direct {v11, v13, v14}, Lorg/apache/tools/ant/taskdefs/Delete$ReverseDirs;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto/16 :goto_4

    :cond_f
    invoke-virtual {v8, v2}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    if-eqz v11, :cond_10

    new-instance v1, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    sget-object v11, Lorg/apache/tools/ant/taskdefs/Delete;->EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-virtual {v1, v11}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->rcs:Lorg/apache/tools/ant/types/resources/Resources;

    invoke-virtual {v1, v11}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    new-instance v9, Lorg/apache/tools/ant/types/resources/Sort;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/resources/Sort;-><init>()V

    sget-object v11, Lorg/apache/tools/ant/taskdefs/Delete;->REVERSE_FILESYSTEM:Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;

    invoke-virtual {v9, v11}, Lorg/apache/tools/ant/types/resources/Sort;->add(Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;)V

    invoke-virtual {v9, v1}, Lorg/apache/tools/ant/types/resources/Sort;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/types/resources/Resources;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    :cond_10
    :try_start_0
    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Resources;->isFilesystemOnly()Z

    move-result v11

    if-eqz v11, :cond_15

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/resources/Resources;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_11
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_16

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->isExists()Z

    move-result v11

    if-eqz v11, :cond_11

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    if-nez v11, :cond_11

    :cond_12
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Deleting "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    iget v12, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v11

    if-nez v11, :cond_11

    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    if-eqz v11, :cond_11

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Unable to delete "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/FileResource;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_14

    const-string v11, "directory "

    :goto_7
    invoke-virtual {v12, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_6

    :catch_0
    move-exception v0

    :try_start_1
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_13

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11, v5}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_13
    :goto_8
    return-void

    :cond_14
    :try_start_2
    const-string v11, "file "

    goto :goto_7

    :cond_15
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getTaskName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " handles only filesystem resources"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_16
    if-eqz v5, :cond_13

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v11, v5}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    goto :goto_8

    :catchall_0
    move-exception v11

    if-eqz v5, :cond_17

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Delete;->filesets:Ljava/util/Vector;

    invoke-virtual {v12, v5}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_17
    throw v11
.end method

.method protected removeDir(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v4, 0x0

    new-array v2, v4, [Ljava/lang/String;

    :cond_0
    const/4 v1, 0x0

    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_4

    aget-object v3, v2, v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->removeDir(Ljava/io/File;)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Deleting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    :goto_2
    invoke-virtual {p0, v5, v4}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to delete file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget v4, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_2

    :cond_4
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Deleting directory "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to delete directory "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method protected removeFiles(Ljava/io/File;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v6, 0x3

    array-length v5, p2

    if-lez v5, :cond_3

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Deleting "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    array-length v7, p2

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " files from "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v5, :cond_1

    move v5, v6

    :goto_0
    invoke-virtual {p0, v7, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    const/4 v4, 0x0

    :goto_1
    array-length v5, p2

    if-ge v4, v5, :cond_3

    new-instance v3, Ljava/io/File;

    aget-object v5, p2, v4

    invoke-direct {v3, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Deleting "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v5, :cond_2

    move v5, v6

    :goto_2
    invoke-virtual {p0, v7, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-direct {p0, v3}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Unable to delete file "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_0

    :cond_2
    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_2

    :cond_3
    array-length v5, p3

    if-lez v5, :cond_9

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->includeEmpty:Z

    if-eqz v5, :cond_9

    const/4 v1, 0x0

    array-length v5, p3

    add-int/lit8 v4, v5, -0x1

    :goto_3
    if-ltz v4, :cond_8

    new-instance v0, Ljava/io/File;

    aget-object v5, p3, v4

    invoke-direct {v0, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    array-length v5, v2

    if-nez v5, :cond_5

    :cond_4
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Deleting "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v5, :cond_6

    move v5, v6

    :goto_4
    invoke-virtual {p0, v7, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Delete;->delete(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Unable to delete directory "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/tools/ant/taskdefs/Delete;->handle(Ljava/lang/String;)V

    :cond_5
    :goto_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    :cond_6
    iget v5, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_4

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_8
    if-lez v1, :cond_9

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Deleted "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " director"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/4 v5, 0x1

    if-ne v1, v5, :cond_a

    const-string v5, "y"

    :goto_6
    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " form "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz v7, :cond_b

    :goto_7
    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Delete;->log(Ljava/lang/String;I)V

    :cond_9
    return-void

    :cond_a
    const-string v5, "ies"

    goto :goto_6

    :cond_b
    iget v6, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_7
.end method

.method public setCaseSensitive(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setCaseSensitive(Z)V

    return-void
.end method

.method public setDefaultexcludes(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setDefaultexcludes(Z)V

    return-void
.end method

.method public setDeleteOnExit(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->deleteOnExit:Z

    return-void
.end method

.method public setDir(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->dir:Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Delete;->getImplicitFileSet()Lorg/apache/tools/ant/types/FileSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    return-void
.end method

.method public setExcludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setExcludes(Ljava/lang/String;)V

    return-void
.end method

.method public setExcludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setExcludesfile(Ljava/io/File;)V

    return-void
.end method

.method public setFailOnError(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->file:Ljava/io/File;

    return-void
.end method

.method public setFollowSymlinks(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setFollowSymlinks(Z)V

    return-void
.end method

.method public setIncludeEmptyDirs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->includeEmpty:Z

    return-void
.end method

.method public setIncludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setIncludes(Ljava/lang/String;)V

    return-void
.end method

.method public setIncludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->usedMatchingTask:Z

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->setIncludesfile(Ljava/io/File;)V

    return-void
.end method

.method public setQuiet(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Delete;->quiet:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->failonerror:Z

    :cond_0
    return-void
.end method

.method public setVerbose(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Delete;->verbosity:I

    goto :goto_0
.end method
