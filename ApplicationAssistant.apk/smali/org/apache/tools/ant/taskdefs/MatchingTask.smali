.class public abstract Lorg/apache/tools/ant/taskdefs/MatchingTask;
.super Lorg/apache/tools/ant/Task;
.source "MatchingTask.java"

# interfaces
.implements Lorg/apache/tools/ant/types/selectors/SelectorContainer;


# instance fields
.field protected fileset:Lorg/apache/tools/ant/types/FileSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FileSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    return-void
.end method


# virtual methods
.method public XsetIgnore(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "The ignore attribute is deprecated.Please use the excludes attribute."

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->log(Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ", "

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "**/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "/**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public XsetItems(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v2, "The items attribute is deprecated. Please use the includes attribute."

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->log(Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    const-string v2, "*"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v2

    const-string v3, "**"

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ", "

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "/**"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public add(Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FileSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->add(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    return-void
.end method

.method public addAnd(Lorg/apache/tools/ant/types/selectors/AndSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/AndSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addAnd(Lorg/apache/tools/ant/types/selectors/AndSelector;)V

    return-void
.end method

.method public addContains(Lorg/apache/tools/ant/types/selectors/ContainsSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ContainsSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addContains(Lorg/apache/tools/ant/types/selectors/ContainsSelector;)V

    return-void
.end method

.method public addContainsRegexp(Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addContainsRegexp(Lorg/apache/tools/ant/types/selectors/ContainsRegexpSelector;)V

    return-void
.end method

.method public addCustom(Lorg/apache/tools/ant/types/selectors/ExtendSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/ExtendSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addCustom(Lorg/apache/tools/ant/types/selectors/ExtendSelector;)V

    return-void
.end method

.method public addDate(Lorg/apache/tools/ant/types/selectors/DateSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DateSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addDate(Lorg/apache/tools/ant/types/selectors/DateSelector;)V

    return-void
.end method

.method public addDepend(Lorg/apache/tools/ant/types/selectors/DependSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DependSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addDepend(Lorg/apache/tools/ant/types/selectors/DependSelector;)V

    return-void
.end method

.method public addDepth(Lorg/apache/tools/ant/types/selectors/DepthSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DepthSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addDepth(Lorg/apache/tools/ant/types/selectors/DepthSelector;)V

    return-void
.end method

.method public addDifferent(Lorg/apache/tools/ant/types/selectors/DifferentSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/DifferentSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addDifferent(Lorg/apache/tools/ant/types/selectors/DifferentSelector;)V

    return-void
.end method

.method public addFilename(Lorg/apache/tools/ant/types/selectors/FilenameSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FilenameSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addFilename(Lorg/apache/tools/ant/types/selectors/FilenameSelector;)V

    return-void
.end method

.method public addMajority(Lorg/apache/tools/ant/types/selectors/MajoritySelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/MajoritySelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addMajority(Lorg/apache/tools/ant/types/selectors/MajoritySelector;)V

    return-void
.end method

.method public addModified(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addModified(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;)V

    return-void
.end method

.method public addNone(Lorg/apache/tools/ant/types/selectors/NoneSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/NoneSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addNone(Lorg/apache/tools/ant/types/selectors/NoneSelector;)V

    return-void
.end method

.method public addNot(Lorg/apache/tools/ant/types/selectors/NotSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/NotSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addNot(Lorg/apache/tools/ant/types/selectors/NotSelector;)V

    return-void
.end method

.method public addOr(Lorg/apache/tools/ant/types/selectors/OrSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/OrSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addOr(Lorg/apache/tools/ant/types/selectors/OrSelector;)V

    return-void
.end method

.method public addPresent(Lorg/apache/tools/ant/types/selectors/PresentSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/PresentSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addPresent(Lorg/apache/tools/ant/types/selectors/PresentSelector;)V

    return-void
.end method

.method public addSelector(Lorg/apache/tools/ant/types/selectors/SelectSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/SelectSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addSelector(Lorg/apache/tools/ant/types/selectors/SelectSelector;)V

    return-void
.end method

.method public addSize(Lorg/apache/tools/ant/types/selectors/SizeSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/SizeSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addSize(Lorg/apache/tools/ant/types/selectors/SizeSelector;)V

    return-void
.end method

.method public addType(Lorg/apache/tools/ant/types/selectors/TypeSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/TypeSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->addType(Lorg/apache/tools/ant/types/selectors/TypeSelector;)V

    return-void
.end method

.method public appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FileSelector;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    return-void
.end method

.method public createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v0

    return-object v0
.end method

.method public createPatternSet()Lorg/apache/tools/ant/types/PatternSet;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->createPatternSet()Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v0

    return-object v0
.end method

.method protected getDirectoryScanner(Ljava/io/File;)Lorg/apache/tools/ant/DirectoryScanner;
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    return-object v0
.end method

.method protected final getImplicitFileSet()Lorg/apache/tools/ant/types/FileSet;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    return-object v0
.end method

.method public getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;

    move-result-object v0

    return-object v0
.end method

.method public hasSelectors()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->hasSelectors()Z

    move-result v0

    return v0
.end method

.method public selectorCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->selectorCount()I

    move-result v0

    return v0
.end method

.method public selectorElements()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileSet;->selectorElements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public setCaseSensitive(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setCaseSensitive(Z)V

    return-void
.end method

.method public setDefaultexcludes(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setDefaultexcludes(Z)V

    return-void
.end method

.method public setExcludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setExcludes(Ljava/lang/String;)V

    return-void
.end method

.method public setExcludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setExcludesfile(Ljava/io/File;)V

    return-void
.end method

.method public setFollowSymlinks(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setFollowSymlinks(Z)V

    return-void
.end method

.method public setIncludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setIncludes(Ljava/lang/String;)V

    return-void
.end method

.method public setIncludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setIncludesfile(Ljava/io/File;)V

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MatchingTask;->fileset:Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileSet;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method
