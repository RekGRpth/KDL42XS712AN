.class public Lorg/apache/tools/ant/taskdefs/CallTarget;
.super Lorg/apache/tools/ant/Task;
.source "CallTarget.java"


# instance fields
.field private callee:Lorg/apache/tools/ant/taskdefs/Ant;

.field private inheritAll:Z

.field private inheritRefs:Z

.field private targetSet:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritAll:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritRefs:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->targetSet:Z

    return-void
.end method


# virtual methods
.method public addConfiguredTarget(Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->addConfiguredTarget(Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->targetSet:Z

    return-void
.end method

.method public addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V

    return-void
.end method

.method public addReference(Lorg/apache/tools/ant/taskdefs/Ant$Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->addReference(Lorg/apache/tools/ant/taskdefs/Ant$Reference;)V

    return-void
.end method

.method public createParam()Lorg/apache/tools/ant/taskdefs/Property;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Ant;->createProperty()Lorg/apache/tools/ant/taskdefs/Property;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->targetSet:Z

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Attribute target or at least one nested target is required."

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    const-string v2, "ant.file"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Ant;->setAntfile(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritAll:Z

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Ant;->setInheritAll(Z)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritRefs:Z

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Ant;->setInheritRefs(Z)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Ant;->execute()V

    return-void
.end method

.method public handleErrorFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleErrorFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleErrorOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleErrorOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Ant;->handleInput([BII)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tools/ant/Task;->handleInput([BII)I

    move-result v0

    goto :goto_0
.end method

.method public handleOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public init()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Ant;-><init>(Lorg/apache/tools/ant/Task;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Ant;->init()V

    return-void
.end method

.method public setInheritAll(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritAll:Z

    return-void
.end method

.method public setInheritRefs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->inheritRefs:Z

    return-void
.end method

.method public setTarget(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/CallTarget;->init()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->callee:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->setTarget(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/CallTarget;->targetSet:Z

    return-void
.end method
