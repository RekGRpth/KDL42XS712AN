.class public abstract Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;
.super Ljava/lang/Object;
.source "DefaultRmicAdapter.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter$RmicFileNameMapper;
    }
.end annotation


# static fields
.field private static final RAND:Ljava/util/Random;

.field public static final RMI_SKEL_SUFFIX:Ljava/lang/String; = "_Skel"

.field public static final RMI_STUB_SUFFIX:Ljava/lang/String; = "_Stub"

.field public static final RMI_TIE_SUFFIX:Ljava/lang/String; = "_Tie"

.field public static final STUB_1_1:Ljava/lang/String; = "-v1.1"

.field public static final STUB_1_2:Ljava/lang/String; = "-v1.2"

.field public static final STUB_COMPAT:Ljava/lang/String; = "-vcompat"


# instance fields
.field private attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

.field private mapper:Lorg/apache/tools/ant/util/FileNameMapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->RAND:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;)Lorg/apache/tools/ant/taskdefs/Rmic;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    return-object v0
.end method

.method static access$100()Ljava/util/Random;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->RAND:Ljava/util/Random;

    return-object v0
.end method


# virtual methods
.method public getClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method protected getCompileClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getBase()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->setLocation(Ljava/io/File;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/types/Path;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIncludeantruntime()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "last"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->addExisting(Lorg/apache/tools/ant/types/Path;)V

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIncludejavaruntime()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->addJavaRuntime()V

    :cond_1
    return-object v0

    :cond_2
    const-string v2, "ignore"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Path;->concatSystemClasspath(Ljava/lang/String;)Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/Path;->addExisting(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public getMapper()Lorg/apache/tools/ant/util/FileNameMapper;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    return-object v0
.end method

.method public getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    return-object v0
.end method

.method protected getSkelClassSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, "_Skel"

    return-object v0
.end method

.method protected getStubClassSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, "_Stub"

    return-object v0
.end method

.method protected getTieClassSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, "_Tie"

    return-object v0
.end method

.method protected logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 9
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v8, 0x3

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getCompileList()Ljava/util/Vector;

    move-result-object v2

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Compilation "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->describeArguments()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "File"

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v5, 0x1

    if-eq v1, v5, :cond_0

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v5, " to be compiled:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    const-string v5, "    "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public setRmic(Lorg/apache/tools/ant/taskdefs/Rmic;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Rmic;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter$RmicFileNameMapper;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter$RmicFileNameMapper;-><init>(Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    return-void
.end method

.method protected setupRmicCommand()Lorg/apache/tools/ant/types/Commandline;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->setupRmicCommand([Ljava/lang/String;)Lorg/apache/tools/ant/types/Commandline;

    move-result-object v0

    return-object v0
.end method

.method protected setupRmicCommand([Ljava/lang/String;)Lorg/apache/tools/ant/types/Commandline;
    .locals 9
    .param p1    # [Ljava/lang/String;

    const/4 v8, 0x2

    new-instance v1, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_0

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    aget-object v6, p1, v2

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-d"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getBase()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getExtdirs()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-extdirs"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getExtdirs()Lorg/apache/tools/ant/types/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_1
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-classpath"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getStubVersion()Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    if-eqz v4, :cond_2

    const-string v5, "1.1"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const-string v3, "-v1.1"

    :cond_2
    :goto_1
    if-nez v3, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIiop()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIdl()Z

    move-result v5

    if-nez v5, :cond_3

    const-string v3, "-vcompat"

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    invoke-virtual {v5, v3}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_4
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getSourceBase()Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-keepgenerated"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_5
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIiop()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    const-string v6, "IIOP has been turned on."

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-iiop"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIiopopts()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "IIOP Options: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIiopopts()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIiopopts()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_6
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIdl()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-idl"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    const-string v6, "IDL has been turned on."

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIdlopts()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIdlopts()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "IDL Options: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->getIdlopts()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    :cond_7
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getDebug()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v5

    const-string v6, "-g"

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_8
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getCurrentCompilerArgs()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/tools/ant/types/Commandline;->addArguments([Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    return-object v1

    :cond_9
    const-string v5, "1.2"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v3, "-v1.2"

    goto/16 :goto_1

    :cond_a
    const-string v5, "compat"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    const-string v3, "-vcompat"

    goto/16 :goto_1

    :cond_b
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Rmic;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Unknown stub option "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
