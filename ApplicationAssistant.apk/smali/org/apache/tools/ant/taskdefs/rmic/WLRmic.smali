.class public Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;
.super Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;
.source "WLRmic.java"


# static fields
.field public static final COMPILER_NAME:Ljava/lang/String; = "weblogic"

.field public static final ERROR_NO_WLRMIC_ON_CLASSPATH:Ljava/lang/String; = "Cannot use WebLogic rmic, as it is not available.  A common solution is to set the environment variable CLASSPATH."

.field public static final ERROR_WLRMIC_FAILED:Ljava/lang/String; = "Error starting WebLogic rmic: "

.field public static final WLRMIC_CLASSNAME:Ljava/lang/String; = "weblogic.rmic"

.field public static final WL_RMI_SKEL_SUFFIX:Ljava/lang/String; = "_WLSkel"

.field public static final WL_RMI_STUB_SUFFIX:Ljava/lang/String; = "_WLStub"

.field static array$Ljava$lang$String:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public execute()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v5

    const-string v6, "Using WebLogic rmic"

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    new-array v5, v9, [Ljava/lang/String;

    const-string v6, "-noexit"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->setupRmicCommand([Ljava/lang/String;)Lorg/apache/tools/ant/types/Commandline;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v5, "weblogic.rmic"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    :goto_0
    const-string v6, "main"

    const/4 v5, 0x1

    new-array v7, v5, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v5, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->array$Ljava$lang$String:Ljava/lang/Class;

    if-nez v5, :cond_2

    const-string v5, "[Ljava.lang.String;"

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->array$Ljava$lang$String:Ljava/lang/Class;

    :goto_1
    aput-object v5, v7, v8

    invoke-virtual {v0, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->getArguments()[Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lorg/apache/tools/ant/AntClassLoader;->cleanup()V

    :cond_0
    return v9

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v4

    const-string v5, "weblogic.rmic"

    const/4 v6, 0x1

    invoke-static {v5, v6, v4}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v5, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->array$Ljava$lang$String:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_2
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "Cannot use WebLogic rmic, as it is not available.  A common solution is to set the environment variable CLASSPATH."

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v5

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lorg/apache/tools/ant/AntClassLoader;->cleanup()V

    :cond_3
    throw v5

    :catch_1
    move-exception v3

    :try_start_3
    instance-of v5, v3, Lorg/apache/tools/ant/BuildException;

    if-eqz v5, :cond_4

    check-cast v3, Lorg/apache/tools/ant/BuildException;

    throw v3

    :cond_4
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "Error starting WebLogic rmic: "

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/WLRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-direct {v5, v6, v3, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public getSkelClassSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, "_WLSkel"

    return-object v0
.end method

.method public getStubClassSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, "_WLStub"

    return-object v0
.end method
