.class public Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;
.super Ljava/lang/Object;
.source "Tstamp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Tstamp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomFormat"
.end annotation


# instance fields
.field private country:Ljava/lang/String;

.field private field:I

.field private language:Ljava/lang/String;

.field private offset:I

.field private pattern:Ljava/lang/String;

.field private propertyName:Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

.field private timeZone:Ljava/util/TimeZone;

.field private variant:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/taskdefs/Tstamp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->offset:I

    const/4 v0, 0x5

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->field:I

    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/tools/ant/Project;Ljava/util/Date;Lorg/apache/tools/ant/Location;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/util/Date;
    .param p3    # Lorg/apache/tools/ant/Location;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->propertyName:Ljava/lang/String;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "property attribute must be provided"

    invoke-direct {v2, v3, p3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->pattern:Ljava/lang/String;

    if-nez v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "pattern attribute must be provided"

    invoke-direct {v2, v3, p3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->language:Ljava/lang/String;

    if-nez v2, :cond_4

    new-instance v1, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->pattern:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :goto_0
    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->offset:I

    if-eqz v2, :cond_2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->field:I

    iget v3, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->offset:I

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->timeZone:Ljava/util/TimeZone;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->propertyName:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lorg/apache/tools/ant/taskdefs/Tstamp;->access$000(Lorg/apache/tools/ant/taskdefs/Tstamp;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->variant:Ljava/lang/String;

    if-nez v2, :cond_5

    new-instance v1, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->pattern:Ljava/lang/String;

    new-instance v3, Ljava/util/Locale;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->language:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->country:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->pattern:Ljava/lang/String;

    new-instance v3, Ljava/util/Locale;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->language:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->country:Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->variant:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, " \t\n\r\u000c,"

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->language:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->country:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->variant:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "bad locale format"

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Tstamp;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "bad locale format"

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Tstamp;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_0
    :try_start_1
    const-string v2, ""

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->country:Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->offset:I

    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->pattern:Ljava/lang/String;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->propertyName:Ljava/lang/String;

    return-void
.end method

.method public setTimezone(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method public setUnit(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->this$0:Lorg/apache/tools/ant/taskdefs/Tstamp;

    const-string v2, "DEPRECATED - The setUnit(String) method has been deprecated. Use setUnit(Tstamp.Unit) instead."

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Tstamp;->log(Ljava/lang/String;)V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;->setValue(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;->getCalendarField()I

    move-result v1

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->field:I

    return-void
.end method

.method public setUnit(Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Tstamp$Unit;->getCalendarField()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Tstamp$CustomFormat;->field:I

    return-void
.end method
