.class public Lorg/apache/tools/ant/taskdefs/Classloader;
.super Lorg/apache/tools/ant/Task;
.source "Classloader.java"


# static fields
.field public static final SYSTEM_LOADER_REF:Ljava/lang/String; = "ant.coreLoader"


# instance fields
.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private name:Ljava/lang/String;

.field private parentFirst:Z

.field private parentName:Ljava/lang/String;

.field private reset:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->reset:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentFirst:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 13

    :try_start_0
    const-string v9, "only"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    const-string v11, "build.sysclasspath"

    invoke-virtual {v10, v11}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    if-eqz v9, :cond_0

    const-string v9, "ant.coreLoader"

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_0
    const-string v9, "Changing the system loader is disabled by build.sysclasspath=only"

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Classloader;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    if-nez v9, :cond_4

    const-string v6, "ant.coreLoader"

    :goto_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v9, v6}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->reset:Z

    if-eqz v9, :cond_3

    const/4 v7, 0x0

    :cond_3
    if-eqz v7, :cond_5

    instance-of v9, v7, Lorg/apache/tools/ant/AntClassLoader;

    if-nez v9, :cond_5

    const-string v9, "Referenced object is not an AntClassLoader"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Classloader;->log(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_4
    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    goto :goto_1

    :cond_5
    move-object v0, v7

    check-cast v0, Lorg/apache/tools/ant/AntClassLoader;

    move-object v1, v0

    if-nez v1, :cond_8

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentName:Ljava/lang/String;

    if-eqz v9, :cond_6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    instance-of v9, v8, Ljava/lang/ClassLoader;

    if-nez v9, :cond_6

    const/4 v8, 0x0

    :cond_6
    if-nez v8, :cond_a

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    move-object v9, v8

    :goto_2
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    if-nez v10, :cond_7

    :cond_7
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Setting parent loader "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-boolean v12, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentFirst:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v10, v11, v12}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v1, Lorg/apache/tools/ant/AntClassLoader;

    check-cast v9, Ljava/lang/ClassLoader;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    iget-boolean v12, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentFirst:Z

    invoke-direct {v1, v9, v10, v11, v12}, Lorg/apache/tools/ant/AntClassLoader;-><init>(Ljava/lang/ClassLoader;Lorg/apache/tools/ant/Project;Lorg/apache/tools/ant/types/Path;Z)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v9, v6, v1}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    if-nez v9, :cond_8

    const-string v9, "org.apache.tools.ant.taskdefs.optional"

    invoke-virtual {v1, v9}, Lorg/apache/tools/ant/AntClassLoader;->addLoaderPackageRoot(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v9, v1}, Lorg/apache/tools/ant/Project;->setCoreLoader(Ljava/lang/ClassLoader;)V

    :cond_8
    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    :goto_3
    array-length v9, v5

    if-ge v4, v9, :cond_1

    new-instance v3, Ljava/io/File;

    aget-object v9, v5, v4

    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lorg/apache/tools/ant/AntClassLoader;->addPathElement(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Adding to class loader "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/Classloader;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_a
    move-object v9, v8

    goto/16 :goto_2
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Classloader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Path;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->classpath:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->name:Ljava/lang/String;

    return-void
.end method

.method public setParentFirst(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentFirst:Z

    return-void
.end method

.method public setParentName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentName:Ljava/lang/String;

    return-void
.end method

.method public setReset(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->reset:Z

    return-void
.end method

.method public setReverse(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Classloader;->parentFirst:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
