.class public Lorg/apache/tools/ant/taskdefs/FixCRLF;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "FixCRLF.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;,
        Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;,
        Lorg/apache/tools/ant/taskdefs/FixCRLF$OneLiner;
    }
.end annotation


# static fields
.field public static final ERROR_FILE_AND_SRCDIR:Ljava/lang/String; = "srcdir and file are mutually exclusive"

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private destDir:Ljava/io/File;

.field private encoding:Ljava/lang/String;

.field private fcv:Ljava/util/Vector;

.field private file:Ljava/io/File;

.field private filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

.field private outputEncoding:Ljava/lang/String;

.field private preserveLastModified:Z

.field private srcDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->preserveLastModified:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-direct {v0}, Lorg/apache/tools/ant/filters/FixCrLfFilter;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fcv:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/FixCRLF;)Lorg/apache/tools/ant/filters/FixCrLfFilter;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/FixCRLF;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    return-object v0
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/FixCRLF;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/FixCRLF;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method private processFile(Ljava/lang/String;)V
    .locals 19
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fcv:Ljava/util/Vector;

    if-nez v2, :cond_0

    new-instance v16, Lorg/apache/tools/ant/types/FilterChain;

    invoke-direct/range {v16 .. v16}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/FilterChain;->add(Lorg/apache/tools/ant/filters/ChainableReader;)V

    new-instance v2, Ljava/util/Vector;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Ljava/util/Vector;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fcv:Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fcv:Ljava/util/Vector;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v2, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v5, "fixcrlf"

    const-string v6, ""

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v6, v7}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->deleteOnExit()V

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fcv:Ljava/util/Vector;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    if-nez v10, :cond_5

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual/range {v2 .. v11}, Lorg/apache/tools/ant/util/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/types/FilterSetCollection;Ljava/util/Vector;ZZLjava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V

    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v13, v12, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v14, 0x1

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "destFile exists"

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    sget-object v2, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, v13, v4}, Lorg/apache/tools/ant/util/FileUtils;->contentEquals(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v14, 0x1

    :goto_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    if-eqz v14, :cond_7

    const-string v2, " is being written"

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    :cond_1
    if-eqz v14, :cond_3

    sget-object v2, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, v4, v13}, Lorg/apache/tools/ant/util/FileUtils;->rename(Ljava/io/File;Ljava/io/File;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->preserveLastModified:Z

    if-eqz v2, :cond_2

    const-string v2, "preserved lastModified"

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    sget-object v2, Lorg/apache/tools/ant/taskdefs/FixCRLF;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-wide/from16 v0, v17

    invoke-virtual {v2, v13, v0, v1}, Lorg/apache/tools/ant/util/FileUtils;->setFileLastModified(Ljava/io/File;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    const/4 v4, 0x0

    :cond_3
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    goto/16 :goto_0

    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const/4 v14, 0x0

    goto :goto_2

    :cond_7
    const-string v2, " is not written, as the contents are identical"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v15

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private validate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->file:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "srcdir and file are mutually exclusive"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->fileset:Lorg/apache/tools/ant/types/FileSet;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FileSet;->setFile(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "srcdir attribute must be set!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "srcdir does not exist!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "srcdir is not a directory!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "destdir does not exist!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "destdir is not a directory!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-void
.end method


# virtual methods
.method public final chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1
    .param p1    # Ljava/io/Reader;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->chain(Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->validate()V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    if-nez v4, :cond_0

    const-string v1, "default"

    :goto_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "options: eol="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->getEol()Lorg/apache/tools/ant/filters/FixCrLfFilter$CrLf;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter$CrLf;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " tab="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->getTab()Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " eof="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->getEof()Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " tablength="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->getTablength()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " encoding="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " outputencoding="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    if-nez v5, :cond_1

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    invoke-super {p0, v4}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->getDirectoryScanner(Ljava/io/File;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_2
    array-length v4, v2

    if-ge v3, v4, :cond_2

    aget-object v4, v2, v3

    invoke-direct {p0, v4}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->processFile(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    goto :goto_1

    :cond_2
    return-void
.end method

.method public setCr(Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;

    const/4 v3, 0x1

    const-string v2, "DEPRECATED: The cr attribute has been deprecated,"

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    const-string v2, "Please use the eol attribute instead"

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->log(Ljava/lang/String;I)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;->getValue()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;-><init>()V

    const-string v2, "remove"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "lf"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;->setValue(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/FixCRLF;->setEol(Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;)V

    return-void

    :cond_0
    const-string v2, "asis"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "asis"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, "crlf"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDestdir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->destDir:Ljava/io/File;

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setEof(Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;->newInstance(Ljava/lang/String;)Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setEof(Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;)V

    return-void
.end method

.method public setEol(Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/FixCRLF$CrLf;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$CrLf;->newInstance(Ljava/lang/String;)Lorg/apache/tools/ant/filters/FixCrLfFilter$CrLf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setEol(Lorg/apache/tools/ant/filters/FixCrLfFilter$CrLf;)V

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->file:Ljava/io/File;

    return-void
.end method

.method public setFixlast(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setFixlast(Z)V

    return-void
.end method

.method public setJavafiles(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setJavafiles(Z)V

    return-void
.end method

.method public setOutputEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->outputEncoding:Ljava/lang/String;

    return-void
.end method

.method public setPreserveLastModified(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->preserveLastModified:Z

    return-void
.end method

.method public setSrcdir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->srcDir:Ljava/io/File;

    return-void
.end method

.method public setTab(Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/FixCRLF$AddAsisRemove;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;->newInstance(Ljava/lang/String;)Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setTab(Lorg/apache/tools/ant/filters/FixCrLfFilter$AddAsisRemove;)V

    return-void
.end method

.method public setTablength(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/FixCRLF;->filter:Lorg/apache/tools/ant/filters/FixCrLfFilter;

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter;->setTablength(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
