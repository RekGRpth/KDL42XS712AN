.class public Lorg/apache/tools/ant/taskdefs/AntStructure;
.super Lorg/apache/tools/ant/Task;
.source "AntStructure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/AntStructure$1;,
        Lorg/apache/tools/ant/taskdefs/AntStructure$DTDPrinter;,
        Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;
    }
.end annotation


# static fields
.field private static final LINE_SEP:Ljava/lang/String;

.field static class$java$lang$Boolean:Ljava/lang/Class;

.field static class$org$apache$tools$ant$TaskContainer:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$EnumeratedAttribute:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$Reference:Ljava/lang/Class;


# instance fields
.field private output:Ljava/io/File;

.field private printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/AntStructure;->LINE_SEP:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/AntStructure$DTDPrinter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/AntStructure$DTDPrinter;-><init>(Lorg/apache/tools/ant/taskdefs/AntStructure$1;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    return-void
.end method

.method static access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/AntStructure;->LINE_SEP:Ljava/lang/String;

    return-object v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    return-void
.end method

.method protected areNmtokens([Ljava/lang/String;)Z
    .locals 1
    .param p1    # [Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/AntStructure$DTDPrinter;->areNmtokens([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->output:Ljava/io/File;

    if-nez v8, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "output attribute is required"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/PrintWriter;

    new-instance v8, Ljava/io/OutputStreamWriter;

    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->output:Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v10, "UTF8"

    invoke-direct {v8, v9, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v3, v8}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v3

    :goto_0
    :try_start_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/Project;->getTaskDefinitions()Ljava/util/Hashtable;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/tools/ant/Project;->getDataTypeDefinitions()Ljava/util/Hashtable;

    move-result-object v11

    invoke-interface {v8, v2, v9, v10, v11}, Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;->printHead(Ljava/io/PrintWriter;Lorg/apache/tools/ant/Project;Ljava/util/Hashtable;Ljava/util/Hashtable;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    invoke-interface {v8, v2}, Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;->printTargetDecl(Ljava/io/PrintWriter;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getDataTypeDefinitions()Ljava/util/Hashtable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getDataTypeDefinitions()Ljava/util/Hashtable;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    invoke-interface {v9, v2, v10, v6, v8}, Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;->printElementDecl(Ljava/io/PrintWriter;Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Error writing "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->output:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v1, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v8

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    :cond_1
    throw v8

    :catch_1
    move-exception v7

    :try_start_3
    new-instance v3, Ljava/io/PrintWriter;

    new-instance v8, Ljava/io/FileWriter;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->output:Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v8}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getTaskDefinitions()Ljava/util/Hashtable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AntStructure;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getTaskDefinitions()Ljava/util/Hashtable;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    invoke-interface {v9, v2, v10, v4, v8}, Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;->printElementDecl(Ljava/io/PrintWriter;Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Class;)V

    goto :goto_2

    :cond_3
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->printer:Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;

    invoke-interface {v8, v2}, Lorg/apache/tools/ant/taskdefs/AntStructure$StructurePrinter;->printTail(Ljava/io/PrintWriter;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    :cond_4
    return-void
.end method

.method protected isNmtoken(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/AntStructure$DTDPrinter;->isNmtoken(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AntStructure;->output:Ljava/io/File;

    return-void
.end method
