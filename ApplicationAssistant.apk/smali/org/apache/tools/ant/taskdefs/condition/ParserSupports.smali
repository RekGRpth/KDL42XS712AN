.class public Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "ParserSupports.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# static fields
.field public static final ERROR_BOTH_ATTRIBUTES:Ljava/lang/String; = "Property and feature attributes are exclusive"

.field public static final ERROR_NO_ATTRIBUTES:Ljava/lang/String; = "Neither feature or property are set"

.field public static final ERROR_NO_VALUE:Ljava/lang/String; = "A value is needed when testing for property support"

.field public static final FEATURE:Ljava/lang/String; = "feature"

.field public static final NOT_RECOGNIZED:Ljava/lang/String; = " not recognized: "

.field public static final NOT_SUPPORTED:Ljava/lang/String; = " not supported: "

.field public static final PROPERTY:Ljava/lang/String; = "property"


# instance fields
.field private feature:Ljava/lang/String;

.field private property:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    return-void
.end method

.method private getReader()Lorg/xml/sax/XMLReader;
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getParser()Lorg/xml/sax/Parser;

    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public eval()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Property and feature attributes are exclusive"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Neither feature or property are set"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->evalFeature()Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "A value is needed when testing for property support"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->evalProperty()Z

    move-result v0

    goto :goto_0
.end method

.method public evalFeature()Z
    .locals 7

    const/4 v6, 0x3

    const/4 v3, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->getReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    if-nez v4, :cond_0

    const-string v4, "true"

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    :cond_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    invoke-static {v4}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v2

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x1

    :goto_0
    return v3

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "feature not recognized: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->log(Ljava/lang/String;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "feature not supported: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public evalProperty()Z
    .locals 6

    const/4 v5, 0x3

    const/4 v2, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->getReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "property not recognized: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->log(Ljava/lang/String;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "property not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public setFeature(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->feature:Ljava/lang/String;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->property:Ljava/lang/String;

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ParserSupports;->value:Ljava/lang/String;

    return-void
.end method
