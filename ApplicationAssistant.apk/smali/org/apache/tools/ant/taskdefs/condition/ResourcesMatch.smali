.class public Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;
.super Ljava/lang/Object;
.source "ResourcesMatch.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# instance fields
.field private asText:Z

.field private resources:Lorg/apache/tools/ant/types/resources/Union;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->asText:Z

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    :goto_1
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    goto :goto_1
.end method

.method public eval()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x1

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    if-nez v5, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "You must specify one or more nested resource collections"

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v5

    if-le v5, v4, :cond_1

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->resources:Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/resources/Union;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/Resource;

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/Resource;

    :try_start_0
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->asText:Z

    invoke-static {v2, v3, v5}, Lorg/apache/tools/ant/util/ResourceUtils;->contentEquals(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_2

    const/4 v4, 0x0

    :cond_1
    return v4

    :catch_0
    move-exception v1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "when comparing resources "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :cond_2
    move-object v2, v3

    goto :goto_0
.end method

.method public setAsText(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ResourcesMatch;->asText:Z

    return-void
.end method
