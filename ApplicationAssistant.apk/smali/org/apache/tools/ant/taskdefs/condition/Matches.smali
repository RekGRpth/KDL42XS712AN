.class public Lorg/apache/tools/ant/taskdefs/condition/Matches;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "Matches.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# instance fields
.field private caseSensitive:Z

.field private multiLine:Z

.field private regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

.field private singleLine:Z

.field private string:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->caseSensitive:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->multiLine:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->singleLine:Z

    return-void
.end method


# virtual methods
.method public addRegexp(Lorg/apache/tools/ant/types/RegularExpression;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/RegularExpression;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one regular expression is allowed."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    return-void
.end method

.method public eval()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->string:Ljava/lang/String;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Parameter string is required in matches."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    if-nez v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Missing pattern in matches."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v0, 0x0

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->caseSensitive:Z

    if-nez v2, :cond_2

    or-int/lit16 v0, v0, 0x100

    :cond_2
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->multiLine:Z

    if-eqz v2, :cond_3

    or-int/lit16 v0, v0, 0x1000

    :cond_3
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->singleLine:Z

    if-eqz v2, :cond_4

    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/condition/Matches;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/RegularExpression;->getRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->string:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lorg/apache/tools/ant/util/regexp/Regexp;->matches(Ljava/lang/String;I)Z

    move-result v2

    return v2
.end method

.method public setCasesensitive(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->caseSensitive:Z

    return-void
.end method

.method public setMultiline(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->multiLine:Z

    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one regular expression is allowed."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/RegularExpression;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/RegularExpression;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->regularExpression:Lorg/apache/tools/ant/types/RegularExpression;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->setPattern(Ljava/lang/String;)V

    return-void
.end method

.method public setSingleLine(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->singleLine:Z

    return-void
.end method

.method public setString(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/Matches;->string:Ljava/lang/String;

    return-void
.end method
