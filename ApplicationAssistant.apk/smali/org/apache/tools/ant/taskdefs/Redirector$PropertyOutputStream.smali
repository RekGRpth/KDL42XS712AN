.class Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
.super Ljava/io/ByteArrayOutputStream;
.source "Redirector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Redirector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PropertyOutputStream"
.end annotation


# instance fields
.field private closed:Z

.field private property:Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Redirector;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->this$0:Lorg/apache/tools/ant/taskdefs/Redirector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->closed:Z

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->property:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->closed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->this$0:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->access$000(Lorg/apache/tools/ant/taskdefs/Redirector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->this$0:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->access$100(Lorg/apache/tools/ant/taskdefs/Redirector;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->this$0:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->property:Ljava/lang/String;

    invoke-static {v0, p0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->access$200(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->closed:Z

    :cond_1
    return-void
.end method
