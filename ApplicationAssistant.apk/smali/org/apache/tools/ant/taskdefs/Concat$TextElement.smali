.class public Lorg/apache/tools/ant/taskdefs/Concat$TextElement;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "Concat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Concat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextElement"
.end annotation


# instance fields
.field private encoding:Ljava/lang/String;

.field private filtering:Z

.field private trim:Z

.field private trimLeading:Z

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trimLeading:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trim:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->filtering:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->encoding:Ljava/lang/String;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/Concat$TextElement;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Concat$TextElement;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getFiltering()Z

    move-result v0

    return v0
.end method

.method private getFiltering()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->filtering:Z

    return v0
.end method


# virtual methods
.method public addText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 7

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    if-nez v6, :cond_0

    const-string v6, ""

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    :cond_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    const-string v6, ""

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    :cond_1
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trimLeading:Z

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuffer;

    array-length v6, v2

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v5, 0x1

    const/4 v3, 0x0

    :goto_0
    array-length v6, v2

    if-ge v3, v6, :cond_6

    add-int/lit8 v4, v3, 0x1

    aget-char v1, v2, v3

    if-eqz v5, :cond_3

    const/16 v6, 0x20

    if-eq v1, v6, :cond_9

    const/16 v6, 0x9

    if-ne v1, v6, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v6, 0xa

    if-eq v1, v6, :cond_4

    const/16 v6, 0xd

    if-ne v1, v6, :cond_5

    :cond_4
    const/4 v5, 0x1

    :cond_5
    move v3, v4

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    :cond_7
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trim:Z

    if-eqz v6, :cond_8

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    :cond_8
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;

    return-object v6

    :cond_9
    move v3, v4

    goto :goto_0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "File "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " does not exist."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->encoding:Ljava/lang/String;

    if-nez v3, :cond_1

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->readFully(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->value:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    return-void

    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->encoding:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v3

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v3
.end method

.method public setFiltering(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->filtering:Z

    return-void
.end method

.method public setTrim(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trim:Z

    return-void
.end method

.method public setTrimLeading(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Concat$TextElement;->trimLeading:Z

    return-void
.end method
