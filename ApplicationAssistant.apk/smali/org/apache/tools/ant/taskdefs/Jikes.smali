.class public Lorg/apache/tools/ant/taskdefs/Jikes;
.super Ljava/lang/Object;
.source "Jikes.java"


# instance fields
.field protected command:Ljava/lang/String;

.field protected jop:Lorg/apache/tools/ant/taskdefs/JikesOutputParser;

.field protected project:Lorg/apache/tools/ant/Project;


# direct methods
.method protected constructor <init>(Lorg/apache/tools/ant/taskdefs/JikesOutputParser;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/JikesOutputParser;
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "As of Ant 1.2 released in October 2000, the Jikes class"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "is considered to be dead code by the Ant developers and is unmaintained."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Don\'t use it!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Jikes;->jop:Lorg/apache/tools/ant/taskdefs/JikesOutputParser;

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/Jikes;->command:Ljava/lang/String;

    iput-object p3, p0, Lorg/apache/tools/ant/taskdefs/Jikes;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method


# virtual methods
.method protected compile([Ljava/lang/String;)V
    .locals 16
    .param p1    # [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v10, 0x0

    :try_start_0
    const-string v12, "os.name"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    const-string v13, "windows"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    if-ltz v12, :cond_3

    move-object/from16 v0, p1

    array-length v12, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v13, 0xfa

    if-le v12, v13, :cond_3

    const/4 v7, 0x0

    :try_start_1
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "jikes"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v13, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-direct {v13, v14, v15}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v13}, Ljava/util/Random;->nextLong()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v8, Ljava/io/PrintWriter;

    new-instance v12, Ljava/io/FileWriter;

    invoke-direct {v12, v11}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v12}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    const/4 v5, 0x0

    :goto_0
    :try_start_3
    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v5, v12, :cond_0

    aget-object v12, p1, v5

    invoke-virtual {v8, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v8}, Ljava/io/PrintWriter;->flush()V

    const/4 v12, 0x2

    new-array v2, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Jikes;->command:Ljava/lang/String;

    aput-object v13, v2, v12

    const/4 v12, 0x1

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "@"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v2, v12
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :try_start_4
    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v10, v11

    move-object v1, v2

    :goto_1
    :try_start_5
    new-instance v4, Lorg/apache/tools/ant/taskdefs/Execute;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/Jikes;->jop:Lorg/apache/tools/ant/taskdefs/JikesOutputParser;

    invoke-direct {v4, v12}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/Jikes;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v4, v12}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/ant/taskdefs/Jikes;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v12}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v4, v12}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    invoke-virtual {v4, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_1
    return-void

    :catch_0
    move-exception v3

    :goto_2
    :try_start_6
    new-instance v12, Lorg/apache/tools/ant/BuildException;

    const-string v13, "Error creating temporary file"

    invoke-direct {v12, v13, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v12

    :goto_3
    :try_start_7
    invoke-static {v7}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    throw v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v12

    :goto_4
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_2
    throw v12

    :cond_3
    :try_start_8
    move-object/from16 v0, p1

    array-length v12, v0

    add-int/lit8 v12, v12, 0x1

    new-array v1, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/taskdefs/Jikes;->command:Ljava/lang/String;

    aput-object v13, v1, v12

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p1

    array-length v14, v0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v1, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :catch_1
    move-exception v3

    new-instance v12, Lorg/apache/tools/ant/BuildException;

    const-string v13, "Error running Jikes compiler"

    invoke-direct {v12, v13, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_2
    move-exception v12

    move-object v10, v11

    move-object v1, v2

    goto :goto_4

    :catchall_3
    move-exception v12

    move-object v10, v11

    goto :goto_3

    :catchall_4
    move-exception v12

    move-object v7, v8

    move-object v10, v11

    goto :goto_3

    :catch_2
    move-exception v3

    move-object v10, v11

    goto :goto_2

    :catch_3
    move-exception v3

    move-object v7, v8

    move-object v10, v11

    goto :goto_2
.end method
