.class public Lorg/apache/tools/ant/taskdefs/ManifestTask;
.super Lorg/apache/tools/ant/Task;
.source "ManifestTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;
    }
.end annotation


# instance fields
.field private encoding:Ljava/lang/String;

.field private manifestFile:Ljava/io/File;

.field private mode:Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

.field private nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Manifest;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->mode:Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->mode:Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    const-string v1, "replace"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;->setValue(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Manifest;->addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;)V

    return-void
.end method

.method public addConfiguredSection(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest$Section;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Manifest;->addConfiguredSection(Lorg/apache/tools/ant/taskdefs/Manifest$Section;)V

    return-void
.end method

.method public execute()V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v17, v0

    if-nez v17, :cond_0

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    const-string v18, "the file attribute is required"

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_0
    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Manifest;->getDefaultManifest()Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v14

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_1

    const/4 v7, 0x0

    const/4 v10, 0x0

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->encoding:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    new-instance v11, Ljava/io/InputStreamReader;

    const-string v17, "UTF-8"

    move-object/from16 v0, v17

    invoke-direct {v11, v8, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object v10, v11

    :goto_0
    new-instance v4, Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-direct {v4, v10}, Lorg/apache/tools/ant/taskdefs/Manifest;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    move-object v3, v4

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Manifest;->getWarnings()Ljava/util/Enumeration;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v17

    if-eqz v17, :cond_3

    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "Manifest warning: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->log(Ljava/lang/String;I)V

    goto :goto_2

    :cond_2
    :try_start_2
    new-instance v11, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->encoding:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v11, v8, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object v10, v11

    goto :goto_0

    :catch_0
    move-exception v12

    :goto_3
    :try_start_3
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "Existing manifest "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string v18, " is invalid"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v6, v0, v12, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    goto :goto_1

    :catch_1
    move-exception v5

    :goto_4
    :try_start_4
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "Failed to read "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v6, v0, v5, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v17

    :goto_5
    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v17

    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->mode:Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;->getValue()Ljava/lang/String;

    move-result-object v17

    const-string v18, "update"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_4

    if-eqz v3, :cond_6

    invoke-virtual {v14, v3}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->nestedManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V
    :try_end_5
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_5 .. :try_end_5} :catch_2

    invoke-virtual {v14, v3}, Lorg/apache/tools/ant/taskdefs/Manifest;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    const-string v17, "Manifest has not changed, do not recreate"

    const/16 v18, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->log(Ljava/lang/String;I)V

    :cond_5
    :goto_6
    return-void

    :cond_6
    if-eqz v6, :cond_4

    :try_start_6
    throw v6
    :try_end_6
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception v12

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    const-string v18, "Manifest is invalid"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v12, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v17

    :cond_7
    const/4 v15, 0x0

    :try_start_7
    new-instance v9, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v13, Ljava/io/OutputStreamWriter;

    const-string v17, "UTF-8"

    move-object/from16 v0, v17

    invoke-direct {v13, v9, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    new-instance v16, Ljava/io/PrintWriter;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lorg/apache/tools/ant/taskdefs/Manifest;->write(Ljava/io/PrintWriter;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v16, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/io/PrintWriter;->close()V

    goto :goto_6

    :catch_3
    move-exception v5

    :goto_7
    :try_start_9
    new-instance v17, Lorg/apache/tools/ant/BuildException;

    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    const-string v19, "Failed to write "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v17
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v17

    :goto_8
    if-eqz v15, :cond_8

    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    :cond_8
    throw v17

    :catchall_2
    move-exception v17

    move-object/from16 v15, v16

    goto :goto_8

    :catch_4
    move-exception v5

    move-object/from16 v15, v16

    goto :goto_7

    :catchall_3
    move-exception v17

    move-object v7, v8

    goto/16 :goto_5

    :catch_5
    move-exception v5

    move-object v7, v8

    goto/16 :goto_4

    :catch_6
    move-exception v12

    move-object v7, v8

    goto/16 :goto_3
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->manifestFile:Ljava/io/File;

    return-void
.end method

.method public setMode(Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestTask;->mode:Lorg/apache/tools/ant/taskdefs/ManifestTask$Mode;

    return-void
.end method
