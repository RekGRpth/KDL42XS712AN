.class Lorg/apache/tools/ant/taskdefs/Length$AllHandler;
.super Lorg/apache/tools/ant/taskdefs/Length$Handler;
.source "Length.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Length;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllHandler"
.end annotation


# instance fields
.field private accum:J

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Length;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/taskdefs/Length;Ljava/io/PrintStream;)V
    .locals 2
    .param p2    # Ljava/io/PrintStream;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Length$Handler;-><init>(Lorg/apache/tools/ant/taskdefs/Length;Ljava/io/PrintStream;)V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->this$0:Lorg/apache/tools/ant/taskdefs/Length;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->accum:J

    return-void
.end method


# virtual methods
.method complete()V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->getPs()Ljava/io/PrintStream;

    move-result-object v0

    iget-wide v1, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->accum:J

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->print(J)V

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/Length$Handler;->complete()V

    return-void
.end method

.method protected getAccum()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->accum:J

    return-wide v0
.end method

.method protected declared-synchronized handle(Lorg/apache/tools/ant/types/Resource;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->this$0:Lorg/apache/tools/ant/taskdefs/Length;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Size unknown for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/taskdefs/Length;->log(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-wide v2, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->accum:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/tools/ant/taskdefs/Length$AllHandler;->accum:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
