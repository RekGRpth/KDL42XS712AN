.class public Lorg/apache/tools/ant/taskdefs/Ant;
.super Lorg/apache/tools/ant/Task;
.source "Ant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;,
        Lorg/apache/tools/ant/taskdefs/Ant$Reference;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field static class$org$apache$tools$ant$Project:Ljava/lang/Class;


# instance fields
.field private antFile:Ljava/lang/String;

.field private dir:Ljava/io/File;

.field private inheritAll:Z

.field private inheritRefs:Z

.field private newProject:Lorg/apache/tools/ant/Project;

.field private out:Ljava/io/PrintStream;

.field private output:Ljava/lang/String;

.field private properties:Ljava/util/Vector;

.field private propertySets:Ljava/util/Vector;

.field private references:Ljava/util/Vector;

.field private targetAttributeSet:Z

.field private targets:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Ant;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritAll:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritRefs:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->references:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->propertySets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targets:Ljava/util/Vector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targetAttributeSet:Z

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Task;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritAll:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritRefs:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->references:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->propertySets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targets:Ljava/util/Vector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targetAttributeSet:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->bindToOwner(Lorg/apache/tools/ant/Task;)V

    return-void
.end method

.method private addAlmostAll(Ljava/util/Hashtable;)V
    .locals 4
    .param p1    # Ljava/util/Hashtable;

    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "basedir"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ant.file"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v3, v1}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v3, v1, v2}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addReferences()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/Project;->getReferences()Ljava/util/Hashtable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Hashtable;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v7}, Lorg/apache/tools/ant/Project;->getReferences()Ljava/util/Hashtable;

    move-result-object v2

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->references:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-lez v7, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->references:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/taskdefs/Ant$Reference;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Ant$Reference;->getRefId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "the refid attribute is required for reference elements"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {v5, v4}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Parent project doesn\'t contain any reference \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Ant$Reference;->getToRefid()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    move-object v6, v4

    :cond_2
    invoke-direct {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Ant;->copyReference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritRefs:Z

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-direct {p0, v1, v1}, Lorg/apache/tools/ant/taskdefs/Ant;->copyReference(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/Project;->inheritIDReferences(Lorg/apache/tools/ant/Project;)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private copyReference(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    invoke-virtual {v7, p1}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "No object referenced by "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, ". Can\'t copy to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7, v9}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    move-object v2, v5

    :try_start_0
    const-string v7, "clone"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v0, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v1, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Adding clone of reference "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {p0, v7, v8}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :cond_1
    :goto_1
    instance-of v7, v2, Lorg/apache/tools/ant/ProjectComponent;

    if-eqz v7, :cond_3

    move-object v7, v2

    check-cast v7, Lorg/apache/tools/ant/ProjectComponent;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/ProjectComponent;->setProject(Lorg/apache/tools/ant/Project;)V

    :cond_2
    :goto_2
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v7, p2, v2}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v8, "setProject"

    const/4 v7, 0x1

    new-array v9, v7, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object v7, Lorg/apache/tools/ant/taskdefs/Ant;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    if-nez v7, :cond_4

    const-string v7, "org.apache.tools.ant.Project"

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/Ant;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    sput-object v7, Lorg/apache/tools/ant/taskdefs/Ant;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    :goto_3
    aput-object v7, v9, v10

    invoke-virtual {v0, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    if-eqz v6, :cond_2

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    aput-object v9, v7, v8

    invoke-virtual {v6, v2, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :catch_0
    move-exception v7

    goto :goto_2

    :cond_4
    sget-object v7, Lorg/apache/tools/ant/taskdefs/Ant;->class$org$apache$tools$ant$Project:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v3

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Error setting new project instance for reference with id "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v7, v4, v3, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7

    :catch_2
    move-exception v7

    goto :goto_1
.end method

.method private getBuildListeners()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->getBuildListeners()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private initializeProject()V
    .locals 9

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/Project;->getInputHandler()Lorg/apache/tools/ant/input/InputHandler;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/Project;->setInputHandler(Lorg/apache/tools/ant/input/InputHandler;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getBuildListeners()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/tools/ant/BuildListener;

    invoke-virtual {v7, v6}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    if-eqz v6, :cond_1

    const/4 v4, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    if-eqz v6, :cond_2

    sget-object v6, Lorg/apache/tools/ant/taskdefs/Ant;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    :goto_1
    :try_start_0
    new-instance v6, Ljava/io/PrintStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    new-instance v3, Lorg/apache/tools/ant/DefaultLogger;

    invoke-direct {v3}, Lorg/apache/tools/ant/DefaultLogger;-><init>()V

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/DefaultLogger;->setMessageOutputLevel(I)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/DefaultLogger;->setOutputPrintStream(Ljava/io/PrintStream;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/DefaultLogger;->setErrorPrintStream(Ljava/io/PrintStream;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v6, v3}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/Project;->copyUserProperties(Lorg/apache/tools/ant/Project;)V

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritAll:Z

    if-nez v6, :cond_3

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->setSystemProperties()V

    :goto_3
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Ant;->propertySets:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/Ant;->addAlmostAll(Ljava/util/Hashtable;)V

    goto :goto_4

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Ant: Can\'t set output to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->getProperties()Ljava/util/Hashtable;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/Ant;->addAlmostAll(Ljava/util/Hashtable;)V

    goto :goto_3

    :cond_4
    return-void
.end method

.method private overrideProperties()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_0
    if-ltz v1, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Property;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Property;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Property;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Property;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Property;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Property;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v4}, Lorg/apache/tools/ant/taskdefs/Property;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Property;->execute()V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Project;->copyInheritedProperties(Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method private reinit()V
    .locals 0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->init()V

    return-void
.end method


# virtual methods
.method public addConfiguredTarget(Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targetAttributeSet:Z

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "nested target is incompatible with the target attribute"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "target name must not be empty"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targets:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->propertySets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addReference(Lorg/apache/tools/ant/taskdefs/Ant$Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->references:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createProperty()Lorg/apache/tools/ant/taskdefs/Property;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Property;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Property;-><init>(ZLorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getNewProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Property;->setProject(Lorg/apache/tools/ant/Project;)V

    const-string v1, "property"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Property;->setTaskName(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public execute()V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    new-instance v10, Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->targets:Ljava/util/Vector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getNewProject()Lorg/apache/tools/ant/Project;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v17, v0

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritAll:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    :cond_0
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->initializeProject()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lorg/apache/tools/ant/Project;->setBaseDir(Ljava/io/File;)V

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    const-string v18, "basedir"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lorg/apache/tools/ant/Project;->setInheritedProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->overrideProperties()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    const-string v17, "build.xml"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    :cond_2
    sget-object v17, Lorg/apache/tools/ant/taskdefs/Ant;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "calling target(s) "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v17

    if-lez v17, :cond_4

    invoke-virtual {v10}, Ljava/util/Vector;->toString()Ljava/lang/String;

    move-result-object v17

    :goto_1
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string v18, " in build file "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    const-string v18, "ant.file"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Lorg/apache/tools/ant/Project;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v17

    const-string v18, "ant.file"

    invoke-virtual/range {v17 .. v18}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v17

    if-eqz v17, :cond_6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getTaskName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "antcall"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    const-string v18, "antcall must not be used at the top level."

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v17

    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v18

    if-eqz v18, :cond_10

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/tools/ant/taskdefs/Property;

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setProject(Lorg/apache/tools/ant/Project;)V

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    goto/16 :goto_0

    :cond_4
    const-string v17, "[default]"

    goto/16 :goto_1

    :cond_5
    new-instance v17, Lorg/apache/tools/ant/BuildException;

    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getTaskName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " task at the"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " top level must not invoke"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " its own build file."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lorg/apache/tools/ant/ProjectHelper;->configureProject(Lorg/apache/tools/ant/Project;Ljava/io/File;)V
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v17

    if-nez v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Project;->getDefaultTarget()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v10, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    const-string v18, "ant.file"

    invoke-virtual/range {v17 .. v18}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v18

    const-string v19, "ant.file"

    invoke-virtual/range {v18 .. v19}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v17

    if-eqz v17, :cond_b

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getTaskName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " task calling "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "its own parent target."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17

    :catch_0
    move-exception v7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lorg/apache/tools/ant/ProjectHelper;->addLocationToBuildException(Lorg/apache/tools/ant/BuildException;Lorg/apache/tools/ant/Location;)Lorg/apache/tools/ant/BuildException;

    move-result-object v17

    throw v17

    :cond_8
    const/4 v4, 0x0

    invoke-virtual {v10}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    if-nez v4, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_a

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Project;->getTargets()Ljava/util/Hashtable;

    move-result-object v17

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/tools/ant/Target;

    move-object/from16 v0, v17

    check-cast v0, Lorg/apache/tools/ant/Target;

    move-object v11, v0

    if-eqz v11, :cond_9

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/Target;->dependsOn(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    const/16 v17, 0x1

    :goto_4
    or-int v4, v4, v17

    goto :goto_3

    :cond_9
    const/16 v17, 0x0

    goto :goto_4

    :cond_a
    if-eqz v4, :cond_b

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getTaskName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " task calling a target"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " that depends on"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " its parent target \'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "\'."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_b
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->addReferences()V

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v17

    if-lez v17, :cond_d

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    const-string v17, ""

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v17

    if-nez v17, :cond_d

    :cond_c
    const/4 v3, 0x0

    :try_start_4
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "Entering "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string v18, "..."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/Project;->fireSubBuildStarted()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lorg/apache/tools/ant/Project;->executeTargets(Ljava/util/Vector;)V
    :try_end_4
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "Exiting "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string v18, "."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/Project;->fireSubBuildFinished(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_d
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->properties:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/tools/ant/taskdefs/Property;

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setProject(Lorg/apache/tools/ant/Project;)V

    goto :goto_5

    :catch_1
    move-exception v7

    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lorg/apache/tools/ant/ProjectHelper;->addLocationToBuildException(Lorg/apache/tools/ant/BuildException;Lorg/apache/tools/ant/Location;)Lorg/apache/tools/ant/BuildException;

    move-result-object v3

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v17

    :try_start_7
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    const-string v19, "Exiting "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/Project;->fireSubBuildFinished(Ljava/lang/Throwable;)V

    throw v17
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    move-object/from16 v17, v0

    if-eqz v17, :cond_f

    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/PrintStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    :cond_f
    :goto_6
    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    return-void

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Ant;->out:Ljava/io/PrintStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/PrintStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    :cond_11
    :goto_7
    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    throw v17

    :catch_2
    move-exception v18

    goto :goto_7

    :catch_3
    move-exception v17

    goto :goto_6
.end method

.method protected getNewProject()Lorg/apache/tools/ant/Project;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->reinit()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method public handleErrorFlush(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/tools/ant/Project;->demuxFlush(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleErrorOutput(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/tools/ant/Project;->demuxOutput(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleFlush(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/tools/ant/Project;->demuxFlush(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/Project;->demuxInput([BII)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tools/ant/Task;->handleInput([BII)I

    move-result v0

    goto :goto_0
.end method

.method public handleOutput(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/tools/ant/Project;->demuxOutput(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public init()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Ant;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->createSubProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->newProject:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->setJavaVersionProperty()V

    return-void
.end method

.method public setAntfile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->antFile:Ljava/lang/String;

    return-void
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->dir:Ljava/io/File;

    return-void
.end method

.method public setInheritAll(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritAll:Z

    return-void
.end method

.method public setInheritRefs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->inheritRefs:Z

    return-void
.end method

.method public setOutput(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Ant;->output:Ljava/lang/String;

    return-void
.end method

.method public setTarget(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "target attribute must not be empty"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Ant;->targetAttributeSet:Z

    return-void
.end method
