.class Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;
.super Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncherProxy;
.source "Execute.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Execute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WinNTCommandLauncher"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncherProxy;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    return-void
.end method


# virtual methods
.method public exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;
    .locals 6
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p4

    if-nez p4, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v0

    :cond_0
    const/4 v2, 0x6

    array-length v3, p2

    add-int/lit8 v3, v3, 0x6

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "cmd"

    aput-object v3, v1, v5

    const/4 v3, 0x1

    const-string v4, "/c"

    aput-object v4, v1, v3

    const/4 v3, 0x2

    const-string v4, "cd"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string v4, "/d"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x5

    const-string v4, "&&"

    aput-object v4, v1, v3

    const/4 v3, 0x6

    array-length v4, p2

    invoke-static {p2, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, p1, v1, p3}, Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;->exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;->exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    goto :goto_0
.end method
